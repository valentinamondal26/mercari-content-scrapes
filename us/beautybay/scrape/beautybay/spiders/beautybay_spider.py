'''
https://mercari.atlassian.net/browse/USCC-380 - Jeffree Star Cosmetics Lip Makeup
https://mercari.atlassian.net/browse/USCC-382 - Jeffree Star Cosmetics Makeup palettes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider

import json
import itertools
from parsel import Selector
from urllib.parse import urlparse, parse_qs, urlencode

class BeautybaySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from beautybay.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Lip Makeup
        2) Makeup palettes

    brand : str
        brand to be crawled, Supports
        1) Jeffree Star Cosmetics

    Command e.g:
    scrapy crawl beautybay -a category='Lip Makeup' -a brand='Jeffree Star Cosmetics'
    scrapy crawl beautybay -a category='Makeup palettes' -a brand='Jeffree Star Cosmetics'
    """

    name = 'beautybay'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'beautybay.com'
    blob_name = 'beautybay.txt'

    url_dict = {
        'Lip Makeup': {
            'Jeffree Star Cosmetics': {
                'url': 'https://www.beautybay.com/l/jeffreestarcosmetics/?f_filter_body_areas=Lips',
                'filters': [
                    'Product Type', 'Collection', 'Colour',
                ]
            }
        },
        'Makeup palettes': {
            'Jeffree Star Cosmetics': {
                'url': 'https://www.beautybay.com/l/jeffreestarcosmetics/palettes/',
                'filters': [
                    'Product Type', 'Collection', 'Colour', 'Body Area',
                ]
            }
        },
    }

    base_url = 'https://www.beautybay.com'

    auth_bearer_token = ''

    filter_list = []

    extra_item_infos = {}

    listing_page_url = 'https://lister-page-api.public.prd.beautybay.com/listings?'

    detail_page_api_url = 'https://pdp-api.public.prd.beautybay.com/product/{}-{}'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BeautybaySpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        product_url = response.meta.get('product_url', '')
        data = json.loads(response.text)

        facets = {}
        for facet in data.get('attraqt', {}).get('facets', []):
            facets[facet.get('facet', '')] = facet.get('values', '')

        facets['product_type'] = data.get('attraqt', {}).get('productType', '')

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,

            'title': data.get('name', ''),
            'description': Selector(data.get('description', '')).xpath('//p/text()').extract_first(default=''),
            'price': data.get('price', {}).get('amount', ''),
            'facets': facets,
            'variation_type': data.get('variationType', ''),
            'product_url': product_url,
        }

        if data.get('variationType', '') and data.get('variants', {}):
            variants_in_stock = data.get('variants', {}).get('inStock', [])
            variants_out_of_stock = data.get('variants', {}).get('outOfStock', [])
            for variant in itertools.chain(variants_in_stock, variants_out_of_stock):
                item.update({
                    'id': self.base_url + product_url.split('?')[0] + '?variant=' + variant.get('url', ''),

                    'variant_code': variant.get('sku', ''),
                    'variant_name': variant.get('name', ''),
                    'measurement': variant.get('measurement', ''),
                    'shade_description': variant.get('shadeDescription', ''),
                })
        else:
            item.update({
                'id': self.base_url + product_url.split('?')[0],

                'variant_code': data.get('sku', ''),
                'measurement': data.get('measurement', ''),
                'shade_description': data.get('shadeDescription', ''),
            })

        variants = data.get("variants").get(
            "inStock")+data.get("variants").get("outOfStock")
        color_variants = [cl.get("name", "") for cl in variants]
        color_description = [cl.get("shadeDescription", "") for cl in variants]
        urls = [cl.get("url", "") for cl in variants]
        images = [cl.get("swatch", "") for cl in variants]
        if color_variants:
            for ctr,color in enumerate(color_variants):
                items={}
                items.update(item)
                items['id'] = item['id'][:item['id'].rfind("/")][: item['id'][:item['id'].rfind("/")].rfind("/")]+"/"+urls[ctr]+"/"
                self.update_extra_item_infos(
                    items['id'], response.meta.get('extra_item_info', {}))
                items['image'] = urls[ctr]
                items['color_variant'] = color
                items['color_description'] = color_description[ctr]
                yield items
        else:
            self.update_extra_item_infos(
                items['id'], response.meta.get('extra_item_info', {}))
            yield item


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def parse_listing_page(self, response):
        extra_item_info = response.meta.get('extra_item_info', {})
        data = json.loads(response.text)

        for product in data.get('listerContent', {}).get('tiles', {}).get('content', []):
            product_url = product.get('productUrl', '')
            if product_url:
                if not self.extra_item_infos.get(product_url, None):
                    self.extra_item_infos[product_url] = {}

                self.update_extra_item_infos(product_url, response.meta.get('extra_item_info', {}))
                # self.extra_item_infos.get(product_url).update(extra_item_info)

                s = product_url.split('/p/')[-1].split('/')
                if len(s) > 1:

                    url = self.detail_page_api_url.format(s[0],s[1])
                    if len(s) > 2:
                        url = url + '?variant=' + s[2]
                    request = self.create_request(url=url, callback=self.crawlDetail)
                    request.meta['product_url'] = product_url
                    yield request

        total_pages = data.get('totalPages', 0)
        current_page = data.get('currentPage', 0)
        if current_page < total_pages:
            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            query_dict.update({'page':[current_page + 1]})
            url = parts._replace(query=urlencode(query_dict, True)).geturl()
            request = self.create_request(url=url, callback=self.parse_listing_page, meta={'dont_obey_robotstxt': True, })
            request.meta['extra_item_info'] = extra_item_info
            yield request


    def parse_filter(self, response):
        data = json.loads(response.text)

        totalProducts = data.get('totalProducts', 0)
        print(f'totalProducts: {totalProducts}')

        for facet in data.get('facets', []):
            if facet.get('title', '') in self.filter_list: # and not facet['selected']:
                for option in facet.get('options', []):
                    parts = urlparse(response.url)
                    query_dict = parse_qs(parts.query)
                    filters = query_dict.get('filters', ['{}'])
                    filter_info = json.loads(filters[0])
                    filter_info.update({
                        facet.get('id', ''): [option.get('title', '')],
                    })
                    query_dict['filters'] = json.dumps(filter_info)
                    url = parts._replace(query=urlencode(query_dict, True)).geturl()

                    request = self.create_request(url=url, callback=self.parse_listing_page, meta={'dont_obey_robotstxt': True, })

                    request.meta['extra_item_info'] = {
                        facet.get('title', ''): option.get('title', ''),
                    }
                    yield request

        request = self.create_request(url=response.url, callback=self.parse_listing_page, meta={'dont_obey_robotstxt': True, }, dont_filter= True)
        yield request


    def parse(self, response):
        self.auth_bearer_token = {cookie.decode().split('=', 1)[0]:cookie.decode().split('=', 1)[-1].split(';', 1)[0] for cookie in response.headers.getlist('Set-Cookie')}.get('AuthToken')
        print(f'auth_bearer_token:{self.auth_bearer_token}')
        if not self.auth_bearer_token:
            self.logger.error('Could not able to get the AuthToken from Cookie for this site, requried for the api parsing')
            return

        parts = urlparse(response.url)
        query_dict = parse_qs(parts.query)
        filters = {}
        for k,v in query_dict.items():
            if k.startswith('f_filter_'):
                filter_name = k.replace('f_filter_', '')
                filter_value = v
                if filter_name in filters:
                    filters[filter_name].append(filter_value)
                else:
                    filters[filter_name] = filter_value
        filter_info = json.dumps(filters)

        listing_page_params = {"pageUrl":"","filters": filter_info ,"sortOption":"{\"value\":\"null\",\"title\":\"Staff Faves\",\"sortValue\":null,\"sortOrder\":null,\"selected\":true}","page":"1","pageSize":"72","userId":"ca127c4b-d90e-4d74-84b4-33e2bb01ee83","sessionId":"5f05f90a-f53f-4e5b-b50a-0d0a98b22aea","preview":"false"}

        listing_page_params['pageUrl'] = self.launch_url.replace(self.base_url, '')
        url = self.listing_page_url + urlencode(listing_page_params, True)
        yield self.create_request(url=url, callback=self.parse_filter, meta={'dont_obey_robotstxt': True})


    def create_request(self, url, callback, meta=None, dont_filter=False):
        headers = None
        if self.auth_bearer_token:
            headers = {
                'Authorization': f'Bearer {self.auth_bearer_token}'
            }

        request = scrapy.Request(url=url, callback=callback, meta=meta, headers=headers, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request
