import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color_variant = record.get('color_variant', '')
            product_types = [record.get('facets', {}).get('product_type')]
            product_type = ', '.join(product_types)
            product_type = re.sub(r'Lipsticks', 'Lipstick', product_type)
            product_type = re.sub(r'Lip Scrubs', 'Lip Scrub', product_type)
            # product_types = record.get('Product Type', [])

            description = re.sub(r'\n', ' ', record.get('description', ''))
            description = re.sub(re.compile(
                r'Jeffree Star approved! BEAUTY BAY are an approved retailer of Jeffree Star Cosmetics|'
                r'__Limited to \d+ per person__|Beauty editor approved|– read more', re.IGNORECASE), '', description).strip()
            description = description.strip().strip('-')
            description = re.sub(r'\s+', ' ', description).strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bBundle\b|X\s*Jeffree', re.IGNORECASE), '', model)
            if color_variant and color_variant!='Shane':
                model = re.sub(re.compile(r'(-\s*){0,1}\b'+color_variant+r'\b', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            color = record.get("color_description","").strip()
            color = color.title()
            color = re.sub(re.compile(r"^A\s", re.IGNORECASE), "", color)
            color = " ".join(color.split())
        
            formulation = ', '.join(record.get('facets', {}).get('formulation', ''))
            if not formulation:
                match = re.findall(re.compile(r'\bLiquid\b|\bPencil\b|\bScrub\b|\bStick\b', re.IGNORECASE), model)
                if match:
                    formulation = match[0]

            if re.findall(re.compile(r'\bSets\b', re.IGNORECASE), ','.join(product_types)) or \
                re.findall(re.compile(r'\bBundle\b', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "ner_query": ner_query,

                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                "model": model,
                'product_type': product_type,
                "color": color or color_variant,
                'collection': ', '.join(record.get('facets', {}).get('collection', '')),
                'ingredient': ', '.join(record.get('facets', {}).get('preference', '')),
                'finish': ', '.join(record.get('facets', {}).get('finish', '')),
                'formulation': formulation,
                "description": description,
            }

            return transformed_record
        else:
            return None
