import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            colors = record.get('Colour', '')
            product_types = record.get('Product Type', [])
            product_type = ' '.join(product_types)
            product_type = re.sub(r'Highlighters', 'Highlighter', product_type)
            product_type = re.sub(r'Palettes', 'Palette', product_type)

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bBundle\b|\bMakeup palette\b|\bPalette\b|\bpalettes\b', re.IGNORECASE), '', model)
            if colors:
                model = re.sub(re.compile(pattern_words(colors), re.IGNORECASE), '', model)
            if product_types:
                model = re.sub(re.compile(pattern_words(product_types), re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            description = re.sub(r'\n', ' ', record.get('description', ''))
            description = re.sub(re.compile(
                r'Jeffree Star approved! BEAUTY BAY are an approved retailer of Jeffree Star Cosmetics|'
                r'__Limited to one per customer.__|'
                r'__Limited to \d+ per person__|Beauty editor approved|–\s*read more|\*|__', re.IGNORECASE), '', description).strip()
            description = description.strip().strip('-')
            description = re.sub(r'\s+', ' ', description).strip()

            if re.findall(re.compile(r'\bLipsticks\b|\bLiquid Lipstick\b|\bSets\b', re.IGNORECASE), ','.join(product_types)) or \
                re.findall(re.compile(r'\bBundle\b', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "ner_query": ner_query,

                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                "model": model,
                'product_type': product_type,
                "color": ', '.join(colors),
                'collection': ', '.join(record.get('Collection', '')),
                'ingredient': ', '.join(record.get('facets', {}).get('preference', '')),
                'finish': ', '.join(record.get('facets', {}).get('finish', '')),
                'body_area': ', '.join(record.get('Body Area', '')),
                "description": description,
            }

            return transformed_record
        else:
            return None
