import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile('LEGO|®|™', re.IGNORECASE), '', model)
            model = re.sub(re.compile('Donut shop opening', re.IGNORECASE), 'Donut Shop Opening', model)
            model = re.sub(re.compile('Hi-speed Chase', re.IGNORECASE), 'Hi-Speed Chase', model)
            model = re.sub(re.compile(r'\bT\. rex', re.IGNORECASE), 'T. Rex', model)
            model = re.sub(re.compile(r'\bAcc\.', re.IGNORECASE), 'Accessory', model)
            model = re.sub(' +', ' ', model).strip()

            product_line = record.get('product_line', '')
            product_line = re.sub(re.compile('®|™', re.IGNORECASE), '', product_line)
            product_line = re.sub(' +', ' ', product_line).strip()

            if re.findall(re.compile(r'Pencil|dictionary|Key Chain|hair|watch|draw|tumbler|'
                r'bag tag|alarm|NiteLite|Night Light|Sticker Roll|Torch|Movie Guide|Blu-ray|'
                r'Bottle|Notebook|Clip|Plush', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                'image': record.get('image', ''),
                'rating': record.get('rating', ''),
                'breadcrumb': record.get('breadcrumb', ''),

                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "model": model,
                "age_range": record.get('ages', ''),
                "number_of_pieces": record.get('pieces', ''),
                'product_line': product_line,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
