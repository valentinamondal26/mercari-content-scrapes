'''
https://mercari.atlassian.net/browse/USCC-71 - LEGO LEGO Bricks & Building Pieces
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
from urllib.parse import urlparse, parse_qs
import json
import re

class LegoSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from lego.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) LEGO Bricks & Building Pieces
    
    brand : str
        brand to be crawled, Supports
        1) LEGO

    Command e.g:
    scrapy crawl lego -a category='LEGO Bricks & Building Pieces' -a brand='LEGO'
    """

    name = 'lego'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'lego.com'
    blob_name = 'lego.txt'

    listing_api_url = 'https://www.lego.com/api/graphql/ContentPageQuery'

    url_dict = {
        'LEGO Bricks & Building Pieces': {
            'LEGO': [
                'https://www.lego.com/en-us/themes/architecture',
                'https://www.lego.com/en-us/themes/boost',
                'https://www.lego.com/en-us/themes/brickheadz',
                'https://www.lego.com/en-us/themes/city',
                'https://www.lego.com/en-us/themes/classic',
                'https://www.lego.com/en-us/themes/creator-3-in-1',
                'https://www.lego.com/en-us/themes/creator-expert',
                'https://www.lego.com/en-us/themes/dc',
                'https://www.lego.com/en-us/themes/disney',
                'https://www.lego.com/en-us/themes/duplo',
                'https://www.lego.com/en-us/themes/fantastic-beasts',
                'https://www.lego.com/en-us/themes/friends',
                'https://www.lego.com/en-us/themes/disney-frozen-2',
                'https://www.lego.com/en-us/themes/harry-potter',
                'https://www.lego.com/en-us/themes/hidden-side/products',
                'https://www.lego.com/en-us/themes/ideas',
                'https://www.lego.com/en-us/themes/juniors',
                'https://www.lego.com/en-us/themes/jurassic-world',
                'https://www.lego.com/en-us/themes/lego-batman-sets',
                'https://www.lego.com/en-us/themes/lego-originals',
                'https://www.lego.com/en-us/themes/lego-spider-man',
                'https://www.lego.com/en-us/themes/marvel',
                'https://www.lego.com/en-us/themes/mindstorms',
                'https://www.lego.com/en-us/themes/minecraft',
                'https://www.lego.com/en-us/themes/minifigures',
                'https://www.lego.com/en-us/themes/nexo-knights',
                'https://www.lego.com/en-us/themes/ninjago',
                'https://www.lego.com/en-us/themes/overwatch',
                'https://www.lego.com/en-us/themes/power-functions',
                'https://www.lego.com/en-us/themes/powered-up',
                'https://www.lego.com/en-us/themes/powerpuff-girls',
                'https://www.lego.com/en-us/themes/serious-play',
                'https://www.lego.com/en-us/themes/speed-champions',
                'https://www.lego.com/en-us/themes/star-wars',
                'https://www.lego.com/en-us/themes/stranger-things',
                'https://www.lego.com/en-us/themes/technic',
                'https://www.lego.com/en-us/themes/the-lego-batman-movie',
                'https://www.lego.com/en-us/themes/the-lego-movie-2',
                'https://www.lego.com/en-us/themes/the-lego-ninjago-movie',
                'https://www.lego.com/en-us/themes/toy-story-4',
                'https://www.lego.com/en-us/themes/unikitty',
                'https://www.lego.com/en-us/themes/xtra',
            ]
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(LegoSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_urls = self.url_dict.get(category, {}).get(brand, [])
        if not self.launch_urls:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + ','.join(self.launch_urls))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_urls))
        if self.launch_urls:
            for launch_url in self.launch_urls:
                yield self.create_request(url=launch_url, callback=self.parse)


    def create_post_request(self, url, callback, total):
        o = urlparse(url)
        d = parse_qs(o.query)
        icmp = ''
        if d:
            icmp = ",".join(d.get('icmp', []))
        path = o.path
        slug = path.rsplit('/en-us')[-1]

        data = {
            "operationName":"ContentPageQuery",
            "variables": {
                "page": 1,
                "isPaginated": False,
                "perPage": total,
                "sort": {"key":"FEATURED","direction":"DESC"},
                "filters":[],
                "slug": slug,
                "hideTargetedSections": False,
                "icmp": icmp,
            },
            "query":"query ContentPageQuery($slug: String!, $perPage: Int, $page: Int, $isPaginated: Boolean!, $sort: SortInput, $filters: [Filter!], $hideTargetedSections: Boolean!) {\n  contentPage(slug: $slug) {\n    analyticsGroup\n    analyticsPageTitle\n    metaTitle\n    metaDescription\n    metaOpenGraph {\n      title\n      description\n      imageUrl\n      __typename\n    }\n    url\n    title\n    displayTitleOnPage\n    ...Breadcrumbs\n    sections {\n      ... on LayoutSection {\n        ...PageLayoutSection\n        __typename\n      }\n      ...ContentSections\n      ... on TargetedSection {\n        hidden(hideTargetedSections: $hideTargetedSections)\n        section {\n          ...ContentSections\n          ... on LayoutSection {\n            ...PageLayoutSection\n            __typename\n          }\n          ... on ProductCarouselSection {\n            ...ProductCarousel_UniqueFields\n            productCarouselProducts: products(page: 1, perPage: 16, sort: $sort) {\n              ...Product_ProductItem\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      ... on SplitTestingSection {\n        variantId\n        testId\n        optimizelyEntityId\n        hidden(hideTargetedSections: $hideTargetedSections)\n        section {\n          ...ContentSections\n          ... on LayoutSection {\n            ...PageLayoutSection\n            __typename\n          }\n          ... on ProductCarouselSection {\n            ...ProductCarousel_UniqueFields\n            productCarouselProducts: products(page: 1, perPage: 16, sort: $sort) {\n              ...Product_ProductItem\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      ... on ProductSection {\n        ... on DisruptorProductSection {\n          ...DisruptorSection\n          __typename\n        }\n        products(perPage: $perPage, page: $page, sort: $sort, filters: $filters) @include(if: $isPaginated) {\n          ...ProductListings\n          __typename\n        }\n        products(page: $page, perPage: $perPage, sort: $sort, filters: $filters) @skip(if: $isPaginated) {\n          ...ProductListings\n          __typename\n        }\n        __typename\n      }\n      ... on ProductCarouselSection {\n        ...ProductCarousel_UniqueFields\n        productCarouselProducts: products(page: 1, perPage: 16, sort: $sort) {\n          ...Product_ProductItem\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment ContentSections on ContentSection {\n  __typename\n  id\n  ...AccordionSectionData\n  ...BreadcrumbSection\n  ...CategoryIndexSection\n  ...CategoryListingSection\n  ...ListingBannerSection\n  ...CardContentSection\n  ...CopyContent\n  ...CopySectionData\n  ...QuickLinksData\n  ...ContentBlockMixedData\n  ...HeroBannerData\n  ...MotionBannerData\n  ...MotionSidekickData\n  ...InPageNavData\n  ...GalleryData\n  ...TableData\n  ...RecommendationSectionData\n  ...SidekickBannerData\n  ...TextBlockData\n  ...CountdownBannerData\n}\n\nfragment AccordionSectionData on AccordionSection {\n  __typename\n  title\n  showTitle\n  accordionblocks {\n    title\n    text\n    __typename\n  }\n}\n\nfragment PageLayoutSection on LayoutSection {\n  __typename\n  id\n  backgroundColor\n  removePadding\n  fullWidth\n  innerSection: section {\n    ...ContentSections\n    ... on ProductCarouselSection {\n      ...ProductCarousel_UniqueFields\n      productCarouselProducts: products(page: 1, perPage: 16, sort: $sort) {\n        ...Product_ProductItem\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment CategoryIndexSection on CategoryIndexSection {\n  ...CategoryIndex\n  __typename\n}\n\nfragment CategoryIndex on CategoryIndexSection {\n  title\n  children {\n    ...CategoryBannerSection\n    __typename\n  }\n  __typename\n}\n\nfragment CategoryBannerSection on CategoryIndexChildren {\n  title\n  description\n  thumbnailImage\n  url\n  __typename\n}\n\nfragment BreadcrumbSection on BreadcrumbSection {\n  ...BreadcrumbDynamicSection\n  __typename\n}\n\nfragment BreadcrumbDynamicSection on BreadcrumbSection {\n  breadcrumbs {\n    label\n    url\n    analyticsTitle\n    __typename\n  }\n  __typename\n}\n\nfragment ListingBannerSection on ListingBannerSection {\n  ...ListingBanner\n  __typename\n}\n\nfragment ListingBanner on ListingBannerSection {\n  title\n  description\n  contrast\n  logoImage\n  images {\n    size\n    url\n    __typename\n  }\n  __typename\n}\n\nfragment CategoryListingSection on CategoryListingSection {\n  ...CategoryListing\n  __typename\n}\n\nfragment CategoryListing on CategoryListingSection {\n  title\n  description\n  thumbnailImage\n  children {\n    ...CategoryLeafSection\n    __typename\n  }\n  __typename\n}\n\nfragment CategoryLeafSection on CategoryListingChildren {\n  title\n  description\n  thumbnailImage\n  logoImage\n  url\n  ageRange\n  tag\n  __typename\n}\n\nfragment DisruptorSection on DisruptorProductSection {\n  disruptor {\n    ...DisruptorData\n    __typename\n  }\n  __typename\n}\n\nfragment DisruptorData on Disruptor {\n  __typename\n  image\n  contrast\n  background\n  title\n  description\n  link\n  openInNewTab\n}\n\nfragment ProductListings on ProductQueryResult {\n  count\n  total\n  results {\n    ...Product_ProductItem\n    __typename\n  }\n  facets {\n    ...Facet_FacetSidebar\n    __typename\n  }\n  sortOptions {\n    ...Sort_SortOptions\n    __typename\n  }\n  __typename\n}\n\nfragment Product_ProductItem on Product {\n  __typename\n  id\n  productCode\n  name\n  slug\n  primaryImageUrl(size: THUMBNAIL)\n  baseImgUrl: primaryImageUrl\n  overrideUrl\n  ... on ReadOnlyProduct {\n    readOnlyVariant {\n      ...Variant_ReadOnlyProduct\n      __typename\n    }\n    __typename\n  }\n  ... on SingleVariantProduct {\n    variant {\n      ...Variant_ListingProduct\n      __typename\n    }\n    __typename\n  }\n  ... on MultiVariantProduct {\n    variants {\n      ...Variant_ListingProduct\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment Variant_ListingProduct on ProductVariant {\n  id\n  sku\n  salePercentage\n  attributes {\n    rating\n    maxOrderQuantity\n    availabilityStatus\n    availabilityText\n    vipAvailabilityStatus\n    vipAvailabilityText\n    canAddToBag\n    vipCanAddToBag\n    onSale\n    isNew\n    ...ProductAttributes_Flags\n    __typename\n  }\n  ...ProductVariant_Pricing\n  __typename\n}\n\nfragment ProductVariant_Pricing on ProductVariant {\n  price {\n    formattedAmount\n    centAmount\n    currencyCode\n    formattedValue\n    __typename\n  }\n  listPrice {\n    formattedAmount\n    centAmount\n    __typename\n  }\n  attributes {\n    onSale\n    __typename\n  }\n  __typename\n}\n\nfragment ProductAttributes_Flags on ProductAttributes {\n  featuredFlags {\n    key\n    label\n    __typename\n  }\n  __typename\n}\n\nfragment Variant_ReadOnlyProduct on ReadOnlyVariant {\n  id\n  sku\n  attributes {\n    featuredFlags {\n      key\n      label\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment Facet_FacetSidebar on Facet {\n  name\n  key\n  id\n  labels {\n    __typename\n    displayMode\n    name\n    labelKey\n    count\n    ... on FacetValue {\n      value\n      __typename\n    }\n    ... on FacetRange {\n      from\n      to\n      __typename\n    }\n  }\n  __typename\n}\n\nfragment Sort_SortOptions on SortOptions {\n  id\n  key\n  direction\n  label\n  __typename\n}\n\nfragment CardContentSection on CardContentSection {\n  ...CardContent\n  __typename\n}\n\nfragment CardContent on CardContentSection {\n  moduleTitle\n  showModuleTitle\n  blocks {\n    title\n    isH1\n    description\n    textAlignment\n    logo\n    secondaryLogo\n    primaryLogoSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    secondaryLogoSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    logoPosition\n    image\n    imageSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    callToActionText\n    callToActionLink\n    altText\n    contrast\n    __typename\n  }\n  __typename\n}\n\nfragment CopyContent on CopyContentSection {\n  blocks {\n    title\n    body\n    textAlignment\n    titleColor\n    imageSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment CopySectionData on CopySection {\n  title\n  showTitle\n  body\n  __typename\n}\n\nfragment QuickLinksData on QuickLinkSection {\n  title\n  quickLinks {\n    title\n    isH1\n    image\n    link\n    openInNewTab\n    contrast\n    imageSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment ContentBlockMixedData on ContentBlockMixed {\n  moduleTitle\n  showModuleTitle\n  blocks {\n    title\n    isH1\n    description\n    backgroundColor\n    blockTheme\n    contentPosition\n    mobileImageURL\n    desktopImageURL\n    logoURL\n    logoPosition\n    callToActionText\n    callToActionLink\n    altText\n    __typename\n  }\n  __typename\n}\n\nfragment HeroBannerData on HeroBanner {\n  heroblocks {\n    id\n    title\n    isH1\n    tagline\n    bannerTheme\n    contentVerticalPosition\n    contentHorizontalPosition\n    contentHeight\n    primaryLogoSrc\n    secondaryLogoSrc\n    animatedMedia\n    videoMedia {\n      url\n      id\n      __typename\n    }\n    logoPosition\n    contentBackground\n    callToActionText\n    callToActionLink\n    secondaryCallToActionText\n    secondaryCallToActionLink\n    secondaryOpenInNewTab\n    backgroundImages {\n      small\n      medium\n      large\n      __typename\n    }\n    altText\n    __typename\n  }\n  __typename\n}\n\nfragment MotionBannerData on MotionBanner {\n  motionBannerBlocks {\n    id\n    title\n    isH1\n    tagline\n    bannerTheme\n    contentHorizontalPosition\n    primaryLogoSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    secondaryLogoSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    animatedMedia\n    videoMedia {\n      url\n      id\n      __typename\n    }\n    logoPosition\n    contentBackground\n    callToActionText\n    callToActionLink\n    backgroundImages {\n      small {\n        url\n        width\n        height\n        maxPixelDensity\n        format\n        __typename\n      }\n      medium {\n        url\n        width\n        height\n        maxPixelDensity\n        format\n        __typename\n      }\n      large {\n        url\n        width\n        height\n        maxPixelDensity\n        format\n        __typename\n      }\n      __typename\n    }\n    altText\n    __typename\n  }\n  __typename\n}\n\nfragment MotionSidekickData on MotionSidekick {\n  motionSidekickBlocks {\n    id\n    title\n    isH1\n    tagline\n    bannerTheme\n    contentHorizontalPosition\n    primaryLogoSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    secondaryLogoSrc {\n      url\n      width\n      height\n      maxPixelDensity\n      format\n      __typename\n    }\n    animatedMedia\n    videoMedia {\n      url\n      id\n      __typename\n    }\n    logoPosition\n    contentBackground\n    callToActionText\n    callToActionLink\n    backgroundImages {\n      small {\n        url\n        width\n        height\n        maxPixelDensity\n        format\n        __typename\n      }\n      medium {\n        url\n        width\n        height\n        maxPixelDensity\n        format\n        __typename\n      }\n      large {\n        url\n        width\n        height\n        maxPixelDensity\n        format\n        __typename\n      }\n      __typename\n    }\n    altText\n    __typename\n  }\n  __typename\n}\n\nfragment InPageNavData on InPageNav {\n  inPageNavBlocks {\n    id\n    title\n    isH1\n    text\n    contrast\n    textPosition\n    textAlignment\n    primaryLogoSrc\n    secondaryLogoSrc\n    animatedMedia\n    videoMedia {\n      url\n      id\n      __typename\n    }\n    logoPosition\n    contentBackground\n    backgroundImages {\n      small\n      medium\n      large\n      __typename\n    }\n    callToActionText\n    callToActionLink\n    openInNewTab\n    secondaryCallToActionText\n    secondaryCallToActionLink\n    secondaryOpenInNewTab\n    __typename\n  }\n  __typename\n}\n\nfragment GalleryData on Gallery {\n  galleryblocks {\n    id\n    primaryLogoSrc\n    animatedMedia\n    videoMedia {\n      url\n      id\n      __typename\n    }\n    backgroundImages {\n      small\n      medium\n      large\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment TableData on TableSection {\n  rows {\n    isHeadingRow\n    cells\n    __typename\n  }\n  __typename\n}\n\nfragment RecommendationSectionData on RecommendationSection {\n  __typename\n  title\n  showTitle\n  recommendationType\n}\n\nfragment SidekickBannerData on SidekickBanner {\n  __typename\n  id\n  sidekickBlocks {\n    title\n    isH1\n    text\n    textAlignment\n    theme\n    fallbackImage: image\n    backgroundColor\n    logo\n    logoPosition\n    ctaTextPrimary: ctaText\n    ctaLinkPrimary: ctaLink\n    ctaTextSecondary\n    ctaLinkSecondary\n    contentHeight\n    bgImages {\n      small\n      medium\n      large\n      __typename\n    }\n    videoMedia {\n      url\n      id\n      __typename\n    }\n    altText\n    __typename\n  }\n}\n\nfragment ProductCarousel_UniqueFields on ProductCarouselSection {\n  __typename\n  productCarouselTitle: title\n  showTitle\n  showAddToBag\n  seeAllLink\n}\n\nfragment TextBlockData on TextBlock {\n  textBlocks {\n    title\n    isH1\n    text\n    textAlignment\n    contrast\n    backgroundColor\n    callToActionLink\n    callToActionText\n    openInNewTab\n    secondaryCallToActionLink\n    secondaryCallToActionText\n    secondaryOpenInNewTab\n    __typename\n  }\n  __typename\n}\n\nfragment CountdownBannerData on CountdownBanner {\n  countdownBannerBlocks {\n    title\n    isH1\n    text\n    textPosition\n    textAlignment\n    contrast\n    backgroundColor\n    callToActionLink\n    callToActionText\n    openInNewTab\n    countdownDate\n    __typename\n  }\n  __typename\n}\n\nfragment Breadcrumbs on Content {\n  breadcrumbs {\n    __typename\n    label\n    url\n    analyticsTitle\n  }\n  __typename\n}\n"
        }
        request = scrapy.Request(url=self.listing_api_url, method='POST', body=json.dumps(data),
            headers={'Content-Type':'application/json', 'x-locale':'en-US'}, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request


    def crawlDetail(self, response):
        # NOTE: description can be fetched alternatively from the script, but json is too huge. So ignored this approach
        # script = response.xpath('//script[contains(text(), "window.__PUBLIC_PATH__=")]/text()').extract_first()
        # data = re.sub('window.__PUBLIC_PATH__="/_build/";window.__INITIAL_PROPS__=', '', script)
        # json_data = json.loads(data) 
        # description = json_data['apolloState']['data']['SingleVariantProduct:ff2cc7f2-7238-43be-afaa-215f903d9b00']['description']

        description = response.xpath('//div[@data-test="product-details-description"]/preceding-sibling::span//text()').extract_first(default='')
        detail_description = '\n'.join(response.xpath('//div[@data-test="product-details-description"]//text()[normalize-space(.)]').extract())

        if not description and not detail_description:
            description = '\n'.join(response.xpath('//div[@data-test="product-accordion-specifications-content"]//text()[normalize-space(.)]').extract())

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,
            # 'id': response.xpath('//meta[@property="og:url"]/@content').extract_first(default=''),

            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(default=''),
            'rating': response.xpath('//span[@itemprop="aggregateRating"]/span[@itemprop="ratingValue"]/text()').extract_first(default=''),
            # 'mpn': response.xpath('//span[@itemprop="mpn"]/text()').extract_first(default=''),
            # 'product_id': response.xpath('//span[@itemprop="productID"]/text()').extract_first(default=''),

            'title': response.xpath('//h1[@itemprop="name"]//text()').extract_first(default=''),
            'price': response.xpath('//span[contains(@class, "ProductPrice")]/text()').extract_first(default=''),
            'ages': response.xpath('//span[@data-test="product-details__ages"]//span/text()').extract_first(default=''),
            'pieces': response.xpath('//span[@data-test="product-details__piece-count"]//text()').extract_first(default=''),
            'model_sku': response.xpath('//span[@data-test="product-details__product-code"]//text()').extract_first(default=''),
            'breadcrumb': ' | '.join(response.xpath('//a[@data-test="breadcrumb-link"]//text()').extract()),
            'product_line': response.xpath('//a[@class="ProductOverviewstyles__LinkImg-sc-1a1az6h-1 bqskty"]/img/@alt').extract_first(default=''),
            'description': description + detail_description,
        }


    def parse(self, response):
        result_count = response.xpath('//span[contains(text(), "Showing")]/text()').extract_first(default='')
        match = re.findall(re.compile(r'Showing 1 - 18 of (\d+?) results', re.IGNORECASE), result_count)
        if match:
            total = match[0]
            if total:
                yield self.create_post_request(url=response.url, callback=self.parse_listing_page, total=int(total))


    def parse_listing_page(self, response):
        res = json.loads(response.text)
        sections = res.get('data', {}).get('contentPage', {}).get('sections', [])
        for section in sections:
            if section.get('__typename', '') == 'DisruptorProductSection':
                total = section.get('products', {}).get('total', 0)
                print('total items:{}'.format(total))
                products = section.get('products', {}).get('results', 0)
                for product in products:
                    detail_base_url = 'https://www.lego.com/en-us/product/'
                    id = product.get('slug', '')
                    if id:
                        request = self.create_request(url=detail_base_url + id, callback=self.crawlDetail)
                        yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
