import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')

            pattern=re.compile(r"UPPA|baby|travel|systems",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            model=' '.join(model.split())
            pattern=re.compile(r"Age\s*\/\s*Weight\s*Capacity\s*[\w\d\W]*\s*open",re.IGNORECASE)
            weight_capacity=''
            length=''
            width=''
            height=''
            weight=''
            if re.findall(pattern,description)!=[]:
                weight_capacity=re.findall(pattern,description)[0]
                weight_capacity=re.sub(r"Age\s*\/\s*Weight\s*Capacity|open|\:",'',weight_capacity).strip()
            
            pattern=re.compile(r"open\s*\(\s*in\s*\)\s*\:\s*[\w\d\s\.]*\s*H",re.IGNORECASE)
            if re.findall(pattern,description)!=[]:
                measurements=re.findall(pattern,description)[0]
                if re.findall(r"\d+\.*\d*\s*L",measurements)!=[]:
                    length=re.findall(r"\d+\.*\d*\s*L",measurements)[0].strip(' L')
                    
                if re.findall(r"\d+\.*\d*\s*W",measurements)!=[]:
                    width=re.findall(r"\d+\.*\d*\s*W",measurements)[0].strip(' W')
                    
                if re.findall(r"\d+\.*\d*\s*H",measurements)!=[]:
                    height=re.findall(r"\d+\.*\d*\s*H",measurements)[0].strip(' H')
            
            pattern=re.compile(r"Stroller\s*weight\s*\:\s*[\w\W\d\s]*\s*Age",re.IGNORECASE) 
            if re.findall(pattern,description)!=[]:
                weight=re.findall(pattern,description)[0]
                weight=re.sub(r"Stroller|weight|\:|Age",'',weight).strip()
            
            model=re.sub(re.compile(r"MESA",re.IGNORECASE),"Mesa",model)
            model=re.sub(re.compile(r"VISTA",re.IGNORECASE),"Vista",model)
            model=re.sub(re.compile(r"\(.*\)",re.IGNORECASE),"",model)
            model=' '.join(model.split())
        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date',''),
                "status_code": record.get('status_code',''),
                "category": "Travel systems",
                "breadcrumb":record.get('breadcrumb',''),
                "description": description.replace('\n',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query.replace('\n',''),
                "price":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "sku" : record.get('sku',''),
                "weight_capacity":weight_capacity,
                "length":length,
                "height":height,
                "width":width,
                "weight":weight
                }

            return transformed_record
        else:
            return None
