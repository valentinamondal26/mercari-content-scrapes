'''
https://mercari.atlassian.net/browse/USCC-228 - UPPAbaby Travel systems
'''

import scrapy
import datetime
import random
import os
from scrapy.exceptions import CloseSpider

class AlbeebabySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from albeebaby.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Travel systems

    brand : str
        category to be crawled, Supports
        1) UPPAbaby

    Command e.g:
    scrapy crawl albeebaby -a category="Travel systems" -a brand='UPPAbaby'

    """

    name = 'albeebaby'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None
    number_of_products = None
    source = 'albeebaby.com'
    blob_name = 'albeebaby.txt'
    base_url = 'https://www.albeebaby.com/'

    url_dict = {
        "Travel systems": {
            'UPPAbaby': {
                'url': 'https://www.albeebaby.com/uppababy-travel-systems.html',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if  os.environ.get('SCRAPY_CHECK', ''):
            return
        super(AlbeebabySpider, self).__init__(*args, **kwargs)
        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category}, url: {url}".format(category = category,url = self.launch_url))


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse)


    def parse(self, response):
            """
            @url https://www.albeebaby.com/uppababy-travel-systems.html
            @returns requests 1
            """

            all_links=response.xpath('//div[@class="pdpaging-name"]/a/@href').getall()
            all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
            for link in all_links:
                yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)


    def parse_product(self,response):
        """
        @url https://www.albeebaby.com/uppababy-vista-mesa-travel-system-jordan-charcoal-melange.html
        @scrape_values image breadcrumb image title price description sku
        """

        item={}
        item["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"]=str(response.status)
        item['id']=response.url
        item['image']=response.xpath('//div[@class="item active"]/img/@src').get(default='')
        item['brand']=self.brand
        item['category']=self.category
        item['breadcrumb']='|'.join(response.xpath('//div[@class="pdbreadcrumbs"]/a/text()').getall())
        item['image']=response.xpath('//div[@class="pdGalleryMain"]//img/@src').get(default='')
        item['title']=response.xpath('normalize-space(//div[@class="pditem-right-inner"]/h1/text())').get(default='')
        item['price']=response.xpath('normalize-space(//span[@class="pditem-was"]/text())').get(default='').strip('Was ')
        description=' '.join(response.xpath('//table[@class="tableizer-table"]//text()').getall()).replace("\n","").replace("\xa0","").replace("_","")
        item['description']=' '.join(description.split())
        item['sku']=response.xpath('//span[@class="pdGalleryItemCode"]/text()').get().replace('ITEM# ','')

        yield item


    def createRequest(self, url, callback,meta=None,headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key,value in headers.items():
                request.headers[key]=value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value
        return request


