import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            type_of_rise = ['High-Waisted','High Waist','Mid-Waisted','Low-Waisted','Low-Rise','High-Rise','Mid-Rise','Versatile']
            model = record.get('title', '')
            description = record.get('description','')
            title = record.get('title','')
            rise = ', '.join(record.get('Rise', ''))
            
            if not rise:
                rise = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',type_of_rise))),title,flags=re.IGNORECASE)
                if not rise:
                    rise = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',type_of_rise))),description,flags=re.IGNORECASE)
                
                if rise:
                    rise = rise[0].replace('-',' ').title()
                    rise = re.sub('Waisted','Rise',rise, flags=re.IGNORECASE)
                else:
                    rise = ''

            transform_strings = ['Legging','Leggings','Strappy','Capri','Joggers', 'Jogger', 'Short', 'Shorts' ,'Pant', 'Sweatpants', 'Crop','Pants','Pocket Short']
            trans_string = re.findall(r'|'.join(list(map(lambda x: r'\b' + x + r'\b', transform_strings))), model, flags=re.IGNORECASE)            
            if trans_string:
                trans_string = trans_string[0]
            else:
                try:
                    trans_string = re.findall(r'\bLeggings*\b', description, flags=re.IGNORECASE)[0]
                except IndexError:
                    trans_string = ''
                    
            if trans_string:
                model = re.sub(trans_string,'',model,flags=re.IGNORECASE)
                update_strings = ['Legging', 'Short', 'Pant']
                trans_string = re.sub(r'|'.join(list(map(lambda x: r'\b' + x + r'\b', update_strings))), lambda x: f'{x.group()}s',\
                        trans_string, flags=re.IGNORECASE)
                model = f'{model} {trans_string.title()}'

            model = re.sub(r'®|™|\bShine-Panel\b|\bShine\b|\bTie-Waist\b','',model,flags=re.IGNORECASE)
            model = re.sub(r'mini', 'Mini', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            fabric_care = '\n'.join(record.get('fabrics_care', []))
            match = re.findall(r'\d+%\s*(.*?)[/\n\s]', fabric_care)
            if match:
                material = ', '.join(sorted(list(dict.fromkeys(list(map(lambda x: re.sub('Imported', '', x, flags=re.IGNORECASE).strip(), match))))))

            color = ', '.join(record.get('selected_color', ''))
            color = re.sub('Multi', 'Multicolor', color, flags=re.IGNORECASE)
            remove_strings = ['Logo', 'Stripe', 'Sports','Sport','Inspirational','Kick Butt']
            color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',remove_strings))), '', color,flags=re.IGNORECASE)
            colors = color.split('/')     
            if len(colors) >= 3:
                color = f'{colors[0]}/Multicolor'           

            if len(colors) == 2:
                if re.findall(r'{}'.format(colors[1]), colors[0], flags=re.IGNORECASE):
                    color = colors[0]
                elif re.findall(r'{}'.format(colors[0]), colors[1], flags=re.IGNORECASE):
                    color = colors[1]                            
            color = re.sub(r'\s+',' ',color)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                'activity': ', '.join(record.get('Activity', '')),
                'rise': rise,
                'compression': ', '.join(record.get('Compression', '')),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
