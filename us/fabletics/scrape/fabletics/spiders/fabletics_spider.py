'''
https://mercari.atlassian.net/browse/USDE-1678 - Fabletics Pants, tights, leggings
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json
import re
import urllib.parse


class FableticsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from fabletics.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Pants, tights, leggings

    brand : str
        brand to be crawled, Supports
        1) Fabletics

    Command e.g:
    scrapy crawl fabletics -a category='Pants, tights, leggings' -a brand='Fabletics'
    """

    name = 'fabletics'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'fabletics.com'
    blob_name = 'fabletics.txt'

    url_dict = {
        'Pants, tights, leggings': {
            'Fabletics': {
                'url': [
                    'https://www.fabletics.ca/womens/bottoms/pants-and-joggers',
                    'https://www.fabletics.ca/womens/bottoms/leggings',

                ],
                'filters': ['Color', 'Activity', 'Rise', 'Compression']
            }
        }
    }

    listing_page_api = 'https://www.fabletics.ca/?action=products.grid&page_virtual_url={page_virtual_url}&token={token}&psrc={psrc}&state={state}'

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK', ''):
            self.contracts_mock_function()
            return
        super(FableticsSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {category} and brand: {self.brand}, url: {self.launch_url}")

    def contracts_mock_function(self):
        self.filters = ['Color', 'Activity', 'Rise', 'Compression']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, list):
                urls = self.launch_url
            elif isinstance(self.launch_url, str):
                urls = [self.launch_url]

            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.fabletics.ca/products/DARIA-FLEECE-JOGGER-PT2040618-0001
        @scrape_values id title title_description breadcrumb image price description features fabric_care selected_color
        @meta {"use_proxy": "True"}
        """
        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath(u'normalize-space(//h1[@itemprop="name"]/text())').extract_first(),
            'title_description': response.xpath(u'normalize-space(//p[@class="product-name-medium-description"]/text())').extract_first(),
            'breadcrumb': response.xpath('//ol[@class="breadcrumbs"]/li[@class="breadcrumb"]/a/span').xpath('normalize-space(./text())').extract(),
            'image': 'https:' + response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'price': response.xpath(u'normalize-space(//span[@class="price" and @data-price-type="retail"]/span[@class="price-value"]/text())').extract_first(),
            'description': response.xpath(u'normalize-space(//span[@class="product-why-we-made-this-copy"]/text())').extract_first(),
            'features': response.xpath('//div[@class="product-details-features"]/ul/li/text()').extract(),
            'fabrics_care': response.xpath('//div[@class="product-details-fabrics"]/div').xpath('normalize-space(./text())').extract(),
            'selected_color': re.findall(r'GTM_dataLayerProductsArray\[0\]\["variant"\] = "(.*)";', response.xpath('//script[@type="text/javascript" and contains(text(), "GTM_dataLayerProductsArray[0]")]/text()').extract_first())
        }


    def parse_listing_page(self, response):
        """
        @url https://www.fabletics.ca/?action=products.grid&page_virtual_url=%2Fcatalog%2Fshop_bottoms%2Fpants&token=PURE-GRID%2FDD979FAD6E9737E80DB3C33607E14674&psrc=womens_bottoms_pants-and-joggers&state=%257B%2522gender%2522%253A%2520%2522F%2522%252C%2520%2522size%2522%253A%2520%25229999%2522%252C%2520%2522colors%2522%253A%2520%255B%2522Black%2522%255D%257D
        @returns requests 1
        @meta {"use_proxy": "True"}
        """
        extra_item_info = response.meta.get('extra_item_info', {})

        res = json.loads(response.text)

        for product in res.get('elements', {}).get('listing', {}).get('items', []):
            urls = []
            if extra_item_info.get('Color', ''):
                urls.append(product.get('url', ''))
            else:
                for color in product.get('colors', []):
                    url = color.get('href', '')
                    if url:
                        urls.append(url)
            for id in urls:
                id = id.split('?')[0]
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.update_extra_item_infos(id, extra_item_info)
                yield self.create_request(url=id, callback=self.crawlDetail)


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def parse(self, response):
        """
        @url https://www.fabletics.ca/womens/bottoms/pants-and-joggers
        @meta {"use_proxy": "True"}
        @returns requests 1
        """
        page_virtual_url = urllib.parse.quote(json.loads(re.findall('window.Fabletics =(.*);\n', response.xpath('//script[@type="text/javascript" and contains(text(), "window.Fabletics =")]/text()').extract_first())[0])['page_virtual_url'], safe='')
        token = urllib.parse.quote(response.xpath('//div[@class="body"]/div[@class="pg-frame grid-frame pg-frame--desktop"]/@data-pg').extract_first().split(':', 1)[0], safe='')
        psrc = urllib.parse.urlparse(response.url).path.replace('/', '_').strip('_')
        q = {"gender":"F","size":"9999"}
        state = urllib.parse.quote(urllib.parse.quote(json.dumps(q), safe=''), safe='')

        url = self.listing_page_api.format(page_virtual_url=page_virtual_url, token=token, psrc=psrc, state=state)
        yield self.create_request(url=url, callback=self.parse_listing_page)

        for filter_list in response.xpath('//div[@class="pg-filters"]/div[@class="product-filters"]/div'):
            filter_name = filter_list.xpath(u'normalize-space(.//div[@class="pg-sidebar-heading-label"]/text())').extract_first()
            if filter_name in self.filters:
                for filter_item in filter_list.xpath('.//div[@class="pg-sidebar-list"]/div[@class="pg-sidebar-list-item filter-item"]'):
                    tag = filter_item.xpath('./input/@name').extract_first()
                    value = ''.join(filter_item.xpath('./label/text()').extract()).strip()
                    if not tag or not value:
                        continue
                    a = list(filter(None, re.split(r'[\[\]]', tag)))
                    q = {"gender":"F","size":"9999"}
                    if len(a) == 1:
                        q.update({a[0]: [value]})
                    elif len(a) == 2:
                        q.update({a[0]: {a[1]: [value]}})
                    state = urllib.parse.quote(urllib.parse.quote(json.dumps(q), safe=''), safe='')
                    url = self.listing_page_api.format(page_virtual_url=page_virtual_url, token=token, psrc=psrc, state=state)
                    request = self.create_request(url=url, callback=self.parse_listing_page)
                    request.meta['extra_item_info'] = {
                        filter_name: value
                    }
                    yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
