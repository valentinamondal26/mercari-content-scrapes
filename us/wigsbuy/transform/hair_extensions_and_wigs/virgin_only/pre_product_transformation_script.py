import re
import hashlib
import collections


class Mapper(object):

    def map(self, record):
        if record:

            # description transformation
            description = record.get('description', '')
            description = re.sub(re.compile(
                r"specifications|product name", re.IGNORECASE), "", description)
            description = description.encode("ascii", "ignore").decode()
            description = ", ".join([des for ctr, des in enumerate(description.split(",")) if re.findall(
                re.compile(r"^[^\w|\d]$"), des) == [] and des != ""])
            ner_query = description
            
            id = record.get("id", "")
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            # color transformation
            color = record.get("color", "")
            color = re.sub(re.compile(
                r"\bDrak\b", re.IGNORECASE), "Dark", color)
            color = re.sub(re.compile(r'Same As First Picture',
                                      re.IGNORECASE), '  ', color)
            if '#' in color:
                color = ''
            color = re.sub(re.compile(
                r"\bBrwon\b", re.IGNORECASE), "Brown", color)
            color = re.sub(re.compile(r"\bBlondeEvenly\b",
                                      re.IGNORECASE), "Blonde Evenly", color)
            color =  re.sub(re.compile(r"\bhair\b", re.IGNORECASE), "", color)
            color = re.sub('Evenly Blended With', '/', color, flags=re.IGNORECASE)
            for word, count in collections.Counter(color.split()).items():
                if count > 1:
                    color = color.replace(word, '', count-1)
            color = re.sub(r'\s*/\s*', '/', color)
            color = re.sub(r'\s+', ' ', color).title().strip()

            # model transforamtion
            model = record.get('title', '')
            pattern = re.compile(
                r"Hair\s*Extensions\s*\&*\s*Wigs|Virgin\s*Only|Virgin|Wigsbuy|with closure|\d*\s*bundles*|\d*\.*\-*\_*\d*\s*inche*s*|length|\d+\.*\d+\s*x\s*\d+\.*\d*", re.IGNORECASE)
            model = re.sub(pattern, '', model)
            for word in record.get('material', '').split():
                pattern = re.compile(r"{}".format(word), re.IGNORECASE)
                model = re.sub(pattern, '', model)

            for word in record.get('texture', '').split():
                pattern = re.compile(r"{}".format(word), re.IGNORECASE)
                model = re.sub(pattern, '', model)
            model = " ".join(model.split())

            # length transformation
            length = record.get("length", "")
            length_pattern = re.compile(
                r"\b\d+\.*\d+\s*inches*\b", re.IGNORECASE)
            # if length is empty extract from description
            if length == "":
                if re.findall(length_pattern, description) != []:
                    length = re.findall(length_pattern, description)
                    length = ", ".join([l.strip() for l in length])

            # Exclude items having length value more than X inch [ 8 inch *8inch ...]
            if len(re.findall(re.compile(r"\b\d+\.*\d*\s*inches*\b", re.IGNORECASE), length)) > 1:
                return None
            # convert common unit format for length
            length = re.sub(re.compile(
                r"\binches\b|\binch\b", re.IGNORECASE), "Inches", length)
            # fix spacing issue in length - 8Inch
            length = re.sub(re.compile(r"(\d+\.*\d*)(Inch)",
                                       re.IGNORECASE), r"\1 Inches", length)
            if re.findall(re.compile(r"Lot|bundle|Accessories", re.IGNORECASE), record.get("title", "")) or (record.get('texture', '') == "" and record.get('material', '') == ""):
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawled_date', ''),
                "status_code": record.get('status_code', ''),
                "category": "Hair Extensions & Wigs",
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,
                "price": {
                    "currency_code": "USD",
                    "amount": str(record.get("price", "")).strip('$').replace(',', '')
                },
                "msrp": {
                    "currency_code": "USD",
                    "amount": str(record.get("msrp", "")).strip('$').replace(',', '')
                },
                "color": color,
                "material": record.get('material', ''),
                "texture": record.get('texture', ''),
                "weight": record.get('weight', ''),
                "number_in_pack": record.get('number_in_pack', ''),
                "length": length,
                "description": description

            }

            return transformed_record
        else:
            return None
