'''
https://mercari.atlassian.net/browse/USCC-304 - Virgin Only Hair Extensions & Wigs
'''

import json
import datetime
import requests
import itertools
import os
import re

import scrapy
from scrapy.exceptions import CloseSpider


class WigsbuySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from wigsbuy.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1)  Hair Extensions & Wigs

    brand : str
        category to be crawled, Supports
        1) Virgin Only

    Command e.g:
    scrapy crawl wigsbuy -a category='Hair Extensions & Wigs' -a brand='Virgin Only'
    """

    name = 'wigsbuy'

    category = None
    brand = None

    source = 'wigsbuy.com'
    blob_name = 'wigsbuy.txt'

    round_robin = None
    base_url = "https://shop.wigsbuy.com"

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com/',
        'http://shp-mercari-us-d00002.tp-ns.com/',
        'http://shp-mercari-us-d00003.tp-ns.com/',
        'http://shp-mercari-us-d00004.tp-ns.com/',
        'http://shp-mercari-us-d00005.tp-ns.com/',
        'http://shp-mercari-us-d00006.tp-ns.com/',
        'http://shp-mercari-us-d00007.tp-ns.com/',
        'http://shp-mercari-us-d00008.tp-ns.com/',
        'http://shp-mercari-us-d00009.tp-ns.com/',
        'http://shp-mercari-us-d00010.tp-ns.com/',
        'http://shp-mercari-us-d00011.tp-ns.com/',
        'http://shp-mercari-us-d00012.tp-ns.com/',
        'http://shp-mercari-us-d00013.tp-ns.com/',
        'http://shp-mercari-us-d00014.tp-ns.com/',
        'http://shp-mercari-us-d00015.tp-ns.com/',
        'http://shp-mercari-us-d00016.tp-ns.com/',
        'http://shp-mercari-us-d00017.tp-ns.com/',
        'http://shp-mercari-us-d00018.tp-ns.com/',
        'http://shp-mercari-us-d00019.tp-ns.com/',
        'http://shp-mercari-us-d00020.tp-ns.com/',
    ]

    url_dict = {
        "Hair Extensions & Wigs": {
            'Virgin Only': {
                'url': 'https://shop.wigsbuy.com/Virgin-Only-Hair/',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(WigsbuySpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.round_robin = itertools.cycle(self.proxy_pool)
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.round_robin = itertools.cycle(self.proxy_pool)


    def start_requests(self):
        print('launch_url', self.launch_url)
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://shop.wigsbuy.com/Virgin-Only-Hair/
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        all_links = response.xpath('//dl[@class="l_g_s"]/dt/a/@href').getall()
        all_links = [self.base_url +
                     link if "https" not in link else link for link in all_links]
        for link in all_links:
            yield self.create_request(url=link, callback=self.parse_products)
        next_page_url = response.xpath(
            '//dd[@id="list_selects"][2]//a[contains(text(),"Next")]/@href').get(default='')
        if "https" not in next_page_url:
            next_page_url = self.base_url+next_page_url
        if next_page_url != '' and next_page_url != "https://shop.wigsbuy.com":
            yield self.create_request(url=next_page_url, callback=self.parse)


    def parse_products(self, response):
        """
        @url https://shop.wigsbuy.com/product/Wigsbuy-3-Bundles-Indian-Virgin-Hair-Straight-With-Closure-13302055.html
        @scrape_values breadcrumb title description image colors material texture weight number_in_pack price sku length msrp
        @meta {"use_proxy":"True"}
        """

        item = {}
        item["crawled_date"] = str(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"] = str(response.status)
        item["id"] = response.url
        breadcrumb = response.xpath(
            '//div[@id="nav_bread_crumb"]//text()').getall()
        for br in breadcrumb:
            if re.findall(r"\w+\d*", br) == []:
                del breadcrumb[breadcrumb.index(br)]

        item['breadcrumb'] = '|'.join(breadcrumb)
        item['title'] = response.xpath(
            '//div[@class="pro-title"]//h1//text()').get(default='')
        item['description'] = ','.join(response.xpath(
            '//div[@class="desdiv"]//text()').getall())
        item['image'] = response.xpath(
            '//div[@class="lagerimg"]//img/@src').get(default='')
        item['category'] = self.category
        item['brand'] = self.brand
        item['colors'] = ','.join(response.xpath(
            '//div[@class="color-card-big-outer"]/@data-describe').getall())
        if item['colors']=='':
            item['colors'] = ",".join(response.xpath('//div[@class="tileRequired-color-text"]/following-sibling::ul/li/@data-value').getall())
        item['material'] = ' '.join(re.findall(r"\w+\d*", response.xpath(
            '//dd[span[contains(text(),"Hair Material:")]]/text()').get(default='')))
        item['texture'] = ' '.join(re.findall(r"\w+\d*", response.xpath(
            '//dd[span[contains(text(),"Hair Texture:")]]/text()').get(default='')))
        item['weight'] = ' '.join(re.findall(
            r"\w+\d*", response.xpath('//dd[span[contains(text(),"Weight:")]]/text()').get(default='')))
        item['number_in_pack'] = ' '.join(re.findall(
            r"\w+\d*", response.xpath('//dd[span[contains(text(),"Pack:")]]/text()').get(default='')))
        item['price'] = response.xpath(
            '//meta[@property="og:price:amount"]/@content').get(default='')
        item['sku'] = response.xpath(
            '//input[@id="SkuId"]/@value').get(default='')
        item['length'] = ''

        inches = response.xpath(
            '//ul[@class="clearfix hasDownRequired"]/li/span/text()').getall()
        pattern = re.compile(r"\d+\s*inch", re.IGNORECASE)
        inches = [inch for inch in inches if re.findall(pattern, inch) != []]
        inches = [inch.replace("\xa0(+", '') for inch in inches]
        
        spu_id = response.xpath('//input[@name="reviewSpuId"]/@value').get()
        script_data = response.xpath(
            '//script[contains(text(),"window.SpuLeiMuLeafIds = ")]/text()').get()
        leimu_ids = script_data.split("window.SpuLeiMuLeafIds = ")[
            1].split('"')[1]
        catgory_id = script_data.split("window.SpuCategoryId = ")[
            1].split(";")[0]
        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'referer': response.url}
        data = {'spuId': spu_id, 'leiMuIds': leimu_ids,
                'categoryId': catgory_id}

        proxies = {'http': response.meta['proxy']}
        res = requests.post(
            url="https://shop.wigsbuy.com/ajax/priceHanlder.js?action=getSkuPromotionPrice", proxies=proxies, data=data)

        if res.status_code == 200:
            from json.decoder import JSONDecodeError
            try:
                price_data = json.loads(res.text)
                item['msrp'] = price_data.get("PriceData", {}).get(
                    "Price", [""])[0].get("MarketPrice", "")
            except JSONDecodeError as e:
                # self.logger.error(e)
                pass
            except ValueError as e:
                pass
        
        if inches != []:
            prices = []
            lengths = []
            skus = []
            sc = response.xpath(
                "//script[contains(text(),'{}')]/text()".format('"SKUID":')).get()
            skus = re.findall(re.compile(
                r"\"\s*SKUID\s*\"\s*\:\s*\d+", re.IGNORECASE), sc)
            skus = [sk.replace('"SKUID":', '').strip() for sk in skus]
            data_values = re.findall(re.compile(
                r"\"\s*SpecificationCode\s*\"\s*\:\s*\"\s*[\d\&\=]+\s*\"", re.IGNORECASE), sc)
            data_values = [d.split("=")[-1].strip('"').strip()
                           for d in data_values]
            sku_data_dict = dict(zip(data_values, skus))
            data_value_ids = response.xpath(
                '//ul[@class="clearfix hasDownRequired"]/li/@data-valueid').getall()
            data_value_ids = [d.strip() for d in data_value_ids]
            length_data_dict = dict(zip(inches, data_value_ids))
            if res.status_code == 200:
                price_data = {}
                try:
                    price_data = json.loads(res.text)
                except ValueError as e:
                    pass
                prices = price_data.get('PriceData', {}).get('Price', [])
                msrp = [str(p.get('MarketPrice')).strip() for p in prices]
                skuids = [str(p.get('SKUID')).strip() for p in prices]
                price_sku_dict = dict(zip(skuids, msrp))
                for inch in inches:
                    items = item
                    items['sku'] = sku_data_dict.get(
                        length_data_dict.get(inch, ""), "")
                    if items["sku"] != '':
                        items['price'] = price_sku_dict.get(
                            items.get("sku", ""), "")
                        items['id']=item['id']+","+inch
                        items['length'] = inch
                        if items['colors']!='':
                            for cl in items['colors'].split(","):
                                items['color']=cl
                                item['id']=items['id']+","+cl
                                yield items
                        else:
                            yield items
            else:
                yield item
        else:
            yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request