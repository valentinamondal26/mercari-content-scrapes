class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''
        material = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            if "material" == entity['name'] and entity["attribute"]:
                material = entity["attribute"][0]["id"]
            if "color" == entity['name'] and entity["attribute"]:
                color = entity["attribute"][0]["id"]

        key_field = color + material + model
        return key_field
