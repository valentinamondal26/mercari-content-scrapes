import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            msrp = record.get('MSRP',"")
            msrp = msrp.replace("$","").strip()

            measurements_data = record.get("measurements","")
            measurements =[x for x in measurements_data if 'Approximately measures' in x][0]

            pattern=re.compile(r'[H]\s*[\d+]*\.*\d+"')
            height = re.findall(pattern,measurements)[0].replace("H","").strip()

            pattern=re.compile(r'[W]\s*[\d+]*\.*\d+"')
            width = re.findall(pattern,measurements)[0].replace("W","").strip()

            pattern=re.compile(r'[L]\s*[\d+]*\.*\d+"')
            length = re.findall(pattern,measurements)[0].replace("L","").strip()

            description = record.get('description', '')
            description = ''.join(description)

            color = record.get('item_color', '')
            color = re.sub(re.compile(r'Black Black', re.IGNORECASE), 'Black', color)
            color = color.title()
            color = re.sub(r'\s+', ' ', color).strip()

            material = record.get('Material', '')
            if not material:
                material_meta = ['Exotic Leather', 'Pebbled Leather', 'Smooth Leather', 'Nylon', 'Textured Leather', 'Leather']
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b{}\b'.format(x), material_meta))), re.IGNORECASE), description)
                if match:
                    material = ', '.join(match)
                    match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
                    if match and len(match) > 1:
                        material = ','.join(list(filter(lambda x: x.lower() != 'leather', material.split(', '))))
                        if not material:
                            material = 'Leather'

            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()

            model = record.get('model_name', '')
            model = re.sub('|'.join(re.escape(r) for r in re.findall(r"\w+", material)), '', model)
            model = re.sub('|'.join(re.escape(r) for r in color.split(', ')), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            try:
                weight = re.findall(re.compile(r"weight*s*\s*\d+\.*\d*\s*lbs*\s*\d*\.*\d*\s*o*z*",re.IGNORECASE),measurements)[0].rstrip('.')
            except:
                weight = ''
            weight = weight.replace('weighs', '').replace('weights', '').strip()

            system_category = record.get('category', '')

            if system_category in ['Tote Bags','Crossbody Bags'] \
                or (re.findall(re.compile(r"\bcross\s*\-*body\b|\btote\b|\bshoulder\s*bags*\b", re.IGNORECASE), record.get('model_name', '')) \
                    and not re.findall(re.compile(r"\bsatchel\b", re.IGNORECASE), record.get("model_name",""))
                ):
                return None

            model =  re.sub(re.compile(r"Crossbody Satchel", re.IGNORECASE), "Satchel", model)
            if not re.findall(re.compile(r"\bSatchel\b", re.IGNORECASE), model):
                model+= " Satchel"
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', '').split(', ')[0],
                "status_code": record.get('status_code', ''),

                "category": system_category,
                "brand": record.get('brand', ''),

                "title": record.get('model_name', ''),
                "image": record.get('image', ''), 

                "model": model,
                "color": color,
                "color_family": record.get('Color', ''),
                "material": material,
                "collection": record.get('Collection', ''),
                "height": height,
                'width': width,
                "length": length,
                "weight": weight,
                "model_sku": record.get('style_id', ''),
                "description": description,
                "price": {
                    "currencyCode": 'USD',
                    "amount": price
                },
                "msrp": {
                    "currencyCode": 'USD',
                    "amount": msrp
                }
            }

            return transformed_record
        else:
            return None
