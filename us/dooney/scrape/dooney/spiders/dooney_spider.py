'''
https://mercari.atlassian.net/browse/USDE-1050 - Dooney & Bourke Shoulder Bags
https://mercari.atlassian.net/browse/USDE-1051 - Dooney & Bourke Crossbody Bags
https://mercari.atlassian.net/browse/USDE-1052 - Dooney & Bourke Tote Bags
https://mercari.atlassian.net/browse/USDE-1053 - Dooney & Bourke Satchel
'''

import random
import os
import scrapy
import json
from datetime import datetime
import random
import json
from scrapy.exceptions import CloseSpider
import re


class DooneySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from dooney.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags
        2) Crossbody Bags
        3) Tote Bags
        4) Satchel

    brand : str
        brand to be crawled, Supports
        1) Dooney & Bourke

    Command e.g:
    scrapy crawl dooney_spider -a category='Shoulder Bags' -a brand='Dooney & Bourke'
    scrapy crawl dooney_spider -a category='Crossbody Bags' -a brand='Dooney & Bourke'
    scrapy crawl dooney_spider -a category='Tote Bags' -a brand='Dooney & Bourke'
    scrapy crawl dooney_spider -a category='Satchel' -a brand='Dooney & Bourke'
    """

    name = "dooney_spider"
    source = 'dooney.com'
    blob_name = 'dooney.txt'

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None

    base_url = 'https://www.dooney.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict={
        "Shoulder Bags": {
            "Dooney & Bourke": {
                "url": "https://www.dooney.com/styles/shoulder-bags/",
                "filters": ["Color", "Material", "Collection"]
            }
        },
        "Crossbody Bags": {
            "Dooney & Bourke": {
                "url": "https://www.dooney.com/styles/crossbodies/",
                "filters": ["Color", "Material", "Collection"]
            }
        },
        "Tote Bags": {
            "Dooney & Bourke": {
                "url": "https://www.dooney.com/styles/totes/",
                "filters": ["Color", "Material", "Collection"]
            }
        },
        "Satchel": {
            "Dooney & Bourke": {
                "url": "https://www.dooney.com/styles/satchels/",
                "filters": ["Color", "Material", "Collection"]
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        print(category)
        print(brand)
        super(DooneySpider, self).__init__(*args, **kwargs)
        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ["Color", "Material", "Collection"]
        self.category = 'Satchel'


    def start_requests(self):
        meta={}
        if self.launch_url:
            if self.filters:
                meta={"filter_flag":True}

            headers = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'}

            yield self.createRequest(url=self.launch_url,meta=meta,headers=headers,callback=self.parse_page)


    def parse_page(self,response):
        """
        @url https://www.dooney.com/styles/satchels/
        @returns requests 2
        @meta {"use_proxy":"True"}
        """

        headers = {'user-agent': ' Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'}
        for filter in self.filters:
            class_name  = "product-filters-options "+filter
            filter_urls = response.xpath("//div[@class='"+ class_name+"']/div/div/div/ul/li/a/@href").getall()
            filter_name = response.xpath("//div[@class='"+ class_name+"']/div/div/div/ul/li/a/@title").getall()
            print("filter: "+ class_name)
            # print(filter_urls)

            meta = {"start":0}
            for filter_link,filter_name in zip(filter_urls,filter_name):
                print(filter_link)
                filter_name = filter_name.replace("Refine by Collection: ","").replace("Refine by Color: ","").replace("Refine by Material: ","").strip()
                print(filter_name)
                filter_url = filter_link+"&start="+str(meta["start"])+"&sz=12&format=page-element&contentTilesSlotCounter=2"
                meta.update({"filter_url":filter_link})
                meta.update({"filter":filter})
                meta.update({meta["filter"]:filter_name})
                yield self.createRequest(filter_url,headers=headers,callback=self.filter_page,meta=meta)

        item_page_api = "https://www.dooney.com/styles/"+self.category.lower().replace("_","-")+"/?start=0&sz=12&format=page-element&contentTilesSlotCounter=2"
        yield self.createRequest(item_page_api,headers=headers,callback=self.non_filter_page,meta=meta)


    def filter_page(self,response):
        """
        @url https://www.dooney.com/styles/satchels/?prefn1=collection&prefv1=Patent
        @returns requests 1
        @meta {"use_proxy":"True","start":0,"filter_url":"https://www.dooney.com/styles/satchels/?prefn1=collection&prefv1=Patent"} 
        """

        headers = {'user-agent': ' Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'}
        meta = response.meta

        items_urls  =  response.xpath("//div[@class='product-detial-wrapper']/h5[@class='product-name-heading desktop hidden-xs']/a/@href").getall()
        if items_urls!=[]:
            for item_url in items_urls:
                yield self.createRequest(item_url,headers=headers,callback=self.parse_items,meta=meta)

            meta["start"] = meta["start"]+12
            filter_link = meta["filter_url"]+"&start="+str(meta["start"])+"&sz=12&format=page-element&contentTilesSlotCounter=2"
            yield self.createRequest(filter_link,headers=headers,meta=meta,callback=self.filter_page)


    def non_filter_page(self,response):##without filter
        """
        @url https://www.dooney.com/styles/satchel/?start=0&sz=12&format=page-element&contentTilesSlotCounter=2
        @meta {"use_proxy":"True","start":0}
        @returns requests 1
        """

        meta = response.meta
        headers = {'user-agent': ' Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'}
        items_urls  =  response.xpath("//div[@class='product-detial-wrapper']/h5[@class='product-name-heading desktop hidden-xs']/a/@href").getall()
        if items_urls!=[]:
            for item_url in items_urls:
                yield self.createRequest(item_url,headers=headers,callback=self.parse_items,meta=meta)

            meta["start"] = meta["start"]+12
            listing_page_api = "https://www.dooney.com/styles/"+self.category.lower().replace("_","-")+"/?start="+str(meta["start"])+"&sz=12&format=page-element&contentTilesSlotCounter=2"
            yield self.createRequest(listing_page_api,headers=headers,meta=meta,callback=self.non_filter_page)


    def parse_items(self,response):
        """
        @url https://www.dooney.com/city-flynn-LJ044.html?dwvar_LJ044_color=3CDEPANA&cgid=dooney-bags-style-tote#pg=1&start=12&sz=24&cgid=dooney-bags-style-tote
        @scrape_values model_name measurements image style_id MSRP price description item_color
        @meta {"use_proxy":"True"}
        """
        print(response.url)
        product_details = response.xpath('//h5[contains(text(),"Product Details")]//following-sibling::div/p/text()').getall()
        meta = response.meta
        model_name = response.xpath("//div[@class='pdp-productinfo-details']//h1/text()").get(default='').strip()
        product_details = list(map(str.strip, product_details))
        description1 =[x for x in product_details if "Approximately measures" not in x ]
        description = response.xpath('//h5[contains(text(),"Designer Notes")]//following-sibling::div/p/text()').getall()
        description = description + description1
        for color_variant in response.xpath('//div[@class="pdp-color-swatches-holder"]//ul/li[contains(@class, "has-hover swatch-class")]'):
            item_data = {}
            url = ''
            is_color_selected = 'selected' in color_variant.xpath('./@class').extract_first()
            if is_color_selected:
                color_variant_url = color_variant.xpath('./a/@href').extract_first()
                if 'color=&&' in color_variant_url:
                    match = re.findall(r'color=.*?&|color=.*?$', response.url)
                    if match:
                        url = color_variant_url.replace('color=&', match[0])

            if not url:
                url = color_variant.xpath('./a/@href').extract_first()
            data = color_variant.xpath('./a/@data-lgimg').extract_first()

        # for url,data in zip(response.xpath('//div[@class="pdp-color-swatches-holder"]//li/a/@href'),response.xpath('//div[@class="pdp-color-swatches-holder"]//li/a/@data-lgimg')):
        #     id = url.get()
            # variant_data = data.get()
            variant_data = json.loads(data)
            image = variant_data["url"]
            style_id = variant_data.get("styleId","")
            colorName = variant_data.get("colorName","")
            standard_price = variant_data.get("standardPrice","")
            salePrice =variant_data.get("salePrice","")
            item_data.update({
                "id":url,
                "model_name":model_name,
                "measurements":product_details,
                "crawl_date":(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
                "status_code":str(response.status),
                "category":self.category,
                "brand":self.brand,
                "image":image,
                "style_id":style_id,
                "MSRP":standard_price,
                "price":salePrice,
                "description":description,
                "item_color":colorName,
            })

            filter_name =  meta.get("filter","")
            if filter_name and (filter_name != 'Color' or is_color_selected):
                meta_data = dict.fromkeys([filter_name],meta[filter_name])
                item_data.update(meta_data)
            yield item_data


    def createRequest(self, url, callback, meta=None, headers={}):
        request = scrapy.Request(url=url, callback=callback, meta=meta, headers=headers)

        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({"use_cache":True})
        return request

