'''
https://mercari.atlassian.net/browse/USCC-253 - Rae Dunn Home decor accents
'''

# NOTE: Request is Getting Timed Out with following error message:
# TimeoutError: User timeout caused connection failure: Getting https://tjmaxx.tjx.com/store/shop/_/N-1791108153?searchTerm=Rae+Dunn took longer than 180.0 seconds..
# Possible Reasons might be, Observed reasons are marked as (Y)
#   -> (Y) Server has rate limited your IP Address.
#   -> (Y)[US only region IPs] Server only responds to the IP Addresses of the the specific region.
#   -> Server is too busy or under very heavy load for long period of time.
#   -> Server responds to only specific User-Agent.
#   -> Server responds only if Cookies are present inside request header.
# Since the IPs are getting blocked(currently unsure permanently/temporarly),
# and so used dedicated proxies and used selenium request with download delays(10sec) and concurreny(1 request)

import scrapy
import random
import itertools
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

import random
from common.selenium_middleware.http import SeleniumRequest


class TjmaxxSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from tjmaxx.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Home decor accents
    
    brand : str
        brand to be crawled, Supports
        1) Rae Dunn

    Command e.g:
    scrapy crawl tjmaxx -a category='Home decor accents' -a brand='Rae Dunn'
    """

    name = 'tjmaxx'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80', # This proxy is blocked or getting Timedout
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'tjmaxx.com'
    blob_name = 'tjmaxx.txt'

    base_url = "https://tjmaxx.tjx.com"

    url_dict = {
        'Home decor accents': {
            'Rae Dunn': {
                'url': 'https://tjmaxx.tjx.com/store/shop/_/N-1791108153?searchTerm=Rae+Dunn',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TjmaxxSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            # yield self.create_request(url=self.launch_url, callback=self.parse)
            yield self.createDynamicRequest(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://tjmaxx.tjx.com/store/jump/product/2pk-Bunnies-And-Brunch-Plates/1000606743?colorId=NS1003516&pos=1:1
        @scrape_values title breadcrumb product_brand image price price_comparison product_details
        @returns items 1
        @meta {"use_proxy":"True"}
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'breadcrumb': '|'.join(response.xpath('//ul[@class="breadcrumbs"]/li/a/text()').extract()),
            'product_brand': response.xpath('//h1[@class="product-brand"]/text()').extract_first(),
            'title': response.xpath('//h2[@class="product-title"]/text()').extract_first(),
            'price': response.xpath(u'normalize-space(//p[@class="price"]/span[@class="product-price"]/text())').extract_first(),
            'price_comparison': response.xpath(u'normalize-space(//p[@class="price"]/span[@class="price-comparison"]/text())').extract_first().replace('Compare At', '').strip(),
            'product_details': response.xpath('//div[contains(@class, "product-description")]/ul/li/text()').extract(),
        }


    def parse(self, response):
        """
        @url https://tjmaxx.tjx.com/store/shop/_/N-1791108153?searchTerm=Rae+Dunn
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for product in response.xpath('//section[@class="content"]/div[@id="product-grid-wrapper"]//div[contains(@class, "product")]'):
            link = product.xpath('.//a[@class="product-link"]/@href').extract_first()
            if link:
                link = self.base_url + link
                # yield self.create_request(url=link, callback=self.crawlDetail)
                yield self.createDynamicRequest(url=link, callback=self.crawlDetail)

        next_link = response.xpath('//section[@class="content"]/div[@class="product-navigation"]/ul/li[@class="next"]/a/@href').extract_first()
        if next_link:
            next_link = self.base_url + next_link
            # yield self.create_request(url=next_link, callback=self.parse)
            yield self.createDynamicRequest(url=next_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            # request.meta['proxy'] = random.choice(self.proxy_pool)
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request


    def createDynamicRequest(self, url, callback, meta=None, wait_time=0, wait_until=None, perform_action=None):
        request = SeleniumRequest(url=url, callback=callback,
            wait_time = wait_time,
            wait_until = wait_until,
            perform_action = perform_action,
        )

        if self.proxy_pool:
            # request.meta['proxy'] = random.choice(self.proxy_pool)
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request
