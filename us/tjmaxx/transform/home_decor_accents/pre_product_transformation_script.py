import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = '. '.join(record.get('product_details', []))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price_comparison', '') or record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'(\d+)pk', r'Set of \1', model, flags=re.IGNORECASE)
            model = re.sub(r'(Set of \d+)(.*)', r'\2 \1', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            match = re.findall(r'Porcelain|Stoneware|accacia wood|Linen|Cotton', description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(list(dict.fromkeys(sorted(list(map(lambda x: x.title(), match))))))

            width = ''
            match = re.findall(r'(\d+)(.\d+){0,1}in W', description, flags=re.IGNORECASE)
            if match:
                width = match[0][0] + match[0][1] + ' in.'

            height = ''
            match = re.findall(r'(\d+)(.\d+){0,1}in H', description, flags=re.IGNORECASE)
            if match:
                height = match[0][0] + match[0][1] + ' in.'

            length = ''
            match = re.findall(r'(\d+)(.\d+){0,1}in L', description, flags=re.IGNORECASE)
            if match:
                length = match[0][0] + match[0][1] + ' in.'

            model_sku = ''
            match = re.findall(r'style #:(.*?)$', description, flags=re.IGNORECASE)
            if match:
                model_sku = match[0]

            product_type_dict = {
                "Mug": "Kitchen & Storage",
                "Plate": "Kitchen & Storage",
                "Food": "Kitchen & Storage",
                "Tea": "Kitchen & Storage",
                "Kettle": "Kitchen & Storage",
                "Pot": "Kitchen & Storage",
                "Bowl": "Kitchen & Storage",
                "Canister": "Kitchen & Storage",
                "Sheet": "Office & Other Home Decor",
                "Blanket": "Office & Other Home Decor",
                "Throw": "Office & Other Home Decor",
                "Underwear": "Clothing & Accessories",
                "pant": "Clothing & Accessories",
                "pajamas": "Clothing & Accessories",
                "shirt": "Clothing & Accessories",
                "bikini": "Clothing & Accessories",
                "Doormat": "Outdoor Decor",
                "Birdhouse": "Outdoor Decor",
            }

            product_type = ''
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', list(product_type_dict.keys())))), model, flags=re.IGNORECASE)
            if match:
                product_type = ', '.join(list(dict.fromkeys(list(map(lambda x: product_type_dict.get(x, x), match)))))
            else:
                product_type = 'Office & Other Home Decor'


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                'width': width,
                'length': length,
                'height': height,
                'product_type': product_type,
                'model_sku': model_sku,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
