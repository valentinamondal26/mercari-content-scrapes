import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()


            spec = record.get('specifications', {})
            year = spec.get('Year', '')
            model = record.get('title', '')
            model = re.sub(re.compile(r'TOPPS NOW®|TOPPS NOW|Topps x|Topps\s+-|Topps|®|™|\+ Free Gift!',
                re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            width = ''
            height = ''
            paper_type = ''
            product_information = spec.get('Product Information', '')
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}\"x(\d+)(.\d+){0,1}\"'), product_information)
            if match:
                group = match[0]
                width = group[0]+group[1]+'"'
                height = group[2]+group[3]+'"'

            match = re.findall(re.compile(r'(Printed on.*)', re.IGNORECASE), product_information)
            if match:
                paper_type = match[0]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                'image': record.get('image', ''),
                'breadcrumb': record.get('breadcrumb', ''),

                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "model": model,
                'year': year,
                "paper_type": paper_type,
                "product_line": spec.get('Property', ''),
                'product_type': spec.get('Product Type', ''),
                'teambaseball': spec.get('Baseball Team', ''),
                'player': spec.get('Baseball Player', ''),
                'model_sku': spec.get('SKU', ''),
                'width': width,
                'height': height,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
