'''
https://mercari.atlassian.net/browse/USCC-90 - Topps Sport Cards & Collectibles
'''

import scrapy
import os
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider


class ToppsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from topps.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Sport Cards & Collectibles

    brand : str
        brand to be crawled, Supports
        1) Topps

    Command e.g:
    scrapy crawl topps -a category='Sport Cards & Collectibles' -a brand='Topps'
    """

    name = 'topps'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'topps.com'
    blob_name = 'topps.txt'

    # &p=3&is_scroll=1&_=1574435861449
    query_params = '&p={page}&is_scroll=1&_={timestamp}'
    page = 1

    url_dict = {
        'Sport Cards & Collectibles': {
            'Topps': {
                'url': 'https://www.topps.com/cards-collectibles.html?property=11221%2C11227%2C20536%2C20658%2C15493%2C11237%2C11239%2C11243',
            },
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ToppsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.launch_url = 'https://www.topps.com/cards-collectibles.html?property=11221%2C11227%2C20536%2C20658%2C15493%2C11237%2C11239%2C11243'


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.topps.com/topps-project-2020-card-170-1984-don-mattingly-by-ben-baller.html
        @scrape_values id breadcrumb title specifications.Year specifications.Property specifications.SKU  specifications.Product\sInformation  specifications.Product\sType specifications.Property specifications.Baseball\sTeam specifications.Baseball\sPlayer specifications.Artist\sName price image description
        """

        specifications = {}
        for spec in response.xpath('//table[@id="product-attribute-specs-table"]//td[@class="col data"]'):
            key = spec.xpath('./@data-th').extract_first()
            value = '\n'.join(spec.xpath(u'.//text()[normalize-space(.)]').extract())
            specifications[key] = value.strip()

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            # 'id': response.url,
            'id': response.xpath('//meta[@property="og:url"]/@content').extract_first(default=''),
            'breadcrumb': ' | '.join(response.xpath('//div[@class="breadcrumbs"]/ul[@class="items"]/li/a/text()').extract()),

            'title': response.xpath('//span[@itemprop="name"]/text()').extract_first(default=''),
            'specifications': specifications,
            'price': response.xpath('//div[@class="product_price__wr"]//span[@class="price"]/text()').extract_first(default=''),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(default=''),
            'description': ''.join(response.xpath('//div[@class="product attribute description"]/div[@class="value"]/p//text()').extract()) or '',
        }


    def parse(self, response):
        """
        @url https://www.topps.com/cards-collectibles.html?property=11221%2C11227%2C20536%2C20658%2C15493%2C11237%2C11239%2C11243
        @returns requests 1

        """
        products = response.xpath('//div[@data-container="product-grid"]')
        if products and len(products) > 0:
            for product in products:

                id = product.xpath('.//a[@class="product-item-link"]/@href').extract_first(default='')

                if id:
                    request = self.create_request(url=id, callback=self.crawlDetail)
                    yield request

            self.page = self.page + 1
            nextLink = self.launch_url + self.query_params.format(page=self.page, timestamp=int(datetime.now().timestamp()*1000))
            yield self.create_request(url=nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
