'''
https://mercari.atlassian.net/browse/USDE-697 - Coach Satchel
https://mercari.atlassian.net/browse/USDE-698 - Coach Women's Wallets
https://mercari.atlassian.net/browse/USCC-426 - Coach Satchel
'''

import scrapy
from scrapy.exceptions import CloseSpider
import os
import datetime

import itertools
from urllib.parse import urlparse, parse_qs, urlencode


class CoachSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from coach.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags
        2) Handbags
        3) Women's Wallets

    brand : str
        brand to be crawled, Supports
        1) Coach

    Command e.g:
    scrapy crawl coach -a category='Shoulder Bags' -a brand='Coach'

    scrapy crawl coach -a category='Handbags' -a brand='Coach'
    scrapy crawl coach -a category="Women's Wallets" -a brand='Coach'
    scrapy crawl coach -a category="Satchel" -a brand='Coach'
    """

    name = 'coach'
    category = None
    brand = None
    source = 'coach.com'
    blob_name = 'coach.txt'

    extra_item_infos = {}

    merge_key = 'id'

    BASE_URL = 'https://shop.rebag.com'

    size = 24
    
    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Shoulder Bags' : {
            'Coach': {
                'url': 'https://www.coach.com/shop/women-handbags-shoulder-bags',
            },
        },
        'Handbags' : {
            'Coach': {
                'url': 'https://www.coach.com/shop/women-handbags',
                'filters': ['Categories', 'Material', 'Color', 'Hardware Color', 'Handbag Size', 'Collection']
            },
        },
        "Women's Wallets" : {
            'Coach': {
                'url': 'https://www.coach.com/shop/women-wallets?viewAll=true',
                'filters': ['Categories', 'Material', 'Color', 'Hardware Color']
            },
        },
        'Satchel' :{
            'Coach': {
                'url': 'https://www.coach.com/shop/women-handbags#prefn1=silhouette&prefv1=Satchels',
                'filters': ['Material', 'Color', 'Handbag Size', 'Hardware Color', 'Collection']
            }
        } 
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(CoachSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url =self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info(f"Crawling for category {category} and brand {brand},url:{self.launch_url}")

    def contracts_mock_function(self):
        self.round_robin = itertools.cycle(self.proxy_pool)
        self.filters = ['Categories', 'Material', 'Color', 'Hardware Color']

    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)
        if self.filters:
            yield self.create_request(url=self.launch_url, callback=self.parse_filters, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://www.coach.com/coach-disney-mickey-mouse-x-keith-haring-kisslock-bag/4719.html?dwvar_color=B4OH3#prefn1=silhouette&prefv1=Satchels&start=1
        @meta {"use_proxy":"True"}
        @scrape_values title price image description style details color
        """
        yield {
            'id': response.url,
            'category': self.category,
            'brand': self.brand,
            'crawlDate': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),

            'title': response.xpath('//h1[@class="product-name-desc"]/text()').extract_first(),
            'price': response.xpath(u'normalize-space(//div[@class="prices"]//span[@class="pdp list"]/text())').extract_first(),
            'image': response.xpath('//div[@id="pdp-carousel-element-1"]/img/@data-src').extract_first(),

            'description': response.xpath(u'normalize-space(//p[@id="editor-notes"]/text())').extract_first(),
            'style': response.xpath(u'//li[@class="style-number"]/text()[normalize-space(.)]').extract_first(),
            'details': ', '.join(response.xpath('//div[@class="pdp-info__details-content"]//text()[normalize-space(.)]').extract()),
            'color': response.xpath('//p[@class="current-swatch-color"]/text()').extract_first(),
        }


    def update_extra_item_infos(self, id, extra_item_info): 
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def parse_filters(self, response):
        """
        @url https://www.coach.com/shop/women-handbags
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        ##Note :Make sure All the filters urls extracting XPATH is same.
        for filter in self.filters:
            for facet in response.xpath(f'//div[@class="filters-row"]/section[contains(@aria-label, "{filter}")]//li[@class="refinement"]'):
                filter_url = facet.xpath('./a/@href').extract_first()
                filter_name = facet.xpath('./a/@title').extract_first()
                if filter_name and filter_url:
                    request = self.create_request(url=filter_url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                        filter: filter_name,
                    }
                    print(f'==>parse_filters: {filter}: {filter_name}, {filter_url}')
                    yield request


    def parse(self, response):
        """
        @url https://world.coach.com/shop/women-wallets?viewAll=true#prefn1=fabrication&prefv1=Leather
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        extra_item_info = response.meta.get('extra_item_info', {})

        products = response.xpath('//div[@class="col-lg-3 col-md-3 col-sm-4 col-xs-6 prod-grid "]')
        for product in products:

            url = product.xpath('.//a[@itemprop="url"]/@href').extract_first()
            if url:
                if not self.extra_item_infos.get(url, None):
                    self.extra_item_infos[url] = {}

                self.update_extra_item_infos(url, extra_item_info)
                yield self.create_request(url=url, callback=self.crawlDetail)

        if products:
            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            start = query_dict.get('start', [0])
            query_dict.update({'start':[int(start[0]) + self.size], 'size': [self.size]})
            url = parts._replace(query=urlencode(query_dict, True)).geturl()

            request = self.create_request(url=url, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request
        else:
            print('End of crawl: start-offset:{}'.format(response.url))


    def create_request(self, url, callback, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request
