import hashlib
import re

class Mapper(object):
    def post_process(self, record):
        def trim(value):
            return re.sub(re.compile(r'\s+'), ' ', value).strip() if isinstance(value, str) else value
        record = {k: trim(v) for k, v in record.items()}
        return record

    def map(self, record):
        if record:
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace('$', '').strip()

            brand = record.get('brand', '')
            title = record.get('title', '')
            details = record.get('details', '').replace('\n', '').strip()

            color = record.get('color','')
            color = color.title()
            pattern = re.compile(r'^[\w+]{2}\/', re.IGNORECASE)
            match = re.findall(pattern, color)
            if match:
                color = f"{re.sub(pattern, '', color)}/{match[0].replace('/', '')}"
                color = re.sub(r'\s+', ' ', color).strip()

            materials_meta_dict = {
                'Leather': ['Smooth Leather', 'Pebble Leather', 'Embossed Leather'],
                'Canvas': ['Coated Canvas', 'Signature Coated Canvas', 'Coated Canvas'],
            }
            materials_meta = []
            materials_meta.extend(materials_meta_dict.keys())
            [materials_meta.extend(x) for x in materials_meta_dict.values()]

            material = ', '.join(record.get('Material', []))
            if not material:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), title)
                material = ', '.join(match)
            if not material:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), details)
                material = ', '.join(match)
            for key, value in materials_meta_dict.items():
                material = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', value))), re.IGNORECASE), key, material)
            material = ', '.join(sorted(list(dict.fromkeys(material.split(', ')))))
            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()

            model = title
            keywords = []
            keywords.extend(materials_meta)
            keywords.append(color)
            keywords.append(brand)
            keywords.extend(['wallet'])
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: x, keywords))), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'^\s*x\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()
            title_case_exceptions = {
                'id': 'ID',
                'noa': 'NOA',
            }
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', title_case_exceptions.keys()))), re.IGNORECASE), lambda x: title_case_exceptions.get(x.group().lower(), x.group()), model)
            model = re.sub(r'\s+', ' ', model).strip()

            color = re.sub(re.compile(r"\bmulti\b", re.IGNORECASE), "Multicolor", color)
            # remove duplicates brass/chalk chalk/brasss -> brass/chalk
            color = "/".join(sorted(set(color.split("/")), key=color.split("/").index))
            color = "/".join([" ".join(sorted(set(c.split()), key=c.split().index)) for c in color.split("/")])
            # extract multicolor if part of another color chalk multicolor/b4 -> chalk/multicolor/b4
            color = re.sub(re.compile(r"(.*)\smulticolor\/|(.*)\smulticolor$", re.IGNORECASE),r"\1\2/Multicolor/" ,color).strip("/")
            # push multicolor to rear end
            color = color.split("/")
            color.sort(key = 'Multicolor'.__eq__)
            # if colors > 2 make its as 1st color/ multicolor
            if len(color) > 2:
                color =  color[0]+"/Multicolor"
            else:
                color = "/".join(color)

            measurements = ''
            length = ''
            height = ''
            width = ''

            match = re.findall(re.compile(r'((\d+\s*\d*\/*\d*")\s*\(L\)\s*x\s*(\d+\s*\d*\/*\d*")\s*\(H\)\s*x\s*(\d+\s*\d*\/*\d*") \s*\(W\))', re.IGNORECASE), details)
            if match:
                measurements = match[0][0]
                length = match[0][1]
                height = match[0][2]
                width = match[0][3]

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": brand,

                "image": record.get('image', ''),
                "description": details,
                "ner_query": details,
                "title": record.get('title', ''),

                "model": model,
                "color": color,
                "material": material,
                
                'length': length,
                'height': height,
                'width': width,
                'measurement': measurements,
                'item_type': ', '.join(record.get('Categories', [])),

                "msrp": {
                    "currency_code": "USD",
                    "amount": price,
                },

                "color_filter": ', '.join(record.get('Color', [])),
            }

            return self.post_process(transformed_record)
        else:
            return None
