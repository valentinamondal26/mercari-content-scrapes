import hashlib
import re
from titlecase import titlecase
from colour import Color

class Mapper(object):
    def post_process(self, record):
        def trim(value):
            return re.sub(re.compile(r'\s+'), ' ', value).strip() if isinstance(value, str) else value
        record = {k: trim(v) for k, v in record.items()}
        return record

    def map(self, record):
        if record:
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace('$', '').strip()

            color = "/".join(record.get('Color', ''))
            if color == "":
                color = record.get("color", "")
                color = color.title()

            color = re.sub(re.compile(r'\bMulti\b', re.IGNORECASE), 'Multicolor', color)
            colors = color.split("/")
            colors = list(dict.fromkeys(colors))#list(set(colors))
            primary_colors = []
            secondary_colors = []
            for col in colors:
                try: 
                    Color(col)
                    primary_colors.append(col)
                except ValueError:
                    secondary_colors.append(col)
            primary_colors.extend(secondary_colors)
            if len(primary_colors) > 2:
                color = f'{primary_colors[0]}/Multicolor'

            color = re.sub(r'\/.* (Multicolor)', r'/\1', color, flags=re.IGNORECASE)
            color = re.sub(r'(.*)(\w\d\/)(.*)', r'\1\3/\2', color, flags=re.IGNORECASE)
            color = re.sub(r'\/$', '', color, flags=re.IGNORECASE).strip()

            category = ''
            categories = record.get('Categories', [])
            if 'Satchels' in categories or record.get('category', '') == 'Satchel':
                category = 'Satchel'
            else:
                return None

            brand = record.get('brand', '')
            details = record.get('details', '').replace('\n', '').strip()

            material_meta = ["Leather", "Canvas", "Chambray",
                             "Suede", "Snakeskin", "Lambskin", "Calfskin"]
            material = record.get('Material', []) or []
            if re.findall(r'\bAlligator\b', record.get('title', ''), flags=re.IGNORECASE):
                material.append('Alligator')

            sorted_materials = []
            material_type1 = re.findall(r'\b\w+\s+Leather\b|\b\w+\s+Canvas\b', ', '.join(material), flags=re.IGNORECASE)
            material_type2 = ''.join(re.findall(r', Leather\b|, Canvas\b|^Leather\b|^Canvas', ', '.join(material), flags=re.IGNORECASE))
            material_type2 = re.sub(r'^,', '', material_type2).strip().split(', ')
            sorted_materials.extend(material_type1)
            sorted_materials.extend(material_type2)
            sorted_materials = [item for item in sorted_materials if item.strip()]
            missed_material = list(set(material) - set(sorted_materials))            
            sorted_materials.extend(missed_material)
            
            material = '/'.join(sorted_materials)
            material = re.sub(r'\s+', ' ', material).strip()
            if material == '':
                material = "/".join(re.findall(re.compile(
                    "|".join(material_meta), re.IGNORECASE), details))
            material =re.sub(r'Signature Coated Canvas', 'Coated Canvas', material, flags=re.IGNORECASE)
            material = re.sub('\bPebble Leather/Smooth Leather\b', 'Pebble & Smooth Leather', material, flags=re.IGNORECASE)
            material = '/'.join(list(dict.fromkeys(material.split('/'))))
            if re.findall(r'\b\w+\s+Leather', material, flags=re.IGNORECASE) and re.findall(r'\/\bLeather\b', material, flags=re.IGNORECASE):
                material = re.sub(r'\/Leather\b', '', material, flags=re.IGNORECASE)
            if re.findall(r'\b\w+\s+Canvas', material, flags=re.IGNORECASE) and re.findall(r'\/\bCanvas\b', material, flags=re.IGNORECASE):
                material = re.sub(r'\/Canvas\b', '', material, flags=re.IGNORECASE)
                    
            model = record.get('title', '')
            keywords = []
            # keywords.extend(record.get('Material', []))
            materials = [m for m in re.findall(
                r"\w+", material) if m != "Signature"]
            keywords.extend(materials)
            keywords.append(color)
            keywords.append(brand)
            keywords.extend(material_meta)
            keywords.extend(categories)
            keywords.extend(['satchel', 'crossbody', 'tote', 'Alligator','and'])
            model = re.sub(re.compile(
                '|'.join(list(map(lambda x: r'\b'+ x + r'\b', keywords))), re.IGNORECASE), '', model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(re.compile(
                r"[with|in]*\s*Signature\s*(?!chain)\b(\S+)|[with|in]*\s*Signature\s*(?!chain)\b$", re.IGNORECASE), r" \1 ", model)
            if re.compile(r"^x\b", re.IGNORECASE).match(model):
                model = model[1:]
            model = " ".join(model.split())
            model = titlecase(model)
            model = re.sub(r'\bSatchel Bag\b|\bSatchel\b|\bBag\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(?!\bMessenger Bag\b)\bMessenger\b', 'Messenger Bag', model, flags=re.IGNORECASE)
            model = re.sub(r'\bIn\b$', '', model, flags=re.IGNORECASE)

            measurements = ''
            length = ''
            height = ''
            width = ''

            match = re.findall(re.compile(
                r'((\d+\s*\d*\/*\d*")\s*\(L\)\s*x\s*(\d+\s*\d*\/*\d*")\s*\(H\)\s*x*x*\s*(\d*\s*\d*\/*\d*\"*)\s*\(*W*\)*)', re.IGNORECASE), details)
            if match:
                measurements = match[0][0]
                length = match[0][1]
                height = match[0][2]
                width = match[0][3]

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": category,
                "brand": brand,

                "image": record.get('image', ''),
                "description": details,
                "ner_query": details,
                "title": record.get('title', ''),

                "model": model,
                "material": material.title(),
                "color": color.title(),
                'length': length,
                'height': height,
                'width': width,
                'measurement': measurements,

                "msrp": {
                    "currency_code": "USD",
                    "amount": price,
                },

                # "color_filter": ', '.join(record.get('Color', [])),
                # "model_sku": record.get('style','').strip('\n').strip(''),
                # "product_line": ', '.join(record.get('Collection', [])),
            }

            return self.post_process(transformed_record)
        else:
            return None
