import hashlib
import re


class Mapper(object):
    def post_process(self, record):
        def trim(value):
            return re.sub(re.compile(r'\s+'), ' ', value).strip() if isinstance(value, str) else value
        record = {k: trim(v) for k, v in record.items()}
        return record

    def map(self, record):
        if record:
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace('$', '').strip()

            color = ", ".join(record.get('Color', ''))
            if color == "":
                color = record.get("color", "")
                color = color.title()

            color = re.sub(re.compile(r'\bMulti\b', re.IGNORECASE), 'Multicolor', color)
            colors = color.split(", ")
            if len(colors) > 3 or \
                (len(colors) > 1 and re.findall(re.compile(r'\bMulticolor\b', re.IGNORECASE), color)):
                color = "/".join([colors[0] if colors[0] != 'Multicolor' else colors[1], "Multicolor"])

            category = ''
            categories = record.get('Categories', [])
            if 'Satchels' in categories:
                category = 'Satchel'
            elif 'Totes' in categories:
                category = 'Tote Bags'
            elif 'Crossbodies' in categories:
                category = 'Crossbody Bags'
            else:
                return None

            brand = record.get('brand', '')
            details = record.get('details', '').replace('\n', '').strip()

            material_meta = ["Leather", "Canvas", "Chambray",
                             "Suede", "Snakeskin", "Lambskin", "Calfskin"]
            material = record.get('Material', [])
            material = ', '.join(material)
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ', '.join(
                    list(filter(lambda x: x != 'Leather', material.split(', '))))
            material = re.sub(r'\s+', ' ', material).strip()
            if material == '':
                material = ", ".join(re.findall(re.compile(
                    "|".join(material_meta), re.IGNORECASE), details))
            material = ', '.join(list(dict.fromkeys(material.split(', '))))
            if "Signature Coated Canvas" in material.split(", "):
                material = (["Signature Coated Canvas" if mat ==
                             "Coated Canvas" else mat for mat in material.split(", ")])
                material = ", ".join(sorted(set(material), key=material.index))

            model = record.get('title', '')
            keywords = []
            # keywords.extend(record.get('Material', []))
            materials = [m for m in re.findall(
                r"\w+", material) if m != "Signature"]
            keywords.extend(materials)
            keywords.append(color)
            keywords.append(brand)
            keywords.extend(categories)
            keywords.extend(['satchel', 'crossbody', 'tote'])
            model = re.sub(re.compile(
                '|'.join(list(map(lambda x: x, keywords))), re.IGNORECASE), '', model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(re.compile(
                r"[with|in]*\s*Signature\s*(?!chain)\b(\S+)|[with|in]*\s*Signature\s*(?!chain)\b$", re.IGNORECASE), r" \1 ", model)
            if re.compile(r"^x\b", re.IGNORECASE).match(model):
                model = model[1:]
            model = " ".join(model.split())

            measurements = ''
            length = ''
            height = ''
            width = ''

            match = re.findall(re.compile(
                r'((\d+\s*\d*\/*\d*")\s*\(L\)\s*x\s*(\d+\s*\d*\/*\d*")\s*\(H\)\s*x\s*(\d+\s*\d*\/*\d*") \s*\(W\))', re.IGNORECASE), details)
            if match:
                measurements = match[0][0]
                length = match[0][1]
                height = match[0][2]
                width = match[0][3]

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": category,
                "brand": brand,

                "image": record.get('image', ''),
                "description": details,
                "ner_query": details,
                "title": record.get('title', ''),

                "model": model,
                "material": material.title(),
                "color": color.title(),
                'length': length,
                'height': height,
                'width': width,
                'measurement': measurements,

                "msrp": {
                    "currency_code": "USD",
                    "amount": price,
                },

                # "color_filter": ', '.join(record.get('Color', [])),
                # "model_sku": record.get('style','').strip('\n').strip(''),
                # "product_line": ', '.join(record.get('Collection', [])),
            }

            return self.post_process(transformed_record)
        else:
            return None
