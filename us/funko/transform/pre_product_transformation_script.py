import hashlib
 

class Mapper(object):

    categories_list = ["Apparel", "Games", "Pop!", "Vinyl", "Mystery Minis", "Pop! Pez", "Pop Protector", "Pocket Pop! Keychain"]
    sub_type_list = ["Animation", "Movies", "Television"]
    features_list = ["Diamond Collection", "Glow in the Dark", "Flocked", "Chrome"]
    product_line_list =["Ad Icons","DC", "Disney", "Fantastik Plastik","Freddy Funko","Hanna Barbera","Marvel","Myths","Scott Pilgrim","Star Wars","Stranger Things","Wetmore Forest"]

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record
    
    def common_member(self,a, b): 
        a_set = set(a) 
        b_set = set(b) 
        if (a_set & b_set): 
            return (a_set & b_set) 
        else: 
            return set()
        
    def remove_html_tags(self,text):
        """Remove html tags from a string"""
        import re
        clean = re.compile('<.*?>')
        cleaned_text= re.sub(clean, '', text)
        head, sep, tail = cleaned_text.partition('PLEASE NOTE')
        head, sep, tail = head.partition('Please note')
        return head

    def map(self, record):
        if record:
            record = self.pre_process(record)
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            msrp = record.get('msrp', '')
            msrp = msrp.replace("$", "").strip()

            head, sep, tail = record["title"].partition(':')
            product_line = ''
            content = head.split(' ', 1)
            print("content::", content)
            model =''
            if sep:
                model = head
            if len(content) > 1:
                product_line = content[1]
            description = self.remove_html_tags(record["description"])
            product_type = ''
            if (len(self.common_member(record["tags"], self.categories_list))> 0):
                product_type = ', '.join(self.common_member(record["tags"], self.categories_list))
            sub_type = ''
            if (len(self.common_member(record["tags"], self.sub_type_list))> 0):
                sub_type = ', '.join(self.common_member(record["tags"], self.sub_type_list))
            features = ''
            if (len(self.common_member(record["tags"], self.features_list)) > 0):
                features = ', '.join(self.common_member(record["tags"], self.features_list))
                
            categories = ', '.join(self.common_member(record["tags"], self.product_line_list))
                    
            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawled_date'],
                "status_code": record['status_code'],
                "model": model,
                "title":record["title"],
                "msrp": {
                    "currency_code": "USD",
                    "amount": msrp
                },
                "model_sku": record["model_sku"],
                "description":description,
                "category": record['category'],
                "brand": record['brand'],
                "product_type": product_type,
                "sub_type": sub_type,
                "product_line": categories,
                "features": features,
                "tags": record["tags"],
                "image":"https:" + record["image"]
            }
 
            return transformed_record
        else:
            return None

