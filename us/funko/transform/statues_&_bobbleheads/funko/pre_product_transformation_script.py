import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            title=record.get('title')
        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "crawl_category": record.get('category'),
                "category":"Statues & Bobbleheads",
                "brand": record.get('brand', ''),
                "image":record.get('image',''),
                "ner_query": ner_query,

                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "title":title,
                "item_number":record.get('item_number',''),
                "release_date":record.get('release_date'),
                "product_type":record.get('product_type',''),
                "license":record.get('license',''),
                "exclusivity":record.get('exclusivity','')
            }

            return transformed_record
        else:
            return None
    
   