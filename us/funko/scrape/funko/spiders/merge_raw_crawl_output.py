# https://mercari.atlassian.net/browse/USDE-867
# Funko data merge
# We need merge the attached dataset where we merge the new data into the old data itemId.
# old data is located here : content_us/raw/funko.com/sports/funko/2019-07-18_19_45_16/json/funko.jl
# new data is located here : content_us/raw/funko.com/sports/funko/2019-12-05_07_48_25/json/funko.txt
# merged data is uploaded to : content_us/raw/funko.com/sports/funko/2019-12-05_07_48_25/json/funko_updated.txt
# csv data availble at: https://docs.google.com/spreadsheets/d/1SHn2IyDtnbcGPbXgj_RGPNjsY5RzDnGlEGV5KBkXzj0/edit#gid=726259500

if __name__ == "__main__":

    import pandas as pd
    import numpy
    path = '/Desktop/debug/funko/'
    with open(path+'2019-07-18_19_45_16_funko.jl', 'r') as f1, open(path+'2019-12-05_07_48_25_funko.txt', 'r') as f2:
        df1 = pd.read_json(f1, lines=True)
        df2 = pd.read_json(f2, lines=True)
        df1['key'] = df1.apply(lambda x: x.item_number + x.title + x.product_type + x.release_date, axis=1)
        df2['key'] = df2.apply(lambda x: x.item_number + x.title + x.product_type + x.release_date, axis=1)
        df1.to_csv(path+'2019-07-18_19_45_16_funko.csv', index=False)
        df2.to_csv(path+'2019-12-05_07_48_25_funko.csv', index=False)
        print(df1.info())
        print(df2.info())
        print(df1.key.nunique(), df2.key.nunique())
        import numpy as np
    #     df1 = df1.replace(np.nan, '', regex=True)
    #     df2 = df2.replace(np.nan, '', regex=True)
        df1.drop_duplicates(subset ="key", keep = False, inplace = True)
        df3 = pd.merge(df2, df1, how='left', on='key', suffixes=('', '_y'))
        df3 = df3.rename(columns={"id": "id_a"})
        df3 = df3.replace(np.nan, '', regex=True)
        def f(x):
            id = x.id_y if x.id_y else x.id_x
    #         print(id)
            return id
        df3['id'] = df3.apply(func=lambda x: x.id_y if x.id_y else x.id_a, axis=1)
        print(df3.info())
        df3.to_csv(path+'m_left.csv', index=False, columns=["brand","category","crawlDate","exclusivity","id","image","item_number","license","price","product_type","release_date","statusCode","title"])

    import pandas as pd
    def csv_to_jsonlines(path, filename):
        with open(path+filename, 'r+') as f:
            df = pd.read_csv(f)
            df = df.replace(np.nan, '', regex=True)
            df.to_json(path+filename+'.jl', orient='records', lines=True)

    csv_to_jsonlines(path, 'm_left.csv')