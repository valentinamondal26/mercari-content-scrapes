import scrapy
from scrapy import Request
import json
import datetime
import unicodedata
import time
import re
from datetime import date

import random
from scrapy.loader import ItemLoader
from funko.items import FunkoProduct
from scrapy.exceptions import CloseSpider
from requests.utils import requote_uri


class FunkoSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from funko.com

    Attributes

    brand : str
        brand to be crawled, Supports
        1) Funko
    Command e.g:
    scrapy crawl funko  -a brand='Funko'
    """

    name = 'funko'
    brand = None
    source = 'funko.com'
    blob_name = 'funko.txt'

    # schema_keys=['brand', 'title', 'release_date', 'product_type', 'item_number']

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]
    start_urls=["https://www.funko.com/api/search/terms"]
    baseurl="https://www.funko.com"
    url_dict = {
            'Funko': 'https://www.funko.com/api/search/terms',
    }
    payload={"type":"catalog","sort":{"releaseDate":"desc"},"pageCount":"160","page":"1"}

    


    def __init__(self, brand=None, *args, **kwargs):
        super(FunkoSpider,self).__init__(*args, **kwargs)

        if not brand:
            self.logger.error("brand should not be None")
            raise CloseSpider(' brand not found')

        self.brand = brand
        self.launch_url = self.url_dict.get(brand)
        if not self.launch_url:
            self.logger.error('Spider is not supported for  brand:{}'.format(brand))
            raise(CloseSpider('Spider is not supported for and brand:{}'.format(brand)))

        self.logger.info("Crawling for  brand "+brand + ",url:" + self.launch_url)


    
    def start_requests(self):
        if self.launch_url:
            yield Request(url=self.launch_url, callback=self.parse,method="POST",body=json.dumps(self.payload),headers={'Content-Type': 'application/json; charset=UTF-8'})


    def createRequest(self, url,callback,body,category):
        request = scrapy.Request(url=url, method="POST",body=body,headers={'Content-Type': 'application/json; charset=UTF-8'},callback=callback,meta={'category':category})
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request


    def parse(self, response):

        needed_json=json.loads(response.body.decode("utf-8"))
        categories=needed_json["attributes"]["productCategories"]

        for category in categories:
            product_category=category['key']
            pageCount=category["count"]
            payload={"type":"catalog","sort":{"releaseDate":"desc"},"productCategories":["{}".format(product_category)],"pageCount":"{}".format(pageCount),"page":"1"}
            yield self.createRequest(response.url,self.parse_products,json.dumps(payload),product_category)
        

    def parse_products(self,response):

     try:
        # file_name=response.meta['category']+".json"
        # file=open(file_name,"w")
        # file.write(response.body.decode("utf-8"))

        needed_json=json.loads(response.body.decode("utf-8"))

        for product in needed_json.get('hits'):
            item = FunkoProduct()
            item['crawlDate']=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            item["statusCode"]=str(response.status)

            category=product.get('productCategories',[])
            product_brands=product.get('productBrands',[])
            licenses=product.get('licenses',[])
            referenceUrl=product.get('referenceUrl','')

            if category or product_brands or licenses or referenceUrl:
                data={'baseurl':self.baseurl,'category':category[0],'product_brands':product_brands[0],'licenses':licenses[0],'referenceUrl':referenceUrl}
                item['id']=requote_uri("{baseurl}/products/{category}/{product_brands}/{licenses}/{referenceUrl}/".format_map(data))
            else:
                item['id']=''

            item['title']=product.get('title','')
            item['brand']=self.brand
            releaseDate=product.get('releaseDate','').split('T')[0]
            if releaseDate!='':
                releaseDate=datetime.datetime.strptime(releaseDate,'%Y-%m-%d').strftime('%B %d %Y')
                releaseDate=releaseDate.split(' ')
                day=int(releaseDate[1].lstrip('0'))
                suffix=''
                if 4 <= day <= 20 or 24 <= day <= 30:
                    suffix = "th"
                else:
                    suffix = ["st", "nd", "rd"][day % 10 - 1]
                releaseDate[1]='{}{}'.format(day,suffix)
                releaseDate=' '.join(releaseDate)
            item['release_date']=releaseDate

            image=product.get('imageUrl','')
            if image!='':
                item['image']=self.baseurl+image
            else:
                item['image']=image
            item['item_number']=product.get('itemNumber','')
            item['category']=','.join(product.get('productCategories','')) 
            item['product_type']=','.join(product.get('productLines',''))
            item['license']=','.join(product.get('licenses',''))
            item['exclusivity']=','.join(product.get('exclusivity',''))
            price=product.get('marketValue','')
            if price!='':
                item['price']='${}'.format(price)
            else:
                item['price']=price

            yield item
     except Exception as e:
             print("Exception occured due to  ",e)
             item=FunkoProduct()
             yield item
