import scrapy
import json
from datetime import datetime
from scrapy.exceptions import CloseSpider
import random

from scrapy import signals

class FunkoSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from funko.com

    Attributes

    brand : str
        brand to be crawled, Supports
        1) Funko
    Command e.g:
    scrapy crawl funko_filter  -a category='Status & Bobbleheads' -a brand='Funko'
    """

    name = "funko_filter"

    base_url = 'https://shop.funko.com'

    category = "Status & Bobbleheads"
    brand = "Funko"
    source = 'funko.com'
    blob_name = 'funko.txt'

    # NOTE: Doesn't work with proxies, Api's are irresponsive
    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Status & Bobbleheads': {
            'Funko': 'https://shop.funko.com/collections/all-products/pop',
        }
    }

   
    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(FunkoSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category "+category+" and brand "+brand + ",url:" + self.launch_url)


    def start_requests(self):
        print('launch_url', self.launch_url)
        if self.launch_url:
            yield scrapy.Request(url=self.launch_url, callback=self.parse)


    def get_url(self,category,brand):
        try:
            return self.url_dict[category][brand]
        except Exception as e:
            print('Exception occured:', e)
            raise(ValueError('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)
        return request


    def parse(self, response):
        for link in response.xpath('//div[@class="small-6 medium-6 large-3 catalog-product"]//a/@href').getall():
                yield self.createRequest(url=self.base_url+ link, callback=self.parse_products)
                         
       
        next_page_url = response.xpath('//span[@class="next"]//a/@href').get()
        print(next_page_url,"\n\nNEXT PAGE\n\n")
        if next_page_url:
            yield self.createRequest(self.base_url + next_page_url, self.parse)
            
    def parse_products(self, response):

        item = dict()
          
        item["crawled_date"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item["status_code"] = str(response.status)
        item["category"] = self.category
        item["brand"] = self.brand
        item["id"] = response.url
        json_string = response.xpath('normalize-space(//*[@id="ProductJson-funko-product-template"][contains(text(),"id")]/text())').get(default='')
        product_dict = json.loads(json_string)
        for key, value in product_dict.items():
            if key == "title":
                item[key] = value
            if key == "price":
                item["msrp"] = response.xpath('normalize-space(//span[@class="product-single__price"]/text())').get()
            if key == "variants":
                item["model_sku"] = value[0]["sku"]
            if key == "description":
                item["description"] = value
            if key == "tags":
                item["tags"] = value
            if key == "featured_image":
                item["image"] = value
        
        yield item
        
                
                

