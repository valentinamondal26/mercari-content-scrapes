# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FunkoProduct(scrapy.Item):
    crawlDate = scrapy.Field()
    statusCode = scrapy.Field()
    id = scrapy.Field()
    title=scrapy.Field()
    brand=scrapy.Field()
    price=scrapy.Field()
    release_date=scrapy.Field()
    image=scrapy.Field()
    item_number=scrapy.Field()
    category=scrapy.Field()
    product_type=scrapy.Field()
    license=scrapy.Field()
    exclusivity=scrapy.Field()
    