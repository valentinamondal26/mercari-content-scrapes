BOT_NAME = 'funko.com'

SPIDER_MODULES = ['funko.spiders']
NEWSPIDER_MODULE = 'funko.spiders'
DOWNLOAD_DELAY = 0.5
#By default auto_throttle strat - max delay is 5s to 60s
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_TARGET_CONCURRENCY = 64
ROBOTSTXT_OBEY = False
#LOG_FILE="log.txt"
COOKIES_ENABLED = False

HTTPERROR_ALLOW_ALL=True
DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

import os,sys
from os.path import dirname as up
sys.path.append(up(up(up(os.getcwd())))+"/us/")
ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

# GCS settings - actual value be 'raw/goat.com/nike/womens_sneakers/json/2019-02-06_00_00_00/goat'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

CRAWL_ID=''
GCS_BLOB_TIMESTAMP=''