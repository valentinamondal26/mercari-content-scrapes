import scrapy
import random
from datetime import datetime

from puma.items import PumaItem

class PumaSpider(scrapy.Spider):
    name = 'puma'
    start_urls = [
        'https://us.puma.com/en/us/womens/shoes/running-%2B-training'
    ]

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = "Womens Sneakers"
    brand = "PUMA"
    source = 'puma.com'
    blob_name = 'puma.txt'

    def crawlDetail(self, response):
        item = response.meta['item']
        item['style'] = response.xpath(u'normalize-space(//div[@class="mb-3"]//span[@class="product-style-number"]/@data-style-number)').extract_first() 
        item['description'] = response.xpath(u'normalize-space(//div[@class="row details"]//p/text())').extract_first()
        if item['description'] is None or item['description'] is '':
            item['description'] = response.xpath(u'normalize-space(//div[@class="row details"]//h3/following-sibling::text())').extract_first()
        yield item


    def parse(self, response):
        for product in response.selector.xpath('//div[@class="grid-tile col-6 col-sm-4 col-md-3"]'):

            item = PumaItem()

            item['title'] = product.xpath('.//a[@class="link"]/text()').extract_first()
            item['price'] = product.xpath('.//div[@class="price"]/span[@class="sales"]/span[@class="value"]/text()').extract_first()
            item['id'] = product.xpath('.//a[@class="link"]/@href').extract_first()
            item['image'] = product.xpath('.//img[@class="tile-image"]/@src').extract_first()

            item['crawlDate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['statusCode'] = "200"
            item['category'] = self.category
            item['brand'] = self.brand

            if item['id']:
                item['id'] = 'https://us.puma.com' + item['id']
                request = self.createRequest(url=item['id'], callback=self.crawlDetail)
                request.meta['item'] = item
                yield request

        nextLink = response.xpath('//div[@class="show-more"]//button[@class="btn btn-outline-primary show-more-button col-12 col-sm-5 col-md-4 mr-sm-2 mb-3"]/@data-url').extract_first()
        if nextLink:
            yield self.createRequest(url=nextLink, callback=self.parse)
        

    def createRequest(self, url, callback):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request
