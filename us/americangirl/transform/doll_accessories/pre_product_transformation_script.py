import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('sale_price', '')
            price = price.replace("$", "").strip()

            msrp = record.get('list_price', '')
            msrp = msrp.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'#\d+|new|award winner|exclusive|™|®', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            width = ''
            height = ''
            length = ''
            dimensions = record.get('dimensions', '')
            match = re.findall(re.compile(r'W:(.*?)\" x H:(.*?)\" x D:(.*?)\"', re.IGNORECASE), dimensions)
            if match and len(match) > 0:
                width = match[0][0].strip()+'"'
                height = match[0][1].strip()+'"'
                length = match[0][2].strip()+'"'

            if re.findall(re.compile(r'girls', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,

                "title": record.get('title', ''),

                'character': record.get('character', ''),
                'age': record.get('age', ''),
                "model": model,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "msrp": {
                    "currencyCode": "USD",
                    "amount": msrp
                },

                "length": length,
                "width": width,
                'height': height,
            }

            return transformed_record
        else:
            return None
