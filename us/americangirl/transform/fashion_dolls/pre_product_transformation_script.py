import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('sale_price', '')
            price = price.replace("$", "").strip()

            msrp = record.get('list_price', '')
            msrp = msrp.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'new|award winner|\& Book|\& Paperback|™', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'doll\s+book', re.IGNORECASE), 'Doll and Book', model)
            model = re.sub(' +', ' ', model).strip()

            product_line = record.get('product_line', '')

            if re.findall(re.compile(r'less', re.IGNORECASE), model) and \
                re.findall(re.compile(r'bitty', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,

                "title": record.get('title', ''),

                'character': record.get('character', ''),
                'product_line': product_line,
                'age': record.get('age', ''),
                "model": model,
                'hair_color': ','.join(record.get('hair_color', '').split('|')),
                'eye_color': ','.join(record.get('eye_color', '').split('|')),
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "msrp": {
                    "currencyCode": "USD",
                    "amount": msrp
                },
            }

            return transformed_record
        else:
            return None
