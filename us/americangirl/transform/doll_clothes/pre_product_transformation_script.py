import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('sale_price', '')
            price = price.replace("$", "").strip()

            msrp = record.get('list_price', '')
            msrp = msrp.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'™|®', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            if re.findall(re.compile(r"(?:\,|\&|and|for)+\s(?:Girls*|Little Girls*|women)+", re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,

                "title": record.get('title', ''),

                'character': record.get('character', ''),
                'age': record.get('age', ''),
                "model": model,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "msrp": {
                    "currencyCode": "USD",
                    "amount": msrp
                },
            }

            return transformed_record
        else:
            return None
