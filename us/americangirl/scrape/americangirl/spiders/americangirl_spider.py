'''
https://mercari.atlassian.net/browse/USCC-368 - American Girl Doll Clothes
https://mercari.atlassian.net/browse/USCC-369 - American Girl Fashion Dolls
https://mercari.atlassian.net/browse/USCC-371 - American Girl Doll Accessories
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import json

from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from common.selenium_middleware.http import SeleniumRequest
from selenium.webdriver.support import expected_conditions as EC

class AmericangirlSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from americangirl.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Doll Clothes
        2) Fashion Dolls
        3) Doll Accessories

    brand : str
        brand to be crawled, Supports
        1) American Girl

    Command e.g:
    scrapy crawl americangirl -a category='Doll Clothes' -a brand='American Girl'
    scrapy crawl americangirl -a category='Fashion Dolls' -a brand='American Girl'
    scrapy crawl americangirl -a category='Doll Accessories' -a brand='American Girl'
    """

    name = 'americangirl'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'americangirl.com'
    blob_name = 'americangirl.txt'

    api_query_params = 'AGCategory={ag_category}&count={count}&page=1&price=sale&plp=true'

    detail_page_base_url = 'https://www.americangirl.com/shop/p/'

    url_dict = {
        'Doll Clothes': {
            'American Girl': 'https://www.americangirl.com/shop/c/clothing',
        },
        'Fashion Dolls': {
            'American Girl': 'https://www.americangirl.com/shop/c/dolls',
        },
        'Doll Accessories': {
            'American Girl': 'https://www.americangirl.com/shop/c/furniture-accessories',
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            return

        super(AmericangirlSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.createDynamicRequest(url=self.launch_url, callback=self.parse)


    def crawl_detail(self, response):
        """
        @url https://sp1004f984.guided.ss-omtrdc.net/?index=prod&do=json_sayt&AGCategory=clothing&count=158&page=1&price=sale&plp=true
        @meta {"use_proxy":"True","item_index_to_be_checked":4}
        @scrape_values title character product_line age list_price sale_price hair_color eye_color model_sku dimensions
        """
        res = json.loads(response.text)
        for resultset in res.get('resultsets', []):
            if resultset.get('name', '') == 'default':
                for product in resultset.get('results', []):
                    yield {
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': str(response.status),
                        'category': self.category,
                        'brand': self.brand,
                        'id': self.detail_page_base_url + product.get('url'),

                        'image': product.get('imageLink', ''),

                        'title': product.get('title', ''),
                        'character': product.get('Character', ''),
                        'product_line': product.get('category', ''),
                        'age': product.get('LegalAge', ''), # or product.get('MarketingAge', ''),
                        'list_price': product.get('list_price', ''),
                        'sale_price': product.get('sale_price', ''),
                        'hair_color': product.get('DollHairColor', ''),
                        'eye_color': product.get('DollEyeColor', ''),
                        'model_sku': product.get('PartNumber', ''),
                        'dimensions': product.get('ProductDimensions', '')
                    }


    def parse(self, response):
        """
        @url https://www.americangirl.com/shop/c/clothing
        @meta {"use_selenium":"True"}
        @returns requests 1
        """
        self.base_url = response.xpath('//input[@id="snpEP"]/@value').extract_first(default='')
        self.ag_category = response.xpath('//ul[@id="product-grid-container"]/@data-snp-param').extract_first(default='').replace('AGCategory=', '')
        count = response.xpath('//span[@class="total-itemcnt-inner"]')
        print("count_element:",count)
        total = count.xpath('./text()').extract_first(default='')

        print('base_url:{}, ag_category:{}, total:{}'.format(self.base_url, self.ag_category, total))
        if self.base_url and self.ag_category and total:
            url = self.base_url + self.api_query_params.format(ag_category=self.ag_category, count=total)
            yield self.create_request(url=url, callback=self.crawl_detail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request


    def createDynamicRequest(self, url, callback, wait_time=None,wait_until=None,errback=None, meta=None):
        request = SeleniumRequest(url=url, wait_time=wait_time, wait_until=wait_until, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request
