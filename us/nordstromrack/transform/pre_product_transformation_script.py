import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = str(record.get('price', ''))
            price = price.replace("$", "").strip()

            product_type = record.get('product_type', '')
            description = ''
            match = re.findall(re.compile(r'(.*?)Sizing:', re.IGNORECASE|re.DOTALL), record.get('description', ''))
            if match:
                description = match[0].strip()

            style = ''
            match = re.findall(re.compile(r'\n\n-(.*?)\n', re.IGNORECASE), record.get('description', ''))
            if match:
                style = match[0]

            material = record.get('material', '')
            material = re.sub(re.compile(r'\bUpper\b|\bOutsole\b|\bSole\b|\bliner\b|\bLining\b|\band\b', re.IGNORECASE), '', material)
            material = material.replace('/', ',')
            material = ', '.join([m.strip() for m in material.split(',')])
            material = re.sub(r'\s+', ' ', material).strip()

            color = record.get('color_family', '') or record.get('color', '')
            color = re.sub(r'\bMulti\b', 'Multicolor', color, flags=re.IGNORECASE)

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bSneaker\b|\bSneakers\b|\bsandals\b|\bsandal\b|- Discontinued|\b'+product_type+r'\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\bzcloud\b', re.IGNORECASE), 'Z Cloud', model)
            model = re.sub(re.compile(r'\bZVolv\b', re.IGNORECASE), 'Z Volv', model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r'Flip-Flops', re.IGNORECASE), product_type):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                'image': record.get('image', ''),

                "model": model,
                "description": description,
                "color": color,
                "material": material,
                'product_type': product_type,
                'style': style,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
