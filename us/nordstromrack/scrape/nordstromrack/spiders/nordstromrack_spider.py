'''
https://mercari.atlassian.net/browse/USDE-1073 - Chaco Sandals
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json
from urllib.parse import urlparse, parse_qs, urlencode

class NordstromrackSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from nordstromrack.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Sandals
    
    brand : str
        brand to be crawled, Supports
        1) Chaco

    Command e.g:
    scrapy crawl nordstromrack -a category='Sandals' -a brand='Chaco'
    """

    name = 'nordstromrack'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'nordstromrack.com'
    blob_name = 'nordstromrack.txt'

    url_dict = {
        'Sandals': {
            'Chaco': {
                'url': 'https://www.nordstromrack.com/shop/Women/Shoes/Sandals?brands%5B%5D=Chaco&sort=most_popular'
            }
        }
    }

    base_url = 'https://www.nordstromrack.com'

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(NordstromrackSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://www.nordstromrack.com/shop/Women/Shoes/Sandals?brands%5B%5D=Chaco&sort=most_popular
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        script = response.xpath('//script[contains(text(), "window.__INITIAL_STATE__ =")]/text()').extract_first()
        j = json.loads(script.replace('window.__INITIAL_STATE__ =', ''))
        filters = j.get('catalog', {}).get('filters', {})
        url = j.get('catalog', {}).get('catalogUrlBase', '')
        if url:
            parts = urlparse(url)
            query_dict = parse_qs(parts.query)
            filters.pop('context', '')
            filters.pop('subclass', '')
            filters.pop('query', '')
            query_dict.update(filters)
            url = self.base_url + parts._replace(query=urlencode(query_dict, True)).geturl()
            request = self.create_request(url=url, callback=self.parse_listing_page, meta={'dont_obey_robotstxt': True, })
            yield request


    def parse_listing_page(self, response):
        """
        @url https://www.nordstromrack.com/api/ecomcp/public/pages/b7f14d92-4ec3-4729-9a9c-4f5ba3f8471e/catalog?includeFlash=True&includePersistent=True&nestedColors=False&site=nordstromrack&brands=Chaco&class=Sandals&department=Shoes&division=Women&limit=99&page=1&sort=most_popular
        @meta {"use_proxy":"True","scrapy_check":"True"}
        @scrape_values title price sale_price color color_family product_type description material id image sku style_number 
        """

        j = json.loads(response.text)
        for product in j.get('_embedded', {}).get('http://hautelook.com/rels/products', []):
            for variant in product.get('_embedded', {}).get('http://hautelook.com/rels/skus', []):
                # from pprint import pprint 
                # pprint(
                yield {
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': str(response.status),
                    'category': self.category,
                    'brand': self.brand,

                    'title': product['name'],
                    'price': variant['price_retail'],
                    'sale_price': variant['price_sale'],
                    'color': variant['color'],
                    'color_family': variant['color_family'],
                    'product_type': product['sub_class'],
                    'description': product['description'],
                    'material': product['material'],
                    #'style': '',
                    'id': 'https:'+product['_links']['alternate']['href'].replace('{?color,size}', '') + '?color={}'.format(variant['color']), #'https://www.nordstromrack.com' + variant['_links']['self']['href'],
                    'image': variant['_links']['http://hautelook.com/rels/original-images'][0]['href'].replace('{size}', 'large'),
                    'sku': variant['sku'],
                    'style_number': product['style_num'],
                    'upc': variant['upc'],
                }
                # )

        current_page = j.get('page', 0)
        if current_page and current_page < j.get('pages', 0):
            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            query_dict.update({'page':[current_page + 1]})
            url = parts._replace(query=urlencode(query_dict, True)).geturl()
            request = self.create_request(url=url, callback=self.parse_listing_page, meta={'dont_obey_robotstxt': True, })
            yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request