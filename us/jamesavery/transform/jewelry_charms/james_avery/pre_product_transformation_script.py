import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')

            # extract metal color from model
            metal_color = record.get('meta_color', '')
            product_specification = record.get('product_specifications', '')
            if metal_color == '':
                metal_color = []
                metal_color_meta = ["Sterling Silver", "14 K Gold", "Sterling Silver & Bronze",
                                    "Sterling Silver & 14 K Gold", "Bronze", "Sterling Silver & Copper"]
                for word in metal_color_meta:
                    if re.findall(re.compile(r"\b{}\b".format(word), re.IGNORECASE), product_specification) != []:
                        metal_color.append(word)
                metal_color = ', '.join(metal_color)
            metal_color = re.sub(re.compile(
                r"\bSterling Silver\, Bronze\b", re.IGNORECASE), "Sterling Silver & Bronze", metal_color)

            # extract gemstone from model
            gemstone = record.get('Gemstone', '')
            if gemstone == '':
                gemstone = []
                gemstone_meta = ["Amethyst", "Aqua Spinel", "Blue Sapphire", "Garnet",
                                 "Pink Sapphire", "Ruby", "Cubic Zirconia", "Pearl", "Turquoise", "White Sapphire"]
                for word in gemstone_meta:
                    if re.findall(re.compile(r"\b{}\b".format(word), re.IGNORECASE), product_specification) != []:
                        gemstone.append(word)
                gemstone = ', '.join(gemstone)

            if gemstone == "":
                if re.findall(re.compile(r"Avery Remembrance Cross|Floret Cross|Keepsake Heart", re.IGNORECASE), model) != []:
                    if re.findall(re.compile(r"\bwith\b", re.IGNORECASE), model) != []:
                        gemstone = re.findall(re.compile(
                            r"\bwith.*", re.IGNORECASE), model)[0]
                        gemstone = re.sub(re.compile(
                            r"\bwith\b|\bLab\-Created\b", re.IGNORECASE), "", gemstone)
                        gemstone = " ".join(gemstone.split())

            # model transformations
            if re.findall(re.compile(r"Avery Remembrance Cross|Floret Cross|Keepsake Heart", re.IGNORECASE), model) != []:
                model = re.sub(re.compile(
                    r"\bwith.*", re.IGNORECASE), "", model)

            pattern = re.compile(
                r"charm|jewelry|®|\b\,\s*Small\b|\bSilver \& Copper\b", re.IGNORECASE)
            model = re.sub(pattern, '', model)
            model = ' '.join(model.split())
            if model[len(model)-1] == "-":
                model = model[:-1]
            model = re.sub(re.compile(
                r"\,\s*Large", re.IGNORECASE), "", model)
            model = ' '.join(model.split())

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": "Jewelry Charms",
                "breadcrumb": record.get('breadcrumb', ''),
                "description": description.replace('\n', ''),
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query.replace('\n', ''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "color": record.get('color', ''),
                "product_type": record.get('Jewelry Style', ''),
                "recipient": record.get('Recipient', ''),
                "event": record.get('Occasion', ''),
                "gemstone": gemstone,
                "theme": record.get('Theme', ''),
                "metal_color": metal_color,
                "length": record.get('length', '').strip('L')
            }

            return transformed_record
        else:
            return None
