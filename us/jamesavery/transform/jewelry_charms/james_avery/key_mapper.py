#Key Mapper for albeebaby data - model combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                key_field += entity["attribute"][0]["id"]
                break
        for entity in entities:
            if "metal_color" == entity['name']:
                key_field += entity["attribute"][0]["id"]
                break

        return key_field
