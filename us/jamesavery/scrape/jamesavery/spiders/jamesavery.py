'''
https://mercari.atlassian.net/browse/USCC-299 - James Avery Jewelry Charms
'''

import scrapy
import datetime
import random
import os
import re
from scrapy.exceptions import CloseSpider


class JamesaverySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from jamesavery.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Jewelry Charms

    brand : str
        category to be crawled, Supports
        1) James Avery

    Command e.g:
    scrapy crawl jamesavery -a category="Jewelry Charms" -a brand='James Avery'

    """

    name = 'jamesavery'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com/',
        #     'http://shp-mercari-us-d00002.tp-ns.com/',
        #     'http://shp-mercari-us-d00003.tp-ns.com/',
        #     # 'http://shp-mercari-us-d00004.tp-ns.com/',
        #     'http://shp-mercari-us-d00005.tp-ns.com/',
        #     'http://shp-mercari-us-d00006.tp-ns.com/',
        #     'http://shp-mercari-us-d00007.tp-ns.com/',
        #     'http://shp-mercari-us-d00008.tp-ns.com/',
        #     'http://shp-mercari-us-d00009.tp-ns.com/',
        #     'http://shp-mercari-us-d00010.tp-ns.com/',
        #     'http://shp-mercari-us-d00011.tp-ns.com/',
        #     'http://shp-mercari-us-d00012.tp-ns.com/',
        #     'http://shp-mercari-us-d00013.tp-ns.com/',
        #     'http://shp-mercari-us-d00014.tp-ns.com/',
        #     'http://shp-mercari-us-d00015.tp-ns.com/',
        #     'http://shp-mercari-us-d00016.tp-ns.com/',
        #     'http://shp-mercari-us-d00017.tp-ns.com/',
        #     'http://shp-mercari-us-d00018.tp-ns.com/',
        #     'http://shp-mercari-us-d00019.tp-ns.com/',
        #     'http://shp-mercari-us-d00020.tp-ns.com/',
    ]

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None

    count = 1
    number_of_products = None

    source = 'jamesavery.com'
    blob_name = 'jamesavery.txt'

    base_url = 'https://www.jamesavery.com'

    url_dict = {
        "Jewelry Charms": {
            'James Avery': {
                "url": "https://www.jamesavery.com/categories/charms-and-pendants?jewelry_type[]=Charms",
                "filters": ['Jewelry Style','Recipient','Occasion','Gemstone','Theme']
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(JamesaverySpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Jewelry Style','Recipient','Occasion','Gemstone','Theme']


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self,response):
        """
        @url https://www.jamesavery.com/categories/charms-and-pendants?jewelry_type[]=Charms
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        if self.filters:
          for filter in self.filters:

            filter_urls=response.xpath('//div[@data-filters-accordion=""]/h3[contains(text(),"{}")]/following-sibling::div[@data-filter-section-name="{}"]/ul/li/a/@href'.format(filter,filter.replace(' ','_').lower())).getall()
            filter_urls=[self.base_url+link if "https" not in link else link for link in filter_urls ]
            filter_values=response.xpath('//div[@data-filters-accordion=""]/h3[contains(text(),"{}")]/following-sibling::div[@data-filter-section-name="{}"]/ul/li/@data-filter-value-name'.format(filter,filter.replace(' ','_').lower())).getall()
            filter_values=[val.strip() for val in filter_values]
            for url in filter_urls:
                meta={}
                index=filter_urls.index(url)
                meta=meta.fromkeys([filter],filter_values[index])
                meta.update({'filter_name':filter})
                yield self.createRequest(url=url,callback=self.parse,meta=meta)


    def parse(self, response):
            """
            @url https://www.jamesavery.com/categories/charms-and-pendants?jewelry_type%5B%5D=Charms&recipient%5B%5D=For+Her&sort%5B%5D=featured&sort%5B%5D=top_sellers
            @returns requests 19
            ### 18 products and 1 next page url so totally 19 requests
            """

            all_links=response.xpath('//a[@class="product-summary__media-link"]/@href').getall()
            all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
            for link in all_links:
                yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)

            next_page_url=response.xpath('//div[@class="pagination__button"]/a/@href').get(default='')
            if next_page_url!='':
                next_page_url=[self.base_url+link if "https" not in link else link for link in [next_page_url]]
                next_page_url=next_page_url[0]
                yield self.createRequest(url=next_page_url,callback=self.parse,meta=response.meta)


    def parse_product(self,response):
        """
        @url https://www.jamesavery.com/products/enamel-american-flag-charm?via=Z2lkOi8vamFtZXNhdmVyeS9Xb3JrYXJlYTo6Q2F0YWxvZzo6Q2F0ZWdvcnkvNTk5NTAwOTI2MTcwNzAzYWUyMDAwMDBh
        @scrape_values breadcrumb image price title meta_color length description color
        """

        item={}

        item["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"]=str(response.status)
        item['id']=response.url
        item['breadcrumb']='|'.join(response.xpath('//p[@class="breadcrumbs__node-group"]//span//a//text()').getall())
        item['brand']=self.brand
        item['image']=response.xpath('//picture[@class="product-details__primary-image-button-image"]//img/@src').get(default='')
        item['category']=self.category
        item['price']=' '.join(response.xpath('//div[@class="product-details__info"]//p[@class="product-prices__price"]/span//text()').getall()).strip()
        item['title']=response.xpath('//h1[@class="product-details__name"]/text()').get(default='')
        item['meta_color']=', '.join(response.xpath('//div[@class="product-options"]//li/a/@title').getall())
        product_specifications=', '.join(response.xpath('//div[contains(text(),"Product Specifications")]/following-sibling::ul//li/text()').getall())
        pattern=re.compile(r"\,\s*[\d\/\.\"]*\s*long",re.IGNORECASE)
        length=''
        if re.findall(pattern,product_specifications)!=[]:
            length=re.findall(pattern,product_specifications)[0].strip(", long")
        item['length']=length
        item['description']=response.xpath('//div[@id="description"]/p/text()').get(default='')
        item['color']=', '.join(response.xpath('//label[@class="product-options__label"][span[contains(text(),"Select a Color")]]/following-sibling::ul//li/input/@value').getall())

        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)
        yield item


    def createRequest(self, url, callback,meta=None,headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key,value in headers.items():
                request.headers[key]=value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request