import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            model=re.sub(re.compile(r"tieks*",re.IGNORECASE),'',model)
            model=' '.join(model.split())
            product_line=record.get('product_type','')
            if product_line =="Featured":
                product_line=record.get('product_line','')
            

        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category":"Women's Flats",
                "brand": 'Tieks',
                "ner_query":ner_query.strip().strip('.'),
                "product_line":product_line,
                "title":record.get('title',''),
                "model":model,
                "material":record.get("material",''),
                "description": record.get('description', '').strip().strip('.'),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None


