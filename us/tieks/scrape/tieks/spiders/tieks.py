'''
https://mercari.atlassian.net/browse/USCC-103 - Tiek Women's Flats
'''

import scrapy
import datetime
import random
import os
from scrapy.exceptions import CloseSpider

class TieksSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from tieks.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Flats

    brand : str
        category to be crawled, Supports
        1) Tiek

    Command e.g:
    scrapy crawl tieks -a category="Women's Flats" -a brand='Tiek'

    """

    name = 'tieks'

    start_urls = [
        'https://www.tieks.com'
    ]

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None
    launch_url=None
    source = 'tieks.com'
    blob_name = 'tieks.txt'
    base_url = 'https://tieks.com/boutiek/'

    url_dict = {
        "Women's Flats": {
            'Tiek': {
                'url': 'https://tieks.com/boutiek/',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TieksSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://tieks.com/boutiek/
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        product_type_dict = {}
        names_list = response.xpath('//div[@class="boutiek-subcategory"]/span/text()').getall()
        product_type_dict = product_type_dict.fromkeys(names_list,[])
        nodes = response.xpath('//div[@class="listing-type-grid catalog-listing"]//*')
        ctr = 0
        found = 0
        ols = []
        for node in nodes:
            if node.css('.grid-row'):
                if found == 0:
                    found = 1
                ols.append(node)
            elif "boutiek-subcategory" in node.get() and found == 1 and "item " not in node.get():
                product_type_dict[names_list[ctr]] = ols
                ctr+=1
                found = 0
                ols = []

        #product_type_dict[names_list[ctr-1]]=ols
        for key,value in product_type_dict.items():
            for ol in value:
                links = ol.xpath('./li/p/a/@href').getall()
                for link in links:
                    if "http" in link:
                        yield self.createRequest(url=link, callback=self.parse_product,
                            meta={'product_type':key})
                    else:
                        yield self.createRequest(url=self.base_url+link, callback=self.parse_product,
                            meta={'product_type':key})


    def parse_product(self, response):
        """
        @url https://tieks.com/poppy.html
        @scrape_values id product_type title price description image material product_line
        """

        yield {
            'crawlDate': str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            'statusCode': "200",
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'product_type': response.meta.get("product_type"),
            'title': response.xpath('normalize-space(//h1[@class="product-name"]/text())').get(default=''),
            'price': response.xpath('normalize-space(//span[@class="price"]/text())').get(default=''),
            'description': '.'.join(response.xpath('//li[@class="open-description info"]//text()').getall()),
            'image': response.xpath('//div[@class="product-img-box"]//img/@src').get(default=''),
            'material': response.xpath('//li[@class="open-description info"]//ul/li/text()').get(default=''),
            'product_line': response.xpath('//div[@class="info-container"]/h3/text()').get(default=''),
        }


    def createRequest(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
