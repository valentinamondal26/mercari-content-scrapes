class Mapper:
    def map(self, record):
        product_type = ''
        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "product_type" == entity['name']:
                product_type = entity["attribute"][0]["id"]
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            if "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = product_type + model + color
        return key_field
