import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = ''
            color_meta = [
                'White', 'Black', 'Stainless Steel', 'White/Stainless Steel'
            ]
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color_meta))), record.get('title', ''))
            if match:
                color = ', '.join(match)

            model = record.get('title', '')
            if not color:
                model = re.sub(r'\b'+color+r'\b', '', model, flags=re.IGNORECASE)

            model = re.sub(r'\(|\)|S\/S|white\/SS|Stainless Steel', '', model, flags=re.IGNORECASE)
            model = re.sub(r'Tumbler Create Your Own', 'Personalized Tumbler', model, flags=re.IGNORECASE)
            model = re.sub(r'w\/Domed', 'Domed', model, flags=re.IGNORECASE)
            model = re.sub(r'With Domed', 'Domed', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                'image': record.get('image', ''),

                "model": model,
                "color": color,
                "product_type": record.get('product_type', ''),
                'release_year': record.get('year', ''),
            }

            return transformed_record
        else:
            return None
