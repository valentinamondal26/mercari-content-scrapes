'''
https://mercari.atlassian.net/browse/USCC-527 - Starbucks Coffee & Tea Accessories
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import re

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from common.selenium_middleware.http import SeleniumRequest


class StarbucksSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from starbucks.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Coffee & Tea Accessories

    brand : str
        brand to be crawled, Supports
        1) Starbucks

    Command e.g:
    scrapy crawl starbucks -a category='Coffee & Tea Accessories' -a brand='Starbucks'
    """

    name = 'starbucks'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'starbucks.com'
    blob_name = 'starbucks.txt'

    url_dict = {
        'Coffee & Tea Accessories': {
            'Starbucks': {
                'url': [
                    'https://www.starbucks.co.id/coffeehouse/merchandise/drinkware',
                    'https://www.starbucks.co.id/coffeehouse/merchandise/cold-drinkware',
                    'https://www.starbucks.co.id/coffeehouse/merchandise/machines',
                    'https://www.starbucks.co.id/coffeehouse/merchandise/accessories',
                    'https://stories.starbucks.com/stories/2017/20-years-of-starbucks-holiday-cups/',
                ]
            }
        }
    }

    base_url = 'https://www.starbucks.co.id'

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(StarbucksSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f'Crawling for category: {category} and brand: {self.brand}, url: {self.launch_url}')


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            for url in self.launch_url:
                if 'stories' in url:
                    yield self.createDynamicRequest(url=url, callback=self.parse_stories)
                else:
                    yield self.create_request(url=url, callback=self.parse)


    def parse_stories(self, response):
        product_type = ''
        if re.findall('holiday-cups', response.url):
            product_type = 'Holiday Cup'

        for product in response.xpath('//div[@id="content-wrap"]//div[@class="article__content"]/h3'):
            year = product.xpath('./text()').extract_first()
            title = f'{product_type} {year}'
            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': f'{response.url}#{title}',

                'title': title,
                'image': product.xpath('./../following-sibling::div[@class="article__content"][1]/p/img/@src').extract_first(),
                'product_type': product_type,
                'year': year,
                'description': product.xpath('./../following-sibling::div[@class="article__content"][2]/p/text()').extract_first(),
            }


    def parse(self, response):
        product_type = response.xpath('//h1[@class="articleBlock__header"]/text()').extract_first()
        for product in response.xpath('//div[@class="menuAssetGroup"]/div[@class="menuAsset"]'):
            title = product.xpath('./span[@class="menuAsset__linkText"]/text()').extract_first()
            image = product.xpath('.//img/@src').extract_first()
            if not image.startswith(self.base_url):
                image = self.base_url + image
            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': f'{response.url}#{title}',

                'title': title,
                'image': image,
                'product_type': product_type,
            }


    def createDynamicRequest(self, url, callback, errback=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, 
            wait_time = 60,
            wait_until = EC.visibility_of_element_located((By.XPATH, '//div[@id="content-wrap"]//div[@class="article__content"]/h3'))
        )
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
