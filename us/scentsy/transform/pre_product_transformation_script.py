'''
https://mercari.atlassian.net/browse/USCC-266 - 
https://mercari.atlassian.net/browse/USCC-240 - 
https://mercari.atlassian.net/browse/USCC-225 - 
https://mercari.atlassian.net/browse/USCC-203 - 
https://mercari.atlassian.net/browse/USCC-136 - 
https://mercari.atlassian.net/browse/USCC-132 - 
https://mercari.atlassian.net/browse/USCC-81 - 
https://mercari.atlassian.net/browse/USCC-80 - 
https://mercari.atlassian.net/browse/USCC-78 - 
'''

import re
import html
import hashlib

class Mapper(object):
    def __init__(self):
        self.products = ['Warmer', 'Travel Tin', 'Fragrance Flower', 'Aroma Porcelain', 'Room Spray',
                         'Scent Circle', 'Scent Pak', 'Car Bar Clip', 'Car Bar', 'Bar']
        self.synonyms = {'Bar' : 'Wax Bar'}
        self.url_prefix = 'https://scentsy.com{}'
        self.brand = 'Scentsy'
        self.category = 'Home Fragrances'
        self.transformed_record = {}

    def map(self, record):
        if record:

            price = record['price']
            price = price[1:]
            description = record.get('longDescription', '')
            description = self.remove_html_tags(description)
            model,product_type = self.extract_model_product(record)
            id = self.url_prefix.format(record['productModelUrl'])
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            self.transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "category": self.category,
                "brand": self.brand,
                "product_type": product_type,
                "sku": record['sku'],
                "description": description,
                "title": record['displayName'],
                "model": model,
                "price": {
                    "currency_code": "USD",
                    "amount": str(price)
                }
            }

            self.extract_model_product(record)
            self.extract_dimensions(description, self.transformed_record)
            self.extract_finish(description, self.transformed_record)
            self.extract_includes(description, self.transformed_record)
            self.extract_wattage(description, self.transformed_record)

            return self.transformed_record
        else:
            return None

    def extract_model_product(self, record):
        model = record['displayName']
        product_type = ''
        for product in self.products:
            if product in model:
                product_type = product if not self.synonyms.get(product) else self.synonyms.get(product)
                model = model[: model.index(product)]
                model = self.post_process(model)

        return model, product_type
    
    def remove_html_tags(self,text):
        """Remove html tags from a string"""
        import re
        clean = re.compile('<.*?>')
        cleaned_text = re.sub(clean, '', text)
        return cleaned_text

    def extract_wattage(self, description, transformed_record):
        wattage_regex = r'<p>Wattage:\s[0-9]*W<\/p>'
        wat = re.findall(wattage_regex, description)
        wattage = ''
        if wat:
            w = wat[0]
            wattage = w[w.index(':') + 1: w.index('</p>')]
            transformed_record['wattage'] = self.post_process(wattage)
        return transformed_record

    def extract_dimensions(self, description, transformed_record):
        dim_regex = r'<p>Dimensions:\s.*;<\/p>'
        dim = re.findall(dim_regex, description)
        dimensions = ''
        if dim:
            d = dim[0]
            dimensions = d[d.index(':') + 1:d.index(';')]
            dimensions = html.unescape(dimensions)
            transformed_record['dimension'] = self.post_process(dimensions)
        return transformed_record

    def extract_finish(self, description, transformed_record):
        finish_regex = r'<p>Finish:\s[a-zA-Z]*<\/p>'
        fin = re.findall(finish_regex, description)
        finish = ''
        if fin:
            f = fin[0]
            finish = f[f.index(':') + 1: (f.index('</p>'))]
            transformed_record['finish'] = self.post_process(finish)
        return transformed_record

    def extract_includes(self, description, transformed_record):
        includes_regex = r'<p>Includes: .*<\/p>'
        incl = re.findall(includes_regex, description)
        includes = ''
        if incl:
            i = incl[0]
            includes = i[i.index(':') + 1: i.index('</p>')]
            transformed_record['includes'] = self.post_process(includes)
        return transformed_record

    def  post_process(self, entity):
        entity = entity.strip()
        entity = re.sub(re.compile(r'Scentsy|®|- | –', re.IGNORECASE),'', entity)
        entity = entity.rstrip('-')
        return entity.strip()

if __name__ == '__main__':
    #record = {"collectionId":0,"discount":"$0.00","displayName":"Apple & Cinnamon Sticks Aroma Porcelain","displayPercentOff":False,"fragranceFamilyId":0,"hasSavings":False,"id":36606,"imageUrl":"https://imagelive.scentsy.com:443/cmsimages/products/r12scentaromaporcelainappleandcinnamonsticksdishporcelainpieces101isofw19.png","isAvailableInMultipackOnly":False,"isBackordered":False,"isMultiPack":False,"isNewMultiPack":False,"materialGroup":"1291","isBuddy":False,"isOutOfStock":False,"isSalePrice":False,"isSubscribable":False,"longDescription":"<p>Our new Aroma Porcelain delivers beautiful, holiday-ready fragrance to your favorite small spaces, without the need for an outlet. Simply arrange the pieces on the included six-inch decorative plate, add the fragrance oil and enjoy the gorgeous scent for up to 90 days.</p><p>APPLE PEEL, spiced WHITE PUMPKIN and a touch of OAK.</p><p>Includes: 3 porcelain pieces, 1 decorative plate and 1 bottle of fragrance oil.</p>","metadata":None,"multipackSkusThatContainsProduct":[],"percentOff":0,"price":"$25.00","productModelUrl":"/shop/p/57042/apple-and-cinnamon-sticks-aroma-porcelain","productProperties":[{"active":False,"id":88566,"productId":36606,"productPropertyType":{"allowMultiple":False,"dataType":"int","displayOnSite":False,"enforceUnique":False,"imageThumb":"","manageValues":False,"name":"BackOrderBehavior","productPropertyTypeId":1,"required":True,"termDescription":"ppt_BackOrderBehavior_Description","termName":"ppt_BackOrderBehavior"},"productPropertyTypeId":1,"productPropertyValue":{"dataType":"int","imageThumb":"","name":"AllowBackOrderDontNotify","productPropertyTypeId":1,"productPropertyValueId":3,"termDescription":"ppv_BackOrderBehavior_AllowBackOrderDontNotify_Description","termName":"ppv_BackOrderBehavior_AllowBackOrderDontNotify","value":3}},{"active":False,"id":88567,"productId":36606,"productPropertyType":{"allowMultiple":False,"dataType":"bit","displayOnSite":False,"enforceUnique":False,"imageThumb":"","manageValues":False,"name":"HideAutomaticallyAddedChildProducts","productPropertyTypeId":2,"required":False,"termDescription":"ppt_HideAutomaticallyAddedChildProducts_Description","termName":"ppt_HideAutomaticallyAddedChildProducts"},"productPropertyTypeId":2,"productPropertyValue":{"dataType":"bit","imageThumb":"","name":"True","productPropertyTypeId":2,"productPropertyValueId":10,"termDescription":"ppv_HideAutomaticallyAddedChildProducts_True_Description","termName":"ppv_HideAutomaticallyAddedChildProducts_True","value":1}}],"productTypeId":711,"salePrice":"$25.00","savingsString":"","shortDescription":"","sku":"57042","shouldShowOriginalSalePrice":True}
    record = {"collectionId":0,"discount":"$0.00","displayName":"Aloe Water & Cucumber Scentsy Bar","displayPercentOff":False,"fragranceFamilyId":0,"hasSavings":False,"id":30081,"imageUrl":"https://imagelive.scentsy.com:443/cmsimages/products/46199r1barlabelaloewatercucumber600x600.png","isAvailableInMultipackOnly":False,"isBackordered":False,"isMultiPack":False,"isNewMultiPack":False,"materialGroup":"1150","isPresell":False,"isBuddy":False,"isOutOfStock":False,"isSalePrice":False,"isSubscribable":False,"longDescription":"<p>Aloe water, cucumber peel, palm frond and pineapple nectar are oh-so-mellow.</p>\n\n<p>Made of high-quality paraffin wax for long-lasting fragrance, Scentsy Bars are composed of eight break-apart cubes designed to use with any Scentsy Warmer. As the cubes melt, they fill your space with our exclusive scents, inspiring imagination and memories. Safe to use &mdash; no wick, flame, smoke or soot. Made in Idaho, USA. Net wt 2.6 oz.</p>","metadata":None,"multipackSkusThatContainsProduct":[],"percentOff":0,"price":"$6.00","productModelUrl":"/shop/p/46199/aloe-water-and-cucumber-scentsy-bar","productProperties":[{"active":False,"id":73529,"productId":30081,"productPropertyType":{"allowMultiple":False,"dataType":"int","displayOnSite":False,"enforceUnique":False,"imageThumb":"","manageValues":False,"name":"BackOrderBehavior","productPropertyTypeId":1,"required":True,"termDescription":"ppt_BackOrderBehavior_Description","termName":"ppt_BackOrderBehavior"},"productPropertyTypeId":1,"productPropertyValue":{"dataType":"int","imageThumb":"","name":"AllowBackOrderDontNotify","productPropertyTypeId":1,"productPropertyValueId":3,"termDescription":"ppv_BackOrderBehavior_AllowBackOrderDontNotify_Description","termName":"ppv_BackOrderBehavior_AllowBackOrderDontNotify","value":3}},{"active":False,"id":73530,"productId":30081,"productPropertyType":{"allowMultiple":False,"dataType":"bit","displayOnSite":False,"enforceUnique":False,"imageThumb":"","manageValues":False,"name":"HideAutomaticallyAddedChildProducts","productPropertyTypeId":2,"required":False,"termDescription":"ppt_HideAutomaticallyAddedChildProducts_Description","termName":"ppt_HideAutomaticallyAddedChildProducts"},"productPropertyTypeId":2,"productPropertyValue":{"dataType":"bit","imageThumb":"","name":"False","productPropertyTypeId":2,"productPropertyValueId":11,"termDescription":"ppv_HideAutomaticallyAddedChildProducts_False_Description","termName":"ppv_HideAutomaticallyAddedChildProducts_False","value":0}}],"productTypeId":7,"salePrice":"$6.00","savingsString":"","shortDescription":"","sku":"46199","shouldShowOriginalSalePrice":True}
    print(Mapper().map(record))