import scrapy
from scrapy import Request
import datetime
import random
import re
import os
from scrapy.exceptions import CloseSpider


class LeprixSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from leprix.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags

    brand : str
        category to be crawled, Supports
        1) Coach

    Command e.g:
    scrapy crawl leprix -a category='Shoulder Bags' -a brand='Coach'
    """

    name = 'leprix'

    category = None
    brand = None

    source = 'leprix.com'
    blob_name = 'leprix.txt'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]
    start_urls=["https://leprix.com/shop/brand/coach?pp=36&s=featured&p=1&category=6%3A68&timestamp=1562797874311"]
    
    url_dict = {
        'Shoulder Bags' : {
            'Coach': {
                'url': 'https://leprix.com/shop/brand/coach?pp=36&s=featured&p=1&category=6%3A68&timestamp=1562797874311',
            }
        }
    }



    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(LeprixSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    
    def start_requests(self):
        if self.launch_url:
            yield Request(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://leprix.com/shop/brand/coach?pp=36&s=featured&p=1&category=6%3A68&timestamp=1562797874311
        @returns requests 37
        @meta {"use_proxy":"True"}
        @headers {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"}
        """

        # Number of request mentioned in contracts is 37 beacause 36 items per page and 1 Next Page url
        needed_script=response.css('script:contains("window.criteo_q")::text').get()
        item_links=[]
        item_list=[]
        if needed_script:
            needed_script=re.findall("item: (.+?);\n", needed_script, re.S)
            if needed_script:
                items=needed_script[0].replace('"',"").replace("[","").replace("]","").split(",")
                item_list=[x.strip() for x in items]
                item_list[len(item_list)-1]=item_list[len(item_list)-1].split('\n')[0]
          
        for item in item_list:
            item_links.append(response.css('#{} a::attr(href)'.format(item)).get())

        next_page_url=response.css('a[rel="next"]::attr(href)').get()

        if item_links:
            for link in item_links:
                yield self.createRequest(link,self.parse_products)

        if next_page_url:
            yield self.createRequest(next_page_url,self.parse)


    def parse_products(self,response): 
        """
        @url https://leprix.com/shop/coach/shoulder-bags/coach-dufflette-in-signature-jacquard-shoulder-bag-12l-x-9h-x-4w
        @meta {"use_proxy":"True"}
        @headers {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"}
        @scrape_values title model price image breadcrumb description measurements color condition est_retail
        """

        title=''
        model=''
        price=''
        image=''
        breadcrumb=''
        description=''
        measurements=''
        color=''
        condition=''
        
        description=response.xpath('//div[@class="description"]//label[contains(text(),"Description")]/following-sibling::node()/div//text()').getall()
        description=' '.join(description)
        measurements=response.xpath('//div[@class="description"]//label[contains(text(),"Size & Fit")]/following-sibling::node()//p//text()').getall()
        measurements=' '.join(measurements)
        color=response.xpath('//div[@class="description"]//label[contains(text(),"Color")]/following-sibling::node()//p//text()').getall()
        color=' '.join(color)
        condition=response.xpath('//div[@class="description"]//label[contains(text(),"Condition")]/following-sibling::node()//p//text()').getall()
        condition=' '.join(condition)  
        model=response.xpath('//p[strong[contains(text(),"Item Number")]]/text()').get()
        if len(model)>1:
            model=model.split(':')[1].strip()
            
        breadcrumb_li=response.css('ol.breadcrumb a::text').getall()
        breadcrumb_li.append(response.css('ol.breadcrumb li::text').get())
        breadcrumb='|'.join(breadcrumb_li)

        title=response.css('h1.item-name::text').get()  
        price=response.css('span.product-price::text').get()
        image=response.css('img#mainImage::attr(src)').get()
        est_retail=response.xpath('//h5[contains(text(),"est. ret.")]/text()').get()
        if est_retail is None:
            est_retail=''

        item = {}
        item['crawlDate'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item["statusCode"]=str(response.status)
        item["id"]=response.url
        item["title"]=title
        item["model"]=model
        item["brand"]=self.brand
        item["condition"]=condition
        item["description"]=description
        item["measurements"]=measurements
        item["price"]=price
        item["image"]=image
        item["breadcrumb"]=breadcrumb
        item["category"]=self.category
        item['color']=color
        item['est_retail']=est_retail
        yield item


    def createRequest(self, url, callback):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request