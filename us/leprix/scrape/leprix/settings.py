BOT_NAME = 'leprix.com'

SPIDER_MODULES = ['leprix.spiders']
NEWSPIDER_MODULE = 'leprix.spiders'

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36'

ROBOTSTXT_OBEY = False

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

COOKIES_ENABLED = False

DOWNLOAD_DELAY = 0.5
#By default auto_throttle strat - max delay is 5s to 60s
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_TARGET_CONCURRENCY = 64

FEED_FROMAT="jsonlines"

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

# GCS settings - actual value be 'raw/leprix.com/shoulder_bags/coach/2019-02-06_00_00_00/json/leprix.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10,
    'common.scrapy_contracts.contracts.Headers':10
}