import hashlib


class Mapper(object):
    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v)>0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            id = record['id']
            crawl_date = record['crawlDate']
            status_code = record['statusCode']
            category = record['category']
            brand = record['brand']
            image = record['image']
            model = record['model']
            condition = record['condition']
            breadcrumb = record['breadcrumb']
            measurements = record['measurements']
            description = record['description']
            title = record['title']
            price = record['price']
            color = record['color']
            est_retail = record['est_retail']

            ner_query = record['description'] + " "+record['condition']

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = price.replace("$", "")
            price = price.replace("\n", "")
            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawlDate": crawl_date,
                "statusCode": status_code,
                "category": category,
                "brand": brand,
                "image": image,
                "model": model,
                "condition": condition,
                "breadcrumb": breadcrumb,
                "measurements": measurements,
                "description": description,
                "ner_query": ner_query,
                "title": title,
                "color": color,
                "estimated_original_price": est_retail,
                "price":{
                    "currencyCode": "USD",
                    "amount": price
                }
            }
            transformed_record = self.post_process(transformed_record)

            return transformed_record
        else:
            return None
