import requests
import traceback
from requests import Timeout
import urllib


class Mapper(object):
    def __init__(self):
        self.url = "https://us-central1-mercari-us-de.cloudfunctions.net/entity_normalizer?query={}&entity={}"
        return

    def map(self, record):
        try:
            resolved_entities = self.resolve_entities(record['entities'])
            entity_resolved_item = {
                                      "item_id": record['item_id'],
                                      "entities": resolved_entities,
                                      "source": "leprix"
            }
        except Exception as e:
            print("Leprix Scraper Exception Raised for {} with exception {}".format(record, str(e)))
            print(traceback.format_exc())
            return None
        return entity_resolved_item

    def resolve_entities(self, raw_entities):

        resolved_entities = []

        for entity in raw_entities:
            key = next(iter(entity))
            attr = entity[key]
            if len(attr) == 0:
                continue
            resolved_entities.append(self.resolve(entity))
        return resolved_entities

    def resolve(self, entity):
        key = next(iter(entity))
        attributes = entity[key]
        resolved_entity_id = ""
        resolved_attributes = []
        entity_map = dict()
        for attr in attributes:
            attr = urllib.parse.quote(attr)
            url = self.url.format(attr, key)
            try:
                with requests.get(url, timeout=60) as r:
                    if r.status_code == requests.codes.ok:
                        res = r.json()
                        resolved_entity_id = res['entity_id']
                        attr_map = dict()
                        attr_map['id'] = res['attribute_id'] if res['attribute_id'] else ""
                        attr_map['name'] = res['attribute_name'] if res['attribute_name'] else ""
                        attr_map['value'] = res['query']
                        resolved_attributes.append(attr_map)
                        entity_map['entity'] = resolved_entity_id
                        entity_map['name'] = key
                        entity_map['attribute'] = resolved_attributes
                    else:
                        print("FAILED: Status_code {0} - {1} ".format(r.status_code, url))
            except Timeout:
                print("FAILED: Request Timed out - {}".format(url))
                print(traceback.format_exc())

        return entity_map
