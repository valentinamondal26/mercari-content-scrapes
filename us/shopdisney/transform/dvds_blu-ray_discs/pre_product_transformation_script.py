import hashlib
import html
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # model transformations
            model = record.get('title', '')
            model = re.sub(re.compile(r'\bCombo Pack\b|\bFilm\b|'
                                      r'\Digital code\b|\+ Digital Copy',
                                      re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'Bluray', re.IGNORECASE), 'Blu-ray',
                           model)
            model = html.unescape(model)
            model = re.sub(r"•", "-", model)
            model = re.sub(re.compile(r"(WALL)(E)", re.IGNORECASE), r"\1-\2",
                           model)
            model = re.sub(r"\/\/|\s(\-|\–)\s|\s(\-|\–)$|"
                           r"\s(\-|\–)|(\-|\–)\s",
                           " ", model)
            if re.findall(re.compile(r"\bdvd\b", re.IGNORECASE), model) \
                and not re.findall(re.compile(r"\bblu\s*\-*\s*ray\b|\+",
                                   re.IGNORECASE), model):
                model = re.sub(re.compile(r"(.*)(\bDVD\b)(.*)", re.IGNORECASE),
                               r"\1 \3 \2", model)

            if re.findall(re.compile(r"\bBlu\-*\s*ray\b",
                          re.IGNORECASE), model) \
                    and not re.findall(re.compile(r"\bdvd\b|\+",
                                       re.IGNORECASE), model):
                model = re.sub(re.compile(r"(.*)(\bBlu\-*\s*ray\b)(.*)",
                               re.IGNORECASE),
                               r"\1 \3 \2", model)
            model = re.sub(re.compile(r"([^\s])\s*\/\s*([^\s])",
                           re.IGNORECASE), r"\1/\2", model)
            model = re.sub(re.compile(r"\+ Digital", re.IGNORECASE), "", model)
            if not re.findall(r'Blu\-*\s*ray', model, flags=re.IGNORECASE) and not re.findall(r'\bDVD\b', model, flags=re.IGNORECASE):
                model = f'{model} DVD'

            model = re.sub(r"Blu-Ray", "Blu-ray", model)
            model = re.sub(r'\s+', ' ', model).strip(' +')

            # extract other specs
            specs = record.get('product_specification', [])
            specs.extend(record.get('extended_description', []))
            tech_specs = '\n'.join(specs)

            aspect_ratio = ''
            surround_sound_formats = ''
            runtime = ''
            rating = ''

            match = re.findall(
                    re.compile(r'Aspect Ratio:(.*?)\n', re.IGNORECASE),
                    tech_specs)
            if not match:
                match = re.findall(
                        re.compile(r'Apect Ratio:(.*?)\n', re.IGNORECASE),
                        tech_specs)

            if match:
                aspect_ratio = match[0].strip()

            match = re.findall(re.compile(r'Sound:(.*?)\n', re.IGNORECASE),
                               tech_specs)
            if match:
                surround_sound_formats = match[0].strip()

            match = re.findall(
                    re.compile(r'Running Time:(.*?)\n', re.IGNORECASE),
                    tech_specs)
            if not match:
                match = re.findall(
                        re.compile(r'Run Time:(.*?)\n', re.IGNORECASE),
                        tech_specs)
            if match:
                runtime = match[0].strip()

            match = re.findall(re.compile(r'Rating:(.*?)\n', re.IGNORECASE),
                               tech_specs)
            if match:
                rating = match[0].strip()

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawlDate": record.get('crawlDate', ''),
                    "statusCode": record.get('statusCode', ''),
                    "category": record.get('category'),
                    "brand": record.get('brand', ''),
                    "ner_query": ner_query,
                    "title": record.get('title', ''),

                    "model": model,
                    "product_type": record.get('Product Type', ''),
                    "gender": record.get('Gender', ''),
                    "color": record.get('Color', ''),
                    "age": record.get('Age', ''),
                    "character": record.get('Character', ''),
                    'series': record.get('Movie / Show', ''),

                    'franchise': record.get('Franchise', ''),
                    'description': record.get('short_description', ''),
                    'aspect_ratio': aspect_ratio,
                    'surround_sound_formats': surround_sound_formats,
                    'runtime': runtime,
                    'rating': rating,
                    'model_sku': record.get('sku', ''),

                    "msrp": {
                            "currencyCode": "USD",
                            "amount": price
                    },
            }

            return transformed_record
        else:
            return None
