import hashlib
import html
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            if isinstance(price, dict):
                price = ''
            price = price.replace("$", "").strip()

            product_type = record.get('Product Type', '')
            category = record.get('Category', '')

            specs = record.get('product_specification', [])
            specs.extend(record.get('extended_description', []))
            tech_specs = '\n'.join(specs)

            # product type extraction
            product_type_meta = ['Action Figures*', 'Buttons*', 'Canvas Arts*',
                                 'Dolls*', 'Ear Hats*', 'Figurines*',
                                 'Fine Arts*', 'Giclees*',
                                 'Journals* & Binders*',
                                 'Keepsakes*', 'Keepsakes* & Figurines*',
                                 'Lanyards*',
                                 'LEGO', 'Lithographs*', 'Medium Plush',
                                 'Patches*',
                                 'Pins*', 'Pin Sets*', 'Play Sets*',
                                 'Plush & Stuffed Animals*',
                                 'Posters* & Prints*',
                                 'Small Plush', 'Snowglobes*', 'Trains*',
                                 'Vinyl Figures*', 'Wall Arts*', 'Patched']

            if not product_type:
                product_type = []
                for word in product_type_meta:
                    if re.findall(
                            re.compile(r"\b{}\b".format(word), re.IGNORECASE),
                            record.get("title", "")) \
                            or re.findall(re.compile(r"\b{}\b".format(word),
                                          re.IGNORECASE),
                                          record.get("short_description",
                                                     "") + " " + record.get(
                                    "long_description", "")) \
                            and not product_type:
                        if word == "Patched":
                            word = "Patches"
                        product_type.append(word)
                product_type = ", ".join(product_type)
            # extract measurements
            height = ''
            width = ''
            depth = ''

            match = re.findall(re.compile(
                    r"(?:\d+)(?:\.\d+){0,1}(?:\s*\d+\/\d+){0,1}''\s*H",
                    re.IGNORECASE),
                    tech_specs)
            if match:
                height = match[0]
                height = re.sub(re.compile(
                        r"''\s*H", re.IGNORECASE), '"', height).strip()

            match = re.findall(re.compile(
                    r"(?:\d+)(?:\.\d+){0,1}(?:\s*\d+\/\d+){0,1}''\s*W",
                    re.IGNORECASE),
                    tech_specs)
            if match:
                width = match[0]
                width = re.sub(re.compile(
                        r"''\s*W", re.IGNORECASE), '"', width).strip()

            match = re.findall(re.compile(
                    r"(?:\d+)(?:\.\d+){0,1}(?:\s*\d+\/\d+){0,1}''\s*D",
                    re.IGNORECASE),
                    tech_specs)
            if match:
                depth = match[0]
                depth = re.sub(re.compile(
                        r"''\s*D", re.IGNORECASE), '"', depth).strip()

            # model transformations
            model = html.unescape(record.get('title', ''))
            model = re.sub(re.compile(
                    r'\bPlush\b|\bPin\b|Disney\b', re.IGNORECASE), '', model)
            if height:
                model = re.sub(re.compile(
                        r'–\s*' + height.strip('"') + r"''", re.IGNORECASE), '',
                        model)
            if width:
                model = re.sub(re.compile(
                        r'–\s*' + width.strip('"') + r"''", re.IGNORECASE), '',
                        model)
            if depth:
                model = re.sub(re.compile(
                        r'–\s*' + depth.strip('"') + r"''", re.IGNORECASE), '',
                        model)
            model = re.sub(r"–", '-', model)
            model = re.sub(r"''", '"', model)
            model = re.sub(r"&eacute;", 'é', model)

            # model extraction
            model = re.sub(re.compile(r"\bon$", re.IGNORECASE), "", model)
            # title case but retain capitalization in cases like POP or BB-10
            # but title case instaces like Tea-riffic - > Tea-Riffic
            model = " ".join([m[0].upper() + m[1:] for m in model.split()])
            model = re.sub(re.compile(r"(\d+)\s*th", re.IGNORECASE),
                           lambda m: m.group(1) + "th", model)
            model = re.sub(re.compile(r"(\w+)\-(\w+)", re.IGNORECASE),
                           lambda match: " ".join([m[0].upper() + m[1:]
                                                   for m in match.group(
                                           1).split()]) + "-" + " ".join(
                                   [m[0].upper() + m[1:]
                                    for m in match.group(2).split()]), model)

            model = re.sub(re.compile(r"pop\!", re.IGNORECASE), "POP!", model)
            model = re.sub(re.compile(r"\sH\s|\sH$|\sW\s|\sW$|\sD\s|\sD$",
                                      re.IGNORECASE), "",
                           model)
            model = re.sub(r'\s+', ' ', model).strip()

            product_type = product_type.split(", ")

            for ctr, word in enumerate(product_type):
                product_type[ctr] = re.sub(r"\*$", "", word).strip()
                if word != "Patches":
                    product_type[ctr] = re.sub(re.compile(
                            r"s$", re.IGNORECASE), "",
                            product_type[ctr]).strip()
            product_type = ", ".join(product_type)
            if re.findall(re.compile(r"Keepsakes \& Figurine", re.IGNORECASE),
                          product_type):
                product_type = "Figurine"
            if re.findall(re.compile(r"Ear Hat, Figurine", re.IGNORECASE),
                          product_type):
                product_type = "Ear Hat"

            if re.findall(re.compile('Pins, Buttons & Patches', re.IGNORECASE),
                          category) or re.findall(
                    re.compile(r'Pins*, Buttons* & Patches*|\bbuttons*\b|'
                               r'\bPins*\b|Plushes* & Stuffed Animals*|'
                               r'\bPlush\b|\bstuffed animals*\b',
                               re.IGNORECASE), product_type):
                return None

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawlDate": record.get('crawlDate', ''),
                    "statusCode": record.get('statusCode', ''),
                    "category": record.get('category'),
                    "brand": record.get('brand', ''),
                    "ner_query": ner_query,
                    "title": html.unescape(record.get('title', '')),

                    "model": model,
                    "item_category": category,
                    "product_type": product_type,
                    "gender": record.get('Gender', ''),
                    "color": record.get('Color', ''),
                    "age": record.get('Age', ''),
                    "character": record.get('Character', ''),
                    'series': record.get('Movie / Show', ''),

                    'franchise': record.get('Franchise', ''),
                    'description': record.get('short_description', ''),
                    'product_line': record.get('Disney Parks Resorts & More',
                                               ''),
                    'collection': record.get('Features', ''),
                    'height': height,
                    'width': width,
                    'depth': depth,
                    'model_sku': record.get('sku', ''),

                    "msrp": {
                            "currencyCode": "USD",
                            "amount": price
                    },
            }

            return transformed_record
        else:
            return None
