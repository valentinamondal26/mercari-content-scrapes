import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('msrp', '')
            if price:
                price = price.get('value', '')
                price = str(price)
            else:
                price = record.get('price', '') 
            price = price.replace("$", "").strip()

            category = record.get('Category', '')
            character = record.get('Character', '')
            franchise = record.get('Franchise', '')
            series = record.get('Movie / Show', '')
            model = record.get('title', '')


            specs = record.get('product_specification', [])
            specs.extend(record.get('extended_description', []))
            tech_specs = '\n'.join(specs)

            height = ''
            match = re.findall(re.compile(r"(?:\d+)(?:\.\d+){0,1}(?:\d*\s*\d+\/\d+){0,1}''\s*[H|L]", re.IGNORECASE), tech_specs)
            if match:
                height = match[0]
                height = re.sub(re.compile(r"''\s*[H|L]", re.IGNORECASE), '"', height).strip()

            if character and franchise:
                model = re.sub(r'({character})(.*)–\s+({franchise})(.*)'.format(character=character, franchise=franchise), r'\1 \2 from \3 \4', model, flags=re.IGNORECASE)

            if character and series:
                model = re.sub(r'({character})(.*)–\s+({series})(.*)'.format(character=character, series=series), r'\1 \2 from \3 \4', model, flags=re.IGNORECASE)            

            model = re.sub(r'\–', '', model)
            model_height = re.sub(r'\"', "''", height)
            model =  re.sub(r'{height}|{model_height}'.format(height=height, model_height=model_height), '', model, flags=re.IGNORECASE) 
            model = re.sub(re.compile(r"(?:\d+)(?:\.\d+){0,1}(?:\d*\s*\d+\/\d+){0,1}''\s*", re.IGNORECASE),'', model)
            model = re.sub(r"\d+\'\'", '', model)
            plush_type = ['Mini', 'Small', 'Medium', 'Large', 'Jumbo']
            model = re.sub(r'|'.join(list(map(lambda x: r'(\b'+x+r'\b)', plush_type))), '', model, flags=re.IGNORECASE)
            plush_found = re.findall(r'|'.join(list(map(lambda x: r'(\b'+x+r'\b)', plush_type))), record['title'], flags=re.IGNORECASE)
            if plush_found:
                plush = [x for x in plush_found[0] if x]
                if plush:
                    model = f'{model} {plush[0]}'
            model = re.sub(r'\bPlush\b', '', model, flags=re.IGNORECASE)
            model = f'{model} Plush'
            model = re.sub(r'\s+', ' ', model).strip()
          
            duplicate_models = ['Baby Pegasus from Hercules Large Plush', 'Baby Tuk Tuk from Raya and the Last Dragon Small Plush',\
                'Bingo from Puppy Dog Pals Personalized Small Plush', 'Boun from Raya and the Last Dragon Small Plush', \
                'Bullseye from Toy Story 4 Medium Plush', 'Buzz Lightyear from Toy Story 4 Medium Plush',\
                'Cheshire Cat Big Feet from Alice in Wonderland Medium Plush', 'Copper from The Fox and the Hound Medium Plush',\
                'Dory from Finding Dory Medium Plush', 'Hissy from Puppy Dog Pals Small Plush', 'Jessie from Toy Story 4 Medium Plush', \
                'Khan from Mulan Medium Plush', 'Lady from Lady and the Tramp Medium Plush', 'Marie from The Aristocats Medium Plush', 'Mira from Mira,\
                Royal Detective Small Plush', 'Miss Bunny Cuddleez from Bambi Large Plush', 'Nemo from Finding Dory Medium Plush',\
                'Olaf from Frozen 2 Medium Plush', 'Ongis Chattering from Raya and the Last Dragon Plush',\
                'Piglet from Winnie the Pooh Small Plush', 'Pua Big Feet from Moana Small Plush', \
                'Raya from Raya and the Last Dragon Medium Plush', 'Rex Cuddleez from Toy Story Large Plush', \
                'Rolly from Puppy Dog Pals Personalized Small Plush', 'Simba from The Lion King Medium Plush',\
                'Sisu Dragon from Raya and the Last Dragon Medium Plush', 'Sorcerer Mickey Mouse Sequined from Fantasia 80th Anniversary Small Plush',\
                'Tiana Doll from The Princess and the Frog Medium Plush', 'Tigger from Winnie the Pooh Medium Plush',\
                'Tuk Tuk from Raya and the Last Dragon Medium Plush', 'Wilden Lightfoot from Onward Medium Plush', \
                'Woody Disney Parks Wishables from Toy Story Mania! Series Micro Limited Release Plush', 'The Child in Hover Pram from Star Wars : The Mandalorian Small Plush',\
                'Woody from Toy Story 4 Medium Plush', 'The Child Pillow from Star Wars : The Mandalorian Plush', \
                'The Child Real Moves by Mattel from Star Wars : The Mandalorian Plush']  # https://mercari.atlassian.net/browse/USCC-57?focusedCommentId=456921
                                                        #TODO:We found out the dupicates manually based on the given comment in the ticket and removed the "from" word in the model to remove duplicates. \
                                                        # In future dedupe pipeline will replace this kind of duplicate finding works and we can remove this workaround.

            if re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', duplicate_models))), model, flags=re.IGNORECASE):
                model = re.sub(r'\bfrom\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\s+:\s+', f': ', model)
            model = re.sub(r'\bDisney land\b', 'Disneyland', model, flags=re.IGNORECASE)
            model = re.sub(r'(\bDisney’s\b|\bDisney \'s\b|\bDisney\'s\b)', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(?<!\bDisney Cruise Line\b)(?!\bDisney Cruise Line\b)(?!Disney Parks)(?<!Disney Parks)(\bDisney\b)', '', model, flags=re.IGNORECASE)
            if re.findall(r'\bWalt Disney\b', record['title'], flags=re.IGNORECASE):
                model = re.sub(r'(?!\bWalt Disney\b)\bWalt\b', 'Walt Disney', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+\,\s+', ', ', model)
            model = re.sub(r'\s+', ' ', model).strip()


            material = ''
            filtered_specs = list(filter(lambda x: x if re.findall(re.compile(r'Polyester|cotton|acrylic|man-made', re.IGNORECASE), x) else None, specs))
            if filtered_specs:
                material = filtered_specs[0]
                material = material.split(':')[-1]
                material = re.sub(r'\s*\/\s*', '/', material)
                material = re.sub(r'\s+', ' ', material).strip()
                if re.findall(r'\bImported\b', tech_specs, flags=re.IGNORECASE):
                    material = f'Imported {material}'
                material = material.title()


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "title": record.get('title', ''),

                "model": model,

                "color": record.get('Color', ''),
                "character": record.get('Character', ''),
                'series': record.get('Movie / Show', ''),
                'franchise': record.get('Franchise', ''),
                'height': height,
                'model_sku': record.get('sku', ''),
                'material': material,

                'description': record.get('short_description', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
