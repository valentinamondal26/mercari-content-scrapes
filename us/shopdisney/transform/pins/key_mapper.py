class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        franchise = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "franchise" == entity['name']:
                if entity["attribute"]:
                    franchise = entity["attribute"][0]["id"]

        key_field = franchise + model
        return key_field
