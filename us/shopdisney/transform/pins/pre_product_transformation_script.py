import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            if isinstance(price, dict):
                price=''
            price = price.replace("$", "").strip()

            product_type = record.get('Product Type', '')
            franchise = record.get('Franchise', '')

            specs = record.get('product_specification', [])
            specs.extend(record.get('extended_description', []))
            tech_specs = '\n'.join(specs)

            height = ''
            width = ''

            match = re.findall(re.compile(r"(?:\d+)(?:\.\d+){0,1}(?:\s*\d+\/\d+){0,1}''\s*H", re.IGNORECASE), tech_specs)
            if match:
                height = match[0]
                height = re.sub(re.compile(r"''\s*H", re.IGNORECASE), '"', height).strip()

            match = re.findall(re.compile(r"(?:\d+)(?:\.\d+){0,1}(?:\s*\d+\/\d+){0,1}''\s*W", re.IGNORECASE), tech_specs)
            if match:
                width = match[0]
                width = re.sub(re.compile(r"''\s*W", re.IGNORECASE), '"', width).strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bPin\b|Disney\b|\bpersonalizable\b|\bgift\b|', re.IGNORECASE), '', model)
            if franchise:
                model = re.sub(re.compile(r'\b'+franchise+r'\b', re.IGNORECASE), '', model)
            model = re.sub(r"–", '-', model)
            model = re.sub(r"''", '"', model)
            model = re.sub(r"&eacute;", 'é', model)
            model = re.sub(r'\s+', ' ', model).strip()

            # USDE-764 #
            model = re.sub(r"^[:]|:|-", "", model)
            model = re.sub(' +', ' ', model).strip()
            model = re.sub(re.compile(r'\bup\b', re.IGNORECASE), 'UP', model)
            model = re.sub(re.compile(r'oh my set$', re.IGNORECASE), 'Oh My Set Pixar', model)

            material = ''
            material = ', '.join(list(filter(re.compile(r'.*(metal|Polyester|resin)', re.IGNORECASE).match, specs)))
            material = material.split(':')[-1]
            material = re.sub(r'\s+', ' ', material).strip()

            description = record.get('short_description', '')


            if re.findall(re.compile(r'Pins, Buttons & Patches|\blanyards\b', re.IGNORECASE), product_type) \
                or re.findall(re.compile(r'\bPatches\b|\bPatched\b|b\bPatch\b|\blanyard\b', re.IGNORECASE), model + ' ' + description):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "product_type": product_type,
                "gender": record.get('Gender', ''),
                "color": record.get('Color', ''),
                "age": record.get('Age', ''),
                "character": record.get('Character', ''),
                'series': record.get('Movie / Show', ''),
                
                'franchise': franchise,
                'description': description,
                'features': record.get('Features', ''),
                'height': height,
                'width': width,
                'material': material,
                'model_sku': record.get('sku', ''),

                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
