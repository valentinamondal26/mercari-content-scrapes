'''
https://mercari.atlassian.net/browse/USCC-172 - Disney Pins
https://mercari.atlassian.net/browse/USCC-57 - Disney Stuffed Animals
https://mercari.atlassian.net/browse/USCC-184 - Disney Disney Collectibles
https://mercari.atlassian.net/browse/USCC-67 - Disney DVDs & Blu-ray Discs
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
from urllib.parse import urlparse, parse_qs, urlencode
import json
import re
from parsel import Selector


class ShopdisneySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from shopdisney.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) DVDs & Blu-ray Discs
        2) Disney Collectibles
        3) Stuffed Animals
        4) Pins

    brand : str
        brand to be crawled, Supports
        1) Disney

    Command e.g:
    scrapy crawl shopdisney -a category='Pins' -a brand='Disney'
    scrapy crawl shopdisney -a category='Stuffed Animals' -a brand='Disney'
    scrapy crawl shopdisney -a category='Disney Collectibles' -a brand='Disney'
    scrapy crawl shopdisney -a category='DVDs & Blu-ray Discs' -a brand='Disney'
    """

    name = 'shopdisney'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80'
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'shopdisney.com'
    blob_name = 'shopdisney.txt'

    url_dict = {
        'DVDs & Blu-ray Discs': {
            'Disney': {
                'url': 'https://www.shopdisney.com/home/entertainment/movies/',
                'filters': [
                    'Product Type', 'Brands', 'Gender', 'Color', 'Age', 'Character',
                    'Movie / Show', 'Franchise', 'Price',
                ],
            }
        },
        'Disney Collectibles': {
            'Disney': {
                'url': 'https://www.shopdisney.com/toys/for-the-collectors/',
                'filters': [
                    'Category', 'Product Type', 'Brands', 'Gender', 'Color', 'Size',
                    'Age', 'Character', 'Movie / Show', 'Franchise', 'Price',
                    'Disney Parks Resorts & More', 'Features',
                ],
            }
        },
        'Stuffed Animals': {
            'Disney': {
                'url': 'https://www.shopdisney.com/toys/shop-by-category/plush-stuffed-animals/?srule=default-sorting-rule&start=0&sz=24',
                'filters': [
                    'Category', 'Product Type', 'Brands', 'Gender', 'Color', 'Size',
                    'Age', 'Character', 'Movie / Show', 'Franchise', 'Price',
                    'Disney Parks Resorts & More', 'Features',
                ],
            }
        },
        'Pins': {
            'Disney': {
                'url': 'https://www.shopdisney.com/toys/for-the-collectors/pins-buttons-patches/',
                'filters': [
                    'Category', 'Product Type', 'Brands', 'Gender', 'Color', 'Size',
                    'Age', 'Character', 'Movie / Show', 'Franchise', 'Price',
                    'Disney Parks Resorts & More', 'Features',
                ],
            }
        },
    }

    base_url = 'https://www.shopdisney.com'

    filter_list = []

    merge_key = 'id'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ShopdisneySpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filter_list = [
                    'Category', 'Product Type', 'Brands', 'Gender', 'Color', 'Size',
                    'Age', 'Character', 'Movie / Show', 'Franchise', 'Price',
                    'Disney Parks Resorts & More', 'Features',
                ]


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_filter)


    def crawlDetail(self, response):
        """
        @url https://www.shopdisney.com/on/demandware.store/Sites-shopDisney-Site/default/Product-ShowQuickView?pid=468119229949&guestFacing=Toys-For%2520the%2520Collectors
        @scrape_values title price image long_description short_description rating product_specification extended_description sku
        @meta {"use_proxy":"True"}
        """
        ###extended description is not available
        res = json.loads(response.text)
        meta = response.meta
        product = res.get('product', '')

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': self.base_url + res.get('productUrl', ''),

            'title': product.get('productName', ''),
            'price': product.get('price', {}).get('sales', {}).get('formatted', ''),
            'msrp': product.get('price', {}).get('list', ''),
            'image': product.get('images', {}).get('large', [{}])[0].get('url', ''),
            'long_description': ''.join(Selector(product.get('longDescription', '')).xpath('.//text()').extract()),
            'short_description': ''.join(Selector(product.get('shortDescription', '')).xpath('.//text()').extract()),
            'rating': product.get('rating', ''),
            'product_specification': Selector(product.get('productSpecifications', '')).xpath('.//text()').extract(),
            'extended_description': Selector(product.get('extendedDescription', '')).xpath('.//text()').extract(),
            'sku': product.get('id', ''),
        }
        filter_name = meta.get('filter', '')
        if filter_name:
            filter_data = dict.fromkeys([filter_name], meta[filter_name])
            item.update(filter_data)

        yield item

    def parse_filter(self, response):
        """
        @url https://www.shopdisney.com/toys/for-the-collectors/
        @returns requests 2
        @meta {"use_proxy":"True"}
        """
        total_count = re.findall(r'\d+', response.xpath('//div[@class="search__results_count category-results "]/span/text()').extract_first())[0]
        print(f'total_count: {total_count}')

        for refinement in response.xpath('//div[@class="refinements"]/div'):
            name = refinement.xpath('./header/button/@title').extract_first()
            if name in self.filter_list:
                for value in refinement.xpath('.//li[contains(@class, "refinement__value")]/a'):
                    label = value.xpath(u'normalize-space(.//span[contains(@class, "refinement__label")]/text())').extract_first()
                    link = value.xpath('./@href').extract_first()
                    match = re.findall(r'\(\d+\)', label)
                    count = '48'
                    if match:
                        count = match[0]
                        label = label.replace(count, '').strip()
                        count = count.replace('(', '').replace(')', '').strip()

                    if link:
                        link = self.base_url + link

                        parts = urlparse(link)
                        query_dict = parse_qs(parts.query)
                        query_dict.update({'sz':[count],'start':['0']})
                        url = parts._replace(query=urlencode(query_dict, True)).geturl()
                        meta = {'filter': name}
                        meta.update({meta['filter']: label})
                        request = self.create_request(url=url, callback=self.parse_listing_page, meta=meta)
                        yield request

        parts = urlparse(response.url)
        query_dict = parse_qs(parts.query)
        query_dict.update({'sz':[total_count],'start':['0']})
        url = parts._replace(query=urlencode(query_dict, True)).geturl()
        yield self.create_request(url=url, callback=self.parse_listing_page)


    def parse_listing_page(self, response):

        """
        @url https://www.shopdisney.com/toys/for-the-collectors/?sz=525&start=0
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        for product in response.xpath('//div[@class="product "]'):
            id = product.xpath('.//a[@itemprop="url"]/@href').extract_first()
            data_url = product.xpath('.//a[@data-target="#quickViewModal"]/@href').extract_first()
            if id and data_url:
                id = self.base_url + id
                data_url = self.base_url + data_url
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.extra_item_infos.get(id).update(extra_item_info)
                yield self.create_request(url=data_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
