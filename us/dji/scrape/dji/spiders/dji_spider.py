'''
https://mercari.atlassian.net/browse/USCC-408 - DJI Drones
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class DjiSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from dji.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Drones

    brand : str
        brand to be crawled, Supports
        1) DJI

    Command e.g:
    scrapy crawl dji -a category='Drones' -a brand='DJI'
    """

    name = 'dji'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'dji.com'
    blob_name = 'dji.txt'

    url_dict = {
        'Drones': {
            'DJI': {
                'url': 'https://www.dji.com/products/enterprise?site=brandsite&from=nav#drones',
            }
        }
    }

    base_url = 'https://www.dji.com'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(DjiSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.dji.com/matrice-300/specs
        @meta {"use_proxy":"True"}
        @scrape_values title specs
        """

        specs = {}
        for group_specs in response.xpath('//section[@class="select-list"]//div[@class="detailed-parameter-wrap"]/div[@class="specs-container"]/div[@class="specs-parameter-wrap"]') \
            or response.xpath('//section[@class="section-specs"]//h3[@class="section-title"]') \
            or response.xpath('//div[contains(@class, "spec-content")]//table[contains(@class, "table-spec")]/tr'):
            group_title = group_specs.xpath('./h3[@class="group-list-title"]/text()').extract_first() \
                or group_specs.xpath('./text()').extract_first() \
                or group_specs.xpath('./th/h4/text()').extract_first()
            group_spec = {}
            for group_spec_element in group_specs.xpath('./ul[@class="detailed-parameter-list"]') \
                or group_specs.xpath('./following-sibling::table[@class="table-specs"][1]/tbody/tr') \
                or group_specs.xpath('./td/div[@class="row"]'):
                key = group_spec_element.xpath('./li[@class="detailed-parameter-key"]/h4/text()').extract_first() \
                    or group_spec_element.xpath('./th/text()').extract_first() \
                    or group_spec_element.xpath('./div/h5//text()').extract_first()
                value = group_spec_element.xpath('./li[@class="detailed-parameter"]/div[@class="detailed-parameter-value"]//text()').extract() \
                    or group_spec_element.xpath('./td//text()').extract() \
                    or group_spec_element.xpath('./div[2]/text()').extract()
                group_spec[key] = value
            specs[group_title] = group_spec

        if not specs and not response.meta.get('fetching_specs', False):
            specs_url = response.xpath('//div[@class="page"]/iframe/@src').extract_first()
            if not specs_url.startswith(self.base_url):
                specs_url = self.base_url + specs_url
            yield self.create_request(url=specs_url, callback=self.crawlDetail, meta={'item': response.meta.get('item', {}), 'fetching_specs': True})
            return

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            # 'id': response.meta.get('id', ''),

            'title': response.xpath('//h1[contains(@class, "product-title")]//text()').extract_first()
                or response.xpath('//h2[@class="product-logo"]/a/img/@alt').extract_first(),
            'specs': specs,
        }
        item.update(response.meta.get('item', {}))

        yield item


    def parse_landing_page(self, response):
        """
        @url https://www.dji.com/matrice-300?site=brandsite&from=landing_page
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        specs_page_url = response.xpath('//div[contains(@class, "style__sub-nav-container")]//li/a[text()="Specs"]/@href').extract_first() \
            or response.xpath('//nav[@id="product-subnav"]/div[@class="container"]//li/a[contains(text(), "Specs")]/@href').extract_first() \
            or response.xpath('//div[@id="subNavBar"]//li/a[contains(text(), "Specs")]/@href').extract_first() \
            or response.url.split('?')[0]+'/spec'
        # print('specs_page_url:', specs_page_url)
        if specs_page_url:
            if not specs_page_url.startswith(self.base_url):
                specs_page_url = self.base_url + specs_page_url
            item = {
                'id': response.url,
                'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            }
            yield self.create_request(url=specs_page_url, callback=self.crawlDetail, meta={'item': item})


    def parse(self, response):
        """
        @url https://www.dji.com/products/enterprise?site=brandsite&from=nav#drones
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.xpath('//div[@class="products-list"]/section[@id="drones-list"]/ul/li'):
            url = product.xpath('.//a/@href').extract_first()
            if url:
                # print('url:', url)
                yield self.create_request(url=url, callback=self.parse_landing_page)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
