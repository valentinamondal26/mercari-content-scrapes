import hashlib
import re
from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = titlecase(model)
            model = re.sub(r'\bMATRICE\b', 'Matrice', model, flags=re.IGNORECASE)
            model = re.sub(r'\bArf\b', 'ARF', model, flags=re.IGNORECASE)

            model_color_dict = {
                'MATRICE 300 RTK': 'White',
                'Mavic 2 Enterprise Series': 'Gray/Orange',
                'Matrice 200 Series V2': 'Black',
                'Matrice 200 Series': 'Black',
                'P4 Multispectral': 'White',
                'Phantom 4 RTK': 'White',
                'Phantom 4 Pro': 'White',
                'Matrice 600': 'Black/Red',
                'Matrice 600 Pro': 'Black/Red',
                'Matrice 100': 'Black',
                'Flame Wheel ARF KIT': 'Black/Multicolor',
                'Spreading Wings S1000+': 'Black/Red',
                'Spreading Wings S900': 'Black/Red',
            }
            model_color_dict = dict(zip(map(str.lower, model_color_dict.keys()), model_color_dict.values()))
            color = model_color_dict.get(model.lower(), '')

            dimensions = '\n'.join(record.get('specs', {}).get('Aircraft', {}).get('Dimensions', []))

            length = ''
            width = ''
            height = ''
            match = re.findall(r'Unfolded.*?(\d+)×(\d+)×(\d+) (\w+)', dimensions, flags=re.IGNORECASE)
            if match:
                unit = match[0][3]
                length = match[0][0] + f' {unit}'
                width = match[0][1] + f' {unit}'
                height = match[0][2] + f' {unit}'

            weight = ''
            weight_info = ' '.join(record.get('specs', {}).get('Aircraft', {}).get('Weight (with single downward gimbal)', []))
            match = re.findall(r'(\d+\.*\d*\s*kg\b)', weight_info, flags=re.IGNORECASE)
            if match:
                weight = match[0]

            battery_life = ''
            match = re.findall(r'Built-in battery: .*?(\d+\.*\d*\s*h\b)', ' '.join(record.get('specs', {}).get('Remote Controller', {}).get('Battery Life', [])), flags=re.IGNORECASE)
            if match:
                battery_life = match[0]
                battery_life = re.sub(r'\s*h$', ' hours', battery_life.strip(), flags=re.IGNORECASE)

            speed = ', '.join(record.get('specs', {}).get('Aircraft', {}).get('Max Speed', []))
            maximum_flight_time = ', '.join(record.get('specs', {}).get('Aircraft', {}).get('Max Flight Time', []))

            video_resolution = ', '.join(record.get('specs', {}).get('FPV Camera', {}).get('Resolution', []))
            field_of_view = ', '.join(record.get('specs', {}).get('FPV Camera', {}).get('FOV', []))

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "length": length,
                'width': width,
                'height': height,
                'weight': weight,
                'speed': speed,
                'maximum_flight_time': maximum_flight_time,
                'battery_life': battery_life,
                'video_resolution': video_resolution,
                'field_of_view': field_of_view,
            }

            return transformed_record
        else:
            return None
