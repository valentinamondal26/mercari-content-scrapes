import hashlib
import re

from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')

            price = record.get('price', '').replace('$', '').strip()
            product_specs = record.get('product_details')

            try:
                model_sku = re.findall(r'\((.*)\)', title)[0]
            except IndexError:
                model_sku = product_specs.get('manufacturer #', '')
            model_sku = re.sub(r'\s+', ' ', model_sku).strip()
            model = re.sub(model_sku, '', title)

            description = record.get('description', '')
            description = [des.strip() for des in description]
            description = '. '.join(description).replace("..", '.')

            # ink color transformation
            ink_color = product_specs.get('color', '')
            ink_color_meta = ['Black', 'Blue', 'Chromatic Blue',
                              'Chromatic Green', 'Chromatic Red', 'Clear',
                              'Cyan', 'Dark Gray', 'Dye\-*\s*Based Cyan',
                              'Dye\-*\s*Based Gray', 'Dye\-*\s*Based Magenta',
                              'Gray', 'Light Cyan', 'Light Gray',
                              'Light Magenta', 'Magenta', 'Matte Black',
                              'Photo Black', 'Red', 'Tri\-*\s*Color', 'Yellow']
            # if ink color is empty extract from model
            ink_color_meta += [i for i in 
                               re.findall(r"\w+",
                                          model) + re.findall(r"\w+",
                                                              description)
                               if self.check_color(i)]
            if not ink_color:
                ink_color = []                    
                for cl in ink_color_meta:
                    if re.findall(re.compile(r"\b{}\b\s*Drum".format(cl),
                                             re.IGNORECASE),
                                             title + " "+ description):
                        ink_color.append(re.findall(re.compile(
                                r"\b{}\b\s*Drum".format(cl), re.IGNORECASE),
                                title + " "+ description)[0])
                        
                    elif re.findall(re.compile(r"\b{}\b".format(cl),
                                    re.IGNORECASE), title + " "+ description):
                        ink_color.append(re.findall(
                                         re.compile(r"\b{}\b".format(cl),
                                                    re.IGNORECASE),
                                                    title + " "+ description)[0]
                                                    )
                ink_color = "/".join(ink_color)
            ink_color = "/".join(ink_color.split("; "))

            ink_color = re.sub(re.compile(r"Dye\-*\s*based",
                               re.IGNORECASE), "", ink_color)
            ink_color = re.sub(re.compile(r"^Color$",
                               re.IGNORECASE), "Tri-color", ink_color)
            ink_color = re.sub(r'\s+', ' ', ink_color).strip().title()
            ink_color = "/".join(sorted(set(ink_color.split("/")),
                                        key=ink_color.split("/").index))

            # model transformations
            model = re.sub(re.compile(
                    r'\(\)|\,|\bgovernment\b', re.IGNORECASE), '', model)

            model = re.sub(record.get('brand', ''), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            # string tri-color transformations
            model = re.sub(re.compile(r"Tri\-*\s*color",
                           re.IGNORECASE), "Tri-Color", model)
            model = re.sub(re.compile(
                    r"(original cartridge|original ink cartridge|"
                    r"original toner cartridge|cartridge)(.*)Tri\-*color",
                    re.IGNORECASE), r"Tri-Color \1 \2", model)
            model = re.sub(re.compile(
                    r"\-*\s*for.*", re.IGNORECASE), "", model)

            # remove ink_color from model
            for cl in re.findall(r"\w+", ink_color):
                model = re.sub(re.compile(r"(\b{}\b\s*)\&".format(cl),
                                  re.IGNORECASE), r"\1", model)
            
            if re.findall(re.compile(r"\b{}\b\s*Drum".format(ink_color),
                                             re.IGNORECASE), model):
                model = re.sub(re.compile(r"\b{}\b\s*Drum".format(ink_color),
                                          re.IGNORECASE),
                              "", model)
                ink_color = re.sub(re.compile(r"(\b{}\b)".format(ink_color),
                                   re.IGNORECASE), r"\1 Drum", ink_color)
            for color in re.findall(r"\w+",
                                    product_specs.get('color', '')
                                    ) + re.findall(r"\w+", ink_color):
                model = re.sub(re.compile(r"\b{}\b".format(
                        color), re.IGNORECASE), "", model)
            ## converting multiple colors
            colors = ink_color.split('/')
            if len(colors) == 4:
                ink_color = colors[0]+'/Tri-Color'


            # pack of transformation
            model = re.sub(re.compile(r"Pack Of (\d+)|(\d+)\s*\/*\s*Pack",
                           re.IGNORECASE), r"\1\2-Pack", model)
            if len(re.findall(re.compile(r"\bcartridges\b",
                              re.IGNORECASE), model)) > 1:
                model = re.sub(re.compile(
                        r"(.*)cartridges(.*)(\d+\-Pack)(.*)cartridges",
                        re.IGNORECASE), r"\3 of \1 Cartridges \2", model)
            model = re.sub(re.compile(r"(.+)(\d+\-Pack)",
                           re.IGNORECASE), r"\2 of \1", model)

            # string transformations
            model = re.sub(re.compile(
                    r"\d+\s*Pages|\d+\s*\/*\s*Each", re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                    r"High\s*Yield", re.IGNORECASE), "High-Yield", model)
            model = re.sub(re.compile(r"(.*)cartridge(.*)(\blaser\b|\bink\b)(.*)",
                           re.IGNORECASE), r"\1 \3 Cartridge \2 \4",
                           model)
            model = re.sub(r'\s+', ' ', model).strip()

            # remove rouge - from model
            model = re.sub(r"\s+(\-|\/)\s+|\s+(\-|\/)|(\-|\/)\s+", " ", model)
            model = model.strip("/ -")
            model = re.sub(r"[^\w]+\/[^\w]+", " ", model)
            model = re.sub(re.compile(r'\bT6L90AN#140\b',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'Designjet',re.IGNORECASE),'DesignJet',model)
            model = re.sub(r'\s+', ' ', model).strip()
            if re.findall(re.compile(r'\d+xl \w+ and \d+',re.IGNORECASE),model):
                color_word = re.findall(re.compile(r'\d+xl (\w+) and \d+',re.IGNORECASE),model)[0]
                if color_word in ink_color_meta:
                    model = re.sub(re.compile(r'\w+ and',re.IGNORECASE),'',model)
                model = re.sub(r'\s+',' ',model)
            if re.findall(r'\d+XL\s\d+',model):
                replace_string = re.findall(r'\d+XL\s\d+',model)[0].replace(' ','/')
                model = re.sub(re.compile(r'\d+XL\s\d+',re.IGNORECASE),replace_string,model)
            model = re.sub(r'\s+', ' ', model).strip()
            if record.get('title','') == 'HP C9460A, Matte Black/Cyan Printhead':
                model = 'C9460A Printhead'

            product_specs_keys = list(product_specs.keys())

            max_output_key = [key for key in product_specs_keys
                              if 'maximum yield per unit' in key]
            if max_output_key:
                max_output_key = max_output_key[0]
            else:
                max_output_key = ''

            max_output = product_specs.get(max_output_key, '')

            quality = product_specs.get('yield', '')
            compatible_model = product_specs.get('compatible with', '')

            series = product_specs.get('printer series', '')
            technology = product_specs.get('print technology', '')
            item_condition = product_specs.get('remanufactured', '')
            product_type = product_specs.get('product type', '')

            if re.findall(re.compile(r"\bkit\b|\bbundle\b|\bRecycled\b",
                          re.IGNORECASE),
                          record.get("title", "")):
                return None

            transformed_record = {
                    'title': title,
                    'model': model,
                    'ink_color': ink_color,

                    'category': record.get('category', ''),
                    'brand': record.get('brand', ''),

                    'image': record.get('image', ''),
                    'description': description,

                    'status_code': record.get('status_code', ''),
                    'id': record.get('id', ''),
                    'item_id': hex_dig,
                    'crawl_date': record.get('crawl_date', '').split(',')[0],

                    'max_output': max_output,
                    'quality': quality,
                    'compatible_model': compatible_model,
                    'model_sku': model_sku,
                    'series': series,
                    'technology': technology,
                    'item_condition': item_condition,
                    'product_type': product_type,

                    'msrp': {
                            'currency_code': 'USD',
                            'amount': price
                    },
            }

            return transformed_record

    def check_color(self, color):
        try:
            Color(color)
            return True
        except Exception:
            return False
