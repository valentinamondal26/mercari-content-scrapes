'''
https://mercari.atlassian.net/browse/USCC-314 - HP Printer Ink, Toner & Paper
'''

import scrapy
from datetime import datetime
import re
import os
from scrapy.exceptions import CloseSpider
import itertools


class officedepotSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and sub-category from officedepot.com

    Attributes

    category : str
        category to be crawled, Supports
        1) Ink & Toner


    brand : str
    brand to be crawled, Supports
        1) HP

    Command e.g:
    scrapy crawl officedepot  -a category="Ink & Toner" -a brand='HP'

    """

    name = "officedepot"

    category = None
    brand = None

    launch_url = None
    filters = None

    source = 'officedepot.com'
    blob_name = 'officedepot.txt'

    spider_project_name = "officedepot"
    gcs_cache_file_name = 'officedepot.tar.gz'

    base_url = 'https://www.officedepot.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com',
        'http://shp-mercari-us-d00002.tp-ns.com',
        'http://shp-mercari-us-d00003.tp-ns.com',
        'http://shp-mercari-us-d00004.tp-ns.com',
        'http://shp-mercari-us-d00006.tp-ns.com',
    ]

    url_dict = {
        "Printer Ink, Toner & Paper":{
            "HP":{
                "url":"https://www.officedepot.com/a/browse/hp-ink-and-toner/N=5+513382&cbxRefine=302157&cbxRefine=558090&cbxRefine=558982&cbxRefine=1443862/"
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(officedepotSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK', ''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url = self.url_dict.get(category, {}).get(brand,{}).get('url','')
        self.filters = self.url_dict.get(category,{}).get(brand,{}).get('filter','')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info(f"Crawling for  category: {category}, url: {self.launch_url}")

    def contracts_mock_function(self):
        self.round_robin = itertools.cycle(self.proxy_pool)

    def start_requests(self):
        if isinstance(self.launch_url, list):
            for url in self.launch_url:
                request = self.createRequest(url=url, callback=self.parse_listing_page)
                yield request
        else:
            request = self.createRequest(url=url, callback=self.parse_listing_page)
            yield request


    def parse_listing_page(self,response):
        """
        @url https://www.officedepot.com/a/browse/hp-ink-and-toner/N=5+513382&cbxRefine=302157&cbxRefine=558090&cbxRefine=558982&cbxRefine=1443862/
        @meta {"use_proxy": "True"}
        @returns requests 25
        """
        #No. of requests specified in contract is 25. This is because 24 items per page and 1 next page url.
        product_urls = response.xpath("//div[@class='cm_teaser_text bnow']/following-sibling::a/@href").getall()
        if not product_urls:
            product_urls = list(set(response.xpath("//div[@class='photo_no_QV flcl']/a/@href").getall()))

        for url in product_urls:
            if self.base_url not in url:
                url = self.base_url + url

            yield self.createRequest(url=url, callback=self.crawldetails)

        next_page_url = response.xpath("//div[@class='next pg_btn']/a/@href").get()
        if next_page_url:
            if self.base_url not in next_page_url:
                next_page_url = self.base_url + next_page_url

            yield self.createRequest(url=next_page_url, callback=self.parse_listing_page)


    def crawldetails(self,response):
        """
        @url https://www.officedepot.com/a/products/434207/HP-950XL951-BlackCyanMagentaYellow-Ink-Cartridges-C2P01FNM/
        @meta {"use_proxy": "True"}
        @scrape_values title price description image breadcrumb product_details.color product_details.product\stype product_details.total\syield product_details.brand\
            product_details.remanufactured product_details.brand\sname product_details.eco-conscious product_details.manufacturer \
            product_details.maximum\syield\sper\sunit\s(color) product_details.original\sequipment\smanufacturer\s(oem)\spart\snumber\
            product_details.pack\stype product_details.yield product_details.number\sof\sunits\s(color)\
            product_details.compatible\swith product_details.model product_details.ink\sseries \
            product_details.printer\sseries product_details.print\stechnology product_details.officemax\s# product_details.manufacturer\s#
        """
        ##Skipped Checking product_details['item #'] because in "item #" space is coming as ascii code in the output.
        title = response.xpath('//h1[@itemprop="name"]/text()').get(default='').strip()
        price = response.xpath('//div[@class="unified_price_row unified_sale_price red_price"]/span/text()').get(default="")
        des1 = response.xpath('//div[@class="sku_desc show"]/p/text()').getall()
        des2 = response.xpath('//div[@class="sku_desc show"]/ul/li/text()').getall()
        des3 = response.xpath('//div[@id="productTabs"]//section[@id="descriptionContent"]//text()').getall()
        description  = des1+des2 + des3
        description = [re.sub(r"\t|\n|\r", "", d) for d in description if not re.compile(r"^\W*$", re.IGNORECASE).match(d)]
        image= response.xpath('//img[@itemprop="image"]/@src').get(default='')

        breadcrumb = response.xpath('//li[@itemprop="itemListElement"]/a/span/text()').getall()
        breadcrumb1 = [response.xpath('//li[@itemprop="itemListElement"]/span/text()').get(default='').strip()]
        breadcrumb = breadcrumb+breadcrumb1
        breadcrumb = '|'.join(breadcrumb)

        details = []
        product_details_selector = response.xpath('//tbody[@class="showTheseRows"]/tr/td')
        for product in product_details_selector:
            details.append(product.xpath('.//text()').get(default=''))

        product_details1_selector = response.xpath('//tbody[@class="permanentRows"]/tr/td')
        for product in product_details1_selector:
            details.append(product.xpath('.//text()').get(default=''))

        product_details2_selector =  response.xpath('//div[@class="hideTheseRows"]/table/tbody/tr/td')
        for product in product_details2_selector:
            details.append(product.xpath('.//text()').get(default=''))

        if  not product_details2_selector and not product_details_selector:
            details = []
            details_response = response.xpath('//table[@class="data tabDetails gw9"]/tbody/tr/td')
            for detail in details_response:
                details.append(detail.xpath('.//text()').get().strip())

        details_specs= {}
        for i in range(0,len(details),2):
            details_specs.update({details[i].lower().strip():details[i+1].strip()})

        missed_item = []
        for key, val in details_specs.items():
            if not val:
                missed_item.append(key)

        for item in missed_item:
            if item.lower() == 'compatible with':
                item_id = 'attribute'+item.lower().replace(' ','_')+'key'
                values = response.xpath('//td[@id=''+item_id+'']/p/text()').get(default='')
                if not values:
                    heading = response.xpath('//td[@id=''+item_id+'']/p/b/text()').get(default='')
                    value = response.xpath('//td[@id=''+item_id+'']/ul/li/text()').getall()
                    value = ', '.join(value)
                    values = heading+value
                details_specs.update({item:values})

        item = {
            'title': title,
            'price': price,
            'description': description,
            'image': image,
            'brand': self.brand,
            'breadcrumb': breadcrumb,
            'id': response.url,
            'category': self.category,
            'status_code': str(response.status),
            'crawl_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'product_details': details_specs,
        }
        yield item


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] =  next(self.round_robin)
    
        request.meta.update({'use_cache':True})
        return request
