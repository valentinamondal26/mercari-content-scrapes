import hashlib
import re


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get("price", "").replace("$", "").strip()

            # description transformation
            description = record.get("description", "")
            description = ".".join(description).replace("\n", "").replace(
                "\xa0", "").replace("\u200b", "").replace("..", ".").strip()
            if description[-1] != "." and description[-1] != "!":
                description = description + "."

            # extract hari color
            try:
                hair_color = re.findall(re.compile(
                    r'\w+ hair', re.IGNORECASE), record.get("model", ""))[0].replace("Hair", "").strip()
            except IndexError:
                hair_color = ''

            # model transformations
            model = record.get('model', '')
            model = re.sub(re.compile(
                r'\bdoll\b|®|™|', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'with\s*$', re.IGNORECASE), '', model)
            model = re.sub(r'\s+,|,\s*,', ',', model)
            model = model.title()
            model = re.sub(re.compile(r'Bmr', re.IGNORECASE), 'BMR', model)
            model = re.sub(re.compile(r'\dSt|\dNd\b|\drd\b|\dTh\b|\band\b',
                                      re.IGNORECASE), lambda x: x.group(0).lower(), model)
            model_words = model.split()
            if model_words[-1] == "and":
                model = " ".join(model_words[:-1])
            if model[-1] == "," or model[-1] == "&" or model[-1] == "-":
                model = model[:-1]
            model = re.sub(re.compile(r"Fashionistas*\s*(\d+)",
                                      re.IGNORECASE), r"Fashionistas #\1", model)
            model = re.sub(r"[\-|\–]\s|\s[\-|\–]|\s[\-|\–]\s", " ", model)
            model = re.sub(r'\s+', ' ', model).strip()
            model_sku = record.get("model_sku").replace("SKU #:", "").strip()

            product_line = record.get("DOLLS", "")

            if re.findall(re.compile(r'Doll Clothes & Closets', re.IGNORECASE), product_line) \
                    or re.findall(re.compile(r'Gift\s*Set|Bundle|Clothes: Puma Branded Outfit|\bWith Accessories\b', re.IGNORECASE), model) \
                    or re.findall(re.compile(r"^Barbie Chelsea Dolls$|^Barbie and Accessories$", re.IGNORECASE), model) != []:
                return None
            transformed_record = {
                "title": record.get("model", ""),
                "model": model,
                "hair_color": hair_color,
                "id": record.get('id', ""),
                "item_id": hex_dig,
                "image": record.get("image", ""),
                "brand": record.get("brand", ""),
                "category": record.get('category', ""),
                "description": description,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "status_code": record.get("status_code", ""),
                "model_sku": model_sku,
                "crawl_date": record.get("crawl_date", "").split(",")[0],
                "age": record.get('age', ""),
                "product_line": product_line,
                "series": record.get("BRAND", "").strip(),
            }

            return transformed_record
        else:
            return None
