class Mapper:
    def map(self, record):
        key_field = ""

        model = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]!=[]:
                    model = entity["attribute"][0]["id"]
                    break

        key_field = model
        return key_field
