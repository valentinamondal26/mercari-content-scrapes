'''
https://mercari.atlassian.net/browse/USDE-582 - Barbie Fashion Dolls
'''

import scrapy
import datetime
from scrapy.http import FormRequest
import math
import os
from datetime import datetime
import re
from scrapy.exceptions import CloseSpider


class BarbieSpider(scrapy.Spider):

    """
     spider to crawl items in a provided category and sub-category from barbie.mattel.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Fashion Dolls


     brand : str
      brand to be crawled, Supports
       1) Barbie

     Command e.g:
     scrapy crawl barbie  -a category='Fashion Dolls' -a brand='Barbie'

    """

    name = 'barbie'

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None

    source = 'barbie.mattel.com'
    blob_name = 'barbie.txt'

    base_url = 'https://barbie.mattel.com'

    url_dict = {
        "Fashion Dolls": {
            'Barbie': {
                'url': 'https://barbie.mattel.com/shop/en-us/ba/all-barbie-dolls',
                'filters': ['DOLLS','BRAND'],
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BarbieSpider, self).__init__(*args, **kwargs)
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['BRAND','DOLLS']


    def start_requests(self):
        if self.launch_url:
            yield scrapy.Request(url=self.launch_url, callback=self.total_filters)


    def total_filters(self,response):
        """
        @url https://barbie.mattel.com/shop/en-us/ba/all-barbie-dolls
        @returns requests 1
        """

        filter_urls = response.xpath('//div[input[@value="{}"]]//ul/li[.//a[contains(text(),"{}")]]/ul//div[@class="item"]/a/@href'.format("DOLLS","DOLLS")).getall()
        i=0
        # while "#" in filter_urls: filter_urls.remove("#")
        filter_urls.append("https://barbie.mattel.com/shop/en-us/ba/all-barbie-dolls")
        while i < len(filter_urls):
            if "https" not in filter_urls[i]:
                filter_urls[i] = "https://barbie.mattel.com" + filter_urls[i]
            yield scrapy.Request(url=filter_urls[i],callback = self.find_product_count)
            i+=1

        for filter in self.filters:
            if filter == 'DOLLS':
                filter_urls=response.xpath('//div[input[@value="{}"]]//ul/li[.//a[contains(text(),"{}")]]/ul//div[@class="item"]/a/@href'.format("DOLLS","DOLLS")).getall()
                filter_urls.append("https://barbie.mattel.com/shop/en-us/ba/all-barbie-dolls")
                for url in filter_urls:
                    if "barbie.mattel.com" not in url:
                        url = "https://barbie.mattel.com" + url
                    yield scrapy.Request(url=url,callback = self.find_product_count)


    def find_product_count(self,response):
        test_string = response.xpath("//span[@class='num_products']/text()").extract()[0]
        res = [int(i) for i in test_string.split() if i.isdigit()]
        total_products  =  max(res)
        total_pages = math.ceil(total_products/36)
        i=1
        productBeginIndex=0
        beginIndex=0
        aa = response.xpath('//input[@id="yottaLazySwitch"]//following-sibling::script[contains(text(),"/shop/ProductListingView?")]//text()').get()
        bb = aa.split("/shop/ProductListingView")[1]
        cc=bb.split("category_rn")[0]
        listing_page_url = "https://barbie.mattel.com/shop/ProductListingView"+cc+"category_rn="
        # print(listing_page_url)
        for i in range(total_pages):
            formdata =   {'contentBeginIndex':'0','productBeginIndex':str(productBeginIndex),
            'beginIndex':str(beginIndex),'pageView':'grid',
            'resultType':'products','requesttype':'ajax'}
            # print(formdata)
            meta={"filter_name":"DOLLS","formdata":formdata}
            yield FormRequest(listing_page_url,meta=meta,formdata=formdata,callback=self.parse_page)
            productBeginIndex+=36
            beginIndex+=36
        if "BRAND" in self.filters:
            filter_name = response.xpath('//form[@id="productsFacets"]//div[h5[contains(text(),"Brand")]]//following-sibling::div//li//span[contains(@id,"facetLabel")]/text()').getall()
            filter_id = response.xpath('//form[@id="productsFacets"]//div[h5[contains(text(),"Brand")]]//following-sibling::div//li//span[contains(@id,"facetLabel")]/@id').getall()
            for name,id in zip(filter_name,filter_id):
                facetId = re.findall(r'\d+',id)[0]
                formdata={'contentBeginIndex': '0',
                'productBeginIndex': '0',
                'beginIndex': '0',
                'facetId':facetId}
                # print(formdata)
                # formdata=json.dumps(formdata)
                # brand_filter_request(listing_page_url,format)
                meta={'formdata':formdata,"filter_name":"BRAND","listing_url":listing_page_url}
                meta.update({meta["filter_name"]:name})
                yield FormRequest(listing_page_url,formdata=formdata,meta=meta,callback=self.brand_filter_request)
                yield FormRequest(listing_page_url,formdata=formdata,meta=meta,callback=self.parse_page)


    def brand_filter_request(self,response):
        formdata=response.meta["formdata"]
        # print("brand filter")
        # formdata=json.loads(formdata)
        # print(formdata)
        test_string = response.xpath("//span[@class='num_products']/text()").extract()[0]
        res = [int(i) for i in test_string.split() if i.isdigit()]
        total_products  =  max(res)
        total_pages = math.ceil(total_products/36)
        if total_pages>1:
            productBeginIndex=36
            beginIndex=36
            for i in range(1,total_pages):
                formdata['productBeginIndex'] = str(productBeginIndex)
                formdata['beginIndex'] = str(beginIndex)
                # formdata=json.dumps(formdata)
                meta = response.meta
                yield FormRequest(response.url,meta = meta,formdata=formdata,callback=self.parse_page)
                productBeginIndex+=36
                beginIndex+=36


    def parse_page(self,response):
        """
        @url https://barbie.mattel.com/shop/ProductListingView?advancedSearch=&shopBy=&giftAge=&manufacturer=&metaData=&enableSKUListView=false&catalogId=10104&searchTerm=&resultsPerPage=36&filterFacet=&resultCatEntryType=&giftPrice=&giftCategories=&gridPosition=&top_category=&categoryFacetHierarchyPath=&ajaxStoreImageDir=%2Fwcsstore%2FMattelSAS%2F&searchType=1002&filterTerm=&searchTermScope=&storeId=10151&ddkey=ProductListingView_5_-2011_140548&sType=SimpleSearch&emsName=Widget_CatalogEntryList_701_140548&disableProductCompare=true&thematicName=&langId=-1&facet=&categoryId=15602&parent_category_rn=
        @returns requests 1
        """

        items_url =  response.xpath("//div[@class='product']").xpath("//div[@class='image']/a/@href").extract()
        i=0
        items_url_set = set(items_url)
        items_url = list(items_url_set)
        while i < len(items_url):
            if "barbie.mattel.com" not in items_url[i]:
                items_url[i] = "https://barbie.mattel.com"+items_url[i]
            yield scrapy.Request(items_url[i],meta = response.meta,callback=self.parse_item)
            i+=1


    def parse_item(self,response):
        """
        @url https://barbie.mattel.com/shop/en-us/ba/all-barbie-dolls/barbie-fashionistas-doll-134-ghw50
        @scrape_values breadcrumb model image description model_sku price age
        """

        try:
            price = response.xpath("//div[@class='price_display']").xpath("//span[@class='price ']/text()").get()
            price="$"+price
        except:
            price = ""
        name =  response.xpath("//div[@class='col8 acol12']/h1/text()").extract()[0]
        breadcrumb_list = response.xpath("//div[@aria-label='breadcrumb']/ul/li/text()").getall()
        index = str(len(breadcrumb_list)-1)
        dolls_category = response.xpath("//div[@aria-label='breadcrumb']/ul/li["+index+"]/a/text()").extract()[0]
        description = []
        des1 = response.xpath('//div[@class="text-container scroll-pane panel-collapse collapse in"]/p/text()').extract()
        des2 = (response.xpath('//div[@class="text-container scroll-pane panel-collapse collapse in"]/ul/li/text()').extract())
        description = des1 + des2
        age_xpath = response.xpath("//div[@class='col8 acol12']/h5/text()").extract()
        age = age_xpath[0].replace("\r","").replace("\n","").replace("\t","").split()[1]
        model_sku = response.xpath("//div[@class='text-container scroll-pane panel-collapse collapse in']/h6/text()").extract()[0]
        breadcrumb_list = "Dolls | "+ dolls_category+" | "+name
        image_url = response.xpath("//meta[@property='og:image']/@content").get()
        status_code = response.status
        item_data={
            "model":name,
            "price":str(price),
            "breadcrumb":breadcrumb_list,
            "category" : "Fashion Dolls",
            "id":response.url,
            "brand": "Barbie",
            "age": str(age),
            "image":image_url,
            "description":description,
            "model_sku":model_sku,
            'crawl_date': (datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            'status_code':str(status_code)
        }
        # print(response.meta)
        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            if filter_name=="DOLLS":
                meta=meta.fromkeys([filter_name],dolls_category)
            elif filter_name == "BRAND":
                meta = meta.fromkeys([filter_name],response.meta[filter_name])

            item_data.update(meta)

        yield item_data
