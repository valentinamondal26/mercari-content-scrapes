import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = '.'.join(description )
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            details = record.get('details', [])
            title_info = record.get('title_info', '')

            length = ''
            width = ''
            height = ''
            pattern = re.compile(r"\(\s*\w+\s*x\s*\w+\s*x\s*\w+\s*\)")
            index = 0
            for d in details:
               if re.findall(pattern, d):
                   index = details.index(d)
                   break
            color = ', '.join(record.get('colors', ''))
            if not color:
                color = record.get('COLORS', '')
            if not color:
                match = re.findall(r'\bin\s(.*)\s-', title_info)
                if match:
                    color = ', '.join(match)

            material = ', '.join(record.get('materials', ''))
            if not material:
                material = record.get('MATERIALS', '')
            materials_meta = [
                'Monogram Canvas', 'Damier Canvas', 'Canvas', 'Epi Leather', 'Leather',
                'Damier Ebene', 'Crocodile', 'Lambskin', 'Sheepskin', 'Snakeskin', 'PVC',
                'VVN',
            ]
            if not material:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), title_info)
                if match:
                    material = ', '.join(match)
            if not material:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), ', '.join(details))
                if match:
                    material = ', '.join(match)
            material = material.title()
            material = ', '.join(list(dict.fromkeys(material.split(', '))))
            material = re.sub(re.compile(r"Vvn", re.IGNORECASE), "VVN", material)

            if index > 0:
                measurements = details[index-1]
                measurements_pattern = details[index]
                measurements_pattern = re.sub(r'\(|\)|\xa0', '', measurements_pattern).split('x')
                measurements_pattern = [mp.strip() for mp in measurements_pattern]
                measurements = re.sub(r'\(|\)|\xa0', '', measurements).split('x')
                measurements = [m.strip().strip('inches') for m in measurements]

                for measure in measurements:
                    if measurements_pattern[measurements.index(measure)] == "Length":
                        length = measure
                    elif measurements_pattern[measurements.index(measure)] == "Height":
                        height = measure
                    elif measurements_pattern[measurements.index(measure)] == "Width":
                        width = measure

            for word in re.findall(r'\w+', record.get('MATERIALS', '')):
                pattern = re.compile(r'\b{}\b'.format(word),re.IGNORECASE)
                model = re.sub(pattern, '', model)

            model = re.sub(re.compile(r"EXCLUSIVE PRELAUNCH", re.IGNORECASE), '', model)
            model = model.title()
            d = {
                'Mm': 'MM',
                'Gm': 'GM',
                'Pm': 'PM',
                'Xsmall': 'XSmall',
                'Xxl': 'XXL',
                'Lv': 'LV',
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)
            model = re.sub(re.compile(r"(\d+\s*)Ml\b", re.IGNORECASE),r"\1ML",model)
            model = re.sub(re.compile(r"(\d+\s*)R\b", re.IGNORECASE),r"\1R",model)
            model = re.sub('All-In', 'All In', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            if "Sleep Mask" in record.get('title',''):
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),

                "category": record.get('category', ''),
                "brand": record.get('brand',''),

                #"breadcrumb":record.get('breadcrumb',''),
                "title": record.get('title','').upper(),
                "image": record.get('image',''),
                "ner_query": ner_query.replace('\n',''),
                "description": '.'.join(description).replace('\n',''),

                "model": model,
                "msrp": {
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "model_sku": record.get('model_sku',''),
                "color": color,
                "material": material,
                "bag_type": record.get('CATEGORIES',''),
                "height": height,
                "length": length,
                "width": width,

            }

            return transformed_record
        else:
            return None
