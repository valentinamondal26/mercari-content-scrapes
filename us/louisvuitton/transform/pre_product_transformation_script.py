import hashlib
import re
import json
class Mapper(object):
    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawlDate','')
            status_code = record.get('statusCode','')
            category = record.get('category','')
            brand = record.get('brand','')
            image = record.get('image','')
            description = record.get('description','')
            title = record.get('title','').replace(brand,"")
            color = record.get('color','')
            model=' '.join(title.replace(brand,'').split())
            sku=record.get('model','')
            detailed_features=record.get('detailed_features','')
            if '$' in record.get('price',''):
                price=record.get('price','').split('$')[1]
            else:
                price=record.get('price','')

            ner_query = description + detailed_features

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": category,
                "brand": brand,
                "image": image,
                "sku":sku,
                "description": description,
                "ner_query": ner_query,
                "title": title,
                "color": color,
                "model":model,
                "detailed_features":detailed_features,
                "material":"Leather,Canvas,Suede,Velvet,Satin,Fur,Shearling,Rubber,Acrylic,Wicker & Straw,Other",
                "price":{
                    "currencyCode": "USD",
                    "amount": price.replace("$","")
                }
            }
            return transformed_record
        else:
            return None
