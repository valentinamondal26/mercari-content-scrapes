'''
https://mercari.atlassian.net/browse/USDE-568 - Louis Vuitton Luggage and Accessories
'''

# -*- coding: utf-8 -*-
import scrapy
import json
import datetime
import re
import os
import html
import random
from scrapy.exceptions import CloseSpider



class LouisvuittonSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from us.louisvuitton.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags
        2) Luggage and Accessories

    brand : str
        category to be crawled, Supports
        1) Louis Vuitton

    Command e.g:
    scrapy crawl louisvuitton -a category='Shoulder Bags' -a brand="Louis Vuitton"
    scrapy crawl louisvuitton -a category='Luggage and Accessories' -a brand="Louis Vuitton"
    """

    name = "louisvuitton"

    source = 'louisvuitton.com'
    blob_name = 'louisvuitton.txt'

    category = None
    brand = None

    merge_key = 'id'

    filters = None

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    base_url = "https://us.louisvuitton.com/eng-us"

    url_dict = {
        'Shoulder Bags': {
            "Louis Vuitton": {
                'url': 'https://us.louisvuitton.com/eng-us/women/handbags/shoulder-bags-totes/_/N-fy3mxw',
                "filters": ['COLORS'],
            }
        },
        'Luggage and Accessories':{
            "Louis Vuitton": {
                'url': 'https://us.louisvuitton.com/eng-us/women/travel/all-luggage-and-accessories/_/N-18ffell',
                "filters": ['COLORS','CATEGORIES','MATERIALS'],
            }
        }
    }

    api = 'https://us.louisvuitton.com/ajax/endeca/browse-frag/{}?storeLang=eng-us&pageType=category&Nrpp={}&showColor=true&No=0'

    # api_dict = {
    #     'Shoulder Bags':'https://us.louisvuitton.com/ajax/endeca/browse-frag/{}?storeLang=eng-us&pageType=category&Nrpp={}&showColor=true&No=0',
    #     'Luggage':'https://us.louisvuitton.com/ajax/endeca/browse-frag/{}?storeLang=eng-us&pageType=category&Nrpp={}&showColor=true&No=0'
    # }

    def __init__(self, category=None,brand=None, *args, **kwargs):
        super(LouisvuittonSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("Category should not be None")
            raise CloseSpider('category not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category,{ }).get(brand, {}).get('filters', None)
        # self.api=self.api_dict.get(category, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['COLORS','CATEGORIES','MATERIALS']
        self.launch_url = 'https://us.louisvuitton.com/eng-us/women/travel/all-luggage-and-accessories/_/N-18ffell'


    def start_requests(self):
        if  self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self,response):
        """ 
        @url https://us.louisvuitton.com/eng-us/women/travel/all-luggage-and-accessories/_/N-18ffell
        @meta {"use_proxy":"True"}
        @returns requests 2
        """

        ##10 is dummy count this crawler is broken
        product_count=response.xpath('//span/@data-products-count').get(default='') or \
            re.findall(r'\d+', response.xpath('//span[@class="lv-category__count"]/text()').get(default='0'))[0]
        print(f'==>product_count:{product_count}')
        if self.filters:
            for filter in self.filters:
                if filter=="COLORS" or filter=="MATERIALS":
                    filter_urls=response.xpath('//div[h2[@class="facetgroup-title"]/button[contains(text(),"{}")]]/following-sibling::div//li/button/@onclick'.format(filter)).getall()
                    pattern=r"location.href=|\'"
                    filter_urls=[re.sub(pattern,'',url) for url in filter_urls]
                    filter_values=response.xpath('//div[h2[@class="facetgroup-title"]/button[contains(text(),"{}")]]/following-sibling::div//li/button/img/@alt'.format(filter)).getall()
                    filter_values=[val.strip() for val in filter_values]
                else:
                    filter_urls=response.xpath('//div[h2[@class="facetgroup-title"]/button[contains(text(),"{}")]]/following-sibling::div//li/a/@href'.format(filter)).getall()
                    filter_values=response.xpath('//div[h2[@class="facetgroup-title"]/button[contains(text(),"{}")]]/following-sibling::div//li/a/text()'.format(filter)).getall()
                    filter_values=[val.strip() for val in filter_values]

                for url in filter_urls:
                    meta={}
                    index=filter_urls.index(url)
                    meta=meta.fromkeys([filter],filter_values[index])
                    meta.update({'filter_name':filter})
                    meta.update({'be':url})
                    url=self.api.format(url[1:],product_count)
                    meta.update({'url':url})
                    yield self.createRequest(url=url,callback=self.parse,meta=meta)

        if self.api:
            url_part=self.launch_url.split(self.base_url)[1]
            yield self.createRequest(url=self.api.format(url_part,product_count),callback=self.parse,meta=response.meta)


    def parse(self,response):
        """
        @url https://us.louisvuitton.com/eng-us/women/travel/all-luggage-and-accessories/_/N-18ffell
        @returns requests 1
        """

        all_links=response.xpath('//li[@class="productItem"]/a/@href').getall()
        all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
        for link in all_links:
            link=link.replace('eng-us/','',1)
            yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta) 


    def parse_product(self,response):
        """
        @url https://us.louisvuitton.com/eng-us/products/horizon-soft-duffle-65-damier-graphite-nvprod2140036v
        @scrape_values title_info title model_sku image price description details colors materials
        """

        item={}
        item['crawlDate']=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item['statusCode']=str(response.status)
        item['id']=response.url
        item['category']=self.category
        item['brand']=self.brand
        item['title_info']=response.xpath('//title/text()').extract_first(default='')
        item['title']=response.xpath('//h1[@class="productName"]/text()').get(default='')
        item['model_sku']=response.xpath('//span[@class="sku"]/text()').get(default='')
        item['image']=response.xpath('//picture[@id="productMainImage"]/source/@data-src').get(default='')
        item['price']=response.xpath('normalize-space(//div[@class="priceValue"]/text())').get(default='')
        item['description']=response.xpath('//div[@id="productDescription"]/div//text()').getall()

        if not item['title']:
            js=json.loads(response.xpath('//script[contains(text(),"priceCurrency")]/text()').get().replace("\n","").strip()) 
            item['title']=js.get('name','')
            item['model_sku']=js.get('sku','')
            item['price']=js.get('price','')
            item['image']=js.get('image','')
            item['description']=html.unescape(js.get('description',''))
        details=response.xpath('//div[@id="productFeatures"]/div/bdo//text()').getall()
        details=[d for d in details if re.findall(r"^[\s\n]+$",d)==[]]
        item_details=response.xpath('//div[@id="productFeatures"]/div/ul/li/text()').getall()
        [item_details.append(d) for d in details]
        item['details']=item_details
        item['colors']=response.xpath('//div[@class="panel-content"][h1[contains(text(),"Colors")]]//ul/li//span[@class="name"]/text()').getall()
        item['materials']=response.xpath('//div[@class="panel-content"][h1[contains(text(),"Material")]]//ul/li//span[@class="name"]/text()').getall()
        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)

        yield item


    def createRequest(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)
        if headers:
            request.headers.update(headers)

        request.meta.update({"use_cache":True})
        return request
