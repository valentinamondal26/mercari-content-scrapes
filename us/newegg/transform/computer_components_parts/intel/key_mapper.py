class Mapper:
    def map(self, record):

        model = ''
        product_type = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "product_type" == entity['name']:
                product_type = entity["attribute"][0]["id"]

        key_field = product_type + model
        return key_field
