import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            title = record.get('title', '')

            description = ', '.join(record.get(
                'description', [])).replace('\r\n', '')
            description = re.sub(r'\s+', ' ', description).strip()

            product_type_meta = ['Any Category', 'CPUs \\/ Processors', 'Processors \\- Desktops', 'Motherboards', 'Intel Motherboards', 'Wired Networking', 'Network Interface Cards', 'Accessories \\- Controller cards Add\\-on cards Ect', 'Accessories Barebone', 'Action Camera Accessories', 'Add\\-On Cards', 'Airsoft Cases \\& Storage', 'AMD Motherboards', 'Antivirus \\& Internet Security', 'Audio \\/ Video Accessories', 'Aviation GPS', 'Backup Drives', 'Backup Media', 'Barebone PCs', 'Blenders', 'Blu\\-Ray Media', 'Bluetooth Adapters', 'Bluetooth Headsets \\& Accessories', 'Bookshelves', 'Business \\& Tax Forms', 'Cable Management', 'Cables \\- Automotive', 'Card Readers', 'Case Accessories', 'Case Fans', 'CCTV \\/ Analog Cameras', 'CD \\/ DVD \\/ Blu\\-Ray Media', 'CD \\/ DVD Accessories', 'CD \\/ DVD Burners', 'CD \\/ DVD Drives', 'Cell Phones \\- No Contract \\& Prepaid', 'Computer Cases', 'Computer Power Adapter Cords', 'Computer Power Cords', 'Connectors', 'Controllers \\/ RAID Cards', 'Cooling Device Accessories', 'CPU Accessories', 'CPU Fans \\& Heatsinks', 'Dehumidifiers', 'Desktop Computers', 'Desktop Internal Hard Drives', 'Desktop Memory', 'Desktop NAS', 'Device Server', 'DLP Replacement Lamps', 'DVI Cables', 'E\\-Book Accessories', 'Electrical Boxes', 'Electronics', 'Electronics for Kids', 'Embedded Solutions', 'Enterprise SSDs', 'Extenders \\& Repeaters', 'Factory Integration', 'Fiber Optic Cables', 'File Cabinets', 'File Folders \\& Accessories', 'Firewalls \\/ Security Appliances', 'Gadgets', 'Gaming Laptops', 'Genuine Tablet Accessories', 'Gifts', 'Hard Drive Adapters', 'Hard Drive Enclosures', 'Headphones \\& Accessories', 'Headsets \\& Accessories', 'Home Safety \\& Security', 'Home Theater Projectors', 'Hubs', 'IDE \\/ SATA \\/ SCSI \\/ Floppy Cables', 'IDE Cables', 'Industrial Single Board Computers',
                                 'Ink Cartridges \\(Aftermarket\\)', 'Ink Cartridges \\(Genuine Brands\\)', 'Internal Power Cables', 'Internal SSDs', 'Keyboards', 'Laptop Batteries \\/ AC Adapters', 'Laptop Cases \\& Bags', 'Laptop Cooling Pads', 'Laptop Internal Hard Drives', 'Laptop Networking', 'Laptops \\/ Notebooks', 'Mac Hard Drives', 'Memory \\& Chipset Cooling', 'Memory Cards', 'Mini PCs \\/ Thin Client', 'Mini\\-PC Barebone', 'Mobile Computing Accessories', 'Modems \\/ Gateways', 'Monitor Accessories', 'Motherboard Accessories', 'Mounts \\& Holders', 'Mouse Pads \\& Accessories', 'Mouse\\/Keyboard \\(PS2\\) Adapters', 'Network Connectors\\/Adapters', 'Network Ethernet Cables', 'Network Transceivers', 'Other Adapters \\& Gender Changers', 'Other Computer Accessories', 'PC Tools \\& Testers', 'PDA Accessories', 'Personal Protective Equipment', 'Portable External Hard Drives', 'POS Accessories', 'Power Adapters', 'Power Cables', 'Power Distribution Unit', 'Power Supplies', 'Powerline Networking', 'Printer \\& Scanner Supplies', 'Pro  Sound', 'Processors \\- Mobile', 'Processors \\- Servers', 'Professional Video Devices', 'RAID Enclosure \\/ Subsystems', 'RAM Heatspreaders \\& Heatsinks', 'SATA \\/ eSATA Cables', 'SCSI \\/ SAS \\/ InfiniBand Cables', 'Security Locks \\& Accessories', 'Semiconductors', 'Serial Cables', 'Server \\& Workstation Systems', 'Server Accessories', 'Server Barebones', 'Server Chassis', 'Server Motherboards', 'Server Power Supplies', 'Server Racks \\/ Cabinets', 'SSD \\/ HDD Accessories', 'Standard Batteries \\& Chargers', 'Surveillance Security Systems', 'Switch Modules', 'Switches', 'Thermal Compound \\/ Grease', 'Toner Cartridges \\(Aftermarket\\)', 'TV Accessories', 'USB Cables', 'VGA \\/ SVGA Cables', 'Video Card Accessories', 'VoIP', 'Water \\/ Liquid Cooling', 'Web Cams', 'Whole Home \\/ Mesh Wifi', 'Wire Cap Connectors', 'Wired Accessories', 'Wired Headsets \\& Speakers', 'Wired Routers', 'Wireless Accessories', 'Wireless Adapters', 'Wireless AP', 'Wireless Routers']
            product_type = record.get('Department', [])
            if not product_type:
                product_type = re.findall(re.compile(
                    r"|".join(product_type_meta), re.IGNORECASE), title)
            if not product_type:
                if re.findall(re.compile(r"Desktop Processor|Processor", re.IGNORECASE), title):
                    product_type = ["Desktop Processor"]
            product_type = ", ".join(product_type)
            product_type = re.sub(re.compile(
                r"Processors \- Desktops", re.IGNORECASE), "Desktop Processor", product_type)

            series_meta = ['Intel Core i9', 'Core i9', 'Core i9 10th Gen', 'Core i9 9th Gen', 'Core i9 X\\-Series', 'Core i9 X\\-Series Extreme Edition', 'Intel Core i7', 'Core i7', 'Core i7 2nd Gen', 'Core i7 3rd Gen', 'Core i7 4th Gen', 'Core i7 5th Gen', 'Core i7 6th Gen', 'Core i7 7th Gen', 'Core i7 8th Gen', 'Core i7 9th Gen', 'Core i7 10th Gen', 'Core i7 Extreme Edition', 'Core i7 X\\-Series', 'Intel Core i5', 'Core i5', 'Core i5 2nd Gen', 'Core i5 3rd Gen', 'Core i5 4th Gen', 'Core i5 5th Gen', 'Core i5 6th Gen', 'Core i5 7th Gen',
                           'Core i5 8th Gen', 'Core i5 9th Gen', 'Core i5 10th Gen', 'Core i5 X\\-Series', 'Intel Core i3', 'Core i3', 'Core i3 2nd Gen', 'Core i3 3rd Gen', 'Core i3 4th Gen', 'Core i3 6th Gen', 'Core i3 7th Gen', 'Core i3 8th Gen', 'Core i3 9th Gen', 'Core i3 10th Gen', 'Intel Celeron \\/ Pentium', 'Celeron', 'Celeron D', 'Celeron Dual\\-Core', 'Pentium', 'Pentium 4', 'Pentium D', 'Pentium Dual\\-Core', 'Pentium Gold', 'Pentium M', 'Other Intel CPUs', 'Core 2 Duo', 'Core 2 Extreme', 'Core 2 Quad', 'Core Duo', 'Xeon']

            brand = record.get('brand', '')
            series = record.get('Series', []) \
                or [record.get('specs', {}).get('Model', {}).get('Series', '')]
            series = [x for x in series if 'intel' not in x.lower()]

            if not series:
                series = re.findall(re.compile(
                    r"|".join(series_meta), re.IGNORECASE), title)

            frequency = record.get('Operating Frequency', [])
            frequency = [x for x in frequency if not(
                ' - ' in x or 'and higher' in x.lower())]

            model = record.get('title', '')
            # keywords = series
            # model = re.sub(re.compile(r'|'.join(
            #     list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = re.sub(r'\(\s*\)', '', model)
            model = re.sub(re.compile(
                r"\bMOTHBD\b|\bMOTHERBOARD\b", re.IGNORECASE), "Motherboard", model)
            model = re.sub(re.compile(r"^\W+"), "", model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": description or title,
                "title": title,
                "image": record.get('image', ''),

                "description": description,
                "model": model,
                "product_type": product_type,
                "product_line": ', '.join(series),
                "number_of_cores": ', '.join(record.get('# of Cores', [])),
                "frequency": ', '.join(frequency),
                "memory_type": ', '.join(record.get('Memory Types', []))
                or record.get('specs', {}).get('Details', {}).get('Memory Types', ''),
                "cooling_type": ', '.join(record.get('Cooling Device', []))
                or record.get('specs', {}).get('Details', {}).get('Cooling Device', ''),
                "graphics_processing_type": ', '.join(record.get('Integrated Graphics', []))
                or record.get('specs', {}).get('Details', {}).get('Integrated Graphics', ''),

                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
