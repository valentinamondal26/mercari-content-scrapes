'''
https://mercari.atlassian.net/browse/USDE-802 - Intel Computer Components & parts
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider

import os
import re
import json
import requests
from urllib.parse import urlparse, parse_qs, urlencode

class NeweggSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from newegg.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Computer Components & Parts
    
    brand : str
        brand to be crawled, Supports
        1) Intel

    Command e.g:
    scrapy crawl newegg -a category='Computer Components & Parts' -a brand='Intel'
    """

    name = 'newegg'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'newegg.com'
    blob_name = 'newegg.txt'

    extra_item_infos = {}

    url_dict = {
        'Computer Components & Parts': {
            'Intel': {
                'url': 'https://www.newegg.com/p/pl?d=Intel&N=50001157%20100&name=Any%20Category',
                'filters': ['Department', 'Series', '# of Cores', 'Operating Frequency',
                    'Memory Types', 'Cooling Device', 'Integrated Graphics',],
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(NeweggSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])

        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.filters = ['Department', 'Series', '# of Cores', 'Operating Frequency',
                    'Memory Types', 'Cooling Device', 'Integrated Graphics',]


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)
        if self.filters:
            yield self.create_request(url=self.launch_url, callback=self.parse_filters, dont_filter=True)


    def crawlDetail(self, response):

        model_sku = response.xpath('//input[@id="hiddenItemNumber"]/@value').extract_first()
        price = re.findall(r'product_sale_price:\[\'(.*?)\'\],', response.xpath('//script[contains(text(), "utag_data =")]/text()').extract_first().replace('\n', ''))[0]
        if not price:
            r = requests.get(f'https://www.newegg.com/LandingPage/ItemInfo4ProductDetail2016.aspx?Item={model_sku}') 
            price = json.loads(re.findall(r'rawItemInfo=(.*?);', r.text)[0])['mainItem']['unitPrice']

        specs = {} 
        for fieldset in response.xpath('//div[@id="Details_Content"]/div[@id="detailSpecContent"]/div[@id="Specs"]/fieldset'): 
            spec_name = fieldset.xpath('./h3[@class="specTitle"]/text()').extract_first() 
            a_spec = {} 
            for dl in fieldset.xpath('./dl'): 
                a_spec.update({dl.xpath('./dt/text()').extract_first(): dl.xpath('./dd/text()').extract_first()}) 
            specs[spec_name] = a_spec 

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//ol[@id="baBreadcrumbTop"]//li').xpath('.//text()[normalize-space(.)]').extract(),
            'title': response.xpath(u'normalize-space(//h1[@id="grpDescrip_h"]/span/text())').extract_first(),
            'description': response.xpath('//div[@id="Overview_Content"]/div[@class="itemDesc"]/p').xpath('./text()').extract(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'model_sku': model_sku,

            'price': price,
            'specs': specs,
        }


    def parse_filters(self, response):
        """
        @url https://www.newegg.com/p/pl?d=Intel&N=50001157%20100&name=Any%20Category
        @meta {"use_proxy": "True"}
        @returns requests 7
        """
        ##No of requests mentioned in contracts is based on the length of the filters(Minimum requests).     

        for filter in response.xpath('//dl[contains(@class, "filter-box")]'):
            filter_type = filter.xpath('.//dt[@class="filter-box-title"]/text()').extract_first()
            if filter_type in self.filters:
                for f in filter.xpath('.//ul[@class="filter-box-list"]/li'):
                    filter_name = f.xpath('.//label/span[@class="form-checkbox-title"]/text()').extract_first() or f.xpath('./a/@title').extract_first()
                    filter_url = f.xpath('.//label/input/@neg-n-value').extract_first() or f.xpath('./a/@href').extract_first()
                    if 'Department' != filter_type:
                        parts = urlparse(response.url)
                        query_dict = parse_qs(parts.query)
                        n_value = query_dict.get('N', [''])
                        n_value = ' '.join(list(dict.fromkeys(f'{n_value[0]} {filter_url}'.split(' '))))
                        query_dict.update({'N':[n_value]})
                        filter_url = parts._replace(query=urlencode(query_dict, True)).geturl()

                    request = self.create_request(url=filter_url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                        filter_type: filter_name
                    }
                    yield request



    def parse(self, response):
        for product in response.xpath('//div[@class="items-view is-grid"]/div[contains(@class, "item-container")]'):

            id = product.xpath('.//a[@class="item-title"]/@href').extract_first()

            if id:
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.update_extra_item_infos(id, response.meta.get('extra_item_info', {}))
                request = self.create_request(url=id, callback=self.crawlDetail)
                yield request

        if response.xpath('//button[@class="btn" and @aria-label="Next" and not(@disabled)]').extract_first():
            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            start = query_dict.get('Page', [0])
            query_dict.update({'Page':[int(start[0]) + 1]})
            next_page = parts._replace(query=urlencode(query_dict, True)).geturl()

            yield self.create_request(url=next_page, callback=self.parse)


    def update_extra_item_infos(self, id, extra_item_info): 
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request