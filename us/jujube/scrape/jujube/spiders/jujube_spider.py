'''
https://mercari.atlassian.net/browse/USCC-146 - Ju-Ju-Be Diaper Bags
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import json
from parsel import Selector


class JujubeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from jujube.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Diaper Bags
    
    brand : str
        brand to be crawled, Supports
        1) Ju-Ju-Be

    Command e.g:
    scrapy crawl jujube -a category='Diaper Bags' -a brand='Ju-Ju-Be'
    """

    name = 'jujube'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'jujube.com'
    blob_name = 'jujube.txt'

    url_dict = {
        'Diaper Bags': {
            'Ju-Ju-Be': {
                'url': 'https://www.jujube.com/collections/diaper-bags'
            }
        }
    }

    base_url = 'https://www.jujube.com'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(JujubeSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://jujube.com/collections/diaper-bags/products/b-f-f-diaper-bag-social-butterfly
        @scrape_values title description tags price  image features dimensions_care description_features variants
        @meta {"use_proxy":"True"}
        """

        j = json.loads(response.xpath('//script[@id="ProductJson-product-template"]/text()').extract_first())
        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': j['title'],
            'description': Selector(j['description']).xpath('//p/text()').extract_first(default=''),
            'tags': j['tags'],
            'price': '{0:.2f}'.format(j['price']/100) if j['price'] else '',
            'variants': [{
                    'sku': variant['sku'], 
                    'barcode': variant['barcode'], 
                    'name': variant['name'], 
                    'price': variant['price'], 
                    'weight': variant['weight'], 
                } for variant in j['variants']],
            'image': self.base_url + j['featured_image'],
            # 'features': {re.sub(r'–|\xa0', '', a.xpath('./strong/text()').extract_first(default='')).strip(): a.xpath('./text()').extract_first(default='') for a in response.xpath('//div[contains(@class,"accordion-tab")]/h3[text()="Features"]/following-sibling::div[@class="accordion-tab-content"]/ul/li')},
            # 'dimensions_care': {re.sub(r'–|\xa0', '', a.xpath('./strong/text()').extract_first(default='')).strip(): a.xpath('./text()').extract_first(default='') for a in response.xpath('//div[contains(@class,"accordion-tab")]/h3[text()="Dimensions & Care"]/following-sibling::div[@class="accordion-tab-content"]/ul/li')},
            # 'features': {a.split('–', 1)[0].strip(): a.split('–', 1)[-1].strip('\xa0').strip() for a in response.xpath('//div[contains(@class,"accordion-tab")]/h3[text()="Features"]/following-sibling::div[@class="accordion-tab-content"]/ul/li//text()').extract()},
            # 'dimensions_care': {a.split('–', 1)[0].strip(): a.split('–', 1)[-1].strip('\xa0').strip() for a in response.xpath('//div[contains(@class,"accordion-tab")]/h3[text()="Dimensions & Care"]/following-sibling::div[@class="accordion-tab-content"]/ul/li//text()').extract()},
            'features': {re.sub(r'–|\xa0', '', a.xpath('./strong/text()').extract_first(default='')).strip() if a.xpath('./strong') else re.compile(r'[-–]').split(a.xpath('./text()').extract_first(), 1)[0].strip() : a.xpath('./text()').extract_first(default='') if a.xpath('./strong') else re.compile(r'[-–]').split(a.xpath('./text()').extract_first(), 1)[-1].strip('\xa0').strip() for a in response.xpath('//div[contains(@class,"accordion-tab")]/h3[text()="Features"]/following-sibling::div[@class="accordion-tab-content"]/ul/li')},
            'dimensions_care': {re.sub(r'–|\xa0', '', a.xpath('./strong/text()').extract_first(default='')).strip() if a.xpath('./strong') else re.compile(r'[-–]').split(a.xpath('./text()').extract_first(), 1)[0].strip() : a.xpath('./text()').extract_first(default='') if a.xpath('./strong') else re.compile(r'[-–]').split(a.xpath('./text()').extract_first(), 1)[-1].strip('\xa0').strip() for a in response.xpath('//div[contains(@class,"accordion-tab")]/h3[text()="Dimensions & Care"]/following-sibling::div[@class="accordion-tab-content"]/ul/li')},
            'description_features': {re.sub(r'-|–|\xa0', '', a.xpath('./strong/text()').extract_first(default='')).strip() if a.xpath('./strong') else re.compile(r'[-–]').split(a.xpath('./text()').extract_first(), 1)[0].strip() : ''.join(a.xpath('./text()[normalize-space(.)]').extract()) if a.xpath('./strong') else re.compile(r'[-–]').split(a.xpath('./text()').extract_first(), 1)[-1].strip('\xa0').strip() for a in response.xpath('//div[contains(@class,"accordion-tab")]/p[./strong/text()="Features & Specifications"]/following-sibling::ul/li')},
        }


    def parse(self, response):
        """
        @url https://jujube.com/collections/diaper-bags
        @returns requests 49
        @meta {"use_proxy":"True"}
        """

        # Number of request mentioned in contracts is 49 beacause 48 items per page and 1 Next Page url
        for product in response.xpath('//div[@class="products products-grid "]/div[@class="box product"]'):

            id = product.xpath('.//a/@href').extract_first(default='')

            if id:
                request = self.create_request(url=self.base_url + id, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('//link[@rel="next"]/@href').extract_first(default='')
        if nextLink:
            yield self.create_request(url=self.base_url + nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

