class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            if "color" == entity['name']:
                if entity["attribute"]:
                    for attribute in entity["attribute"]:
                        color = color + attribute.get("id", '')

        key_field = model + color
        return key_field
