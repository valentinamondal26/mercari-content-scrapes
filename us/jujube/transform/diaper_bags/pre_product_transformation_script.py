import hashlib
import re
import enum

class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            width = ''
            height = ''
            depth = ''
            dimensions = record.get('dimensions_care', {}).get('Dimensions', '') or record.get('description_features', {}).get('Dimensions', '')
            match = re.findall(re.compile(r'(.*)W\s*x\s*(.*)H\s*x\s*(.*)D', re.IGNORECASE), dimensions)
            if match:
                width = match[0][0]
                height = match[0][1]
                depth = match[0][2]

            tags = record.get('tags', [])
            colors = list(map(lambda x: x.replace('Color_', ''), filter(lambda x: x.startswith('Color_'), tags)))
            colors.sort()
            themes = list(map(lambda x: x.replace('Theme_', ''), filter(lambda x: x.startswith('Theme_'), tags)))
            themes.sort()
            types = list(map(lambda x: x.replace('Type_', '').rstrip('s'), filter(lambda x: x.startswith('Type_'), tags)))

            product_type_meta = ['Backpack', 'Tote Bag', 'Duffel Bag', 'Breast Pump Bag', 'Messenger Bag', 'Purse', 'Dad Bag']
            if not types:
                match = re.findall('|'.join(list(map(lambda x: r'\b'+x+r'\b', product_type_meta))), description, flags=re.IGNORECASE)
                if match:
                    types = list(map(lambda x: x.title(), match))

            class Product_Type(enum.Enum):
                backpack = 1
                tote_bag = 2
                duffel_bag = 3
                breast_pump_bag = 4
                messenger_bag = 5
                purse = 6
                dad_bag = 7

            def convert_to_snake_case(string):
                return re.sub(r'\s+', '_', string.lower())

            def product_type_comparator(x):
                value = 0
                try:
                    value = Product_Type[convert_to_snake_case(x)].value
                except:
                    pass
                return value

            types = sorted(types, key=lambda x: product_type_comparator(x))

            product_type = types[0] if types else ''

            color = ''
            model = record.get('title', '')
            model_split = model.rsplit('-', 1)
            if len(model_split) == 2:
                model = model_split[0].strip()
                color = model_split[1].strip()

            model = re.sub(r'\bTote$', 'Tote Bag', model, flags=re.IGNORECASE)
            if not re.findall(r'\b'+product_type+r'\b', model, flags=re.IGNORECASE):
                model = f'{model} {product_type}'
            model = re.sub(r'\s-\s', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r'\bbundle\b|\bset\b', re.IGNORECASE), model):
                return None


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                'color': color,
                "material": record.get('features', {}).get('Fabrics', '') or record.get('description_features', {}).get('Fabrics', ''),
                'model_sku': record.get('variants', [{}])[0].get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'theme': ', '.join(themes),
                'product_type': product_type,
                'width': width,
                'height': height,
                'depth': depth,
                'weight': record.get('dimensions_care', {}).get('Weight', '')  or record.get('description_features', {}).get('Weight', ''),
            }

            return transformed_record
        else:
            return None
