'''
https://mercari.atlassian.net/browse/USCC-646 - Build-A-Bear Workshop Stuffed Animals
'''

import os
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import urllib

class BuildabearSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from buildabear.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stuffed Animals

    brand : str
        brand to be crawled, Supports
        1) Build-A-Bear Workshop

    Command e.g:
    scrapy crawl buildabear -a category='Stuffed Animals' -a brand='Build-A-Bear Workshop'
    """

    name = 'buildabear'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    source = 'buildabear.com'
    blob_name = 'buildabear.txt'

    base_url = 'https://www.buildabear.com'

    url_dict = {
        'Stuffed Animals': {
            'Build-A-Bear Workshop': {
                'url': 'https://www.buildabear.com/stuffed-animals',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BuildabearSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)
            yield self.create_request(url=self.launch_url, callback=self.parseFilter)


    def crawlDetail(self, response):
        """
        @url https://www.buildabear.com/online-exclusive-barkleigh/029146.html?cgid=stuffed-animals
        @meta {"use_proxy":"True"}
        @scrape_values id title image price description specs.SKU
        """

        specs = {}
        for tr in response.xpath("//div[@id='hor_1_tab_item-1']/div/table/tbody/tr"):
            key = tr.xpath("./th/text()").get()
            value = tr.xpath("./td/text()").get()
            specs[key] = value

        for span in response.xpath("//div[@id='hor_1_tab_item-1']/div/table/tbody/tr//ul[@class='product-attributes list']/li"):
            key = span.xpath("./span[@class='label']/text()").get().replace(": ","")
            value = span.xpath("./span[@class='value']/text()").get().replace("\n","")
            specs[key] = value
        
        specs_table_data = response.xpath("//div[@id='product-set-list-2']/div/div/div/div[@class='product-main-attributes']/table")
        for res, index in zip(specs_table_data, range(0, len(specs_table_data))):
            item = f'item_{str(index)}'
            specs[item]={}
            for b in res.xpath(".//tr"):
                key = b.xpath(".//th/text()").get(default='')
                value = b.xpath(".//td/text()").get(default='').strip() or b.xpath(".//td/ul/li/span/text()").getall()
                specs[item][key]=value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath("//h1[@class='pdp-name large-desktop-only']/text()").get(default='') or \
                     response.xpath("//h1[@class='product-name large-desktop-only']/text()").get(default=''),
            'image': response.xpath("//img[@class='primary-image']/@src").get(default=''),
            'price': response.xpath("//span[@class='price-sales']/span[@itemprop='price']/text()").get(default=''),
            'specs': specs,
            'description': response.xpath("//div[@id='hor_1_tab_item-0']/div/text()").get(default='').replace('\n',''),
            'filter_color': response.meta.get('color','')
        }


    def parseFilter(self, response):
        """
        @url https://www.buildabear.com/stuffed-animals
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        filters = list(set(response.xpath("//ul[@class='clearfix swatches color']/li/a/text()").getall()))
        filters.remove("\n")
        for filter in filters:
            url_filter = filter
            url_filter.replace("\n", "").replace(" ", "%20").replace("/", "%2F")
            url = f'{response.url}?' + urllib.parse.urlencode({'prefn1':'color', 'prefv1':url_filter})
            yield self.create_request(url=url, callback=self.parse, meta={'color': filter.replace("\n", "")})


    def parse(self, response):
        """
        @url https://www.buildabear.com/stuffed-animals
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        size = 18
        total_results = int(response.xpath("//div[@class='results-hits']/text()").get().split(' ')[0])
        for start in range(0, total_results, size):
            if response.meta.get('color','') == '':
                url = f'{response.url}?'+urllib.parse.urlencode({'start':start, 'sz': size})
            else:
                url = f'{response.url}&'+urllib.parse.urlencode({'start':start, 'sz': size})
            yield self.create_request(url=url,callback=self.parseProductUrl,meta=response.meta)


    def parseProductUrl(self, response):
        """
        @url https://www.buildabear.com/stuffed-animals?start=0&sz=18
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for url in response.xpath("//ul[@id='search-result-items']/li/div/div/a/@href").getall():
            yield self.create_request(url=self.base_url+url,callback=self.crawlDetail,meta=response.meta)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({'use_cache': True})
        return request
