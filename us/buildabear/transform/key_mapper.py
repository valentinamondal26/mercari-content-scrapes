class Mapper:
    def map(self, record):

        model_sku = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model_sku" == entity['name']:
                if entity["attribute"]:
                    model_sku = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = model_sku + color
        return key_field
