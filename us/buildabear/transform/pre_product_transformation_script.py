import hashlib
import re
from titlecase import titlecase
from colour import Color

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            
            specs = record.get('specs', {})


            color = specs.get('Color', '') or record.get('filter_color', '') or \
                    specs.get('item_0', {}).get('Color', '')
            color = re.sub(r'\bMulti\b', 'Multicolor', color, flags=re.IGNORECASE)
            color = titlecase(color)
            color = re.sub(r'\bCombo\b', '', color, flags=re.IGNORECASE)
            
            model = record.get('title', '')
            model = model.replace('\u2122', '').replace('\u00ae', '')
            model = re.sub(r'\b\d+\s+in\b\.|\d+\s+in\b|\b\d+\bin\b\.|\b\d+in\b', '', model)
            model = re.sub(r'\bBuild-A-Bear as\b', 'Bear as', model, flags=re.IGNORECASE)
            model = re.sub(r'\bBuild-A-Bear\b|\bOnline Exclusive\b|\s+\-\s+', ' ', model, flags=re.IGNORECASE)
            model = re.sub(r"\bDisney's\b", 'Disney', model, flags=re.IGNORECASE)
            model = re.sub(r'\bBundle\b|\bPre-Stuffed\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color.split()))), '', model, flags=re.IGNORECASE)
            for word in model.split():
                try:
                    Color(word)
                    model = re.sub(r'\b{string}\b'.format(string=word), '', model, flags=re.IGNORECASE)
                except ValueError:
                    pass
            model = re.sub(r'\s+', ' ', model).strip()
            

            height = specs.get('Friend Height', '')
            if not height:
                title = record.get('title', '')
                title = title.replace('\u2122', '').replace('\u00ae', '')
                height = re.findall(r'\b\d+\s+in\b\.|\d+\s+in\b|\b\d+\bin\b\.|\b\d+in\b', title)
                if height:
                    height = height[0]
                else:
                    height = ''
                height = re.sub(r'(\b\d+\b)\s+(in)\.', r'\1\2', height, flags=re.IGNORECASE)
                
            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),
                "description": record.get('description', ''),

                "model": model,
                "color": color,
                "height": height,
                'model_sku': specs.get('SKU', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None

