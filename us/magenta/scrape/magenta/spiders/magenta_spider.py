'''
https://mercari.atlassian.net/browse/USCC-253 - Rae Dunn Home decor accents
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import json
import os
from parsel import Selector

class MagentaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from magenta.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Home decor accents
    
    brand : str
        brand to be crawled, Supports
        1) Rae Dunn

    Command e.g:
    scrapy crawl magenta -a category='Home decor accents' -a brand='Rae Dunn'
    """

    name = 'magenta'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'magenta.com'
    blob_name = 'magenta.txt'

    BASE_URL = 'https://www.magenta-inc.com'

    merge_key = 'id'

    url_dict = {
        'Home decor accents': {
            'Rae Dunn': {
                'url': 'https://www.magenta-inc.com/collections/all',
            }
        },
        # 'Coffee and Tea Accessories': {
        #     'Rae Dunn': {
        #         'url': 'https://www.magenta-inc.com/collections/rae-dunn',
        #     }
        # },
        # 'Dining & entertaining': {
        #     'Rae Dunn': {
        #         'url': 'https://www.magenta-inc.com/collections/rae-dunn',
        #     }
        # },
        # 'Coffee and Tea Accessories': {
        #     'Rae Dunn': {
        #         'url': 'https://www.magenta-inc.com/collections/rae-dunn/mugs',
        #     }
        # }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(MagentaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)
            yield self.create_request(url=self.launch_url, callback=self.parse_filters)


    def crawlDetail(self, response):
        """
        @url https://www.magenta-inc.com/products/acacia-wood-forestry-bowl
        @scrape_values title image price model_sku description type weight barcode
        @returns items 1
        @meta {"use_proxy":"True"}
        """

        script = response.xpath('//script[@type="application/json" and @data-product-json]/text()').extract_first(default='')
        if script:
            data = json.loads(script)
            product = data.get('product', {})

            variants = product.get('variants', [])
            for variant in variants:
                item = {
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': str(response.status),
                    'category': self.category,
                    'brand': self.brand,
                    'id': response.url,

                    'title': variant.get('name', ''),
                    'price': str(variant.get('price')/100) if variant.get('price', 0) else '',
                    'compare_price': str(variant.get('compare_at_price')/100) if variant.get('compare_at_price', 0) else '',
                    'model_sku': variant.get('sku', ''),
                    # 'description': product.get('description', ''),
                    'description': '. '.join([desc.replace('\xa0', ' ').strip() for desc in Selector(product.get('description', '')).xpath('//text()[normalize-space(.)]').extract()]),

                    'type': product.get('type', ''),
                    'image': 'https:' + product.get('featured_image', ''),
                    'weight': str(variant.get('weight', '')),
                    'barcode': variant.get('barcode', ''),
                }
                filter_name = response.meta.get('filter_name', '')
                if filter_name:
                    item[filter_name] = response.meta.get(filter_name, '')
                yield item


    def parse_filters(self, response):
        """
        @url https://www.magenta-inc.com/collections/all
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for filter_tag in response.xpath('//div[@class="CollectionFilters"]//ul[@class="Linklist"]/li[@class="Linklist__Item"]'): 
            product_type = filter_tag.xpath('./button/text()').extract_first()
            product_type_tag = filter_tag.xpath('./button/@data-tag').extract_first()
            if product_type and product_type_tag:
                url = f'{response.url}/{product_type_tag}'
                yield self.create_request(url=url, callback=self.parse, meta={'product_type': product_type, 'filter_name': 'product_type'})


    def parse(self, response):
        """
        @url https://www.magenta-inc.com/collections/all
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for product in response.xpath('//div[@class="ProductListWrapper"]//div[@class="ProductItem "]'): 
            id = product.xpath('.//a/@href').extract_first(default='')
            if id:
                yield self.create_request(url=self.BASE_URL+id, callback=self.crawlDetail, meta=response.meta)

        nextLink = response.xpath('//a[@rel="next"]/@href').extract_first(default='')
        if nextLink:
            url = self.BASE_URL + nextLink
            yield self.create_request(url=url, callback=self.parse, meta=response.meta)


    def create_request(self, url, callback, meta={}, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, dont_filter=dont_filter, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
