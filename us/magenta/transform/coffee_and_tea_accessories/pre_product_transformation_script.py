import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            title = record.get('title', '')
            model = title
            # model = re.sub(re.compile('', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            material = ''
            description = record.get('description', '')
            match = re.findall(re.compile(r'Porcelain|Stoneware', re.IGNORECASE), description)
            if match:
                material = match[0]
            size_diameter = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"DIA', re.IGNORECASE), description)
            if match:
                size_diameter = match[0][0] + match[0][1]

            height = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"H', re.IGNORECASE), description)
            if match:
                height = match[0][0] + match[0][1]

            width = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"W', re.IGNORECASE), description)
            if match:
                width = match[0][0] + match[0][1]

            length = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"L', re.IGNORECASE), description)
            if match:
                length = match[0][0] + match[0][1]

            depth = ''
            size = ''

            set_of = ''
            match = re.findall(re.compile(r'[Pack|Set] of (\d+)', re.IGNORECASE), title)
            if match:
                set_of = match[0]


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": title,
                "description": record.get('description', ''),

                "model": model,
                "size": size,
                "set_of": set_of,
                'width': width,
                'height': height,
                'length': length,
                'depth': depth,
                'size_diameter': size_diameter,
                'material': material,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
