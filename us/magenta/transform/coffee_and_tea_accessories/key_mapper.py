class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        size = ''
        set_of = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            if "size" == entity['name']:
                size = entity["attribute"][0]["id"]
            if "set_of" == entity['name']:
                set_of = entity["attribute"][0]["id"]

        key_field = model + size + set_of
        return key_field
