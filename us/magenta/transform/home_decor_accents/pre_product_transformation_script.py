import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('compare_price', '') or record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r',|\(|\)', '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            description = record.get('description', '')

            material = ''
            match = re.findall(r'Porcelain|Stoneware|accacia wood|Linen|Cotton', description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(list(dict.fromkeys(sorted(list(map(lambda x: x.title(), match))))))

            height = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"H', re.IGNORECASE), description)
            if match:
                height = match[0][0] + match[0][1] + ' in.'

            width = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"W', re.IGNORECASE), description)
            if match:
                width = match[0][0] + match[0][1] + " in."

            length = ''
            match = re.findall(re.compile(r'(\d+)(.\d+){0,1}"L', re.IGNORECASE), description)
            if match:
                length = match[0][0] + match[0][1] + " in."

            # product_type_dict = {
            #     "Aprons": "Clothing & Accessories",
            #     "Bakeware": "Kitchen & Storage",
            #     "Baking": "Kitchen & Storage",
            #     "Barware": "Kitchen & Storage",
            #     "Bathroom": "Office & Other Home Decor",
            #     "Beer Stein": "Kitchen & Storage",
            #     "Bowls": "Kitchen & Storage",
            #     "Bundle": "DO NOT USE",
            #     "Cake Stands": "Kitchen & Storage",
            #     "Cakestand": "Kitchen & Storage",
            #     "Candle Holders": "Office & Other Home Decor",
            #     "Canister": "Kitchen & Storage",
            #     "Charcuterie": "Kitchen & Storage",
            #     "Cheese": "Kitchen & Storage",
            #     "Chopping Board": "Kitchen & Storage",
            #     "Coasters": "Office & Other Home Decor",
            #     "Coffee": "Kitchen & Storage",
            #     "Cream and Sugar": "Kitchen & Storage",
            #     "Desk": "Office & Other Home Decor",
            #     "Desk Organization": "Office & Other Home Decor",
            #     "Dog collar accessories": "Pet Accessories",
            #     "Dog tag charm": "Pet Accessories",
            #     "drinkware": "Kitchen & Storage",
            #     "Enamel Pins": "Clothing & Accessories",
            #     "Exclusions": "DO NOT USE",
            #     "Face Masks": "Clothing",
            #     "Gift Card": "DO NOT USE",
            #     "Gift Tags": "Gift Tags & Wrapping",
            #     "Gift Wrap": "Gift Tags & Wrapping",
            #     "Gifts for Him": "DO NOT USE",
            #     "Glassware": "Kitchen & Storage",
            #     "Holiday": "DO NOT USE",
            #     "Home Decor": "Office & Other Home Decor",
            #     "Home Décor": "Office & Other Home Decor",
            #     "Home Office": "Office & Other Home Decor",
            #     "Hostess Gifts": "DO NOT USE",
            #     "Kitchen": "Kitchen & Storage",
            #     "Kyoto": "Clothing & Accessories",
            #     "Leather": "Clothing & Accessories",
            #     "Leather Pouches": "Clothing & Accessories",
            #     "LGBTQ": "DO NOT USE",
            #     "Linen coasters": "Kitchen & Storage",
            #     "Marble": "DO NOT USE",
            #     "Measuring": "Kitchen & Storage",
            #     "Mugs": "Kitchen & Storage",
            #     "Napkins": "Kitchen & Storage",
            #     "New Arrivals": "DO NOT USE",
            #     "Office": "Office & Other Home Decor",
            #     "Ornaments": "Ornaments",
            #     "Outdoor Decor": "Outdoor Decor",
            #     "Pet Accessories": "Pet Accessories",
            #     "Pet Bed": "Pet Accessories",
            #     "Pet Bowls": "Pet Accessories",
            #     "Pet Tags": "Pet Accessories",
            #     "Picture Frames": "Office & Other Home Decor",
            #     "Pitcher & Vases": "Office & Other Home Decor",
            #     "Pitchers & Vases": "Kitchen & Storage",
            #     "Placemats": "Outdoor Decor",
            #     "Planters": "Office & Other Home Decor",
            #     "Plates": "Kitchen & Storage",
            #     "Pouches": "Clothing & Accessories",
            #     "Rae dunn coasters": "Office & Other Home Decor",
            #     "Ribbon": "Gift Tags & Wrapping",
            #     "Salt & Pepper": "Kitchen & Storage",
            #     "Serveware": "Kitchen & Storage",
            #     "Serving Board": "Kitchen & Storage",
            #     "Serving Utensils": "Kitchen & Storage",
            #     "Stamps": "Stamps",
            #     "Stem Print Pet": "Pet Accessories",
            #     "Stocking Fillers": "DO NOT USE",
            #     "sum": "DO NOT USE",
            #     "Summer": "DO NOT USE",
            #     "Tabletop": "Kitchen & Storage",
            #     "Tea": "Kitchen & Storage",
            #     "Tea Towels": "DO NOT USE",
            #     "Textiles": "DO NOT USE",
            #     "Totes": "Clothing & Accessories",
            #     "Trays & Platters": "Kitchen & Storage",
            #     "Trays and Platters": "Kitchen & Storage",
            #     "Trinket Trays": "Office & Other Home Decor",
            #     "Tumbler": "Kitchen & Storage",
            #     "Utensil Holder": "Kitchen & Storage",
            #     "Utensils": "Kitchen & Storage",
            #     "Vase": "Office & Other Home Decor",
            #     "Wall Decor": "Office & Other Home Decor",
            #     "Wall Décor": "Office & Other Home Decor",
            #     "Wall Tacks": "Office & Other Home Decor",
            #     "Wedding Gifts": "DO NOT USE",
            #     "Wine": "DO NOT USE",
            #     "wood": "DO NOT USE",
            #     "Word Stones": "Office & Other Home Decor",
            # }

            # product_type = record.get('product_type', '') or record.get('type', '')
            # product_types = list(dict.fromkeys(list(map(lambda x: product_type_dict.get(x, x), product_type.split(', ')))))

            # if len(product_types) == 1 and "DO NOT USE" in product_types:
            #     return None
            # product_types = list(filter(lambda x: x!="DO NOT USE", product_types))
                
            # if len(product_types) > 1:
            #     for p_type in product_types:
            #         tokens = list(filter(lambda x: x.strip() not in [None, '', '&'], re.split(r'\b', p_type)))
            #         tokens.extend(list(filter(None, map(lambda x: x[:-1] if x.endswith('s') else None, tokens))))
            #         match = re.findall(r'', model, flags=re.IGNORECASE)
            #         if match:
            #             product_types = [p_type]
            #             break
            #     else:
            #         if 'Office & Other Home Decor' in product_types:
            #             product_types = ['Office & Other Home Decor']
            # product_type = ', '.join(product_types)

            product_type = ''
            product_type_meta = [
                'Mugs & Cups', 'Glassware', 'Bowls', 'Pet Decor', 'Bathroom Decor',
                'Trays, Platters & Cakestands', 'Pitchers ', 'Bakeware ', 'Planters & Outdoor Decor',
                'Containers & Canisters', 'Gift Wrap & Gift Tags', 'Word Stones', 'Napkins & Towels',
                'Aprons', 'Totes & Pouches', 'Masks', 'Wall Decor ', 'Utensils', 'Tea & Coffee Decor',
                'Shakers', 'Coasters', 'Vases', 'Journals', 
            ]
            tokens = list(filter(lambda x: x.strip() not in [None, '', '&', 'Decor'], re.split(r'\b', ' '.join(product_type_meta))))
            tokens.extend(list(filter(None, map(lambda x: x[:-1] if x.endswith('s') else None, tokens))))
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', tokens))), model, flags=re.IGNORECASE)
            if match:
                indices = [i for i, s in enumerate(list(map(lambda x: x.lower(), product_type_meta))) if match[0].lower() in s] 
                product_type = product_type_meta[indices[0]]

            if re.findall(r'\bGift Card\b', model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', '').split(',')[0],
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                'width': width,
                'length': length,
                'height': height,
                'material': material,
                'model_sku': record.get('model_sku', ''),
                "product_type": product_type,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
