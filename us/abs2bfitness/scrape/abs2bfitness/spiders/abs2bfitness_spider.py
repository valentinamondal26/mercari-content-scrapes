'''
https://mercari.atlassian.net/browse/USCC-0 - abs2bfitness Women's Leggings
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import logging
import copy


class Abs2BfitnessSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from abs2bfitness.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Leggings

    brand : str
        brand to be crawled, Supports
        1) abs2bfitness

    Command e.g:
    scrapy crawl abs2bfitness -a category="Women's Leggings" -a brand="abs2bfitness"
    """

    name = 'abs2bfitness'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'abs2bfitness.com'
    blob_name = 'abs2bfitness.txt'

    base_url = 'https://abs2bfitness.com'

    url_dict = {
        "Women's Leggings": {
            "abs2bfitness": {
                 'url': 'https://abs2bfitness.com/collections/leggings',
            }
        },
        "Women's Sports Bras": {
            "abs2bfitness": {
                 'url': 'https://abs2bfitness.com/collections/sports-bra',
            }
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(Abs2BfitnessSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://abs2bfitness.com/collections/leggings/products/sassy-back/
        @meta {"use_proxy":"True"}
        @scrape_values id title price description
        """

        title = response.xpath('//h1[@itemprop="name"]/text()').get()

        waistband_options = response.xpath('//div[@data-option-name="waistband-options"]//ul/li/label/text()').getall()
        waistband_options = list(map(lambda x: re.sub("[\(\[].*?[\)\]]", "", x), waistband_options))

        length_variants = response.xpath('//div[@data-option-name="length"]/div/ul/li').xpath('./label/text()').getall()
        description = response.xpath('//div[@itemprop="description"]').extract_first()

        material = response.xpath('string(//span[contains(text(), "Content")]/parent::li)').get().strip().replace(u'\xa0', u' ').strip()

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': title,
            'price': response.xpath('//span[@itemprop="price"]/text()').extract_first().strip(),
            'description': description
        }

        if not length_variants:
            yield item

        for length in length_variants:
            for waistband_option in waistband_options:
                variant = copy.deepcopy(item)
                variant.update({
                    'length': length,
                    'waistband': waistband_option,
                    'material': material
                })
                yield variant



    def parse(self, response):
        """
        @url https://abs2bfitness.com/collections/leggings
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.xpath('//div[@class="grid-product__content"]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                full_url = self.base_url + product_url
                logging.debug(f'Full url: {full_url}')
                yield self.create_request(url=full_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//div[@class="pagination"]//span[@class="next"]/a/@href').get()
        if next_page_link:
            full_url = self.base_url + next_page_link
            yield self.create_request(url=full_url, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
