import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub('[\(\[].*?[\)\]]', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPatent\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s*\&\s*', ' & ', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'(abs2b fitness)|(abs2b)', '', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'(Zero Flaw)|(Zero-Flaws)|(Zero-Flaw)', 'Zero Flaws', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'leggings', "", model, flags=re.IGNORECASE).strip()
            model = re.sub(r'\s*\/\s*', '/', model, flags=re.IGNORECASE)
            model = re.sub(r'sports', "", model, flags=re.IGNORECASE).strip()
            model = re.sub(r'bra', "", model, flags=re.IGNORECASE).strip()
            model += " Sports Bra"
            model = model.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "price": price,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
