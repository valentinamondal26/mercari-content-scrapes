class Mapper:
    def map(self, record):

        model = ''
        style = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            elif "style" == entity['name'] and entity["attribute"]:
                style = entity["attribute"][0]["id"]

        key_field = model + style
        return key_field
