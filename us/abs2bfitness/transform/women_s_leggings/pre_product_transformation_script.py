import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            style = record.get("length", "")
            style = re.sub("[\(\[].*?[\)\]]", "", style, flags=re.IGNORECASE).strip()
            style = re.sub("Capris", "Capri", style, flags=re.IGNORECASE)

            model = record.get('title', '')
            model = re.sub('[\(\[].*?[\)\]]', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPatent\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'(abs2b fitness)|(abs2b)', '', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'(Zero Flaw)|(Zero-Flaws)|(Zero-Flaw)', 'Zero Flaws', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'(leggings)|(legging)', "", model, flags=re.IGNORECASE).strip()
            model = re.sub(r':D', "", model, flags=re.IGNORECASE).strip()
            model = model.title()
            model = re.sub(r'B\*Tch', "B*TCH", model, flags=re.IGNORECASE).strip()

            material = record.get('material', '')
            material = re.sub("[\(\[].*?[\)\]]", "", material, flags=re.IGNORECASE).strip()
            material = re.sub(r'[0-9]', "", material, flags=re.IGNORECASE).strip()
            material = material.replace("%", "").replace("Content:", "").strip()
            material = re.sub(r'\s*\/\s*', "/", material).strip()
            material = re.sub(r'\s*-\s*', "/", material).strip()
            material = re.sub(' +', ' ', material)
            material = material.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "price": price,
                "rise": record.get('waistband'),
                "style": style,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
