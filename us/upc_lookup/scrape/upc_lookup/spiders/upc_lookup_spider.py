import json
import urllib
from typing import Dict
import re
import scrapy
from google.cloud import storage
from scrapy.exceptions import CloseSpider


class UpcCheck(scrapy.Spider):
    """
    Spider to crawl the upc look ups for the list of items from KG.
    Passes the brand name , model name , storage capacity , color and
      model sku details in the free text seacrh keyword query.
    collects the upc numbers from the list of items returned by the lookup.
    """
    name = "UpcIteMdb"
    api_url = 'https://api.upcitemdb.com/prod/v1/search?'
    brand = None
    category = None
    blob_name = None
    items_list = []
    launch_url = 'https://devs.upcitemdb.com/'
    client = storage.Client()
    bucket = client.get_bucket("content_us")

    def __init__(self, category=None, brand=None,
                 *args, **kwargs):
        super(UpcCheck, self).__init__(*args, **kwargs)
        if not brand and category:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.blob_name = brand + "_" + category + ".txt"
        gcs_path = f'upc/{self.blob_name}'
        gcs_path = re.sub(re.compile(r"['&,]"), '', gcs_path)
        gcs_path = re.sub(r'\s+', '_', gcs_path).strip().lower()
        blob = self.bucket.blob(gcs_path)
        try:
            blob.download_to_filename(self.blob_name)
        except Exception as exeception:
            raise CloseSpider("KG items not found:"+exeception)
        self.items_list = json.loads(open(self.blob_name, "r").read())

    def start_requests(self) -> scrapy.Request:
        """
        Scrapy start the initial request
        @return: Scrapy Reuqest
        """
        yield scrapy.Request(url=self.launch_url,
                             callback=self.parse, dont_filter=True)

    def parse(self, response: scrapy.http.Response) -> scrapy.Request:
        """
        Method forms the look-up api with the needed params for each item
        from the kg items.
        @param response: Scrapy Response
        @return: Scrapy Request
        """
        for item in self.items_list:
            # source details for search keyword
            domain = item.get('source', '').split(".")[1] + ".com"
            entity_attributes = item.get("entityAttributes")
            search_keywords = [pair.get("attributeName")
                               for pair in entity_attributes
                               if pair.get("entityName") == "model"]
            if not search_keywords:
                search_keywords.append(self.brand)
                search_keywords = [pair.get("attributeName")
                                   for pair in entity_attributes
                                   if pair.get("entityName") not in
                                   ["description", "price", "msrp",
                                    "system_category", "brand"]]
            search_keywords = " ".join(search_keywords)
            params = {
                "s": search_keywords,
                "match_mode": 0,
                "type": "product",

            }
            header = {
                "user_key": '<user key>',
                "key_type": "3scale",
                "Accept": "application / json"
            }
            request = scrapy.Request(
                self.api_url + urllib.parse.urlencode(params),
                method='GET',
                headers=header,
                callback=self.parse_detail,
                meta={"seach_keyword": search_keywords,
                      "item_id": item.get("_key", "")},
                dont_filter=True)
            yield request

    def parse_detail(self, response: scrapy.http.Response) -> Dict:
        """
        Method handles the upc matching from the api response and yields the
        item with the upc list from the response objects.
        @param response: Scrapy Response
        @return: item dict
        """
        response_json = json.loads(response.text)
        upcs = response_json.get("items", [])
        upcs = [item.get("upc") for item in upcs]
        item = {
            "item_id": response.meta.get("item_id", ""),
            "search_keywords": response.meta.get("seach_keyword"),
            "brand": self.brand,
            "category": self.category,
            "api_upc": upcs,
            # "api_response": response_json

        }
        yield item
