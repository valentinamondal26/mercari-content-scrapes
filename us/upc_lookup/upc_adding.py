import json
import logging
import os
import re
import subprocess
from io import BytesIO

from arango import ArangoClient
from arango.database import StandardDatabase
from google.cloud import storage
from typing import Dict

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class UpcAdding:
    """
    Class for Updating the UPC lookup info the KG-items
    """
    ENV = 'dev'

    def __init__(self, category, brand):
        self.kg_con = self.get_kg_con()
        self.category = category
        self.brand = brand
        self.client = storage.Client()
        self.bucket = self.client.get_bucket("content_us")

    def kg_config(self, file_path: str) -> Dict:
        """
        Retrives the Config dict from the config.ini
        @param file_path: config file path
        @return: config as dict
        """
        config = {}
        keys = []
        values = []
        if not os.path.isfile(file_path):
            logger.error("KG credentials file not found")
            raise FileNotFoundError
        for line in open(file_path, "r"):
            keys.append(line.split("=")[0])
            values.append(line.split("=")[1])
        config = config.fromkeys(keys, "")
        for ctr, key in enumerate(keys):
            config[key] = values[ctr].strip("\n ")
        return config

    def get_kg_con(self) -> StandardDatabase:
        """
        Make arnago connection and returns the connection object
        @return: Database Connection object
        """
        kg_creds_path = 'kg_configs/configs/' + self.ENV + '/config.ini'
        config = self.kg_config(kg_creds_path)

        try:
            client = ArangoClient(protocol="http",
                                  host=config.get("host"),
                                  port=int(config.get("port")))
        except Exception as exception:
            logger.error(exception)
            raise exception

        return client.db(config.get("knowledgeDb"),
                         username=config.get("username"),
                         password=config.get("password"))

    def get_items(self) -> None:
        """
        Fetches the KG-items for the given category and brand and uploads the
        content to gcs
        """
        query = """
        FOR item in items
            FILTER (item.entityAttributes[*].entityId 
                        ANY == 'b935695f-e479-4f7e-aa54-b17966fd3fa8' 
                    AND item.entityAttributes[*].attributeName 
                        ANY == @category)
                    AND 
                    (item.entityAttributes[*].entityId 
                            ANY == '2af53444-0171-487d-9133-208950388ec7'
                    AND item.entityAttributes[*].attributeName 
                            ANY == @brand)
        RETURN item
        """
        try:
            logger.info("Fetching KG Items")
            query_result = self.kg_con.aql.execute(
                query,
                bind_vars={
                    "category": self.category,
                    "brand": self.brand,
                    },
                count=True,
            )
        except Exception as exception:
            logger.error(exception)
            raise exception

        filename = self.brand + "_" + self.category + ".txt"
        gcs_path = f'upc/{filename}'
        gcs_path = re.sub(re.compile(r"['&,]"), '', gcs_path)
        gcs_path = re.sub(r'\s+', '_', gcs_path).strip().lower()
        blob = self.bucket.blob(gcs_path)
        file_obj = BytesIO()
        file_obj.write(json.dumps(list(query_result)).encode())
        file_obj.seek(0)
        try:
            blob.upload_from_file(file_obj)
        except Exception as exeception:
            logger.error(exeception)
            raise Exception

    def start_scrape(self, preexec_fn=False) -> None:
        """
        Start the upc lookup scrape for Kg Items
        @param preexec_fn: preexec_fn
        """
        os.chdir(os.getcwd()+"/scrape/")
        cmd = 'python3 -m scrapy crawl UpcIteMdb -a category={category} -a ' \
              'brand={brand}'
        cmd = cmd.split()
        cmd = '%'.join(cmd).format(category=self.category,
                                   brand=self.brand,)
        cmd = cmd.split('%')
        logger.info(f"Starting crawl..for {self.category} and {self.brand}")
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             stdin=subprocess.PIPE,
                             preexec_fn=os.setsid if preexec_fn else None)
        err, out = p.communicate()
        if err.decode("utf-8"):
            logger.error(err.decode("utf-8"))
            raise Exception
        else:
            logger.info(out.decode("utf-8"))

    def update_kg_items(self) -> None:
        """
        Update the approx-upc and related-upc to the KG items
        """
        logger.info("Updating Items in KG...")
        filename = self.brand + "_" + self.category + ".txt"
        gcs_path = f'upc/{filename}'
        gcs_path = re.sub(re.compile(r"['&,]"), '', gcs_path)
        gcs_path = re.sub(r'\s+', '_', gcs_path).strip().lower()
        blob = self.bucket.blob(gcs_path)
        try:
            blob.download_to_filename(filename)
            logger.info(f"File Downloaded: {filename}")
        except Exception as exception:
            logger.error(exception)
            raise Exception
        query = """
        FOR item IN items
            FILTER item._key == @itemId 
                UPDATE item WITH { approxUPC: @approxupc,
                                   relatedUPC: @relatedupc
                                   } IN items
        """
        for item in open(filename, "r"):
            item = json.loads(item)
            api_upcs = item.get('api_upc')
            api_upcs = [upc for upc in api_upcs if upc]
            approx_upc = api_upcs[0]
            related_upc = api_upcs[1:]
            try:
                self.kg_con.aql.execute(
                    query,
                    bind_vars={"approxupc": approx_upc,
                               "relatedupc": related_upc,
                               "itemId": item.get("item_id")},
                    count=True
                )
            except Exception as exception:
                logger.error(exception)
                raise exception

    def main(self):
        self.get_items()
        logger.info("KG items fetched")
        self.start_scrape()
        logger.info("Crawl Complete")
        self.update_kg_items()
        filename = self.brand + "_" + self.category + ".txt"
        if os.path.exists(filename):
            os.remove(filename)


upc_lookup = UpcAdding('Cell Phones & Smartphones', 'Apple')
upc_lookup.main()
