'''
https://mercari.atlassian.net/browse/USCC-697 - Chippewa Boots Men's Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class ChippewabootsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from chippewaboots.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Shoes
    
    brand : str
        brand to be crawled, Supports
        1) Chippewa Boots

    Command e.g:
    scrapy crawl chippewaboots -a category="Men's Shoes" -a brand="Chippewa Boots"
    """

    name = 'chippewaboots'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'chippewaboots.com'
    blob_name = 'chippewaboots.txt'

    base_url = 'https://www.chippewaboots.com'

    url_dict = {
        "Men's Shoes": {
            "Chippewa Boots": {
                'url': 'https://www.chippewaboots.com/footwear/footwear_category',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ChippewabootsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.chippewaboots.com/footwear/footwear/category/logger/73238
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title price details
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//ul[@class="breadcrumbs"]/li/a/text()').getall(),
            'image': response.xpath('//div[@id="product-main-image"]//img/@src').get(),
            'title': response.xpath('//h2[@class="product-title"]/text()').get(),
            'price': ''.join(
                    response.xpath('//div[@class="large-4 medium-4 columns text-right nopad show-for-medium-up"]/h3[@class="product-price"]//text()').getall()
                ).strip(),
            'details': dict(zip(
                response.xpath('//div[@id="panel-boot-details"]/dl/dt/text()').getall(),
                [dd.xpath('./text()').get().strip() or dd.xpath('.//a/text()').get().strip() for dd in response.xpath('//div[@id="panel-boot-details"]/dl/dd')]
            )),
        }


    def parse(self, response):
        """
        @url https://www.chippewaboots.com/footwear/footwear_category
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="row"]/ul[contains(@class, "category-products")]/li'):
            is_product_listing = product.xpath('./@data-price').get()
            product_url = product.xpath('./a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail if is_product_listing else self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
