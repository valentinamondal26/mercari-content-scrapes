# -*- coding: utf-8 -*-

# Scrapy settings for chippewaboots project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'chippewaboots'

SPIDER_MODULES = ['chippewaboots.spiders']
NEWSPIDER_MODULE = 'chippewaboots.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36'


# Obey robots.txt rules
ROBOTSTXT_OBEY = False

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

TELNETCONSOLE_ENABLED = False

EXTENSIONS = {
    'scrapy.extensions.telnet.TelnetConsole': None,
}

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

# GCS settings - actual value be 'raw/app_name.com/sandals/app_name/2019-02-06_00_00_00/json/app_name.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10,
    'common.scrapy_contracts.contracts.Headers':10
}