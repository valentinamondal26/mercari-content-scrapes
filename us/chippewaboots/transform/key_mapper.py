class Mapper:
    def map(self, record):

        model = ''
        material = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "material" == entity['name']:
                material = entity["attribute"][0]["id"]

        key_field = model + material
        return key_field
