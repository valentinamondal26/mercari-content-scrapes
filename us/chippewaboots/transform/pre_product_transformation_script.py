import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('details', {}).get('Description:', '')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            def roman_numerals(word, **kwargs):
                PATTERN_ROMAN_NUMERALS = re.compile(r'(^(?=[MDCLXVI])M*(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$)')
                if re.findall(PATTERN_ROMAN_NUMERALS, word):
                    return word
            model = titlecase(model, callback=roman_numerals)
            model = model.strip()

            material = record.get('details', {}).get('Leather:', '')
            material = titlecase(material).strip()

            product_type = ''
            def wrap(list_data):
                return r'|'.join(list(map(lambda x: r'\b'+x+r'\b', list_data)))
            if re.findall(wrap(['Logger', 'Paladin']), description, flags=re.IGNORECASE):
                product_type = 'Logger'
            elif re.findall(wrap(['Snakes', 'Snake']), description, flags=re.IGNORECASE):
                product_type = 'Snake'
            elif re.findall(wrap(['Wedge', 'Edge']), description, flags=re.IGNORECASE):
                product_type = 'Wedge'
            elif re.findall(wrap(['Hiker', 'hiking']), description, flags=re.IGNORECASE):
                product_type = 'Hiking'
            elif re.findall(wrap(["women's", 'women']), description, flags=re.IGNORECASE):
                product_type = "Women's"

            breadcrumbs = record.get('breadcrumb', [])

            if not product_type:
                if breadcrumbs[-1] in ['Logger', 'Snake', 'Wedge', 'Hiking', "Women's"]:
                    product_type = breadcrumbs[-1]


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "product_type": product_type,
                "material": material,
                'model_sku': record.get('details', {}).get('Style:', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
