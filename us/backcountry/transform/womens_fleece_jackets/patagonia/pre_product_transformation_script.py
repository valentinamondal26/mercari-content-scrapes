import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record.get('product_id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')

            pattern=re.compile(r"Patagonia|Women\'*s*|Fleece|Jackets*",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            model=' '.join(model.split())
            if model[len(model)-1]=="-":
                model=model[:-1]
            
            model=' '.join(model.split())

            color = record.get('color', '')
            if color:
                color = self.process_color(color)
        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date',''),
                "status_code": record.get('status_code',''),
                "category": "Women's Fleece Jackets",
                "breadcrumb":record.get('breadcrumb',''),
                "description": description.replace('\n',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query.replace('\n',''),
                "price":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "size":record.get('size',''),
                "color": color,
                "material":record.get('details',{}).get('Material',''),
                "fit":record.get('details',{}).get('Fit',''),
                "length":record.get('details',{}).get('Length',''),
                "number_of_pockets":record.get('details',{}).get('Pockets',''),
                "weight":record.get('details',{}).get('Claimed Weight','')            
                }

            return transformed_record
        else:
            return None

    def process_color(self, color):
        color = re.sub(re.compile(r'x-dye', re.IGNORECASE), '', color)
        color = re.sub(' +', ' ', color).strip()
        l = [e.strip() for e in color.split('/')]
        rem_list = []
        for c in l:
            r = re.compile(rf'.+{c}$')
            newlist = list(filter(r.match, l))
            if len(newlist) > 0:
                rem_list.append(c)
        return '/'.join(list(set(l) - set(rem_list)))
