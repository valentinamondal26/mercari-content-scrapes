'''
https://mercari.atlassian.net/browse/USCC-197 - Patagonia Women's Fleece Jackets
'''

import scrapy
import datetime
import json
import random
import os
from scrapy.exceptions import CloseSpider

class BackcountrySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from backcountry.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Fleece Jackets

    brand : str
        category to be crawled, Supports
        1) Patagonia

    Command e.g:
    scrapy crawl backcountry -a category="Women's Fleece Jackets" -a brand='Patagonia'

    """

    name = 'backcountry'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None
    launch_url=None
    filters=None
    number_of_products=None
    source = 'backcountry.com'
    blob_name = 'backcountry.txt'
    base_url='https://www.backcountry.com'

    url_dict={
        "Women's Fleece Jackets": {
            'Patagonia': {
                'url': 'https://www.backcountry.com/rc/patagonia-fleece-jackets?p=category%3A3.bcs.Women%27s%5C+Clothing.Women%27s%5C+Jackets.Women%27s%5C+Fleece%5C+Jackets',
            },
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BackcountrySpider, self).__init__(*args, **kwargs)
        if  os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse)


    def parse(self, response):
            """
            @url https://www.backcountry.com/rc/patagonia-fleece-jackets?p=category%3A3.bcs.Women%27s%5C+Clothing.Women%27s%5C+Jackets.Women%27s%5C+Fleece%5C+Jackets
            @returns requests 43
            """
            ###Number of request expected is 43 because 42 products per page and 1 Next page url

            all_links=response.xpath('//div[@class="ui-pl-expandable js-pl-expandable"]/div/a/@href').getall()
            all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
            for link in all_links:
                yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)
            next_page_url=response.xpath('//li[@class="pag-next"]/a[@rel="nofollow"]/@href').get(default='')
            next_page_url=[self.base_url+link if "https" not in link else link for link in [next_page_url]]
            next_page_url=next_page_url[0]
            if next_page_url!='' or next_page_url!="https://www.backcountry.com":
                yield self.createRequest(url=next_page_url,callback=self.parse)


    def parse_product(self,response):
        """
        @url https://www.backcountry.com/patagonia-synchilla-lightweight-snap-t-fleece-pullover-womens?skid=PAT3000-LTBAL-XXS&ti=UExQIFJ1bGUgQmFzZWQ6UGF0YWdvbmlhIEZsZWVjZSBKYWNrZXRzOjE6MTo=
        @scrape_values title breadcrumb description product_id price color size image
        """

        item={}

        item["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"]=str(response.status)
        item['id']=response.url
        item['breadcrumb']='|'.join(response.xpath('//div[@class="breadcrumb"]/ul/li/a/text()').getall())
        item['brand']=self.brand
        item['category']=self.category
        item['title']=response.xpath('//h1[@class="product-name qa-product-title"]/text()').get(default='')
        item['description']=', '.join(response.xpath('//ul[@class="product-details-accordion__list"]/li/text()').getall())
        details={}
        keys=keys=response.xpath('//div[@class="table product-details-accordion__techspecs-container"]/div[@class="tr  js-techspec-row"]/div[@class="td product-details-accordion__techspec-name js-techspec-name"]/text()').getall()
        details=details.fromkeys(keys,'')
        values=response.xpath('//div[@class="table product-details-accordion__techspecs-container"]/div[@class="tr  js-techspec-row"]/div[@class="td product-details-accordion__techspec-value js-techspec-value"]/text()').getall()
        count=0
        for key,value in details.items():
            if value=='':
                details[key]=values[count]
                count+=1
        item['details']=details

        product_ids=[]
        prices=[]
        colors=[]
        sizes=[]
        images=[]
        # lis=response.xpath('//ul[@class="buybox-dropdown__options js-basedropdown__options"]/li')
        # for li in lis:
        #     if li.xpath('./meta[@itemprop="price"]/@content').get(default='')!='':
        #         prices.append(li.xpath('./meta[@itemprop="price"]/@content').get(default=''))
        #     if li.xpath('./div/meta[@itemprop="color"]/@content').get(default='')!='':
        #         colors.append(li.xpath('./div/meta[@itemprop="color"]/@content').get(default=''))
        #     if li.xpath('./div/meta[@itemprop="productID"]/@content').get(default='')!='':
        #         product_ids.append(li.xpath('./div/meta[@itemprop="productID"]/@content').get(default=''))
        #     if li.xpath('./div[2]/img/@data-src').get(default='')!='':
        #         images.append("https:"+li.xpath('./div[2]/img/@data-src').get(default=''))
        #     break
        # if colors==[]:
        #     divs=response.xpath('//div[@class="product-variant-selector js-product-variant-selector"]/div[@class="hidden"]/div')
        #     for li in divs:
        #         if li.xpath('./meta[@itemprop="price"]/@content').get(default='')!='':
        #             prices.append(li.xpath('./meta[@itemprop="price"]/@content').get(default=''))
        #         if li.xpath('./div/meta[@itemprop="color"]/@content').get(default='')!='':
        #             colors.append(li.xpath('./div/meta[@itemprop="color"]/@content').get(default=''))
        #         if li.xpath('./div/meta[@itemprop="productID"]/@content').get(default='')!='':
        #             product_ids.append(li.xpath('./div/meta[@itemprop="productID"]/@content').get(default=''))

        script_data=response.xpath('/html/body/div[2]/div/div[4]/article/script/text()').get()
        st=script_data.split('BC.product = ')[1].strip('];BC.publish(\'pdp.domReady\');')
        js=json.loads(st.split(';BC.product.fullProductImages = [')[0])
        collections=js.get('skusCollection',{})
        for key,value in collections.items():
            product_ids.append(key)
            images.append("https:"+value.get('color'))
            color,size=value.get('displayName').split(",")
            colors.append(color)
            sizes.append(size)
            prices.append(value.get('displayPrice'))
        for product_id in product_ids:
            items={}
            index=product_ids.index(product_id)
            item['product_id']=product_id
            item['price']=prices[index]
            item['color']=colors[index]
            item['size']=sizes[index]
            item['image']=images[index]

            items.update(item)
            yield items


    def createRequest(self, url, callback,meta=None,headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key,value in headers.items():
                request.headers[key]=value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request

