import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '').replace('\n', ' ')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("JPY", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\bNendoroid\b|\bfigma\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\(', ' (', model)
            model = re.sub(r'\s+', ' ', model).strip()

            product_type = ''
            if re.findall('/Nendoroid/', record.get('id', ''), flags=re.IGNORECASE):
                product_type = 'Nendoroid'
            elif re.findall('/figma/', record.get('id', ''), flags=re.IGNORECASE):
                product_type = 'figma'

            if re.findall(r'\bNendoroid Plus\b|\bNendoroid Pouch\b|\bNendoroid Petite\b|\bfigma Basic\b',
                record.get('title', ''), flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "series": record.get('specs', {}).get('Series', '').strip() or '',
                "product_type": product_type,
                'release_date': record.get('specs', {}).get('Release Date', '').strip() or '',
                "msrp": {
                    "currencyCode": "JPY",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
