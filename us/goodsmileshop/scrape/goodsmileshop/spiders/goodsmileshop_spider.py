'''
https://mercari.atlassian.net/browse/USCC-749 - Good Smile Company Action Figures
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class GoodsmileshopSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from goodsmileshop.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Action Figures

    brand : str
        brand to be crawled, Supports
        1) Good Smile Company

    Command e.g:
    scrapy crawl goodsmileshop -a category="Action Figures" -a brand="Good Smile Company"
    """

    name = 'goodsmileshop'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'goodsmileshop.com'
    blob_name = 'goodsmileshop.txt'

    base_url = 'https://goodsmileshop.com'

    url_dict = {
        "Action Figures": {
            "Good Smile Company": {
                'url': [
                    'https://goodsmileshop.com/en/CATEGORY-ROOT/Nendoroid/c/133?sort=showAllProduct&q=%3AshowAllProduct%3AshowAllProduct%3AshowAllProduct',
                    'https://goodsmileshop.com/en/CATEGORY-ROOT/figma/c/135?sort=showAllProduct&q=%3ApostingDate%3AshowAllProduct%3AshowAllProduct',
                ]
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(GoodsmileshopSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://goodsmileshop.com/en/CATEGORY-ROOT/Nendoroid/Nendoroid-Natsuiro-Matsuri/p/GSC_WD_02462
        @meta {"use_proxy":"True"}
        @scrape_values id title image description price specs specs.Series specs.Release\sDate
        """

        specs = {}
        for tr in response.xpath('//div[@class="productSpec-right"]/table//tbody/tr'):
            key = tr.xpath('./th[@class="label"]/text()').get()
            value = tr.xpath('./td[contains(@class, "data")]/text()').get()
            specs[key] = value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath(u'normalize-space(//h1[@class="pull-left"]/text())').get(),
            'image': self.base_url + response.xpath('//div[@id="primary_image"]//img/@data-original').get(),
            'description': ''.join(response.xpath('//div[@class="productDescription-right goodsmile-global"]/text()[normalize-space(.)]').getall()).strip(),
            'price': response.xpath('//div[@class="big-price"]/span/text()').get(),
            'specs': specs,
        }


    def parse(self, response):
        """
        @url https://goodsmileshop.com/en/CATEGORY-ROOT/Nendoroid/c/133?sort=showAllProduct&q=%3AshowAllProduct%3AshowAllProduct%3AshowAllProduct
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="productGrid"]/div'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//ul[@class="pagination"]/li[@class="next"]/a/@href').extract_first()
        if next_page_link:
            next_page_link = self.base_url + next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
