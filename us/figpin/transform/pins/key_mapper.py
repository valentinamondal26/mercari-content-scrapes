class Mapper:
    def map(self, record):
        key_field = ""

        product_type = ''
        model = ''

        entities = record['entities']
        for entity in entities:
            if "product_type" == entity['name']:
                product_type = entity["attribute"][0]["id"]
            elif "model" == entity['name']:
                model = entity["attribute"][0]["id"]

        key_field = product_type + model
        return key_field
