import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = ' '.join(record.get('description', [])) or record.get('title', '')
            ner_query = ner_query.replace('\n', ' ').strip()
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '') or ''
            price = price.replace("$", "").strip()

            description = ' '.join(record.get('description', []))

            upc = ''
            match = re.findall(r'UPC\s*:\s*(.*?)\n', description)
            if match:
                upc = match[0].replace('\xa0', '').replace('\ufeff', '').strip()

            height = ''
            match = re.findall(r'(\d+\.*\d*\")\s*tall', description)
            if match:
                height = match[0]

            depth = ''
            match = re.findall(r'(\d+\.*\d*\")\s*thick', description)
            if match:
                depth = match[0]

            match = re.findall(r'(.*)\n(<?.*)', description)
            if match:
                description = match[-1][-1] if isinstance(match[-1], tuple) else match[-1]
            description = description.strip()

            model = record.get('title', '')
            model = re.sub(r'\(|\)|#', '', model)
            model = re.sub(r'\s+\+|\s+-sign-\s+|\s+\-\s+', ' ', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'\s+', ' ', model).strip()

            product_type = ''
            collection = record.get('collection', '')
            if collection in ['figpin-standard', 'vault-pin']:
                product_type = 'FiGPiN Standard'
            elif collection in ['figpin-mini', 'vault-pin-mini']:
                product_type = 'FiGPiN Mini'
            elif collection in ['figpin-xl', 'vault-pin-xl']:
                product_type = 'FiGPiN XL'

            product_line = ', '.join(record.get('Interest', []))

            if not product_type:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                'image': record.get('image', ''),
                "description": description,

                "model": model,
                "series": record.get('series', ''),
                'product_type': product_type,
                # 'product_line': product_line,
                'height': height,
                'depth': depth,
                'model_sku': record.get('sku', ''),
                "upc": upc,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
