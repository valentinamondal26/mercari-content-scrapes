'''
https://mercari.atlassian.net/browse/USCC-70 - FiGPiN Pins
'''

import scrapy
import random
import json
import os
from datetime import datetime
from scrapy.exceptions import CloseSpider

class FigpinSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from figpin.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Pins

    brand : str
        brand to be crawled, Supports
        1) FiGPiN

    Command e.g:
    scrapy crawl figpin -a category='Pins' -a brand='FiGPiN'
    """

    name = 'figpin'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'figpin.com'
    blob_name = 'figpin.txt'

    url_dict = {
        'Pins': {
            'FiGPiN': {
                'url': [
                    'https://figpin.com/collections/figpin-standard',
                    'https://figpin.com/collections/figpin-mini',
                    'https://figpin.com/collections/figpin-xl',
                    'https://figpin.com/collections/vault-pin',
                    'https://figpin.com/collections/vault-pin-mini',
                    'https://figpin.com/collections/vault-pin-xl',
                ],
                'filters': ['Category', 'Interest'],
            }
        }
    }

    base_url = 'https://figpin.com'

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(FigpinSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        self.filters = ['Category', 'Interest']


    def start_requests(self):
        self.logger.info(f'launch_url: {self.launch_url}')
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, list):
                urls = self.launch_url
            elif isinstance(self.launch_url, str):
                urls = [self.launch_url]

            for url in urls:
                yield self.create_request(url=url, callback=self.parse, dont_filter=True)
                yield self.create_request(url=url, callback=self.parse_filter, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://figpin.com/collections/figpin-xl/products/endeavor-x26
        @scrape_values id title image price series sku description collection
        @meta {"use_proxy":"True"}
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//h1[@class="title"]/text()').extract_first(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first() \
                or response.xpath('//div[@class="main-image"]/a/@href').extract_first(),
            'price': response.xpath('//h2[@class="h4-style price-area"]/span[@class="price theme-money"]/text()').extract_first(),
            'series': response.xpath('//div[@class="vendor"]/a/text()').extract_first(),
            'sku': response.xpath('//div[@class="sku "]/span[@class="sku__value"]/text()').extract_first(),
            'description': response.xpath('//div[@class="description user-content lightboximages"]/ul/li//text()').extract(),
            'collection': json.loads(response.xpath('//script[@type="application/vnd.locksmith+json"]/text()').extract_first())['state']['collection'],
        }


    def parse_filter(self, response):
        """
        @url https://figpin.com/collections/figpin-standard
        @returns requests 1 20
        @meta {"use_proxy":"True"}
        """

        for filter_type in response.xpath('//div[@id="main-nav"]/div[@aria-label="Primary navigation"]/div[@class="tier-1"]/ul/li[@data-linkgd="Shop" or @data-linkgd="Catalog"]/ul/li'):
            filter_name = filter_type.xpath('normalize-space(./a/text())').extract_first().replace('By', '').strip()
            if filter_name in self.filters:
                for filter_button in filter_type.xpath('./ul/li'):
                    filter_value = filter_button.xpath('./a/text()').extract_first()
                    url = filter_button.xpath('./a/@href').extract_first()
                    url = self.base_url + url
                    request = self.create_request(url=url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                            filter_name: filter_value,
                    }
                    yield request


    def parse(self, response):
        """
        @url https://figpin.com/collections/figpin-standard
        @returns requests 1 41
        @meta {"use_proxy":"True"}
        """

        extra_item_info = response.meta.get('extra_item_info', {})
        for product in response.xpath('//div[@class="product-list"]/div'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                id = self.base_url + id
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.update_extra_item_infos(id, extra_item_info)

                yield self.create_request(url=id, callback=self.crawlDetail)

        nextLink = response.xpath('//div[@class="pagination"]/a[@class="next"]/@href').extract_first()
        if nextLink:
            nextLink = self.base_url + nextLink
            request = self.create_request(url=nextLink, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
