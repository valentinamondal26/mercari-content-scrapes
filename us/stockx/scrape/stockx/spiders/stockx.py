'''
https://mercari.atlassian.net/browse/USDE-530 - Yeezy Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1130 - Off-White Men's Athletic Shoes
https://mercari.atlassian.net/browse/USDE-1131 - Air Jordan Men's Athletic Shoes
https://mercari.atlassian.net/browse/USDE-1132 - Nike Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1134 - Gucci Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1135 - Vans Fashion Sneakers
https://mercari.atlassian.net/browse/USCC-63 - New Balance Men's Athletic Shoes
https://mercari.atlassian.net/browse/USDE-1144 - Fila Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1145 - PUMA Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1146 - Reebok Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1147 - Converse Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-1149 - Versace Fashion Sneakers

https://mercari.atlassian.net/browse/USCC-135 - Supreme Backpacks, Bags & Briefcases
https://mercari.atlassian.net/browse/USCC-106 - Louis Vuitton Men's Belts
https://mercari.atlassian.net/browse/USCC-324 - Supreme Men's T-Shirts

https://mercari.atlassian.net/browse/USCC-333 - Chanel Wallets
https://mercari.atlassian.net/browse/USCC-335 - Gucci Wallets

https://mercari.atlassian.net/browse/USCC-224 - Gucci Women's Belts
https://mercari.atlassian.net/browse/USCC-109 - Louis Vuitton Men's Wallets
'''

import scrapy
from scrapy.exceptions import CloseSpider
import os
import re
import datetime
import json
from urllib.parse import urlparse, parse_qs, urlencode
import itertools


class StockxSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from stockx.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Athletic Shoes
        2) Women's Athletic Shoes
        3) Wallets
        4) Women's Belts
        5) Men's T-Shirts

    brand : str
        category to be crawled, Supports
        1) Air Jordan
        2) Nike
        3) Adidas
        4) All
        5) Supreme

    Command e.g:
    scrapy crawl stockx -a category="Women's Athletic Shoes" -a brand='All'
    scrapy crawl stockx -a category="Backpacks, Bags & Briefcases" -a brand='Supreme'

    scrapy crawl stockx -a category="Men's Belts" -a brand='Louis Vuitton'
    scrapy crawl stockx -a category="Men's T-Shirts" -a brand='Supreme'

    scrapy crawl stockx -a category="Wallets" -a brand='Chanel'
    scrapy crawl stockx -a category="Wallets" -a brand='Gucci'

    scrapy crawl stockx -a category="Women's Belts" -a brand='Gucci'
    scrapy crawl stockx -a category="Men's Wallets" -a brand='Louis Vuitton'
    """

    name = 'stockx'

    category = None
    brand = None

    merge_key = 'id'

    source = 'stockx.com'
    blob_name = 'stockx.txt'
    
    spider_project_name = "stockx"
    gcs_cache_file_name = 'stockx.tar.gz'


    gender = ''
    brand_tag = None
    product_category = 'sneakers'

    product_listing_page_url = 'https://stockx.com/api/browse?'
    page = 0

    detail_page_url = 'https://stockx.com/api/products/{url_key}?includes=market,360&currency=USD'

    historical_details_url = 'https://stockx.com/api/products/{product_uuid}/activity?state=480&currency=USD&limit={count}&page={page}&sort=createdAt&order=DESC'

    sold_count_max_per_page = 20000

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]
 
    url_dict = {
        "Men's Athletic Shoes" : {
            'Jordan': { # retired
                'url': 'https://stockx.com/retro-jordans',
            },
            'Nike': { # retired
                'url': 'https://stockx.com/nike',
            },
            'Adidas': { # retired
                'url': 'https://stockx.com/adidas?size_types=men',
            },
            'All': { # retired
                'url': 'https://stockx.com/sneakers?size_types=men',
            },
            "Air Jordan": {
                "url" : "https://stockx.com/retro-jordans?size_types=men",
                "filters" : ["series"]
            },
            "Off-White": {
                "url" : "https://stockx.com/off-white-sneakers?size_types=men",
                "filters" : []
            },
            "New Balance": {
                "url" : "https://stockx.com/new-balance?size_types=men",
                "filters" : []
            },
        },
        "Women's Athletic Shoes": {
            'All': { # retired
                'url': 'https://stockx.com/sneakers?size_types=women',
            },
        },
        "Men's T-Shirts": {
            'Supreme': {
                'url': 'https://stockx.com/supreme/t-shirts/fw19?gender=men',
            }
        },
        "Wallets": {
            'Chanel': {
                'url': 'https://stockx.com/chanel/wallet?gender=women',
            },
            'Gucci':  {
                'url': 'https://stockx.com/gucci/wallet?gender=women',
            }
        },
        "Women's Belts": {
            'Gucci': {
                'url': 'https://stockx.com/gucci/belt?gender=women',
            },
        },
        "Men's Wallets": {
            'Louis Vuitton': {
                'url': 'https://stockx.com/louis-vuitton/wallet?gender=men',
            },
        },
        "Men's Belts": {
            'Louis Vuitton': {
                'url': 'https://stockx.com/louis-vuitton/belt?gender=men',
            },
        },
        "Backpacks, Bags & Briefcases": {
            'Supreme': {
                'url': 'https://stockx.com/supreme/bags?gender=men',
            },
        },
        "Fashion Sneakers":{
            "Yeezy":{
                "url" : "https://stockx.com/adidas/yeezy?size_types=men",
                "filters" : []
            },
            "Nike":{
                "url" : "https://stockx.com/nike?size_types=men",
                "filters" : ["series"]
            },
            "Gucci":{
                "url" : "https://stockx.com/gucci-sneakers?size_types=men",
                "filters" : []
            },
            "Fila":{
                "url" : "https://stockx.com/search/sneakers?size_types=men&s=fila",
                "filters" : []
            },
            "Puma":{
                "url" : "https://stockx.com/puma?size_types=men",
                "filters" : []
            },
            "Reebok":{
                "url" : "https://stockx.com/reebok?size_types=men",
                "filters" : []
            },
            "Converse":{
                "url" : "https://stockx.com/converse?size_types=men",
                "filters" : []
            },
            "Versace":{
                "url" : "https://stockx.com/versace-sneakers?size_types=men",
                "filters" : []
            },
            "Vans":{
                "url" : "https://stockx.com/vans?size_types=men",
                "filters" : []
            },
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(StockxSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return
        else:
            self.SCRAPY_CHECK = False

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get("url","")
        # self.launch_url = self.url_dict.get(category, {}).get(brand, '')

        self.round_robin = itertools.cycle(self.proxy_pool)
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get("filters","")

        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        o = urlparse(self.launch_url)
        d = parse_qs(o.query)
        if d:
            if (self.category == "T-Shirts" or self.category == "Wallets" or
                self.category == "Women's Belts" or self.category == "Men's Wallets"
                or self.category == "Men's Belts"
                or self.category == "Backpacks, Bags & Briefcases"):
                    self.gender=''.join(d.get('gender',[]))
            else:
                self.gender = ''.join(d.get('size_types', []))
        path = o.path
        sp = path.split('/')
        # self.logger.info(sp)
        if len(sp) > 1:
            self.brand_tag = sp[-1]
            if self.brand_tag == 'retro-jordans':
                self.brand_tag = 'air jordan'
        self.logger.info('brand_tag: {}, gender: {}'.format(self.brand_tag, self.gender))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def contracts_mock_function(self):
        self.brand = 'Nike'
        self.round_robin = itertools.cycle(self.proxy_pool) 
        self.SCRAPY_CHECK = True


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.filters:
            for filter in self.filters:
                meta={}
                meta["filter"] = filter
                request =self.createRequest(url= self.launch_url,callback = self.filter_page,errback=None,meta = meta)
                yield request
        if self.brand_tag:
            meta ={}
            meta["page"] = 0
            yield self.req(filter_name=None,meta=meta)
    

    def filter_page(self,response):
        """
        @url https://stockx.com/nike?size_types=men
        @returns requests 1
        @meta {"filter":"series","use_proxy":"True"}
        """
        meta = response.meta
        if meta["filter"]=="series":
            # response_json =  json.loads(re.findall("window.preLoadedBaseProps =(.*);", response.xpath('//script[contains(text(), "window.preLoadedBaseProps =")]/text()').extract_first())[0]) 
            response_json =  json.loads(re.findall("window.preLoadedBrowseProps =(.*);", response.xpath('//script[contains(text(), "window.preLoadedBrowseProps =")]/text()').extract_first())[0])
            for brands in  response_json.get("filters",{}).get("sneakers",{}).get("children",{})[0].get("children",[]):
                brand =  brands["name"]
                if brand == 'Nike' and self.brand == "Nike":
                    for filter in brands["children"]:
                        filter_name = filter["name"]
                        meta.update({"page":0})
                        # print(filter_name)
                        # if filter_name == 'Basketball':
                        yield self.req(filter_name,meta)
                elif brand == 'Air Jordan' and self.brand =="Air Jordan":
                    for filter in brands["children"]:
                        filter_name = filter["algolia"]
                        meta.update({"page":0})
                        # print(filter_name)
                        yield self.req(filter_name,meta)



    def get_url(self,category,brand):
        try:
            return self.url_dict[category][brand]
        except Exception as e:
            self.logger.error('Exception occured:{}'.format(e))
            raise(ValueError('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))


    def crawlDetail(self, response):
        """
        @url https://stockx.com/api/products/nike-dunk-low-brazil-2020?includes=market,360&currency=USD
        @scrape_values id crawl_category release_date title price image lowestAsk highestBid description condition style color est_retail listed_price_history breadcrumb product_line gender
        @meta {"use_proxy":"True"}
        """
        data = json.loads(response.text)
        product = data.get('Product', {})
        open("sample.txt","w").write(json.dumps(product))

        item = dict()

        base_url = 'https://stockx.com'
        product_id = product.get('urlKey', '')
        item['id'] = '{}/{}'.format(base_url, product_id) if product_id else response.url

        brand_name = product.get('brand', '') or ''

        item['category'] = self.category
        item['brand'] = brand_name if self.brand == 'All' else self.brand
        item['crawl_date'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['status_code'] = str(response.status)
        item['crawl_category'] = product.get('contentGroup', '') or ''
        item['release_date'] = product.get('releaseDate', '') or ''

        uuid = product.get('uuid', '')
        item['title'] = product.get('title', '') or ''
        average_sale_price = product.get('market', {}).get('averageDeadstockPrice', 0)
        item['price'] = str(average_sale_price) if average_sale_price else ''
        item['image'] = product.get('media', {}).get('imageUrl', '')
        item["lowestAsk"] = product.get('market',{}).get("lowestAsk","")
        item['highestBid'] = product.get('market',{}).get("highestBid","")

        # Buy Price
        listed_price_history = []
        for key, variant in product.get('children', {}).items():
            market = variant.get('market', {})
            average_sale_price = market.get('averageDeadstockPrice', 0)
            lowest_bid = market.get('lowestAsk',0)
            lowest_ask_size = market.get('lowestAskSize',0)
            shoe_size = variant.get('shoeSize', 0)
            price = {
                'average_sale_price': str(average_sale_price) if average_sale_price else '',
                # 'amount': str(average_sale_price) if average_sale_price else '',
                'condition': variant['condition'],
                'size': str(shoe_size) if shoe_size else '',
                'currency_code': 'USD',
                'lowest_bid_price' : lowest_bid,
                'lowest_ask_size' : lowest_ask_size,
                'date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            }
            listed_price_history.append(price)

        item['description'] = product.get('description', '') or ''
        item['condition'] = product.get('condition', '') or ''
        item['style'] = product.get('styleId', '') or ''
        item['color'] = product.get('colorway', '') or ''
        est_retail_price = product.get('retailPrice', 0)
        item['est_retail'] = str(est_retail_price) if est_retail_price else ''
        traits=product.get('traits', [])
        for trait in traits:
            if trait.get('name','')=="Color":
                item['color']=trait.get('value','')
            if trait.get('name','')=="Season":
                item['season']=trait.get('value','')
            if trait.get('name','')=="Retail":
                item['est_retail'] = trait.get('value', '')
            if trait.get('name','')=="height":
                item['height'] = trait.get('value', '')
            if trait.get('name','')=="width":
                item['width'] = trait.get('value', '')
            if trait.get('name','')=="depth":
                item['depth'] = trait.get('value', '')
            if trait.get('name','')=="Material":
                item['material'] = trait.get('value', '')
            if trait.get('name','')=="Hardware":
                item['metal_color'] = trait.get('value', '')
            if trait.get('name','')=="Size":
                item['size'] = trait.get('value', '')
            if trait.get('name','')=="Season":
                item['Season'] = trait.get('value', '')
                        
        breadcrumb = []
        for b in product.get('breadcrumbs', []):
            breadcrumb.append(b.get('name', ''))
        item['breadcrumb'] = " | ".join(breadcrumb)
        item['listed_price_history'] = listed_price_history
        item['price_history'] = []
        item['product_line'] = product.get('secondaryCategory', '') or ''
        item['gender'] = product.get('gender', '') or ''
        meta_data={}
        meta =  response.meta
        filter_name = meta.get("filter","")
        if filter_name!="":
            meta_data =  meta_data.fromkeys([filter_name],meta[filter_name])
            item.update(meta_data)

        sold_count = product.get('market', {}).get('deadstockSold', 0)
        if sold_count and not self.SCRAPY_CHECK:
            url = self.historical_details_url.format(product_uuid=uuid,
                count=sold_count if sold_count < self.sold_count_max_per_page else self.sold_count_max_per_page,
                page=1)
            meta = {}
            request = self.createRequest(url=url, callback=self.parse_price_history, errback=self.parse_price_history_error_handler,meta=meta)
            request.meta['item'] = item
            request.meta['uuid'] = uuid
            yield request
        else:
            item['price_history'] = []
            yield item


    def parse_price_history(self, response):
        """
        @url https://stockx.com/api/products/2e05f9fd-db88-428a-92c5-09f0cc97fd7c/activity?state=480&currency=USD&limit=3655&page=1&sort=createdAt&order=DESC
        @scrape_values price_history
        @meta {"use_proxy":"True"}
        """
        item = response.meta.get('item',{})
        price_history = item.get('price_history',[])
        sold_details = json.loads(response.text)
        for sold_detail in sold_details.get('ProductActivity', []):
            amount = sold_detail.get('amount', 0)
            # "createdAt": "2019-08-19T14:28:28+00:00",
            createdAt = sold_detail.get('createdAt', '')
            date = ''
            try:
                date = datetime.datetime.strptime(createdAt, "%Y-%m-%dT%H:%M:%S+00:00").strftime("%Y-%m-%d") if createdAt else ''
            except Exception as e:
                self.logger.error('Exception occured: {}'.format(e))

            price = {
                'amount': str(amount) if amount else '',
                'condition': "New",
                'size': sold_detail.get('shoeSize', '') or '',
                'currency_code': 'USD',
                # 'date': date_parser.parse(createdAt).strftime("%Y-%m-%d %H:%M:%S") if createdAt else ''
                'date': date
            }
            price_history.append(price)

        item['price_history'] = price_history

        current_page = sold_details.get('Pagination', {}). get('page', 0)
        next_page_url = sold_details.get('Pagination', {}). get('nextPage', None)
        if next_page_url:
            next_page = current_page + 1
            uuid = response.meta.get('uuid','')
            url = self.historical_details_url.format(product_uuid=uuid,
                count=self.sold_count_max_per_page, page=next_page)
            request = self.createRequest(url=url, callback=self.parse_price_history, errback=self.parse_price_history_error_handler)
            request.meta['item'] = item
            request.meta['uuid'] = uuid
            yield request
        else:
            yield item


    def parse_price_history_error_handler(self,failure):
        self.logger.error("Handling failed request " + failure.request.url)

        item = failure.request.meta['item']
    
        yield item


    def parse(self, response):
        """
        @url https://stockx.com/api/browse?_tags=nike&productCategory=sneakers&page=1&gender=men
        @meta {"page":1,"filter":"series","series":"Foamposite"}
        @returns requests 1
        @meta {"use_proxy":"True","page": 1}
        """
        meta = response.meta
        data = json.loads(response.text)

        last_page = 0
        last_page_url = data.get('Pagination', {}).get('lastPage', '')
        if last_page_url:
            o = urlparse(last_page_url)
            d = parse_qs(o.query)
            if d:
                last_page = int(''.join(d.get('page', [])))

        for product in data.get('Products', []):
            # id = product.get('id', '')
            url_key = product.get('urlKey', '')
            if url_key:
                url = self.detail_page_url.format(url_key=url_key)
                yield self.createRequest(url=url, callback=self.crawlDetail, errback = None,meta=meta)
        if meta["page"] < last_page:
            if meta.get("filter","") == "series" :
                filter_name = meta["series"]
                yield self.req(filter_name, meta)
            else:
                yield self.req(filter_name=None, meta=meta)


    def req(self,filter_name=None,meta=None):
        meta["page"]+=1
        if self.category=="T-Shirts":
            tags='supreme,t-shirts'
            category = 'streetwear'
        elif self.category == "Wallets":
            if self.brand == "Chanel":
                tags = 'chanel,styles|wallet'
            elif self.brand == "Gucci":
                tags = 'gucci,styles|wallet'
            category = 'handbags'
        elif self.category=="Women's Belts": 
            tags='gucci,styles|belt'
            category = 'handbags'
        elif self.category=="Men's Wallets": 
            tags='louis vuitton,styles|wallet'
            category = 'handbags'
        elif self.category=="Men's Belts": 
            tags='louis vuitton,styles|belt'
            category = 'handbags'
        elif self.category=="Backpacks, Bags & Briefcases":
            tags='supreme,bags'
            category = 'streetwear'     
        else:
            tags=self.brand.lower()
            category="sneakers"
            if filter_name:
                meta.update({meta["filter"]:filter_name})
                query_params = {
                    "_tags": filter_name.lower()+","+tags,
                    "productCategory":category,
                    "page": str(meta["page"])
                }
            else:
                query_params = {
                    "_tags": tags,
                    "productCategory":category,
                    "page": str(meta["page"])
                }
        # if self.brand_tag:
        #     query_params["_tags"] = self.brand_tag.lower()

        if self.gender:
            query_params['gender'] = self.gender
        url = self.product_listing_page_url + urlencode(query_params)
        return self.createRequest(url=url, callback=self.parse, errback=None, meta=meta)


    def createRequest(self, url, callback, errback=None, meta=None):
        request = scrapy.Request(url=url, callback=callback, errback=errback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] =  next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request

