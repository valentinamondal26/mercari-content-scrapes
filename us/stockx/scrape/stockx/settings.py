# -*- coding: utf-8 -*-

# Scrapy settings for stockx project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'stockx'

SPIDER_MODULES = ['stockx.spiders']
NEWSPIDER_MODULE = 'stockx.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

ROBOTSTXT_OBEY = False

DEFAULT_REQUEST_HEADERS = {
    'referer': 'https://stockx.com/vans?size_types=men',
    #'sec-fetch-dest': 'empty',
    #'sec-fetch-mode': 'cors',
    #'sec-fetch-site': 'same-origin',
    # 'x-anonymous-id': 'b693dbf3-c9fe-45fe-ac59-f7c983049685',
    # 'x-anonymous-id': 'b693dbf3-c9fe-45fe-ac59-f7c983049685',
    #"authority": "stockx.com",
    #"method": "GET",
    #'scheme': "https",
    #"accept": "*/*",
    #"accept-encoding": "gzip, deflate, br",
    #"accept-language": "en-US,en;q=0.9,fr;q=0.8",
    #"appos": "web",
    #"appversion": "0.1",
}

CONCURRENT_REQUESTS = 5

AUTOTHROTTLE_ENABLED = True

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

# GCS settings - actual value be 'raw/stockx.com/men_athletic_shoes/adidas/2019-02-06_00_00_00/json/stockx.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}