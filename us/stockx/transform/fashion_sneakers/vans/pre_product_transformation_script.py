import hashlib
import re
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')

            model = re.sub(re.compile(
                r'\bSneakers\b|\bShoes\b', re.IGNORECASE), '', title)
            model = re.sub(re.compile(
                r"\bAuthentic\b|\(|\)|Size\s*\?|\bMulti\b|\bMulti\s*\-*color\b|\bCamo\b|\b\s*\-\s*color|\-Logo|\btrue\b", re.IGNORECASE), "", model)
            model = re.sub(re.compile(material, re.IGNORECASE), '', model)

            color = re.sub(re.compile(
                r"\bMul\b|\bmulti\s*\-*color\b|\bMgnlhytr\b|\bmulti\b", re.IGNORECASE), "Multicolor", color)
            color = re.sub(re.compile(
                r"\bMickey\b|\bNightmare\b|\bCheck\b|\bVans\b", re.IGNORECASE), "", color)

            if "/" not in color.strip("/"):
                color = "/".join(color.strip("/").split("-"))

            color = re.sub(re.compile(r"\bdigi\b|\bDigita\b",
                                      re.IGNORECASE), "Digital", color)
            if "Digital" in color:
                model = re.sub(re.compile(
                    r"\bdigi\b|\bdigital\b|\bDigita\b", re.IGNORECASE), "", model)

            colors = re.findall(r'\w+', color)
            if title != "Vans Authentic Black Outsole Herbal Grey":
                colors += [i for i in re.findall(r"\w+", title)
                           if self.check_color(i)]
            for cl in colors:
                model = re.sub(re.compile(r"\b{}\b|\b{}s\b".format(
                    cl, cl), re.IGNORECASE), '', model).strip()
            year_pattern = re.compile(r'[19|20]\d{3}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(r"\b(\w{1})\s*\&\s*(\w{1})\b", r"\1&\2", model)
            model = re.sub(
                r"[^\w\d\)]$", "", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(re.compile(
                r"All Online Exclusive|Online Only|Shop Only", re.IGNORECASE), "", model)
            d = {
                "Sinners": "Sinner's",
                "GroundBrkrs": 'GroundBreakers'
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)
            model = re.sub(r'\s+', ' ', model).strip()
            if re.findall(re.compile(r"^x\s|^x$", re.IGNORECASE), model):
                if len(model) > 1:
                    model = model[1:]
                else:
                    model = ''
            model = re.sub(re.compile(r"Reissue", re.IGNORECASE), "Re-Issue", model)
            model = re.sub(re.compile(r"Slip On", re.IGNORECASE), "Slip-On", model)

            model = re.sub(r'\s+', ' ', model).strip()

            color = re.sub(re.compile(
                r"\b\ss\s|\b\ss$", re.IGNORECASE), "", color)
            if title == "Vans Authentic Supreme Bruce Lee (Blue)" and re.findall(re.compile(r"\bnavy\b", re.IGNORECASE), color):
                color = re.sub(re.compile(
                    r"\b(navy)\b", re.IGNORECASE), r"\1 Blue", color)
            color = re.sub(r'\s+', ' ', color).strip()
            color = color.split("/")
            color = sorted(set(color), key=color.index)
            color = "/".join(color)
            color = re.sub(re.compile(r"\bChckrbrd\b",
                                      re.IGNORECASE), "Checkerboard", color)
            color = re.sub(re.compile(
                r"\bOG\b|\(*\s*Chambray\s*\)*|Suede|Coated Canvas", re.IGNORECASE), "", color)
            color = re.sub(r"\/\s(\w+)", r"/\1", color)
            color = re.sub(r"\-\s(\w+)", r"-\1", color)
            color = re.sub(r"(\w+)\s\/", r"\1/", color)
            color = re.sub(r"(\w+)\s\-", r"\1-", color)
            if color == "New York/Yankees/Navy":
                color = "New York Yankees/Navy"

            # remove duplicatec colors
            sorted_colors = []
            for ctr, c in enumerate(color.split("/")):
                words = c.split("-")
                sorted_colors += sorted(set(words), key=words.index)
                for ctr1, cl in enumerate(sorted_colors):
                    check = [c for ctr2, c in enumerate(
                        sorted_colors) if ctr1 != ctr2]
                    check = [c.split() for c in check]
                    check = sum(check, [])
                    if cl in check:
                        sorted_colors.remove(cl)
            color = "/".join(sorted_colors)
            color = "/".join(sorted(set(color.split("/")),
                                    key=color.split("/").index))
            color = color.strip(" /-")
            # put multicolor at rear end
            if "Multicolor" in color:
                multi_color = [c for c in color.split(
                    "/") if re.findall(re.compile(r"\bMulticolor\b", re.IGNORECASE), c)]
                color = [c for c in color.split(
                    "/") if not re.findall(re.compile(r"\bMulticolor\b", re.IGNORECASE), c)]
                color += multi_color
                color = "/".join(color)
            if "Multicolor" in color and len(color.split("/")) > 2:
                color = color.split("/")[0]+"/Multicolor"

            if color == "Blackberry Cordial/Tillandsia Purple/Grape Jam/Southern Moss":
                color = "Blackberry/Purple Multicolor"

            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern, release_date)[0]
            except IndexError:
                year = ''

            description = record.get('description', '')
            description = re.sub(re.compile(
                r'\bGrab a fresh pair of vans on StockX now.\b', re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                    or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),

                'title': title,
                'image': record.get('image', ''),

                'model': model,

                'item_condition': record.get('condition', ''),
                'product_line': record.get('product_line', ''),
                'style': record.get('style', ''),
                'description': description,
                'color': color,
                'release_year': year,
                'release_date': record.get('release_date', ''),

                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail', ''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
