import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title','')

            brand = record.get('brand','')
            material = record.get('material','')
            color = record.get('color','')

            model =  re.sub(re.compile(r'\bSneakers\b|\bShoes\b',re.IGNORECASE),'',title)
            model = re.sub(re.compile(brand,re.IGNORECASE),'',model)
            model = re.sub(re.compile(material,re.IGNORECASE),'',model)
            colors = re.findall(r'\w+',color)
            for color in colors:
                model = re.sub(re.compile(color,re.IGNORECASE),'',model).strip()
            year_pattern = re.compile(r'[19|20]\d{3}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(r'\s+',' ',model).strip()

            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern,release_date)[0]
            except IndexError:
                year = '' 

            description = record.get('description','')
            description = re.sub(re.compile(r'\bGrab a fresh pair of vans on StockX now.\b',re.IGNORECASE),'',description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history','')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series','').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),

                'title': title,
                'image': record.get('image', ''),

                'model': model,

                'item_condition':record.get('condition', ''),
                'color' : color,
                'product_line' : record.get('product_line', ''),
                'style' : record.get('style', ''),
                'description' : description,
                'color' : record.get('color', ''),
                'release_year': year,
                'release_date': record.get('release_date', ''),
                
                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail',''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code' : 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None
