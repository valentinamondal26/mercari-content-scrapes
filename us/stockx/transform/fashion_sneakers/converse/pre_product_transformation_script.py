import hashlib
import re
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')

            model = re.sub(re.compile(
                r'\bSneakers*\b|\bShoes\b', re.IGNORECASE), '', title)
            model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
            model = re.sub(re.compile(material, re.IGNORECASE), '', model)
            model = re.sub(re.compile(record.get(
                'style', ''), re.IGNORECASE), "", model)
            model = re.sub(re.compile(r"Multi\-*\s*color|\bmulti\b",
                                      re.IGNORECASE), " Multicolor ", model)
            colors = re.findall(r'\w+', color)
            colors += [i for i in re.compile(r'\s|\/').split(model)
                       if self.check_color(i)]
            colors += ["Crom", "Multicolor", "Camo", "Burgundy", "Monocrom", 'Peach Pearl', 'Clearwater', 'Airway']
            for cl in colors:
                model = re.sub(re.compile(r"\b{}\b".format(
                    cl), re.IGNORECASE), '', model).strip()
            year_pattern = re.compile(r'[19|20]\d{3}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(
                r"\s(\-|\/)\s|\s(\-|\/)$|\s(\-|\/)|(\-|\/)\s", "", model)
            model = re.sub(re.compile(
                r"\(*\s*Special Box\s*\)*|size\s*\?*|\bshoes*\b|\(|\)|\bSneaker\b", re.IGNORECASE), "", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(re.compile(
                r"\bHeart\b", re.IGNORECASE), "Multi-Heart", model)
            model = re.sub(re.compile(r"\b(Off)\-*\s|\b(Off)\-*$",
                                      re.IGNORECASE), r"\1\2-White", model).strip("/")

            d = {
                'UNDFTD': 'Undefeated',
                'GLITTER_GUTTER': 'Glitter Gutter',
                'Ox': 'OX',
                'Play': 'PLAY',
                'And': 'and',
                'Or': 'or',
                'With': 'with',
                'For': 'for'
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()])), lambda x: d.get(x.group()), model)
            
            model = re.sub(re.compile(r'\bcx\b',re.IGNORECASE),'CX',model)
            model = re.sub(r'(?!\w+-\d+)(\w+-\w+)',lambda x:x.group().title(),model) 
            model = re.sub(re.compile(r'\bCorduroy\b',re.IGNORECASE),'',model)
            model = re.sub(r"'70|\b70\b",'70s',model)
            model = re.sub(r'(\s-\w+)|(\w+-\s)|(\w+-$)',lambda x:x.group().replace('-',''),model).strip()
            model = re.sub(r'(Coca-Cola)|(ERX-400)',lambda x:x.group().replace('-',' '),model,flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\bct\b',re.IGNORECASE),'CT',model)
            model = re.sub(re.compile(r'Sophnet\.',re.IGNORECASE),'Sophnet',model)
            model_words = re.findall(r"\w+",re.sub(re.compile(r"off\-*\s*white", re.IGNORECASE), "", model))
            for mod in model_words:
                if self.check_color(mod):
                    model = re.sub(re.compile(r'\b{}\b'.format(mod),re.IGNORECASE),'',model)
            model = re.sub(r'Chuck Taylor All-Star 70s Hi Kith X Coca Cola', 'Chuck Taylor All-Star 70s Hi Kith Coca Cola',\
                    model, flags=re.IGNORECASE)
            model = f'{model}  Sneakers'
            model = re.sub(r'\s+', ' ', model).strip()

            color = re.sub(re.compile(r"Multi\-*\s*color|\bmulti\b",
                                      re.IGNORECASE), "Multicolor", color).title()
            color = re.sub(re.compile(r"\bBLK\b", re.IGNORECASE),
                           "Black", color).title()
            color = re.sub(re.compile(r"\bHou\b", re.IGNORECASE),
                           "Hour", color).title()

            falttern_color = [cl.strip() for c in color.split("/")
                              for cl in c.split("-")]
            color = "/".join(sorted(set(falttern_color),
                                    key=falttern_color.index))
            if "Multicolor" in color:
                color = re.sub(re.compile(
                    r"(.*)(\/*)multicolor(\/*)(.*)", re.IGNORECASE), r"\2\1\4\3Multicolor", color)
            color = re.sub(re.compile(r'Clear Water',re.IGNORECASE),'Clearwater',color)
            color = re.sub(re.compile(r'olve',re.IGNORECASE),'Olive',color)
            color = re.sub(re.compile(r'Raw Sugar/Dark Clo',re.IGNORECASE),'Raw Sugar Brown',color)
            
            if 'Corduroy' in title.title():
                color = f'{color} Corduroy'
            colors = color.split('/')
            if len(colors) >= 3:
                color = f'{colors[0]}/Multicolor'
            color = re.sub(r'\bOptical\b', 'Optic', color, flags=re.IGNORECASE)
            color = re.sub(r'\/\w$', '', color, flags=re.IGNORECASE)
            color = re.sub(r'\bLt Blu\b', 'Light Blue', color, flags=re.IGNORECASE)
            color = re.sub(r'\bBlack Buckle Studded/Black\b', 'Black', color, flags=re.IGNORECASE)
            colors = color.split('/')
            
            traditional_colors = []
            secondary_colors = []
            for col in colors:
                try:
                    Color(col)
                    traditional_colors.append(col)
                except ValueError:
                    secondary_colors.append(col)
            if traditional_colors and secondary_colors:
                traditional_colors = '/'.join(traditional_colors)
                secondary_colors = '/'.join(secondary_colors)
                color =f'{traditional_colors}/{secondary_colors}'
            if len(colors) == 2:
                if re.findall(r'\b{col}\b'.format(col=colors[0]), colors[1], flags=re.IGNORECASE):
                    color = colors[1]
                elif re.findall(r'\b{col}\b'.format(col=colors[1]), colors[0], flags=re.IGNORECASE):
                    color = colors[0]
            
            if record['id'] == 'https://stockx.com/converse-crimson-ox-buffalo':
                model = 'Buffalo OX Sneakers'
                color = 'Crimson/Multicolor'
            
            color = re.sub(r'\s+', ' ', color).strip()

            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern, release_date)[0]
            except IndexError:
                year = ''

            description = record.get('description', '')
            description = re.sub(re.compile(
                r'\bGrab a fresh pair of vans on StockX now.\b', re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                    or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders', 'Boot', 'boots']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),

                'title': title,
                'model': model,
                'color': color,


                'image': record.get('image', ''),


                'item_condition': record.get('condition', ''),
                'product_line': record.get('product_line', ''),
                'style': record.get('style', ''),
                'description': description,
                'release_year': year,
                'release_date': record.get('release_date', ''),

                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail', ''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
