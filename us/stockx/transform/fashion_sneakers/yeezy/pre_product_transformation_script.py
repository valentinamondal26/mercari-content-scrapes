import hashlib
import re
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')

            # model transformation
            model = re.sub(re.compile(
                r'\bSneakers\b|\bShoes\b|\(|\)|\bModel\b|Non\-*\s*Reflective|\bBSKTBL\b|Sneakers*', re.IGNORECASE), '', title)
            model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
            model = re.sub(re.compile(material, re.IGNORECASE), '', model)
            # remove color from model
            colors = re.compile(r'\/|\-').split(color)
            for cl in re.compile(r'\s|\/|\-').split(color):
                model = re.sub(re.compile(
                    r"\b{}\b".format(cl), re.IGNORECASE), '', model)

            # remove year and model colors from model
            year_pattern = re.compile(r'[19|20]\d{3}')
            model = re.sub(year_pattern, '', model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            _colors = [i for i in re.compile(
                r'\s|\/').split(model) if self.check_color(i)]
            _colors = [cl for cl in _colors if cl != '']
            for cl in _colors:
                model = re.sub(re.compile(
                    r"\b{}\b".format(cl), re.IGNORECASE), '', model)
            # color transformation - removing duplicates
            colors += _colors
            colors = re.sub(re.compile(r"\bYEGLRF\b", re.IGNORECASE),
                            "YYEGLRF", "/".join(colors)).split("/")
            colors = sorted(set(colors), key=colors.index)
            color = "/".join(colors)
            # remove left over /Triple from model after color removal
            model = re.sub(re.compile(
                r"\/*\s*Triple\b", re.IGNORECASE), "", model)
            model = re.sub(r'\s+', ' ', model).strip()

            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern, release_date)[0]
            except IndexError:
                year = ''

            description = record.get('description', '')
            description = re.sub(re.compile(
                r'\bGrab a fresh pair of vans on StockX now.\b', re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                    or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model) \
                    or re.findall(re.compile(r"\bboot\b|\bslippers*\b", re.IGNORECASE), title):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),

                'title': title,
                'image': record.get('image', ''),

                'model': model,

                'item_condition': record.get('condition', ''),
                'color': color.title(),
                'product_line': record.get('product_line', ''),
                'style': record.get('style', ''),
                'description': description,
                'release_year': year,
                'release_date': record.get('release_date', ''),

                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail', ''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
