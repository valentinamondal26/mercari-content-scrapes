import hashlib
import re

from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')

            # model transformations
            model = re.sub(re.compile(r'\bSneakers\b|\bShoes\b', re.IGNORECASE),
                           '', title)
            model = re.sub(
                    re.compile(r"Multi\-*\s*Color|\bmulti\b", re.IGNORECASE),
                    "Multicolor", model)
            model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
            model = re.sub(re.compile(material, re.IGNORECASE), '', model)
            colors = re.findall(r'\w+', color)
            colors += [i for i in re.compile(r'\s|\/').split(model)
                       if self.check_color(i)]
            for cl in colors:
                model = re.sub(re.compile(r"\b{}\b".format(cl), re.IGNORECASE),
                               '', model).strip()

            year_pattern = re.compile(r'(19|20)\d{3}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(r"\/\/|\s(\-|\/)\s|\s(\-|\/)$|\s(\-|\/)|(\-|\/)\s",
                           " ", model)
            model = re.sub(re.compile(r"size\s*\?*|\(|\)|\bsneaker\b|"
                                      r"\bshoes*\b|Tri\-*\s*color|\"\s*\"|"
                                      r"multicolor",
                           re.IGNORECASE), "", model)
            if re.findall(re.compile(r"3D OP\.*\s*Lite\b", re.IGNORECASE),
                          model):
                model = re.sub(re.compile(r"\blite\b", re.IGNORECASE), "",
                               model)
            model = re.sub(re.compile(r"\_", re.IGNORECASE), r" ", model)
            model = re.sub(re.compile(r"10 10th Anniversary", re.IGNORECASE),
                           "10th Anniversary", model)
            model = re.sub(
                    re.compile(r"\b(Off)\-\s*|\b(Off)\-$", re.IGNORECASE),
                    r"\1\2-White", model).strip("/ - .")
            model = " ".join([m[0].upper() + m[1:] for m in model.split()])
            model = re.sub(re.compile(r"\b(OP)\.", re.IGNORECASE), r"OP ",
                           model)
            model = re.sub(re.compile(r"^(cl)\s", re.IGNORECASE), "CL ", model)
            model = re.sub(re.compile(r"\bEdt\.", re.IGNORECASE), "Edition",
                           model)
            model = re.sub(re.compile(r"\bUNDFTD\b", re.IGNORECASE),
                           "Undefeated", model)
            model = re.sub(re.compile(r"\bis\b", re.IGNORECASE), "IS", model)
            model = re.sub(re.compile(r"\bog\b", re.IGNORECASE), "OG", model)

            model = re.sub(
                    re.compile(r"\bLthr\b|\bLtr\b|\bPkbl\b", re.IGNORECASE), "",
                    model)
            model = re.sub(
                    re.compile(r"\band$|\bwith$|\bor$|\bfor$", re.IGNORECASE),
                    "", model)

            def to_upper(match):
                return match.group(0).upper()
            model = re.sub(re.compile(r"\bSo\b|\bTr\b|\bUk\b|\bNpc\b",
                           re.IGNORECASE), to_upper, model)
            model = re.sub(re.compile(r"(\d+)\s*\:\s*(\w{2})", re.IGNORECASE),
                           r"\1\2", model)

            model = re.sub(r'(.*)(\bI{1,3}\b)(\s\b\d\b)(.*)',r'\1 \2 \4',model)  #CL Nylon RBC Patent II 2 ---> CL Nylon RBC Patent II

            model = re.sub(r'CNCPTS','Concepts',model,flags=re.IGNORECASE)
            model = re.sub(r'Instapump Fury Sneakersnstuff A About Something','Instapump Fury Sneakersnstuff About Something',model,flags=re.IGNORECASE)
            model = re.sub(r'\bMID\b','Mid',model,flags=re.IGNORECASE)
            model = re.sub(r'\bHoodie\b|\bCamo\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            # color transformations
            color = re.sub(
                    re.compile(r"Multi\-*\s*color|\bmulti\b", re.IGNORECASE),
                    "Multicolor", color).title()
            color = re.sub(re.compile(r"\bL\b|\bR\b", re.IGNORECASE), "", color)

            def add_slash(match):
                if "-" in match.group(0):
                    return "/"
                else:
                    return ""
            color = re.sub(re.compile(r"\-*Reebok\-*", re.IGNORECASE),
                           add_slash, color)
            colors = color.split("/")
            for ctr, cl in enumerate(colors):
                colors[ctr] = "-".join(
                        sorted(set(cl.split("-")), key=cl.split("-").index))
                if "-" in colors[ctr] and len(colors[ctr].split("-")) > 2:
                    colors[ctr] = "Multicolor"
            color = "/".join(colors)
            if "Multicolor" in color:
                color = re.sub(re.compile(r"(.*)(\/*)multicolor(\/*)(.*)",
                               re.IGNORECASE), r"\2\1\4\3Multicolor", color)
            if len(color.split("/")) > 3:
                color = color.split("/")[0] + "/Multicolor"
            color = "/".join([c.strip() for c in sorted(set(color.split("/")),
                                                        key=color.split(
                                                                "/").index)])
            color = "/".join(color.split("-"))
            if len(color.split("/")) > 2:
                color = color.split("/")[0]+"/Multicolor"

            color = re.sub(r'^Stl/Multicolor$|\bWht\b', 'White', color, flags=re.IGNORECASE)
            color = re.sub(r'Red, Black And Grey', 'Black/Multicolor', color, flags=re.IGNORECASE)
            color = re.sub(r'\bOg\b', 'OG', color, flags=re.IGNORECASE)
            color = re.sub(r'\bGry\b', 'Grey', color, flags=re.IGNORECASE)
            color = re.sub(r'\bGid/Multicolor\b', 'Acid Rain/White', color, flags=re.IGNORECASE)
            color = re.sub(r'\bExb\b', 'EXB', color, flags=re.IGNORECASE)
            color = re.sub(r'\bBlk\b', 'Black', color, flags=re.IGNORECASE)
            if re.findall(r'\bCamo\b', title):
                color = re.sub(r'\bCaf̩', 'Camo', color, flags=re.IGNORECASE)  ## In the word Caf̩ 'f̩' is a special character because of that \b is not given in the regex.   
            
            # extract release data
            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern, release_date)[0]
            except IndexError:
                year = ''

            # description transformation
            description = record.get('description', '')
            description = re.sub(
                    re.compile(r'\bGrab a fresh pair of vans on StockX now.\b',
                               re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            # extract shoe sizes
            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(
                    map(lambda x: r'\b' + x + r'\b',
                        ['packs', 'spizike', 'other']))), re.IGNORECASE),
                    series) \
                    or re.findall(re.compile('|'.join(list(
                    map(lambda x: r'\b' + x + r'\b',
                        ['slides', 'slide', 'sliders']))), re.IGNORECASE),
                    model):
                return None

            transformed_record = {
                    'id': record.get('id', ''),
                    'item_id': hex_dig,
                    'crawl_date': record.get('crawl_date', '').split(',')[
                        0].strip(),
                    'status_code': record.get('status_code', ''),
                    'brand': brand,
                    'category': record.get('category', ''),
                    'image': record.get('image', ''),
                    'title': title,
                    'model': model,
                    'color': color,
                    'item_condition': record.get('condition', ''),
                    'style': record.get('style', ''),
                    'description': description,
                    'release_year': year,
                    'release_date': record.get('release_date', ''),
                    'msrp': {
                            'currency_code': 'USD',
                            'amount': record.get('est_retail', ''),
                    },
                    'price_history': record.get('price_history', '') or '',
                    'price': {
                            'currency_code': 'USD',
                            'amount': record.get('price', ''),
                    },
                    # 'lowestbid': {
                    #    'currency_code': 'USD',
                    #    'amount': record.get('lowestAsk', ''),
                    # },
                    # 'highestbid': {
                    #     'currency_code': 'USD',
                    #     'amount': record.get('highestBid', ''),
                    # },

                    'series': record.get('series', ''),
                    'material': record.get('material', ''),
                    'product_line': record.get('product_line', ''),
                    'listed_price_history': record.get('listed_price_history', '') or '',
                    'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except Exception:
            return False
