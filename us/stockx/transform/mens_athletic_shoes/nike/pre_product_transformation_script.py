import hashlib
import re
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')

            model = re.sub(re.compile(
                r"Signed Card|Restock Pair|Regular Box|Special Box|Sole Collector|"
                r"\(\s*Extra Lace Set Only\s*\)|Promo|"
                r"\(\s*House of Hoops Special Box and Accessories\s*\)|\(\s*Special Packaging\s*\)|With Socks|No Socks|"
                r"Size\?|"
                r"\s\/$|\s*[\-\-|\-]$|Multi\-*\s*Color|\(|\)", re.IGNORECASE), " ", title)
            model = re.sub(re.compile(
                r"LV\s*(\d+)", re.IGNORECASE), r"LV\1", model)
            d = {
                'UNDFTD': 'Undefeated',
                'Anniv': 'Anniversary',
                'Ltd': 'LTD'
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)

            model = re.sub(re.compile(
                r'\bSneakers\b|\bShoes\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
            model = re.sub(re.compile(material, re.IGNORECASE), '', model)
            colors = ['Burgundy\-*Paisley']
            colors += re.findall(r'\w+', color)
            for cl in colors:
                model = re.sub(re.compile(r"\b{}\b".format(cl), re.IGNORECASE),
                               '', model)
            _colors = [i.strip("()") for i in model.split()
                       if self.check_color(i.strip("()"))]
            for cl in _colors:
                model = re.sub(re.compile(r"\b{}\b".format(cl),
                                          re.IGNORECASE), '', model)
            model = " ".join(model.split())
            if re.findall(re.compile(r"\boff\s*\-*(\w+)", re.IGNORECASE), title):
                model = re.sub(re.compile(r"\b(off)\s*\-*\s|\b(off)\s*\-*$", re.IGNORECASE), r"Off-"+re.findall(
                    re.compile(r"\boff\s*\-*(\w+)", re.IGNORECASE), title)[0]+" ", model)
            if re.findall(re.compile(r"\bFade To\s*(\w+)", re.IGNORECASE), title):
                model = re.sub(re.compile(r"\b(Fade To)\s|\b(Fade To)$", re.IGNORECASE), r"Fade To "+re.findall(
                    re.compile(r"\bFade To\s*(\w+)", re.IGNORECASE), title)[0]+" ", model)
            model = ' '.join(model.split())
            model = re.sub(
                r"\(\s*\)|\"\s*\"|\b\/$|[^a-zA-Z0-9\)\"]+$", "", model)
            model = re.sub(r"\(\s*(.*[^\s$])\s*\)", r"(\1)", model)
            model = re.sub(r"\"\s*(.*)\s*\"", r'"\1"', model)
            model = re.sub(r"\s07", " '07", model)
            model = re.sub(re.compile(
                r"(Air Force \d*[\s\w]*\'\d+\s*L*V*\s*\d+)\s*\d+", re.IGNORECASE), r"\1", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(re.compile(
                r"Kobe A\.*\s*D\.*", re.IGNORECASE), r"Nike Kobe A.D.", model)
            model = re.sub(re.compile(
                r"OFF\-*\s*WHITE", re.IGNORECASE), "Off-White", model)

            model = " ".join(model.split())

            try:
                release_date = record.get('release_date')
                year = re.findall(re.compile(r'[19|20]\d{3}'), release_date)[0]
            except IndexError:
                year = ''

            description = record.get('description', '')
            description = re.sub(re.compile(
                r'\bGrab a fresh pair of vans on StockX now.\b', re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            colors = re.sub(re.compile(r"Multi\-*Color|\bMulti\b",
                                       re.IGNORECASE), "Multicolor", color)

            colors = [c.split("-") for c in colors.split("/")]
            less_priority = []
            for ctr, l in enumerate(colors):
                prio = []
                for ll in l:
                    _colors = [i for i in re.compile(
                        r'\s|\/').split(ll) if self.check_color(i)]
                    if _colors:
                        prio.append(ll)
                    else:
                        less_priority.append(ll)
                colors[ctr] = prio
            colors = sum(colors, [])+less_priority
            colors = sorted(set(colors), key=colors.index)
            if len(colors) > 2 and "Multicolor" not in colors:
                color = "/".join([l.strip()for l in colors][0:2])+" Multicolor"
            else:
                color = "/".join([l.strip()for l in colors][0:2])
            color = re.sub(re.compile(
                r"off\sWhite", re.IGNORECASE), "Off-White", color)
            if re.compile(r"Nike Air Force 1 Low Reflective Desert Camo", re.IGNORECASE).match(title) and color == "":
                color = "Desert Camo"
                model = re.sub(re.compile(
                    r"Desert Camo\b", re.IGNORECASE), "", model)
            model= " ".join(model.split())

            old_color = color
            color = re.sub(r"(\w+\d*)\s*/\s*(\w+\d*)",r"\1/\2",color)
            # color = color.title()
            color_words=color.split("/")
            for ctr1,cl1 in enumerate(color_words):
                color_inside=cl1.split()
                for ctr2,cl2 in enumerate(color_inside):
                    if re.findall(re.compile(r"White|Black|Purple|pearl", re.IGNORECASE),cl2):
                        color_inside[ctr2]=cl2.title()
                    else:
                        color_inside[ctr2]=cl2[0].upper()+cl2[1:]
                color_words[ctr1]=" ".join(color_inside)
            color = "/".join(color_words)
            color = re.sub(re.compile(r"\bColor\b", re.IGNORECASE), "", color)
            color =  re.sub(r"\,|\;","/",color)
            d = {
                'Gree': 'Green',
                'Whitie': 'White',
                'Opti': 'Optic',
                'Lcqr': '',
                'Univ': 'University',
                'Rasbperry': 'Raspberry',
                'Bl':"Blue"
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            color = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), color)
            if re.findall(re.compile(r"\bVarious\b", re.IGNORECASE), color):
                color = ''
            
            # remove duplicates
            color = "/".join(sorted(set(color.split("/")), key=color.split("/").index))
            color=color.split("/")
            for ctr,cl in enumerate(color):
                color[ctr]=" ".join(sorted(set(cl.split()),key=cl.split().index))
            color = "/".join(color)
            position_to_remove=[]
            for ctr1,cl1 in enumerate(color.split("/")):
                for ctr2,cl2 in enumerate(color.split("/")):
                    if ctr1!=ctr2 and cl1 in cl2:
                        position_to_remove.append(ctr1)
            color = "/".join([c for ctr,c in enumerate(color.split("/")) if ctr not in position_to_remove])
            # put Multicolor at rear end
            if "Multicolor" in color:
                multi_color = [c for c in color.split("/") if re.findall(re.compile(r"\bMulticolor\b", re.IGNORECASE),c)]
                color = [c for c in color.split("/") if not re.findall(re.compile(r"\bMulticolor\b", re.IGNORECASE),c)]
                color+=multi_color
                color = "/".join(color)
            if len(color.split("/"))>2:
                color = "/".join([color.split("/")[0],"Multicolor"])
            
            model_meta = ['Air Foamposite One XX 20th Anniversary 2017', 'Air Foamposite One XX 20th Anniversary 2017', 'Air Foamposite Pro', 'Air Foamposite Pro', 'Air Foamposite Pro Pearl', 'Air Foamposite Pro Pearl', 'Air Force 1 High Riccardo Tisci Victorious Minotaurs', 'Air Force 1 High Riccardo Tisci Victorious Minotaurs', 'Air Force 1 Low', 'Air Force 1 Low', 'Air Force 1 Low', 'Air Force 1 Low', 'Air Force 1 Low A Cold Wall', 'Air Force 1 Low A Cold Wall', 'Air Force 1 Low Just Do It Pack', 'Air Force 1 Low Just Do It Pack', 'Air Force 1 Low Just Do It Pack', 'Air Force 1 Low Utility', 'Air Force 1 Low Utility', 'Air Force 1 Mid Utility', 'Air Force 1 Mid Utility', 'Air Force 1 Utility', 'Air Force 1 Utility', 'Air Max 1', 'Air Max 1', 'Air Max 1', 'Air Max 1 Jewel', 'Air Max 1 Jewel', 'Air Max 270', 'Air Max 270', 'Air Max 270', 'Air Max 270', 'Air Max 90 Recraft', 'Air Max 90 Recraft', 'Air Max 90 Recraft', 'Air Max 90 Recraft', 'Air Max 97', 'Air Max 97', 'Air Max 97', 'Air Max 97', 'Air Max 97', 'Air Max 97', 'Air Max 97', 'Air Max 97 Olympic Rings Pack', 'Air Max 97 Olympic Rings Pack', 'Air Max 98', 'Air Max 98', 'Air Max 98', 'Air Presto Off-White 2018', 'Air Presto Off-White 2018', 'Air VaporMax Flyknit 3', 'Air VaporMax Flyknit 3', 'Air VaporMax Plus', 'Air VaporMax Plus', 'Air VaporMax Plus', 'Air Zoom Generation', 'Air Zoom Generation', 'Air Zoom Generation Retro', 'Air Zoom Generation Retro', 'Kobe 4', 'Kobe 4', 'Kobe 7', 'Kobe 7', 'Kobe 7', 'LeBron 15 Griffey', 'LeBron 15 Griffey', 'LeBron 7', 'LeBron 7', 'LeBron Zoom Soldier 10 Team Bank', 'LeBron Zoom Soldier 10 Team Bank', 'Zoom Soldier II Team Bank', 'Zoom Soldier II Team Bank', 'Zoom Soldier III', 'Zoom Soldier III', 'Zoom Soldier III Team Bank', 'Zoom Soldier III Team Bank', 'Zoom Soldier III Team Bank']
            if model in model_meta:
                color = re.sub(re.compile(r"White\/Black Multicolor", re.IGNORECASE), "Black/White Multicolor", color)
            
            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                    or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model) \
                or re.findall(re.compile(r"\(*\s*sample\s*\)*", re.IGNORECASE), model) != [] or not isinstance(model, str):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),
                'title': title,
                'model': model,
                'image': record.get('image', ''),
                'brand': brand,
                'category': "Men's Athletic Shoes",
                'item_condition': record.get('condition', ''),
                'product_line': record.get('product_line', ''),
                'style': record.get('style', ''),
                'description': description,
                'color': color,
                'release_year': year,
                'release_date': record.get('release_date', ''),

                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail', ''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
