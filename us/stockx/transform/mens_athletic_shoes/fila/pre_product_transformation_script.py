import hashlib
import re

from colour import Color
from titlecase import titlecase


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')

            # model transformations
            model = re.sub(re.compile(
                    r'\bSneakers\b|\bShoes\b', re.IGNORECASE), '', title)

            if len(re.findall(r"\bfila\b", model, flags=re.IGNORECASE)) > 1:
                model = re.sub(re.compile(r"\bfila\b", re.IGNORECASE), "",
                               model)
                model = "Fila " + model

            model = re.sub(re.compile(r"M\-Squad", re.IGNORECASE), "M Squad",
                           model)

            model = re.sub(re.compile(material, re.IGNORECASE), '', model)

            # remove color from model
            colors = re.findall(r'\w+', color)
            colors += [i for i in re.findall(r"\w+", model)
                       if self.check_color(i)]
            colors = [c for c in colors
                      if not re.compile(r"\bfila\b", re.IGNORECASE).match(c)]
            for cl in colors:
                model = re.sub(re.compile(r"\b{}\b".format(
                        cl), re.IGNORECASE), '', model).strip()

            # remove year from model
            year_pattern = re.compile(r'\(*(19|20)\d{2}\)*')
            model = re.sub(year_pattern, '', model)
            model = re.sub(r"\(\s*\)|\s\/\s|\s\/$|Multi\-*\s*Color|\bmulti\b",
                           "", model)

            model = titlecase(model)
            model = re.sub(r"II 2", "II", model)
            update_strings = ['FILA', 'Flla', 'FIla']
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',update_strings))), 'Fila', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            # color transformations
            color = re.sub(re.compile(r"\bFila\b", re.IGNORECASE), "", color)

            color = re.sub(re.compile(r"Multi\-*\s*color|\bmulti\b",
                           re.IGNORECASE), "Multicolor", color)
            color = color.split("/")
            color = "/".join(sorted(set(color), key=color.index))
            backup_color = color
            color = re.sub(r"\-", " ", color)
            colors = backup_color.split("/")
            colors = [cc for c in colors for cc in c.split("-")]
            if len(colors) > 2:
                color = color.split("/")[0] + "/Multicolor"
            color = re.sub(r"\s(\/)\s|\s(\/\w+)|(\w+\/)\s", r"\1\2\3", color)
            color = re.sub(r"\s+", " ", color).strip()

            # extract release year
            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern, release_date)[0]
            except IndexError:
                year = ''

            # description transformation
            description = record.get('description', '')
            description = re.sub(re.compile(
                    r'\bGrab a fresh pair of vans on StockX now.\b',
                    re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            # extract shoe sizes
            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(
                    map(lambda x: r'\b' + x + r'\b',
                        ['packs', 'spizike', 'other']))), re.IGNORECASE),
                    series) \
                    or re.findall(re.compile('|'.join(list(
                    map(lambda x: r'\b' + x + r'\b',
                        ['slides', 'slide', 'sliders']))), re.IGNORECASE),
                    model):
                return None

            transformed_record = {
                    'id': record.get('id', ''),
                    'item_id': hex_dig,
                    'crawl_date': record.get('crawl_date', '').split(',')[
                        0].strip(),
                    'status_code': record.get('status_code', ''),

                    'brand': brand,
                    'category': record.get('category', ''),

                    'image': record.get('image', ''),
                    'title': title,

                    'model': model,
                    'color': color,

                    'item_condition': record.get('condition', ''),
                    'product_line': record.get('product_line', ''),
                    'style': record.get('style', ''),
                    'description': description,
                    'release_year': year,
                    'release_date': record.get('release_date', ''),

                    'msrp': {
                            'currency_code': 'USD',
                            'amount': record.get('est_retail', ''),
                    },
                    'price_history': record.get('price_history', ''),
                    'price': {
                            'currency_code': 'USD',
                            'amount': record.get('price', ''),
                    },
                    # 'lowestbid': {
                    #    'currency_code': 'USD',
                    #    'amount': record.get('lowestAsk', ''),
                    # },
                    # 'highestbid': {
                    #     'currency_code': 'USD',
                    #     'amount': record.get('highestBid', ''),
                    # },

                    'series': record.get('series', ''),
                    'material': record.get('material', ''),
                    'listed_price_history': record.get('listed_price_history',
                                                       ''),
                    'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
