import hashlib
import re
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')

            brand = record.get('brand', '')
            material = record.get('material', '')
            color = record.get('color', '')
            model = title
            for word in re.findall(r"\w+", color):
                model = re.sub(re.compile(r"\b{}\b".format(
                    word), re.IGNORECASE), "", model)
            color = re.sub(re.compile(
                r'\bBlac\b', re.IGNORECASE), 'Black', color)
            color = re.sub(re.compile(
                r'\bPrpl\b', re.IGNORECASE), 'Purple', color)
            color = re.sub(re.compile(
                r'\bMulti-Color\b|\bMulti Color\b', re.IGNORECASE), 'Multicolor', color)
            colors = re.split(r'/|-', color)
            if len(colors) > 2:
                color = '/'.join([colors[0] if colors[0] !=
                                  'Multicolor' else colors[1], 'Multicolor'])
            color = '/'.join([' '.join(list(dict.fromkeys(a.split(' '))))
                              for a in list(dict.fromkeys(color.split('/')))])

            keywords = [
                'Blank Tag', 'Sample', 'Special Box', 'Regular Box', 'Special Packaging',
                'Both Pairs', 'Gym Red',
            ]
            keywords.append(material)
            keywords.extend(re.findall(r'\w+', color))
            model = re.sub(re.compile(r'Univ\.', re.IGNORECASE),
                           'University', model)
            model = re.sub(re.compile(r'|'.join(
                list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = re.sub(re.compile(
                r'Multi-Color|\bColor\b', re.IGNORECASE), '', model)
            model = re.sub(
                r"'\s\d", lambda x: x.group().replace(' ', ''), model)
            # model = re.sub(r'\(\s', '(',model)
            # model = re.sub(r'\s\)', ')',model)
            # model = re.sub(r'\(\s*\)', '',model)
            model = re.sub(
                r' - |\s-\s*$|\s\/\s*$|W\/ No Pants and T\-Shirt|With T\-Shirt|No Shirt', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()
            d = {
                'off-white': 'Off-White',
                'quai54': 'Quai 54',
                'undftd': 'Undefeated',
                'all-star': 'All Star',
                'bel-air': 'Bel Air',
            }
            model = re.sub(re.compile(
                r"levi's \(tag with levi's logo\)|levi's \(levi's tag\)", re.IGNORECASE), "Levi's Tag", model)
            model = re.sub(re.compile(r'Jordan Air Jordan',
                                      re.IGNORECASE), 'Jordan', model)
            # if re.findall(r'Jordan', model) and not re.findall(r'X', model):
            #     model = re.sub(re.compile(r'\bJordan\b', re.IGNORECASE), '', model)
            
            model = re.sub(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', d.keys()))),
                                      re.IGNORECASE), lambda x: d.get(x.group().lower(), x.group()), model)
            model = re.sub(r'\(|\)|[^\w\d\"\)]+$|\"\"', '', model)
            model = re.sub(r"\s\/\s|\s\/$", " ", model)
            model = re.sub(r'\s+', ' ', model).strip()

            extra_color = [i.strip() for i in re.findall(
                r"\w+", model) if self.check_color(i)]
            model = re.sub(re.compile(
                r"|".join(extra_color), re.IGNORECASE), "", model)

            model = re.sub(re.compile(r'\boff\-*\s|\boff\-*\s*$',
                                      re.IGNORECASE), 'Off-White ', model)
            model = re.sub(r'\s+', ' ', model).strip()

            try:
                release_date = record.get('release_date', '')
                year = re.findall(re.compile(r'[19|20]\d{3}'), release_date)[0]
            except IndexError:
                year = ''

            description = record.get('description', '')
            description = re.sub(re.compile(
                r'\bGrab a fresh pair of vans on StockX now.\b', re.IGNORECASE), '', description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history', '')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series', '').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                    or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),


                'image': record.get('image', ''),

                'title': title,
                'model': model,

                'item_condition': record.get('condition', ''),
                'color': color,
                'product_line': record.get('product_line', ''),
                'style': record.get('style', ''),
                'description': description,
                'release_year': year,
                'release_date': record.get('release_date', ''),

                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail', ''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
