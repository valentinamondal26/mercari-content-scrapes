import hashlib
import re
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title','')

            brand = record.get('brand','')
            material = record.get('material','')
            color = record.get('color','')

            model =  re.sub(re.compile(r'\bSneakers\b|\bShoes\b',re.IGNORECASE),'',title)
            remove_strings = ['Multi-color', 'Tri-Color', 'Multicolor', 'Multi', 'Burghundy', 'Standard Width', 'Wide Width', \
                'Made In', 'Colors']
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', remove_strings))), '', model, flags=re.IGNORECASE)

            model = re.sub(re.compile(material,re.IGNORECASE),'',model)
            colors = re.findall(r'\w+',color)
            for col in colors:                
                model = re.sub(re.compile(r'(?!New Balance)\b{col}\b'.format(col=col),re.IGNORECASE),'',model).strip()
            color = re.sub(r'\s*/\s*','/',color)
            multi_color_strings = ['Multi-Color','Multi']
            color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',multi_color_strings))),'Multicolor',color,flags=re.IGNORECASE)           
            color = '/'.join(list(dict.fromkeys(color.split('/')))) ## remove duplicates from list without changing order
            color = color.replace('-',' ')
            colors = color.split('/')
            if len(colors) > 2:
                color = colors[0]+'/'+'Multicolor'
            elif len(colors) == 2: 
                #'Wheat/White Wheat ---> White Wheat'
                if re.findall(r'\b{col}\b'.format(col=colors[0]), colors[1], flags=re.IGNORECASE): 
                    color = colors[1]
                elif re.findall(r'\b{col}\b'.format(col=colors[1]), colors[0], flags=re.IGNORECASE):
                    color = colors[0]
            color = re.sub(r'LightGrey', 'Light Grey', color, flags=re.IGNORECASE)
            colors = color.split('/')
            color = '/'.join(list(dict.fromkeys(colors)))
            year_pattern = re.compile(r'\b(19|20)\d{2}\b')  
            model = re.sub(year_pattern, '', model)
            model = re.sub(r'\s/|\s\|\s/\s','',model)
            model_words = re.findall(r'\w+',model)
            for mod in model_words:
                if self.check_color(mod):
                    model = re.sub(r'\b{mod}\b'.format(mod=mod),'',model,flags=re.IGNORECASE)
            model = re.sub(r'New Balance-','New Balance ',model,flags=re.IGNORECASE)
            model = re.sub(r'""','',model)
            remove_strings = ['Special Box', 'Special Packaging', 'Regular Box', 'Removed Logo', 'Encap Reveal']
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',remove_strings))),'',model,flags=re.IGNORECASE)
            match = re.findall(r'"\s\w+\s*\w*"',model)
            if match:
                found_string = match[0]
                replace_string = re.sub(r'^"\s','"',found_string)
                model = re.sub(found_string,replace_string,model,flags=re.IGNORECASE)
            model = re.sub(r'\s+',' ',model).strip()
            model = re.sub(r'Blance','Balance',model,flags=re.IGNORECASE)    
            model = ' '.join([mod[0].upper()+mod[1:] for mod in model.split()]) ##titlecasing the model
            model = re.sub(r'size\?|\(\s*\)|\s-\s|-$|\(|\)|\"','',model,flags=re.IGNORECASE)
            model = re.sub(r'\(\s+','(',model,flags=re.IGNORECASE)
            model = re.sub(r'\s+\)',')',model,flags=re.IGNORECASE)
            model = re.sub(r'Classics', 'Classic', model, flags=re.IGNORECASE)
            model = re.sub(r'J\. Crew', 'J Crew', model, flags=re.IGNORECASE)
            update_strings = ['U\.S\.A\.*','U\.K\.*']
            model = re.sub(r'|'.join(list(map(lambda x: x, update_strings))), lambda x:x.group().replace('.',''), model, flags=re.IGNORECASE)
            model = re.sub(r'_', '-', model, flags=re.IGNORECASE)
            model = re.sub(r'New Balance X-90', 'New Balance X90', model, flags=re.IGNORECASE)
            model = re.sub(r'\bX Racer\b', 'X-Racer', model, flags=re.IGNORECASE)
            model = re.sub(r'\bUNDFTD\b', 'Undefeated', model, flags=re.IGNORECASE)
            model = re.sub(r'\bSneakers*\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\b(1300)(JP)\b', r'\1 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = f'{model} Sneakers'
            try:
                release_date = record.get('release_date')
                year = re.findall(r'\b\d{4}\b',release_date)[0]
            except IndexError:
                year = '' 

            description = record.get('description','')
            description = re.sub(re.compile(r'\bGrab a fresh pair of vans on StockX now.\b',re.IGNORECASE),'',description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history','')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series','').lower()

            if re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),

                'title': title,
                'image': record.get('image', ''),

                'model': model,

                'item_condition':record.get('condition', ''),
                'color' : color,
                'product_line' : record.get('product_line', ''),
                'style' : record.get('style', ''),
                'description' : description,
                # 'old_color' : record.get('color', ''),
                'release_year': year,
                'release_date': record.get('release_date', ''),
                
                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail',''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code' : 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def check_color(self,model_word):
        try:
            Color(model_word)
            return True
        except ValueError:
            return False
