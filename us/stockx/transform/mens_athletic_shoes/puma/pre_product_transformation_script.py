import hashlib
import re
from titlecase import titlecase
from colour import Color
class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title','')

            brand = record.get('brand','')
            material = record.get('material','')
            color = record.get('color','')
            # old_color =  color

            model =  re.sub(re.compile(r'\bSneakers*\b|\bShoes\b|\(|\)',re.IGNORECASE),'',title)
            model = re.sub(re.compile(brand,re.IGNORECASE),'',model)
            model = re.sub(re.compile(material,re.IGNORECASE),'',model)
            # remove colors from model
            colors = re.findall(r'\w+',color)
            colors += [i for i in re.findall(r"\w+", model)
                       if self.check_color(i)]
            for cl in colors:
                model = re.sub(re.compile(r"\b{}\b".format(cl),re.IGNORECASE),'',model).strip()
            # remove year from model
            year_pattern = re.compile(r'\(*\s*(19|20)\d{2}\s*\)*')
            model = re.sub(year_pattern, '', model)
            # removing style numbers from model
            model = re.sub(r"\d{4,}", " ", model)
            # remove rouge / - from model
            model = re.sub(r"\/\/|\s(\-|\/)\s|\s(\-|\/)$|\s(\-|\/)|(\-|\/)\s|size\?|\(*\s*Special Box\/*\s*\-*Cannister\s*\)*|\(*\s*Special Box\s*\)*", " ", model)
            # titlecase retain source capitalization
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            # string transformations
            model = re.sub(re.compile(r"UNDFTD", re.IGNORECASE), "Undefeated", model)
            model = re.sub(re.compile(r"(\bMy\-*\s*)(\d+)", re.IGNORECASE), r"MY-\2", model)
            model = re.sub(re.compile(r"\bTx\b", re.IGNORECASE),"TX", model)
            # fix spacing issue in within quoted text : eg: "text "-> "text"
            model = re.sub(r"\"\s*(\w+)\s*\"", r'"\1"', model)
            # remove empty quotes: eg: "", " "
            model = re.sub(r'\"\s*\"', "", model)
            # remove reduntant string Karl from model and transform
            # Suede Classic Karl 2 Karl Lagerfeld - > Suede Classic Karl Lagerfeld 2
            model = re.sub(re.compile(r"(.*)Karl(.*)Karl(.*)", re.IGNORECASE),r"\1Karl\2\3", model)
            model = re.sub(re.compile(r"(.*)(Karl\s*)(\d+)(.*)", re.IGNORECASE), r"\1\2\4 \3", model).strip("/ . - +")
            model = re.sub(re.compile(r'\bRwb\b',re.IGNORECASE),'',model)                        
            if model.lower() == 'mcqueen mobe lo alexander mcqueen':
                self.string_count = 0 ##make sure this variable is initialised to zero when try to remove any other second occurence of the string
                model = re.sub(r'\bMcqueen\b',lambda x: self.remove_string(x),model,flags=re.IGNORECASE) ##mcqueen mobe lo alexander mcqueen --> mcqueen mobe lo alexander

            if model == 'Rs100 Op Lux':
                model = 'RS100 Op Lux'
            model = re.sub(r'\bIi\b|\bIl\b','II',model)
            model = re.sub(r'(.*)(XT)(\d+\+*)(.*)',r'\1 \2 \3 \4',model,flags=re.IGNORECASE)
            model = re.sub(r'\bXt\b','XT',model,flags=re.IGNORECASE)
            if model == 'TX-3':
                model = 'TX 3'
            model = re.sub(r'G\s*v\s*Special', 'GV Special', model, flags=re.IGNORECASE)
            model = re.sub(r'Lqd\s*Cell', 'LQD CELL', model, flags=re.IGNORECASE)
            model = re.sub(r'Wvo', 'WVO', model, flags=re.IGNORECASE)
            def transform_string(model_part):
                value = re.sub(r'\s+', '', model_part)
                value =  re.sub(r'RS|XT', lambda x: x.group().replace(x.group(), f'{x.group()}-'),\
                     value, flags=re.IGNORECASE) ###RS100 --> RS-100 or XT 2---> XT-2  or RS X → RS-X 
                return value
            model = re.sub(r'\bRS\s*\d+\b|\bRS\s*\w+\b|\bXT\s*\d+\b|\bXT\s*\w\d+\b', lambda x: transform_string(x.group()), model, flags=re.IGNORECASE)
            model = f'{model} Sneakers'

            model = re.sub(r'\s+',' ',model).strip()

            # multi, multi-color, multi-color-gum to multicolor
            color = re.sub(re.compile(r"Multi\-Color\-Gum|Multi\-*\s*color|\bmulti\b", re.IGNORECASE), "Multicolor", color)
            def add_slash(match):
                if "-" in match.group(0):
                    return "/"
                else:
                    return ""
            # remove -puma from color     
            color = re.sub(re.compile(r"\-*puma\-*", re.IGNORECASE), add_slash, color)
            # color string fixes
            color =  re.sub(re.compile(r"Cb\-Nasturtium\-White\-Lg", re.IGNORECASE), "Nasturtium White", color)
            color =  re.sub(re.compile(r"White\-Vg\-Buttercup\-Cb", re.IGNORECASE), "White Buttercup", color)
            # move multicolor to rear end
            if "Multicolor" in color:
                color = re.sub(re.compile(r"(.*)(\/*)multicolor(\/*)(.*)", re.IGNORECASE), r"\2\1\4\3Multicolor", color)
            # if len(colors) > 3 make it as color1/multicolor
           
            if not re.findall(re.compile(r'(\bRed\b)-\s*(\bRed\b|\bBlue\b|\bWhite\b)-\s*(\bWhite\b|\bBlue\b|\bRed\b)',re.IGNORECASE),color):
                color = '/'.join(color.split('-'))

            if len(color.split("/")) > 3:
                color = color.split("/")[0]+ "/Multicolor"
            # remove duplicates at "/" level
            color = "/".join([c.strip() for c in sorted(set(color.split("/")), key=color.split("/").index)])

            # remove duplicates at "-" level eg:Black/White-Black -> Black/White
            colors = color.split("/")
            colors = []
            for ctr,cl in enumerate(color.split("/")):
                color_part= cl.split("-")
                if len(color_part) == 1:
                    colors.append(color_part[0])
                else:
                    color_sub = []
                    for ctr1,c in enumerate(color_part):
                        if c not in color.split("/"):
                            color_sub.append(c)
                    colors.append(color_sub)

            color= ""
            for cl in colors:
                if isinstance(cl, list):
                    color+="/"+"-".join(cl)
                else:
                    color+="/"+cl
            color = color.strip("/")
            # fix spacing issue with / and - in color
            color = re.sub(r"(\w+)\s*\-\s*(\w+)",r"\1-\2", color)
            color = re.sub(r"(\w+)\s*\/\s*(\w+)",r"\1/\2", color)

            color = re.sub(re.compile(r'\bmediebal\b',re.IGNORECASE),'Medieval',color)
            if color == 'White/Whisper White' or color == 'White/White Whisper':
                color = 'Whisper White'
            colors = color.split('/')
            if len(colors) > 2 and not re.findall(r'(\bWhite\b|\bRed\b|\bBlue\b)\/(\bWhite\b|\bRed\b|\bBlue\b)\/(\bWhite\b|\bRed\b|\bBlue\b)', color, flags=re.IGNORECASE):
                color = f'{colors[0]}/Multicolor'
            color = re.sub(r'Grn', 'Green', color, flags=re.IGNORECASE)
            color = re.sub(r'White\s+Silver', 'White/Silver', color, flags=re.IGNORECASE)
            if not re.findall(r'Multicolor', color, flags=re.IGNORECASE):
                primary_colors = []
                secondary_colors = []
                colors = color.split('/')
                for col in colors:
                    col_type = self.check_color(col)
                    if col_type:
                        primary_colors.append(col)
                    else:
                        secondary_colors.append(col)
                primary_colors = '/'.join(primary_colors)
                secondary_colors = '/'.join(secondary_colors)
                if primary_colors and secondary_colors:
                    color = f'{primary_colors}/{secondary_colors}'
            color = re.sub(r'\bTurbulence Black\b', 'Black/Turbulence', color, flags=re.IGNORECASE)
            color = titlecase(color)
            try:
                release_date = record.get('release_date')
                year = re.findall(year_pattern,release_date)[0]
            except IndexError:
                year = ''

            description = record.get('description','')
            description = re.sub(re.compile(r'\bGrab a fresh pair of vans on StockX now.\b',re.IGNORECASE),'',description)
            description = re.sub(re.compile('<.*?>'), '', description)
            if 'PLEASE NOTE SIZE CONVERSIONS' in description:
                description = ''
            description = description.replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            us_shoe_size_mens = []
            listed_price_history = record.get('listed_price_history','')
            for price in listed_price_history:
                size = price.get('size', '')
                try:
                    if (float(size)) < 20:
                        us_shoe_size_mens.append(size)
                except ValueError:
                    pass
            us_shoe_size_mens = ', '.join(us_shoe_size_mens)

            series = record.get('series','').lower()

            if not model or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['packs', 'spizike', 'other']))), re.IGNORECASE), series) \
                or re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', ['slides', 'slide', 'sliders']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(',')[0].strip(),
                'status_code': record.get('status_code', ''),

                'brand': brand,
                'category': record.get('category', ''),

                'title': title,
                

                'model': model,
                # "old_color": old_color,
                'color' : color,
                'image': record.get('image', ''),

                'item_condition':record.get('condition', ''),
                
                'product_line' : record.get('product_line', ''),
                'style' : record.get('style', ''),
                'description' : description,
                'release_year': year,
                'release_date': record.get('release_date', ''),
                
                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('est_retail',''),
                },
                'price_history': record.get('price_history', ''),
                'price': {
                    'currency_code' : 'USD',
                    'amount': record.get('price', ''),
                },
                # 'lowestbid': {
                #    'currency_code': 'USD',
                #    'amount': record.get('lowestAsk', ''),
                # },
                # 'highestbid': {
                #     'currency_code': 'USD',
                #     'amount': record.get('highestBid', ''),
                # },

                'series': record.get('series', ''),
                'material': record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'listed_price_history': record.get('listed_price_history', ''),
                'us_shoe_size_mens': us_shoe_size_mens,
            }
            return transformed_record
        else:
            return None

    def remove_string(self, x): ###remove second occurence of the mentioned string
        self.string_count += 1 
        if self.string_count > 1: 
            return '' 
        return x.group() 

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
