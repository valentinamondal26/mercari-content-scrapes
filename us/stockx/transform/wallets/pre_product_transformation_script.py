import re
import hashlib

class Mapper(object):
    def multireplace(self,string, replacements, ignore_case=True):
        if ignore_case:
            def normalize_key(s):
                return s.lower()

            re_mode = re.IGNORECASE

        else:
            def normalize_key(s):
                return s

            re_mode = 0

        replacements = {normalize_key(key): val for key, val in replacements.items()}
                
        rep_sorted = sorted(replacements, key=len, reverse=True)
        rep_escaped = map(re.escape, rep_sorted)
        
        pattern = re.compile("|".join(rep_escaped), re_mode)
        
        # For each match, look up the new string in the replacements, being the key the normalized old string
        return pattern.sub(lambda match: replacements[normalize_key(match.group(0))], string)


    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(str(v)) > 0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            description = record.get('description', '')
            condition = record.get('condition', '')
            ner_query = description + ' ' + condition
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            
            brand = record.get('brand', '')
            
            model=record.get('title','')
            model=model.replace(brand,'',1)
            # if brand:
            #     model = re.sub(re.compile(r'\b{0}'.format(brand)), '', model)

            year_pattern = re.compile(r'(19|20)\d{2}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(re.compile(r'\(\W*\)'), '', model).strip()

            color = record.get('color', '')

            colors = re.split(r'/|-| ', color)
            for col in colors:
                color_pattern = r'\s+' + '{color}'.format(color=col) + r'(/){0,1}'
                
            model = re.sub(re.compile(color, re.IGNORECASE), '', model)
            model = model.replace(r'\"\s*\"', '')
            model = re.sub(r"^[-]|[-]$|\s+-\s+", '', model.strip())
            
            model = re.sub(re.compile(r'Wallet|Wallets|Quilted|\(\s*with\s*removable\s*insert\s*\)', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'{0}'.format(record['product_line']), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'{0}'.format(record['material']), re.IGNORECASE), '', model)
            
            
            color_words=color.split('/')
            for word in color_words:
                model=model.replace(word,'')
            
            if len(record.get("metal_color", '')) >0: 
                model = re.sub(record.get("metal_color", ''),record.get("metal_color", '')+' metal',model)

            material = self.multireplace(record.get("material", ''),{'Patent Leather': 'Leather','Aged Calfskin' : 'Calfskin'} )
            model = re.sub(' +', ' ', model).strip()
            
            model=re.sub(re.compile(r"\bOn Chain\b",re.IGNORECASE),"",model)

            model=' '.join(model.split())

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawl_date'],
                "status_code": record['status_code'],
                "category": record['category'],
                "brand": brand,
                "image": record['image'],
                # "description": description,
                "release_date":record.get("release_date",''),
                "title": record['title'],
                "model": model,
                "product_line":record.get("product_line", ''),
                "condition": condition,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","")
                },
                "est_retail":{
                     "currency_code": "USD",
                    "amount": record.get("est_retail","")
                },
                "style": record['style'],
                "size": record.get("size", ''),
                "height": record.get("height", ''),
                "width": record.get("width", ''),
                "depth": record.get("depth", ''),
                "material": material,
                "metal_color":record.get("metal_color", ''),
                "color": color,
                "breadcrumb": record['breadcrumb'],
                "price_history": record['price_history'],
                }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None


