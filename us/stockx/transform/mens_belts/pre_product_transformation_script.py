import re
import hashlib
from colour import Color


class Mapper(object):
    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(str(v)) > 0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            description = record.get('description', '')
            condition = record.get('condition', '')
            ner_query = description + ' ' + condition
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            
            brand = record.get('brand', '')
            
            model=record.get('title','')
            if brand:
                model = re.sub(re.compile(r'{0} '.format(brand), re.IGNORECASE), '', model)

            year_pattern = re.compile(r'(19|20)\d{2}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(re.compile(r'\(\W*\)'), '', model).strip()

            color = record.get('color', '')

            # colors = re.split(r'/|-| ', color)
            # for col in colors:
            #     color_pattern = r'\s+' + '{color}'.format(color=col) + r'(/){0,1}'
            #     #model = re.sub(re.compile(color_pattern, re.IGNORECASE), '', model)


            

            model = model.replace(r'\"\s*\"', '')
            model = re.sub(r"^[-]|[-]$|\s+-\s+", '', model.strip())
            model = re.sub(' +', ' ', model).strip()

            model = re.sub(re.compile(r'Belt|belt|Belted|Width', re.IGNORECASE), '', model)
            
            pattern = r"\d+\.*\d*\"[w|W]{1}|\d+[w|W]{1}|\.*\d"
            model = re.sub(re.compile(pattern, re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\b{0}'.format(record.get("width", '')), re.IGNORECASE), '', model).strip()
           

            
            
            if color=='':
                color=[]
                description_words=re.findall(r"\w+",record.get("description",'')+" "+model)
                _color = [i for i in description_words if self.check_color(i)]
                if _color!=[]:
                    for cl in _color:
                        if cl!='':
                            color.append(cl)
                            model = re.sub(re.compile(r'\b{0}\b'.format(cl), re.IGNORECASE), '', model)
                
                color='/'.join(color)

            model = re.sub(re.compile(r'\b{0}\b'.format(color), re.IGNORECASE), '', model)
            

            color=re.sub(re.compile(r"Black Gray|Black Grey|Black/Grey|Black/Gray|Grey/Black",re.IGNORECASE),"Black/Gray",color)
            material=record.get("material", '')
            pattern=re.compile(r"\bTaurillon\b",re.IGNORECASE)
            if re.findall(pattern,material)!=[] and re.findall(re.compile(r"leather",re.IGNORECASE),material)==[]:
                material=re.sub(pattern,"Taurillon Leather",material)
            pattern=re.compile(r"\bcalfskin\b",re.IGNORECASE)
            if re.findall(pattern,material)!=[] and re.findall(re.compile(r"leather",re.IGNORECASE),material)==[]:
                material=re.sub(pattern,"Calfskin Leather",material)
            
            material=re.sub(re.compile(r"leather",re.IGNORECASE),"Leather",material)
            material=re.sub(re.compile(r"\bCalf\b",re.IGNORECASE),"Calfskin",material)

            if material=='':
                material=[]
                material_meta=['Coated Canvas','leather','canvas','calfskin','lambskin']
                for word in material_meta:
                    if re.findall(re.compile(r"\b{}\b".format(word),re.IGNORECASE),description+" "+model)!=[]:
                        material.append(word)
                        model=re.sub(re.compile(r"\b{}\b".format(word),re.IGNORECASE),'',model)
                material='/'.join(material)

            model=re.sub(re.compile(r"\b{}\b".format(material),re.IGNORECASE),'',model)

            color_words=color.split("/")
            color_words=[cl.title() for cl in color_words]
            color='/'.join(list(set(color_words)))

            material_words=material.split("/")
            material_words=[cl.title() for cl in material_words]
            material='/'.join(list(set(material_words)))
            material=re.sub(re.compile(r"\bpvc\b",re.IGNORECASE),"PVC",material)
            model=re.sub(re.compile(r"\bmm\b",re.IGNORECASE),"MM",model)
            materials=material.split("/")

            if "Canvas" in materials and "Coated Canvas" in materials:
                del materials[materials.index("Canvas")]
                material="/".join(materials)



            if re.findall(re.compile(r"\bx\s*supreme",re.IGNORECASE),model)!=[]:
                model=re.sub(re.compile(r"\bx\s*supreme",re.IGNORECASE),'Supreme',model)
            else:
                model = re.sub(re.compile(r'\b{0}\b'.format(record['product_line']), re.IGNORECASE), '', model)


            if model[0]=="x" or model[0]=="X":
                model=model[1:]


            model=' '.join(model.split())
            
                #model=model[0].upper()+model[2:]


            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawl_date'],
                "status_code": record['status_code'],
                "category": record['category'],
                "brand": brand,
                "image": record['image'],
                "description": description,
                "release_date":record.get("release_date",''),
                "title": record['title'],
                "model": model,
                "product_line":record.get("product_line", ''),
                "condition": condition,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","")
                },
                "est_retail":{
                     "currency_code": "USD",
                    "amount": record.get("est_retail","")
                },
                "style": record['style'],
                "size": record.get("size", ''),
                "height": record.get("height", ''),
                "width": record.get("width", ''),
                "depth": record.get("depth", ''),
                "material": material,
                "metal_color":record.get("metal_color", ''),
                "color": color,
                "breadcrumb": record['breadcrumb'],
                "price_history": record['price_history'],
                }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None


    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

        