import re
import hashlib

class Mapper(object):
    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(str(v)) > 0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            description = record.get('description', '')
            condition = record.get('condition', '')
            ner_query = description + ' ' + condition
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            
            brand = record.get('brand', '')
            brand = re.sub(re.compile(r'Air Jordan', re.IGNORECASE), 'Jordan', brand)
            
            model=record.get('title','')
            if brand:
                model = re.sub(re.compile(r'{0}'.format(brand), re.IGNORECASE), '', model)

            year_pattern = re.compile(r'(19|20)\d{2}')
            model = re.sub(year_pattern, '', model)
            model = re.sub(re.compile(r'\(\W*\)'), '', model).strip()

            color = record.get('color', '')

            colors = re.split(r'/|-| ', color)
            for col in colors:
                color_pattern = r'\s+' + '{color}'.format(color=col) + r'(/){0,1}'
                model = re.sub(re.compile(color_pattern, re.IGNORECASE), '', model)

            model = model.replace(r'\"\s*\"', '')
            model = re.sub(r"^[-]|[-]$|\s+-\s+", '', model.strip())
            model = re.sub(' +', ' ', model).strip()

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawl_date'],
                "status_code": record['status_code'],
                "category": record['category'],
                "brand": brand,
                "image": record['image'],
                "description": description,
                "release_date":record.get("release_date",''),
                "title": record['title'],
                "model":model,
                "condition": condition,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","")
                },
                "est_retail":{
                     "currency_code": "USD",
                    "amount": record.get("est_retail","")
                },
                "style" : record['style'],
                "color": color,
                "breadcrumb": record['breadcrumb'],
                "listed_price_history": record['listed_price_history'],
                "price_history" : record['price_history']
                }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None





