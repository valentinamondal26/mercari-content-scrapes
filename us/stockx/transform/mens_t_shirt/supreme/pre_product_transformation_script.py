import hashlib
import re
import html
from colour import Color


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            description = record.get('description', '')
            description = re.sub(re.compile(
                r"\<\/{0,1}\s*\w+\s*\>", re.IGNORECASE), "", description)
            ner_query = description
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')

            # model transformations
            # extract and replace color
            wordList = re.sub("[^\w]", " ",  model).split()
            _color = [i.strip(' ,') for i in wordList
                      if self.check_color(i.strip(' ,'))]
            color = ''
            for color in _color:
                if color:
                    model = model.replace(color, '')
            color = record.get('color', '')
            colo_words = re.findall(r"\w+", color)
            for word in colo_words:
                model = re.sub(re.compile(r"{}".format(
                    word), re.IGNORECASE), "", model)
            # replace brand
            model = re.sub(re.compile(r"{}".format(
                record.get('brand', '')), re.IGNORECASE), "", model)
            # remove (2 Pack) form model
            model = re.sub(re.compile(
                r"\(\s*\d+\s*pack\s*\)", re.IGNORECASE), "", model)
            model = model.replace('//', '').replace('"', '')
            model = model.replace("Ftp", "FTP")
            model = ' '.join(model.split())
            # remove x if model starts with x
            if model[0] == "x" and model[1] == " ":
                model = model[1:]
                model = ' '.join(model.split())
            # Model title case with retaining exsisting captiliazation inside the
            # words
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = model.replace("&", "and")
            # remove "L/S" from model
            model = re.sub(re.compile(r"\bL/S\b", re.IGNORECASE), "", model)

            model = " ".join(model.split())

            exclude_pattern = re.compile(r"fuck", re.IGNORECASE)
            if re.findall(exclude_pattern, model) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": "Men's T-Shirts",
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "image": record.get("image", ''),
                "breadcrumb": record.get("breadcrumb", ""),
                "model": model,
                "description": description,
                "condition": record.get('condition', ''),
                "price": {
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$", "")
                },
                "price_history": record.get('price_history', []),
                "msrp": {
                    "currency_code": "USD",
                    "amount": str(record.get('est_retail')).
                    replace("$", "")
                },
                "gender": "men",
                "product_line": record.get("season", ''),
                "color": color,
                "release_date": record.get('release_date', ''),
                "size_mens": record.get('listed_price_history', [])[0].
                get('size', '')


            }

            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
