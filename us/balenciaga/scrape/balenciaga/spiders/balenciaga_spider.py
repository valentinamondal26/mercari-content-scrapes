'''
https://mercari.atlassian.net/browse/USCC-77 - Balenciaga Fashion sneakers
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import json
import urllib
import requests
from parsel import Selector


class BalenciagaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from balenciaga.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Fashion sneakers
    
    brand : str
        brand to be crawled, Supports
        1) Balenciaga

    Command e.g:
    scrapy crawl balenciaga -a category='Fashion sneakers' -a brand='Balenciaga'
    """

    name = 'balenciaga'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'balenciaga.com'
    blob_name = 'balenciaga.txt'

    url_dict = {
        'Fashion sneakers': {
            'Balenciaga': {
                'url': 'https://www.balenciaga.com/us/men/shoes',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BalenciagaSpider,self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.balenciaga.com/us/looks-shoes_cod11764488rc.html#/us/men/shoes
        @meta {"use_proxy":"True"}
        @scrape_values title material product_details sku
        """

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath(u'normalize-space(//h1[@class="modelName inner"]/text())').extract_first(default=''),
            'material': response.xpath('//div[@class="item-composition-value"]//span[@class="value"]/text()').extract_first(default=''),
            'product_details': response.xpath('//span[@class="item-description-text"]/text()').extract_first(default=''),
            'sku': response.xpath('//span[@class="item-mfc-value"]/text()').extract_first(default='').replace('Product ID: ', '').strip(),
        }

        item.update(response.meta.get('item_info', {}))

        yield item


    def parse_listing_page(self, response):
        """
        @url https://www.balenciaga.com/Search/RenderProductsDepartmentAsync?ytosQuery=true&linkdepartment=&linkdepartmentId=&department=mnccshs_micro&departmentId=&gender=U&brand=&macro=&micro=&season=A%2CP%2CE&color=&size=&site=&section=&sortRule=&yurirulename=searchwithdepartmentgallery&microcolor=&agerange=adult&macroMarchio=&page=1&productsPerPage=24&modelnames=&look=&washtype=&fabric=&prints=&suggestion=false&suggestionValue=&material=&occasion=&weight=&gallery=&colortype=&style=&issale=&heeltype=&wedge=&salesline=&family=&environment=&structure=&authorlocalized=&themecollection=&waist=&stone=&filter=&collection=&price=&facetsvalue=%5B%5D&model=&virtualnavigation=&textSearchFilters=&textSearch=&minMaxPrice=&searchType=&fabricColor=&modelFabric=&itembinding=&totalPages=1&rsiUsed=false&totalItems=14&partialLoadedItems=14&itemsToLoadOnNextPage=0&dept=mnccshs_micro
        @returns requests 1
        @meta {"use_proxy":"True"}
        @scrape_values meta.item_info.price meta.item_info.sizes meta.item_info.colors
        """
        for item in response.xpath('//li[@data-listitem-index]'):
            id = item.xpath('.//a[@class="item-link"]/@href').extract_first(default='')
            if id:
                price = item.xpath('//span[@class="price"]/span[@class="value"]/text()').extract_first(default='')
                code = item.xpath('//div[@class="mainImage"]/@data-ytos-scope').extract_first(default='')
                size_color_url = 'https://www.balenciaga.com/Item/RenderSizesAndColors?code10={code}&siteCode=BALENCIAGA_US'.format(code=code)

                sizes = []
                colors = []
                proxy = random.choice(self.proxy_pool)
                proxies = { "http"  : proxy, "https" : proxy}
                try:
                    r=requests.get(size_color_url, proxies=proxies)
                    sel = Selector(r.text)

                    sizes = sel.xpath('//div[@data-ytos-ctrl="item.selectSize"]//li//span/text()').extract()
                    colors = sel.xpath(f'//div[@data-ytos-ctrl="item.selectColor"]//li//img[@data-ytos-code10="{code}"]/following-sibling::div[@class="description"]/text()').extract()
                except Exception as e:
                    print('Error:{}, while fetching sizes and colors for {}'.format(e, id))

                request = self.create_request(url=id, callback=self.crawlDetail)
                request.meta['item_info'] = {
                    'price': price,
                    'sizes': sizes,
                    'colors': colors,
                }
                yield request


    def parse(self, response):
        """
        @url https://www.balenciaga.com/us/men/shoes
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        script=response.xpath('//script[contains(text(), "yTos.search =")]/text()').extract_first(default='')
        match=re.findall(r'yTos.search =(.*);', script)
        if match:
            j=json.loads(match[0])
            j['dept']=j['department'] 
            total_items = j['totalItems']
            total_pages = j['totalPages']
            products_per_page = j['productsPerPage']
            # current_page = j['page']
            # itemsToLoadOnNextPage = j['itemsToLoadOnNextPage']
            for i in range(1, int(total_pages)+1, 1):
                j['page'] = str(i)
                params=urllib.parse.urlencode(j)
                url='https://www.balenciaga.com/Search/RenderProductsDepartmentAsync?' + params
    
                yield self.create_request(url=url, callback=self.parse_listing_page)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

