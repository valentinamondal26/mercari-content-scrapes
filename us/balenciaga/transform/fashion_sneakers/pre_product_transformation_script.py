import hashlib
import re
from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:
            if re.findall(re.compile(r"Sandals*|Slippers*|Loafers*|Slides*|Boots*|Booties*|Derby|Derbies", re.IGNORECASE), record.get("title", "")) != []:
                return None
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(' +', ' ', model).strip()
            model = re.sub(re.compile(r'\bSneaker\b',re.IGNORECASE),'Sneakers',model)
            if model == 'Track.2':
                model = 'Track.2 Sneakers'
            if 'Trainers' in model:
                model = re.sub(re.compile(r'\bTrainers\b',re.IGNORECASE),'',model)
                model = model + ' Trainers'
            
            if 'Sneakers' in model:
                model = re.sub(re.compile(r'\bSneakers\b',re.IGNORECASE),'',model)
                model = model + ' Sneakers'
            
            if model == 'Speed':
                model = model + ' Sneakers'
            
            model = re.sub(r'\s+',' ',model).strip()
            
            new_title = []
            for ti in model.split():
                if  ti.isupper():
                    new_title.append(ti)
                else:
                    new_title.append(titlecase(ti))
            model =  ' '.join(new_title)

            description = record.get('product_details', '')
            description = re.sub(re.compile(r'\r\n|\n'), '. ', description)
            description = description.replace('•', '')
            description = re.sub(' +', ' ', description).strip()

            material = re.sub(re.compile(
                r'\d+\s*%', re.IGNORECASE), '', record.get('material', ''))
            material = re.sub(' +', ' ', material).strip()

            color = ','.join(record.get('colors', '')).title()
            color = [c.strip() for c in color.split("/")]
            color = sorted(set(color), key=color.index)
            color = "/".join(color)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": description,
                "model": model,
                "color": color,
                "material": material,
                'french_shoe_size': ','.join(record.get('sizes', '')),
                'model_sku': record.get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
