import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title','')
            color = record.get('color','')
            if 'Outdoor Set' in title or 'In-Wall Set' in title or 'In-Ceiling Set' in title:
                color = 'Black/White'
            elif 'Outdoor Speaker (Pair)' in title or 'In-Wall Speaker (Pair)' in title or 'In-Ceiling Speaker (Pair)' in title or 'Boost' in title:
                color = 'White'
            elif 'Amp' in title or 'Port' in title:
                color = 'Black'
            else:
                color = [col.split('-')[-1] for col in color.split(', ')]
                color = ', '.join(color)
            color = color.replace(', ','/').title()

            ###the below function is for finding the required entity in specs dict.for e.g they mentioned to find Memory_ram_capacity in power & networkin.sometime
            ###it can be found in some other  key in the dict also, for finding that statement which contains the memory or any spec we created the function.

            # def find_sentence(check_word):
            #     specs_key = list(record.get('specs').keys())
            #     for key in specs_key:
            #         data = record.get('specs',{}).get(key,[])
            #         data = ', '.join(data).replace("\n",'')
            #         if  check_word in data:
            #             return data
            #     return ''

            power_data = record['specs'].get('Power & Networking',[])
            power_data = " ".join(power_data).replace("\n", "")

            # if  'CPU' not in power_data :
            #     power_data = find_sentence('CPU')

            cpus = re.findall(re.compile(r"CPU\s*.*\d+\.\d*\s*GHz\s|CPU\s*.*\d+\.\d*\s*GHz$", re.IGNORECASE), power_data)
            cpus = ",".join([cpu.strip("CPU ") for cpu in cpus])

            # if 'Memory' not in power_data:
            #     power_data =find_sentence('Memory')

            try:
                memory = re.findall(r'Memory.*Flash|Memory.*NV|Memory.*RAM',power_data)[0]
                memory = re.sub(r'^Memory,|Memory','',memory).strip()
            except:
                memory = ''

            dimensions_data = record['specs'].get('Dimensions',[])
            dimensions_data = " ".join(dimensions_data).replace("\n", "")

            # if 'Dimensions' not in dimensions_data:
            #     dimensions_data = find_sentence('Dimensions')

            try:
                weight = re.findall(re.compile(r'Weight(.*kg\))',re.IGNORECASE),dimensions_data)[0].replace('\r','').strip()
                weight = re.sub(r'^,','',weight).strip()
            except IndexError:
                weight = ''

            height = ''
            width = ''
            depth = ''

            if dimensions_data:
                try:
                    dim_unit = re.findall(re.compile(r"Dimensions\s*\-\s\w\sx\s\w\sx\s\w", re.IGNORECASE), dimensions_data)[0]
                    dim_unit = dim_unit.replace("Dimensions - ", "").strip()
                    dim_value = re.findall(re.compile(r"\d+\.*\d*\sx\s\d+\.*\d*\sx\s\d+\.*\d*\s*in|inches|inch", re.IGNORECASE), dimensions_data)[0]
                    height = dim_value.split(" x ")[dim_unit.split(" x ").index('H')]
                    width = dim_value.split(" x ")[dim_unit.split(" x ").index('W')]
                    depth = dim_value.split(" x ")[dim_unit.split(" x ").index('D')]
                except IndexError:
                    pass

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,

                'title': record.get('title',''),
                'model': record.get('title',''),

                'brand': record.get('brand',''),
                'category': record.get('category',''),
                'image': record.get('image',''),
                'description': record.get('description',''),

                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statusCode',''),


                'color': color,
                'height': height.strip(),
                'depth': depth.replace('in','').strip(),
                'width': width.strip(),
                'weight': weight.strip(),
                'cpu_brand': cpus.strip(),
                'memory_ram_capacity': memory,

                'msrp':{
                    'currency_code':'USD',
                    'amount':record.get('price','').replace('$','')
                },
            }
            if 'gift card' in title.lower():
                return None
            else:
                return transformed_record
        else:
            return None
