'''
https://mercari.atlassian.net/browse/USDE-1675 - Sonos Home Speakers & Subwoofers
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re

class SonosSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from sonos.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Home Speakers & Subwoofers

    brand: str
        1) Sonos

        Command e.g:
    scrapy crawl sonos -a category='Home Speakers & Subwoofers' -a brand='Sonos'
    """

    name = 'sonos'

    category = None
    brand = None

    merge_key = 'id'

    source = 'sonos.com'
    blob_name = 'sonos.txt'

    base_url = 'https://www.sonos.com'

    url_dict = {
        'Home Speakers & Subwoofers': {
            'Sonos': {
                'url': 'https://www.sonos.com/en-us/shop',
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            return
        super(SonosSpider,self).__init__(*args, **kwargs)

        if not category and not brand:
            self.logger.error("Category should not be None")
            raise CloseSpider('category  not found')

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        self.category = category
        self.brand = brand
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{}'.format(category))
            raise CloseSpider('launch url  not found')

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_listing_page)


    def parse_listing_page(self, response):
        """
        @url https://www.sonos.com/en-us/shop
        @returns requests 1
        @meta {"use_proxy": "True"}
        @scrape_values meta.title meta.description meta.color meta.image
        """
        products_category = response.xpath("//div[@class='main']/div[@class='cards beside-n-e ph-x2 lg-ph-x1']/@id").getall()
        for categ in products_category:
            products_data = response.xpath("//div[@id='"+categ+"']/ul/li/a/div[@class='max-width-full width-full lg-width-inherit']")
            products_url = response.xpath("//div[@id='"+categ+"']/ul/li/a")

            for item_data,item_url  in zip(products_data,products_url):
                color = ', '.join(item_data.xpath(".//div[@class='flex-row lg-flex-column']/div/form/fieldset/input/@id").getall())
                meta = {
                    'title': item_data.xpath(".//h2[@id='beside-n-e-headline']/text()").get(default='').strip(),
                    'description': item_data.xpath(".//p[@class='bodycopy width-full lg-width-x9 pr-x3 lg-pr-x1 lg-pl-x1 mb-x1 lg-ml-auto text-x2']/text()").get(default='').strip(),
                    'color': color,
                    'image': item_data.xpath(".//div/img/@data-src").get(default='')
                }
                url = self.base_url+item_url.xpath("./@href").get()
                yield self.create_request(url=url,callback=self.crawldetails, meta=meta)


    def crawldetails(self, response):
        """
        @url https://www.sonos.com/en-us/shop/arc.html
        @meta {"use_proxy": "True"}
        @scrape_values specs.Dimensions specs.Power\s&\sNetworking specs.Contents
        """
        meta = response.meta
        specs = {}
        spec_names = response.xpath("//button[contains(@data-section-title,'features-and-specs')]/following-sibling::div/div/ul/li/a/text()").getall()
        specs_ids = response.xpath("//div[@class='tabbed-section-container']/section/@id").getall() ##this is beacause id is generated based on the title of the item.
        for spec_name,spec_id in zip(spec_names,specs_ids):
            data = response.xpath("//section[@id='"+spec_id+"']/div/section/div/div[@class='mt-x0p75 lg-mt-x0p25 text-x2 lg-text-x1 body']/text()").getall()
            specs.update({spec_name.strip():data})

        item = {
            'id': response.url,
            'brand': self.brand,
            'category': self.category,
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'title': meta.get('title',''),
            'image': meta.get('image',''),
            'description': meta.get('description'),
            'color': meta.get('color',''),
            'specs': specs
        }
        price_url_id = re.findall(r'shop\/(.*).html',response.url)[0]
        price_url = 'https://www.sonos.com/on/demandware.store/Sites-Sonos_US-Site/en_US/Product-GetPricingByView?pid={}&view=newedition&standardFirst=false&styleassetid=priceStyleNewEditionPDP&showBadge=true'.format(price_url_id)
        meta.update({'item':item})
        yield self.create_request(url=price_url, callback=self.get_price, meta=meta)


    def get_price(self, response):
        meta = response.meta
        item = meta.get('item', {})
        price = response.xpath("//div/span/text()").get(default='').strip()
        item.update({'price':price})
        yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
