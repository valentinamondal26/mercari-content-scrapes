import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = ' '.join(record.get('description', record.get('title', '')))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace('From', '').replace("$", "").strip()

            description = ' '.join(record.get('description', ''))

            max_weight_capacity = ''
            match = re.findall(re.compile(r'up to a load of (\d+ kg/ \d+ lbs)', re.IGNORECASE), description)
            if match:
                max_weight_capacity = match[0]

            color = record.get('color', '')
            color = re.sub(re.compile(r'\bclassic collection\b', re.IGNORECASE), '', color)
            color = re.sub(r'\s+', ' ', color).strip()
            color = color.title()

            filter_color = record.get('Color', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'\b'+color+r'\b'r'|\b'+filter_color+r'\b|®', re.IGNORECASE), '', model)

            product_type = record.get('Product type', '')

            if not product_type:
                product_type = record.get('breadcrumb', '').rsplit('|', 1)[-1]

            d = {
                "Raincovers": "Rain Cover",
                "Adapters": "Adapter",
                "Bassinets": "Bassinet",
                "Babycocoons": "Baby Cocoon",
                "seat fabrics": "Seat Fabric",
                "seatliners": "Seat Liner",
                "holderstrays": "Holder/Tray",
                "Covers": "Cover",
                "Bags": "Bag",
                "Sun Canopies": "Sun Canopy",
                "mosquitonets": "Mosquito Net",
                "Parasols": "Parasol",
                "Style Sets": "Style Set",
                "Wheeledboards": "Wheeled Board",
                "Seatfabrics": "Seat Fabric",
                "Mattresscovers": "Mattress cover",
            }
            for k, v in d.items():
                model = re.sub(re.compile(r'\b'+k+r'\b', re.IGNORECASE), v, model)
                product_type = re.sub(re.compile(r'\b'+k+r'\b', re.IGNORECASE), v, product_type)

            model = re.sub(re.compile(r'\b'+product_type+r'\b|\bfootmuff\b|\bholder\b|\btray\b|\bMattress\b', re.IGNORECASE), '', model)
            model = model.title()
            model = re.sub(re.compile(r"Bugaboo Fox Bugaboo Fox", re.IGNORECASE), "Bugaboo Fox", model)
            model = re.sub(r'\s+', ' ', model).strip()

            product_type = product_type.title()
            product_type = re.sub(r'\s+', ' ', product_type).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                "product_type": product_type,
                "color": color,
                "compatibility": record.get('Compatibility', ''),
                "collection": record.get('Collection', ''),
                "max_weight_capacity": max_weight_capacity,
                "material": record.get('', ''),
                "description": description,
            }

            return transformed_record
        else:
            return None
