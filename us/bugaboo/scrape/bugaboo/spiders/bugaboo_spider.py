'''
https://mercari.atlassian.net/browse/USCC-302 - Bugaboo Stroller's Accessories
'''

import scrapy
import os
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider

import re


class BugabooSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bugaboo.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stroller's Accessories

    brand : str
        brand to be crawled, Supports
        1) Bugaboo

    Command e.g:
    scrapy crawl bugaboo -a category="Stroller's Accessories" -a brand='Bugaboo'
    """

    name = 'bugaboo'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'bugaboo.com'
    blob_name = 'bugaboo.txt'

    url_dict = {
        "Stroller's Accessories": {
            'Bugaboo': {
                'url': 'https://www.bugaboo.com/us-en/accessories/',
                'filters': ['Product type', 'Collection', 'Color', 'Compatibility']
            }
        }
    }

    base_url = 'https://www.bugaboo.com'

    filter_list = []

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BugabooSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filter_list = ['Product type', 'Collection', 'Color', 'Compatibility']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_filter, dont_filter=True)
            yield self.create_request(url=self.launch_url, callback=self.parse, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://www.bugaboo.com/us-en/accessories/wheeled-boards/bugaboo-comfort-wheeled-board-85600WB01.html
        @scrape_values breadcrumb title price color description
        @meta {"use_proxy":"True"}
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': '|'.join(list(map(lambda x: x.strip(), response.xpath('//span[@class="breadcrumb__value"]/text()[normalize-space(.)]').extract()))),
            'title': response.xpath(u'normalize-space(//h1[@class="pdp-header__title"]/text())').extract_first(default=''),
            'price': response.xpath(u'normalize-space(//div[@class="pdp-header__price"]/text())').extract_first(default=''),
            'color': response.xpath(u'normalize-space(//div[@class="product-attribute__name"]/span/text())').extract_first(default=''),
            'description': response.xpath('//div[@class="o-container c-product-detail"]//div[@class="s-rich-text"]//text()[normalize-space(.)]').extract(),
        }


    def parse_filter(self, response):
        """
        @url https://www.bugaboo.com/us-en/accessories/
        @returns requests 1
        """

        for _filter in response.xpath('//div[@class="c-refinements refinements"]/div[@class="c-refinement-accordion c-accordion"]'):
            filter_name = _filter.xpath('.//button/span/text()').extract_first()
            if filter_name in self.filter_list:
                for value in _filter.xpath('.//li[@class="refinement-accordion__list-item"]'):
                    filter_value = re.sub(re.compile(r'\(\d+\)'), '', value.xpath(u'normalize-space(./a/span/span/text())').extract_first(default='')).strip()
                    url = value.xpath('./a/@href').extract_first(default='')
                    if url:
                        request = self.create_request(url='https://www.bugaboo.com'+url, callback=self.parse)
                        request.meta['extra_item_info'] = {
                                filter_name: filter_value,
                            }
                        yield request


    def parse(self, response):
        """
        @url  https://www.bugaboo.com/us-en/accessories/adapters/
        @returns requests 13
        @meta {"use_proxy":"True"}
        """
        ###Number of request mentioned in contracts is 13 beacause 12 items per page and 1 Next Page url

        extra_item_info = response.meta.get('extra_item_info', {})

        # total_count = re.findall(r'\d+', response.xpath('//div[contains(@class, "search-results__count-container")]/span/text()').extract_first())[0]

        for product in response.xpath('//div[@class="c-product"]'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                id = self.base_url + id
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.extra_item_infos.get(id).update(extra_item_info)

                request = self.create_request(url=id, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('//a[@class="c-button--primary search-results__next-button"]/@href').extract_first()
        if nextLink:
            request = self.create_request(url=nextLink, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

