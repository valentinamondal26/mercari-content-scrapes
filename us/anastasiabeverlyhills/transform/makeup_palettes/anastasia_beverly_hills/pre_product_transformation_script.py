import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title')
            
            pattern=re.compile(r"Makeup|palettes*|®",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            for color in record.get('Color','').split(', '):
                pattern=re.compile("{}".format(color),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            pattern=re.compile(r"\bvol\.*",re.IGNORECASE)
            model=re.sub(pattern,'Volume',model)
            model=' '.join(model.split())

        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date',''),
                "status_code": record.get('status_code',''),
                "category": "Makeup palettes",
                "breadcrumb":record.get('breadcrumb',''),
                "description": description.replace('\n',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query.replace('\n',''),
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "model_sku" : record.get('model_sku',''),
                "color":record.get('Color',''),
                "finish":record.get('Finish','')

                }

            return transformed_record
        else:
            return None
