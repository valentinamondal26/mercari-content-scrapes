'''
https://mercari.atlassian.net/browse/USCC-305 - Anastasia Beverly Hills Makeup palettes
'''

import scrapy
import datetime
import random
import os
from scrapy.exceptions import CloseSpider


class AnastasiabeverlyhillSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from anastasiabeverlyhills.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Makeup palettes

    brand : str
        category to be crawled, Supports
        1) Anastasia Beverly Hills

    Command e.g:
    scrapy crawl anastasiabeverlyhills -a category="Makeup palettes" -a brand='Anastasia Beverly Hills'

    """

    name = 'anastasiabeverlyhills'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None
    number_of_products = None
    source = 'anastasiabeverlyhills.com'
    blob_name = 'anastasiabeverlyhills.txt'
    base_url = 'https://anastasiabeverlyhills.com'

    url_dict = {
        "Makeup palettes": {
            'Anastasia Beverly Hills': {
                "url": 'https://www.anastasiabeverlyhills.com/products/eye-makeup/palettes-and-sets/',
                'filters': ['Color','Finish']
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AnastasiabeverlyhillSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters=self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category}, url: {url}".format(category = category,url = self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Color','Finish']
        self.base_url = 'https://anastasiabeverlyhills.com'
        self.number_of_products = 1 #dummy value just for contracts purpose alone to run the else part in parse callback function


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self,response):
        """
        @url https://www.anastasiabeverlyhills.com/products/eye-makeup/palettes-and-sets/
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        if self.filters:
            for filter in self.filters:
                filter_urls=response.xpath('//div[@class="refinements"]//div//a[contains(text(),"{}")]/following-sibling::ul//li/@data-refine-href'.format(filter)).getall()
                filter_urls=[self.base_url+link if "https" not in link else link for link in filter_urls ]
                filter_values=response.xpath('//div[@class="refinements"]//div//a[contains(text(),"{}")]/following-sibling::ul//li/label/text()'.format(filter)).getall()
                filter_values=[val.strip() for val in filter_values]
                for url in filter_urls:
                    meta={}
                    index=filter_urls.index(url)
                    meta=meta.fromkeys([filter],filter_values[index])
                    meta.update({'filter_name':filter})
                    yield self.createRequest(url=url,callback=self.parse,meta=meta)

        yield self.createRequest(url=response.url,callback=self.parse)


    def parse(self, response):
        """
        @url https://www.anastasiabeverlyhills.com/products/eye-makeup/palettes-and-sets/?prefn1=refinementColor&prefv1=black
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        if not self.number_of_products:
            self.number_of_products=response.xpath('//div[@class="count"]/text()').get().strip(' items')
            if not self.filters:
                yield self.createRequest(url='https://www.anastasiabeverlyhills.com/products/eye-makeup/palettes-and-sets/?sz={}&start=0&format=page-element&'.format(self.number_of_products),callback=self.parse,meta=response.meta)
            else:
                url=response.url+"&sz={}&start=0&format=page-element"
                yield self.createRequest(url=url.format(self.number_of_products),callback=self.parse,meta=response.meta)
        else:
            all_links=response.xpath('//div[@class="product-name"]/a/@href').getall()
            all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
            for link in all_links:
                yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta,headers={'Referer':link})


    def parse_product(self,response):
        """
        @url https://www.anastasiabeverlyhills.com/alyssa-edwards-palette/ABH01-18183.html?cgid=eyes-palettes-and-sets
        @meta {"use_proxy":"True"}
        @scrape_values image title price description model_sku
        """

        item={}
        item["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"]=str(response.status)
        item['id']=response.url
        item['image']=response.xpath('//div[@class="item active"]/img/@src').get(default='')
        item['brand']=self.brand
        item['category']=self.category
        breadcrumb=response.xpath('//div[@class="breadcrumb"]//a/text()').getall()
        breadcrumb=[br.strip() for br in breadcrumb]
        item['breadcrumb']='|'.join(breadcrumb)
        item['image']=response.xpath('//img[@class="primary-image"]/@src').get(default='')
        item['title']=response.xpath('normalize-space(//span[@class="prod-name-wrap"]/text())').get(default='')
        item['price']=response.xpath('//input[@name="price"]/@value').get(default='')
        description=response.xpath('//div[@class="long-description"]//text()').getall()
        for des in description:
            if des=='\n':
                del description[description.index(des)]

        item['description']=", ".join(description)
        item['model_sku']=response.xpath('//span[@itemprop="productID"]/text()').get(default='')
        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)
        yield item


    def createRequest(self, url, callback,meta=None,headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key,value in headers.items():
                request.headers[key]=value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request


