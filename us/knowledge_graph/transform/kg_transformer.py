import datetime
import json

class Mapper(object):
    def map(self, item):

        currDate = datetime.date.today()

        enDict = {'entities': []}
        kg_items = {'items': []}
        kg_attribute_dict = {'attributes': []}
        kg_category_dict = {'categories': []}
        kg_item_category_dict = {'hascategory': []}
        kg_item_entity_dict = {'hasEntity': []}
        kg_item_attribute_dict = {'hasAttributes': []}
        kg_ent_attribute_dict = {'hasAttributes': []}
        categoryEntityDict = {'hasEntity': []}

        catDict = {""}
        eDict = {""}
        entityAttrDict = {'entityAttributes': []}
        entities = item['entities']
        attributeList = []
        cList = []
        cDict = {""}
        for entity in entities:
            if entity['entity'] not in eDict:
                entDict = {'_key': entity['entity'],
                           'entityId': entity['entity'],
                           'entityName': entity['name'],
                           # 'freeText': entity.get('freeText', False),
                           'alias': entity['name'],
                           'created': str(currDate)
                           }
                eDict.add(entity['entity'])
                enDict['entities'].append(entDict)
            if (entity['name'] == 'system_category'):
                sysFlag = True
            else:
                sysFlag = False
            if len(entity['attribute']) > 0 and len(entity['entity']) > 0:
                entityId = entity['entity']
                entityName = entity['name']
                freeText = entity.get('freeText', False)
                for attribute in entity['attribute']:
                    attrDict = {}
                    singleAttr = {}

                    attributeDict = {'attributes': []}

                    attributeName = attribute['name']
                    itemAttr = {}
                    key = str(attribute['id'])
                    attrDict['_key'] = key
                    attrDict['attributeId'] = key
                    attrDict['attributeName'] = attributeName
                    attrDict['alias'] = attributeName
                    attrDict['created'] = str(currDate)
                    attrDict['update'] = ''
                    itemAttr['_key'] = str(item['item_id']) + '-ha-' + str(key)
                    itemAttr['_from'] = 'items/' + str(item['item_id'])
                    itemAttr['_to'] = 'attributes/' + str(key)
                    itemAttr['created'] = str(currDate)
                    kg_item_attribute_dict['hasAttributes'].append(itemAttr)
                    attrEntDict = {
                        '_key': str(entityId) + '-ha-' + str(key),
                        '_from': 'entities/' + str(entityId),
                        '_to': 'attributes/' + str(key),
                        # 'freeText': freeText,
                        'created': str(currDate)
                    }
                    kg_ent_attribute_dict['hasAttributes'].append(attrEntDict)
                    kg_attribute_dict['attributes'].append(attrDict)
                    singleAttr['entityId'] = entityId
                    singleAttr['entityName'] = entityName
                    singleAttr['attributeId'] = key
                    singleAttr['attributeName'] = attributeName
                    attributeList.append(singleAttr)

                    categoryDict = {}
                    if (sysFlag) and key not in cDict:
                        cDict.add(key)
                        cList.append(key)
                        if key not in catDict:
                            categoryDict['_key'] = key
                            categoryDict['categoryId'] = key
                            categoryDict['categoryName'] = attributeName
                            categoryDict['alias'] = attributeName
                            categoryDict['created'] = str(currDate)
                            categoryDict['updated'] = ''
                            kg_category_dict['categories'].append(categoryDict)
                            catDict.add(key)

                    attributeDict['attributes'] = attributeList
                    entityAttrDict['entityAttributes'] = attributeDict['attributes']

                    itemEntDict = {
                        '_key': str(item['item_id']) + '-he-' + str(entityId),
                        '_from': 'items/' + str(item['item_id']),
                        '_to': 'entities/' + str(entityId),
                        'created': str(currDate)
                    }
                    kg_item_entity_dict['hasEntity'].append(itemEntDict)

        p = {'_key': item['item_id'],
             'itemId': item['item_id'],
             'categoryId': cList,
             'entityAttributes': entityAttrDict['entityAttributes'],
             'created': str(currDate),
             'alias': '',
             'updated': '',
             'source': item['source']}

        for c in cList:
            itemCatDict = {'_key': str(item['item_id']) + '-hc-' + str(c),
                                 '_from': 'items/' + item['item_id'],
                                 '_to': 'categories/' + str(c),
                                 'created': str(currDate),
                                 'updated': ''}
            kg_item_category_dict['hascategory'].append(itemCatDict)

            for entity in entities:
                entityId = entity['entity']
                catEntDict = {
                    '_key': str(c) + '-he-' + str(entityId),
                    '_from': 'categories/' + str(c),
                    '_to': 'entities/' + str(entityId),
                    'created': str(currDate),
                    'updated': ''
                }
                categoryEntityDict['hasEntity'].append(catEntDict)

        kg_items['items'].append(p)

        result = {}
        result['items-col'] = self.flatten(kg_items['items'])
        result['attributes-col'] = self.flatten(kg_attribute_dict['attributes'])
        result['entities-col'] = self.flatten(enDict['entities'])
        result['categories-col'] = self.flatten(kg_category_dict['categories'])
        result['category-entities-col'] = self.flatten(categoryEntityDict['hasEntity'])
        result['entity-attributes-col'] = self.flatten(kg_ent_attribute_dict['hasAttributes'])
        result['item-attributes-col'] = self.flatten(kg_item_attribute_dict['hasAttributes'])
        result['item-categories-col'] = self.flatten(kg_item_category_dict['hascategory'])
        result['item-entities-col'] = self.flatten(kg_item_entity_dict['hasEntity'])

        print("Result Item : {}".format(result))

        return result

    #Flattening to output JSON lines instead of array
    def flatten(self, array):
        data = ""
        length = len(array)
        if length > 0:
            for idx, val in enumerate(array):
                data = data + json.dumps(val)
                if idx < (length-1):
                    data += "\n"

        return data
