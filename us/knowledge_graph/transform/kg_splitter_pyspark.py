import json

from pyspark.sql.functions import udf, explode, split


class SplitDataframe(object):

    def process(self, context, config, datadict):
        df = datadict['transformed']

        extract_items_collection_udf = udf(lambda x: self.extract_collection(x, "items-col"))
        items_col_df = df.withColumn("result", extract_items_collection_udf(df.columns[0]))
        items_col_df = items_col_df.select(explode(split(items_col_df.columns[0], "\n")))
        items_col_df = items_col_df.dropDuplicates()
        datadict['items-collection'] = items_col_df

        extract_attributes_collection_udf = udf(lambda x: self.extract_collection(x, "attributes-col"))
        attributes_col_df = df.withColumn("result", extract_attributes_collection_udf(df.columns[0]))
        attributes_col_df = attributes_col_df.select(explode(split(attributes_col_df.columns[0], "\n")))
        attributes_col_df = attributes_col_df.dropDuplicates()
        datadict['attributes-collection'] = attributes_col_df

        extract_entities_collection_udf = udf(lambda x: self.extract_collection(x, "entities-col"))
        entities_col_df = df.withColumn("result", extract_entities_collection_udf(df.columns[0]))
        entities_col_df = entities_col_df.select(explode(split(entities_col_df.columns[0], "\n")))
        entities_col_df = entities_col_df.dropDuplicates()
        datadict['entities-collection'] = entities_col_df

        extract_categories_collection_udf = udf(lambda x: self.extract_collection(x, "categories-col"))
        categories_col_df = df.withColumn("result", extract_categories_collection_udf(df.columns[0]))
        categories_col_df = categories_col_df.select(explode(split(categories_col_df.columns[0], "\n")))
        categories_col_df = categories_col_df.dropDuplicates()
        datadict['categories-collection'] = categories_col_df

        extract_category_entities_collection_udf = udf(lambda x: self.extract_collection(x, "category-entities-col"))
        category_entities_col_df = df.withColumn("result", extract_category_entities_collection_udf(df.columns[0]))
        category_entities_col_df = category_entities_col_df.select(explode(split(category_entities_col_df.columns[0], "\n")))
        category_entities_col_df = category_entities_col_df.dropDuplicates()
        datadict['category-entities-collection'] = category_entities_col_df

        extract_entity_attributes_collection_udf = udf(lambda x: self.extract_collection(x, "entity-attributes-col"))
        entity_attributes_col_df = df.withColumn("result", extract_entity_attributes_collection_udf(df.columns[0]))
        entity_attributes_col_df = entity_attributes_col_df.select(explode(split(entity_attributes_col_df.columns[0], "\n")))
        entity_attributes_col_df = entity_attributes_col_df.dropDuplicates()
        datadict['entity-attributes-collection'] = entity_attributes_col_df

        extract_item_attributes_collection_udf = udf(lambda x: self.extract_collection(x, "item-attributes-col"))
        item_attributes_col_df = df.withColumn("result", extract_item_attributes_collection_udf(df.columns[0]))
        item_attributes_col_df = item_attributes_col_df.select(explode(split(item_attributes_col_df.columns[0], "\n")))
        item_attributes_col_df = item_attributes_col_df.dropDuplicates()
        datadict['item-attributes-collection'] = item_attributes_col_df

        extract_item_categories_collection_udf = udf(lambda x: self.extract_collection(x, "item-categories-col"))
        item_categories_col_df = df.withColumn("result", extract_item_categories_collection_udf(df.columns[0]))
        item_categories_col_df = item_categories_col_df.select(explode(split(item_categories_col_df.columns[0], "\n")))
        item_categories_col_df = item_categories_col_df.dropDuplicates()
        datadict['item-categories-collection'] = item_categories_col_df

        extract_item_entities_collection_udf = udf(lambda x: self.extract_collection(x, "item-entities-col"))
        item_entities_col_df = df.withColumn("result", extract_item_entities_collection_udf(df.columns[0]))
        item_entities_col_df = item_entities_col_df.select(explode(split(item_entities_col_df.columns[0], "\n")))
        item_entities_col_df = item_entities_col_df.dropDuplicates()
        datadict['item-entities-collection'] = item_entities_col_df

        report_item = {
            "items-collections": items_col_df.count(),
            "attributes-collection": attributes_col_df.count(),
            "entities-collection": entities_col_df.count(),
            "categories-collection": categories_col_df.count(),
            "category-entities-collection": category_entities_col_df.count(),
            "entity-attributes-collection": entity_attributes_col_df.count(),
            "item-attributes-collection": item_attributes_col_df.count(),
            "item-categories-collection": item_categories_col_df.count(),
            "item-entities-collection": item_entities_col_df.count()
        }

        context.report_writer.add_line_item("product-collections", report_item)

    def extract_collection(self, record, key):
        if record:
            record = json.loads(record)
            if key in record:
                return record[key]

        return None
