import re

class Mapper(object):

    def map(self, record):
        transformed_record = {}
        if record:
            itemId = record['item_id']
            record['itemId'] = itemId
            del record['item_id']

            ignore_keys = ['entities']
            transformed_record = self.snake_to_camel_case(record, ignore_keys)

            transformed_record['_key'] = itemId
        return transformed_record

    def snake_to_camel_case(self, dictionary, keys_to_ignore):
        transformed = {}
        for key in dictionary:
            transformed_key = re.sub('_.', lambda x: x.group()[1].upper(), key)
            if key in keys_to_ignore:
                transformed[transformed_key] = dictionary[key]
            elif type(dictionary[key]) is dict:
                transformed[transformed_key] = self.snake_to_camel_case(dictionary[key], [])
            elif (type(dictionary[key])) is list:
                transformed_list = list()
                for item in dictionary[key]:
                    if type(item) is dict:
                        transformed_list.append(self.snake_to_camel_case(item, []))
                    else:
                        transformed_list.append(item)
                transformed[transformed_key] = transformed_list
            else:
                transformed[transformed_key] = dictionary[key]

        return transformed

if __name__ == '__main__':
    a = {'x':{'one_one':'1', 'two_two_two':{'hello_hello':2, 'helo_':3}}, 'entities': {'one_one': 1}, 'y_y': ['yyyy_y', {'y_1':3, 'y_2':4, 'y':4}]}
    m = Mapper()
    print(m.snake_to_camel_case(a, ['entities']))
