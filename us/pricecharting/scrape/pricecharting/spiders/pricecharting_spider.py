'''
https://mercari.atlassian.net/browse/USCC-179 - Sony Handheld Consoles
https://mercari.atlassian.net/browse/USCC-465 - Nintendo Video Game Consoles
https://mercari.atlassian.net/browse/USCC-513 - Xbox Video Game Consoles
https://mercari.atlassian.net/browse/USCC-448 - Nintendo Video gaming merchandise
https://mercari.atlassian.net/browse/USCC-515 - Video Game Accessories Xbox
https://mercari.atlassian.net/browse/USCC-518 - Microsoft Video Games 
https://mercari.atlassian.net/browse/USCC-519 - Sony Video Games
https://mercari.atlassian.net/browse/USCC-520 - Nintendo Video Games
https://mercari.atlassian.net/browse/USCC-521 - Sega Video Games
https://mercari.atlassian.net/browse/USCC-522 - Atari Video Games
'''


import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re


class PricechartingSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from pricechart.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Handheld Consoles
        2) Video Game Consoles
        3) Video gaming merchandise
        4) Video Game Accessories

    brand : str
        brand to be crawled, Supports
        1) Sony
        2) Nintendo
        3) Microsoft
        4) Xbox

    Command e.g:
    scrapy crawl pricecharting -a category='Handheld Consoles' -a brand='Sony'
    scrapy crawl pricecharting -a category='Video Game Consoles' -a brand='Nintendo'
    scrapy crawl pricecharting -a category='Video Game Consoles' -a brand='Microsoft'
    scrapy crawl pricecharting -a category='Video gaming merchandise' -a brand='Nintendo'
    scrapy crawl pricecharting -a category='Video Game Accessories' -a brand='Xbox'
    scrapy crawl pricecharting -a category='Video Games' -a brand='Microsoft'
    scrapy crawl pricecharting -a category='Video Games' -a brand='Nintendo'
    scrapy crawl pricecharting -a category='Video Games' -a brand='Sega'
    scrapy crawl pricecharting -a category='Video Games' -a brand='Atari'
    """

    name = "pricecharting"

    source = 'pricecharting.com'
    blob_name = 'pricecharting.txt'

    merge_key = 'id'

    category = None
    brand = None

    base_url = 'https://www.pricecharting.com'
    listing_page_api_end_part = '&cursor={start_index}&format=json'
    item_page_base_url = 'https://www.pricecharting.com/game/{consoleuri}/{producturi}'

    proxy_pool = [
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
    ]

    url_dict = {
        'Handheld Consoles': {
            'Sony': {
                'url': [
                    'https://www.pricecharting.com/console/psp?sort=name&genre-name=systems',
                    'https://www.pricecharting.com/console/playstation-vita?sort=name&genre-name=systems',
                ]
            }
        },
        'Video Game Consoles': {
            'Nintendo': {
                'url': [
                    'https://www.pricecharting.com/console/nes?sort=name&genre-name=systems',
                    'https://www.pricecharting.com/console/super-nintendo?sort=name&genre-name=systems',
                    'https://www.pricecharting.com/console/nintendo-64?sort=name&genre-name=systems',
                    'https://www.pricecharting.com/console/gamecube?sort=name&genre-name=systems',
                    'https://www.pricecharting.com/console/wii?sort=name&genre-name=systems',
                    'https://www.pricecharting.com/console/game-&-watch',
                    'https://www.pricecharting.com/console/virtual-boy?sort=name&genre-name=systems&exclude-variants=false&exclude-hardware=false'
                ],
            },
            'Microsoft': {
                'url': [
                    'https://www.pricecharting.com/console/xbox?sort=name&genre-name=systems',
                ],
                'filters': ['variant']
            }
        },
        'Video gaming merchandise': {
            'Nintendo': {
                'url': [
                    'https://www.pricecharting.com/console/amiibo?sort=name&genre-name=',
                ]
            }
        },
        'Video Game Accessories': {
            'Xbox':{
                'url': ['https://www.pricecharting.com/console/xbox?sort=name&genre-name=accessories', \
                    'https://www.pricecharting.com/console/xbox-360?sort=name&genre-name=accessories', \
                    'https://www.pricecharting.com/console/xbox-one?sort=name&genre-name=accessories']
            }
        },
        'Video Games':{
            'Microsoft': {
                'url': ['https://www.pricecharting.com/console/xbox', 'https://www.pricecharting.com/console/xbox-360', \
                    'https://www.pricecharting.com/console/xbox-one', 'https://www.pricecharting.com/console/xbox-series-x']
            },
            'Sony': {
                'url': ['https://www.pricecharting.com/console/playstation', 'https://www.pricecharting.com/console/playstation-2', \
                    'https://www.pricecharting.com/console/playstation-3', 'https://www.pricecharting.com/console/playstation-4', \
                        'https://www.pricecharting.com/console/playstation-5', 'https://www.pricecharting.com/console/playstation-vita', \
                            'https://www.pricecharting.com/console/psp']
            },
            'Nintendo': {
                'url': ['https://www.pricecharting.com/console/nes', 'https://www.pricecharting.com/console/super-nintendo', 'https://www.pricecharting.com/console/nintendo-64', \
                    'https://www.pricecharting.com/console/gamecube', 'https://www.pricecharting.com/console/wii', 'https://www.pricecharting.com/console/wii-u', \
                        'https://www.pricecharting.com/console/nintendo-switch', 'https://www.pricecharting.com/console/gameboy', 'https://www.pricecharting.com/console/gameboy-color', \
                        'https://www.pricecharting.com/console/gameboy-advance', 'https://www.pricecharting.com/console/nintendo-ds', 'https://www.pricecharting.com/console/nintendo-3ds', \
                            'https://www.pricecharting.com/console/virtual-boy', 'https://www.pricecharting.com/console/game-&-watch']
            },
            'Sega': {
                'url': ['https://www.pricecharting.com/console/sega-master-system', 'https://www.pricecharting.com/console/sega-genesis', \
                    'https://www.pricecharting.com/console/sega-cd', 'https://www.pricecharting.com/console/sega-32x', 'https://www.pricecharting.com/console/sega-saturn', \
                        'https://www.pricecharting.com/console/sega-dreamcast', 'https://www.pricecharting.com/console/sega-game-gear', \
                            'https://www.pricecharting.com/console/sega-pico']
            },
            'Atari': {
                'url': ['https://www.pricecharting.com/console/atari-2600', 'https://www.pricecharting.com/console/atari-5200', 'https://www.pricecharting.com/console/atari-7800', \
                    'https://www.pricecharting.com/console/atari-400', 'https://www.pricecharting.com/console/atari-lynx', 'https://www.pricecharting.com/console/jaguar']
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(PricechartingSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category:
            self.logger.error("Category should not be None")
            raise CloseSpider('category  not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])
        self.filters = self.url_dict.get(category, []).get(brand, {}).get('filter', [])
        if not self.launch_url:
            self.logger.error(f'Spider is not supported for the category:{category}, brand:{brand}')
        self.logger.info(f'Crawling for category:{category}, brand:{brand}')


    def start_requests(self):
        if self.launch_url:
            for url in self.launch_url:
                meta = {}
                meta['start_index'] = 0
                if  re.findall(r'\?sort=name', url):
                    url = url + self.listing_page_api_end_part.format(start_index=meta['start_index'])
                else:
                    start_index = meta['start_index']
                    url = f'{url}?sort=name{self.listing_page_api_end_part.format(start_index=start_index)}'
      
                yield self.create_request(url=url, callback=self.parse_listing_page_api, meta=meta)


    def parse_listing_page_api(self,response):
        """
        @url https://www.pricecharting.com/console/psp?sort=name&genre-name=systems&cursor=0&format=json
        @meta {"use_proxy": "True", "start_index": 0}
        @returns requests 1
        """
        meta = response.meta
        response_json = response.text
        response_json = json.loads(response_json)
        for product in response_json.get('products', []):
            consoleuri = product.get('consoleUri', '')
            producturi = product.get('productUri', '')
            item_page_url = self.item_page_base_url.format(consoleuri=consoleuri, producturi=producturi)
            yield self.create_request(url=item_page_url, callback=self.crawldetails)

        if response_json.get('products', []):
            meta['start_index'] += 50
            startindex_value = 'cursor={}'.format(str(meta['start_index']))
            next_page_url  = re.sub(r'cursor=\d+', startindex_value, response.url)
            yield self.create_request(url=next_page_url, callback=self.parse_listing_page_api, meta=meta)


    def crawldetails(self, response):
        """
        @url https://www.pricecharting.com/game/psp/psp-2000-limited-edition-daxter-version-silver
        @meta {"use_proxy": "True"}
        @scrape_values id title loose_price CIB_price new_price graded_price box_only_price manual_only_price image \
            UPC ASIN\s(Amazon) ePID\s(eBay) Release\sDate completed-auctions-used completed-auctions-cib publisher
        """
        meta = response.meta
        data = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,
            'title':  response.xpath("//h1[@id='product_name']/text()").get(default='').strip(),
            'loose_price': response.xpath("//td[@id='used_price']/span/text()").get(default='').strip(),
            'CIB_price': response.xpath("//td[@id='complete_price']/span/text()").get(default='').strip(),
            'new_price':  response.xpath("//td[@id='new_price']/span/text()").get(default='').strip(),
            'graded_price':  response.xpath("//td[@id='graded_price']/span/text()").get(default='').strip(),
            'box_only_price':  response.xpath("//td[@id='box_only_price']/span/text()").get(default='').strip(),
            'manual_only_price':  response.xpath("//td[@id='manual_only_price']/span/text()").get(default='').strip(),
            'platform': response.xpath("//h1[@id='product_name']/a/text()").get(default='').strip(),
            'image': response.xpath("//div[@class='cover']/img/@src").get(default=''),
            'publisher': response.xpath("//td[@itemprop='publisher']/a/text()").get(default=''),
            'variant': meta.get('variant_name', ''),
        }
        price_history_names = ['completed-auctions-used', 'completed-auctions-cib', 'completed-auctions-new', 'completed-auctions-graded','completed-auctions-box-only','completed-auctions-manual-only']
        price_history = {}
        for price_his in price_history_names:
            price_history_data = []
            for j in response.xpath("//div[@class='"+price_his +"']/table/tbody/tr"):
                item1 =  { 'date': j.xpath(".//td[@class='date']/text()").get(),
                    'title_id': j.xpath(".//td[@class='title']/@title").get(),
                    'link': j.xpath(".//td[@class='title']/a/@href").get(),
                    'title': j.xpath(".//td[@class='title']/a/text()").get().strip(),
                    'price': j.xpath(".//td[@class='numeric']/span/text()").get(),
                }
                price_history_data.append(item1)
            price_history.update({price_his:price_history_data})

        specs = {}
        for table_row in response.xpath("//table[@id='attribute']/tr"):
            table_data = table_row.xpath(".//td/text()")
            if len(table_data) == 2:
                specs.update({table_data[0].get().replace(":",'').strip():table_data[1].get().strip()})
        

        # In this site, name of variant is not specified in detail page of an item instead detail page has the other names of the variant is listed in the detail page. 
        # e.g https://www.pricecharting.com/game/xbox/america%27s-army-rise-of-a-soldier-special-edition 
        # this is a special edition variant but the variant shown in detail page is standard so the detail page is not showing the name of the variant instead other variant of the product. Crawling for the variants are done below.
        if not meta.get('depth_2', '') and self.filters:
            if meta.get('depth_1', ''):
                meta.pop('depth_1', '')
                level = 'depth_2'
            else:
                level = 'depth_1'
                crawling_variants = response.xpath("//table[@id='attribute']//tr/td[contains(text(),'Variants')]/../td[@class='details']/a/text()").getall()
                meta.update({'crawled_variants': crawling_variants})
            
            variants_item = response.xpath("//table[@id='attribute']/tr/td[contains(text(),'Variants')]/../td[@class='details']/a")
            for item in variants_item:
                name = item.xpath('.//text()').get()
                url = item.xpath('.//@href').get()
                meta.update({'variant_name': name})
                meta[level] =  True
                if level == 'depth_2' and name in meta.get('crawled_variants', ''):
                    continue
                if url and name:
                    yield self.create_request(url=self.base_url+url, callback=self.crawldetails, meta=meta) 

        data.update(specs)
        data.update(price_history)
        data = {k: v for k, v in data.items() if k and v}
        yield data


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
