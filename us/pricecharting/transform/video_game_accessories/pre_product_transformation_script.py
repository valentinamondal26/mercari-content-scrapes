import re
import hashlib
from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title','')
            model = record.get('title','')
            
            platform = record.get('platform', '')
            
            model = re.sub(r'\[|\]', '', model, flags=re.IGNORECASE)
                        
            colors = []
            for word in model.split():
                try:
                    Color(word)
                    colors.append(word)
                except ValueError:
                    pass
            color = '/'.join(colors)

            model = re.sub(r'\b{}\b'.format(platform), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bXbox\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda  x: r'\b'+x+r'\b', colors))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            
            if re.findall(r'^\bXbox\b$', platform, flags=re.IGNORECASE):
                platform = 'XBOX Original'
            platform = re.sub(r'\bXbox Series X\b', 'Xbox Series X|S', platform, flags=re.IGNORECASE)
            

            upc = record.get('UPC', '')
            upc = re.sub(r'\bnone\b', '', upc, flags=re.IGNORECASE).strip()

            publisher = record.get('publisher', '') or record.get('Publisher', '')
            publisher = re.sub(r'\bnone\b', '', publisher, flags=re.IGNORECASE).strip()
            
            meta_product_type = ['Sensor', 'Screen', 'Adapter', 'Adaptor', 'Hard Drive', 'Stage Kit', 'Charge Kit', 'Headset', 'DVD Player', \
                'Controller', 'Memory Unit', 'Remote', 'Camera', 'Media Drive', 'battery', 'charger']
            
            transform_meta_product_type ={
                'controller': 'Controllers & Controller Accessories',
                'remote': 'Controllers & Controller Accessories',
                'media drive': 'Hard Drives',
                'memory unit': 'Hard Drives',
                'hard drive': 'Hard Drives',
                'adapter': 'Cables & Networking',
                'adaptor': 'Cables & Networking',
                'charge kit': 'Batteries & Chargers',
                'battery': 'Batteries & Chargers',
                'charger': 'Batteries & Chargers',
                'headset': 'Headsets',
                'camera': 'Cameras & Sensors',
                'sensor': 'Cameras & Sensors'
            }
            product_types = re.findall(r'|'.join(list(map(lambda x: r'\b' + x +r'\b', meta_product_type))), title, flags=re.IGNORECASE)
            if product_types:
                product_type = product_types[0].lower()
                product_type = transform_meta_product_type.get(product_type, product_type)
                product_type =  titlecase(product_type)
            else:
                product_type = ''
            
            if re.findall(r'|'.join(list(map(lambda x: r'\b' + x + r'\b', ['Battery', 'Charger']))), title, flags=re.IGNORECASE):
                product_type = 'Batteries & Chargers'   ##This is for model "Rechargeable controller and battery pack". if battery or charger is present \
                                                        ##in the model, we have to select batteries and charger product type despite rest of the product type.\
                                                        ## In this model "controller" is present but we have to select battery and chargers.

            if not product_type:
                controller_and_accessories_items = ['Movie Playback Kit', 'Blaster', 'Action Replay', 'Gun Bundle']
                cables_and_networking_items = ['AV Pack']
                stage_kit = ['Microphone']
                cameras_sensors = ['Kinectimals']
                if re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', controller_and_accessories_items))), title, flags=re.IGNORECASE):
                    product_type = 'Controllers & Controller Accessories'
                elif  re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', cables_and_networking_items))), title, flags=re.IGNORECASE):
                    product_type = 'Cables & Networking'
                elif re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', stage_kit))), title, flags=re.IGNORECASE):
                    product_type = 'Stage Kit'
                elif re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', cameras_sensors))), title, flags=re.IGNORECASE):
                    product_type = 'Cameras & Sensors'

            description = record.get('Description', '')
            if re.findall(r'^\bnone\b$', description, flags=re.IGNORECASE):
                description = ''
            
            if re.findall(r'\bDVD Movie Playback Kit\b', model, flags=re.IGNORECASE):
                model = 'DVD Movie Remote and Sensor'

            if re.findall(r'\bDead Space 3 Dev Team Edition \(Accessories Only\)', title, flags=re.IGNORECASE):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image': record.get('image', ''),

                'upc': upc,
                'model': model,
                'publisher': publisher,
                'price':  {
                    'currency_code': 'USD',
                    'amount': record.get('loose_price', '').strip('$')
                },
                'color': color,
                'platform': platform,
                'product_type': product_type,
                'description': description,
            }
            return transformed_record
        else:
            return None
