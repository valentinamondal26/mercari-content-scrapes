import re
import hashlib
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
           
            title = record.get('title', '')
            model = record.get('title', '')
            genre = record.get('Genre', '')
            
            model = re.sub(re.compile(r'\[|\]',re.IGNORECASE), '', model)
            model =  re.sub(r'\bNot for Resale\b|\bText Label\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            
            
            upc = record.get('UPC', '')
            upc = re.sub(r'\bnone\b', '', upc, flags=re.IGNORECASE)
            
            platform = record.get('platform', '')
            if re.findall(r'^\bXbox\b$', platform, flags=re.IGNORECASE):
                platform = 'Xbox Original'

            if record.get('brand', '') == 'Atari' and re.findall(r'^Jaguar$', platform, flags=re.IGNORECASE):
                platform = 'Atari Jaguar'
            
            if record.get('brand', '') == 'Nintendo' and not re.findall(r'\bNintendo\b', platform, flags=re.IGNORECASE):
                platform = f'Nintendo {platform}'

            if record.get('brand', '') == 'Nintendo':
                platform_words = re.findall(r'\w+', platform)
                for word in platform_words:
                    if not word.isupper() and not word.istitle():
                        platform = re.sub(r'\b{word}\b'.format(word=word), word.title(), platform)

            if re.findall(r'\bConsole\b|\bSystem\b|\bControllers*\b', title, flags=re.IGNORECASE):
                return None
            
            if  re.findall(r'\baccessories\b|\bsystems*\b|\bControllers*\b', genre, flags=re.IGNORECASE):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image': record.get('image', ''),
                 
                'model': model,
                'price': {
                    'currencyCode': 'USD',
                    'amount': record.get('loose_price', '').strip('$')
                },
                'msrp': {
                    'currencyCode': 'USD',
                    'amount': record.get('new_price', '').strip('$')
                },
                'genre': record.get('Genre', ''),
                'release_date': record.get('Release Date', '').replace('none',''),
                'upc': upc,
                'description': record.get('description', ''),
                'platform': platform,
            }
        
            return transformed_record
        else:
            return None
