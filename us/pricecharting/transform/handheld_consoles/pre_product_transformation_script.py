import re
import hashlib
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\[|\]',re.IGNORECASE), '', model)
            variant_color_names = ['Pearl', 'Piano', 'Mystic', 'Metallic', 'Aqua']
            colors = []
            
            for name in model.split():
                try:
                    Color(name)
                    variant_color = re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\s+' + name + r'\b', variant_color_names))),\
                         model, flags=re.IGNORECASE)
                    if variant_color:
                        colors.append(variant_color[0])
                    if not variant_color:
                        colors.append(name)                    
                except ValueError:
                    pass
            colors = list(dict.fromkeys(colors))
            color = '/'.join(colors)
            color = re.sub(r'(.*)(Aqua)\/(Aqua\s+\w+)(.*)', r'\1\3\4', color, flags=re.IGNORECASE)
            colors = color.split('/')
           
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', colors))), '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\bVersion\b|\bConsole\b',re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\&$', '', model).strip()

            price_history = [
                {
                    'currency_code': 'USD',
                    'amount': record.get('CIB_price', '').strip('$'),
                    'status': 'Used',
                },
                {
                    'currency_code': 'USD',
                    'amount':record.get('new_price', '').strip('$'),
                    'status': 'New',
                },
            ]

            upc = record.get('UPC', '')
            upc = re.sub('none', '', upc, flags=re.IGNORECASE).strip()

            release_year = record.get('Release Date', '')
            release_year = re.sub('none', '', release_year, flags=re.IGNORECASE).strip()

            platform = ''
            if re.findall(r'\bPSP\b', model, flags=re.IGNORECASE):
                platform = 'PSP'
            else:
                platform = 'Playstation Vita'

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image': record.get('image', ''),

                'model': model,
                'color': color,
                'platform': platform,
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('loose_price', '').strip('$')
                },
                'price_history': price_history,
                'release_year': release_year,
                'upc': upc,
            }
            return transformed_record
        else:
            return None
