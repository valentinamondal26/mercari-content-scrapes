import re
import hashlib
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title','')
            model = record.get('title','')
            colors = []
            for name in title.split():
                try:
                    Color(name)
                    secondary_colors_meta = ['Funtastic \w+', 'Pearl', 'Piano', 'Mystic']
                    variant_color = re.findall(r'|'.join(list(map(lambda x: r'\b'+ x +r' {color}\b'.format(color=name),secondary_colors_meta ))), title, flags=re.IGNORECASE)
                    if variant_color:
                        colors.append(variant_color[0])
                    else:
                        colors.append(name)
                except ValueError:
                    pass

            if re.findall(r'\bPlatinum\b', model, flags=re.IGNORECASE):
                colors.append('Platinum')
            color = '/'.join(colors)
            
            platform_data = record.get('platform', '')
            platform_meta = {
                'Super Nintendo': 'SNES',
                'Nintendo 64': 'N64',
            }
            platform = platform_meta.get(platform_data, platform_data)
            
            model = re.sub(color, '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\[|\]|\bSystem\b|\bNot for resale\b|\bConsole\b', re.IGNORECASE), '', model)
            model = re.sub(r'Retro-Bit Super Retro Trio Plus', 'Retro-Bit Super Nintendo Retro Trio Plus', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', colors))), '', model, flags=re.IGNORECASE)
            if not re.findall(r'\bSuper Nintendo\b|\bNintendo 64\b|\b{platform}\b'.format(platform=platform), model, flags=re.IGNORECASE):
                model = f'{platform} {model}'

            model = re.sub(r'\b(.*)({platform})(.*)\b'.format(platform=platform), r'\2 \1\3', model, flags=re.IGNORECASE)
            if not re.findall(r'\bSuper Nintendo\b', model, flags=re.IGNORECASE):
                model = re.sub(r'(.*)(Nintendo 64|Nintendo)(.*)', r'\2 \1 \3',model,flags=re.IGNORECASE)

            model = re.sub(r'\bGamecube\b', 'GameCube', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPlatinum\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bNintendo NES\b|\bNES\b', 'Nintendo Entertainment System', model, flags=re.IGNORECASE)
            if not re.findall(r'\bNintendo\b', model, flags=re.IGNORECASE):
                model = f'Nintendo {model}'
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(r'^Nintendo Wii Mini$', model, flags=re.IGNORECASE):
                color = 'Red/Black'
            upc = record.get('UPC', '')
            upc = re.sub('none', '', upc, flags=re.IGNORECASE).strip()

            release_year = record.get('Release Date', '')
            release_year = re.sub('none', '', release_year, flags=re.IGNORECASE).strip()

            description = record.get('Description', '')
            if re.findall(r'^\bnone\b$', description, flags=re.IGNORECASE):
                description = ''
            
            if re.findall(r'\biQue\b|\bHomebrew\b', model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image': record.get('image', ''),

                'model': model,
                'description': description,
                'color': color,
                'platform': platform,
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('loose_price', '').strip('$').strip()
                },
                'msrp': {
                    'currency_code': 'USD',
                    'amount': record.get('new_price', '').strip('$').strip()
                },
                # 'price_history': price_history,
                'release_date': release_year,
                'upc': upc,
            }
            return transformed_record
        else:
            return None
