import re
import hashlib
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title','')
            title = record.get('title', '')

            platform = record.get('platform', '')
            if re.findall(r'^\bXbox\b$', platform, flags=re.IGNORECASE):
                platform = 'Xbox Original'
                # model = re.sub(r'\bXbox\b', 'Xbox Original', model, flags=re.IGNORECASE)
            
            if re.findall(r'Xbox Original', platform, flags=re.IGNORECASE):
                model = re.sub(r'\bXbox\b', '', model, flags=re.IGNORECASE)
                model = f'Xbox Original {model}'
            
            model = re.sub(r'(\[.*\])', '', model)
    
            variant = record.get('variant', '')
            variant = variant.title()
            if re.findall(r'^Standard$', variant):
                variant = 'Standard Black'
 
            meta_variant = ['Translucent Green', 'Tony Hawk Underground 2']
            if not variant:
                variant_in_model = re.findall(r'|'.join(list(map(lambda  x: r'\b'+x+r'\b', meta_variant))), title, flags=re.IGNORECASE)
                if variant_in_model:
                    variant = variant_in_model[0]
            variant = re.sub(r'\bEdition\b', '', variant, flags=re.IGNORECASE)
            
            model = re.sub(r'\b{variant}\b|\bEdition\b'.format(variant=variant), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            release_date = record.get('Release Date', '')
            release_date = re.sub('none', '', release_date, flags=re.IGNORECASE).strip()

            if re.findall(r'Xbox Original', model, flags=re.IGNORECASE):
                storage_capacity = '8GB' ##manually asked to fill the storage capacity for all the xbox originals

            upc = record.get('UPC', '')
            upc = re.sub('none', '', upc, flags=re.IGNORECASE).strip()
            upc = ', '.join(list(map(lambda x: x.strip(), upc.split(','))))
            
            if record.get('loose_price', '').upper() == 'N/A' or record.get('loose_price','') == '$0.00':
                return None
        
            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image': record.get('image', ''),

                'model': model,
                'color': variant,
                'storage_capacity': storage_capacity,
                'platform': platform,
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('loose_price', '').strip('$').strip()
                },
                'release_date': release_date,
                'upc': upc,
            }
            return transformed_record
        else:
            return None
