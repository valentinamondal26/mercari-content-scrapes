import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', '') or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'Rae Dunn', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(Set of \d+)(.*)', r'\2 \1', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()


            def fraction_to_decimal(fraction):
                from fractions import Fraction
                import unicodedata
                nums = fraction.split('/')
                if len(nums) == 2:
                    return str(round(float(Fraction(int(nums[0]), int(nums[1]))), 1))[1:]
                else:
                    try:
                        return str(round(unicodedata.numeric(u'{}'.format(fraction)), 1))[1:]
                    except:
                        pass
                    return fraction

            dimensions = ', '.join(record.get('dimensions', []))
            length = ''
            match = re.findall(r'Length:\s*(\d+)\s*(\d+/\d+){0,1}', dimensions, flags=re.IGNORECASE)
            if match:
                length = match[0][0] + fraction_to_decimal(match[0][1]) + ' in.'

            width = ''
            match = re.findall(r'Width:\s*(\d+)\s*(\d+/\d+){0,1}', dimensions, flags=re.IGNORECASE)
            if match:
                width = match[0][0] + fraction_to_decimal(match[0][1]) + ' in.'


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                'length': length,
                'width': width,
                'product_type': 'Kitchen & Storage',
                'model_sku': record.get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
