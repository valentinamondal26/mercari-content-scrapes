'''
https://mercari.atlassian.net/browse/USCC-253 - Rae Dunn Home decor accents
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

class HobbylobbySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from hobbylobby.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Home decor accents
    
    brand : str
        brand to be crawled, Supports
        1) Rae Dunn

    Command e.g:
    scrapy crawl hobbylobby -a category='Home decor accents' -a brand='Rae Dunn'
    """

    name = 'hobbylobby'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'hobbylobby.com'
    blob_name = 'hobbylobby.txt'

    base_url = 'https://www.hobbylobby.com'

    url_dict = {
        'Home decor accents': {
            'Rae Dunn': {
                'url': 'https://www.hobbylobby.com/search/?text=Rae+Dunn',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(HobbylobbySpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.hobbylobby.com/Spring-Shop/Kitchen-Picnic/Cookware-Cooking-Utensils/Rae-Dunn-Whisk/p/81012810
        @scrape_values title breadcrumb image price sale_price sku description dimensions full_description
        @returns items 1
        @meta {"use_proxy":"True"}
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'breadcrumb': ''.join(response.xpath('//div[@id="breadcrumb"]/ul/li//text()[normalize-space(.)]').extract()),
            'title': response.xpath('//h1[@itemprop="name"]/text()').extract_first(),
            'price': ''.join(response.xpath('//span[@class="price variant"]/span[@class="old old-price-copy"]/text()').extract()).strip(),
            'sale_price': ''.join(response.xpath('//span[@class="price variant"]/span[@class="current sale-price sale-price-copy"]/text()').getall()).strip(),
            'sku': response.xpath('//p[@itemprop="sku"]/span[@class="value"]/text()').extract_first(),
            'description': response.xpath('//div[@itemprop="description"]/p/text()').extract_first(),
            'dimensions': response.xpath('//div[@itemprop="description"]/p[contains(text(), "Dimensions:")]/following-sibling::ul/li/text()').extract(),
            'full_description': response.xpath('//div[@itemprop="description"]//text()[normalize-space(.)]').extract(),
        }


    def parse(self, response):
        """
        @url https://www.hobbylobby.com/search/?text=Rae+Dunn
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for product in response.xpath('//div[contains(@class, "product-list clearfix")]/ul/li'):
            link = product.xpath('.//a/@href').extract_first()
            if link:
                link = self.base_url + link
                yield self.create_request(url=link, callback=self.crawlDetail)

        next_link = response.xpath('//div[@class="section pagination"]//nav[@aria-label="Pagination Navigation"]/ul/li[@class="next-button"]/a/@href').extract_first()
        if next_link:
            next_link = self.base_url + next_link
            yield self.create_request(url=next_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
