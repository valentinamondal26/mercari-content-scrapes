class Mapper:
    def map(self, record):

        model = ''
        material = ''
        gemstone = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "material" == entity['name']:
                if entity["attribute"]:
                    material = entity["attribute"][0]["id"]
            elif "gemstone" == entity['name']:
                if entity["attribute"]:
                    gemstone = entity["attribute"][0]["id"]

        key_field = model + material + gemstone
        return key_field