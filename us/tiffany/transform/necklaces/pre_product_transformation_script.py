import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            image = record.get('image', '')
            image = 'https://media.tiffany.com/is/image/Tiffany/' + image.split('-')[-1]

            materials_meta = [
                'Lacquer', '18k White Gold', 'White Gold', 'Silk', 'Rubedo® Metal', '18k Rose Gold', 'Rose Gold',
                'Platinum', 'Sterling Silver', 'Crystal', 'Titanium', 'Stainless Steel',
                'Ruthenium', '18k Gold', 'Gold',
            ]
            materials = record.get('Materials', []) or []

            model = record.get('title', '')

            if '18k' in model:
                materials = [ f'18k {material}' if '18k' not in material and re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['White Gold', 'Rose Gold', 'Gold']))), material, re.IGNORECASE) else material for material in materials]

            match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), model)
            if match:
                if isinstance(materials, str):
                    materials = match.extend(materials)
                else:
                    materials.extend(match)

            materials = list(dict.fromkeys(materials))

            gemstones_meta = [
                r'(Pearl)s{0,1}', 'Onyx', r'(Yellow Diamond)s{0,1}', 'Turquoise', r'(Tanzanite)s{0,1}', r'(Sapphire)s{0,1}',
                'Rubies', 'Ruby', r'(Pink Diamond)s{0,1}', 'Opal', "Tiger's Eye", r'(Colored Gemstone)s{0,1}',
                r'(Colored Diamond)s{0,1}', r'(Aquamarine)s{0,1}', r'(Diamond)s{0,1}',
            ]
            gemstones = record.get('Gemstones', []) or []
            if isinstance(gemstones, str):
                gemstones = [gemstones]
            # if not gemstones:
            match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', gemstones_meta))), re.IGNORECASE), model)
            if match:
                gemstones.extend([ group[0] for group in match ])

            d = {
                'Pearl': 'Pearls',
                'Yellow Diamond': 'Yellow Diamonds',
                'Tanzanite': 'Tanzanites',
                'Sapphire': 'Sapphires',
                'Ruby': 'Rubies',
                'Pink Diamond': 'Pink Diamonds',
                'Colored Gemstone': 'Colored Gemstones',
                'Colored Diamond': 'Colored Diamonds',
                'Aquamarine': 'Aquamarines',
                'Diamond': 'Diamonds',

            }
            gemstones = list(map(lambda x: re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', d.keys()))), re.IGNORECASE), lambda x: d.get(x.group(), x.group()), x), gemstones))
            gemstones = list(dict.fromkeys(gemstones))
            gemstones = list(filter(None, gemstones))
            gemstones = list(filter(lambda x: not re.findall(re.compile(r'\bNo Gemstones\b', re.IGNORECASE), x), gemstones))

            keywords = [
                # 'Necklace',
                'Ampersand',
            ]
            if materials:
                keywords.extend(materials)
            if gemstones:
                keywords.extend(gemstones)
            # keywords.extend(materials_meta)
            keywords.extend(gemstones_meta)
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b(\sand)*', keywords))), re.IGNORECASE), '', model)
            model = re.sub(r':', ' ', model)
            model = re.sub(r'®|™', '', model)
            model = re.sub(r'\b\d+([-–]\d+)*"', '', model)
            # model = re.sub(re.compile(r'\b\d+k"', re.IGNORECASE), '', model)
            model = re.sub(r',\s*$', '', model)
            model = re.sub(r'\bwith\s+and\b', 'with', model)
            model = re.sub(r'\bwith\s*$', '', model)
            model = re.sub(r'\bin\s+and\b', '', model)
            model = re.sub(r'\bin\s*$', '', model)
            model = re.sub(r'\band\s*$', '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            description = ', '.join(record.get('detail_description', [])) + ' '
            details = [', '.join(detail) for detail in record.get('details', [])]
            description = description + ', '.join(list(filter(None, details)))
            description = description.strip()

            product_line = ', '.join(record.get('Designers & Collections', []))
            if not product_line:
                product_line = record.get('title', '').split(':')[0]
            product_line = re.sub(r'®|™', '', product_line)
            product_line = re.sub(r'\s+', ' ', product_line).strip()

            if not re.findall(re.compile(r'\bTiffany\b', re.IGNORECASE), model):
                model = f'Tiffany {model}'

            materials = sorted(materials)
            material = ', '.join(materials)
            material = re.sub(r'®', '', material)

            gemstone = ', '.join(gemstones)

            # band_size = ''
            # match = re.findall(re.compile(r'Fits wrists up to (.*")', re.IGNORECASE), description)
            # if match:
            #     band_size = match[0]
            total_carat_weight = ''
            match = re.findall(re.compile(r'Carat total weight (\d+\.*\d*)', re.IGNORECASE), description)
            if match:
                total_carat_weight = match[0]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                'title': record.get('title', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": image,

                "model": model,
                "description": description,
                "product_line": product_line,
                "material": material,
                'gemstone': gemstone,
                # 'band_size': band_size,
                'total_carat_weight': total_carat_weight,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
