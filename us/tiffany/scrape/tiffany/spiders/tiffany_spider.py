'''
https://mercari.atlassian.net/browse/USDE-1257 - Tiffany & Co. Bracelets
https://mercari.atlassian.net/browse/USDE-1258 - Tiffany & Co. Necklaces

NOTE: This site has trottling issue, and stops responding even > 180s.
Detail page crawl count is huge, And so did a workaround utilizing httpcache
and re-ran the pipeline continuously untill all the pages are fetched.
TODO: Should find a better throtlling settings for this spider
'''

import scrapy
import os
from datetime import datetime
from scrapy.exceptions import CloseSpider
import itertools
from copy import deepcopy
from common.scrapy_contracts.contracts import set_contracts_mock_values
import json
import re
import base64


class TiffanySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from tiffany.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Bracelets
        2) Necklaces
    
    brand : str
        brand to be crawled, Supports
        1) Tiffany & Co.

    Command e.g:
    scrapy crawl tiffany -a category='Bracelets' -a brand='Tiffany & Co.'
    scrapy crawl tiffany -a category='Necklaces' -a brand='Tiffany & Co.'
    """

    name = 'tiffany'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'tiffany.com'
    blob_name = 'tiffany.txt'

    filter_list = None
    extra_item_infos = {}

    client_id = ''
    client_secret = ''

    base_url = 'https://www.tiffany.com'

    url_dict = {
        'Bracelets': {
            'Tiffany & Co.': {
                'url': 'https://www.tiffany.com/jewelry/shop/bracelets/',
                'filters': ["Materials", "Gemstones", "Designers & Collections", "GemStoneColor",],
            }
        },
        'Necklaces': {
            'Tiffany & Co.': {
                'url': 'https://www.tiffany.com/jewelry/shop/necklaces-pendants/',
                'filters': ["Materials", "Gemstones", "Designers & Collections", "GemStoneColor",],
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TiffanySpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filter_list = ["Materials", "Gemstones", "Designers & Collections", "GemStoneColor"]

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.client_id = ''
        self.client_secret = ''


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            request = self.create_request(url=self.launch_url, callback=self.parse)
            yield request


    def crawlDetail(self, response):
        id = response.url
        if not self.extra_item_infos.get(id, None):
            self.extra_item_infos[id] = {}

        description = response.xpath(u'normalize-space(//p[@class="product-description__container_long-desc-total"]/text())').extract_first()
        details = response.xpath('//ul[@class="product-description__container_detail_list"]/li/span[@class="product-description__container_list-content"]/text()').extract()
        self.update_extra_item_infos(id,
            {
                'detail_description': description,
                'details': details,
            }
        )


    def parse_api_listings(self, response):
        """
        @url https://www.tiffany.com/tiffanyco/ecomprod02/ecomproductsearchprocessapi/api/process/v1/productsearch/ecomguidedsearch 
        @meta {"use_proxy":"True", "request_method":"POST"}
        @body {}
        @headers {}
        @returns requests 2
        @scrape_values id sku image title price description
        """
        #  Headers and body part for the contracts request url will be added dynamically while running the contracts.
        #  Body and Headers are added in the config json and contracts will add it while running the contracts.

        data = json.loads(response.text)
        for product in data.get('resultDto', {}).get('products', []):
            id = self.base_url + product.get('friendlyUrl', '')
            if not self.extra_item_infos.get(id, None):
                self.extra_item_infos[id] = {}

            # self.extra_item_infos.get(id).update(response.meta.get('extra_item_info', {}))
            self.update_extra_item_infos(id, response.meta.get('extra_item_info', {}))
            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,

                'id': id,
                'sku': product.get('sku', ''),
                'image': product.get('imageURL', ''),
                'title': product.get('title', ''),#product['name']
                'price': product.get('formattedPrice', ''),#product['price']
                'description': [
                    product.get('mipsDescription', ''),
                    product.get('posDescription', ''),
                    product.get('extendedDescription', ''),
                ],
            }

            yield self.create_request(id, callback=self.crawlDetail)


    @set_contracts_mock_values
    def parse_api(self, response):
        """
        @url https://www.tiffany.com/tiffanyco/ecomprod02/ecomproductsearchprocessapi/api/process/v1/productsearch/ecomguidedsearch
        @meta {"use_proxy":"True","request_method":"POST"}
        @headers {}
        @body {}
        @returns requests 2
        """
        # Headers and body part for the contracts request url will be added dynamically while running the contracts. 
        # Body and Headers are added in the config json and contracts will add it while running the contracts.
        payload = response.meta.get('payload', {})

        data = json.loads(response.text)

        for filter_type_details in data.get('resultDto', {}).get('dimensions', []):
            if filter_type_details.get('groupName', '') in self.filter_list:
                for filter_info in filter_type_details.get('dimensionValues', []):
                    body = deepcopy(payload)
                    naviagtion_filters = body.get('navigationFilters', [])
                    naviagtion_filters.append(filter_info.get('id', ''))
                    body['navigationFilters'] = naviagtion_filters
                    body["recordsCountPerPage"] = filter_info.get('count', 0)
                    request = self.create_request(
                        response.url,
                        callback=self.parse_api_listings,
                        method='POST',
                        body=body,
                        headers={
                            'content-type': "application/json",
                            'x-ibm-client-id': self.client_id,
                            'x-ibm-client-secret': self.client_secret,
                        }
                    )
                    request.meta['extra_item_info'] = {
                        filter_type_details['groupName']: filter_info.get('name', ''),
                    }
                    yield request


        body = payload
        body["recordsCountPerPage"] = data.get('resultDto', {}).get('numofRecords', 0)
        yield self.create_request(
            response.url,
            callback=self.parse_api_listings,
            method='POST',
            body=body,
            headers={
                'content-type': "application/json",
                'x-ibm-client-id': self.client_id,
                'x-ibm-client-secret': self.client_secret,
            }
        )


    def parse(self, response):
        """
        @url https://www.tiffany.com/jewelry/shop/bracelets/
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        j = json.loads(re.findall(r'window.tiffany.authoredContent.browseConfig =(.*)', response.xpath('//script[contains(text(), "tagsMap = JSON.parse(")]/text()').extract_first())[0])
        url_type = j.get('request', {}).get('url', '')
        url = self.base_url + json.loads(re.findall(r'}\s*window.tiffany.apiUrl = (.*?);', response.xpath('//script[contains(text(), "window.tiffany.apiUrl =")]/text()').extract_first())[0]).get(url_type, '')
        payload = j.get('request', {}).get('payload', {})

        script = response.xpath('//script[contains(text(), "window.tiffany.apiUrl =")]/text()').get(default='')
        match = re.findall(r'"clientid":"(.*?)"', script)
        if match:
            self.client_id = base64.b64decode(match[0])
        match = re.findall(r'"secret":"(.*?)"', script)
        if match:
            self.client_secret = base64.b64decode(match[0])

        if self.client_id and self.client_secret and payload:
            yield self.create_request(
                url,
                callback=self.parse_api,
                method='POST',
                body=payload,
                headers= {
                    'content-type': "application/json",
                    'x-ibm-client-id': self.client_id,
                    'x-ibm-client-secret': self.client_secret,
                },
                errback=self.errorHandler,
                meta={'payload': payload}
            )
        else:
            CloseSpider('client id or client secret or payload is empty')


    def errorHandler(self,failure):
        self.logger.error(f'Handling failed request {failure.request.url}')


    def update_extra_item_infos(self, id, extra_item_info): 
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def create_request(self, url, callback, method='GET', headers={}, body=None, meta=None, errback=None):
        request = scrapy.Request(
            url=url,
            method=method,
            body=json.dumps(body) if body else None,
            callback=callback,
            meta=meta,
            headers=headers,
        )
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request