# -*- coding: utf-8 -*-

# Scrapy settings for tiffany project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'tiffany'

SPIDER_MODULES = ['tiffany.spiders']
NEWSPIDER_MODULE = 'tiffany.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'httpie/1.0'#'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36'

ROBOTSTXT_OBEY = False

# RETRY_ENABLED=False
RETRY_TIMES=1
# DOWNLOAD_TIMEOUT=90

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

# GCS settings - actual value be 'raw/tiffany.com/sandals/tiffany/2019-02-06_00_00_00/json/tiffany.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10,
    'common.scrapy_contracts.contracts.Headers':10,
    'common.scrapy_contracts.contracts.AddBody':10
}