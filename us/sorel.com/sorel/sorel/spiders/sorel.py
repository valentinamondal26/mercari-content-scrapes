import scrapy
import random
import re

from datetime import datetime

from sorel.items import SorelItem

class SorelSpider(scrapy.Spider):
    name = 'sorel'
    start_urls = [
        'https://www.sorel.com/women/boots'
    ]

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = "Womens Boots"
    brand = "Sorel"
    source = 'sorel.com'
    blob_name = 'sorel.txt'

    def crawlDetail(self, response):
        item = response.meta['item']
        item['title'] = response.xpath(u'normalize-space(//h1[@class="product-name ml-1"]/text())').extract_first()
        item['price'] = response.xpath(u'normalize-space(//span[@class="reg-price"]/text())').extract_first()
        if item['price'] is None or item['price'] is '':
            item['price'] = response.xpath(u'normalize-space(//span[contains(@class, "price-sales")]/text())').extract_first()
        item['rating'] = response.xpath('//span[@class="bvseo-ratingValue"]/text()').extract_first() 
        item['style'] = response.xpath(u'normalize-space(//ul[@class="feature"]/li[contains(., "Style")]/span/text())').extract_first()
        item['description'] = "\n".join(response.xpath('//div[@class="product-summary"]//text()').extract())
        item['breadcrumb'] = " | ".join(response.xpath('//ol[@class="breadcrumb"]//span[@itemprop="name"]/text()').extract())
        item['color'] = response.xpath(u'normalize-space(//span[@class="selectedValue prod-capitalize"]/text())').extract_first()
        item['enduse'] = response.xpath('//div[@class="uses"]/span[@class="value"]/text()').extract_first()
        item['shaftheight'] = response.xpath(u'normalize-space(//ul[@class="feature"]/li[contains(., "Shaft Height")]/text())').extract_first()
        yield item


    def parse(self, response):
        count = ''.join(re.findall(r'\d+', response.xpath('//span[@class="category__resultCount"]/text()').extract_first()))
        if count:
            yield self.createRequest(url='https://www.sorel.com/women/boots/?pgsize=' + str(int(count)+1), callback=self.crawlAllItems)

    def crawlAllItems(self, response):
        for product in response.selector.xpath('//li[contains(@class, "grid-tile product-tile")]'):

            item = SorelItem()

            item['crawlDate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['statusCode'] = "200"
            item['category'] = self.category
            item['brand'] = self.brand

            item['image'] = product.xpath('.//img[@class="product-image"]/@src').extract_first()
            item['id'] = product.xpath('.//a/@href').extract_first()

            if item['id']:
                request = self.createRequest(url=item['id'], callback=self.crawlDetail)
                request.meta['item'] = item
                yield request


    def createRequest(self, url, callback):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request
