# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SorelItem(scrapy.Item):
    crawlDate = scrapy.Field()
    statusCode = scrapy.Field()
    id = scrapy.Field()
    title = scrapy.Field()
    category = scrapy.Field()
    brand = scrapy.Field()
    breadcrumb = scrapy.Field()
    model = scrapy.Field()
    description = scrapy.Field()
    material = scrapy.Field()
    style = scrapy.Field()
    collection = scrapy.Field()
    price = scrapy.Field()
    rating = scrapy.Field()
    image = scrapy.Field()

    color = scrapy.Field()
    enduse = scrapy.Field()
    shaftheight = scrapy.Field()