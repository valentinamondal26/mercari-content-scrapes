import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # remove category from model
            model = record.get('title', '')
            model = re.sub(re.compile(
                r'\bsandals\b|\bsandal\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            # material extraction and transformation
            materials = []
            for material in list(filter(lambda x: x.get('material', '') == "Outer", record.get('composition', []))) or record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    if material not in materials:
                        materials.append(material)
            material = ','.join(sorted(materials))
            material = material.title()
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ','.join(
                    list(filter(lambda x: x != 'Leather', material.split(','))))
            material = re.sub(re.compile(r'\(.*\)'), '', material)
            material = re.sub(r'\s+', ' ', material)
            if not material:
                meta_materials = ['Calf Leather', 'Leather', 'Bovine Leather', 'Raffia',
                                  'Thermoplastic Polyurethane', 'Patent Leather', 'Pvc', 'Polyurethane']
                match = re.findall(re.compile(r'|'.join(list(map(
                    lambda x: r'\b'+x+r'\b', meta_materials))), re.IGNORECASE), record.get('description', ''))
                if match:
                    material = ','.join(match)
            material = material.split(",")
            material = [m.title() for m in material if re.findall(
                re.compile(r"Other Materials", re.IGNORECASE), m) == []]
            material = ",".join(material)
            material = material.replace('Pvc', 'PVC')

            # color transformation
            colors = ','.join([color.strip()
                               for color in record.get('Colour', '').split(',')])
            colors = re.sub(re.compile(
                r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = colors.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "image": record.get("image", ""),
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "material": material,
                # 'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            if "Flops" in transformed_record["model"]:
                return None
            else:
                return transformed_record
        else:
            return None
