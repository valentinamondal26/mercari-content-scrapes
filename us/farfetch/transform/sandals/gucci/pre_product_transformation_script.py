import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bGucci\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'Open Toe|Peep Toe', re.IGNORECASE), lambda x: x.group().replace(' ', '-'), model)
            model = model.title()
            model = re.sub(re.compile(r'\dMm\b|\bGg\b', re.IGNORECASE), lambda x: x.group().upper(), model)
            model = re.sub(r'\s+', ' ', model).strip()

            materials = []
            outer_material = list(filter(lambda x: x.get('material','') == 'Outer', record.get('composition', []))) \
                + list(filter(lambda x: x.get('material','') == '', record.get('composition', [])))
            for material in outer_material:
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal|Other Fabrics|Fabric', re.IGNORECASE), material):
                    if material not in materials:
                        materials.append(material)
            material = ', '.join(sorted(materials))
            material = material.title()
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ', '.join(list(filter(lambda x: x != 'Leather', material.split(', '))))

            # size_unit = record.get('size_unit', '')
            # sizes = ','.join([size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ','.join([color.strip() for color in record.get('Colour', '').split(',')])
            colors = re.sub(re.compile(r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = re.sub('Multicolour', 'Multicolor', colors, flags=re.IGNORECASE)
            colors = colors.title()            

            description = record.get('description','')
            if not material:
                materials_meta = ['Cotton', 'Patent Leather', 'Calf Leather', 'Leather', 'Polyester', 'Jute', 'Suede', 'Acetate',
                                'Glass', 'Nylon', 'Canvas', 'Crystal', 'Lamb Skin', 'Goat Skin', 'Straw', 'Satin', 'Latex', 'Velvet']
                material = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))),description,flags=re.IGNORECASE)
                material = ', '.join(material).title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "image":record.get("image",""),
                "title": record.get('title', ''),
                "description": description,

                "heel_type":record.get("Heel Height",""),
                "product_type":record.get("Detail",""),
                "model": model,
                "color": colors,
                "material": material,
                # 'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            return transformed_record
        else:
            return None
