class Mapper:
    def map(self, record):
        key_field = ""

        color = ''
        material = ''

        entities = record['entities']
        for entity in entities:
            if "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            if "material" == entity['name']:
                if entity["attribute"]:
                    for attribute in entity["attribute"]:
                        material = material + attribute["id"]

        key_field = color + material
        return key_field
