import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(
                r'\bGucci\b|\bbelt\b|\bbelts\b', re.IGNORECASE), '', model)
            model = model.title()

            materials = []
            for material in record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    materials.append(material)
            material = ', '.join(materials)

            size_unit = record.get('size_unit', '')
            sizes = ','.join(
                [size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ', '.join([color.strip()
                                for color in record.get('colors', '').split(',')])

            model = re.sub(re.compile(r"\bgg\b", re.IGNORECASE), "GG", model)
            for mat in re.findall(r"\w+", material):
                if re.findall(re.compile(r"{}\s*with\s*interlocking".format(mat), re.IGNORECASE), model) != []:
                    re.sub(re.compile(
                        r"({})\s*with\s*(interlocking)".format(mat), re.IGNORECASE), r"\1 \2", model)
                model = re.sub(re.compile(
                    r"{}".format(mat), re.IGNORECASE), "", model)
            if re.findall(re.compile(r"\bwith\b", re.IGNORECASE), re.findall(r"\w+", model)[0]):
                model = re.sub(re.compile(
                    r"\bwith\b", re.IGNORECASE), "", model, 1)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "material": material,
                'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
