import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            description = record.get('description', '')

            materials = []
            # for material in record.get('composition', []):
            for material in list(filter(lambda x: x.get('material', '') == "Outer", record.get('composition', []))) or record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    if material not in materials:
                        material = re.sub(re.compile(r'\(top grain\)', re.IGNORECASE), '', material)
                        materials.append(material)
                
            materials = [material.strip() for material in materials]
            materials = list(filter(lambda x: not re.findall(re.compile(r'Other Materials|Other Fibers', re.IGNORECASE), x), materials))
            material = ', '.join(sorted(materials))
            material = material.title()
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ', '.join(list(filter(lambda x: x != 'Leather', material.split(', '))))
            material = material.replace('Pvc', 'PVC')
            material = material.replace('Thermoplastic Polyurethane (Tpu)', 'TPU')
            material = re.sub(r'\s+', ' ', material).strip()

            if not material:
                materials_meta = [
                    'Fabric', 'Raffia', 'Linen/Flax', 'Glass', 'Nylon', 'Recycled Cotton', 'Cotton', 'Spandex/Elastane', 'Polyamide', 'Polyester', 'Calf Suede', 'Rubber', 'TPU', 'PVC', 'Sheepskin', 'Bovine Leather', 'Ovine Leather', 'Leather',
                ]
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), description)
                if match:
                    material = ', '.join(match)

            # size_unit = record.get('size_unit', '')
            # sizes = ','.join([size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ', '.join([color.strip() for color in (record.get('Colour', '') or record.get('colors', '')).split(',')])
            colors = re.sub(re.compile(r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = colors.title()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bflats\b|\bflat\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'-', re.IGNORECASE), ' ', model)
            model = re.sub(re.compile(r'T Logo', re.IGNORECASE), 'T-Logo', model)
            if material:
                model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', material.split(', ')))), re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            if re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Sneakers', 'Pumps', r'\d+Mm']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "image":record.get("image",""),
                "title": record.get('title', ''),
                "product_type":record.get("Category",""),
                "description": description,
                "model": model,
                "color": colors,
                "style":record.get("Detail",""),
                "material": material,
                # 'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            return transformed_record
        else:
            return None
