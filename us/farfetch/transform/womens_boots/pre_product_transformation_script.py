import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            materials = []
            for material in list(filter(lambda x: x.get('material', '') == "Outer", record.get('composition', []))):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    if material not in materials:
                        materials.append(material)
                
            material = ','.join(materials)

            
            heel_height = ["Flat","Low Heel"]
            if record.get("Heel Height","")=="":
                try:
                    heel_height = [heel for heel in heel_height if heel in model][0]
                except IndexError:
                    heel_height = ""
            else:
                heel_height = record.get("Heel Height","")

            # size_unit = record.get('size_unit', '')
            # sizes = ','.join([size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ','.join([color.strip() for color in record.get('Colour', '').split(',')])
            colors = re.sub(re.compile(r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = colors.title()

            style = record.get('Detail', '').replace('Length', '').strip()
            if not style:
                match = re.findall(re.compile(r'\bAnkle\b|\bSnow\b|\bBow Tie\b', re.IGNORECASE), model)
                if match:
                    style = match[0].strip()
            style = style.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "image":record.get("image",""),
                "title": record.get('title', ''),
                "heel_height":heel_height,
                "style": style,
                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "material": material,
                # 'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            return transformed_record
        else:
            return None
