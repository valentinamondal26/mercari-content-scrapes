import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(
                r'\b596040J0740 1010\b|\b610379J0738\b', re.IGNORECASE), '', model)
            # model = re.sub(re.compile(r'\w+-\w+'), lambda x: x.group().replace('-', ' '), model)
            model = re.sub(re.compile(r'\w+-Frame|Cat-Eye', re.IGNORECASE),
                           lambda x: x.group().replace('-', ' '), model)
            model = re.sub(r'\s+', ' ', model).strip()

            materials = []
            for material in record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    if material not in materials:
                        materials.append(material)

            material = ','.join(materials)

            frame_shape = record.get("Frame Shape", "")
            if not frame_shape:
                frame_shapes = ['Aviator', 'Cat Eye', 'Oversized', 'Round', 'Square',
                                'Rectangular', 'Oval', 'Diamond', 'Butterfly', 'Heart', 'Navigator', 'Mask']
                try:
                    match = re.findall(re.compile(r'|'.join(
                        list(map(lambda x: r'\b'+x+r'\b', frame_shapes))), re.IGNORECASE), model)
                    if match:
                        frame_shape = match[0]
                except IndexError:
                    frame_shape = ''

            lens_type = record.get('Lenses', '')
            if not lens_type:
                lenses = ['Gradient', 'Mirrored', 'Tinted']
                try:
                    lens_type = [lens for lens in lenses if lens in model][0]
                except IndexError:
                    lens_type = ''

            size_unit = record.get('size_unit', '')
            sizes = ','.join(
                [size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ','.join([color.strip()
                               for color in record.get('Colour', '').split(',')])
            colors = re.sub(re.compile(
                r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = colors.title()

            if not frame_shape:
                description = record.get('description', '')
                frame_shapes = ['Aviator', 'Cat Eye', 'Oversized', 'Round', 'Square', 'Rectangular',
                                'Oval', 'Diamond', 'Butterfly', 'Heart', 'Navigator', 'Mask', 'Double']
                for frame in frame_shapes:
                    if re.findall(re.compile(r"{}\s*\-*(frames*|framed)".format(frame), re.IGNORECASE), description):
                        frame_shape = frame
                        break

            if not lens_type:
                lenses = ['Gradient', 'Mirrored', 'Tinted', 'Reflection']
                description = record.get('description', '')
                for lens in lenses:
                    if re.findall(re.compile(r"{}\s*lenses*".format(lens), re.IGNORECASE), description) or re.findall(re.compile(r"\b{}\b".format(lens), re.IGNORECASE), description):
                        lens_type = lens
                        break

                if lens_type == "Reflection":
                    lens_type = "Reflective"

            if not colors:
                if id == 'https://www.farfetch.com/shopping/women/gucci-eyewear-oversized-acetate-glitter-sunglasses-item-12143288.aspx?storeid=9423':
                    colors = 'Pink'
                elif id == 'https://www.farfetch.com/shopping/women/gucci-eyewear-oversized-frame-tinted-sunglasses-item-13473188.aspx?storeid=9945':
                    colors = 'Red/Black Tortoiseshell'

            for cl in re.findall(r"\w+", colors)+[r'Tortoiseshell\-*\s*Effect', 'Tortoiseshell']:
                model = re.sub(re.compile(
                    r"\b{}\b".format(cl), re.IGNORECASE), "", model)

            colors = re.sub(re.compile(
                r"\bMulti\-*\s*colour\b|multi\-*\s*color", re.IGNORECASE), "Multicolor", colors)

            model = re.sub(re.compile(r"\-*\s*effect\b",
                                      re.IGNORECASE), " effect", model)
            model = re.sub(re.compile(r"(.*)(Sunglasses)(.*)",
                                      re.IGNORECASE), r"\1 \3 \2", model)

            model = re.sub(re.compile(
                r"frames|framed", re.IGNORECASE), "frame", model)
            frame_shapes = ['Aviator', 'Cat Eye', 'Oversized', 'Round', 'Square', 'Rectangular',
                            'Oval', 'Diamond', 'Butterfly', 'Heart', 'Navigator', 'Mask', 'Double']
            if not re.findall(re.compile(r"\bframes*\b", re.IGNORECASE), model):
                for frame in frame_shapes:
                    if re.findall(re.compile(r"\b{}\b".format(frame), re.IGNORECASE), model):
                        model = re.sub(re.compile(
                            r"(\b{}\b)(.*)".format(frame), re.IGNORECASE), r"\1 Frame \2", model)
                        model = re.sub(re.compile(
                            r"(Frame)\s*\-*(shaped\b|shape\b)", re.IGNORECASE), r"\1 ", model)

            if not re.findall(re.compile(r"\bframes*\b", re.IGNORECASE), model):
                model = re.sub(re.compile(
                    r"(.*)(sunglasses)", re.IGNORECASE), r"\1 {} Frame \2".format(frame_shape), model)

            if not re.findall(re.compile(r"\b{}\b".format(lens_type), re.IGNORECASE), model):
                model = re.sub(re.compile(r"(.*)(Frame)(.*)(sunglasses)",
                                          re.IGNORECASE), r"\1\2 {} \3\4".format(lens_type), model)
            else:
                model = re.sub(re.compile(r"\b{}\b".format(
                    lens_type), re.IGNORECASE), "",  model)
                model = re.sub(re.compile(r"(.*)(Frame)(.*)(sunglasses)",
                                          re.IGNORECASE), r"\1\2 {} \3\4".format(lens_type), model)

            model = re.sub(r'-style\b|-embellished\b|Embellished|-sized\b|\bGucci\b|\bUnisex\b|\bS\b|', '', model, flags=re.IGNORECASE)
            model = re.sub(r"\bEye-frame\b", 'Eye Frame', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r"\bd\b", re.IGNORECASE), "", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(r'\bGlasses\b', 'Sunglasses', model, flags=re.IGNORECASE)
            if not re.findall(r'\bSunglasses\b', model, flags=re.IGNORECASE):
                model += ' Sunglasses'
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "image": record.get("image", ""),
                "title": record.get('title', ''),
                "frame_shape": frame_shape,
                "lens_type": lens_type,
                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "material": material,
                'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            return transformed_record
        else:
            return None
