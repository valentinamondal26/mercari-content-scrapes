class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''
        material = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "material" == entity['name']:
                if entity["attribute"]:
                    for attribute in entity["attribute"]:
                        material = material + attribute["id"]

        key_field = model + color + material
        return key_field
