import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()
            model = re.sub(re.compile(r'Mcgraw', re.IGNORECASE), 'McGraw', model)

            materials = []
            # for material in record.get('composition', []):
            for material in list(filter(lambda x: x.get('material', '') == "Outer", record.get('composition', []))) or record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    if material not in materials:
                        material = re.sub(re.compile(r'\(.*\)', re.IGNORECASE), '', material)
                        materials.append(material)
                
            if not materials:
                meta_materials = ['Bovine Leather', 'Calf Leather', 'Polyurethane', 'Lamb Skin', 'Nylon', 'Lamb Nubuck Leather', 'Straw', 'Goat Skin', 'Leather']
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', meta_materials))), re.IGNORECASE), record.get('description', ''))
                if match:
                    materials = list(set(match))

            materials = [material.strip() for material in materials]
            material = ','.join(sorted(materials))
            material = material.title()
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ','.join(list(filter(lambda x: x != 'Leather', material.split(','))))
            material = re.sub(re.compile(r'Lambskin', re.IGNORECASE), 'Lamb Skin', material)
            material = re.sub(r'\s+', ' ', material).strip()

            # size_unit = record.get('size_unit', '')
            # sizes = ','.join([size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ','.join([color.strip() for color in record.get('Colour', '').split(',')])
            colors = re.sub(re.compile(r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = colors.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "image":record.get("image",""),
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "material": material,
                # 'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            return transformed_record
        else:
            return None
