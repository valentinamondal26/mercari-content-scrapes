import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()
            d = {
                'Cross-Body': 'Crossbody',
                'Chevron-Quilted': 'Chevron Quilted',
                'Floral-Print': 'Floral Print',
                'Chain-Print': 'Chain Print',
                'Metal-Logo': 'Metal Logo',
                'Map-Print': 'Map Print',
                'Logo-Plaque': 'Logo Plaque',
                'Mcgraw': 'McGraw',
            }
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', d.keys()))), re.IGNORECASE), lambda x: d.get(x.group(), ''), model)
            model = re.sub(re.compile(r'Contrasting Tag Crossbody Bag', re.IGNORECASE), 'Contrast Tag Crossbody Bag', model)
            model = re.sub(re.compile(r"Cross\s*\-*Body", re.IGNORECASE),"Crossbody", model)
            model = re.sub(r'\s+', ' ', model).strip()

            materials = []
            for material in list(filter(lambda x: x.get('material', '') == "Outer", record.get('composition', []))):
            # for material in record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    if material not in materials:
                        material = re.sub(re.compile(r'\(top grain\)', re.IGNORECASE), '', material)
                        materials.append(material)
                
            materials = [material.strip() for material in materials]
            material = ', '.join(sorted(materials))
            material = material.title()
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ', '.join(list(filter(lambda x: x != 'Leather', material.split(', '))))
            material = material.replace('Pvc', 'PVC')
            material = re.sub(r'\s+', ' ', material).strip()

            # size_unit = record.get('size_unit', '')
            # sizes = ','.join([size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ', '.join([color.strip() for color in record.get('Colour', '').split(',')])
            colors = re.sub(re.compile(r'NEUTRALS', re.IGNORECASE), 'NEUTRAL', colors)
            colors = colors.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "image":record.get("image",""),
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "material": material,
                # 'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }
            if "Satchel" in model:
                return None
            else:
                return transformed_record
        else:
            return None
