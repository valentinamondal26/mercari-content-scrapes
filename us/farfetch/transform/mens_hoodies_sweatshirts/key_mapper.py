class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name'] and entity["attribute"]:
                color = entity["attribute"][0]["id"]

        key_field = color + model
        return key_field
