import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            # model = re.sub(re.compile(r'\bHoodie\b|\bsweatshirt\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'^x\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()
            title_case_exception_keywords = ['gt', 'tnf', 'udc', 'jpg', r'ss\d+']
            model = re.sub(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b',
                title_case_exception_keywords))), re.IGNORECASE),
                lambda x: x.group().upper(), model)
            model = re.sub(re.compile('Logohoodie', re.IGNORECASE), 'Logo Hoodie', model)

            materials = []
            for material in record.get('composition', []):
                material = material.get('value', '')
                material = re.sub(r'\d+%', '', material).strip()
                if not re.findall(re.compile('metal', re.IGNORECASE), material):
                    materials.append(material)
            material = ', '.join(dict.fromkeys(materials))

            size_unit = record.get('size_unit', '')
            sizes = ','.join([size + ' ' + size_unit if size_unit else size for size in record.get('sizes', '')])

            colors = ', '.join([color.strip() for color in record.get('colors', '').split(',')])
            colors = re.sub(re.compile(r'\bMulticolour\b', re.IGNORECASE), 'Multicolor', colors)
            if re.findall(re.compile(r"\bpants*\b", re.IGNORECASE), record.get("title")):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                "color": colors,
                "neckline": record.get('Knit Neckline', ''),
                "material": material,
                'size': sizes,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
