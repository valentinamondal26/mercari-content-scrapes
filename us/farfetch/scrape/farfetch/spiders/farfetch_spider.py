'''
https://mercari.atlassian.net/browse/USDE-727 - Gucci Men's Belts
https://mercari.atlassian.net/browse/USDE-752 - Supreme Men's Hoodies & Sweatshirts
https://mercari.atlassian.net/browse/USDE-1063 - Tory Burch Sandals
https://mercari.atlassian.net/browse/USDE-1064 - Tory Burch Women's Flats
https://mercari.atlassian.net/browse/USDE-1065 - Tory Burch Shoulder Bags
https://mercari.atlassian.net/browse/USDE-1066 - Tory Burch Crossbody Bags
https://mercari.atlassian.net/browse/USDE-1067 - Tory Burch Tote Bags
https://mercari.atlassian.net/browse/USDE-1068 - Gucci Sandals
https://mercari.atlassian.net/browse/USDE-1069 - Gucci Sunglasses
https://mercari.atlassian.net/browse/USDE-1070 - UGG Australia Women's Boots
'''
import os
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider

from urllib.parse import urlparse, parse_qs, urlencode
import re
import json

class FarfetchSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from farfetch.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Belts
        2) Men's Hoodies & Sweatshirts
        3) Sandals
        4) Women's Flats
        5) Shoulder Bags
        6) Crossbody Bags
        7) Tote Bags
        8) Sunglasses
        9) Women's Boots

    brand : str
        brand to be crawled, Supports
        1) Gucci
        2) Supreme
        3) Tory Burch
        4) UGG Australia

    Command e.g:
    scrapy crawl farfetch -a category="Men's Belts" -a brand='Gucci'
    scrapy crawl farfetch -a category="Men's Hoodies & Sweatshirts" -a brand="Supreme"

    scrapy crawl farfetch -a category="Sandals" -a brand="Tory Burch"
    scrapy crawl farfetch -a category="Women's Flats" -a brand="Tory Burch"
    scrapy crawl farfetch -a category="Shoulder Bags" -a brand="Tory Burch"
    scrapy crawl farfetch -a category="Crossbody Bags" -a brand="Tory Burch"
    scrapy crawl farfetch -a category="Tote Bags" -a brand="Tory Burch"
    scrapy crawl farfetch -a category="Sandals" -a brand="Gucci"
    scrapy crawl farfetch -a category="Sunglasses" -a brand="Gucci"
    scrapy crawl farfetch -a category="Women's Boots" -a brand="UGG Australia"
    """

    name = 'farfetch'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'farfetch.com'
    blob_name = 'farfetch.txt'

    url_dict = {
        "Men's Belts": {
            'Gucci': {
                'url': 'https://www.farfetch.com/shopping/men/gucci/belts-2/items.aspx',
                'filters': ['Colour'],
            }
        },
        "Men's Hoodies & Sweatshirts": {
            'Supreme': {
                'url': 'https://www.farfetch.com/shopping/men/supreme/hoodies-2/items.aspx',
                'filters': ['Knit Neckline', 'Colour'],
            }
        },
        "Sandals": {
            "Tory Burch": {
                'url': "https://www.farfetch.com/shopping/women/tory-burch/items.aspx?view=180&sort=4&scale=280&category=136308",
                "filters": ['Colour']
            },
            "Gucci": {
                "url": "https://www.farfetch.com/shopping/women/sandals-1/items.aspx?view=180&scale=280&designer=25354",
                "filters": ["Colour","Detail","Heel Height"]
            }
        },
        "Women's Flats": {
            "Tory Burch": {
                "url": "https://www.farfetch.com/shopping/women/tory-burch/shoes-1/items.aspx?view=180&scale=280&attributes:17=57",
                "filters": ["Colour","Detail","Category"]
            }
        },
        "Shoulder Bags": {
            "Tory Burch": {
                "url": "https://www.farfetch.com/shopping/women/shoulder-bags-1/items.aspx?view=180&designer=18286",
                "filters": ["Colour"]
            }
        },
        "Crossbody Bags": {
            "Tory Burch": {
                "url": "https://www.farfetch.com/shopping/women/satchel-cross-body-bags-1/items.aspx?view=180&designer=18286",
                "filters": ["Colour"]
            }
        },
        "Tote Bags": {
            "Tory Burch": {
                "url": "https://www.farfetch.com/shopping/women/totes-1/items.aspx?view=180&designer=18286",
                "filters": ["Colour"]
            }
        },
        "Sunglasses": {
            "Gucci": {
                "url": "https://www.farfetch.com/shopping/women/sunglasses-1/items.aspx?view=180&designer=3494667",
                "filters": ["Colour","Frame Shape","Lenses"]
            }
        },
        "Women's Boots": {
            "UGG Australia": {
                "url": "https://www.farfetch.com/shopping/women/designer-ugg-australia/items.aspx?view=180&scale=280&category=136302",
                "filters": ["Colour","Heel Height","Detail"]
            }
        },
    }

    base_url = 'https://www.farfetch.com'

    filter_list = []

    extra_item_infos = {}

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(FarfetchSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filter_list = ["Colour","Heel Height","Detail"]


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_filter)


    def crawlDetail(self, response):
        """
        @url https://www.farfetch.com/in/shopping/women/ugg-shearling-lined-lace-up-boots-item-14572716.aspx?storeid=11165
        @meta {"use_proxy":"True"}
        @scrape_values image title price composition description title colors model_sku breadcrumb sizes size_unit
        """

        script = response.xpath('//script[contains(text(), "window[\'__initialState_slice-pdp__\'] =")]').extract_first(default='')
        script = response.xpath('//script[contains(text(), "window[\'__initialState_slice-pdp__\']")]').extract_first(default='')
        match = re.findall(re.compile(r"window\[\'__initialState_slice-pdp__\'\] = (.*)</script>"), script)
        if match:
            data = json.loads(match[0])
            try:
                image = data.get('productViewModel', {}).get('images', {}).get("main",[])[0].get("large","")
            except IndexError:
                image =''

            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': response.url,
                "image":image,
                'price': data.get('productViewModel', {}).get('priceInfo', {}).get('default', {}).get('formattedFinalPrice', ''),
                'composition': data.get('productViewModel', {}).get('composition', ''),
                'description': data.get('productViewModel', {}).get('details', {}).get('description', ''),
                'title': data.get('productViewModel', {}).get('details', {}).get('shortDescription', ''),
                'colors': data.get('productViewModel', {}).get('details', {}).get('colors', ''),
                'model_sku': data.get('productViewModel', {}).get('designerDetails', {}).get("designerStyleId",""),
                'breadcrumb': '|'.join([b.get('text', '') for b in data.get('productViewModel', {}).get('breadcrumb', '')]),
                'sizes': [s.get('description', '') for s in data.get('productViewModel', {}).get('sizes', {}).get('available', {}).values()],
                'size_unit': data.get('productViewModel', {}).get('sizes', {}).get('friendlyScaleName', ''),
            }


    def parse_filter(self, response):
        """
        @url https://www.farfetch.com/shopping/women/designer-ugg-australia/items.aspx?view=180&scale=280&category=136302
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        script = response.xpath('//script[contains(text(), "window[\'__initialState_portal-slices-listing__\'] = ")]').extract_first(default='')
        match = re.findall(re.compile(r"window\['__initialState_portal-slices-listing__'\] = (.*)</script>"), script)
        if match:
            data = json.loads(match[0])

            total_items = data.get('listing', {}).get('totalItems', '')
            total_pages = data.get('listing', {}).get('totalPages', '')
            print(f'total_items:{total_items}, total_pages:{total_pages}')

            #filter
            for key, value in data.get('listing', {}).get('facets', {}).items():
                #print(key, value)
                if value and value.get('description', '') in self.filter_list:
                    # print(f"--{key}, {value.get('description', '')}--")
                    for _filter in value.get('values', []):
                        # print(color['value'], color['description'], color['count'])

                        parts = urlparse(response.url)
                        query_dict = parse_qs(parts.query)
                        query_dict.update({key:[_filter.get('value', '')]})
                        url = parts._replace(query=urlencode(query_dict, True)).geturl()

                        request = self.create_request(url=url, callback=self.parse)
                        request.meta['extra_item_info'] = {
                            value.get('description', ''): _filter.get('description', '')
                        }
                        yield request

        # yield self.create_request(url=id, callback=self.parse, dont_filter=True)


    def parse(self, response):
        """
        @url https://www.farfetch.com/in/shopping/women/designer-ugg-australia/items.aspx?view=180&scale=280&category=136302&colour=5
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        extra_item_info = response.meta.get('extra_item_info', {})

        script = response.xpath('//script[contains(text(), "window[\'__initialState_portal-slices-listing__\'] = ")]').extract_first(default='')
        match = re.findall(re.compile(r"window\['__initialState_portal-slices-listing__'\] = (.*)</script>"), script)
        if match:
            data = json.loads(match[0])
            for product in data.get('listing', {}).get('products', []):
                id = product.get('url', '')
                if id:
                    id = self.base_url + id
                    if not self.extra_item_infos.get(id, None):
                        self.extra_item_infos[id] = {}

                    self.extra_item_infos.get(id).update(extra_item_info)
                    yield self.create_request(url=id, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
