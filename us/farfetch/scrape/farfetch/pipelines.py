# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from io import BytesIO
from datetime import datetime
from google.cloud import storage
from scrapy.exporters import JsonLinesItemExporter
import re


class GCSPipeline(object):

    def __init__(self, gcs_blob_prefix):
        self.gcs_blob_prefix = gcs_blob_prefix
        self.bucket_name = 'content_us'
        self.client = None
        self.bucket = None
        self.items = []

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            gcs_blob_prefix=crawler.settings.get('GCS_BLOB_PREFIX'),
        )

    def open_spider(self, spider):
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(self.bucket_name)

    def close_spider(self, spider):
        category = spider.category
        brand = spider.brand

        if not category or not brand:
            spider.logger.error('category or brand is empty, so skip uploading result to gcs')
            return

        extra_item_infos = getattr(spider, 'extra_item_infos', {})
        for item in self.items:
            item.update(extra_item_infos.get(item.get('id', ''), {}))

        try:
            with open('result.jl', 'w+') as f:
                import json
                j = json.dumps(extra_item_infos)
                # print(j)
                f.write(j)
        except Exception as e:
            print('Error: writing result file: {}'.format(e))

        source = spider.source
        timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        filename = spider.blob_name
        destination_blob_name = self.gcs_blob_prefix.format(category=category,
            brand=brand, source=source, timestamp=timestamp, filename=filename)
        destination_blob_name = re.sub(re.compile(r"['&,]"), '', destination_blob_name)
        destination_blob_name = re.sub(r'\s+', '_', destination_blob_name).strip().lower()

        blob = self.bucket.blob(destination_blob_name)
        f = self._make_fileobj()
        blob.upload_from_file(f)

        spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
            bucket=self.bucket_name, blob=destination_blob_name))


    def process_item(self, item, spider):
        self.items.append(item)

        return item

    def _make_fileobj(self):
        """
        Build file object from items.
        """

        bio = BytesIO()
        f = bio

        # Build file object using ItemExporter
        exporter = JsonLinesItemExporter(f)
        exporter.start_exporting()
        for item in self.items:
            exporter.export_item(item)
        exporter.finish_exporting()

        # Seek to the top of file to be read later
        bio.seek(0)

        return bio