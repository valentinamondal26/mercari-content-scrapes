'''
https://mercari.atlassian.net/browse/USCC-310 - TaylorMade Men's Golf Clubs
'''

import logging
import scrapy
import json
import os
from datetime import datetime
import ast
import re
from scrapy.exceptions import CloseSpider
import itertools


class TaylormadegolfSpider(scrapy.Spider):


    """
    spider to crawl items in a provided category and sub-category from taylormadegolf.com

    Attributes

    category : str
        category to be crawled, Supports
        1) Men's Golf Clubs


    brand : str
    brand to be crawled, Supports
        1) TaylorMade

    Command e.g:
    scrapy crawl taylormade  -a category="Men's Golf Clubs" -a brand='TaylorMade'

    """

    name = 'taylormade'

    category = None
    brand = None

    merge_key = 'id'

    source = 'taylormadegolf.com'
    blob_name = 'taylormadegolf.txt'

    launch_url = None
    filters = None

    base_url = 'https://www.taylormadegolf.com'
    spider_project_name = 'taylormadegolf'
    gcs_cache_file_name = 'taylormadegolf.tar.gz'

    url_dict = {
        "Men's Golf Clubs": {
            "TaylorMade": {
                "url": 'https://www.taylormadegolf.com/taylormade-clubs/?lang=default',
                "filter": ['Color']
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TaylormadegolfSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filter', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Color']
        self.round_robin = itertools.cycle(self.proxy_pool) 


    def start_requests(self):
        meta={}
        meta["start"] = 0
        if self.launch_url:

            pattern = re.compile(r'.com/(.*)/?lang=')
            try:
                category_id = re.findall(pattern,self.launch_url)[0].replace('/?', '').strip()
            except IndexError:
                category_id = ''

            meta.update({"category_id": category_id})
            if category_id:
                listing_page_api = self.get_listing_page_api(meta)
                request = self.createRequest(url=listing_page_api,callback = self.parse_listing_page,meta = meta)
                yield request
            else:
                logging.debug("No category id")


    def parse_listing_page(self,response):
        """
        @url https://www.taylormadegolf.com/on/demandware.store/Sites-TMaG-Site/default/Search-LoadMoreProducts?cgid=taylormade-clubs&start=0&sz=40&format=page-element
        @returns requests 2
        @meta {"use_proxy":"True","start":0,"category_id":"taylormade-clubs"}
        """
        logging.debug(response.url)
        meta= response.meta
        product_urls = response.xpath("//a[@class='name-link']/@href").getall()
        for product_url in product_urls:
            if self.base_url not in product_url:
                product_url = self.base_url + product_url
            request = self.createRequest(product_url,callback = self.crawldetails,meta = meta)
            yield request

        if not meta.get("filter",""):
            if  product_urls:
                meta["start"] += 40
                listing_page_api = self.get_listing_page_api(meta)
                request = self.createRequest(url=listing_page_api,callback = self.parse_listing_page,meta = meta)
                yield request
            else:
                request = self.createRequest(url = self.launch_url,callback = self.parse_filter_page,meta=meta)
                yield request
                # logging.debug("No data in the last api,All items are crawled")


    def parse_filter_page(self,response):
        """
        @url https://www.taylormadegolf.com/taylormade-clubs/?lang=default
        @returns requests 1
        @meta {"use_proxy":"True"} 
        """

        meta = response.meta
        for filter in self.filters:
            if filter == "Color":
                meta.update({"filter":filter})
                filter_names = response.xpath("//ul[@class= 'clearfix swatches refinementColor']/li/a/@title").getall()
                filter_urls  = response.xpath("//ul[@class= 'clearfix swatches refinementColor']/li/a/@href").getall()
                for name,url in zip(filter_names,filter_urls):
                    name = name.replace("Refine by Color:","").strip()
                    meta.update({meta["filter"]: name})
                    request = self.createRequest(url = url,callback = self.parse_listing_page,meta=meta)
                    yield request


    def get_listing_page_api(self,meta):
        listing_page_api = 'https://www.taylormadegolf.com/on/demandware.store/Sites-TMaG-Site/default/Search-LoadMoreProducts?cgid={category_id}&start={start}&sz=40&format=page-element'.format(category_id=meta["category_id"],start=meta["start"])
        return listing_page_api


    def crawldetails(self,response):
        """
        @url https://www.taylormadegolf.com/SIM-Max-Rescue/DW-JJI25.html?lang=default&cgid=taylormade-clubs#lang=default&start=10&
        @scrape_values title image description hand stock_shaft stock_grip specification loft_type shaft_type specs_data_title price sku
        @meta {"use_proxy":"True"}
        """
        meta = response.meta
        title = response.xpath("//h1[@class='product-name']/text()").get(default = '')
        if  not title:
            title = response.xpath("//title/text()").get(default="").split("|")[0].strip()

        price = response.xpath("//span[@class='price-sales']/text()").get(default = '')

        script_data = response.xpath("//script[contains(text(),'TM_PRODUCT_TRACK')]/text()").get(default='')
        if script_data:
            script_data = script_data.split("(window)")[0].split("TM_PRODUCT_TRACK = ")[1]
            script_data = script_data.replace("\n","").replace(".toLowerCase()","").replace("})","").replace('false',"'false'")
            basic_data = ast.literal_eval(re.search('({.+})', script_data).group(0))
        else:
            basic_data = {}
        if not price :
            price = basic_data.get("price","")
        if not title:
            title = basic_data.get("name","")
        sku = basic_data.get("sku","")
        if not sku:
           sku =  response.xpath("//span[@itemprop='productID']/text()").get(default='').strip()
        image = response.xpath("//ul[@class='all-primary-images']/li/a/@href").get(default='')

        if not image:
            try:
                id = re.findall(r'(-\w+)',sku)[0].replace("-","")
            except IndexError:
                id = ''
            if id:
                image = "https://www.taylormadegolf.com/on/demandware.static/-/Sites-tmag-master-catalog/default/v1589789435838/zoom/{id}_zoom_D.jpg".format(id=id)

        if self.base_url not in image:
            image = self.base_url+image
        desc1 = response.xpath("//div[@class='uikit-col-12 uikit-col-lg-8 cms_product_technology']/p/text()").getall()
        desc2 = response.xpath("//div[@class='uikit-col-12 uikit-col-lg-8 cms_product_technology']/ul/li/text()").getall()
        desc3 = response.xpath("//div[@class='standard-description']/p/text()").getall()
        desc4 = response.xpath("//p[@style='margin:0in 0in 8pt']/span/span/span/text()").getall()
        desc5 = response.xpath("//div[@class='standard-description']/p/span/span/span/span/span/text()").getall()
        desc6 = response.xpath("//span[@style='font-family:Calibri,sans-serif']/text()").getall()
        desc_head = response.xpath("//div[@class='uikit-col-12 uikit-col-lg-8 cms_product_technology']/ul[@class='cms_product_list']/li/h4")
        desc_text = response.xpath("//div[@class='uikit-col-12 uikit-col-lg-8 cms_product_technology']/ul[@class='cms_product_list']/li/p")
        desc = []
        for heading,text_data in zip(desc_head,desc_text):
            heading = heading.xpath('.//text()').get(default = '')
            text_data = text_data.xpath('.//text()').get(default = '')
            c=heading+"::"+text_data
            desc.append(c)

        description = desc1+desc2+desc3+desc4+desc5+desc6+desc

        loft_type = response.xpath("//select[@id='va-tm_enrich_loft']/option/text()").getall()
        loft_type = [i.strip() for i in loft_type if i.strip()!="Select"]
        loft_type = ",".join(loft_type)

        shaft_type = response.xpath("//select[@id='va-tm_enrich_shaft']/option/text()").getall()
        shaft_type = [i.strip() for i in shaft_type if i.strip()!="Select"]
        shaft_type = ",".join(shaft_type)

        # specs_key = response.xpath("//div[@class='pdp-spec-table__wrap']/table/thead/tr/th/text()").getall()
        key_response =response.xpath("//div[@class='pdp-spec-table__wrap']/table/thead/tr/th")
        specs_key = []
        if key_response:
            [specs_key.append(key.xpath(".//text()").get()) for key in key_response]

        values_response = response.xpath("//div[@class='pdp-spec-table__wrap']/table/tbody/tr/td")
        specs_values = []
        if not values_response:
            values_response =response.xpath("//div[@class='pdp-spec-table__wrap']/table/thead/tr/td")
        if values_response:
            [specs_values.append(values.xpath(".//text()").get()) for values in values_response]

        specs = {}
        if specs_key:
            total_sets = int(len(specs_values)/len(specs_key))
            key_len = len(specs_key)
            for i in range(0,len(specs_key)):
                value = []
                k=i
                for j in range (0,total_sets):
                    value.append(specs_values[k])
                    k+=key_len
                specs.update({specs_key[i]:value})

        # stock_shaft_and_grip = response.xpath("//div[@class='js-specs-data']/@data-json").getall()

        stock_shaft = response.xpath("//div[@class='js-specs']/img[contains(@src,'Shaft')]/following-sibling::div/@data-json").getall()
        for i in range(0,len(stock_shaft)):
            stock_shaft[i] = json.loads(json.loads(stock_shaft[i]))

        stock_grip = response.xpath("//div[@class='js-specs']/img[contains(@src,'Grip')]/following-sibling::div/@data-json").getall()
        for i in range(0,len(stock_grip)):
            stock_grip[i] = json.loads(json.loads(stock_grip[i]))

        specs_data_title = response.xpath("//h3[@class='js-spec-headline']/span/text()").getall()
        hands = response.xpath("//div[@class='uikit-form-group uikit-form-radio']/label/input/@id").getall()
        if len(hands) == 1:
            hands.append('')
        elif len(hands) == 0:
            hands = ['', '']

        if "left" in hands[0] or "left" in hands[1]:
            hand = "Left Handed"
        elif  "right" in hands[0] or "right" in hands[1]:
            hand = "Right Handed"
        else:
            hand = ""
        hands = ",".join(hands)
        if "left" and "right" in hands:
            hand = "Left Handed, Right Handed"

        data = {
            "id": response.url,
            "status_code": str(response.status),
            "crawl_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "brand": self.brand,
            "category": self.category,

            "title": title,
            "image": image,
            "description": description,
            "hand": hand,
            # "stock_shaft_and_grip": stock_shaft_and_grip,
            'stock_shaft': stock_shaft,
            'stock_grip': stock_grip,
            "specification": specs,
            "loft_type": loft_type,
            "shaft_type": shaft_type,
            "specs_data_title": specs_data_title,
            "price": price,
            "sku": sku,
        }

        filter_name = meta.get('filter', '')
        if filter_name:
            meta_data =  meta_data.fromkeys([filter_name],meta[filter_name])
            data.update(meta_data)
        yield data


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback = callback, meta=meta)
        if self.proxy_pool:
           request.meta['proxy'] =  next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request


