import hashlib
import re
import pandas as pd

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title','')

            model = re.sub(re.compile(r'\"|\-\s*Type\b',re.IGNORECASE), '', title)
            model =  re.sub(re.compile(r"Hi\-*\s*Toe", re.IGNORECASE), "HI-Toe", model)
            model = " ".join(model.split())

            price =  record.get('price','')
            price =  str(record.get('price','')).replace('$','')

            description = record.get('description','')
            for i  in range(0,len(description)): 
                description[i] = description[i].encode('ascii', 'ignore').decode('unicode_escape').strip() 

            description = ', '.join(description)
            color =  record.get('Color','')
            shaft_type =  record.get('shaft_type','')
            specs = record.get('specification',{})
            specification_keys = list(specs.keys())
            specification_keys_lower = [key.lower() for key in specification_keys] 
            transformed_specs={}
            
            try:
                requirements = specs.get(specification_keys[0],'')          
                # spec_keys = specification_keys[0:]
            except IndexError:
                ''

            if specification_keys:
                if '3 wood' in specification_keys_lower or  '5 wood' in specification_keys_lower or '5 rescue' in specification_keys_lower or '6 rescue' in specification_keys_lower or '7' in specification_keys_lower or '8' in specification_keys_lower or '9' in specification_keys_lower or 'pw' in specification_keys_lower or 'sw' in specification_keys_lower or 'spider mini' in specification_keys_lower:
                    transformed_spec1 = {}
                    for i in range(0,len(requirements)):
                        values = []
                        for keys in specification_keys:
                            values.append(specs[keys][i])
                        if requirements[i] in values:
                            values.remove(requirements[i])
                        transformed_spec1.update({requirements[i].lower():values})
                    transformed_spec2 = {}
                    for spec in specification_keys:
                        for req,i in zip(requirements,range(0,len(requirements))):
                            key_name = spec+'_'+req.lower()
                            key_name = key_name.replace(' ','_').lower().replace('_(steel/graphite)','').strip(u'\xa0').replace('_(steel_&_graphite)','')
                            if "loft" in key_name:
                                key_name =re.findall(r".*loft", key_name)[0]
                                # key_name = key_name.replace('stock','').replace('__','_')
                            elif "lie" in key_name:
                                key_name =re.findall(r".*lie", key_name)[0]
                            elif "length" in key_name:
                                key_name =re.findall(r".*length", key_name)[0]
                                # key_name = key_name.replace('stock','').replace('__','_')
                            elif "swing_weight" in key_name or 'swingweight' in key_name:
                                if 'swingweight' in key_name:
                                    number = re.findall(r'\d+',key_name)
                                    if number:
                                        number = number[0]
                                        key_name = number +"_"+ "swing_weight"
                                else:
                                    key_name =re.findall(r".*swing_weight", key_name)[0]
                            values = specs[spec][i]
                            if values != spec:
                                key_name = key_name.replace('stock','').replace('__','_')
                                
                                transformed_spec2.update({key_name:values})
                    transformed_specs.update(transformed_spec1)
                    transformed_specs.update(transformed_spec2)
                else:
                    for key in specification_keys:
                        transformed_specs.update({key.lower().replace(" ","_"):specs[key]})

            wood_loft_3 = transformed_specs.get('3_wood_loft','')
            wood_lie_3 = transformed_specs.get('3_wood_lie','')
            wood_length_3 = transformed_specs.get('3_wood_length','')
            wood_swing_weight_3 = transformed_specs.get('3_wood_swing_weight','')

            stock_shaft_and_grip=record.get('stock_shaft_and_grip','') 
            stock_shaft_and_grip_specs = {}
            for i in range(0,len(stock_shaft_and_grip)):
                df=pd.DataFrame(data=stock_shaft_and_grip[i][1:],columns=stock_shaft_and_grip[i][0]) 
                column_names = df.columns.values.tolist()  
                df.dropna(subset = [column_names[0]], inplace=True)
                for column in column_names:
                    values = df[column].tolist()
                    stock_shaft_and_grip_specs.update({column:values})
            
            def get_stock_shaft_and_grip_specs(df):
                column_names = df.columns.values.tolist()   
                df.dropna(subset = [column_names[0]], inplace=True)
                for column in column_names:
                    values = df[column].tolist()
                    available_values = stock_shaft_and_grip_specs.get(spec_title.strip(),{}).get(column.lower(),[])
                    main_key =stock_shaft_and_grip_specs.get(spec_title.strip(),{})
                    if  available_values:
                        new_values = available_values+values
                        # new_values = list(set(new_values))                          
                        main_key.update({column.lower():new_values})    
                    else:
                        # values = list(set(values))
                        main_key.update({column.lower():values})
                    stock_shaft_and_grip_specs.update({spec_title:main_key})
    
            stock_shaft = record['stock_shaft']
            stock_grip =  record['stock_grip']
            stock_shaft_and_grip_specs= {}
            stock_shaft_and_grip_specs.update({'stock_shaft':{}})
            stock_shaft_and_grip_specs.update({'stock_grip':{}})

            specs_data_title = ['stock_shaft','stock_grip']            
            for spec_title in specs_data_title:
                for i in range(0,len(record[spec_title])):
                    if spec_title == 'stock_shaft':
                        df=pd.DataFrame(data=stock_shaft[i][1:],columns=stock_shaft[i][0])
                        get_stock_shaft_and_grip_specs(df)
                    elif spec_title =='stock_grip':
                        df=pd.DataFrame(data=stock_grip[i][1:],columns=stock_grip[i][0])
                        get_stock_shaft_and_grip_specs(df)

            stock_grip = stock_shaft_and_grip_specs.get('stock_grip',{})
            # model_sku = ', '.join(stock_grip.get('model',''))
            grip_color = ', '.join(stock_grip.get('color',''))
            size = ', '.join(stock_grip.get('size',''))
            weight = ', '.join(stock_grip.get('weight',''))
            style = ', '.join(stock_grip.get('type',''))
            butt_size = ', '.join(stock_grip.get('butt size',''))            
            feel = ', '.join(stock_grip.get('feel',''))

            stock_shaft = stock_shaft_and_grip_specs.get('stock_shaft',{})

            flex = ', '.join(stock_shaft.get('flex',''))
            shaft_weight = ', '.join(stock_shaft.get('weight',''))
            max_torque = ', '.join(stock_shaft.get('torque',''))
            shaft_tip = ', '.join(stock_shaft.get('tip size',''))
            launch = ', '.join(stock_shaft.get('launch',''))
            shaft_spin = ', '.join(stock_shaft.get('spin',''))
            
            wood_loft_5 = transformed_specs.get('5_wood_loft','')
            wood_lie_5 = transformed_specs.get('5_wood_lie','')
            wood_length_5 = transformed_specs.get('5_wood_length','')
            wood_swing_weight_5 = transformed_specs.get('5_wood_swing_weight','')

            rescue_loft_5 = transformed_specs.get('5_rescue_loft','')
            rescue_lie_5 = transformed_specs.get('5_rescue_lie','')
            rescue_length_5 = transformed_specs.get('5_rescue_length','')
            rescue_swing_weight_5 = transformed_specs.get('5_rescue_swing_weight','')

            rescue_loft_6 = transformed_specs.get('6_rescue_loft','')
            rescue_lie_6 = transformed_specs.get('6_rescue_lie','')
            rescue_length_6 = transformed_specs.get('6_rescue_length','')
            rescue_swing_weight_6 = transformed_specs.get('6_rescue_swing_weight','')
            
            loft_7 = transformed_specs.get('7_loft','')
            lie_7 = transformed_specs.get('7_lie','')
            length_7 = transformed_specs.get('7_length','')
            if not length_7:
                length_7 = transformed_specs.get('7_steel_length','')
            swing_weight_7 = transformed_specs.get('7_swing_weight','')
            
            loft_8 = transformed_specs.get('8_loft','')
            lie_8 = transformed_specs.get('8_lie','')
            length_8 = transformed_specs.get('8_length','')
            if not length_8:
                length_8 = transformed_specs.get('8_steel_length','')
            swing_weight_8 = transformed_specs.get('8_swing_weight','')
            
            loft_9 = transformed_specs.get('9_loft','')
            lie_9 = transformed_specs.get('9_lie','')
            length_9 = transformed_specs.get('9_length','')
            if not length_9:
                length_9 = transformed_specs.get('9_steel_length','')

            pw_length = transformed_specs.get('pw_length','')
            if not pw_length:
                pw_length = transformed_specs.get('pw_steel_length','')

            sw_length = transformed_specs.get('sw_length','')
            if not sw_length:
                sw_length = transformed_specs.get('sw_steel_length','')

            swing_weight_9 = transformed_specs.get('9_swing_weight','') 

            swing_weight = ', '.join(transformed_specs.get('swing_weight',''))
            if not swing_weight:
                swing_weight = ', '.join(transformed_specs.get('swingweight', ''))

            if not record.get("hand",""):
                hand = ', '.join(transformed_specs.get('hand', ''))  
            else:
                hand = record.get('hand', '')     
            # print(transformed_specs)
            # print(record['id'])

            if re.findall(re.compile(r"Ladies|Women's|Kids|Juniors|Gift Card|\bHat\b", re.IGNORECASE), title):
                return None

            transformed_record = {
                'id':record.get('id',''),
                'item_id' : hex_dig,
                'crawl_date': record.get('crawl_date','').split(',')[0],
                'status_code':record.get('status_code',''),

                'category' : record.get('category',''),
                'brand' : record.get('brand',''),

                'title' : title,
                'model' : model,
                'image' : record.get('image',''),
                'description' : description,
                'shaft_type' : shaft_type,
                'color' : color,

                'club_type': ', '.join(transformed_specs.get('head','')),
                'loft': ', '.join(transformed_specs.get('loft','')),
                'right_lefthanded': hand,
                'head_lie': ', '.join(transformed_specs.get('lie','')),
                'volume': ', '.join(transformed_specs.get('volume','')),
                'head_length': ', '.join(transformed_specs.get('length','')),
                'swing_weight': swing_weight,

                'model_sku': record.get("sku",""),
                'grip_color': grip_color,
                'size': size,
                'weight': weight,
                'style': style,
                'butt_size': butt_size,
                'feel': feel,

                'flex': flex,
                'shaft_weight': shaft_weight,
                'max_torque': max_torque,
                'shaft_tip': shaft_tip,
                'launch': launch,
                'shaft_spin': shaft_spin,

                '3_wood_loft': wood_loft_3,
                '3_wood_lie':  wood_lie_3,
                '3_wood_length': wood_length_3,
                '3_wood_swing_weight': wood_swing_weight_3,

                '5_wood_loft': wood_loft_5,
                '5_wood_lie': wood_lie_5,
                '5_wood_length': wood_length_5,
                '5_wood_swing_weight': wood_swing_weight_5,

                '5_rescue_loft': rescue_loft_5,
                '5_rescue_lie': rescue_lie_5,
                '5_rescue_length': rescue_length_5,
                '5_rescue_swing_weight': rescue_swing_weight_5,

                '6_rescue_loft': rescue_loft_6,
                '6_rescue_lie': rescue_lie_6,
                '6_rescue_length': rescue_length_6,
                '6_rescue_swing_weight': rescue_swing_weight_6,

                '7_iron_loft': loft_7,
                '7_iron_lie': lie_7,
                '7_iron_length': length_7,
                '7_iron_swing_weight': swing_weight_7,

                '8_iron_loft': loft_8,
                '8_iron_lie': lie_8,
                '8_iron_length': length_8,
                '8_iron_swing_weight': swing_weight_8,

                '9_iron_loft': loft_9,
                '9_iron_lie': lie_9,
                '9_iron_length': length_9,
                '9_iron_swing_weight': swing_weight_9,

                'driver_loft': transformed_specs.get('driver_loft',''),
                'driver_lie': transformed_specs.get('driver_lie',''),
                'driver_length': transformed_specs.get('driver_length',''),
                'driver_swing_weight': transformed_specs.get('driver_swing_weight',''),
                
                'putting_wedge_loft': transformed_specs.get('pw_loft',''),
                'putting_wedge_lie': transformed_specs.get('pw_lie',''),
                'putting_wedge_length': pw_length,
                'putting_wedge_swing_weight': transformed_specs.get('pw_swing_weight',''),
            
                'sand_wedge_loft': transformed_specs.get('sw_loft',''),
                'sand_wedge_lie': transformed_specs.get('sw_lie',''),
                'sand_wedge_length': sw_length,
                'sand_wedge_weight': transformed_specs.get('sw_swing_weight',''),
                
                'putter_loft': transformed_specs.get('spider_mini_loft',''),
                'putter_lie': transformed_specs.get('spider_mini_lie',''),
                'putter_length': transformed_specs.get('spider_mini_length',''),
                'putter_swing_weight': transformed_specs.get('spider_mini_swing_weight',''),
            
                'msrp': {
                    'currency_code' : 'USD',
                    'amount' : price
                }
            }
            return transformed_record
        else:
            return None
