'''
https://mercari.atlassian.net/browse/USCC-674 - b.ø.c. Women's Sandals
https://mercari.atlassian.net/browse/USCC-675 - b.ø.c. Women's Shoes
https://mercari.atlassian.net/browse/USCC-676 - b.ø.c. Women's Heels
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class BocshoesSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bocshoes.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Sandals
        2) Women's Shoes
        3) Women's Heels

    brand : str
        brand to be crawled, Supports
        1) b.ø.c.

    Command e.g:
    scrapy crawl bocshoes -a category="Women's Sandals" -a brand="b.ø.c."
    scrapy crawl bocshoes -a category="Women's Shoes" -a brand="b.ø.c."
    scrapy crawl bocshoes -a category="Women's Heels" -a brand="b.ø.c."
    """

    name = 'bocshoes'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'bocshoes.com'
    blob_name = 'bocshoes.txt'

    base_url = 'https://www.bocshoes.com'

    url_dict = {
        "Women's Sandals": {
            "b.ø.c.": {
                'url': 'https://www.bocshoes.com/ViewAll/Womens/All-Seasons/Sandals/Most-Popular/All-Pages',
            }
        },
        "Women's Shoes": {
            "b.ø.c.": {
                'url': 'https://www.bocshoes.com/ViewAll/Womens/All-Seasons/Clogs/Most-Popular/All-Pages',
            }
        },
        "Women's Heels": {
            "b.ø.c.": {
                'url': 'https://www.bocshoes.com/ViewAll/Womens/All-Seasons/Heels/Most-Popular/All-Pages',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BocshoesSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.bocshoes.com/Product/BOC/Womens/Altheda/Tan/Color
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb title image price color sku description features
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//div[contains(@class, "info")]/h4//text()[normalize-space(.)]').getall(),
            'title': response.xpath(u'normalize-space(//h1/text())').get(),
            'image': response.xpath('//figure[@class="print"]/img/@src').get(),
            'price': response.xpath('//h1/span/text()').get(),
            'color': response.xpath('//h3/span/text()').get(),
            'sku': ' '.join(response.xpath('//h3/text()[normalize-space(.)]').getall()),
            'description': response.xpath('//p[@class="desc"]/text()').get(),
            'features': response.xpath('//ul[@class="bullets"]/li/text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.bocshoes.com/ViewAll/Womens/All-Seasons/Sandals/Most-Popular/All-Pages
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@id="shoes"]/ul[@class="shoes"]/li'):
            product_url = product.xpath('./a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
