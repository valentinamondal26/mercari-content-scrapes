import hashlib
import re
from colour import Color

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\s-\s|\s-|-\s', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()

            description = record.get('description', '')
            features = ', '.join(record.get('features', ''))

            sku = record.get('sku', '')
            sku = re.sub(r'\s-\s|\s-|-\s', ' ', sku)
            sku = re.sub(r'\s+', ' ', sku).strip()

            def fraction_to_decimal(fraction, decimal_count):
                from fractions import Fraction
                import unicodedata
                nums = fraction.split('/')
                if len(nums) == 2:
                    return str(round(float(Fraction(int(nums[0]), int(nums[1]))), decimal_count))[1:]
                else:
                    try:
                        return str(round(unicodedata.numeric(u'{}'.format(fraction)), decimal_count))[1:]
                    except:
                        pass
                    return fraction

            heel_height = ''
            match = re.findall(r'(?<=,\s)([^,]*)(inches|inch|") heel height', features, flags=re.IGNORECASE)
            if match:
                heel_height, _ = match[0]
                heel_height = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group(), 2), heel_height)
                heel_height += ' in.'

            color = record.get('color', '')
            colors_exclude_string = ['Canvas', 'Leather', 'Nubuck', 'Fabric', 'Tooled', 'with', 'sole', 'Insole', 'and']
            color = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x+r'\b', colors_exclude_string))), '', color, flags=re.IGNORECASE)
            color = re.sub(r'\bLt\b', 'Light', color, flags=re.IGNORECASE)
            color = re.sub(r'\bDk\b', 'Dark', color, flags=re.IGNORECASE)
            color = re.sub(r'\bLight-Blue\b', 'Light Blue', color, flags=re.IGNORECASE)
            color = re.sub(r'\bOff White\b', 'Off-White', color, flags=re.IGNORECASE)
            colors = color.split()
            new_color = []
            combo_colors = re.findall(r'\bLight \w+\b|\bPink \w+\b|\bOff-White\b|\bGold \w+\b', color, flags=re.IGNORECASE)
            if combo_colors:
                color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', combo_colors))), '', color, flags=re.IGNORECASE).strip()
                new_color.append('/'.join(combo_colors))

            new_color = '/'.join(new_color)
            modified_color = f"{new_color}/{color}"
            modified_color = re.sub(r'\s+', ' ', modified_color).strip()
            modified_color = re.sub(r'^\/|\/$', '', modified_color).strip()
            if '/' not in modified_color:
                color_data = []
                for col in modified_color.split():
                    try:
                        Color(col)
                        color_data.append(col)
                    except ValueError:
                        pass
                if len(color_data) == len(modified_color.split()):
                    modified_color = '/'.join(color_data)
            modified_color = modified_color.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description + ' ' + features,
                "image": record.get('image', ''),

                "model": model,
                "color": modified_color,
                'heel_height': heel_height,
                'model_sku': sku,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
