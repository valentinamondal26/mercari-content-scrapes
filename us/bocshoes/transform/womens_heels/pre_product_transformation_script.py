import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\s-\s|\s-|-\s', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()

            description = record.get('description', '')
            features = ', '.join(record.get('features', ''))

            sku = record.get('sku', '')
            sku = re.sub(r'\s-\s|\s-|-\s', ' ', sku)
            sku = re.sub(r'\s+', ' ', sku).strip()

            def fraction_to_decimal(fraction, decimal_count):
                from fractions import Fraction
                import unicodedata
                nums = fraction.split('/')
                if len(nums) == 2:
                    return str(round(float(Fraction(int(nums[0]), int(nums[1]))), decimal_count))[1:]
                else:
                    try:
                        return str(round(unicodedata.numeric(u'{}'.format(fraction)), decimal_count))[1:]
                    except:
                        pass
                    return fraction

            heel_height = ''
            match = re.findall(r'(?<=,\s)([^,]*)(inches|inch|") heel height', features, flags=re.IGNORECASE)
            if match:
                heel_height, _ = match[0]
                heel_height = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group(), 2), heel_height)
                heel_height += ' in.'


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description + ' ' + features,
                "image": record.get('image', ''),

                "model": model,
                "color": record.get('color', ''),
                'heel_height': heel_height,
                'model_sku': sku,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
