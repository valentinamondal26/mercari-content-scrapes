import hashlib
import html
import re

from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = html.unescape(record.get('title', ''))
            model = html.unescape(record.get('title', ''))

            # color transformations
            color = record.get("Color", "")
            colors_in_model = [i for i in re.findall(r"\w+", title)
                               if self.check_color(i)]
            if re.findall(re.compile(r"Gun Metal", re.IGNORECASE), title):
                colors_in_model.append("Gun Metal")
            if not color:
                color = "/".join(colors_in_model)
            if color == "Multicolor" and colors_in_model:
                color = "/".join(colors_in_model)
            for cl in colors_in_model + re.findall(r"\w+", color):
                model = re.sub(re.compile(r"\b{}\b".format(cl),
                               re.IGNORECASE), "", model)
            color = "/".join(color.split(", "))
            if 'multicolor' not in color.lower() and 'multi' in color.lower():
                color = re.sub(re.compile(r'Multi', re.IGNORECASE),
                               'Multicolor', color)

            # model transformations
            if 'Chrome' in model:
                if color:
                    color = color + ', Chrome'
                else:
                    color = 'Chrome'
                model = model.replace('Chrome', '')

            color = "/".join(color.split(", "))

            if record.get('id',
                          '') == 'https://www.walmart.com/ip/WAHL/49086993':
                model = 'Mustache and Beard Battery Trimmer'
            model = re.sub(re.compile(r'\b\w{0,1}\d+-\d+-*\d*-*\d*\w{0,1}\b|#|'
                                      r'\.*\s*This Compact Hair Clipper Is '
                                      r'The Perfect Size '
                                      'For That First Haircut To Total Body '
                                      'Grooming For '
                                      'The Entire Family\s*\.*', re.IGNORECASE),
                           '', model)
            model = re.sub(re.compile(r"A\?\?\b", re.IGNORECASE), " ", model)
            model = re.sub(re.compile(r"pack of (\d+)\b", re.IGNORECASE),
                           r"\1 Pack", model)
            model = re.sub(
                    re.compile(r"\((\s*\d+\s*\-*Pack\s*)\)", re.IGNORECASE),
                    r"\1", model)
            model = re.sub(re.compile(r"(for model|for|model)\s*\d+\s*\,*\s*\d*"
                                      r"\s*(and|\&)\s*\d*\b",
                           re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                    r"For \d+ Model", re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                    r"Model \d{4,7|\b", re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                    r"\(.*\)|\b\-*\w{0,1}\d{4,7}\-*\d*\w{0,1}\b|Wahl\'*s*\b|®",
                    re.IGNORECASE), "", model)
            model = model.replace(
                    '2 In 1', '2-in-1').replace('-In-', '-in-').replace(',', '')

            def to_lower(match):
                return match.group(1) + match.group(2).lower()

            model = re.sub(re.compile(
                    r"(\w+)(\'\w{1}\b)", re.IGNORECASE), to_lower, model)
            
            model = re.sub(re.compile(
                    r"(\d+)\s*\-*Pc\.*\b", re.IGNORECASE), r"\1 Piece", model)
            model = re.sub(re.compile(r'\bPieces\b', re.IGNORECASE), 'Piece',
                           model)
            
            model = re.sub(re.compile(
                    r"(\d+)\s*\-*\s*(Piece|Pack)\.*", re.IGNORECASE), r"\1 \2",
                    model)
            
            model = re.sub(re.compile(
                    r"(.*)(\b\d+ (Pack|Piece))(.*)", re.IGNORECASE),
                    r"\2 \1 \4", model)
            
            model = re.sub(re.compile(r"\b(\d+)(Oz\.|oz\b)", re.IGNORECASE),
                           r"\1 Oz", model)
            
            model = re.sub(re.compile(r"(.*)(\b\d+\-in\-\d+\b)(.*)",
                           re.IGNORECASE), r"\2 \1 \3", model)
            
            if re.findall(re.compile(r"\d+ pack", re.IGNORECASE), model) \
                    and re.findall(re.compile(r"\d+ ea\.*\b", re.IGNORECASE),
                                   model):
                model = re.sub(re.compile(r"\d+ (ea\.|ea\b)", re.IGNORECASE),
                               "", model)
            
            model = re.sub(r"\s\-|\-$|\-\s", " ", model)
            model = re.sub(r'/\s|\s/|,', '', model)
            model = " ".join([m[0].upper() + m[1:] for m in model.split()])
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(
                    re.compile(r"([^\d+])\s*\.\s*([^\d+])", re.IGNORECASE),
                    r"\1 \2", model)
            
            if re.compile(r"^WAHL\'*s*$", re.IGNORECASE).match(
                    record.get("title", "")):
                model = "Super Pocket Pro"
            
            if re.compile(r"^\d+\-\d+ Trimmer$", re.IGNORECASE).match(
                    record.get("title", "")):
                model = "Ear, Nose, Brow Dual Head Trimmer"
            
            model = re.sub(re.compile(r'\bFor Fast Easy Precise And Hygienic '
                                      r'Grooming! Model\b|\bFor Trimming '
                                      r'Sideburns Beard Or Mustache Model\b'
                                      r'|\bCord/Cordless\b',
                           re.IGNORECASE), '', model)
            
            model = re.sub(re.compile(r'\bHair Clippers Electric Shavers And '
                                      r'Mustache Ear Nose Body Grooming By The '
                                      r'Brand Used By Professionals\b',
                           re.IGNORECASE), '', model)
            
            model = re.sub(re.compile(r'\bTrim Detail Ear/Nose And Shave '
                                      r'Model\b|\bQuantity 1\b|\bFor Standard\b'
                                      r'|\bLubricant & Coolant\b',
                           re.IGNORECASE), '', model)
            
            model = re.sub(re.compile(r'Mustache/beard', re.IGNORECASE),
                           'Mustache and Beard', model)
            
            model = re.sub(re.compile(r'\bModels\b|\bModel\b', re.IGNORECASE),
                           '', model)
            
            model = re.sub(r'&', 'and', model)
            model = re.sub(r'\s\+\s', ' with ', model)
            model = re.sub(re.compile(r'ClipperTrimmer', re.IGNORECASE),
                           'Clipper Trimmer', model)
            
            model = re.sub(re.compile(r'\bAll-in-one\b', re.IGNORECASE),
                           'All In One', model)
            model = re.sub(re.compile(
                    r'\d+ Oz|\d+ Fl Oz|\d+\.*\d*" X \d+\.*\d*" X \d+\.*\d*"',
                    re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'Arco', re.IGNORECASE), 'ACRO', model)
            model = re.sub(re.compile(r'Color Pro', re.IGNORECASE), 'ColorPro',
                           model)
            model = re.sub(re.compile(r'Groomsman', re.IGNORECASE), 'GroomsMan',
                           model)
            
            model = re.sub(re.compile(
                    r'GroomsManPro Rechargeable Grooming Kit All In One',
                    re.IGNORECASE),
                    'GroomsManPro Rechargeable All In One Grooming Kit', model)
            
            model = re.sub(re.compile(r'Km2', re.IGNORECASE), '', model)

            model = re.sub(re.compile(r"^(\d+) Piece Mustache and Beard "
                                      r"Trimmer Deluxe Battery Trimmer Kit "
                                      r"with BONUS Nose and Ear Trimmer$",
                           re.IGNORECASE),
                           r"\1 Piece Deluxe Mustache and Beard Battery "
                           r"Trimmer Kit",
                           model)
            model = re.sub(re.compile(r"^(\d+) Piece Clipper Close Cut Pro "
                                      r"Ultra-Close Haircutting Kit Fades "
                                      r"Outlining And Skin Close Shaving",
                           re.IGNORECASE), r"\1 Piece Clipper Close Cut Pro "
                                           r"Ultra-Close Haircutting Kit",
                           model)
            model = re.sub(re.compile(r"For Clean Blades|1 Ea\b",
                           re.IGNORECASE), "", model)
            model = re.sub(re.compile(r"(\d+|\ball\b)\s*\-*\s*(in)\s*\-*\s*"
                                      r"(\d+|\bone\b)", re.IGNORECASE),
                           r"\1-in-\3", model)
            model = re.sub(re.compile(r"\bPet\s*\-\s*Pro\b", re.IGNORECASE),
                           "Pet Pro", model)
            model = re.sub(re.compile(r"(.*)(set of \d+)(.*)", re.IGNORECASE),
                           r"\2 \1 \3", model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)
            d = {
                'Basic U-Clip Pet Kit Basic U-Clip Pet Trimmer Kit': 'Basic U-Clip Pet Trimmer Kit',
                'Flex Shave Rechargeable Foil Shaver Ergonomic Shape Soft Touch Grips Pop-Up Trimmer': 'Flex Shave Rechargeable Pop-Up Trimmer',
                "GroomsMan Pro All-In-One Men's Grooming Kit Rechargeable Beard Trimmers": "GroomsMan Pro All-In-One Men's Grooming Kit",
                'Groom Pro Total Body Hair Clipper Grooming Kit High-Carbon Steel Blades': 'Groom Pro Total Body Hair Clipper Grooming Kit',
                'Signature Series Clipper Trimmer Personal Trimmer': 'Signature Series Clipper & Personal Trimmer Kit',
                'Total Beard Trimmer Cordless': 'Total Beard Cordless Trimmer',
            }
            model = re.sub(r'|'.join(list(d.keys())), lambda x: d.get(x.group(), x.group()), model, flags=re.IGNORECASE)
            model = re.sub(r"Clip \'N", "Clip 'n", model)

            transformed_record = {

                    'id': record.get('id', ''),
                    'item_id': hex_dig,

                    'title': record.get('title', ''),
                    'model': model,
                    'brand': 'Wahl',
                    'category': record.get('category', ''),
                    'image': record.get('image', ''),

                    'crawl_date': record.get('crawlDate', '').split(',')[0],
                    'status_code': record.get('statusCode', ''),
                    'color': color,
                    'price': {
                            'currency_code': 'USD',
                            'amount': record.get('price', '')
                    }
            }
            if 'Massager' in title or 'Therapeutic' in title \
                    or 'Therapy' in title:
                return None
            else:
                return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
