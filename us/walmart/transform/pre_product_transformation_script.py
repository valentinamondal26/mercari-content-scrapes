import hashlib
import re

class Mapper(object):

    def normalize_brand(self,brand):

        normalizer_list = {
            ("AmorosO Enterprise USA","AmorosO"):"AmorosO",
            ("Ella Baby","Ella Baby Stoller"):"Ella Baby",
            ("GB","gb"): "GB",
            ("Maxi Cosi","Maxi-Cosi"): "Maxi-Cosi",
            ("Wonder Buggy","Wonder buggy"): "Wonder Buggy"
        }

        for key,value in normalizer_list.items():
            if brand in key:
                return value
        else:
            return brand


    def get_dimensions(self, measurements):
        length = ''
        width = ''
        height = ''
        if isinstance(measurements, str):
            if "inches" in measurements.lower():
                dimensions = measurements.replace("\"", ":", -1).lower().replace("inches","")
                dimensions = dimensions.split("x")
                if dimensions and len(dimensions) > 2:
                    length = dimensions[0].strip()
                    width = dimensions[1].strip()
                    height = dimensions[2].strip()

        return {
            'length': {'unit': 'INCH', 'value': length},
            'width': {'unit': 'INCH', 'value': width},
            'height': {'unit': 'INCH', 'value': height}
        }

    def get_weight_in_lbs(self,weight):
        weight_in_lbs = ''
        if isinstance(weight, str):
            if "pounds" in weight.lower():
                weight_in_lbs = weight.lower().replace("pounds","").strip()
            elif "lbs" in weight.lower():
                weight_in_lbs = weight.lower().replace("lbs","").strip()
        return { 'unit': 'lbs', 'value': weight_in_lbs }

    def get_price(self, input):
        if not isinstance(input, str):
            return ''

        numbers = re.findall("\d+(?:[\d,.]*\d)", input)
        numbers = [x.replace(",", "") for x in numbers]
        numbers = map(float, numbers)
        return max(numbers)

    def get_color(self,record):
        colors = set()
        spec_color = record.get('Color', '') or ''
        spec_colors = spec_color.split(',')
        if spec_colors:
            colors.update(spec_colors)
        actual_colors = record.get('colors', []) or []
        if actual_colors:
            colors.update(actual_colors)
        colors = [i for i in colors if i]
        return ','.join(colors)




        # if record.get('Color'):
        #     return record['Color']
        # elif record.get('colors'):
        #     return ",".join(record['colors'])
        # else:
        #     return ""



    def transform_title_to_model(self,title,brand, colors):
        if not isinstance(title, str) or not isinstance(brand, str):
            return ''
        def escapeSpecialChars(pattern):
            pattern = pattern.replace('+', '\+')
            return pattern
        pattern = r'\s*'.join(brand.replace(' ', ''))
        pattern = escapeSpecialChars(pattern)
        model = re.sub(re.compile(pattern, re.IGNORECASE), '', title).strip()
        model = model.replace(u"\u2122", '').replace(u"\u00ae","").strip()

        for color in colors.split(','):
            model = model.replace(color,'')

        model = re.sub(re.compile(r'^[-,]|[-,]$'), '', model)

        for words in ["Travel System", "Tandem Stroller", "Double Stroller", "Jogging Stroller",
            "Lightweight Stroller", "Standard Stoller","Baby Stoller","Stroller System","Travel System Stroller"]:

            model = re.sub(re.compile(words, re.IGNORECASE), '', model).strip()

        model = re.sub(re.compile(r"-\s+", re.IGNORECASE), ' ', model).strip()
        model = re.sub(re.compile(r"\s+"), ' ', model).strip()
        return model

    def map(self, record):
        if record:

            crawl_date = record['crawlDate']
            status_code = record['statusCode']
            category = "Strollers"
            id = record['id']
            price = record.get('price', "")
            max_price = record.get('max_price', "")
            image = record.get('image', "")
            rating = record.get('rating', "")
            title = record.get('title', "") #model
            description = record.get('description', "")
            brand = self.normalize_brand(record.get('Brand', ""))
            breadcrumb = record.get('breadcrumb', "")
            sku = record.get('sku', "")
            gtin = record.get('gtin', "")
            wallmart_number = record.get('wallmart_number', "")
            gender = record.get('Gender', "")
            color = self.get_color(record)
            model = self.transform_title_to_model(title, brand, color)
            maximum_weight = self.get_weight_in_lbs(record.get('Maximum Weight', ""))#max_weight_capacity
            collection = record.get('Collection', "")
            stroller_type = record.get('stroller_type', "")
            manufacturer_part_number = record.get('Manufacturer Part Number', "")#model_sku
            product_dimensions = self.get_dimensions(record.get('Assembled Product Dimensions (L x W x H)', ""))
            assembled_product_weight = self.get_weight_in_lbs(record.get('Assembled Product Weight', ""))#weight

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date" : crawl_date,
                "status_code" : status_code,
                "category" : category,
                "max_price" : max_price,
                "image" : image,
                "rating" : rating,
                "title" : title, #model
                "description" : description,
                "brand" : brand,
                "breadcrumb" : breadcrumb,
                "sku" : sku,
                "gtin" : gtin,
                "wallmart_number" : wallmart_number,
                "gender" : gender,
                "color" : color,
                "maximum_weight" : maximum_weight,#max_weight_capacity
                "collection" : collection,
                "stroller_type" : stroller_type,
                "manufacturer_part_number" : manufacturer_part_number,#model_sku
                "product_dimensions" : product_dimensions,
                "assembled_product_weight" : assembled_product_weight,#weight
                "price": {
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "model":model
            }
            return transformed_record
        else:
            return None
