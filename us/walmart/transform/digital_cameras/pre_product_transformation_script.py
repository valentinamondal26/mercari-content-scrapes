import hashlib
import re


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')

            mega_pixel_value = record.get('Megapixels', '')
            resolution = record.get('Resolution', '')
            color = record.get('Color', '')
            if not color:
                colors_available = ['Red', 'Black',
                                    'Blue', 'White', 'Green', 'Yellow']
                try:
                    color = [col for col in colors_available if col in record.get(
                        'title', '')][0]
                except IndexError:
                    pass
            color = '/'.join(list(filter(None, [c.strip() if c.strip().lower() != 'other' else None for c in color.split(',')])))

            model = record.get('title', '')
            model = re.sub(re.compile(
                r'Digital Camera|Camera', re.IGNORECASE), '', model)
            for pixel in mega_pixel_value.split(','):
                values = re.findall(r'\d+', pixel)
                for value in values:
                    model = re.sub(re.compile(
                        r'{}\s*MP'.format(value)), '', model)

            model = re.sub(re.compile(r"\+.*$", re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                r'\d+\s*MP|\d+\.\d+\s*Mp', re.IGNORECASE), '', model)
            model = re.sub(re.compile(
                r'\d+\s*MEGAPIXEL(S)*|\d+.\d+\s*MEGAPIXEL(S)*', re.IGNORECASE), '', model)
            model = model.replace('KODAK', 'Kodak')
            model = model.replace('PIXPRO', 'PixPro')

            for resol in resolution.split(','):
                model = re.sub(resol, '', model)
            model = re.sub(re.compile(
                r'HD 720P|FHD 1080P|HD (720P)|FHD (1080P)|1080p|720p|HD|FHD', re.IGNORECASE), '', model)

            for col in color.split(','):
                model = re.sub(col, '', model)

            model = re.sub(re.compile(
                r"\d+X\s*(Zoom)*", re.IGNORECASE), '', model)
            model = re.sub(re.compile(r"\(.*\)|\|", re.IGNORECASE), '', model)
            model = model.replace(',', '')

            model = re.sub(re.compile(r"\bw\/", re.IGNORECASE), "with ", model)
            if re.findall(re.compile(r"\bwith\b.*$", re.IGNORECASE), model):
                after_with = re.findall(re.compile(
                    r"\bwith\b.*$", re.IGNORECASE), model)[0]
                model = re.sub(re.compile(
                    r"\bwith\b.*$", re.IGNORECASE), "", model)
                if re.findall(re.compile(r"\bwith\b\s*.*\d+(GB|TB|MB|KB).*", re.IGNORECASE), after_with):
                    after_with = re.sub(re.compile(
                        r"(\bwith\b\s*)[^\d]+(\d+(GB|TB|MB|KB).*)(and|\&)\s.*", re.IGNORECASE), r"\1 \2", after_with)
                    after_with = re.sub(re.compile(
                        r"(\bwith\b\s*)[^\d]+(\d+(GB|TB|MB|KB).*).*", re.IGNORECASE), r"\1 \2", after_with)
                else:
                    after_with = ''
                if after_with:
                    model += " "+after_with
            model = re.sub(r"\s\-\s|\s\-$|\s\-|\-\s", " ", model)
            model = re.sub(r'\s+', ' ', model)
            model = re.sub(re.compile(r"Optical\-*\s*Zoom|Optical\-*\s*Video|\bw\/*\s*Optical|"
                                      r"Optical\s*\&|"
                                      r"Accessory|Accessories|\!|"
                                      r"Rugged Waterproof and|"
                                      r"Point \& Shoot Case|"
                                      r"\+*\s*Top Value Accessories|"
                                      r"(\-|\–)\s*Includes.*$|\bzoom\b|"
                                      r"Sony 32GB Class 10 70MB\/s SDHC Case|"
                                      r"27 Exposure Waterproof Up To 50 Feet|"
                                      r"1.5\" LCD CMOS Full|"
                                      r"Lighting LED-On LED Video Light|",
                                      re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                r"(Kodak Underwater Disposable Sport).*$", re.IGNORECASE), r"\1", model)

            if not re.findall(re.compile(r"\d+\.*\d*\s*(GB|TB|MB|KB)\s*"
                                         r"(\bMemory Card\b|\bsd card\b|"
                                         r"\bcard\b|\bsd\b|\bmicrosd card\b|"
                                         r"\bmemory\b)", re.IGNORECASE), model):
                if not re.findall(re.compile(r"with\s*\d+\.*\d*\s*(GB|TB|MB|KB)", re.IGNORECASE), model):
                    model = re.sub(re.compile(
                        r"(\d+\.*\d*\s*(GB|TB|MB|KB))(.*)", re.IGNORECASE), r"with \1 Memory \3", model)
                else:
                    model = re.sub(re.compile(
                        r"(\d+\.*\d*\s*(GB|TB|MB|KB))(.*)", re.IGNORECASE), r"\1 Memory \3", model)
            if re.findall(re.compile(r"\d+\.*\d*\s*(GB|TB|MB|KB)\s*memory", re.IGNORECASE), model) and not re.findall(re.compile(r"with\s*\d+\.*\d*\s*(GB|TB|MB|KB)\s*memory", re.IGNORECASE), model):
                model = re.sub(re.compile(
                    r"(\d+\.*\d*\s*(GB|TB|MB|KB))(.*)", re.IGNORECASE), r"with \1 \3", model)

            if not re.findall(re.compile(r"with\s*\d+\.*\d*\s*(GB|TB|MB|KB)", re.IGNORECASE), model):
                model = re.sub(re.compile(
                    r"(\d+\.*\d*\s*(GB|TB|MB|KB))(.*)", re.IGNORECASE), r"with \1 \3", model)

            model = " ".join([m[0].upper() + m[1:] for m in model.split()])
            model = re.sub(re.compile(r"\bEASYSHARE\b",
                                      re.IGNORECASE), "Easyshare", model)

            def space_strip(match):
                return match.group(1).strip()+"/" + match.group(2).strip()
            model = re.sub(re.compile(
                r"(.*)\/(.*)", re.IGNORECASE), space_strip, model)

            if re.compile(r"^Kodak Mini Digital Film \& Slide Scanner \– "
                          r"Converts 35mm 126 110 Super 8 \& 8mm Film Negatives "
                          r"\& Slides To JPEG Images$",
                          re.IGNORECASE).match(model):
                model = re.sub(re.compile(r"(.*Scanner)(.*)",
                                          re.IGNORECASE), r"\1", model)
            model = re.sub('Memory Card With 4GB SD Card', '4GB Memory Card', model, flags=re.IGNORECASE)
            model = re.sub(r'\bwith\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Digital Camcorder)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Memory', 'Card', 'SD', 'MicroSD']))), '', model, flags=re.IGNORECASE)
            model = re.sub('Fz43', 'FZ43', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(r"Gift|\bBundle\b|\bRefurbished\b|\bKit\b", title, flags=re.IGNORECASE):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,

                'title': title,
                'model': model,
                'brand': record.get('brand', ''),
                'category': record.get('category', ''),
                'image': record.get('image', ''),

                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'color': color,
                'model_SKU': record.get('Model', '') or record.get('Manufacturer Part Number', ''),
                'camera_resolution': resolution.replace('(', '').replace(')', ''),
                'megapixels': mega_pixel_value,

                'price': {
                    'currency_code': 'USD',
                    'amount': record.get("price", '')
                }
            }
            return transformed_record
        else:
            return None
