import hashlib
import re


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')

            # extract dimensions from spec
            dimensions = record.get('Assembled Product Dimensions (L x W x H)',
                                    '')
            if not dimensions:
                dimensions = re.findall(r'Dimensions:\s\d+\.*\d*\"*\sL*\s*x\s'
                                        r'\d+\.*\d*\"*\sW*\s*x\s\d+\.*\d*\"*'
                                        r'\sH*\s*',
                                        record.get('product_highlights', ''))
                if dimensions:
                    dimensions = dimensions[0].replace('Dimensions: ',
                                                       '').strip()
                else:
                    try:
                        dimensions = re.findall(r'\s\d+\.*\d*\"*\sL*\s*x\s\d+'
                                                r'\.*\d*\"*\s*W*\s*x*\s*\d*\.*'
                                                r'\d*\"*\s*H*\s*',
                                                record.get('prdouct_highlights',
                                                           ''))[0]
                    except IndexError:
                        pass

            length = ''
            width = ''
            height = ''
            if dimensions:
                dimension = dimensions.split(' x ')

                if len(dimension) == 3:
                    length = dimension[0].replace('L', '').strip()
                    width = dimension[1].replace('W', '').strip()
                    height = dimension[2].replace('H', '').strip()
                elif len(dimension) == 2:
                    length = dimension[0]
                    height = dimension[1]

            if height and 'Inches' not in height.title():
                height = height + ' Inches'

            # model transformations
            model_sku = record.get('Manufacturer Part Number', '')
            model = model.replace('Type', '')
            model = model.replace(' - ', ' ')
            model = model.replace('|', '')
            model = re.sub(re.compile(r"Bestway", re.IGNORECASE), "", model)
            if re.findall(re.compile(r"Boat \/ Raft", re.IGNORECASE), model):
                model = re.sub(re.compile(r"\/ Raft", re.IGNORECASE), "", model)
            model = re.sub(re.compile(r"\d+\.*\d*\s*\"*\s*x\s*\d+\.*\d*\s*\"*"
                                      r"\s*x*\s*\d*\.*\d*\s*\"*",
                                      re.IGNORECASE),
                           "", model)

            if re.findall(r'\d+ x ', model_sku):
                sku_part = model_sku.split('x')[1].split('-')[0].strip()
                model = re.sub(sku_part, '', model)
            else:
                model = re.sub(model_sku, '', model)

            if re.findall(r'\d\)', model):
                replaceable_string = re.findall(r'\d\)', model)[0]
                number = replaceable_string.replace(')', '')
                replacing_value = str(number) + "-" + "Pack"
                model = model.replace(replaceable_string, replacing_value)

            # remove color from model
            color = record.get('Color', '')
            model = model.replace(color, '').replace(', Camouflage', '')

            # remove model_sku/product number from model
            if model_sku and not re.findall(r'\d+ x ', model_sku):
                try:
                    removable_part = re.findall(r'\d+', model_sku)[0]
                    model = model.replace(removable_part, '')
                except IndexError:
                    pass
            else:
                product_highlights = record.get('product_highlights', '')
                product_number = re.findall(r"Product number: \d+",
                                            product_highlights)
                if product_number:
                    product_number = product_number[0]
                    model = model.replace(product_number, '')
            model = re.sub(r'\d+\w', '', model)

            # moving pack info to model front end
            if re.findall(r'(\(\d-Pack\))', model):
                packs = re.findall(r'(\(\d-Pack\))', model)[0]
                number = re.findall(r'\d', packs)[0]
                model = re.sub(packs, '', model)
                model = number + '-Pack ' + model

            model = re.sub(r'\(|\)', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r"^Inflatable Animal Swim Ring for Small "
                                     r"Children Beaver$|^Inflatable Animal "
                                     r"Swim Ring for Small Children Pelican$|"
                                     r"Inflatable Animal Swim Ring for Small "
                                     r"Children Reindeer$", re.IGNORECASE),
                          model):
                last_word = re.findall(r"\w+", model)[-1]
                model = re.sub(re.compile(r"\b{}\b".format(last_word),
                                          re.IGNORECASE), "", model)
                model = re.sub(re.compile(r"\bAnimal\b", re.IGNORECASE),
                               last_word, model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(r'\s+', ' ', model).strip(' ,')

            # color transformation
            color = re.sub(re.compile(r"Multicolor\s*\,",
                           re.IGNORECASE), "", color)

            transformed_record = {

                    'id': record.get('id', ''),
                    'item_id': hex_dig,

                    'title': record.get('title', ''),
                    'model': model,
                    'brand': record.get('brand', ''),
                    'category': record.get('category', ''),
                    'image': record.get('image', ''),

                    'crawl_date': record.get('crawlDate', '').split(',')[0],
                    'status_code': record.get('statusCode', ''),

                    'description': record.get('product_highlights', ''),
                    'product_type': record.get('product_type', ''),
                    'color': color,
                    'length': length,
                    'width': width,
                    'height': height,

                    'price': {
                            'currency_code': 'USD',
                            'amount': record.get('price', '')
                    },
                    'msrp': {
                            'currency_code': 'USD',
                            'amount': record.get('max_price', '')
                    }
            }

            if "colemon" in record.get('title', '').lower():
                return None
            else:
                return transformed_record

        else:
            return None
