
if __name__ == "__main__":

    import json
    brand_list = ['amoroso enterprise usa', 'amoroso', 'anself', 'aosom', 'apontus', 'bob', 'babideal', 'baby jogger', 'baby trend', 'baby home', 'babyroues', 'biba', 'britax', 'bugaboo', 'chicco', 'contours', 'cosatto', 'cosco', 'costway', 'cybex', 'combi', 'delta children', 'disney', 'disney baby', 'dream on me', 'ella baby', 'ella baby stoller', 'evenflo', 'evezo', 'familidoo', 'foundations', 'gb', 'gb', 'goplus', 'graco', 'gymax', 'hauck', 'high supply', 'jeep', 'joovy', 'jovial', 'kkmoon', 'kolcraft', 'mlb', 'maclaren', 'mamas & papas', 'maxi cosi', 'maxi-cosi', 'mia moda', 'monbebe', 'mountain buggy', 'muv', 'my babiie', 'online', 'peg perego', 'phil&teds', 'quinny', 'roan', 'safety 1st', 'strollair', 'summer infant', 'wonder buggy', 'wonder buggy', 'urbini']
    count = 0
    path = '/home/salman/github/mercari/mercari-content-scrapes/us/walmart/scrape/out/'
    with open(path+'double_strollers.jl', 'r') as f:
        content = f.readlines()
    for line in content:
        line = json.loads(line)
        if line.get("brand") and line.get("brand").lower() in brand_list:
            with open(path+'double_strollers_brand_filtered.jl', "a") as f:
                f.write(json.dumps(line)+"\n")
        else:
            count += 1
            print(line.get("brand"), count)

