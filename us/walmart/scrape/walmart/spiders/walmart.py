'''
https://mercari.atlassian.net/browse/USDE-1662 - Kodak Digital Cameras
https://mercari.atlassian.net/browse/USDE-1665 - Bestway Water Sports
https://mercari.atlassian.net/browse/USDE-1612 - Wahl Hair Styling Tools
'''

import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class WalmartSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from walmart.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Strollers
        2) Hair styling tools
        3) Water Sports
        4) Digital Cameras

    sub_category : str
        brand/stroller_type to be crawled, Supports
        1) Travel System
        2) Double Strollers
        3) Jogging Strollers
        4) Standard Strollers
        5) Lightweight Strollers
        6) Wahl
        7) Bestway
        8) Kodak

    Command e.g:
    scrapy crawl walmart -a category='Strollers' -a brand='Travel System'
    scrapy crawl walmart -a category='Hair styling tools' -a brand='Wahl'
    scrapy crawl walmart -a category='Water Sports' -a brand='Bestway'
    scrapy crawl walmart -a category='Digital Cameras' -a brand='Kodak'
    """

    name = "walmart"

    category = None
    brand = None

    merge_key = 'id'

    source = 'walmart.com'
    blob_name = 'walmart.txt'

    base_url = 'https://www.walmart.com'

    product_listing_page_url_template = 'https://www.walmart.com/search/api/preso?cat_id={category_id}&prg=desktop&page={page}'
    listing_page_api_base_url = 'https://www.walmart.com/search/api/preso?'
    page = 0

    proxy_pool = [
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
    ]

    url_dict = {
        'Strollers': {
            'Travel System': {
                'url':'https://www.walmart.com/browse/baby/travel-systems/5427_118134_1101427',
            },
            'Double Strollers': {
                'url':'https://www.walmart.com/browse/baby/double-strollers/5427_118134_1101428',
            },
            'Jogging Strollers': {
                'url': 'https://www.walmart.com/browse/baby/jogging-strollers/5427_118134_1101424',
            },
            'Standard Strollers': {
                'url':'https://www.walmart.com/browse/baby/standard-strollers/5427_118134_1101426',
            },
            'Lightweight Strollers': {
                'url': 'https://www.walmart.com/browse/baby/lightweight-strollers/5427_118134_1101425',
            }
        },
        'Hair styling tools': {
            'Wahl': {
                'url':'https://www.walmart.com/c/brand/wahl-collection'
            }
        },
        'Water Sports': {
            'Bestway': {
                'url': [
                    'https://www.walmart.com/c/brand/bestway-floats-pool-games',
                    'https://www.walmart.com/c/brand/bestway-pool-supplies',
                    'https://www.walmart.com/c/brand/bestway-boats',
                ]
            }
        },
        'Digital Cameras': {
            'Kodak': {
                'url': 'https://www.walmart.com/browse/electronics/kodak-cameras/kodak/3944_133277_1231331_1231337/YnJhbmQ6S29kYWsie?cat_id=3944_133277_1231331_1231337&facet=brand%3AKodak&page=1',
                'filters': ['Color', 'Megapixels', 'Resolution'],
                'listing_page_api': 'https://www.walmart.com/search/api/preso?cat_id=3944_133277_1231331_1231337&prg=desktop&facet=brand%3AKodak'
            }
        }
    }
    ### we hardcoded the listing_page_api for the digital camera because in the ticket itself gave an
    ### filtered url so api formation is bit difficult .
    ### In rest of the tickets we can form an api using category id from the url e.g Strollers.
    ### if the category id is not able to genearate for some url
    # we can go for html crawling.e.g*.Hair Styling Tools url.

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(WalmartSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.listing_page_api = self.url_dict.get(category, {}).get(brand, {}).get('listing_page_api', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def start_requests(self):
        if self.listing_page_api:
            meta = {'make_filter_request':True}
            yield self.create_request(url=self.listing_page_api, callback= self.parse_listing_page_api,meta=meta)
        else:
            if isinstance(self.launch_url,list):
                for url in self.launch_url:
                    yield self.req(url)
            else:
                yield self.req(self.launch_url)


    def req(self,url):
        category_id = url.rsplit('/', 1)[-1]
        if category_id.replace('_','').isdigit():
            self.page = self.page + 1
            url = self.product_listing_page_url_template.format(page=self.page, category_id=category_id)
            return self.create_request(url=url, callback=self.parse_listing_page_api)
        elif category_id.replace('-','').isalpha():
            return self.create_request(url=url, callback=self.parse_listing_page_url)


    def parse_listing_page_url(self, response):
        """
        @url https://www.walmart.com/c/brand/wahl-collection
        @returns requests 1
        @meta {"use_proxy": "True"}
        """
        item_urls = response.xpath("//div[@class='search-result-product-title gridview']/a/@href").getall()
        meta={
            'listing_page_url':response.url
        }
        for url in item_urls:
            yield self.create_request(url = self.base_url + url, callback=self.parse_detail, meta=meta)


    def parse_listing_page_api(self, response):
        """
        @url https://www.walmart.com/search/api/preso?cat_id=5427_118134_1101425&prg=desktop&page=1
        @meta {"use_proxy": "True", "page": 1}
        @returns requests 30 50
        """
        #TODO: SEPARTE THE FILTER PART IN THIS CALLBACK FUNCTION AND REWRITE THE CONTRACTS.
        
        # print('response:', response.text)
        meta = response.meta

        res = json.loads(response.text)
        products = res.get('items', [])
        if products:
            for product in products:
                id = product.get('productPageUrl', '') or ''

                # prod_id = product.get('productId', '')
                # item_id = product.get('usItemId', '')
                # rating = product.get('customerRating', 0)
                # item = {
                #     'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                #     'statusCode': str(response.status),
                #     'category': self.category,
                #     'id': 'https://www.walmart.com' + id if id else '',
                #     'image': product.get('imageUrl', '') or '',
                #     'rating': str(rating) if rating else '',
                #     'price': product.get('primaryOffer', {}).get('minPrice', '') or '',
                #     'max_price': product.get('primaryOffer', {}).get('maxPrice', '') or '',
                #     'description': product.get('description', '') or '',
                #     'title': product.get('title', ''),
                #     'upc': product.get('upc', ''),
                #     'brand': ", ".join(product.get('brand', [])),
                # }
                if id:
                    meta.update({'listing_page_url':response.url})
                    request = self.create_request(self.base_url + id, self.parse_detail, meta=meta)
                    # request.meta['item'] = item
                    yield request

        if meta.get('make_filter_request',''):
            filters_data = res.get('facets',[])
            filter_names = []
            for fil in filters_data:
                filter_names.append(fil.get("name",''))

            for apply_filter in self.filters:
                filter_index = filter_names.index(apply_filter)
                filter_data_values = filters_data[filter_index].get('values',[])
                for filter_value in filter_data_values:
                    meta_data = {}
                    meta_data.update({'filter_name':apply_filter})
                    meta_data.update({meta_data['filter_name']:filter_value.get('name','')})
                    filter_api = self.listing_page_api_base_url+filter_value.get('url','')
                    yield self.create_request(url=filter_api,callback=self.parse_listing_page_api, meta=meta_data)


        if res.get('pagination',{}).get('next',{}).get('url',''):
            next_page_url =  res.get('pagination',{}).get('next',{}).get('url','')
            next_page_url = self.listing_page_api_base_url+next_page_url
            if meta.get('filter_name',''):
                yield self.create_request(url=next_page_url, callback=self.parse_listing_page_api, meta=meta)
            else:
                yield self.create_request(url=next_page_url, callback=self.parse_listing_page_api)


    def parse_detail(self, response):
        """
        @url https://www.walmart.com/ip/Evenflo-Urbini-Reversi-Lightweight-Stroller-Fog/363333472
        @meta {"use_proxy": "True"}
        @scrape_values id price max_price image rating title description brand product_highlights breadcrumb sku gtin walmart_number colors
        """
        meta = response.meta
        item = {} #response.meta.get('item', {})

        # item_data = json.loads(response.xpath('//script[@id="item"]/text()').extract_first(default={}))
        # color_variants =  [color.lstrip('Actual Color ').rstrip(', ') for color in response.xpath('//span[@class="variant-option-outer-container display-inline-block valign-top"]/@title').extract()]
        color_variants = ', '.join(response.xpath("//div[@class='variants__list']/input/@aria-label").extract())

        image = response.xpath('//img[@itemprop="image"]/@src').extract_first(default='')
        if "https:" not in image:
            image = 'https:'+image

        max_price_whole_number = response.xpath("//span[@class='price display-inline-block xxs-margin-left price--strikethrough']/span[@class='price-group']/span[@class='price-characteristic']/text()").get(default='') or response.xpath("//span[@class='price display-inline-block xxs-margin-left price--strikethrough']/span[@class='price-group price-out-of-stock']/span[@class='price-characteristic']/text()").get(default='')
        max_price_decimal_part = response.xpath("//span[@class='price display-inline-block xxs-margin-left price--strikethrough']/span[@class='price-group']/span[@class='price-mantissa']/text()").get(default='') or response.xpath("//span[@class='price display-inline-block xxs-margin-left price--strikethrough']/span[@class='price-group price-out-of-stock']/span[@class='price-mantissa']/text()").get(default='')
        if max_price_whole_number and max_price_decimal_part:
            max_price = max_price_whole_number+'.'+max_price_decimal_part
        elif max_price_whole_number and not max_price_decimal_part:
            max_price = max_price_whole_number
        else:
            max_price  = ''

        description = ''.join([des.strip() for des in response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/text()[normalize-space(.)]").getall()]) + ''.join([des.strip() for des in response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/p/text()[normalize-space(.)]").getall()])
        if not description:
            description = ', '.join(response.xpath("//div[@class='AboutThisBundle-description']/text()").getall())

        description_features=[]
        for feature_name,feature_desc in zip(response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/p/span"),response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/ul")):
            description_features.append(feature_name.xpath("text()").get())
            description_features.append(', '.join(feature_desc.xpath(".//li/text()").getall()))

        if not description_features:
            description_features.append(', '.join(response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/strong/text()").getall()))
            description_features.append(", ".join(response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/ul/li/text()").getall()))
            description_features.append(', '.join(response.xpath("//div[@class='AboutThisBundle-description']/ul/li/text()").getall()))

        description_features = [des for des in description_features if des]
        description_features = ', '.join(description_features)
        # response.xpath("//div[@class='about-desc about-product-description xs-margin-top']/ul/li/text()").getall()
        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'id': response.url,
            'price': response.xpath('//span[@itemprop="price"]/@content').get(default='') or \
                response.xpath("//span[@class='price display-inline-block arrange-fit price display-inline-block xxs-margin-left']/span/text()").get(default=''),
            'max_price': max_price,
            'image': image,
            'rating': response.xpath('//span[@itemprop="ratingValue"]/text()').extract_first(default=''),
            'title': response.xpath('//h1[@itemprop="name"]/@content').extract_first(default='') or response.xpath("//h2[@class='prod-ProductTitle no-margin heading-b']/div/text()").get(default=''),
            'description': description,
            'brand': response.xpath('//span[@itemprop="brand"]/text()').extract_first(default=''),
            'product_highlights':description_features,
            'breadcrumb': ' | '.join(response.xpath('//span[@itemprop="name"]/text()').extract()) or ' | '.join(list(set(response.xpath("//li[@itemprop='itemListElement']/a/text()").getall()))) ,
            'sku': response.xpath('//meta[@itemprop="sku"]/@content').extract_first(default=''),
            'gtin': response.xpath('//span[@itemprop="gtin13"]/@content').extract_first(default=''),
            'walmart_number': response.xpath("//div[@class='valign-middle secondary-info-margin-right copy-mini display-inline-block wm-item-number']/text()").get(default='').replace('Walmart #','').strip(),
            'colors': color_variants,
            'stroller_type': self.brand,
            'listing_page_url':meta.get('listing_page_url','')
        }

        if response.meta.get('listing_page_url',''):
            listing_pageurl = response.meta.get('listing_page_url','')
            if 'bestway-floats-pool-games' in listing_pageurl:
                item.update({'product_type':'Floats & Pool Games'})
            elif 'bestway-boats' in listing_pageurl:
                item.update({'product_type':'Boats'})
            elif 'bestway-pool-supplies' in listing_pageurl:
                item.update({'product_type':'Pool Supplies'})

        if self.category!='Strollers':
            item.pop('stroller_type',None)

        filter_name = meta.get('filter_name', '')
        if filter_name:
            meta_data =  dict.fromkeys([filter_name], meta[filter_name])
            item.update(meta_data)


        features = {}
        for tr in response.xpath('//table[@class="product-specification-table table-striped"]//tr'):
            features[tr.xpath('./td[@class="s-padding-ends s-padding-sides"]/text()').extract_first(default='')] = tr.xpath('./td/div/text()').extract_first(default='')
        item.update(features)
        yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
