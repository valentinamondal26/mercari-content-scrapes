# -*- coding: utf-8 -*-

# Scrapy settings for walmart project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'walmart'

SPIDER_MODULES = ['walmart.spiders']
NEWSPIDER_MODULE = 'walmart.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'httpie/1.0'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

ROBOTSTXT_OBEY = False

DOWNLOAD_DELAY = 3

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

# GCS settings - actual value be 'raw/walmart.com/travel_systems/all/2019-02-06_00_00_00/json/walmart.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}
