import re
import pandas as pd
import numpy as np


def table_to_dict(selector):
    count = 0
    main_dataset = {}
    for tr in selector.xpath("./tbody/tr"):
        count = count + 1
        row_list = []
        for child in tr.xpath("./child::*"):
            tag_dict = {
                'name': child.xpath("name()").get(),
                'colspan': int(child.xpath("@colspan").get(default='1') or '1'),
                'rowspan': int(child.xpath("@rowspan").get(default='1') or '1'),
                # 'text': ','.join(child.xpath("span/@title").getall()) or ','.join(child.xpath("./span/@style").getall()) or ','.join(child.xpath("span//span/@style").getall()) or child.xpath("normalize-space()").get() or child.xpath(".//img/@src").get() or '',
                'text': child.xpath("normalize-space()").get() or child.xpath(".//img/@src").get() or '',
            }
            row_list.append(tag_dict)
        main_dataset[count] = row_list
    return main_dataset


def dict_to_df(data, no_rows, no_columns):
    df = pd.DataFrame(index=np.arange(no_rows), columns=np.arange(no_columns), data='')
    rows = 0
    for row in data.values():
        col = 0
        for ele in row:

            while col < no_columns and df.iat[rows,col]:
                col += 1

            if col<=no_columns-1:
                r = ele['rowspan']
                c = ele['colspan']
                if c > no_columns:
                    c = no_columns
                data = re.sub(r"\[.*\]",'', ele['text'])
                df.loc[np.arange(rows,rows+r), np.arange(col,col+c)] = data
            col = col + c
        rows += 1
    return df

def parse_html_table_to_df(table_selector, max_rows, max_columns):
    return dict_to_df(table_to_dict(selector=table_selector), no_rows=max_rows, no_columns=max_columns)
