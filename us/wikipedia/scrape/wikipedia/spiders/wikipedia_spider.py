'''
https://mercari.atlassian.net/browse/USCC-475 - Apple Tablets & eReaders
https://mercari.atlassian.net/browse/USCC-476 - Apple Cell Phones & Smartphones
https://mercari.atlassian.net/browse/USCC-735 - San-X Stuffed Animals
'''

from pandas.core.frame import DataFrame
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
from .wiki_parser import table_to_dict, dict_to_df
from itertools import chain

class WikipediaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from wikipedia.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Tablets & eReaders
        2) Cell Phones & Smartphones
        3) Stuffed Animals

    brand : str
        brand to be crawled, Supports
        1) Apple
        2) San-X

    Command e.g:
    scrapy crawl wikipedia -a category='Tablets & eReaders' -a brand='Apple'
    scrapy crawl wikipedia -a category='Cell Phones & Smartphones' -a brand='Apple'
    scrapy crawl wikipedia -a category='Stuffed Animals' -a brand='San-X'
    """

    name = 'wikipedia'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'wikipedia.com'
    blob_name = 'wikipedia.txt'

    url_dict = {
        'Tablets & eReaders': {
            'Apple': {
                'url': 'https://en.wikipedia.org/wiki/IPad#Model_comparison',
            }
        },
        'Cell Phones & Smartphones': {
            'Apple': {
                'url': 'https://en.wikipedia.org/wiki/List_of_iOS_and_iPadOS_devices#iPhone',
            }
        },
        'Stuffed Animals': {
            'San-X':{
                'url': 'https://en.wikipedia.org/wiki/San-X#List_of_characters',
            }
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(WikipediaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def process_items(self, response, meta):
        df = meta.get('df', None)
        if not isinstance(df, DataFrame) or df.empty:
            self.logger.error('Dataframe is Empty')
            return None

        sensor_technology_list = []

        for model in list(df.axes[1]):
            #converting string to dict with storage as keys and price as values
            l=str(df.loc['Launch price.Launch price',model]).split("Wi-Fi +")
            price_dict = {}
            for s in l:
                s = s.split(":")
                if 'Wi-Fi' in s[0]:
                    temp = {}
                    price_list = re.findall(r"(\d+).*?\$(\d+,*\d+)",s[1])
                    for pr in price_list:
                        temp[pr[0]]=pr[1]
                    price_dict['Wi-Fi'] = temp
                else:
                    temp = {}
                    price_list = re.findall(r"(\d+).*?\$(\d+,*\d+)",s[1])
                    for pr in price_list:
                        temp[pr[0]]=pr[1]
                    price_dict['Cellular'] = temp

            sensor_technology = df.loc['Environmental sensors.Environmental sensors',model]
            if "Additionally" not in sensor_technology:
                sensor_technology_list = str(sensor_technology).split(",")
            else:
                sensor_technology_list.append(sensor_technology.replace("Additionally:",""))

            storage = re.findall(r"\b16\b|\b32\b|\b64\b|\b128\b|\b256\b|\b512\b|\b1024\b|\b2048\b", str(df.loc['Launch price.Launch price',model]))
            for gb in list(set(storage)):
                model_number = re.findall(r'A\d* \(.*?\)', df.loc['Model Number.Model Number', model]) \
                    + list(map(lambda x: ' '.join(x), re.findall(r'(A\d*),.*?(\(.*?\))', df.loc['Model Number.Model Number', model])))
                model_number = list(filter(lambda x: 'China' not in x, model_number))
                for sku in model_number:
                    yield {
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': str(response.status),
                        'category': self.category,
                        'brand': self.brand,
                        'id': response.url+f"?model={''.join(model.split())}&storage={gb}&sku={''.join(sku.split())}",

                        'title': df.loc['Model.Model',model],
                        'price_dict': price_dict,
                        'image': df.loc['Image.Image',model],
                        'screen_size': df.loc['Screen Size.Screen Size',model] if 'Screen Size.Screen Size' in df.index else '',
                        'storage_capacity': gb,
                        'color':'White,Black,Rose Gold,Gold,Silver,Space Gray,Green,Sky Blue',
                        'series': meta.get('series', ''),
                        'display': ','.join(list(df.loc['Display.Display',model].values)),
                        'operating_system': df.loc['Highest supported operating system.Highest supported operating system',model],
                        'model_sku': sku,
                        'release_date': df.loc['Release date.Release date',model],
                        'motion_coprocessor': df.loc['Motion coprocessor.Motion coprocessor',model],
                        'cpu_brand': df.loc['CPU.CPU',model],
                        'gpu_video_card': df.loc['GPU.GPU',model],
                        'sensor_technology': list(set(sensor_technology_list)),
                        'memory_ram_capacity': df.loc['Memory.Memory',model],
                        'rear_camera_resolution': df.loc['Camera.Back',model] if 'Camera.Back' in df.index else df.loc['Camera.iSight',model],
                        'front_camera_resolution': df.loc['Camera.Front',model] if 'Camera.Front' in df.index else df.loc['Camera.FaceTime',model],
                        'battery_type': df.loc['Battery.Battery', model],
                        'connectors_supported': df.loc['Connector.Connector',model] if 'Connector.Connector' in df.index else df.loc['Connectors.Connectors',model],
                        'speaker_design': df.loc['Speakers.Speakers',model] if 'Speakers.Speakers' in df.index else '',
                        'weight': df.loc['Weight.Weight',model],
                        'dimensions': df.loc['Dimensions HxWxD.Dimensions HxWxD',model] if 'Dimensions HxWxD.Dimensions HxWxD' in df.index else df.loc['Dimensions.Dimensions',model],
                    }

    def crawlIphone(self, response): #for crawling iPhone data

        df = response.meta.get('df','')

        for model in list(df.axes[1]):

            for color in str(df.loc['Colors.Colors',model]).split(','):
                if "background-color" in color:
                    color = re.findall(r"background-color:(.*?);",color)[0]
                for storage in df.loc['Storage.Storage',model].split(','):
                    yield{
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': str(response.status),
                        'category': self.category,
                        'brand': self.brand,
                        'id': response.url+f"?model={''.join(model.split())}&color={color}&storage={storage}",

                        'title': model,
                        'screen_size': df.loc['Display.Screen Size',model],
                        'resolution': df.loc['Display.Resolution',model],
                        'processor_model': df.loc['Processor.Graphics Processor',model],
                        'color': color,
                        'storage_capacity': storage,
                        'ram_size': df.loc['RAM.RAM',model],
                        'rear_camera': df.loc['Rear Camera.Camera',model],
                        'front_camera': df.loc['Front Camera.Camera',model],
                        'height': df.loc['Dimensions.Height',model],
                        'width': df.loc['Dimensions.Width',model],
                        'depth': df.loc['Dimensions.Depth',model],
                        'weight': df.loc['Weight.Weight',model],
                        'model_sku': df.loc['Model number.Model number',model],
                        'release_date': df.loc['Released Date.Released Date',model]
                    }


    def parse_ipad(self, response):
        ipad, ipad_mini, ipad_air = response.xpath("//table[@class='wikitable mw-collapsible']")
        ipad_pro = response.xpath("//table[@class='wikitable']")[-1]

        ipad_dict = table_to_dict(ipad)
        ipad_df = dict_to_df(ipad_dict,34,10)
        ipad_df[1]=ipad_df[0]+'.'+ipad_df[1]   #joining two columns for indexing it
        ipad_df.drop(columns=0,index=0,inplace=True)  #dropping the first title row
        ipad_df.set_index(1,inplace=True)  #indexing the joined columns
        ipad_df.set_axis(ipad_df.loc['Model.Model'],axis='columns',inplace=True)  #setting Model.Model row as an axis

        ipad_mini_dict = table_to_dict(ipad_mini)
        ipad_mini_df = dict_to_df(ipad_mini_dict,33,10)
        ipad_mini_df[1]=ipad_mini_df[0]+'.'+ipad_mini_df[1]
        ipad_mini_df.drop(columns=0,index=0,inplace=True)
        ipad_mini_df.set_index(1,inplace=True)
        ipad_mini_df.set_axis(ipad_mini_df.loc['Model.Model'],axis='columns',inplace=True)

        ipad_air_dict = table_to_dict(ipad_air)
        ipad_air_df = dict_to_df(ipad_air_dict,34,6)
        ipad_air_df[1]=ipad_air_df[0]+'.'+ipad_air_df[1]
        ipad_air_df.drop(columns=0,index=0,inplace=True)
        ipad_air_df.set_index(1,inplace=True)
        ipad_air_df.set_axis(ipad_air_df.loc['Model.Model'],axis='columns',inplace=True)

        ipad_pro_dict = table_to_dict(ipad_pro)
        ipad_pro_df = dict_to_df(ipad_pro_dict,30,12)
        ipad_pro_df[1]=ipad_pro_df[0]+'.'+ipad_pro_df[1]
        ipad_pro_df.drop(columns=0,inplace=True)
        ipad_pro_df.set_index(1,inplace=True)
        ipad_pro_df.set_axis(ipad_pro_df.loc['Model.Model']+ipad_pro_df.loc['Screen Size.Screen Size'],axis='columns',inplace=True)

        for item in chain(
                self.process_items(response, meta={'df': ipad_df, 'series': 'iPad'}),
                self.process_items(response, meta={'df': ipad_mini_df, 'series': 'iPad Mini'}),
                self.process_items(response, meta={'df': ipad_air_df, 'series': 'iPad Air'}),
                self.process_items(response, meta={'df': ipad_pro_df, 'series': 'iPad Pro'}),
            ):
            yield item


    def parse_iphone(self, response):
        iphone = response.xpath("//table[@class='wikitable collapsible']")

        # In production and supported
        iphone_table1_dict = table_to_dict(iphone[0])
        iphone_table1_df = dict_to_df(iphone_table1_dict,130,10)
        iphone_table1_df[1]=iphone_table1_df[0]+'.'+iphone_table1_df[1]
        iphone_table1_df.drop(columns=[0,9],inplace=True)
        iphone_table1_df.set_index(1,inplace=True)
        iphone_table1_df.set_axis(iphone_table1_df.loc['Model.Model'],axis='columns',inplace=True)

        # Discontinued but still supported
        iphone_table2_dict = table_to_dict(iphone[1])
        iphone_table2_df = dict_to_df(iphone_table2_dict,129,14)
        iphone_table2_df[1]=iphone_table2_df[0]+'.'+iphone_table2_df[1]
        iphone_table2_df.drop(columns=0,inplace=True)
        iphone_table2_df.set_index(1,inplace=True)
        iphone_table2_df.set_axis(iphone_table2_df.loc['Model.Model'],axis='columns',inplace=True)

        # Discontinued and unsupported (64-bit CPU)
        iphone_table3_dict = table_to_dict(iphone[2])
        iphone_table3_df = dict_to_df(iphone_table3_dict,128,7)
        iphone_table3_df[1]=iphone_table3_df[0]+'.'+iphone_table3_df[1]
        iphone_table3_df.drop(columns=[0,5,6],inplace=True)
        iphone_table3_df.set_index(1,inplace=True)
        iphone_table3_df.set_axis(iphone_table3_df.loc['Model.Model'],axis='columns',inplace=True)

        # Discontinued and unsupported (32-bit CPU)
        iphone_table4_dict = table_to_dict(iphone[3])
        iphone_table4_df = dict_to_df(iphone_table4_dict,128,10)
        iphone_table4_df[1]=iphone_table4_df[0]+'.'+iphone_table4_df[1]
        iphone_table4_df.drop(columns=[0,9],inplace=True)
        iphone_table4_df.set_index(1,inplace=True)
        iphone_table4_df.set_axis(iphone_table4_df.loc['Model.Model'],axis='columns',inplace=True)

        for item in chain(
            self.process_items(response, meta={'df':iphone_table1_df}),
            self.process_items(response, meta={'df':iphone_table2_df}),
            self.process_items(response, meta={'df':iphone_table3_df}),
            self.process_items(response, meta={'df':iphone_table4_df}),
        ):
            yield item


    def parse_sanx_stuffed_animals(self, response):
        product_type_list = response.xpath("//h3/span[@class='mw-headline']/@id").getall()
         
        for product_type in product_type_list:
            items = response.xpath(f"//span[@id='{product_type}']/parent::h3/following-sibling::ul[1]/li")
            for item_info in items:
                item_info = item_info.xpath("normalize-space()").get()
                item_infos = item_info.split("-")
                title = ''
                if item_infos:
                    title = item_infos[0]
                description = ''
                if len(item_infos) > 1:
                    description = ' '.join(item_infos[1:])
                
                yield {
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': str(response.status),
                    'category': self.category,
                    'brand': self.brand,
                    'id': response.url+f"?product_type={product_type}&storage={title}",

                    'product_type': product_type,
                    'title': title,
                    'description': description,
                }


    def parse(self, response):
        """
        @url https://en.wikipedia.org/wiki/IPad#Model_comparison
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        if self.category == "Tablets & eReaders" and self.brand == "Apple":
            for item in self.parse_ipad(response):
                yield item

        if self.category == "Cell Phones & Smartphones" and self.brand == "Apple":
            for item in self.parse_iphone(response):
                yield item

        if self.category == "Stuffed Animals" and self.brand == "San-X":
            for item in self.parse_sanx_stuffed_animals(response):
                yield item

    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
