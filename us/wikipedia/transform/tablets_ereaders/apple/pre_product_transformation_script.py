import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            image = record.get('image', '')
            if image and not image.startswith('https:'):
                image = 'https:' + image

            model = record.get('title', '')
            model = re.sub(r'\(|\)', '', model)
            model = re.sub(r'\bgeneration\b', 'Generation', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            if not re.findall(r'\bGeneration\b', model, flags=re.IGNORECASE):
                if not re.findall(r"\b\d$", model):
                    model = f"{model} 1st Generation"
                else:
                    model = re.sub(r"\b2$", "2nd Generation", model)
                    model = re.sub(r"\b3$", "3rd Generation", model)
                    model = re.sub(r"\b4$", "4th Generation", model)
                    model = re.sub(r"\b5$", "5th Generation", model)



            storage_capacity = record.get('storage_capacity', '')

            rear_camera_resolution = ''
            rear_camera_resolution_list = re.findall(r"\d+\s*MP|\d+-megapixel", record.get("rear_camera_resolution", ''))
            if rear_camera_resolution_list:
                rear_camera_resolution = rear_camera_resolution_list[0].replace("-megapixel", " MP")

            front_camera_resolution = ''
            front_camera_resolution_list = re.findall(r"\d+\s*MP|\d+-megapixel", record.get("front_camera_resolution", ''))
            if front_camera_resolution_list:
                front_camera_resolution = front_camera_resolution_list[0].replace("-megapixel", " MP")
            
            dimensions = record.get("dimensions", '')
            dimension_final = re.findall(r"\d+\.*\d*\s*in", dimensions)
            if len(dimension_final) < 3:  #if h, w, d not present using another regex combination
                dimension_final = re.findall(r"(\d+\.*\d+)\s*×\s*(\d+\.*\d+)\s*×\s*(\d+\.*\d+)\s*in", dimensions)
                if dimension_final:
                    dimension_final = dimension_final[0]
            height, width, depth = dimension_final
            height = height.replace("\xa0", '').replace("in", '') + '"'
            width = width.replace("\xa0", '').replace("in", '') + '"'
            depth = depth.replace("\xa0", '').replace("in", '') + '"'

            model_sku = record.get('model_sku', '')
            price_dict = record.get("price_dict", {})
            if 'Cellular' in model_sku or '3G' in model_sku:
                price = price_dict.get('Cellular', {}).get(storage_capacity, '')
            else:
                price = price_dict.get('Wi-Fi', {}).get(storage_capacity, '')            

            if storage_capacity:
                storage_capacity += ' GB'


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": image,

                "model": model,
                "screen_size": record.get('screen_size', ''),
                "storage_capacity": storage_capacity,
                "color": record.get('color', ''),
                "series": record.get('series', ''),
                "display": record.get('display', ''),
                "operating_system": record.get('operating_system', ''),
                "model_sku": model_sku,
                "release_date": record.get('release_date', ''),
                "motion": record.get('motion_coprocessor', ''),
                "cpu_brand": record.get('cpu_brand', ''),
                "gpu_video_card": record.get('gpu_video_card', ''),
                "sensor_technology": ",".join(record.get('sensor_technology', '')),
                "rear_camera_resolution": rear_camera_resolution,
                "front_camera_resolution": front_camera_resolution,
                "battery_type": record.get('battery_type', ''),
                "connectors_supported": record.get('connectors_supported', ''),
                "speaker_design": record.get('speaker_design', ''),
                "weight": record.get('weight', ''),
                "height": height,
                "width": width,
                "depth": depth,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price,
                },
            }

            return transformed_record
        else:
            return None
