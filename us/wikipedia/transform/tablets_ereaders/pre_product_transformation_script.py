import hashlib
import re
class Mapper(object):

    def pre_process(self, record):
        processed_record = {}
        for k, v in record.items():
            key = re.sub(r'\[\d+\]', '', k).strip()
            if isinstance(v, str):
                value = re.sub(r'\[\d+\]', '', v).strip()
            elif isinstance(v, list):
                value = []
                for l in v:
                    value.append(re.sub(r'\[\d+\]', '', l).strip())

            processed_record[key] = value
        return processed_record


    def map(self, record):
        if record:
            record = self.pre_process(record)

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('Model', '')

            model_sku = ''
            match = re.findall(r'A\d+,*\s*A*\d* \(.*?\)', record.get('Model Number', ''))
            if match:
                model_skus = []
                for sku in match:
                    n = re.findall(r'\(.*?\)', sku)
                    skus = [f'{s} {n[0]}' if n and n[0] not in s else s for s in sku.split(',')]
                    skus = list(filter(lambda x: 'China model' not in x, skus))
                    model_skus.extend(skus)
                model_sku = ', '.join(model_skus)

            series = ''
            caption = re.sub(r'unknown_\d+', '', record.get('caption', ''), flags=re.IGNORECASE)
            if not caption:
                caption = model
            series_meta = ['iPad Pro', 'iPad Mini', 'iPad Air','iPad']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', series_meta))), caption, flags=re.IGNORECASE)
            if match:
                series = match[0]

            memory_ram_capacity = ''
            match = re.findall(r'(\d+)\s*\[(MiB|GiB|TiB|MB|GB|TB)\]', record.get('Memory', ''), flags=re.IGNORECASE)
            if match:
                memory_ram_capacity = ' '.join(match[0]).replace('i', '')

            storage_capacity = ''
            match = re.findall(r'(\d+)\s*(GB|MB|TB|,)', record.get('Storage', ''), flags=re.IGNORECASE)
            if match:
                unit = 'GB'
                unit_info = list(filter(lambda x: re.findall(r'[a-zA-Z]+', x[1]), match))
                if unit_info:
                    unit = unit_info[0][1]
                storage_capacity = ', '.join(list(map(lambda x: f'{x[0]} {unit}', match)))

            display = '. '.join(record.get('Display', []))
            display = re.sub(r'\[\d+\]', '', display).strip()

            rear_camera_resolution = ''
            match = re.findall(r'\d+\.*\d*\sMP', record.get('Back.Camera', ''))
            if match:
                rear_camera_resolution = match[0]

            front_camera_resolution = ''
            match = re.findall(r'\d+\.*\d*\sMP', record.get('Front.Camera', ''))
            if match:
                front_camera_resolution = match[0]

            sensor_technology = record.get('Environmental sensors', '')
            sensor_technology = re.sub(r'Additionally:|\[\d+\]', '', sensor_technology, flags=re.IGNORECASE)
            sensor_technology = re.sub(r'\s+', ' ', sensor_technology).strip()

            height = ''
            width = ''
            depth = ''

            dimensions = record.get('Dimensions HxWxD', '') or record.get('Dimensions', '')
            match = re.findall(r'(\d+\.*\d*)\Din', dimensions)
            if match and len(match) == 3:
                height = f'{match[0]} in'
                width = f'{match[1]} in'
                depth = f'{match[2]} in'
            else:
                match = re.findall(r'(\d+\.*\d*)\D×\D(\d+\.*\d*)\D×\D(\d+\.*\d*)\Din', dimensions)
                if match:
                    height = f'{match[0][0]} in'
                    width = f'{match[0][1]} in'
                    depth = f'{match[0][2]} in'

            color_meta = ["White", "Black", "Rose Gold", "Gold", "Silver", "Space Gray", "Green", "Sky Blue", ]
            color = ', '.join(color_meta)

            id = record.get('page_url', '') or record.get('id', '')
            id = id.split('?')[0] + '#model=' + re.sub(r'[^a-zA-Z\d]', '', model)

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": model,
                "title": model,
                "image": record.get('Image', ''),

                "model": model,
                "series": series,
                "screen_size": record.get('Screen Size', ''),
                "operating_system": record.get('Highest supported operating system', ''),
                "model_sku": model_sku,
                "release_date": record.get('Release date', ''),
                "motion": record.get('Motion coprocessor', ''),
                "cpu_brand": record.get('CPU', ''),
                "gpu_video_card": record.get('GPU', ''),
                "memory_ram_capacity": memory_ram_capacity,
                "storage_capacity": storage_capacity,
                "display": display,
                "rear_camera_resolution": rear_camera_resolution,
                "front_camera_resolution": front_camera_resolution,
                "sensor_technology": sensor_technology,
                "battery_type": record.get('Battery', ''),
                "height": height,
                "width": width,
                "depth": depth,
                "weight": record.get('Weight', ''),
                "connectors_supported": record.get('Connector', '') or ', '.join(record.get('Connectors', '').split('\n')),
                "speaker_design": record.get('Speakers', ''),
                "color": color,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
