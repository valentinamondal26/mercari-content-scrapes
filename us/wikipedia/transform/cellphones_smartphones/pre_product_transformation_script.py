import hashlib
import re
class Mapper(object):

    def pre_process(self, record):
        processed_record = {}
        for k, v in record.items():
            key = re.sub(r'\[\d+\]', '', k).strip()
            if isinstance(v, str):
                value = re.sub(r'\[\d+\]', '', v).strip()
            elif isinstance(v, list):
                value = []
                for l in v:
                    value.append(re.sub(r'\[\d+\]', '', l).strip())

            processed_record[key] = value
        return processed_record


    def map(self, record):
        if record:
            record = self.pre_process(record)

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('Model', '')
            model = re.sub(r'\(|\)', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()

            storage_capacity = ''
            match = re.findall(r'(\d+)\s*(GB|MB|TB|,)', record.get('Storage', ''), flags=re.IGNORECASE)
            if match:
                unit = 'GB'
                unit_info = list(filter(lambda x: re.findall(r'[a-zA-Z]+', x[1]), match))
                if unit_info:
                    unit = unit_info[0][1]
                storage_capacity = ', '.join(list(map(lambda x: f'{x[0]} {unit}', match)))

            height = ''
            match = re.findall(r'(\d+\.*\d*)\Din', record.get('Dimensions.Height', ''))
            if match:
                height = f'{match[0]} in'

            width = ''
            match = re.findall(r'(\d+\.*\d*)\Din', record.get('Dimensions.Width', ''))
            if match:
                width = f'{match[0]} in'

            depth = ''
            match = re.findall(r'(\d+\.*\d*)\Din', record.get('Dimensions.Depth', ''))
            if match:
                depth = f'{match[0]} in'

            model_sku = ', '.join(list(map(lambda x: x, filter(None, re.sub(r'(A\d+)', r',\1', record.get('Model number', '')).split(',')))))
            
            # match = re.findall(r'(A\d+?)', )
            # if match:
            #     model_skus = []
            #     for sku in match:
            #         n = re.findall(r'\(.*?\)', sku)
            #         skus = [f'{s} {n[0]}' if n and n[0] not in s else s for s in sku.split(',')]
            #         skus = list(filter(lambda x: 'China model' not in x, skus))
            #         model_skus.extend(skus)
            #     model_sku = ', '.join(model_skus)

            rear_camera = ''
            match = re.findall(r'\d+\.*\d*\sMP', record.get('Camera.Rear Camera', ''))
            if match:
                rear_camera = match[0]

            front_camera = ''
            match = re.findall(r'\d+\.*\d*\sMP', record.get('Front Camera.Camera', ''))
            if match:
                front_camera = match[0]

            id = record.get('page_url', '') or record.get('id', '')
            id = id.split('?')[0] + '#model=' + re.sub(r'[^a-zA-Z\d]', '', model)

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            if not model:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": record.get('Model', ''),
                "title": record.get('Model', ''),
                "description": record.get('description', ''),
                "image": record.get('Picture', ''),

                "model": model,
                "screen_size": record.get('Display.Screen Size', ''),
                "resolution": record.get('Display.Resolution', ''),
                "processor_model": record.get('Graphics Processor.Processor', ''),
                "storage_capacity": storage_capacity,
                "ram_size": record.get('RAM', ''),
                "rear_camera": rear_camera,
                "front_camera": front_camera,
                "color": record.get('', ''),

                "height": height,
                "width": width,
                "depth": depth,
                "weight": record.get('Weight', ''),
                "model_sku": model_sku,
                "release_date": record.get('Release date', ''),

                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
