class Mapper:
    def map(self, record):

        model = ''
        model_sku = ''
        storage_capacity = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            elif "model_sku" == entity['name'] and entity["attribute"]:
                model_sku = entity["attribute"][0]["id"]
            elif "storage_capacity" == entity['name'] and entity["attribute"]:
                storage_capacity = entity["attribute"][0]["id"]
            elif "color" == entity['name'] and entity["attribute"]:
                color = entity["attribute"][0]["id"]

        key_field = model + model_sku + storage_capacity + color
        return key_field
