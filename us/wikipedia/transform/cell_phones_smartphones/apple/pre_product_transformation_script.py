import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = ''

            model = re.sub(r"\(.*\)","",record.get('title', ''))

            rear_camera = re.findall(r"\d+\s*MP",record.get("rear_camera",""))
            if rear_camera:
                rear_camera = rear_camera[0]
            else:
                rear_camera = ''
            front_camera = re.findall(r"\d+\s*MP",record.get("front_camera",""))
            if front_camera:
                front_camera = front_camera[0]
            else:
                front_camera = ''

            model_sku = "A"+record.get('model_sku','')[1:].replace("A",",A")

            height = re.sub(r"\u00a0mm\s*\(.*\)"," in",record.get("height", ''))
            width = re.sub(r"\u00a0mm\s*\(.*\)"," in",record.get("width", ''))
            depth = re.sub(r"\u00a0mm\s*\(.*\)"," in",record.get("depth", ''))

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),

                "model": model,
                "screen_size": record.get('screen_size', ''),
                "resolution": record.get('resolution', ''),
                "storage_capacity": record.get('storage_capacity', ''),
                "color": record.get('color', ''),
                "processor_model": record.get('processor_model', ''),
                "ram_size": record.get('ram_size', ''),
                "model_sku": model_sku,
                "rear_camera": rear_camera,
                "front_camera": front_camera,
                "height": height,
                "width": width,
                "depth": depth,
                "weight": record.get("weight", ''),
                "release_date": record.get("release_date", ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price,
                },
            }

            return transformed_record
        else:
            return None
