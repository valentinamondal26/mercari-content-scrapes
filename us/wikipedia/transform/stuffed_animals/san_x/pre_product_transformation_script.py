import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            model = model.replace('Beer-chan', 'Beer Chan') \
                .replace('Chibi Moomo.', 'Chibi Moomo') \
                .replace('Chokotto Iikoto!', 'Chokotto Iikoto')
            model = model.strip()

            description = record.get('description', '')
            description = re.sub(r'\[\d+\]', '', description)
            description = re.sub(r'\s+', ' ', description).strip()
            description = description.capitalize()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "title": record.get('title', ''),
                "ner_query": ner_query,
                "description": description,

                "product_type": record.get('product_type', ''),
                "model": model,
            }

            return transformed_record
        else:
            return None
