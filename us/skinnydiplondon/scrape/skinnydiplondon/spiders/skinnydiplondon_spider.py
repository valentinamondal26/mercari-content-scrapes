'''
https://mercari.atlassian.net/browse/USCC-719 - Skinnydip London Backpack Purses for Women
https://mercari.atlassian.net/browse/USCC-742 - Skinnydip London Women's Bags & Handbags
https://mercari.atlassian.net/browse/USCC-743 - Skinnydip London Women's Satchel HandBags
https://mercari.atlassian.net/browse/USCC-744 - Skinnydip London Bucket Bags & Handbags for Women
https://mercari.atlassian.net/browse/USCC-745 - Skinnydip London Clutch Vintage Bags, Handbags & Cases
https://mercari.atlassian.net/browse/USCC-746 - Skinnydip London Women's T-Shirts
https://mercari.atlassian.net/browse/USCC-747 - Skinnydip London Women's Swimwear
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class SkinnydiplondonSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from skinnydiplondon.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Backpack Purses for Women
        2) Women's Bags & Handbags
        3) Women's Satchel HandBags
        4) Bucket Bags & Handbags for Women
        5) Clutch Vintage Bags, Handbags & Cases
        6) Women's T-Shirts
        7) Women's Swimwear
    
    brand : str
        brand to be crawled, Supports
        1) Skinnydip London

    Command e.g:
    scrapy crawl skinnydiplondon -a category="Backpack Purses for Women" -a brand="Skinnydip London"
    scrapy crawl skinnydiplondon -a category="Women's Bags & Handbags" -a brand="Skinnydip London"
    scrapy crawl skinnydiplondon -a category="Women's Satchel HandBags" -a brand="Skinnydip London"
    scrapy crawl skinnydiplondon -a category="Bucket Bags & Handbags for Women" -a brand="Skinnydip London"
    scrapy crawl skinnydiplondon -a category="Clutch Vintage Bags, Handbags & Cases" -a brand="Skinnydip London"
    scrapy crawl skinnydiplondon -a category="Women's T-Shirts" -a brand="Skinnydip London"
    scrapy crawl skinnydiplondon -a category="Women's Swimwear" -a brand="Skinnydip London"
    """

    name = 'skinnydiplondon'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'skinnydiplondon.com'
    blob_name = 'skinnydiplondon.txt'

    base_url = 'https://www.skinnydiplondon.com'

    url_dict = {
        "Backpack Purses for Women": {
            "Skinnydip London": {
                'url': 'https://www.skinnydiplondon.com/collections/backpacks',
            }
        },
        "Women's Bags & Handbags": {
            "Skinnydip London": {
                'url': [
                    'https://www.skinnydiplondon.com/collections/tote-bags',
                    'https://www.skinnydiplondon.com/collections/printed-tote-bag',
                ]
            }
        },
        "Women's Satchel HandBags": {
            "Skinnydip London": {
                'url': 'https://www.skinnydiplondon.com/collections/shoulder-bags',
            }
        },
        "Bucket Bags & Handbags for Women": {
            "Skinnydip London": {
                'url': 'https://www.skinnydiplondon.com/collections/bum-bags',
            }
        },
        "Clutch Vintage Bags, Handbags & Cases": {
            "Skinnydip London": {
                'url': [
                    'https://www.skinnydiplondon.com/collections/clutch-bags',
                    'https://www.skinnydiplondon.com/collections/card-holders-purses',
                ]
            }
        },
        "Women's T-Shirts": {
            "Skinnydip London": {
                'url': 'https://www.skinnydiplondon.com/collections/t-shirts',
            }
        },
        "Women's Swimwear": {
            "Skinnydip London": {
                'url': 'https://www.skinnydiplondon.com/collections/swimwear',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SkinnydiplondonSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.skinnydiplondon.com/collections/tote-bags/products/powerpuff-girls-x-skinnydip-mini-tote?nosto_source=cmp&nosto=60b8d98de0f2371e2690448f
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title price description features details
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//nav[@class="breadcrumb"]//text()').getall(),
            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'title': response.xpath('//h1[@class="product-form__title h3"]/text()').get(),
            'price': response.xpath('//span[@js-product-form="price"]/text()').get(),
            'description': response.xpath('//div[@class="accordion__content"]/p/text()').get(),
            'features': response.xpath('//div[@class="accordion__content"]/ul/li/text()').getall(),
            'details': response.xpath('//div[@class="accordion__content"]/p//text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.skinnydiplondon.com/collections/tote-bags
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[contains(@class, "card-grid ")]/div[@class="card-grid__item"]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//link[@rel="next"]/@href').extract_first()
        if next_page_link:
            next_page_link = self.base_url + next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
