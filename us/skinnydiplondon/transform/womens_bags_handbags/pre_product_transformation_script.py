import hashlib
import re
from titlecase import titlecase
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            features = '. '.join(record.get('features', []))
            details = '. '.join(record.get('details', []))
            ner_query = (description + ' ' + features + ' ' + details).replace('\n', '').replace('\r', '') or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\bx Skinnydip\b|\bSkinnydip\b|\bBag\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            match = re.findall(r'\d+%\s*(\w+)', details)
            def handle_case(word, all_caps):
                if word.upper() == 'PU':
                    return 'PU'

            if match:
                material = '/'.join(list(dict.fromkeys(list(map(lambda x: titlecase(x, callback=handle_case), match)))))
            if not material:
                material_meta =['Cotton', 'Polyester', 'PU', 'Canvas']
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', material_meta))), details, flags=re.IGNORECASE)
                if match:
                    material = '/'.join(list(dict.fromkeys(list(map(lambda x: titlecase(x, callback=handle_case), match)))))

            width = ''
            height = ''
            depth = ''
            match = re.findall(r'W(\d+\.*\d*)cm x H(\d+\.*\d*)cm x D(\d+\.*\d*)cm', details)
            if match:
                width, height, depth = match[0]
                width += ' cm'
                height += ' cm'
                depth += ' cm'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query.strip(),
                "title": record.get('title', '').strip(),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                'width': width,
                'height': height,
                'depth': depth,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
