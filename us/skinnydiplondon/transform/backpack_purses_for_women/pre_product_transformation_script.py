import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            features = '. '.join(record.get('features', []))
            details = '. '.join(record.get('details', []))
            ner_query = (description + ' ' + features + ' ' + details).replace('\n', '').replace('\r', '') or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')

            material = ''
            match = re.findall(r'\d+%\s*(\w+)', details)
            if match:
                material = '/'.join(match)

            width = ''
            height = ''
            depth = ''
            match = re.findall(r'W(\d+\.*\d*)cm x H(\d+\.*\d*)cm x D(\d+\.*\d*)cm', details)
            if match:
                width, height, depth = match[0]
                width += ' cm'
                height += ' cm'
                depth += ' cm'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                'width': width,
                'height': height,
                'depth': depth,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
