class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        frame_color = ''
        lens_color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "frame_color" == entity['name']:
                if entity["attribute"]:
                    frame_color = entity["attribute"][0]["id"]
            elif "lens_color" == entity['name']:
                if entity["attribute"]:
                    lens_color = entity["attribute"][0]["id"]

        key_field = model + frame_color + lens_color
        return key_field
