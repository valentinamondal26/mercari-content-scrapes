import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            specs = record.get('specs', {})

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            glasses_sizes = record.get('sizes', [])

            model = specs.get('Name:', '')
            model = re.sub(re.compile(r'™|®|\bXL\b', re.IGNORECASE), '', model)
            if glasses_sizes:
                model = re.sub(re.compile(pattern_words(glasses_sizes), re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()
            model = re.sub(re.compile(r'RB.*?\b', re.IGNORECASE), lambda x: x.group().upper(), model)
            model = re.sub(r'\bIi\b', 'II', model)

            description = record.get('description', '')
            description = re.sub('®', '', description)
            description = re.sub(r'\s+', ' ', description).strip()

            if record.get('brand', '').lower() == "Ray-Ban".lower() and not "Ray-Ban".lower() == record.get('brand_name', '').lower():
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "description": description,
                'upc': specs.get('UPC:', ''),
                'glasses_size': ','.join(glasses_sizes),

                'frame_shape': specs.get('Shape:', ''),
                'frame_material': specs.get('Frame material:', ''),
                'lens_material': specs.get('Lens material:', ''),
                'frame_color': specs.get('Frame color:', ''),
                'lens_color': specs.get('Lens color:', ''),
                'head_shape': specs.get('Looks best on these face shapes:', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
