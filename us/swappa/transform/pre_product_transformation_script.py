import hashlib
from datetime import datetime


class Mapper(object):

    def date_conversion(self, price_history):
        if price_history is None:
            return {}
        else:
            date_format = "%b %d, %Y"
            for price in price_history:
                date = price['date']
                date_new = datetime.strptime(date, date_format)
                date_new = date_new.strftime("%Y-%m-%d %H:%M:%S")
                price['date'] = date_new
            return price_history

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(str(v))>0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            ner_query = record['condition']
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price_history = self.date_conversion(price_history=record['price_history'])
            sold_price_history = self.date_conversion(price_history=record['sold_price_history'])

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawlDate": record['crawlDate'],
                "statusCode": record['statusCode'],
                "title": record['title'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "condition": record['condition'],
                "ner_query": ner_query,
                "breadcrumb" : record['breadcrumb'],
                "color" : record['color'],
                "storage": record['storage'],
                "memory" : record['memory'],
                "model": record['model'],
                "year": record['year'],
                "screen_size": record['screen_size'],
                "platform": record['platform'],
                "type": record['type'],
                "sold_price_history": sold_price_history,
                "priceHistory": price_history
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None
