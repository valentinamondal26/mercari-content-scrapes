import scrapy
import random
from datetime import datetime
import os
from scrapy.exceptions import CloseSpider
import re


class SwappaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from swappa.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Laptops & Netbooks

    sub_category : str
        sub-category to be crawled, Supports
        1) Macbooks
        2) Chromebooks
        3) All Laptops
        4) Windows

    Command e.g:
    scrapy crawl swappa -a category='Laptops & Netbooks' -a sub_category='Macbooks'
    scrapy crawl swappa -a category='Laptops & Netbooks' -a sub_category='Chromebooks'
    scrapy crawl swappa -a category='Laptops & Netbooks' -a sub_category='All Laptops'
    scrapy crawl swappa -a category='Laptops & Netbooks' -a sub_category='Windows'
    """

    name = "swappa"
    category = None
    brand = None
    source = 'swappa.com'
    blob_name = 'swappa.txt'

    sub_category = None

    proxy_pool = [
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
    ]

    url_dict = {
        'Laptops & Netbooks' : {
            'Macbooks' : {
                'url': 'https://swappa.com/laptops/macbooks/search',
            },
            'Chromebooks': {
                'url': 'https://swappa.com/laptops/chromebooks/search',
            },
            'All Laptops': {
                'url': 'https://swappa.com/laptops/search',
            },
            'Windows': {
                'url': 'https://swappa.com/laptops/windows/search',
            },
        }
    }


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(self.launch_url, self.parse)


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SwappaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and sub_category should not be None")
            raise CloseSpider('category and sub_category not found')

        self.category = category
        self.sub_category = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and sub_category:{}'.format(category, self.sub_category))
            raise(CloseSpider('Spider is not supported for the category:{} and sub_category:{}'.format(category, self.sub_category)))

        self.logger.info("Crawling for category: {category} and sub-category/brand: {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.launch_url = 'https://swappa.com/laptops/chromebooks/search'


    def get_url(self,category,sub_category):
        try:
            return self.url_dict[category][sub_category]
        except Exception as e:
            self.logger.error('Exception occured:', e)
            raise(ValueError('Spider is not supported for the category:{} and sub_category:{}'.format(category, sub_category)))


    def parse_detail_page(self, response):
        """
        @url https://swappa.com/laptops/buy/google-pixelbook-go
        @meta {"use_proxy":"True"}
        @scrape_values title image breadcrumb color condition storage memory model year screen_size platform type price_history sold_price_history
        """
        title = response.xpath(u'normalize-space(//div[@class="billboard_container"]/h1/text())').extract_first(default='')
        brand = title.split(' ', 1)[0]
        year = ''.join(re.findall(r'\d{4}', title) or '')
        screen_size = ''.join(re.findall(r'[0-9]+["\']', title) or '')
        breadcrumb = ' | '.join([a.xpath(u'normalize-space(.//text())').extract_first(default='') for a in response.xpath('//ul[@class="breadcrumb"]//a')])
        laptop_type = ''
        platform = ''
        breadcrumb_lower = breadcrumb.lower()
        if 'chromebook' in breadcrumb_lower:
            laptop_type = 'Chromebook'
            platform = 'Chromebook'
        elif 'macbook' in breadcrumb_lower:
            laptop_type = 'Macbook'
            platform = 'MacOS / OS X'
        elif 'windows' in breadcrumb_lower:
            laptop_type = 'Windows'
            platform = 'Windows'

        item_details = dict()

        # listed item details
        for listed_item in response.xpath('//div[@class="listing_row listing_None listing_None"]'):
            self.get_dict(listed_item, item_details, is_sold=False)

        # recently sold item details
        for sold_item in response.xpath('//div[@class="listing_row"]'):
            self.get_dict(sold_item, item_details, is_sold=True)

        for key, item_detail in item_details.items():
            model = '{title} {storage}'.format(title=title, storage=item_detail.get('storage', ''))

            item = {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': brand,
                'title': title,
                'id': response.url,
                'image': response.xpath('//img[@class="img-responsive product_img"]/@src').extract_first(default=''),
                "breadcrumb": breadcrumb,
                "color": item_detail.get('color', ''),
                "condition": item_detail.get('condition', ''),
                'storage': item_detail.get('storage', ''),
                'memory': item_detail.get('memory', ''),
                'model': model,
                'year': year,
                'screen_size': screen_size,
                'platform': platform,
                'type': laptop_type,
                'price_history': item_detail.get('price', []),
                'sold_price_history': item_detail.get('sold_price_history', []),
            }

            yield item


    def get_dict(self, item_details_sel, item_details, is_sold):
        listing_info=item_details_sel.xpath('.//div[@class="listing_info"]')
        price = listing_info.xpath('.//span[@class="price"]/text()').extract_first()

        condition = ''
        color = ''
        processor = ''
        storage = ''
        memory = ''
        date = ''

        other_infos = listing_info.xpath('.//div[@class="col-xs-12 col-md-10"]/span[not(@class)]/text()').extract()
        condition = listing_info.xpath('.//span[@class="condition_label"]/text()').extract_first(default='')
        color = listing_info.xpath('.//span[@class="color_label"]/text()').extract_first(default='')
        storage = listing_info.xpath('.//span[@class="storage_label"]/text()').extract_first(default='')
        
        index = -1
        if is_sold:
            date = other_infos[index]
            if date:
                date = date.replace('Sold on ', '')
            index = index - 1
        memory = other_infos[index]
        index = index - 1

        processor = other_infos[index] if len(other_infos) > 2 else ''

        # infos = listing_info.xpath('.//div[@class="col-xs-12 col-md-10"]/span/text()').extract()
        # if len(infos) > 4:
        #     condition = infos[0]
        #     color = infos[1]
        #     processor = infos[2]
        #     storage = infos[3]
        #     memory = infos[4]

        # if is_sold and len(infos) > 5 and infos[5] is not None:
        #     date = infos[5].replace('Sold on ', '')

        if not is_sold:
            date = datetime.today().strftime("%b %d, %Y")

        key = condition + storage + memory + processor + color
        if key not in item_details or item_details[key] is None:
            item_details[key] = {
                'condition': condition,
                'storage': storage,
                'memory': memory,
                'processor': processor,
                'color': color,
                'sold_price_history': [],
                'price': [],
            }
        item_details.get(key, {}).get('sold_price_history' if is_sold else 'price', []).append({
            'currency': 'USD',
            'amount': price,
            'date': date
        })


    def parse(self, response):
        """
        @url https://swappa.com/laptops/chromebooks/search
        @meta {"use_proxy":"True"}
        @returns requests 61
        """
        base_url = 'https://swappa.com'
        products = response.xpath('//div[@class="col-xs-6 col-sm-4 col-md-5ths col-lg-2"]')
        for product in products:
            detail_page = product.xpath('.//a/@href').extract_first()
            if detail_page:
                detail_page_url = base_url + detail_page
                self.logger.info('detail_page_url: {}'.format(detail_page_url))
                yield self.create_request(url=detail_page_url, callback=self.parse_detail_page)

        next_page = response.xpath('//a[@aria-label="Next Page"]/@href').extract_first()
        if next_page:
            next_page_url = self.launch_url + next_page
            self.logger.info('next_page_url: {}'.format(next_page_url))
            yield self.create_request(url=next_page_url, callback=self.parse)


    def create_request(self, url, callback):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request