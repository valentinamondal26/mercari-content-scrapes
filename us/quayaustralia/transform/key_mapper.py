class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        frame_shape = ''
        lens_color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "frame_shape" == entity['name']:
                for attribute in entity["attribute"]:
                    frame_shape += attribute["id"]
            elif "lens_color" == entity['name']:
                if entity["attribute"]:
                    lens_color = entity["attribute"][0]["id"]

        key_field = model + frame_shape + lens_color
        return key_field
