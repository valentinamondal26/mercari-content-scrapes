import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '').replace('\n', '').strip()
            ner_query = description or record.get('item_name', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # measurements = {}
            measurements = {m.split(':')[0].strip():m.split(':')[-1].strip() for m in record.get('measurements', {})}

            color = record.get('color', '')
            color_meta = {
                'BLK': 'Black',
                'BLKFD': 'Black Fade',
                'SHNYBLK': 'Shiny Black',
                'BLKGLD': 'Black Gold',
                'BLKGRY': 'Black Gray',
                'BLKTOFF': 'Black Toffee',
                'BLKTORT': 'Black Tortoise',
                'BLSH': 'Blush',
                'BLU': 'Blue',
                'BLUEFD': 'Blue Fade',
                'BLUFLS': 'Blue FLS',
                'BLULIL': 'Blue Lilac',
                'BLUPURP': 'Blue Purple',
                'BLURED': 'Blue Red',
                'BLUTURQ': 'Blue Turquoise',
                'BNZ': 'Bronze',
                'BNZBLK': 'Bronze Black',
                'BRN': 'Brown',
                'BRNFD': 'Brown Fade',
                'BRNFADE': 'Brown Fade',
                'BRNFLS': 'Brown FLS',
                'BRNPNK': 'Brown Pink',
                'BRNPNKFLS': 'Brown Pink FLS',
                'CHAM': 'Champagne',
                'CLR': 'Clear',
                'BLKCLR': 'Black Clear',
                'CLRBLT': 'Clear Blue Light Technology ',
                'CORL': 'Coral',
                'NEONCORL': 'Neon Coral',
                'CPR': 'Copper',
                'CPRFD': 'Copper Fade',
                'DARKBLUE': 'Dark Blue',
                'DARKRED': 'Dark Red',
                'FADE': 'Fade',
                'FD': 'Fade',
                'GLD': 'Gold',
                'GRN': 'Green',
                'GRNFD': 'Green Fade',
                'GRNPNK': 'Green Pink',
                'GRNPNKBLT': 'Green Pink Blue Light Technology',
                'BLUEBLT': 'Blue Blue Light Technology',
                'GRY': 'Gray',
                'GREY': 'Gray',
                'GUN': 'Gunmetal',
                'LIL': 'Lilac',
                'LTPNK': 'Light Pink',
                'MATTEBLK': 'Matte Black',
                'MATTEBLACK': 'Matte Black',
                'MATTEGUN': 'Matte Gunmetal',
                'MATTETORT': 'Matte Tortoise',
                'MATTEWHT': 'Matte White',
                'MILKYCAMO': 'Milky Camo',
                'MILKYTORT': 'Milky Tortoise',
                'MLKYTORTRED': 'Milky Tortoise Red',
                'NAVY': 'Navy FLS',
                'NAVYFLS': 'Navy FLS',
                'NAVYPCH': 'Navy peach',
                'NAVYPNK': 'Navy Pink',
                'NAVYTORT': 'Navy Tortoise',
                'NEONYELL': 'Neon Yellow',
                'NEONPINK': 'Neon Pink',
                'NVYPCH': 'Navy Peach',
                'ORNGTORT': 'Orange Tortoise',
                'ORNG': 'Orange',
                'PNK': 'Pink',
                'PNKCLR': 'Pink Clear',
                'PNKGLD': 'Pink Gold',
                'PNKPURP': 'Pink Purple',
                'PNKRNBW': 'Pink Rainbow',
                'POL': 'Polarized',
                'BRNPOL': 'Brown Polarized',
                'PRL': 'Pearl',
                'PRLPNK': 'Pearl Pink',
                'PRLBLU': 'Pearl Blue',
                'PURP': 'Purple',
                'PURPFD': 'Purple Fade',
                'PURPNK': 'Purple Pink',
                'PURPNKFD': 'Purple Pink Fade',
                'PURPNKYEL': 'Purple Pink Yellow',
                'PURPNKYLW': 'Purple Pink Yellow',
                'RNBW': 'Rainbow',
                'ROSEBLT': 'Rose Blue Light Technology',
                'SHINYBLK': 'Shiny Black',
                'SLV': 'Silver',
                'BLKSLV': 'Black Silver',
                'SMK': 'Smoke',
                'SMKFD': 'Smoke Fade',
                'SMKFLS': 'Smoke FLS',
                'SMKPOL': 'Smoke Polarized',
                'SMKRNBW': 'Smoke Rainbow',
                'SMKTPE': 'Smoke Taupe',
                'TOFF': 'Toffee',
                'TOFFPURP': 'Toffee Purple',
                'TORT': 'Tortoise',
                'PNKTORT': 'Pink Tortoise',
                'TORTBLK': 'Tortoise Black',
                'TORTBRN': 'Tortoise Brown',
                'TORTCLR': 'Tortoise Clear',
                'TORTGLD': 'Tortoise Gold',
                'TORTGLDSTUD': 'Gold Studded Tortoise',
                'BLKGLDSTUD': 'Gold Studded Black',
                'TORTGOLD': 'Tortoise Gold',
                'Tortise': 'Tortoise',
                'TORTPURP': 'Tortoise Purple',
                'TORTSTUD': 'Tortoise Studded',
                'BLKSTUD': 'Black Studded',
                'TPE': 'Taupe',
                'TURQ': 'Turquoise',
                'WHT': 'White',
                'WHTFLS': 'White FLS',
                'WHTGLD': 'White Gold',
                'YELL': 'Yellow',
                'YELLBLT': 'Yellow Blue Light Technology',
                'YLW': 'Yellow',
                'GLDROPE': 'Gold Rope',
                'BLKROPE': 'Black Rope',
                'OLV': 'Olive',
                'GLDGLT': 'Gold Glitter',
                'MATTEBRNZ': 'Matte Bronze',
                'LTBLUE': 'Light Blue',
                'COFF': 'Coffee',
            }
            # for color_code, color_name in color_meta.items():
            #     color = re.sub(re.compile(r'\b'+color_code+r'\b', re.IGNORECASE), color_name, color)
            pattern = r'\b({})\b'.format('|'.join(sorted(re.escape(k) for k in color_meta)))
            color = re.sub(re.compile(pattern, re.IGNORECASE), lambda m: color_meta.get(m.group(0).upper()), color)
            color = re.sub(re.compile(r'Blue light technology| Polarized|\bBLT\b|\bFLS\b', re.IGNORECASE), '', color)
            color = re.sub(' +', ' ', color)
            frame_color = color.split('/')[0].strip().title()
            lens_color = color.split('/')[-1].strip().title()


            materials = record.get('materials', '')
            material = ', '.join(materials[2:])
            material = material.title()

            model = record.get('item_name', '')
            model = titlecase(model)
            model = re.sub(r'\b([LXIVCDM]+)\b', lambda x: x.group(0).upper(), model, flags=re.IGNORECASE)
            model = re.sub(r'\botl\b', 'OTL', model, flags=re.IGNORECASE)
            model = re.sub(r"\bI'M\b", "I'm", model, flags=re.IGNORECASE)

            lens_type = record.get('Lens-Type', '')
            if not lens_type and materials:
                lens_type = materials[0].title()

                d = {
                    'Blue Light': 'Bluelight',
                    'Non-Mirored': 'Non-Mirrored',
                    'Non-Mirroed': 'Non-Mirrored',
                }
                lens_type = re.sub(re.compile(r'|'.join(d.keys()), re.IGNORECASE), lambda x: d.get(x.group(), ''), lens_type)

            if '&' in lens_type:
                for t in lens_type.split('&'):
                    match = re.findall(re.compile('|'.join(list(map(lambda x: r'(.*)\s*\('+x+r'\)', [lens_color, frame_color]))), re.IGNORECASE), t)
                    if match:
                        lens_type = ', '.join(filter(None, match[0])).strip()
                        break

            lens_type = ', '.join(sorted(list(map(lambda x: x.title(),lens_type.split(', ')))))

            if re.findall(re.compile(r'\bgift\b|\bset\b', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawl_date', ''),
                "statusCode": record.get('status_code', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('item_name', ''),
                'image': record.get('image', ''),

                "model": model,
                "description": description,
                # "color": record.get('color', ''),
                "frame_color": frame_color,
                "lens_color": lens_color,
                "material": material,
                'frame_shape': record.get('Frame-Shape', ''),
                'lens_type': lens_type,
                'width': measurements.get('WIDTH', ''),
                'height': measurements.get('HEIGHT', ''),
                'bridge_size': measurements.get('NOSE GAP', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
