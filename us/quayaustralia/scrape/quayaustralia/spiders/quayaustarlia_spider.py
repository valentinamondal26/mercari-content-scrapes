'''
https://mercari.atlassian.net/browse/USDE-834 - Quay Australia Sunglasses
'''

import scrapy
import json
from datetime import datetime
import re
import os
from scrapy.exceptions import CloseSpider
import random


class QuayaustraliaSpider(scrapy.Spider):
    """
     spider to crawl items in a provided category and sub-category from QuayAustralia.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Sunglasses


     brand : str
      brand to be crawled, Supports
       1) Quay Australia

     Command e.g:
     scrapy crawl quayaustralia  -a category='Sunglasses' -a brand='Quay Australia'

    """
    name = 'quayaustralia'

    category = None
    brand = None

    merge_key = 'id'

    source = 'quayaustralia.com'
    blob_name = 'quayaustralia.txt'

    filters = None
    launch_url = None

    base_url = 'https://www.quayaustralia.com'

    url_dict={
        'Sunglasses': {
            'Quay Australia': {
                'url': [
                    'https://www.quayaustralia.com/collections/women-sunglasses',
                    'https://www.quayaustralia.com/collections/blue-light-computer-glasses',
                ],
                'filter': ['Lens-Type', 'Frame-Shape']
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(QuayaustraliaSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
            self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filter', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Lens-Type', 'Frame-Shape']


    def start_requests(self):
        if self.launch_url:
            for launch_url in self.launch_url:
                yield self.create_request(url=launch_url, callback=self.parse_page)
                yield self.create_request(url=launch_url, callback=self.parse_filter_url)


    def parse_page(self,response):
        """
        @url https://www.quayaustralia.com/collections/blue-light-computer-glasses
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        items_urls = response.xpath('//div[@class="grid__item large--one-third medium--one-half small--one-whole product-grid-item"]/a/@href').getall()
        meta = response.meta
        for url in items_urls:
            if "https" not in url:
                url = self.base_url+url
                yield self.create_request(url, callback=self.parse_items, meta=meta)

        next_page_url  = response.xpath("//ul[@class='pagination-custom']/li/a[contains(@title,'Next')]/@href").get(default='')
        if next_page_url:
            if "https" not in next_page_url:
                next_page_url = self.base_url+ next_page_url
            yield self.create_request(next_page_url, callback=self.parse_page, meta=meta)


    def parse_filter_url(self, response):
        """
        @url https://www.quayaustralia.com/collections/women-sunglasses
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        for filter in self.filters:
            filter_urls = response.xpath("//div[@class='filter-content']/ul[contains(@class,'"+filter.lower()+"')]/li/div/a/@href").getall()
            filter_names = response.xpath("//div[@class='filter-content']/ul[contains(@class,'"+filter.lower()+"')]/li/div/a/text()").getall()
            meta = {"filter":filter}
            for url,name in zip(filter_urls,filter_names):
                meta.update({meta["filter"]:name})
                if "https" not in url:
                    url = self.base_url + url
                yield self.create_request(url, meta=meta, callback=self.parse_page)


    def parse_items(self,response):
        """
        @url https://www.quayaustralia.com/collections/blue-light-computer-glasses/products/gotta-run-blue-sale?variant=28112474472534
        @meta {"use_proxy":"True"}
        @scrape_values item_name price image materials measurements description color sku
        """

        meta = response.meta
        crawl_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item_name = response.xpath("//div[@class='product__header']/h1/text()").get(default='').strip()
        price = response.xpath("//div[@class='product__header']/h2/span/span/text()").get(default='').strip()
        image = response.xpath("//picture/source/@data-lazy-srcset").get(default='')
        if "https" not in image:
            image = "https:" + image
        materials = response.xpath("//section[@id = 'product_mat_content']/div/p/text()").getall()
        measurements = response.xpath("//section[@id = 'product_mes_content']/div/p/text()").getall()
        description = response.xpath("//meta[@name='twitter:description']/@content").get(default='')
        if not description:
            description = response.xpath("//meta[@property='og:description']/@content").getall()

        # colors = response.xpath("//div[@id='ProductThumbs']/label/span/text()").getall()
        for variant in json.loads(re.findall('var meta = (.*)?;', response.xpath('//script[contains(text(), "var meta = ")]/text()').extract_first())[0]).get('product', {}).get('variants', []):
            variant_id = str(variant.get('id', ''))
            color = variant.get('public_title', '')
            sku = variant.get('sku', '')
            price = str(variant.get('price', 0)/100)

            item_data= {
                "crawl_date": crawl_date,
                "item_name": item_name,
                "price": price,
                "image": image,
                "id": response.url.split('?')[0]+'?variant='+variant_id,
                "description": description,
                "brand": self.brand,
                "category": self.category,
                "materials": materials,
                "color": color,
                "sku": sku,
                "measurements": measurements,
                "status_code": str(response.status)
            }
            meta_data={}

            filter_name =  meta.get("filter","")
            if filter_name:
                meta_data = meta_data.fromkeys([filter_name],meta[filter_name])
                item_data.update(meta_data)
            yield item_data


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
