# -*- coding: utf-8 -*-

# Scrapy settings for quayaustralia project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'quayaustralia'

SPIDER_MODULES = ['quayaustralia.spiders']
NEWSPIDER_MODULE = 'quayaustralia.spiders'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'

ROBOTSTXT_OBEY = True

CONCURRENT_REQUESTS = 16

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
   'common.pipelines.GCSPipeline': 300,
}
FEED_FORMAT = 'jsonlines'

GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'


DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}