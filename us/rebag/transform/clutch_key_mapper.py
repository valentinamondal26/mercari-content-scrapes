#Key Mapper for Rebag data - model + color + material combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''
        material = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            if "color" == entity['name']:
                color = entity["attribute"][0]["id"]

        key_field = model + color
        return key_field
