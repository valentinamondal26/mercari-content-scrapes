#Key Mapper for Rebag data - color + material combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''
        material = ''

        entities = record['entities']
        for entity in entities:
            if "color" == entity['name']:
                if entity['attribute']!=[]:
                    color = entity["attribute"][0]["id"]
            if "material" == entity['name']:
                if entity['attribute']!=[]:
                    material = entity["attribute"][0]["id"]

        key_field = model + color + material
        return key_field
