import hashlib 
import re

class Mapper(object):

    def multireplace(self,string, replacements, ignore_case=True):
        if ignore_case:
            def normalize_key(s):
                return s.lower()

            re_mode = re.IGNORECASE

        else:
            def normalize_key(s):
                return s

            re_mode = 0

        replacements = {normalize_key(key): val for key, val in replacements.items()}
                
        rep_sorted = sorted(replacements, key=len, reverse=True)
        rep_escaped = map(re.escape, rep_sorted)
        
        pattern = re.compile("|".join(rep_escaped), re_mode)
        
        # For each match, look up the new string in the replacements, being the key the normalized old string
        return pattern.sub(lambda match: replacements[normalize_key(match.group(0))], string)



    def map(self, record):
        if record:
            ner_query= record['description']+" " +record['condition']+" "+record.get('material','')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            est_retail = record.get('est_retail', '')
            est_retail = est_retail.replace("$", "").strip()

            brand = record.get('brand', '')
            title = record.get('title', '')
            condition = record.get('condition', '')
            description = record.get('description', '')

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            meta_materials = ["Coated Canvas","Acrylic","Alligator","Beaded","Calf Hair","Canvas Coated","Canvas Plain","Crocodile","Denim","Exotic","Fabric","Felt","Fur","Jersey","Leather","Lizard","Metal","Nylon","Ostrich","Patent Leather","Patent Vinyl","Pony Hair","Pvc","Python","Raffia","Rhinestone","Satin","Sequins","Shearling","Snakeskin","Stingray","Straw","Suede","Tweed","Velvet","Vinyl","Wool","Epi","Perforated","Taiga","Empreinte","Graphic Leather","Mahina","Whipstitch","Canvas","Printe*d*"]
            #meta_colors = ["Black","Blue","Brown","Gold","Gray","Green","Metallic","Multicolor","Neutral","Orange","Pink","Print","Purple","Red","Silver","White","Yellow"]
            meta_conditions = ["Pristine","Excellent","Great","Very Good","Good","Fair"]
            meta_sizes = ["Small", "Medium", "Large"]
            meta_category_names = ["Backpacks","Belt Bag","Bowler","Briefcases","Bucket","Clutches","Cross Body Bags","Duffles","Hobos","Messenger","Satchels","Shoulder Bag(s){0,1}","Small Goods","Top Handle","Totes","Wallets","Waist Bag","Wallets","wallet","tote","tote bags","Clutch","Bag","Handbag","backpack"]
    
            item_info = record.get('item_info', '')
            
            # colors = re.findall(re.compile(f'{pattern_words(meta_colors)}', re.IGNORECASE), item_info)
            # if not colors:
            #     colors = re.findall(re.compile(f'{pattern_words(meta_colors)}', re.IGNORECASE), description)
            # materials = re.findall(re.compile(f'{pattern_words(meta_materials)}', re.IGNORECASE), item_info)
            # if not materials:
            #     materials = re.findall(re.compile(f'{pattern_words(meta_materials)}', re.IGNORECASE), title)
            
            # material = ', '.join(materials)
            # material = self.multireplace(material,
            #  {'Patent Leather': 'Leather', 'Patent Vinyl': 'Vinyl', 'Canvas Plain': 'Canvas', 'Canvas Coated': 'Canvas', 'Calf Leather':'Leather','Canvas and Leather':'Canvas, Leather'})
            # material = self.multireplace(material,
            #  {'Leather, Leather': 'Leather'})
            
            material=''
            if record.get('Exterior Material','')!='':
                material=record.get('Exterior Material','')
            # if record.get('Interior Material','')!='':
            #     material+=", "+record.get('Interior Material','')
            measurements=record.get('measurements','')
            pattern=re.compile(r"\d+\.*\d*\s*\"\s*[W|H|D]\s*x*")
            measurements=' '.join(re.findall(pattern,measurements))
            # if record.get('Interior Color','')!='':
            #     color=record.get('Interior Color','')
            color =''
            if record.get('Exterior Color','')!='':
                color=record.get('Exterior Color','')
            # if record.get('Hardware Color','')!='':
            #     color+=", "+record.get('Hardware Color','')
            
            #color=record.get('Interior Color','')+", "+record.get('Exterior Color','')+", "+record.get('Hardware Color','')
            
            

            item_conditions = re.findall(re.compile(f'{pattern_words(meta_conditions)}', re.IGNORECASE), item_info)
            if not item_conditions:
                item_conditions = re.findall(re.compile(f'{pattern_words(meta_conditions)}', re.IGNORECASE), condition)
                
            model=record.get('title','')
            
            if material == '':
                for word in meta_materials:
                    if re.findall(re.compile(r"\b{}\b".format(word), re.IGNORECASE), model):
                        material = word
                        model =  re.sub(re.compile(r"\b{}\b".format(word), re.IGNORECASE), "" , model)
            
            for word in re.findall(r"\w+",material):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,' ',model)
            
            material = re.sub(re.compile(r"Canvas\s*\(*Coated\)*|Canvas\s*\(*Plain\)*|Canvas\s*\(*Interior\)*|Coated Canvas", re.IGNORECASE), "Canvas", material)
            # for word in meta_category_names:
            #     pattern=re.compile(r"\b{}\b".format(word),re.IGNORECASE)
            #     model=re.sub(pattern,' ',model)
                
            
            for word in meta_sizes:
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,' ',model)
                
            for word in re.findall(r"\w+",color):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,' ',model)
            
            for word in meta_materials:
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,' ',model)
            
            model = re.sub(re.compile(r'\band\b|with$|\bGucci\b|with|\bed\b', re.IGNORECASE), ' ', model.strip())
           
            model = re.sub(' +', ' ', model)

            color =  re.sub(re.compile(r"Prints*", re.IGNORECASE), "Print", color)

            description = self.multireplace(description,
             {'These are professional pictures of the actual bag offered by Rebag.': '','*':''})

            description = re.sub(re.compile(r'Auth*enticity\s*code\s*reads\:*\s*\d*\.*|Hologram\s*st[i|o]cker\s*reads\:*\s*\d*\.*', re.IGNORECASE),'',description.strip())
            ner_query = re.sub(re.compile(r'Auth*enticity\s*code\s*reads\:*\s*\d*\.*|Hologram\s*st[i|o]cker\s*reads\:*\s*\d*\.*', re.IGNORECASE),'',ner_query.strip())


            meta_product_types=["Backpacks*","Belt Bag","Bowler","Briefcase*s*","Bucket","Clutche*s*","Cross Body Bags*","Duffles*","Hobos*","Messenger","Rolling Suitcase","Satchels*","Shoulder Bags*","Small Goods*","Top Handle","Totes*","Wallets*","Waist Bag"]
            product_type=''
            for meta in meta_product_types:
                pattern=re.compile(r"{}".format(meta),re.IGNORECASE)
                if re.findall(pattern,record.get('title',''))!=[]:
                    product_type=meta.strip('s*')
            

            pattern=re.compile(r"wallets*|clutch",re.IGNORECASE)
            if re.findall(pattern,model)!=[] or re.findall(pattern,title)!=[] or record.get('breadcrumb','')=='':
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "title": title,
                "model": model,
                "material": material,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": "Women's Bags & Handbags",
                "brand": brand.strip(),
                #"image": record.get('image', ''),
                "description": description.strip(),
                
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price.replace(",",'')
                },
                "msrp": {
                    "currencyCode": "USD",
                    "amount": est_retail.replace(",",'')
                },
                
                "measurements":measurements,
                "color": color,
                "item_condition": ', '.join(item_conditions),
                "product_line": record.get('Category',''),
                "product_type":product_type
            }

            return transformed_record
        else:
            return None
