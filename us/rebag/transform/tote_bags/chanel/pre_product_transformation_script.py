import hashlib 
import re

class Mapper(object):

    def multireplace(self,string, replacements, ignore_case=True):
        if ignore_case:
            def normalize_key(s):
                return s.lower()

            re_mode = re.IGNORECASE

        else:
            def normalize_key(s):
                return s

            re_mode = 0

        replacements = {normalize_key(key): val for key, val in replacements.items()}
                
        rep_sorted = sorted(replacements, key=len, reverse=True)
        rep_escaped = map(re.escape, rep_sorted)
        
        pattern = re.compile("|".join(rep_escaped), re_mode)
        
        # For each match, look up the new string in the replacements, being the key the normalized old string
        return pattern.sub(lambda match: replacements[normalize_key(match.group(0))], string)



    def map(self, record):
        if record:
            ner_query= record['description']+" " +record['condition']+" "+record.get('material','')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            est_retail = record.get('est_retail', '')
            est_retail = est_retail.replace("$", "").strip()

            brand = record.get('brand', '')
            category = record.get('category', '')
            title = record.get('title', '')
            condition = record.get('condition', '')
            description = record.get('description', '')

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            meta_materials = ["Acrylic","Alligator","Beaded","Calf Hair","Canvas Coated","Canvas Plain","Crocodile","Denim","Exotic","Fabric","Felt","Fur","Jersey","Leather","Lizard","Metal","Nylon","Ostrich","Patent Leather","Patent Vinyl","Pony Hair","Pvc","Python","Raffia","Rhinestone","Satin","Sequins","Shearling","Snakeskin","Stingray","Straw","Suede","Tweed","Velvet","Vinyl","Wool","Epi","Perforated","Taiga","Empreinte","Graphic Leather","Mahina","Whipstitch","Canvas"]
            meta_colors = ["Black","Blue","Brown","Gold","Gray","Green","Metallic","Multicolor","Neutral","Orange","Pink","Print","Purple","Red","Silver","White","Yellow"]
            meta_conditions = ["Pristine","Excellent","Great","Very Good","Good","Fair"]
            meta_product_lines = ["Convertible Bags","Evening","Luggage","Mens","Weekenders"]
            meta_sizes = ["Small", "Medium", "Large"]
            meta_category_names = ["Backpacks","Belt Bag","Bowler","Briefcases","Bucket","Clutches","Cross Body Bags","Duffles","Hobos","Messenger","Satchels","Shoulder Bag(s){0,1}","Small Goods","Top Handle","Totes","Wallets","Waist Bag","Wallets","wallet","tote","tote bags","Clutch","Bag","Handbag","backpack","Crossbody"]
    
            item_info = record.get('item_info', '')
            # category_names = re.findall(re.compile(f'{pattern_words(meta_category_names)}', re.IGNORECASE), item_info)
            # if not category_names:
            #     category_names = re.findall(re.compile(f'{pattern_words(meta_category_names)}', re.IGNORECASE), title)
            # if not category_names:
            #     category_names = re.findall(re.compile(f'{pattern_words(meta_category_names)}', re.IGNORECASE), description)
            colors = re.findall(re.compile(f'{pattern_words(meta_colors)}', re.IGNORECASE), item_info)
            if not colors:
                colors = re.findall(re.compile(f'{pattern_words(meta_colors)}', re.IGNORECASE), description)
            materials = re.findall(re.compile(f'{pattern_words(meta_materials)}', re.IGNORECASE), item_info)
            if not materials:
                materials = re.findall(re.compile(f'{pattern_words(meta_materials)}', re.IGNORECASE), title)
            
            material = ', '.join(materials)
            material = self.multireplace(material,
             {'Patent Leather': 'Leather', 'Patent Vinyl': 'Vinyl', 'Canvas Plain': 'Canvas', 'Canvas Coated': 'Canvas', 'Calf Leather':'Leather','Canvas and Leather':'Canvas, Leather'})
            material = self.multireplace(material,
             {'Leather, Leather': 'Leather', 'Pvc': 'PVC'})

            item_conditions = re.findall(re.compile(f'{pattern_words(meta_conditions)}', re.IGNORECASE), item_info)
            if not item_conditions:
                item_conditions = re.findall(re.compile(f'{pattern_words(meta_conditions)}', re.IGNORECASE), condition)
            product_lines = re.findall(re.compile(f'{pattern_words(meta_product_lines)}', re.IGNORECASE), item_info)

            model = re.sub(re.compile(r''+f'{pattern_words(meta_category_names)}|{pattern_words(colors)}|{pattern_words(materials)}|{pattern_words(meta_sizes)}|\b{brand}\b', re.IGNORECASE), '', title)
            model = re.sub(re.compile(r'\band\b|with$|Canvas|with|\btote\b|bag|Patent', re.IGNORECASE), '', model.strip())
            model=re.sub(re.compile(r"\bskin\b",re.IGNORECASE),'',model)
            model = re.sub(material, '', model)
            model = re.sub(' +', ' ', model)

            description = self.multireplace(description,
             {'These are professional pictures of the actual bag offered by Rebag.': '','*':''})

            description = re.sub(re.compile(r'Auth*enticity\s*code\s*reads\:*\s*\d*\.*|Hologram\s*st[i|o]cker\s*reads\:*\s*\d*\.*', re.IGNORECASE),'',description.strip())
            ner_query = re.sub(re.compile(r'Auth*enticity\s*code\s*reads\:*\s*\d*\.*|Hologram\s*st[i|o]cker\s*reads\:*\s*\d*\.*', re.IGNORECASE),'',ner_query.strip())

            height = ''
            width = ''
            depth = ''
            measurements = record.get('measurements', '')
            matches = re.findall(re.compile(r'Height (.*?\"), Width (.*?\"), Depth (.*?\")', re.IGNORECASE), measurements)
            if matches and len(matches) > 0:
                groups = matches[0]
                height = groups[0]
                width = groups[1]
                depth = groups[2]

            pattern=re.compile(r"wallets*|clutch",re.IGNORECASE)
            if re.findall(pattern,model)!=[] or re.findall(pattern,title)!=[]:
                return None
                

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "title": title,
                "model": model,
                "material": material,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": category,
                "brand": brand.strip(),
                "image": record.get('image', ''),
                "description": description.strip(),
                
                "item_number": record['style'],
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "msrp": {
                    "currencyCode": "USD",
                    "amount": est_retail
                },
                
                "height": height,
                "width": width,
                "depth": depth,
                "color": colors[0] if colors else '',
                "item_condition": ','.join(item_conditions),
                "product_line": ','.join(product_lines),
            }

            return transformed_record
        else:
            return None