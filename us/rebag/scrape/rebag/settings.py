# -*- coding: utf-8 -*-

# Scrapy settings for rebag project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'rebag'

SPIDER_MODULES = ['rebag.spiders']
NEWSPIDER_MODULE = 'rebag.spiders'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"

ROBOTSTXT_OBEY = True

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

# GCS settings - actual value be 'raw/handbags/gucci/rebag.com/2019-02-06_00_00_00/json/rebag.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{common}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.selenium_middleware.middlewares.SeleniumMiddleware':1000,
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}