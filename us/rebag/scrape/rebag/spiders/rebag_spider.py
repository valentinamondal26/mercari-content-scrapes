'''
https://mercari.atlassian.net/browse/USCC-378 - Louis Vuitton Backpacks
'''

import scrapy
from scrapy.exceptions import CloseSpider
import random
from  datetime import datetime

from common.selenium_middleware.http import SeleniumRequest
import os

class RebagSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from rebag.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Shoulder Bags
        2) Shoulder Bags
        3) Crossbody Bags
        4) Tote Bags

    brand : str
        category to be crawled, Supports
        1) Louis Vitton
        2) Gucci
        3) YSL Yves Saint Laurent
        4) Chanel
        5) Burberry
        6) Christian Dior

    Command e.g:
    scrapy crawl rebag -a category="Backpacks" -a brand="Louis Vuitton"
    scrapy crawl rebag -a category="Tote Bags" -a brand="Chanel"
    scrapy crawl rebag -a category="Tote Bags" -a brand="Gucci"
    scrapy crawl rebag -a category="Crossbody Bags" -a brand="Chanel"
    scrapy crawl rebag -a category="Crossbody Bags" -a brand="Gucci"
    scrapy crawl rebag -a category="Crossbody Bags" -a brand="Louis Vuitton"

    scrapy crawl rebag -a category="Shoulder Bags" -a brand="Gucci"
    """

    name = 'rebag'

    category = None
    brand = None

    merge_key = 'id'

    filters = None

    source = 'rebag.com'
    blob_name = 'rebag.txt'

    base_url = 'https://shop.rebag.com'

    materials = []
    colors = []
    product_lines = []

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
        'http://shp-mercari-us-d00011.tp-ns.com:80',
    ]

    url_dict = {
        "Women's Shoulder Bags": {
            'Louis Vuitton': {
                'url': 'https://shop.rebag.com/collections/all-bags/shoulder-bags+louis-vuitton',
            }
        },
        "Shoulder Bags" : {
            'Gucci': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Shoulder%20Bags&pf_v_designers=Gucci',
            },
            'YSL Yves Saint Laurent': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_v_designers=Saint%20Laurent&pf_t_silhouette=bc-filter-Shoulder%20Bags',
            },
            'Chanel': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_v_designers=Chanel&pf_t_silhouette=bc-filter-Shoulder%20Bags',
            },
            'Burberry': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Shoulder%20Bags&pf_v_designers=Burberry',
            },
            'Christian Dior': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Shoulder%20Bags&pf_v_designers=Christian%20Dior',
            },
        },
        'Crossbody Bags': {
            'YSL Yves Saint Laurent': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Cross%20Body%20Bags&pf_v_designers=Saint%20Laurent',
            },
            'Louis Vuitton': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_v_designers=Louis%20Vuitton&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Cross%20Body%20Bags',
            },
            'Gucci': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Cross%20Body%20Bags&pf_v_designers=Gucci',
            },
            'Chanel': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_v_designers=Chanel&pf_t_silhouette=bc-filter-Cross%20Body%20Bags',
            },
        },
        'Tote Bags': {
            'Gucci': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_v_designers=Gucci&pf_t_silhouette=bc-filter-Totes',
            },
            'Chanel': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_v_designers=Chanel&pf_t_silhouette=bc-filter-Totes',
            },
        },
        "Women's Bags & Handbags":{
            'Gucci': {
                'url': 'https://shop.rebag.com/collections/all-bags?_=pf&pf_v_designers=Gucci&pf_st_availability_hidden=true',
                'filters': ['Category'],
            },
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        super(RebagSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', None)
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category "+category+" and brand "+brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.filters = ['Category']
        '''
        filter name is chnaged for the gucci it is changed to Categories instead of Category
        '''

    def start_requests(self):

        if self.launch_url:
            # start selenium driver

            yield self.createDynamicRequest(url=self.launch_url, callback=self.parse_filters)
            yield self.createDynamicRequest(url=self.launch_url,callback=self.parse)


    def parse_filters(self,response):
        """
        @url https://shop.rebag.com/collections/all-bags?_=pf&pf_v_designers=Gucci&pf_st_availability_hidden=true
        @meta {"use_selenium":"True"}
        @returns requests 1
        """

        if self.filters:
            for filter in self.filters:

                filter_urls=response.xpath("//ul[@class='bc-sf-filter-option-multiple-list']/li/a[contains(@data-parent-label,'{}')]/@href".format(filter)).getall()
                filter_urls=[self.base_url+link if "https" not in link else link for link in filter_urls ]
                filter_values=response.xpath("//ul[@class='bc-sf-filter-option-multiple-list']/li/a[contains(@data-parent-label,'{}')]/@data-title".format(filter)).getall()
                filter_values=[val.strip() for val in filter_values]
                for url in filter_urls:
                    meta={}
                    index=filter_urls.index(url)
                    meta=meta.fromkeys([filter],filter_values[index])
                    meta.update({'filter_name':filter})
                    yield self.createDynamicRequest(url=url,callback=self.parse,meta=meta)


    def crawlDetail(self, response):
        """
        @url https://shop.rebag.com/collections/all-bags/products/handbags-gucci-bamboo-shopper-tote-leather-medium4900443
        @scrape_values title id price image description condition est_retail
        """
        response_meta=response.meta
        yield self.get_dict(response, self.category, self.brand, meta=response_meta)


    def parse(self, response):
        """
        @url https://shop.rebag.com/collections/all-bags?_=pf&pf_v_designers=Gucci&pf_st_availability_hidden=true&pf_t_categories=bc-filter-Bags
        @meta {"use_selenium":"True"}
        @returns requests 49
        """
        reposne_meta=response.meta

        total_count = response.xpath('//span[@id="bc-sf-filter-total-product"]/text()').extract_first()
        print(f'total items: {total_count}')

        for product in response.xpath('//div[@id="bc-sf-filter-products"]/li'):

            id = product.xpath('.//div[@class="product-image-wrap"]/a/@href').extract_first()

            if id:
                request = self.createRequest(url=self.base_url + id, callback=self.crawlDetail, meta=reposne_meta)
                yield request

        next_link = response.xpath('//div[@id="bc-sf-filter-bottom-pagination"]/ul/span[@class="next"]/a/@href').extract_first()
        print(next_link)
        if next_link:
            yield self.createDynamicRequest(url=next_link, callback=self.parse,meta=reposne_meta)


    def get_dict(self, response, category, brand,meta={}):

        item = dict()

        item['category'] = category
        item['brand'] = brand
        item['crawlDate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['statusCode'] = "200"
        item['breadcrumb']='|'.join(response.xpath('//ol[@class="breadcrumb"]/li//text()').getall())
        item['id'] = response.xpath('//head//meta[@property="og:url"]/@content').extract_first(default='')
        item['title'] = response.xpath('//span[contains(@class,"pdp__title")]/text()').extract_first(default='')
        item['price'] = response.xpath('//div[contains(@class,"pdp__price")]//text()').extract_first(default='')
        item['image'] = "https:"+response.xpath('//meta[@property="og:image"]/@content').extract_first(default='')

        item['description'] = ', '.join(response.xpath('//div[@class="pdp__section"][h2[contains(text(),"Overview")]]//text()').getall())
        item['condition'] = response.xpath(u'normalize-space(//li[@class="pdp__condition-item pdp__condition-item--selected"]/text())').extract_first(default='')
        item['est_retail'] = response.xpath(u'normalize-space(//div[@class="pdp__retail-price"]/span/text())').extract_first(default='')

        # scripts = response.xpath('//script[@text="text/javascript"]').extract()
        # for script in scripts:
        #     match=re.findall(r'Categories: \[(.*?)(\])', script.replace('\n', ''))
        #     if match:
        #         item['item_info'] = match[0][0]
        #         break

        divs=response.xpath('//div[@class="pdp__section"][h2[contains(text(),"Overview")]]//div[contains(@class,"pdp__section-group")]')
        keys=[]
        values=[]
        for d in divs:
            keys.append(d.xpath('./div[contains(@class,"pdp__group-title")]/text()').get())
            values.append(', '.join(d.xpath('./div[contains(@class,"pdp__group-item")]//text()').getall()))
        keys.append('measurements')
        values.append(', '.join(response.xpath('//div[@class="pdp__section"][h2[contains(text(),"SIZE AND FIT")]]//div[contains(@class,"pdp__section-group")]//div[contains(@class,"pdp__group-item")]/text()').getall()))
        details={}.fromkeys(keys,'')
        ctr=0
        for key,value in details.items():
            if details[key]=='':
                details[key]=values[ctr]
                ctr+=1
        item.update(details)
        reposne_meta={}
        filter_name=meta.get('filter_name','')
        if filter_name!='':
            reposne_meta=reposne_meta.fromkeys([filter_name],meta[filter_name])
            item.update(reposne_meta)

        return item


    def createRequest(self, url, callback, meta=None,headers=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        if headers:
            for key,value in headers.items():
                request.headers[key]=value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request


    def createDynamicRequest(self, url, callback, errback=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, meta=meta)
        # if self.proxy_pool:
        #     request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request
