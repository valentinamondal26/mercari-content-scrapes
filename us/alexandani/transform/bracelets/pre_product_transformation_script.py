import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('details', '') or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = ', '.join(record.get('Color', []))

            details_info = ', '.join(record.get('details_info', []))

            finish = record.get('variant_selection', '') or record.get('Finish', '')
            if not finish:
                match = re.findall(r'Finish:\s*(.*),', details_info, flags=re.IGNORECASE)
                if match:
                    finish = match[0]

            finish = re.sub(r'®', '', finish, flags=re.IGNORECASE)
            finish = re.sub(r'\s+', ' ', finish).strip()

            model = record.get('title', '')
            model = re.sub(r"™|®|,|©|\bColor\b|\(|\)|\bMen's\b|\bMixed Metals\b", '', model, flags=re.IGNORECASE)
            if color:
                model = re.sub(r'\b'+color+r'\b', '', model, flags=re.IGNORECASE)
                color_strings = color.split(', ')
                model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color_strings))), '', model, flags=re.IGNORECASE)
            if finish:
                model = re.sub(r'\b'+finish+r'\b', '', model, flags=re.IGNORECASE)
                finish_strings = finish.split()
                model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', finish_strings))), '', model, flags=re.IGNORECASE)
            model = re.sub(r"'Woof'", 'Woof', model, flags=re.IGNORECASE)
            match = re.findall(r'\bBangle\b|\bBracelet\b|\bBangles\b|\bBracelets\b', model, flags=re.IGNORECASE)
            if match:
                for _match in match:
                    model = re.sub(r'(.*?)(\b'+_match+r'\b)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*) (Bracelet) (Bangle)$', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'(\s+Bangle)+', ' Bangle', model, flags=re.IGNORECASE)
            model = re.sub(r"(?!\'\w+\b)\'|\s+\'|^\'", ' ', model, flags=re.IGNORECASE)
            model = ' '.join(list(map(lambda x: x[0].upper()+x[1:], model.split())))
            model = re.sub(r'-\w+\b',lambda x: x.group().title(), model, flags=re.IGNORECASE)
            model = re.sub(r'(?!U\.S\.A)\bU\.S\.|\bUnited States\b', 'US', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            color = re.sub(r'Multicolored', 'Multicolor', color, flags=re.IGNORECASE)
            colors = color.split(', ')
            if len(colors) > 2:
                colors = [colors[0], 'Multicolor']
            color = '/'.join(colors)

            product_line = ', '.join(record.get('Collection', []))
            product_type = ', '.join(record.get('Bracelet Type', []))

            measurement = ''
            match = re.findall(r'(Expandable|Stretches) from (\d+\.*\d*") to (\d+\.*\d*")', details_info, flags=re.IGNORECASE)
            if match:
                measurement = f'{match[0][1]} - {match[0][2]}'

            sku = record.get('sku', '')
            sku = re.sub(r'SKU:', '', sku, flags=re.IGNORECASE).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),
                "description": record.get('details', '') or '',

                'model': model,
                'color': color,
                'finish': finish,
                'product_type': product_type,
                'product_line': product_line,
                'measurement': measurement,
                'model_sku': sku,
                'msrp': {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
