'''
https://mercari.atlassian.net/browse/USCC-526 - ALEX AND ANI Bracelets
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import json
import re
import os
import urllib.parse

class AlexandaniSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from alexandani.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Bracelets

    brand : str
        brand to be crawled, Supports
        1) ALEX AND ANI

    Command e.g:
    scrapy crawl alexandani -a category='Bracelets' -a brand='ALEX AND ANI'
    """

    name = 'alexandani'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'alexandani.com'
    blob_name = 'alexandani.txt'

    url_dict = {
        'Bracelets': {
            'ALEX AND ANI': {
                'url': 'https://www.alexandani.com/collections/bracelets-all',
                'filters': ['Bracelet Type', 'Collection', 'Color', 'Finish'],
            }
        }
    }

    merge_key = 'id'

    extra_item_infos = {}

    base_url = 'https://www.alexandani.com'

    listing_api_url = 'https://api.searchspring.net/api/search/search.json?domain={url}%23%3Fpage={page}&siteId={site_id}&resultsFormat=native&resultsPerPage={size}&bgfilter.ss_is_published=1&page=1&bgfilter.collection_id={collection_id}'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AlexandaniSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.filters = ['Bracelet Type', 'Collection', 'Color', 'Finish']
        self.collection_id = '158718066791'
        self.site_id = '4e8y2z'


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.alexandani.com/products/guardian-angel-crystal-stretch-bracelets-set-of-3
        @scrape_values title price sku image details id image pre_title details_info variant_selection
        @meta {"use_proxy":"True"}
        """
        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'pre_title': response.xpath('//div[@class="ProductMeta"]/p[@class="ProductMeta__PreTitle"]/text()').extract_first(),
            'title': response.xpath('//div[@class="ProductMeta"]/h1[@class="ProductMeta__Title"]/text()').extract_first(),
            'price': response.xpath('//div[@class="ProductMeta"]/div[@class="ProductMeta__PriceList"]/span[contains(@class, "ProductMeta__Price Price")]/text()').extract_first(),
            'variant_selection': response.xpath('//div[@class="ProductForm__Option"]//span[@class="ProductForm__SelectedValue"]/text()').extract_first(),
            'details': response.xpath('//div[contains(@class, "ProductDetails__TabContent Tab__Content")]/div[@class="Rte"]/p/text()').extract_first(),
            'details_info': response.xpath('//div[contains(@class, "ProductDetails__TabContent Tab__Content")]/div[@class="Rte"]/ul/li/text()').extract(),
            'sku': response.xpath('//p[@class="ProductMeta__Sku js-product-sku"]/text()').extract_first(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
        }


    def parse(self, response):
        """
        @url https://www.alexandani.com/collections/bracelets-all
        @meta {"use_proxy":"True"}
        @returns requests 2
        """

        self.collection_id = re.findall(r'SDG.Data.collectionId\s*=\s*(.*);', response.xpath('//script[contains(text(), "SDG.Data.collectionId =")]').extract_first())[0]
        self.site_id = re.findall(r"Data.ssId='(.*)';", response.xpath('//script[contains(text(), "Data.ssId=")]').extract_first())[0]

        if self.collection_id and self.site_id:
            url = self.listing_api_url.format(
                url=urllib.parse.quote_plus(self.launch_url.replace(self.base_url, '')),
                page=1,
                site_id=self.site_id,
                size=500,
                collection_id=self.collection_id
            )
            yield self.create_request(url=url, callback=self.parse_listing_page, dont_filter=True)
            yield self.create_request(url=url, callback=self.parse_filters, dont_filter=True)


    def parse_filters(self, response):
        """
        @url https://api.searchspring.net/api/search/search.json?domain=%2Fcollections%2Fbracelets-all%23%3Fpage%3D1&siteId=4e8y2z&resultsFormat=native&resultsPerPage=500&bgfilter.ss_is_published=1&page=1&bgfilter.collection_id=158718066791
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        res = json.loads(response.text)
        for facet in res.get('facets', []):
            filter_label = facet.get('label', '')
            if filter_label in self.filters:
                variant_type = facet.get('field', '')
                for value in facet.get('values', []):
                    variant_label = value.get('label', '')
                    # count = value.get('count', 0)
                    url = self.listing_api_url.format(
                        url=urllib.parse.quote_plus(self.launch_url.replace(self.base_url, '')),
                        page=str(1)+f'/filter.{variant_type}={variant_label}',
                        site_id=self.site_id,
                        size=500,
                        collection_id=self.collection_id
                    )
                    url += f'&filter.{variant_type}={variant_label}'
                    request = self.create_request(url=url, callback=self.parse_listing_page)
                    request.meta['extra_item_info'] = {
                        filter_label: variant_label,
                    }
                    yield request


    def parse_listing_page(self, response):
        """
        @url https://api.searchspring.net/api/search/search.json?domain=%2Fcollections%2Fbracelets-all%23%3Fpage%3D1&siteId=4e8y2z&resultsFormat=native&resultsPerPage=500&bgfilter.ss_is_published=1&page=1&bgfilter.collection_id=158718066791
        @meta {"use_proxy":"True"}
        @returns requests 37
        """
        extra_item_info = response.meta.get('extra_item_info', {})

        res = json.loads(response.text)

        for result in res.get('results', []):
            url = result.get('url', '')
            if url:
                if not url.startswith(self.base_url):
                    url = self.base_url + url

                if not self.extra_item_infos.get(url, None):
                    self.extra_item_infos[url] = {}

                self.update_extra_item_infos(url, extra_item_info)
                yield self.create_request(url=url, callback=self.crawlDetail)

        if res.get('pagination', {}).get('currentPage', 0) < res.get('pagination', {}).get('totalPages', 0):
            next_page_number = res.get('pagination', {}).get('nextPage', 0)
            if next_page_number:
                url = re.sub(r'page=(\d+)', str(next_page_number), response.url)
                yield self.create_request(url=url, callback=self.parse_listing_page, meta={'extra_item_info': extra_item_info})


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
