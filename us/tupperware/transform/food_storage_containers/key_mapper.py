class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        volume = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "volume" == entity['name']:
                if entity["attribute"]:
                    volume = entity["attribute"][0]["id"]

        key_field = model + volume
        return key_field
