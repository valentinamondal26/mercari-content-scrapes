import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            title = record.get('title', '')
            features = ' '.join(record.get('features', []))

            volume = ''
            volume_regex = re.compile(r'(\d+\s*[\u00BC-\u00BE\u2150-\u215E]*-(cup|qt\.)\/\d+\.*\d*\s*m*L)', re.IGNORECASE)
            match = re.findall(volume_regex, features) \
                or re.findall(volume_regex, title)
            if match:
                volume = match[0][0]
                # volume = re.sub(r'\s*½', '.5', volume)
                # volume = re.sub(r'\s*¼', '.25', volume)

            model = title
            model = re.sub(re.compile(r'®|™', re.IGNORECASE), '', model)
            model = re.sub(volume_regex, '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            def fraction_to_decimal(fraction):
                from fractions import Fraction
                import unicodedata
                nums = fraction.split('/')
                if len(nums) == 2:
                    return str(round(float(Fraction(int(nums[0]), int(nums[1]))), 1))[1:]
                else:
                    try:
                        return str(round(unicodedata.numeric(u'{}'.format(fraction)), 1))[1:]
                    except:
                        pass
                    return fraction

            length = ''
            width = ''
            height = ''
            dimensions = record.get('specs', {}).get('Dimensions', '')
            match = re.findall(r'(\d+\s*\d*\/*\d*[\u00BC-\u00BE\u2150-\u215E]*)\s*x\s*(\d+\s*\d*\/*\d*[\u00BC-\u00BE\u2150-\u215E]*)\s*x\s*(\d+\s*\d*\/*\d*\s*[\u00BC-\u00BE\u2150-\u215E]*)[”"]', dimensions or features)
            # match = re.findall(r'(\d+\s*\d*\/*\d*[\u00BC-\u00BE\u2150-\u215E]*)*\s*[x”\"]\s*', dimensions or features)
            if match:
                length = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group()), match[0][0])
                width = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group()), match[0][1])
                height = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group()), match[0][2])


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                'image': record.get('image', ''),
                'features': features,

                "model": model,
                "volume": volume,
                'length': length,
                'width': width,
                'height': height,
                'wash': record.get('specs', {}).get('Wash Type', ''),
                'model_sku': record.get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
