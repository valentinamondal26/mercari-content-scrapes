'''
https://mercari.atlassian.net/browse/USDE-1712 - Tupperware Food Storage Containers
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class TupperwareSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from tupperware.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Food Storage Containers

    brand : str
        brand to be crawled, Supports
        1) Tupperware

    Command e.g:
    scrapy crawl tupperware -a category='Food Storage Containers' -a brand='Tupperware'
    """

    name = 'tupperware'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'tupperware.com'
    blob_name = 'tupperware.txt'

    url_dict = {
        'Food Storage Containers': {
            'Tupperware': {
                'url': 'https://www.tupperware.com/shop/food-storage/',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK', ''):
            return
        super(TupperwareSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url+'?product_list_limit=all', callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.tupperware.com/fridgesmart-medium-deep/
        @scrape_values title breadcrumb price description sku features image specs specs.Capacity specs.Dimensions specs.Wash\sType specs.Feature
        @meta {"use_proxy": "True"}
        """

        specs = {}
        for tr in response.xpath('//table[@id="product-attribute-specs-table"]/tbody/tr'):
            specs[tr.xpath('./th[@class="col label"]/text()').extract_first()] = tr.xpath('./td/text()').extract_first()

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//div[@class="breadcrumbs"]/ul[@class="items"]/li/a/text()').extract(),
            'title': response.xpath('//h1[@class="page-title"]/span/text()').extract_first(),
            'price': response.xpath('//meta[@itemprop="price"]/@content').extract_first(),
            'description': response.xpath('//div[@itemprop="description"]/text()').extract_first(),
            'sku': response.xpath('//div[@itemprop="sku"]/text()').extract_first().replace('Item SKU\xa0', ''),
            'features': response.xpath('//div[@class="product attribute description"]/div[@class="value"]//ul/li//text()').extract(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'specs': specs,
        }


    def parse(self, response):
        """
        @url https://www.tupperware.com/shop/food-storage/
        @returns requests 37
        @meta {"use_proxy": "True"}
        """
        ###Next Page url is missed. Next Page url needs to be added.
        # Totally 36 products are displayed per page and 1 next page url so totally 37 requests will be sent. 
        for product in response.xpath('//ol[@class="products list items product-items"]/li[@class="item product product-item"]'):

            id = product.xpath('.//a[@class="product-item-link"]/@href').extract_first()

            if id:
                yield self.create_request(url=id, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
