'''
https://mercari.atlassian.net/browse/USCC-683 - Kork-Ease Women's Sandals
https://mercari.atlassian.net/browse/USCC-684 - Kork-Ease Women's Boots
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re


class KorkeaseSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from korkease.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Sandals
        2) Women's Boots
    
    brand : str
        brand to be crawled, Supports
        1) Kork-Ease

    Command e.g:
    scrapy crawl korkease -a category="Women's Sandals" -a brand="Kork-Ease"
    scrapy crawl korkease -a category="Women's Boots" -a brand="Kork-Ease"
    """

    name = 'korkease'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'korkease.com'
    blob_name = 'korkease.txt'

    base_url = 'https://www.korkease.com'

    url_dict = {
        "Women's Sandals": {
            "Kork-Ease": {
                'url': 'https://www.korkease.com/category.aspx?GenderID=8&CategoryID=110&SortType=P&PageType=R&PageNumber=1000',
            }
        },
        "Women's Boots": {
            "Kork-Ease": {
                'url': 'https://www.korkease.com/category.aspx?GenderID=8&CategoryID=102&SortType=P&PageType=R&PageNumber=1000',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(KorkeaseSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                url = re.sub(r'PageNumber=\d+', 'PageNumber=1000', url, flags=re.IGNORECASE)
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.korkease.com/product.aspx?ProductID=2083
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title color price description features
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//ol[@id="breadcrumb"]/li//text()').getall(),
            'image': response.xpath('//div[@id="shoe-wrp"]/figure/img/@src').get(),
            'title': response.xpath('//h2/span[@id="shoe-title"]/text()').get(),
            'color': response.xpath('//h2/span[@id="shoe-color"]/text()').get(),
            'price': response.xpath('//h2/span[@id="reg-price"]/text()').get(),
            'sizes': response.xpath('//select[@id="select-size"]/option[@data-qty="true"]/text()').get(),
            'description': response.xpath('//div[@id="details"]/p[@class="prod-desc"]/text()').get(),
            'features': response.xpath('//div[@id="details"]/ul/li/text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.korkease.com/category.aspx?GenderID=8&CategoryID=110&SortType=P&PageType=R&PageNumber=1000
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@id="cat-wrp"]/ul/li'):
            product_urls = product.xpath('.//div[@class="details"]/ul[@class="shoe-moreswatches"]/li/a/@href').extract() \
                or [product.xpath('.//div[@class="details"]/p/a/@href').extract_first()]
            for product_url in product_urls:
                product_url = self.base_url + '/' + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
