import hashlib
import re
import json
from titlecase import titlecase
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = record.get('color', '')

            title = record.get('title', '')
            model = re.sub(r'\b'+color+r'\b', '', title, flags=re.IGNORECASE)

            model = re.sub(r'-', ' ', model)
            model = re.sub(r'\bSale\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Backpack)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMINI\b', 'Mini', model)
            model = re.sub(r'\bCorail\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            color = record.get('color','')
            color = re.sub(r'\bArmy\b', 'Army Green', color, flags=re.IGNORECASE)
            color = re.sub(r'\bBlack Polar\b', 'Black', color, flags=re.IGNORECASE)
            if re.findall(r'\bHeathers\b|\bHeather\b', model, flags=re.IGNORECASE):
                color = re.sub(r'\bHeather\b', '', color, flags=re.IGNORECASE)
            color = re.sub(r'\s+', ' ', color).strip()

            material = ''
            material_list = title.split('-')
            if len(material_list) > 1:
                material = material_list[-1]
            material = re.sub(r'\b'+color+r'\b', '', material, flags=re.IGNORECASE)
            material = re.sub(r'\bSale\b', '', material, flags=re.IGNORECASE)

            materials_meta = ['Cotton Canvas', 'Polar', 'Heathers', 'Heather', 'Prints', 'Corail', 'Coral']
            details = ' '.join(record.get('details', []))
            if not material:
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), details, flags=re.IGNORECASE)
                if match:
                    material = '/'.join(list(dict.fromkeys(list(map(lambda x: x.title().strip())))))
            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()
            material = re.sub(r'\bCorail\b', 'Coral', material, flags=re.IGNORECASE)

            width = ''
            depth = ''
            height = ''
            details = ' '.join(record.get('details', []))
            match = re.findall(r"Dimensions:\s*W:\s*(\d+\.*\d+)\D+,\s*D:\s*(\d+\.*\d+)\D+,\s*H:\s*(\d+\.*\d+)\D", details)
            if match:
                width, depth, height = tuple(map(lambda x: x+'"' if x else '', match[0]))

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                "width": width,
                "depth": depth,
                "height": height,
                "model_sku": record.get('model_sku', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                }
            }

            return transformed_record

        else:
            return None
