'''
https://mercari.atlassian.net/browse/USCC-650 - 7 A.M. Enfant Diaper Bags
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

class SevenAmenfantSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from 7amenfant.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Diaper Bags

    brand : str
        brand to be crawled, Supports
        1) 7 A.M. Enfant

    Command e.g:
    scrapy crawl 7amenfant -a category='Diaper Bags' -a brand='7 A.M. Enfant'
    """

    name = '7amenfant'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = '7amenfant.com'
    blob_name = '7amenfant.txt'

    url_dict = {
        'Diaper Bags': {
            '7 A.M. Enfant': {
                'url': 'https://www.7amenfant.com/collections/all/category_backpacks?sort_by=title-ascending',
            }
        }
    }

    base_url = 'https://www.7amenfant.com'

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SevenAmenfantSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.7amenfant.com/products/bk718-classic-backpack-heathers?variant=30065505992768#Image12336164339776
        @meta {"use_proxy":"True"}
        @scrape_values id title image color model_sku price details description
        """

        image = response.xpath(f'//div[@class="Product__Slideshow Product__Slideshow--zoomable Carousel"]/div[@data-image-id="{response.meta.get("image_id", "")}"]//img/@src').get() or \
            response.xpath('//meta[@property="og:image"]/@content').get()
        if not image.startswith('http:'):
            image = 'http:' + image

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': image,
            'title': response.xpath("//h1[@class='ProductMeta__Title Heading u-h2']/text()").get(),
            'color': response.xpath("//span[@class='ProductForm__SelectedValue']/text()").get(),
            'model_sku': response.xpath("//span[@class='ProductMeta__SkuNumber']/text()").get(),
            'price': response.xpath("//span[@class='ProductMeta__Price Price Text--subdued u-h4']/text()").get() \
                or response.xpath("//span[@class='ProductMeta__Price Price Price--compareAt Text--subdued u-h4']/text()").get(),
            'details': response.xpath("//div[@class='Collapsible__Content']/div[@class='Rte']/ul/li/text()").getall(),
            'description': response.xpath("normalize-space(//div[@class='ProductMeta__Description']/div)").get()
        }


    def parse(self, response):
        """
        @url https://www.7amenfant.com/collections/all/category_backpacks?sort_by=title-ascending
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.xpath("//div[@class='ProductItem__ColorSwatchList']//input[@class='ColorSwatch__Radio']"):
            product_url = product.xpath('./@data-variant-url').extract_first()
            image_id = product.xpath('./@data-image-id').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail, meta={'image_id': image_id})



    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
