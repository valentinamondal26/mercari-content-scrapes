import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            for key in record:
                if "{{{" in str(record.get(key,'')):
                    record[key] = ''

            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            model = re.sub(r"\bDr. Seuss's\b|\(|\)|Dr. Seuss’s", '', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'A Dr. Seuss Beginner Fun Book', 'Beginner Fun Book', model, flags=re.IGNORECASE)
            model = re.sub(r'(?!\bBooks* of\b)(.*)(\bbooks*\b)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            if not re.findall(r'\bbooks*\b', model, flags=re.IGNORECASE):
                model = f'{model} Book'
            model = re.sub(r'^Redbook Version of if I Ran the Zoo Book$', \
                'If I Ran The Zoo Redbook Version', model, flags=re.IGNORECASE)
            model = re.sub(r'(?![A-Z])'+r'|'.join(list(map(lambda x: r'\b'+ x+r'\b', model.split()))), 
                                        lambda x: titlecase(x.group()), model)# This is because conjuction words and some preposition words will \
                                                                            # retain the lower case \
                                                                            # if we titlecase it as whole word. To titlecase even the \
                                                                            # preposition and conjuction words we are titlecasing it individually.
            
            release_year = record.get('release_year','')
            match = re.findall("(18..|19..|20..)", release_year)
            if match:
                release_year = match[0]

            if re.findall(r'\bSix by Seuss\b', model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),

                "model": model,
                "release_year": release_year,
                "publisher": record.get("publisher",""),
                "illustrator": record.get("illustrator",""),
                "author": record.get("author",""),
                "characters": record.get("characters","")
            }

            return transformed_record
        else:
            return None
