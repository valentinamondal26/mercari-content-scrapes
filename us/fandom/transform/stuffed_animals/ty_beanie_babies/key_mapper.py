class Mapper:
    def map(self, record):

        model = ''
        release_date = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "release_date" == entity['name']:
                if entity["attribute"]:
                    release_date = entity["attribute"][0]["id"]

        key_field = model + release_date
        return key_field
