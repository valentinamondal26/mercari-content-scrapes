import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            model = re.sub(r'\bBeanie Baby\b|\(|\)', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(\b18\d{2}\b|\b19\d{2}\b|\b20\d{2}\b)(.*)', r'\1 \3 \2', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\b(#\d+)\b', lambda x:f' {x.group()}', model)
            model = re.sub(r'Ruby Max & Ruby Cartoon the Bunny', 'Ruby the Bunny from Max & Ruby Cartoon',\
                    model, flags=re.IGNORECASE)
            model = re.sub(r'\bBear the Bear\b', 'Bear', model, flags=re.IGNORECASE)
            model = re.sub(r"(?!\b\w+\-e\b|\b\w+\'\w+\b|\b\w+\-\w+\-\w+\b|\bI{1,3}\b|\bIV\b|\bV\b|\bVI{1,3}\b|\bIX\b|\bXI{0,3}\b)(" +\
                r'|'.join(list(map(lambda x: r'\b'+x+r'\b', model.split())))+")",\
                lambda x: x.group() if x.group().isupper() else titlecase(x.group()),\
                model)
            model = re.sub(r"(\w+'\w+\-\w+)\b", lambda x: titlecase(x.group()), model)
            model = re.sub(r'\bBarbaro Kentucky Derby Winner The Horse 2006\b', \
                'Barbaro The Horse Kentucky Derby Winner 2006', model, flags=re.IGNORECASE)
            model = re.sub(r'\bHoliday Dora The Girl Explorer\b', 'Holiday Dora The Explorer', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPumkinTemplate:Sic The Pumpkin\b', 'Pumkin The Pumpkin', model, flags=re.IGNORECASE)
            model = re.sub(r'\bHERO\b', 'Hero', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            date_of_creation = re.sub(r'UNKNOWN|dates varied|various dates|Varies|varied', '', \
                record.get('date_of_creation', ''), flags=re.IGNORECASE).strip()
            release_date = re.sub(r'UNKNOWN|dates varied|various dates|Varies|varied', '',\
                 record.get('release_date', ''), flags=re.IGNORECASE).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),

                "model": model,
                "release_date": release_date,
                "date_of_creation": date_of_creation,
            }

            return transformed_record
        else:
            return None
