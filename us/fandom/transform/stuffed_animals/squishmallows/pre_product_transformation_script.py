import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')

            color = record.get('color', '')
            color = re.sub(r'Body:|Shell:', ',', color, flags=re.IGNORECASE)
            color = re.sub(r'\(|\)', '', color)
            color = re.sub(r'\s*,\s*', '/', color)
            color = re.sub(r'/\bTie-Dye\b', f' Tie-Dye', color, flags=re.IGNORECASE)
            color = '/'.join(list(filter(None, color.split('/'))))
            colors = color.split('/')
            if len(colors) > 2 and not re.findall(r'Tie-Dye', color, flags=re.IGNORECASE):
                color = f'{colors[0]}/Multicolor'
            color = color.title()
            color = re.sub(r'\s+', ' ', color).strip()

            size = ''
            match = re.findall(r'(\d+\.*\d*)\s*("|”|inch)', record.get("size", ''))
            if match:
                size = match[0][0] + '"'

            description = record.get("bio") + ' ' + record.get("appearance")[:-1]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image_url', ''),
                "description": description,

                "model": model,
                "color": color,
                "character": record.get('character', ''),
                "product_type": record.get("product_type",""),
                "year": record.get("year",""),
                "size": size,
                "product_line": record.get("squad","")
            }

            return transformed_record
        else:
            return None
