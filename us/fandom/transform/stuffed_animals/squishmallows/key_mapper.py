class Mapper:
    def map(self, record):

        model = ''
        color = ''
        plush_size  = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "plush_size" == entity['name']:
                if entity["attribute"]:
                    plush_size = entity["attribute"][0]["id"]

        key_field = model + color + plush_size
        return key_field
