'''
https://mercari.atlassian.net/browse/USCC-643 - Squishmallows Stuffed Animals
https://mercari.atlassian.net/browse/USCC-644 - Ty Beanie Babies Stuffed Animals
https://mercari.atlassian.net/browse/USCC-645 - Dr. Seuss Children & Young Adult Books
'''

import os
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import urllib

class FandomSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from app_name.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stuffed Animals

    brand : str
        brand to be crawled, Supports
        1) Squishmallows
        2) Ty Beanie Babies

    Command e.g:
    scrapy crawl fandom -a category='Stuffed Animals' -a brand='Squishmallows'
    scrapy crawl fandom -a category='Stuffed Animals' -a brand='Ty Beanie Babies'
    scrapy crawl fandom -a category='Children & Young Adult Books' -a brand='Dr. Seuss'
    """

    name = 'fandom'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    source = 'fandom.com'
    blob_name = 'fandom.txt'

    category = ''
    brand = ''

    base_url = ''

    url_dict = {
        'Stuffed Animals': {
            'Squishmallows': {
                'url': 'https://squishmallowsquad.fandom.com/wiki/Master_List',
            },
            'Ty Beanie Babies': {
                'url': 'https://gyaanipedia.fandom.com/wiki/List_of_Beanie_Babies',
            },
        },
        'Children & Young Adult Books': {
            'Dr. Seuss': {
                'url': 'https://seuss.fandom.com/wiki/Category:Dr._Seuss_Books'
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(FandomSpider,self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.base_url = self.launch_url.split('/wiki')[0]

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.base_url = 'https://squishmallowsquad.fandom.com/'


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://squishmallowsquad.fandom.com/wiki/Abby
        @scrape_values title image_url character product_type color size squad year bio appearance
        @meta {"use_proxy":"True", "title": "Abby the Octopus"}
        """

        # for color in response.xpath('//h3[@class="pi-data-label pi-secondary-font"][contains(text(),"Color")]/following-sibling::div/a/text()').getall():
        color = ''.join(response.xpath('//h3[@class="pi-data-label pi-secondary-font"][contains(text(),"Color")]/following-sibling::div//text()').getall())
        for size in response.xpath('//h3[@class="pi-data-label pi-secondary-font"][contains(text(),"Size(s)")]/following-sibling::div/a/text()').getall():
            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': f'{response.url}?'+urllib.parse.urlencode({'color':color, 'size':size}),

                'title': response.meta.get("title", ""),
                'image_url': response.xpath("//figure[@data-source='image1']/a/@href").get() or '',
                'character': response.xpath("//h1[@id='firstHeading']/text()").get() or '',
                'product_type': response.xpath('//h3[@class="pi-data-label pi-secondary-font"][contains(text(),"Type")]/following-sibling::div/a/text()').get() or '',
                'color': color,
                'size': size,
                'squad': response.xpath('//h3[@class="pi-data-label pi-secondary-font"][contains(text(),"Squad")]/following-sibling::div/a/text()').get() or '',
                'year': response.xpath('//h3[@class="pi-data-label pi-secondary-font"][contains(text(),"Year")]/following-sibling::div/text()').get() or '',
                'bio': response.xpath('//span[@id="Bio"]/parent::h2/following-sibling::blockquote/p/text()').get() or '',
                'appearance': response.xpath('//span[@id="Appearance"]/parent::h2/following-sibling::p/text()').get() or ''
            }


    def crawlDetailSeuss(self, response):
        """
        @url https://seuss.fandom.com/wiki/Bartholomew_and_the_Oobleck
        @meta {"use_proxy":"True"}
        @scrape_values id image release_year author illustrator characters
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.meta.get('title', ''),
            'image': response.xpath("//div[@id='mw-content-text']/div[@class='mw-parser-output']/table/tbody/tr/td/a/@href").get() or response.xpath("//div[@id='mw-content-text']/div[@class='mw-parser-output']/figure/a/@href").get(),
            'release_year': response.xpath("normalize-space(//div[@id='mw-content-text']/div[@class='mw-parser-output']/table/tbody//td[contains(text(),'Published')]/following-sibling::td)").get(),
            'author': response.xpath("normalize-space(//div[@id='mw-content-text']/div[@class='mw-parser-output']/table/tbody//td[contains(text(),'Author')]/following-sibling::td)").get(),
            'illustrator': response.xpath("normalize-space(//div[@id='mw-content-text']/div[@class='mw-parser-output']/table/tbody//td[contains(text(),'Illustrator')]/following-sibling::td)").get(),
            'characters': response.xpath("normalize-space(//div[@id='mw-content-text']/div[@class='mw-parser-output']/table/tbody//td[contains(text(),'Characters')]/following-sibling::td)").get(),
            'publisher': response.xpath("normalize-space(//div[@id='mw-content-text']/div[@class='mw-parser-output']/table/tbody//td[contains(text(),'Publisher')]/following-sibling::td)").get()
        }


    def parse(self, response):
        """
        @url https://squishmallowsquad.fandom.com/wiki/Master_List
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        #for the source https://seuss.fandom.com/wiki/Category:Dr._Seuss_Books
        #crawling the detail page link and title
        #if category page is present passing it to this function again
        for link in response.xpath("//div[@class='category-page__members']//a[@class='category-page__member-link']"):
            url = link.xpath("./@href").get()
            title = link.xpath("./text()").get()
            if 'Category' in title:
                yield self.create_request(url=self.base_url+url,callback=self.parse)
            else:
                yield self.create_request(url=self.base_url+url,callback=self.crawlDetailSeuss,
                                        meta={'title':title})

        # for the source https://gyaanipedia.fandom.com/wiki/List_of_Beanie_Babies,
        # it contains items as a tabled information. And doesn't have the detail page.
        # So extracting/populating items by its title eg: https://gyaanipedia.fandom.com/wiki/List_of_Beanie_Babies?Name=#1 Teacher the bear
        for table in response.xpath("//table[@class='wikitable']"):
            for tr in table.xpath("./tbody/tr"):
                title = tr.xpath("normalize-space(./td)").get()
                if title:
                    yield {
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': str(response.status),
                        'category': self.category,
                        'brand': self.brand,
                        'id': self.launch_url + '?' + urllib.parse.urlencode({'Name':title}),

                        'title': title,
                        'date_of_creation': tr.xpath("normalize-space(./td[2])").get(),
                        'release_date': tr.xpath("normalize-space(./td[3])").get(),
                        'description': tr.xpath("normalize-space(./td[5])").get()
                    }


        # Crawling the detail page link and the title
        for selector in response.xpath("//div[@class='mw-parser-output']//li/a"):
            link = selector.xpath("./@href").get()
            title = selector.xpath("./text()").get()
            if link and title:
                yield self.create_request(url=self.base_url+link, callback=self.crawlDetail,
                    meta={"title":title})


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta['use_cache'] = True
        return request
