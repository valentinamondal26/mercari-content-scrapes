import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('details', {}).get('Description:', '')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()

            boot_shaft_height = ''
            material = ''
            match = re.findall(r'(\d+\.*\d*)f*"(.*)', record.get('details', {}).get('Upper:', ''))
            if match:
                boot_shaft_height, material = match[0]
                boot_shaft_height += ' in.'
                material = re.sub(r'(.*)(Leather)(.*)', r'\1 \3 \2', material, flags=re.IGNORECASE)
                if not re.findall(r'Leather', material, flags=re.IGNORECASE):
                    material += ' Leather'
                material = material.title().strip()


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                'boot_shaft_height': boot_shaft_height,
                'model_sku': record.get('details', {}).get('Style#:', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
