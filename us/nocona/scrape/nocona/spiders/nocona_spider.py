'''
https://mercari.atlassian.net/browse/USCC-702 - Nocona Men's Shoes
https://mercari.atlassian.net/browse/USCC-703 - Nocona Women's Shoes
https://mercari.atlassian.net/browse/USCC-704 - Nocona Unisex Kids' Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class NoconaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from nocona.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Shoes
        2) Women's Shoes
        3) Unisex Kids' Shoes
    
    brand : str
        brand to be crawled, Supports
        1) Nocona

    Command e.g:
    scrapy crawl nocona -a category="Men's Shoes" -a brand="Nocona"
    scrapy crawl nocona -a category="Women's Shoes" -a brand="Nocona"
    scrapy crawl nocona -a category="Unisex Kids' Shoes" -a brand="Nocona"
    """

    name = 'nocona'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'nocona.com'
    blob_name = 'nocona.txt'

    base_url = 'https://www.nocona.com'

    url_dict = {
        "Men's Shoes": {
            "Nocona": {
                'url': 'https://www.nocona.com/footwear/men_shop-all-mens-boots',
            }
        },
        "Women's Shoes": {
            "Nocona": {
                'url': 'https://www.nocona.com/footwear/women_shop-all-womens-boots',
            }
        },
         "Unisex Kids' Shoes": {
            "Nocona": {
                'url': 'https://www.nocona.com/footwear/kids_nocona-kids',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(NoconaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.nocona.com/footwear/men/shop-all-mens-boots/MD2703
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb title image price details details.Description: details.Upper: details.Outsole:
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//ul[@class="breadcrumbs"]/li/a/text()').getall(),
            'image': response.xpath('//div[@id="product-main-image"]//img/@src').get(),
            'title': response.xpath('//h2[@class="product-title"]/text()').get(),
            'price': ''.join(
                    response.xpath('//div[@class="large-4 medium-4 columns text-right nopad show-for-medium-up"]/h3[@class="product-price"]//text()').getall()
                ).strip(),
            'details': dict(zip(
                response.xpath('//div[@id="panel-boot-details"]/dl/dt/text()').getall(),
                [dd.xpath('./text()').get().strip() or dd.xpath('.//a/text()').get().strip() for dd in response.xpath('//div[@id="panel-boot-details"]/dl/dd')]
            )),
        }


    def parse(self, response):
        """
        @url https://www.nocona.com/footwear/men_shop-all-mens-boots
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="row"]/ul[contains(@class, "category-products")]/li'):
            product_url = product.xpath('./a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
