class Mapper:
    def map(self, record):

        model = ''
        release_year = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "release_year" == entity['name']:
                if entity["attribute"]:
                    release_year = entity["attribute"][0]["id"]

        key_field = model + release_year
        return key_field
