import hashlib
import re
import unicodedata
class Mapper(object):

    def map(self, record):
        if record:
            spec_dictionary = record.get("spec_dict",{}) or {}
            spec_info = record.get("spec_info",[])
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            title = record.get('title','')

            ner_query = re.sub(r'\s+', ' ', f"{title} \
                {' '.join(list(map(lambda x: f'{x[0]}: {x[1]}', spec_dictionary.items())))} \
                {' '.join(spec_info)}").strip()

            model = ''
            match = re.split("-boyds bears", title, flags=re.IGNORECASE)
            if match:
                model = match[0]
            
            model = unicodedata.normalize('NFKD', model).encode('ascii', 'ignore').decode('utf8')
            model = re.sub(r'F\.O\.B\.*', 'FOB', model, flags=re.IGNORECASE)
            model = re.sub(r'w\/', f'with ', model, flags=re.IGNORECASE)
            model = re.sub(r'\…\.|\…|\.{2,}', ' ', model, flags=re.IGNORECASE)
            model = re.sub(r'(\-\s+Boyds .*)|(\-Boyds .*)|(-Judith .*)|(\-\bBoydsget\b .*)|(\-\bQvc Exclusive .*)|(\-\bLongaberger\b .*)|(\-\bByods\b .*)|(\-\bSunflower Boyds\b .*)(\bBoyds\b .*)', '', model, flags=re.IGNORECASE).strip()
            model = re.sub(r'\*Novembeary\b', 'Novembeary', model, flags=re.IGNORECASE)
            model = re.sub(r'\bArctic Adventure-Bailey, Edmund, Mush-Mush, Igloo, Dog Sled\b', 'Arctic Adventure: Bailey, Edmund, Mush-Mush, Igloo & Dog Sled', model, flags=re.IGNORECASE)
            model = re.sub(r'\bBailey Spring 2001-Cindarella Boyds Bears #9199-16 \*', 'Bailey Spring 2001', model, flags=re.IGNORECASE)
            model = re.sub(r'\bBAILEYS JUKE BOX\b', "Bailey's Jukebox", model, flags=re.IGNORECASE)
            model = re.sub(r"\bBearstone Santa's Toy Factory-The Elfbeary Workshop, Ringle, Dingle, Merv\)", "Bearstone Santa's Toy Factory: The Elfbeary Workshop, Ringle, Dingle, & Merv", model, flags=re.IGNORECASE)
            model = re.sub(r'Boyds Bears Judith G Collection Ladybug Bear \*\*\*RARE\*\*\* \*', 'Ladybug Bear', model, flags=re.IGNORECASE)
            model = re.sub(r'Boyds Bears Logo Sign #790-49', 'Boyds Bears Logo Sign', model, flags=re.IGNORECASE)
            model = re.sub(r'\bEdmund Spring 2001-Once Upon a Hiccup\b', 'Edmund Spring 2001', model, flags=re.IGNORECASE)
            model = re.sub(r'\bLILY BEARYBLOOM\-', 'Lily Bearybloom', model, flags=re.IGNORECASE)
            model = re.sub(r'\bLucky Liam- Bears #919862 BBC Exclusive BOM \*\*\*Hard to Find\*\*\* \*', 'Lucky Liam BBC Exclusive', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMr. Kringlebeary, Elmer & Christmas Tree-99025SR \*\*\*Hard to Find\*\*\* \*', 'Mr. Kringlebeary, Elmer & Christmas Tree', model, flags=re.IGNORECASE)
            model = re.sub(r'\bN. Mouseking Boyds Bears Resin Shoebox Bear #3223 \*', 'N. Mouseking', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPretty Hat Lady Judith G Boyds Bears-Exclusive \*\*\*RARE\*\*\* \*', 'Pretty Hat Lady', model, flags=re.IGNORECASE)
            model = re.sub(r'\bRosieThe Boyds Bears Collector Doll- Danbury Mint Exclusive\b', 'Rosie Danbury Mint Exclusive', model, flags=re.IGNORECASE)
            model = re.sub(r"Wally's Watermelon with Pip McNibble\"#392142", "Wally's Watermelon with Pip McNibble", model, flags=re.IGNORECASE)
            model = re.sub(r'Amy Quiltbeary-Country Patchwork Boyds Bears #904680 \*', 'Amy Quiltbeary', model, flags=re.IGNORECASE)
            model = re.sub(r'\bBailey Fall 1992-First Bailey Boyds Bears-Rare! Item #9199 \*\*\*Rare\*\*\* \*', 'Bailey Fall 1992', model, flags=re.IGNORECASE)
            model = re.sub(r'\bOur American Hero Strength, Dedication And Courage\b', 'Our American Hero Strength, Dedication & Courage', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMiss Hattie & Company Springtime Friends #27368 \*', 'Miss Hattie & Company Springtime Friends', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMolly W /Raffe\b', 'Molly with Raffe', model, flags=re.IGNORECASE)
            retain_case_words = ['MOM','von','FOB','with', 'II', 'III', 'IV', 'VI', 'VII', 'VIII', 'IX', 'X']
            model = ' '.join([mod.title() if not re.findall(r'|'.join(list(map(lambda x: r'\b'+x + r'\b', retain_case_words))), mod) else mod for mod in model.split()])
            model = re.sub(r'(\'\w+)\b', lambda x: x.group().lower(), model, flags=re.IGNORECASE)
            model_skus = []
            model = re.sub(r'\bSonny-Cheer Up! Boyds Bears #903023\b', 'Sonny Cheer Up!', model, flags=re.IGNORECASE)
            model = re.sub(r'\bSunshine Q. Bearsley-Sunflower Boyds Bears #919847 Bbc Exclusive Bom \*', 'Sunshine Q. Bearsley', model, flags=re.IGNORECASE)
            model = re.sub(r'\b(\d+Th)\b|\b(\d+Rd)\b|\b(\d+st)\b', lambda x: x.group().lower(), model, flags=re.IGNORECASE)
            model = re.sub(r'\bBailey The Night Before Christmasboyds Bears Musical Bearstone #270501 \*$', 'Bailey The Night Before Christmas', model, flags=re.IGNORECASE)
            model = re.sub(r'\bLucinda And Dawn By The Sea Boyds Bears Resin Dollstone Yesterday\'s Child #3536\b', 'Lucinda And Dawn By The Sea', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMr\. Beesley 30" Boyds Bears #919848\b', 'Mr. Beesley 30in.', model, flags=re.IGNORECASE)
            for word in title.split(' '):
                if len(word) > 2 \
                        and "#" in word \
                        and not re.findall(r'#unknown', word, flags=re.IGNORECASE):
                    match = re.findall('(#.*)', word)
                    if match:
                        sku = match[0].replace(',', '').strip()
                        if len(sku) > 1:
                            model_skus.append(sku)
            if not model_skus:
                match = re.findall(r'(#\s*.*?[\b\s])', title)
                if match:
                    model_skus.extend(match)

            model_sku = ', '.join(list(map(lambda x: re.sub(r'\s', '', x), model_skus)))

            release_year = ''
            data = re.sub('Introduced', 'Introduced ', re.sub(r'#\s+', '#', f' {ner_query} '))
            match = re.findall(r'[\s\:\-\`]((19|20)\d{2})[\*\.,\s\-;\!\~]', data)
            if match:
                release_year = sorted(map(lambda x: x[0], match), reverse=True)[0]


            size = ""
            for word in spec_info:
                size = re.search(r"\d+\.*\d* *in[a-z]*\.*",word)
                if size:
                    size = size.group()
                    size = re.sub(r"[a-zA-Z]*","",size)
                    size = size+"\x22"
                    break
                size = re.search(r"H*e*i*g*h*t*:* \d+\.*\d*\x22",word)
                if size:
                    size = size.group()
                    size = re.sub(r"[a-zA-Z]*","",size)
                    size = size.replace(":","")
                    break
                size = re.search(r"\d+ \d\/\d",word)
                if size:
                    size = size.group()
                    size = re.sub(r"[a-zA-Z]*","",size)
                    size = size+"\x22"
                    break

                if size == "":
                    size = spec_dictionary.get("Height","")

            if not size:
                size = spec_dictionary.get('Height', '')
                if not size:
                    match = re.findall(r'(\d+\.*\d*|\d+\s*\d\/\d)("\s*h|"|inches|inch|in\.)', ner_query)
                    if match:
                        size = list(map(lambda x: x[0], match))[0]
                        size = size.strip() + '"'

            if re.findall(r'\bGIFT CERTIFICATE\b', model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "image": record.get('image', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "model_sku": model_sku,
                "release_year": release_year,
                "size": size,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                }
            }

            return transformed_record
        else:
            return None
