'''
https://mercari.atlassian.net/browse/USCC-652 - Boyds Bears Stuffed Animals
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import pandas as pd
import re
import os

class BoydsbearsstoreSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from app_name.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stuffed Animals

    brand : str
        brand to be crawled, Supports
        1) Boyds Bears

    Command e.g:
    scrapy crawl boydsbearsstore -a category='Stuffed Animals' -a brand='Boyds Bears'
    """

    name = 'boydsbearsstore'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'boydsbearsstore.com'
    blob_name = 'boydsbearsstore.txt'

    base_url = 'https://www.boydsbearsstore.com'

    url_dict = {
        'Stuffed Animals': {
            'Boyds Bears': {
                'url': 'https://www.boydsbearsstore.com/apps/webstore/products'
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BoydsbearsstoreSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.boydsbearsstore.com/apps/webstore/products/show/8223319
        @meta {"use_proxy":"True"}
        @scrape_values id title spec_info price image
        """

        spec_info = response.xpath("//div[@class='product-info-container']//p/text()").getall()
        spec_info = set(spec_info)
        spec_info = list(spec_info)
        raw_data = {
            # 'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            # 'statusCode': str(response.status),
            # 'category': self.category,
            # 'brand': self.brand,
            # 'id': response.url,

            'title': response.xpath("//div[@class='product-info-container']/h2/text()").get(),
            'spec_info': spec_info,
            'price': response.xpath("//b[@class='theme-color-text']/span/text()").get(),
            'image':  response.xpath("//div[@class='large-image-container']/a/@data-image-url").get()
        }
        html_string = response.xpath("//table/tr/td/table").get() or ''
        if html_string != "":
            dfs = pd.read_html(html_string)
            df = dfs[0]
            spec_dict = {}
            for key_value in df.values:
                key_value = key_value[0].split(":")
                spec_dict[key_value[0]] = key_value[1]
            raw_data['spec_dict'] = spec_dict
        yield raw_data


    def parse(self, response):
        """
        @url https://www.boydsbearsstore.com/apps/webstore/products
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for link in response.xpath("//div[@class='product-list']/div//div[@class='product-name']/a/@href").getall():
            if link:
                yield self.create_request(url=self.base_url + link, callback=self.crawlDetail)
        next_page_link = response.xpath('//div[@class="pagination"]//a[@class="next_page"]/@href').get()
        if next_page_link:
            yield self.create_request(url=self.base_url + next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request


