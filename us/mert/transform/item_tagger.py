import requests
import traceback
import json
from requests import Timeout
from us.product.transform.util.gcs_iap_request_util import get_google_open_id_connect_token


class Mapper(object):
    def __init__(self):
        # Replace with the Tagger API #
        self.url = "https://ml-de-gateway.double.dev5s.com/ml-mert/v1/predict/ner"
        self.client_id = '529857377597-53llchg9pbpsgt627bc3ohso6hr2i2rs.apps.googleusercontent.com'
        if not hasattr(self, 'token'):
             print("Initializing Identity Token in constructor")
             self.token = get_google_open_id_connect_token(self.client_id)
        self.header = {
            "Authorization": "Bearer {}".format(self.token),
            "Content-Type": "application/json"}
        return

    def map(self, record):
        try:
            if 'keyword' in record:
                name = record['keyword']
                variant = 'query'
            else:
                name = record['name']
                variant = 'item'

            payload = {
                "variant": variant,
                "name": [
                    name
                ]
            }
            with requests.post(self.url, json=payload, timeout=60, headers=self.header) as r:
                if r.status_code == requests.codes.ok:
                    res = r.json()
                    response = json.dumps(res)
                    record['response'] = response
                elif r.status_code == 403:
                    print("FAILED: Status 403")
                    self.token = get_google_open_id_connect_token(self.client_id)
                    self.header = {"Authorization": "Bearer {}".format(self.token)}
                else:
                    print(r.text)
                    print("FAILED: Status_code {0} - {1} ".format(r.status_code, self.url))

        except Exception as e:
            print("Exception Raised for {} with exception {}".format(record, str(e)))
            print(traceback.format_exc())
            return None

        return record
