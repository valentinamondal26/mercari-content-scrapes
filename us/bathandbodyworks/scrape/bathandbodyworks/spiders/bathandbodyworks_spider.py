'''
https://mercari.atlassian.net/browse/USCC-389 - Bath & Body Works Body Lotions & Moisturizers
https://mercari.atlassian.net/browse/USCC-379 - Bath & Body Works Candles & Home Scents
https://mercari.atlassian.net/browse/USCC-52 - Bath & Body Works Fragrances for Women
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import re
import os
import urllib.parse as urlparse 
from urllib.parse import parse_qs 

class BathandbodyworksSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bathandbodyworks.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Body Lotions & Moisturizers
        2) Fragrances for Women
        3) Candles & Home Scents
    
    brand : str
        brand to be crawled, Supports
        1) Bath & Body Works

    Command e.g:
    scrapy crawl bathandbodyworks -a category="Body Lotions & Moisturizers" -a brand='Bath & Body Works'
    scrapy crawl bathandbodyworks -a category="Candles & Home Scents" -a brand='Bath & Body Works'
    scrapy crawl bathandbodyworks -a category="" -a brand='Bath & Body Works'
    """

    name = 'bathandbodyworks'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = ''

    source = 'bathandbodyworks.com'
    blob_name = 'bathandbodyworks.txt'

    url_dict = {
        'Body Lotions & Moisturizers': {
            'Bath & Body Works': {
                'url': [
                    'https://www.bathandbodyworks.com/c/body-care/body-cream',
                    'https://www.bathandbodyworks.com/c/body-care/body-lotion',
                    'https://www.bathandbodyworks.com/c/body-care/body-massage-oils',
                ],
                'filters': ['Product Type', 'Fragrance Name', 'Fragrance Category'],
            }
        },
        'Fragrances for Women': {
            'Bath & Body Works': {
                'url': 'https://www.bathandbodyworks.com/c/body-care/perfume-cologne',
                'filters': ['Product Type', 'Fragrance Name', 'Fragrance Category'],
            }
        },
        'Candles & Home Scents': {
            'Bath & Body Works': {
                'url': 'https://www.bathandbodyworks.com/c/home-fragrance/all-candles',
                'filters': ['Product Type', 'Fragrance Name', 'Fragrance Category'],
            }
        },
    }

    base_url = 'https://www.bathandbodyworks.com'

    filter_list = []
    
    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BathandbodyworksSpider,self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {category} and brand: {self.brand}, url: {self.launch_url}")

    def contracts_mock_function(self):
        self.filter_list = ['Product Type', 'Fragrance Name', 'Fragrance Category']

    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            if isinstance(self.launch_url, list):
                for url in self.launch_url:
                    yield self.create_request(url=url, callback=self.parse_filters)
            else:
                yield self.create_request(url=self.launch_url, callback=self.parse_filters)


    def crawlDetail(self, response):
        """
        @url https://www.bathandbodyworks.com/p/rose-vanilla-moisturizing-body-oil-024354802.html?cgid=body-massage-oils#start=1
        @scrape_values title sub_title image price breadcrumb size fragrance_description description collection
        @meta {"use_proxy":"True"}
        """
        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': ''.join(response.xpath('//h1[@itemprop="name"]/text()[normalize-space(.)]').extract()).strip(),
            'sub_title': response.xpath(u'normalize-space(//span[@class="small-title"]/text())').extract_first(default=''),
            'price': response.xpath('//span[@itemprop="price"]/text()').extract_first(default=''),
            'image': response.xpath('//img[@itemprop="image"]/@src').extract_first(default=''),

            'breadcrumb': ' | '.join(response.xpath('//div[@class="breadcrumb"]/span//text()[normalize-space(.)]').extract()),
            'size': response.xpath(u'normalize-space(//div[@class="product-price"]/following-sibling::div[@class="small-title"]/text())').extract_first(default=''),
            'fragrance_description': response.xpath(u'normalize-space(//div[@id="pdp-accordion"]/h3[contains(text(), "Fragrance")]/following-sibling::div/text())').extract_first(default=''),
            'description': response.xpath(u'normalize-space(//div[@itemprop="description"]/text())').extract_first(default=''),
            'collection': response.xpath(u'normalize-space(//div[@class="small-title brand-tlt"]/text())').extract_first(default=''),
        }


    def parse_filters(self, response):
        """
        @url  https://www.bathandbodyworks.com/c/body-care/body-lotion
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        total_items_count = ''
        match = re.findall(r'\d+', response.xpath('//span[@class="results-hits"]/text()').extract_first(default=''))
        if match:
            total_items_count = match[0]
        print(f'total_items_count: {total_items_count}')
        _filter_list = response.xpath('//div[@class="filters refinements"]/child::div')
        for _filter in _filter_list:
            filter_name = _filter.xpath('./h3/text()[normalize-space(.)]').extract_first(default='').strip()
            if filter_name in self.filter_list:
                for refinement in _filter.xpath('.//li/a'):
                        filter_value = refinement.xpath(u'./text()[normalize-space(.)]').extract_first(default='').strip()
                        link = refinement.xpath('./@href').extract_first()
                        if link:
                            request = self.create_request(url=link, callback=self.parse)
                            request.meta['extra_item_info'] = {
                                filter_name: filter_value, 
                            }
                            yield request

        if not _filter_list:
            yield self.create_request(url=response.url, callback=self.parse, dont_filter=True)


    def parse(self, response):
        """
        @url  https://www.bathandbodyworks.com/c/body-care/body-lotion
        @meta {"use_proxy":"True"}
        @returns requests 49
        """
        ##No of requests mentioned in contract is 49 because 48 items per page and 1 Next page url.
        extra_item_info = response.meta.get('extra_item_info', {})

        # query_param = urlparse.urlparse(response.url).query
        # current_offset = parse_qs(query_param)['start'][0]
        for product in response.xpath('//div[@class="product-tile"]'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                url = self.base_url + id
                if not self.extra_item_infos.get(url, None):
                    self.extra_item_infos[url] = {}

                self.extra_item_infos.get(url).update(extra_item_info)

                request = self.create_request(url=url, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('//link[@rel="next"]/@href').extract_first(default='')
        if nextLink:
            yield self.create_request(url=nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
