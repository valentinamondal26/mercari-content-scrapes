import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = re.sub(re.compile(r'\$|Now', re.IGNORECASE), '', price)
            price = re.sub(' +', ' ', price).strip()

            size = ''
            match = re.findall(re.compile(r'(\d+){0,1}(.\d+){0,1}(\s+fl){0,1}(\s+oz)', re.IGNORECASE), record.get('size', ''))
            if match:
                group = match[0]
                size = ''.join(group)


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": record.get('fragrance_description', ''),
                "model": record.get('title', ''),
                "product_type": record.get('Product Type', ''),
                "fragrance_name": record.get('Fragrance Name', ''),
                'fragrance_type': record.get('Fragrance Category', ''),
                'collection': record.get('collection', ''),
                'size': size,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
