class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        fragrance_type = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "fragrance_type" == entity['name']:
                if entity["attribute"]:
                    fragrance_type = entity["attribute"][0]["id"]

        key_field = model + fragrance_type
        return key_field
