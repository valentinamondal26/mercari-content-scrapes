import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = re.sub(re.compile(r'\$|Now', re.IGNORECASE), '', price)
            price = re.sub(' +', ' ', price).strip()

            size = ''
            match = re.findall(re.compile(r'(\d+){0,1}(.\d+){0,1}(\s+fl){0,1}(\s+oz)', re.IGNORECASE), record.get('size', ''))
            if match:
                group = match[0]
                size = ''.join(group)

            model = '{} {}'.format(record.get('title', ''), record.get('sub_title', ''))
            model = re.sub(' +', ' ', model).strip()

            # product_type = record.get('Product Type', '')
            product_type = re.sub(re.compile(r"\bRoll-On\b|\bCologne\b|\bHair\b|\bPerfume\b|\bEau de Parfum\b", re.IGNORECASE), '', model)
            product_type = re.sub(' +', ' ', product_type).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                "product_type": product_type,
                "fragrance_name": record.get('Fragrance Name', ''),
                'fragrance_type': record.get('Fragrance Category', ''),
                'size': size,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
