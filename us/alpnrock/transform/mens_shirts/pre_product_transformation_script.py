import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', [''])[0]
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'(.*)(Shirt)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            if not re.findall(r'\bShirt\b', model, flags=re.IGNORECASE):
                model = f'{model} Shirt'
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()
            model = re.sub(r'\bUsa\b', 'USA', model, flags=re.IGNORECASE)

            material = ''
            material_info = ' '.join(record.get('product_details', {}).get('Material & Care', []))

            materials_meta = ['nylon', 'recycled polyester', 'polyester', 'Soft Cotton', 'Cotton',
                'Rayon', 'Spandex', 'modal', 'tencel']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), material_info, flags=re.IGNORECASE)
            if match:
                material = '/'.join(list(dict.fromkeys(match)))
                material = material.title()

            color = record.get('color', '')
            color = re.sub(r'\bBark\b', 'Bark Brown', color, flags=re.IGNORECASE)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
