import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', [''])[0]
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()

            material = ''
            material_info = ' '.join(record.get('product_details', {}).get('Material & Care', []))

            materials_meta = ['nylon', 'recycled polyester', 'polyester', 'Soft Cotton', 'Cotton',
                'Rayon', 'Spandex', 'modal', 'tencel']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), material_info, flags=re.IGNORECASE)
            if match:
                material = '/'.join(list(dict.fromkeys(match)))
                material = material.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": record.get('color', ''),
                "material": material,
                'size': ', '.join(record.get('size', [])),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
