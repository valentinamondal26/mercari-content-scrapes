'''
https://mercari.atlassian.net/browse/USCC-660 - Alp N Rock Women's Coats, Jackets & Vests
https://mercari.atlassian.net/browse/USCC-661 - Alp N Rock Men's Shirts
https://mercari.atlassian.net/browse/USCC-662 - Alp N Rock Women's Sweaters
https://mercari.atlassian.net/browse/USCC-663 - Alp N Rock Women's Tops
https://mercari.atlassian.net/browse/USCC-664 - Alp N Rock Women's Pants
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import pandas as pd
import copy

class AlpnrockSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from alpnrock.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Coats, Jackets & Vests
        2) Women's Sweaters
        3) Women's Tops
        4) Women's Pants
        5) Men's Shirts

    
    brand : str
        brand to be crawled, Supports
        1) Alp N Rock

    Command e.g:
    scrapy crawl alpnrock -a category="Women's Coats, Jackets & Vests" -a brand='Alp N Rock'
    scrapy crawl alpnrock -a category="Women's Sweaters" -a brand='Alp N Rock'
    scrapy crawl alpnrock -a category="Women's Tops" -a brand='Alp N Rock'
    scrapy crawl alpnrock -a category="Women's Pants" -a brand='Alp N Rock'
    scrapy crawl alpnrock -a category="Men's Shirts" -a brand='Alp N Rock'
    """

    name = 'alpnrock'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'alpnrock.com'
    blob_name = 'alpnrock.txt'

    merge_key = 'id'

    base_url = 'https://alpnrock.com'

    url_dict = {
        "Women's Coats, Jackets & Vests": {
            'Alp N Rock': {
                'url': [
                    'https://alpnrock.com/collections/womens-vests',
                    'https://alpnrock.com/products/f20-marin-vest',
                ]
            }
        },
        "Women's Sweaters": {
            'Alp N Rock': {
                'url': 'https://alpnrock.com/collections/womens-sweaters'
            }
        },
        "Women's Tops": {
            'Alp N Rock': {
                'url': 'https://alpnrock.com/collections/tops'
            }
        },
        "Women's Pants": {
            'Alp N Rock': {
                'url': 'https://alpnrock.com/collections/bottoms'
            }
        },
        "Men's Shirts": {
            'Alp N Rock': {
                'url': 'https://alpnrock.com/collections/mens-shirts'
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AlpnrockSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {category} and brand {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                if 'products' in url:
                    yield self.create_request(url=url, callback=self.crawlDetail)
                else:
                    yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://alpnrock.com/products/f20-marin-vest
        @meta {"use_proxy":"True"}
        @scrape_values id image title price description features product_details size color
        """

        # pprint(options)
        # from options derive the variants and populate the variants [urls and create request] or [yield items]

        sizes = response.xpath('//div[@class="ProductForm__Variants"]/div[@class="ProductForm__Option ProductForm__Option--labelled ProductForm__Option--size"]/span[@class="ProductForm__SizeWrap"]/ul/li/label/text()').extract()

        details = {}
        for product_details in response.xpath('//div[@class="Product__Tabs"]/div'):
            key = product_details.xpath(u'normalize-space(./button[@class="Collapsible__Button Heading u-h6"]/text())').extract_first()
            value = product_details.xpath('.//ul/li/text()').extract()
            if not value:
                table = product_details.xpath('.//table').extract_first()
                df = pd.read_html(table)
                if df:
                    value = df[0].to_dict(orient='records')
            details[key] = value

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//nav[@aria-label="breadcrumbs"]/span/text()').extract(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'title': response.xpath('//div[@class="ProductMeta"]/h1[@class="ProductMeta__Title Heading u-h4"]/text()').extract_first(),
            'price': response.xpath('//div[@class="ProductMeta__PriceList Heading"]//span/text()').extract(),
            'description': response.xpath('//div[@class="ProductMeta__Description"]//p/text()').extract_first(),
            'features': response.xpath('//div[@class="ProductMeta__FeatureList"]/ul/li//p/text()').extract(),
            'product_details': details,
            'size': sizes,
            'color': response.xpath('//div[@class="ProductForm__Option ProductForm__Option--labelled"]/span[@class="ProductForm__ColorWrap"]/span[@class="ProductForm__SelectedValue"]/text()').get(),
        }

        if 'variant' in response.url:
            yield item
        else:
            options = []
            for option in response.xpath('//div[@class="no-js ProductForm__Option"]/div/select[@title="Variant"]/option'):
                value = option.xpath('./text()').get()
                options.append(
                    {
                        'id': option.xpath('./@value').get(),
                        'selected': option.xpath('./@selected').get(),
                        'sku': option.xpath('./@data-sku').get(),
                        'value': value,
                        'color': value.split('/')[0].strip(),
                        'size': value.split('/')[-1].split('-')[0].strip(),
                    }
                )
            for option in list(filter(lambda x: x['size'] == sizes[0], options)):
                variant = copy.deepcopy(item)
                variant.update({
                    'color': option.get('color', ''),
                    'id': response.url.split('?')[0] + f"?variant={option.get('id', '')}",
                })
                yield variant


    def parse(self, response):
        """
        @url https://alpnrock.com/collections/womens-vests
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.xpath('//div[@class="CollectionInner__Products"]//div[@class="ProductItem "]'):
            product_url = product.xpath('.//h2[@class="ProductItem__Title"]/a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url if not product_url.startswith(self.base_url) else product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//link[@rel="next"]/@href').extract_first()
        if next_page_link:
            next_page_link = self.base_url + next_page_link if not next_page_link.startswith(self.base_url) else next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
