'''
https://mercari.atlassian.net/browse/USCC-291 - Lululemon Athletica Women's Activewear Jackets
https://mercari.atlassian.net/browse/USCC-237 - Lululemon Athletica Women's Activewear Tops
https://mercari.atlassian.net/browse/USCC-99 - Lululemon Athletica Women's Shorts
https://mercari.atlassian.net/browse/USCC-97 - Lululemon Athletica Women's Shorts
https://mercari.atlassian.net/browse/USCC-9 - Lululemon Athletica Pants, tights, leggings
'''

import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

from urllib.parse import urlparse, parse_qs, urlencode


class LululemonSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from lululemon.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Pants, tights, leggings
        2) Women's Activewear Jackets
        3) Women's Activewear Tops
        4) Women's Shorts
        5) Women's Activewear Sports Bras

    sub_category : str
        brand to be crawled, Supports
        1) Lululemon Athletica

    Command e.g:
    scrapy crawl lululemon -a category="Women's Activewear Tops" -a brand='Lululemon Athletica'
    scrapy crawl lululemon -a category="Pants, tights, leggings" -a brand='Lululemon Athletica'
    scrapy crawl lululemon -a category="Women's Activewear Jackets" -a brand='Lululemon Athletica'
    scrapy crawl lululemon -a category="Women's Shorts" -a brand='Lululemon Athletica'
    scrapy crawl lululemon -a category="Women's Activewear Sports Bras" -a brand='Lululemon Athletica'
    """

    name = "lululemon"

    category = None
    brand = None

    merge_key = 'id'

    source = 'lululemon.com'
    blob_name = 'lululemon.txt'

    base_url = 'https://shop.lululemon.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    url_dict = {
        'Pants, tights, leggings': {
            'Lululemon Athletica': {
                'url': 'https://shop.lululemon.com/c/womens-leggings/_/N-8s6?mnid=mn;en-US-JSON;women;leggings',
                'filters': [],
            }
        },
        "Women's Activewear Jackets": {
            'Lululemon Athletica': {
                'url': 'https://shop.lululemon.com/c/jackets-and-hoodies-jackets/_/N-8s2?mnid=mn;en-US-JSON;women;coats-&-jackets',
                'filters': ["Type", "Activity", "Insulation Type", "Warmth Ratings", "Colour"],
            }
        },
        "Women's Activewear Tops": {
            'Lululemon Athletica': {
                'url': 'https://shop.lululemon.com/c/women-maintops/_/N-815?icid=US',
                'filters': ["Type", "Activity", "Colour"],
            }
        },
        "Women's Shorts": {
            'Lululemon Athletica': {
                'url': 'https://shop.lululemon.com/c/women-shorts/_/N-7we?icid=US&lw=true',
                'filters': ["Inseam", "Rise", "Activity", "Colour"],
            }
        },
        "Women's Activewear Sports Bras": {
            'Lululemon Athletica': {
                'url': 'https://shop.lululemon.com/c/women-sports-bras/_/N-8n9',
                'filters': ["Support", "Activity", "Colour", "Collections", "Cup & Band Size"],
            }
        },
    }

    product_list_url = "https://shop.lululemon.com/api{path}?page={page}&page_size={size}"

    total_page_size = 0

    filter_list = []

    extra_item_infos = {}

    result_id_list = []


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(LululemonSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and sub_category:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and sub_category:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filter_list = ["Type", "Activity", "Colour"]


    def parse_filters(self, response):
        """
        @url https://shop.lululemon.com/api/c/women-maintops/_/N-815?page=1&page_size=129
        @returns requests 1
        """

        res = json.loads(response.text)

        secondary_contents = res.get('data', {}).get('attributes', {}).get('secondary-content', [])
        for secondary_content in secondary_contents:
            if secondary_content.get('type', '') == 'ContentSlotSecondary':
                contents = secondary_content.get('contents', [])
                for content in contents:
                    if content.get('type', '') == 'GuidedNavigation':
                        navigations = content.get('navigation', [])
                        for navigation in navigations:
                            if navigation.get('name', '') in self.filter_list:
                                refinements = navigation.get('refinements', [])
                                for refinement in refinements:
                                    url = self.product_list_url.format(path=refinement.get('content-path', '') + refinement.get('navigation-state', ''), page=1, size=refinement.get('count', ''))
                                    request = self.create_request(url, self.parse_detail)
                                    request.meta['extra_item_info'] = {
                                        navigation.get('name', ''): refinement.get('label', ''),
                                    }
                                    print(f'filter_url:{url}')
                                    yield request


    def parse(self, response):
        """
        @url https://shop.lululemon.com/c/women-maintops/_/N-815?icid=US
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        total_count = response.xpath('//span[@class="filter-results"]/text()').extract_first(default="") \
        	or response.xpath("//p[@class='results grid-sort__results']/text()").get(default='')
        total_count = total_count.replace('items','').strip()
        print('total_count=' + total_count)
        self.total_page_size = total_count
        if total_count:
            o = urlparse(response.url)
            url = self.product_list_url.format(path=o.path, page=1, size=total_count)
            print('product_url=' + url)
            if self.filter_list:
                yield self.create_request(url, self.parse_filters)
            else:
                yield self.create_request(url, self.parse_detail)


    def parse_detail_page(self, response):
        """
        @url https://shop.lululemon.com/p/women-pants/Align-Pant-2/_/prod2020012?color=45322
        @scrape_values description details.Buttery-soft,\sNulu™\sFabric details.Product\sFeatures
        @meta {"use_proxy": "True"}
        """

        item_info = response.meta.get('item_info', {})
        description = response.xpath('//p[@class="why-we-made-this__text"]/text()').extract_first()
        item_info['description'] = description
        details = {}
        for detail in response.xpath('//div[@data-testid="accordion"]/div[contains(@data-testid,"accordion-item-")]'):
            key=detail.xpath('.//span[contains(@class,"accordionItemHeadingTitle-")]/text()').extract_first()
            value=detail.xpath('.//li[@class="product-education-accordions__attributes__item"]/span/text()').extract()
            details[key] = value
        item_info['details'] = details

        yield item_info


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def parse_detail(self, response):
        """
        @url https://shop.lululemon.com/api/c/womens-leggings/_/N-8s6?page=1&page_size=114
        @returns requests 1
        @scrape_values meta.item_info.title meta.item_info.price meta.item_info.color meta.item_info.sizes
        @meta {"use_proxy": "True"}
        """

        res = json.loads(response.text)
        main_contents = res.get('data', {}).get('attributes', {}).get('main-content', [])
        for main_content in main_contents:
            if main_content.get('type', '') == 'CDPResultsList':
                records = main_content.get('records', [])
                for record in records:
                    colors = record.get('sku-style-order', [])
                    for color in colors:
                        id = '{base_url}{path}?color={color_id}'.format(base_url=self.base_url, path=record.get('pdp-url', ''), color_id=color.get('color-id', ''))
                        if not self.extra_item_infos.get(id, None):
                            self.extra_item_infos[id] = {}

                        # self.update_extra_item_infos(id, response.meta.get('extra_item_info', {}))
                        self.extra_item_infos.get(id).update(response.meta.get('extra_item_info', {}))
                        if not id in self.result_id_list:
                            self.result_id_list.append(id)
                            request = self.create_request(url=id, callback=self.parse_detail_page)
                            request.meta['item_info'] = {
                                "crawlDate": str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
                                "statusCode": "200",
                                "id": id,
                                "brand": self.brand,
                                "category": self.category,
                                "title": record.get('display-name', ''),
                                "price": record.get('list-price', ''),
                                "color": color.get('color-name', ''),
                                'sizes': record.get('all-available-sizes', [])
                            }
                            yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=True)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request