import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace("$", "").strip()

            cup_band_size = record.get(' Cup & Band Size', '')

            model = record.get('title', '')
            model = model.split('|', 1)[0]
            model = model.replace('*', '')
            model = re.sub(re.compile(r'Bra', re.IGNORECASE), ' ', model)
            model = re.sub(re.compile(cup_band_size, re.IGNORECASE), '', model)
            if not re.findall(re.compile(r'\dLululemon X Barry\'s\d|\dlululemon lab\d', re.IGNORECASE), model):
                model = re.sub(re.compile(r'Lululemon', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\blululemon lab\b', re.IGNORECASE), 'LuluLemon Lab', model)
            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            for material_info in record.get('details', {}).get('Material and care', []):
                match = re.findall(re.compile(r'All:(.*)|Body:(.*)', re.IGNORECASE), material_info)
                if match:
                    material = match[0][0] or match[0][1]
                    material = re.sub(r'\s+', ' ', material).strip()
                    break

            color = record.get("color", '')
            color = '/'.join(list(dict.fromkeys(color.split('/'))))
            color = color.title()

            transformed_record = {
                "id": record.get('id',''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": record.get('category', ''),
                "title": record.get('title', ''),
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,
                "description": description,

                "price":{
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "color": color,
                "support_level": record.get('Support', ''),
                "activity": record.get('Activity', ''),
                "product_line": record.get('Collections', ''),
                "material": material,
            }

            return transformed_record
        else:
            return None
