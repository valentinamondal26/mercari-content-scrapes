import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace("$", "").strip()

            # model transformations
            model = record.get('title', '')
            model = re.sub(re.compile(
                r'Online Only|lululemon lab|lululemon$', re.IGNORECASE), '', model)
            model = model.split('|', 1)[0]
            model = model.replace('*', '')
            model = re.sub(r'\s+', ' ', model).strip()

            # extract length from details
            length = ''
            for detail in record.get('details', {}).keys():
                match = re.findall(re.compile(
                    r',(.*)length', re.IGNORECASE), detail)
                if match:
                    length = match[0]
                    length = re.sub(r'\s+', ' ', length).strip()
                    break

            # extract material from details
            material = ''
            for material_info in record.get('details', {}).get('Material and care', []):
                match = re.findall(re.compile(
                    r'Body:(.*)', re.IGNORECASE), material_info)
                if match:
                    material = match[0]
                    material = re.sub(r'\s+', ' ', material).strip()
                    break

            # color transformations
            color = record.get("color", '')
            color = color.title()
            color = re.sub(re.compile(
                r'3 Colour|4 colour|Modern stripe|Luminous Stripe|Luminosity Foil Print', re.IGNORECASE), '', color)
            colors = list(set(color.split('/')))
            if len(colors) > 3:
                color = 'Multicolor'
            else:
                color = '/'.join(colors)
            color = re.sub(r'\s+', ' ', color).strip()

            if color == 'Space Dye Ice Grey Alpine White':
                color = 'Space Dye/Ice Grey/Alpine White'
            color = re.sub(re.compile(r"\bhtr\b", re.IGNORECASE), "", color)
            color = " ".join(color.split())

            # product type transformation - remove plural s
            product_type = record.get("Type", "")
            if product_type != "":
                if product_type[-1] == "s":
                    product_type = product_type[:-1]

            # remove producttype from model
            for word in product_type.split():
                model = re.sub(re.compile(r"\b{}\b".format(
                    word), re.IGNORECASE), "", model)

            model = " ".join(model.split())
            ignore_pattern = re.compile(r"Bodysuits*|Sweater", re.IGNORECASE)
            if re.findall(ignore_pattern, product_type) != [] or re.findall(ignore_pattern, model) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "title": record.get('title', ''),
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,
                "description": description,

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "color": color,
                "product_type": product_type,
                "activity": record.get('Activity', ''),
                "size_womens": ','.join(record.get('sizes', [])),
                "length": length,
                "material": material,
            }

            return transformed_record
        else:
            return None
