import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'Jacket', re.IGNORECASE), ' ', model)
            model = model.replace('*', '')
            model = model.split('|', 1)[0]
            if not re.findall(re.compile(r'\dLululemon X\d|\dlululemon lab\d', re.IGNORECASE), model):
                model = re.sub(re.compile(r'Lululemon', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\blululemon lab\b', re.IGNORECASE), 'LuluLemon Lab', model)
            model = re.sub(' +', ' ', model).strip()

            color = record.get("color", '')
            color = '/'.join(list(dict.fromkeys(color.split('/'))))
            color = color.title()

            transformed_record = {
                "id": record.get('id',''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": record.get('category', ''),
                "title": record.get('title', ''),
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,

                "price":{
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "color": color,
                "description": '. '.join(record.get('details', {}).get('Product Features', [])),
                "product_type": record.get('Type', ''),
                "activity": record.get('Activity', ''),
                "warmth": record.get('Warmth Ratings', ''),
                "insulation_type": record.get('Insulation Type', ''),
            }

            return transformed_record
        else:
            return None
