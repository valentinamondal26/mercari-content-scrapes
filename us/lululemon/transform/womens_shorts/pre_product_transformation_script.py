import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace("$", "").strip()

            inseam = record.get('Inseam', '')
            if not inseam:
                match = re.findall(
                    r'\d+(?:.\d+){0,1}"', record.get('title', ''))
                if match:
                    inseam = match[0]

            if not inseam:
                for detail in record.get('details', {}).keys():
                    match = re.findall(re.compile(
                        r'\d+(?:.\d+){0,1}" inseam', re.IGNORECASE), detail)
                    if match:
                        inseam = match[0]
                        inseam = re.sub(re.compile(
                            r'inseam', re.IGNORECASE), '', inseam)
                        inseam = re.sub(r'\s+', ' ', inseam).strip()
                        break

            model = record.get('title', '')
            model = model.split('|', 1)[0]
            model = model.replace('*', '')
            model = re.sub(re.compile(inseam, re.IGNORECASE), '', model)
            model = re.sub(re.compile(
                r"Online Only", re.IGNORECASE), '', model)
            pattern = re.compile(r"Shorts*", re.IGNORECASE)
            if re.findall(pattern, model) != []:
                model = re.sub(pattern, "", model)
                model += " Shorts"
                model = " ".join(model.split())
            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            for material_info in record.get('details', {}).get('Material and care', []):
                match = re.findall(re.compile(
                    r'Body:(.*)', re.IGNORECASE), material_info)
                if match:
                    material = match[0]
                    material = re.sub(r'\s+', ' ', material).strip()
                    break
            colors = record.get("color", "").title().split()

            for ctr, color in enumerate(colors):
                if "/" in color:
                    words = color.split("/")
                    words = sorted(set(words), key=words.index)
                    colors[ctr] = "/".join([word.strip() for word in words])

            slash_values = [cl for cl in colors if "/" in cl]
            for ctr, color in enumerate(colors):
                if "/" not in color:
                    for value in slash_values:
                        if color in value:
                            del colors[ctr]

            color = " ".join(sorted(set(colors), key=colors.index))
            if "Luminosity" in color:
                color = re.sub(re.compile(r"print", re.IGNORECASE), "", color)
                color = " ".join(color.split())
            color = re.sub(re.compile(r"\b(\d+)\s*yr\b",
                                      re.IGNORECASE), r"\1th Year", color)
            color = re.sub(re.compile(r"\b(\w+)\s*\/\s*(\w+)",
                                      re.IGNORECASE), r"\1/\2", color)
            color = re.sub(re.compile(r"Lux Multi Black/Heathered",
                                      re.IGNORECASE), "Heather Lux Multi Black", color)
            color = " ".join(color.split())

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "title": record.get('title', ''),
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,
                "description": description,

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "color": color,
                # "old_color": record.get("color", "").title(),
                "inseam": inseam,
                "activity": record.get('Activity', ''),
                "size_womens": ','.join(record.get('sizes', [])),
                "material": material,
                "rise": record.get("Rise", ''),
            }

            return transformed_record
        else:
            return None
