import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '').replace("$", "").strip()
            model = record.get('title', '')
            model = re.sub(re.compile(r'Online Only|\*', re.IGNORECASE), ' ', model)
            model = model.split('|', 1)[0]
            model = re.sub(' +', ' ', model).strip()

            transformed_record = {
                "id": record.get('id',''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": record.get('category', ''),
                "description": description,
                "title": record.get('title', ''),
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": price
                },

                "model": model,
                "color": record.get("color", ''),
            }

            return transformed_record
        else:
            return None
