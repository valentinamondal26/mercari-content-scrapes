import hashlib
import re
from unicodedata import normalize

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            ner_query = normalize('NFKD', ner_query)
            description = record.get('description', '')
            description = normalize('NFKD', description)

            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            image = record.get('image', '')
            if not (image.startswith('https:') or image.startswith('http:')):
                image = f'https:{image}'

            model = record.get('title', '')
            if not re.findall(r'\bSports Bra\b', model, flags=re.IGNORECASE):
                model = re.sub(r'\bBra\b|\bSports\b', 'Sports Bra', model, flags=re.IGNORECASE)

            model = re.sub('(.*)(Sports Bra)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            color = record.get('color', '').replace('Color', '')
            color = re.sub(r'^Army$', 'Army Green', color, flags=re.IGNORECASE)
            colors = color.split('/')
            if len(colors)==2:
                if re.findall(colors[0], colors[1], flags=re.IGNORECASE):
                    color = colors[1]
                elif re.findall(colors[1], colors[0], flags=re.IGNORECASE):
                    color = colors[0]

            materials = []
            material = record.get('material','') or ' '.join(record.get('material_composition', ''))
            material = normalize('NFKD', material)
            fabric_list = ['Polyester','Nylon','Spandex']
            for fabric in fabric_list:
                if fabric in material:
                    materials.append(fabric)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "image": image,
                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,

                "model": model,
                "color": color,
                "material": '/'.join(materials),
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
