import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            image = record.get('image', '')
            if not (image.startswith('https:') or image.startswith('http:')):
                image = f'https:{image}'

            model = record.get('title', '')
            model = re.sub(r'\bShorts\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            color = record.get('color', '')
            color = re.sub(r'\bColor\b', '', color, flags=re.IGNORECASE)
            color = re.sub(r'\s+', ' ', color).strip()

            material = ''
            material_info = record.get('material', '')
            fabric_meta = ['Polyester', 'Nylon', 'Spandex', 'Lycra']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', fabric_meta))), material_info, flags=re.IGNORECASE)
            if match:
                material = '/'.join(list(dict.fromkeys(list(map(lambda x: x.strip().title(), match)))))


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "image": image,
                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),

                "model": model,
                "color": color,
                "model_sku": record.get('model_sku', ''),
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
