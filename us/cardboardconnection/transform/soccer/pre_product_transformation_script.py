import re
import hashlib


class Mapper:
    
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')
            model = record.get('title', '')

            model = re.sub(r'(^\d+\/\d+\b)(.*)', r'\2 \1', model).strip()
            model = re.sub(r'(^\b[A-Z]+\s+\d+\b|^\b[A-Z]+[0-9\-[A-Z]+\b|\b^[0-9]+[0-9\-[A-Z]+\b|^\b\d+\b)(.*)', r'\2 #\1', model)
            model = re.sub(r'\s+\-\s+|^\-|\-$|\*|\(|\)', ' ', model)
            model = re.sub(r'\(PR=\d+\)', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            capital_words = []#https://mercari.atlassian.net/browse/USCC-642?focusedCommentId=476855
            try:
                capital_words = re.findall(r'[A-Z]{5,}', re.findall(r'(.*)#', model)[0])# On seeing the data most of the words in CAPS are  having 5\
                                                                                        # or more letters and words with less than 4 letters are mostly abbreviation.\
                                                                                        #  so we are converting the captial words having length 5 or more into title case.\
                                                                                        #  Please verify the string length for converting the upper case to title case\
                                                                                        #  before proceeding in future.
            except IndexError:
                model = re.sub(r'\b([A-Z]{5,}\b)', lambda x: x.group().title(), model)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', capital_words))), lambda x: x.group().title(), model)
            special_caps_words = ['REAL', 'CITY']
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', special_caps_words))), lambda x: x.group().title(), model)

            card_number = record.get('card_#', '')
            if card_number:
                model = f'{title} #{card_number}'
            
            set_name = record.get('set_name', '').strip()
            set_name = re.sub(r'\bSet\b', '', set_name, flags=re.IGNORECASE)
            set_type = record.get('set_type', '').strip()
            
            
            product_line = f'{set_name} {set_type}'
            product_line = product_line.strip()
            product_line = re.sub(r'\bChecklists*\b|\s+\–\s+|\,', ' ', product_line, flags=re.IGNORECASE).strip()
            product_line = re.sub(r'\s+', ' ', product_line).strip()

            sports_name = record.get('sports_name', '')

            sports_name = re.sub(r'^\bTopps Soccer\b$', 'Soccer', sports_name, flags=re.IGNORECASE)

            if card_number:
                player_name = record.get('title', '')
            else:
                player_name = re.sub(r'^\b[A-Z]+[0-9\-[A-Z]+\b|\b[0-9]+\/[0-9\-[A-Z]+\b', '', title).strip()
                player_name = re.sub(r'\b^\d+', '', player_name).strip()
                player_name = re.sub(r'^\-', '', player_name).strip()
                player_name = re.sub(r'- (.*)$', '', player_name).strip()
                player_name = re.sub(r'^\-|\-$', '', player_name).strip()

            brand = re.sub(r'\b{sports_name}\b'.format(sports_name=sports_name), '', record['brand'], flags=re.IGNORECASE)
            brand = re.sub(r'\s+', ' ', brand).strip()


            if sports_name != 'Soccer' or not model:
                return None

            if re.findall(r'\d{2}\.\d{2}\.\d{4}', title):
                return None

            transformed_record = {
                'id': record['id'], 
                'item_id': hex_dig,

                'crawl_date': record.get('crawl_date', '').split(', ')[0],
                'category': record.get('category', ''),
                'brand': brand,

                'title': record.get('title', ''),
                'model': model,

                'product_line': product_line,
                'sport': sports_name,
                'player': player_name,
            }
            return transformed_record
        else:
            return None
