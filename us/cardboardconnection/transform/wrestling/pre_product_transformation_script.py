import re
import hashlib
from colour import Color

class Mapper:
    
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')
            model = record.get('title', '')

            if not re.findall(r'(^\b[A-Z]+[0-9\-[A-Z]+\b|\b[0-9]+[0-9\-[A-Z]+\b|\b\d+\b)(.*)', model):
                index = record.get('item_index_in_set_type', '')
                serial_number = record.get('serial_number', '')
                if serial_number:
                    serial_number = serial_number[0]
                    match = re.findall(r'#/\d+', serial_number)
                    if match:
                        value = match[0]
                        value = value.replace('#/', f'#{index}/')
                        model = f'{model} {value}'
            else:
                model = re.sub(r'(^\b[A-Z]+[0-9\-[A-Z]+\b|\b[0-9]+[0-9\/\-[A-Z]+\b|\b\d+\b)(.*)', r'\2 #\1', model)
            model = re.sub(r'\s+\-\s+|^\-|\-$|\*|\'|\"', ' ', model)
            model = re.sub(r'\s+\/\s+', '/', model)
            model = re.sub(r'\s+', ' ', model).strip()


            card_number = record.get('card_#', '')
            if card_number:
                model = f'{title} #{card_number}'
            
            set_name = record.get('set_name', '').strip()
            set_name = re.sub(r'\bSet\b', '', set_name, flags=re.IGNORECASE)
            set_type = record.get('set_type', '')
            
            product_line = f'{set_name} {set_type}'
            product_line = product_line.strip()
            product_line = re.sub(r'\bChecklists*\b|\s+\–\s+|\,', ' ', product_line, flags=re.IGNORECASE).strip()
            product_line = re.sub(r'\s+', ' ', product_line).strip()


            sports_name = record.get('sports_name', '')
            if not sports_name:
                if re.findall(r'\bWrestling\b', set_name, flags=re.IGNORECASE):
                    sports_name = 'Wrestling'

            sports_name = re.sub(r'^\bTopps Wrestling\b$', 'Wrestling', sports_name, flags=re.IGNORECASE)

            if card_number:
                player_name = record.get('title', '')
            else:
                player_name = re.sub(r'^\b[A-Z]+[0-9\-[A-Z]+\b|\b[0-9]+[0-9\-[A-Z]+\b', '', title).strip()
                player_name = re.sub(r'\b^\d+\b', '', player_name)
                player_name = re.sub(r'- (.*)$', '', player_name).strip()
                player_name = re.sub(r'^\-|\-$', '', player_name).strip()
          
            brand = re.sub(r'\b{sports_name}\b'.format(sports_name=sports_name), '', record['brand'], flags=re.IGNORECASE)
            brand = re.sub(r'\s+', ' ', brand).strip()

            if sports_name != 'Wrestling' or not title or not re.findall(r'\w+|\d+', title) or\
                 re.findall(r'\bPacks*\b|\bVersions*\b|\bPlates*\b|\bPARALLELS*\b', title, flags=re.IGNORECASE):
                return None
            
            title_words = re.findall(r'[a-z]+', title, flags=re.IGNORECASE)
            colors_in_title = []
            for word in title_words:
                try:
                    Color(word)
                    colors_in_title.append(word)
                except ValueError:
                    pass
            
            if title_words and len(colors_in_title) == len(title_words):#e.g #/10, Red 1/1 #Sometimes only color will be the only string present in the title
                return None

            transformed_record = {
                'id': record['id'], 
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', ''),
                'category': record.get('category', ''),
                'brand': brand,

                'title': record.get('title', ''),
                'model': model,

                'product_line': product_line,
                'sport': sports_name,
                'player': player_name,
            }
            return transformed_record
        else:
            return None
