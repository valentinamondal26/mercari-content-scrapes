import re
import hashlib


class Mapper:
    
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')
            model = record.get('title', '')

            sports_name = record.get('sports_name', '')
            sports_name = re.sub(r'^\bTopps UFC\b$', 'UFC', sports_name, flags=re.IGNORECASE)

            player_name = re.sub(r'^[A-Z]+[0-9\-[A-Z]+|[0-9]+[0-9\-[A-Z]+', '', title).strip()
            player_name = re.sub(r'\b^\d+\b', '', player_name)
            player_name = re.sub(r'- (.*)$', '', player_name).strip()
            player_name = re.sub(r'-^|\*', '', player_name).strip()

            product_line = record.get('set_name', '').strip()
            product_line = re.sub(r'\bChecklists*\b|\s+\–\s+|\,', ' ', product_line, flags=re.IGNORECASE).strip()
            product_line = re.sub(r'\s+', ' ', product_line).strip()

            match = re.findall(r'^\b18\d{2}\b|^\b19\d{2}\b|^\b20\d{2}\b', product_line)
            product_line_year = ''
            if match:
                product_line_year = match[0]
            player_name_replace_string = f'{player_name} {product_line_year}'

            model = re.sub(r'(^\d+\/\d+\b)(.*)', r'\2 \1', model).strip()
            model = re.sub(r'(^\b[A-Z]+\s+\d+\b|^\b[A-Z]+[0-9\-[A-Z]+\b|\b^[0-9]+[0-9\-[A-Z]+\b|^\b\d+\b)(.*)', r'\2 #\1', model)
            model = re.sub(r'\s+\-\s+|^\-|\-$|\*', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\b{player_name}\b'.format(player_name=player_name), player_name_replace_string, model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            
            brand = re.sub(r'\b{sports_name}\b'.format(sports_name=sports_name), '', record['brand'], flags=re.IGNORECASE)
            brand = re.sub(r'\s+', ' ', brand).strip()

            if sports_name != 'UFC' or not title or title=='*':
                return None
                
            transformed_record = {
                'id': record['id'], 
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', ''),
                'category': record.get('category', ''),
                'brand': brand,

                'title': record.get('title', ''),
                'model': model,

                'product_line': product_line,
                'sport': sports_name,
                'player': player_name,
            }
            return transformed_record
        else:
            return None
            