class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        product_line = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "product_line" == entity['name']:
                product_line = entity["attribute"][0]["id"]

        key_field = model + product_line
        return key_field
