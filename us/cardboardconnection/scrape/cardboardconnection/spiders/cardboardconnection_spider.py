'''
https://mercari.atlassian.net/browse/USCC-640 - Sports Trading Cards & Accessories & Topps - UFC cards
https://mercari.atlassian.net/browse/USCC-641 - Sports Trading Cards & Accessories & Topps - WWE cards
https://mercari.atlassian.net/browse/USCC-642 - Sports Trading Cards & Accessories & Topps - Soccer cards
'''


import scrapy
import datetime
from datetime import datetime

import random
import itertools
import os
from scrapy.exceptions import CloseSpider


class CardboardconnectionSpider(scrapy.Spider):
    """
        spider to crawl items in a provided category and brand from cardboardconnection.com

        Attributes

        category : str
        category to be crawled, Supports
        1) Sports Trading Cards & Accessories

        brand : str
        brand to be crawled, Supports
        1) Topps

        Command e.g:
        scrapy crawl cardboardconnection  -a category='Sports Trading Cards & Accessories' -a brand='Topps UFC'
        scrapy crawl cardboardconnection  -a category='Sports Trading Cards & Accessories' -a brand='Topps Wrestling'
        scrapy crawl cardboardconnection  -a category='Sports Trading Cards & Accessories' -a brand='Topps Soccer'

    """
    name = 'cardboardconnection'

    source = 'cardboardconnection.com'
    blob_name = 'cardboardconnection.txt'
    category = None
    brand = None

    launch_url = None
    filters = None

    base_url='https://www.cardboardconnection.com/'

    url_dict = {
        'Sports Trading Cards & Accessories': {
            'Topps UFC': {
               'url': ['https://www.cardboardconnection.com/brand/topps-cards/topps-ufc']
            },
            'Topps Wrestling':{
                'url': ['https://www.cardboardconnection.com/brand/topps-cards/topps-wrestling', \
                    'https://www.cardboardconnection.com/2010-topps-wwe-wrestling-set-checklists']
            },
            'Topps Soccer': {
                'url': ['https://www.cardboardconnection.com/brand/topps-cards/topps-soccer']
            }
        },
        
    }
    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com/',
        'http://shp-mercari-us-d00002.tp-ns.com/',
        'http://shp-mercari-us-d00003.tp-ns.com/',
        'http://shp-mercari-us-d00004.tp-ns.com/',
        'http://shp-mercari-us-d00005.tp-ns.com/',
    ]

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(CardboardconnectionSpider, self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def start_requests(self):
        for url in self.launch_url:
            yield self.create_request(url=url, callback=self.get_listing_page_items)

    def get_listing_page_items(self, response):
        """
        @url https://www.cardboardconnection.com/brand/topps-cards/topps-ufc
        @meta {"use_proxy": "True"}
        @returns requests 1
        @scrape_values meta.sports_name
        """
        
        sports_name = response.xpath("//h1[@class='archive-title']/text()").get() 
        product_urls = response.xpath("//div[@class='brandproductDescription']/h2/a/@href").getall()
        meta = {'sports_name': sports_name}
        if product_urls:
            for url in product_urls:
                yield self.create_request(url=url, callback=self.crawl_details, meta=meta)
            
            next_page_url = response.xpath("//a[@class='nextpostslink']/@href").get(default='')
            if next_page_url:
                yield self.create_request(url=url, callback=self.get_listing_page_items, meta=meta)
        else:
            #NOTE:https://www.cardboardconnection.com/2010-topps-wwe-wrestling-set-checklists
            #This case will be applicable where only table data will be there. In the above mentioned url, there will be no separate item page for items
            #All items will be present in the form of tables so we will form the url and extract the necessary entities and yield it as item.
            product_tables = response.xpath("//table[contains(@class,'wp-table-reloaded')]") 
            for table in product_tables: 
                set_type = table.xpath("./preceding-sibling::h3[1]/text()").get(default='') or table.xpath("./preceding-sibling::h3[1]/a/text()").get(default='') 
                products = table.xpath(".//tbody/tr") 
                for product in products: 
                    product_name = product.xpath(".//td[2]/text()").get(default='') 
                    prod_id = product.xpath(".//td[1]/text()").get(default='') 
                    item = {
                        'id': f'{response.url}#{product_name}_{prod_id}%20{set_type}',
                        'status_code': str(response.status),
                        'crawl_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'category': self.category, 
                        'brand': self.brand,
                        'title': product_name,
                        'set_type': set_type,
                        'card_#': prod_id,
                        'set_name': response.xpath("//h1[@class='producttabbed-title']/text()").get(default=''),
                        'product_details': response.xpath("//h2[contains(text(), 'Product Details')]/../p/text()").getall(),
                    }  
                    yield item




    def crawl_details(self, response):
        """
        @url https://www.cardboardconnection.com/topps-living-set-uefa-champions-league-cards
        @scrape_values id title product_brand set_name 
        @meta {"use_proxy": "True"}
        """
        set_types = response.xpath("//h3[@class='hot-title']/text()").getall()
        set_name = response.xpath("//h1[@class='producttabbed-title']/text()").get(default='').strip()
        meta  = response.meta
        for set_type in set_types: 
            product_names = []
            #NOTE: Same XPath is written in the except part but only the single quotes and double quotes will vary between try part and except part.
            #Set type  having single quotes are handled in "try" block and set type  having double quotes are handled in "except" block.
            try:
                product_names.append(response.xpath('//h3[text()="'+set_type+'"]/following-sibling::div[@class="ezcol ezcol-one-half  tablechecklist"][1]/text()').getall() \
                    or response.xpath('//h3[text()="'+set_type+'"]/following-sibling::div[2]/text()').getall()) 
                product_names.append(response.xpath('//h3[text()="'+set_type+'"]/following-sibling::div[@class="ezcol ezcol-one-half ezcol-last  tablechecklist"][1]/text()').getall()) 
                serial_number = response.xpath('//h3[text()="'+set_type+'"]/following-sibling::div[contains(text(), "Serial numbered")][1]/text()').getall() \
                    or response.xpath('//h3[text()="'+set_type+'"]/following-sibling::div[contains(text(), "cards")][1]/text()').getall()
                parallel_cards = response.xpath('//h3[text()="'+set_type+'"]/following-sibling::div[contains(text(), "PARALLEL")][1]/text()').getall()
            except ValueError:
                product_names.append(response.xpath("//h3[text()='"+set_type+"']/following-sibling::div[@class='ezcol ezcol-one-half  tablechecklist'][1]/text()").getall() \
                    or response.xpath("//h3[text()='"+set_type+"']/following-sibling::div[2]/text()").getall()) 
                product_names.append(response.xpath("//h3[text()='"+set_type+"']/following-sibling::div[@class='ezcol ezcol-one-half ezcol-last  tablechecklist'][1]/text()").getall()) 
                serial_number = response.xpath("//h3[text()='"+set_type+"']/following-sibling::div[contains(text(), 'Serial numbered')][1]/text()").getall() \
                        or response.xpath("//h3[text()='"+set_type+"']/following-sibling::div[contains(text(), 'cards')][1]/text()").getall()
                parallel_cards = response.xpath("//h3[text()='"+set_type+"']/following-sibling::div[contains(text(), 'PARALLEL')][1]/text()").getall()

            product_names = list(itertools.chain.from_iterable(product_names))
            for product_name,index in zip(product_names, range(1,len(product_names)+1)):
                product_name = product_name.strip()
                url_value = product_name.replace(' ', '-')
                id = f'{response.url}#{url_value}%20{set_type}'
                item = {
                    'id': id,
                    'status_code': str(response.status),
                    'crawl_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'category': self.category, 
                    'brand': self.brand,
                    'title': product_name,
                    'product_brand': response.xpath("//strong[contains(text(),'Brand')]/following-sibling::a/text()").getall(),
                    'set_type': set_type,
                    'set_name': set_name,
                    'serial_number': serial_number,
                    'parallel_cards': parallel_cards,
                    'item_index_in_set_type': index, #some item don't have any alphanumeric or numeric string in the item so we are requested to take the numeric string in the order of the items.\
                                                    # For e.g John cena card is present in the 5th position in particular set type. We will consider number 5 \
                                                    # for john cena.
                    'product_details': response.xpath("//h2[contains(text(), 'Product Details')]/../p/text()").getall(),
                    'hobby_box_price': response.xpath("//td[@class='table-data-price']/strong/a/text()").get(default=''),
                    'sports_name': meta.get('sports_name', ''),
                    'release_date': response.xpath("//strong[contains(text(), 'Release Date:')]/following-sibling::text()").get(default='') ,
                    'product_configuration': response.xpath("//strong[contains(text(), 'Product Configuration:')]/following-sibling::text()").get(default=''),
                }
                yield item
        

    def create_request(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta, headers=headers)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({'use_cache': True})
        return request