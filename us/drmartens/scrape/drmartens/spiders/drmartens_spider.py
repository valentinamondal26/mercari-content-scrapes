'''
https://mercari.atlassian.net/browse/USDE-723 - Dr. Martens Women's Boots
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json
import re
from html import unescape
from parsel import Selector


class DrmartensSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from drmartens.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Boots

    brand : str
        brand to be crawled, Supports
        1) Dr. Martens

    Command e.g:
    scrapy crawl drmartens -a category="Women's Boots" -a brand='Dr. Martens'
    """

    name = 'drmartens'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'drmartens.com'
    blob_name = 'drmartens.txt'

    url_dict = {
        "Women's Boots": {
            'Dr. Martens': {
                'url': 'https://www.drmartens.com/us/en/c/womens-boots',
                'filters': [
                    'collection', #'Color',
                ],
            }
        }
    }

    base_url = 'https://www.drmartens.com/us/en'
    listing_page_result_url = '{launch_url}/results?q={query}&sort=relevance&page={page}'
    listing_page_facets_url = '{launch_url}/facets'

    filter_list = []

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(DrmartensSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.launch_url = 'https://www.drmartens.com/us/en/c/womens-boots'
        self.filter_list = ['collection']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_launch_url)


    def parse_launch_url(self, response):
            # start the listing page navigation
            url = self.listing_page_result_url.format(launch_url=response.url,
                page=0, query='')
            print(f'url:{url}')
            yield self.create_request(url=url, callback=self.parse)

            #start the filters based listing page navigation
            if self.filter_list:
                yield self.create_request(
                    url=self.listing_page_facets_url.format(launch_url=response.url),
                    callback=self.parse_facets
                )


    def crawlDetail(self, response):
        """
        @url https://www.drmartens.com/us/en/p/13512006
        @meta {"use_proxy":"True"}
        @scrape_values product_color product_material breadcrumb title price color model_sku description materials_care
        """

        descriptions = []
        materials_care = []

        product_detail_script = response.xpath('//script[@type="text/javascript" and contains(text(), "prodDetail")]/text()').extract_first(default='')
        match = re.findall(r'"prodDetail":(.*?\})', product_detail_script.replace('\n', ''))
        if match:
            descriptions = Selector(unescape(json.loads(match[0]).get('content', ''))).xpath('//text()[normalize-space(.)]').extract()
        match = re.findall(r'"howMade":(.*?\})', product_detail_script.replace('\n', ''))
        if match:
            materials_care = Selector(unescape(json.loads(match[0]).get('content', ''))).xpath('//text()[normalize-space(.)]').extract()


        product_color = ''
        product_material = ''

        product_info_script = response.xpath('//script[contains(text(), "dataLayer.push(")]/text()').extract_first()
        match = re.findall(r'dataLayer.push\((.*)\);', product_info_script)
        if match:
            product_info_data = json.loads(match[0])
            products = product_info_data.get('ecommerce', {}).get('detail', {}).get('products', [])
            if products:
                product_color = products[0].get('product_colour', '')
                product_material = products[0].get('product_material', '')


        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'product_color': product_color,
            'product_material': product_material,
            # 'product_price': product_info_data['ecommerce']['detail']['products'][0]['price'],

            'breadcrumb': '|'.join(response.xpath('//ol[@class="breadcrumb"]//text()[normalize-space(.)]').extract()),
            'title': response.xpath(u'normalize-space(//h1[@class="product-main-info__name"]/text())').extract_first(default=''),
            'price': response.xpath(u'normalize-space(//span[@class="current-price"]/text())').extract_first(default=''),
            'color': response.xpath('//div[@id="js-variant-label"]/h2/span/text()').extract_first(default=''),
            'model_sku': response.xpath('//div[@id="js-variant-label"]/label/text()').extract_first(default=''),

            'description': descriptions,
            'materials_care': materials_care,
        }


    def parse_facets(self, response):
        """
        @url https://www.drmartens.com/us/en/c/womens-boots/facets
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        data = json.loads(response.text)
        for facet in data.get('facets', []):
            if facet.get('name', '') in self.filter_list:
                for value in facet.get('values', []):
                    # print(value['name'], value['count'], value['query']['url'], value['query']['query']['value'])
                    request = self.create_request(url=self.listing_page_result_url.format(
                            launch_url=self.launch_url,
                            query=value.get('query', {}).get('query', {}).get('value', ''),
                            page=0
                        ), callback=self.parse)
                    request.meta['extra_item_info'] = {
                        facet.get('name', ''): value.get('name', ''),
                    }
                    request.meta['query_info'] = value.get('query', {}).get('query', {}).get('value', ''),
                    print(request.url)
                    yield request


    def parse(self, response):
        ###TODO: check the contracts url..it might be wrong also..
        """
        @url https://www.drmartens.com/us/en/c/womens-boots/results?q=&sort=relevance&page=0
        @returns requests 1
        """

        query = response.meta.get('query_info', '')
        extra_item_info = response.meta.get('extra_item_info', {})

        data = json.loads(response.text)
        # print('results:', data.get('results', []))
        for result in data.get('results', []):
            if result.get('variantOptions', []):
                for variation in result.get('variantOptions', []):
                    url = variation.get('url', '')
                    print(variation)
                    if url:
                        url = self.base_url + url
                        if not self.extra_item_infos.get(url, None):
                            self.extra_item_infos[url] = {}

                        self.extra_item_infos.get(url).update(extra_item_info)

                        request = self.create_request(url=url, callback=self.crawlDetail)
                        yield request
            else:
                url = result.get('url', '')
                print(result)
                if url:
                    url = self.base_url + url
                    if not self.extra_item_infos.get(url, None):
                        self.extra_item_infos[url] = {}

                    self.extra_item_infos.get(url).update(extra_item_info)

                    request = self.create_request(url=url, callback=self.crawlDetail)
                    yield request

        current_page = data.get('pagination', {}).get('currentPage', 0)
        total_pages = data.get('pagination', {}).get('numberOfPages', 0)
        # print(f"total_results: {j['pagination']['totalNumberOfResults']}")
        if current_page and total_pages and current_page < total_pages:
            current_page = current_page + 1
            request = self.create_request(url=self.listing_page_result_url.format(
                    launch_url=self.launch_url,
                    query=query,
                    page=current_page
                ), callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            request.meta['query_info'] = query
            yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
