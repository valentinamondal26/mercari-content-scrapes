import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # model transformation
            model = record.get('title', '')
            model = re.sub(re.compile(
                r"\bWomen's\b|\bboots\b", re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()
            d = {
                'Ii': 'II',
                'Dm\'S': 'DM\'s',
                'Hi': 'HI',
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)

            description = ' '.join(record.get('description', []))
            # materials_care_info = record.get('materials_care', [])
            # materials_care = dict(zip(materials_care_info[::2], materials_care_info[1::2]))
            # material = materials_care.get('Material', '')

            # color transformation
            color = record.get('product_color', '') or record.get('color', '')
            color = color.title()
            color = color.split("+")
            color = "/".join(sorted(set(color), key=color.index))
            color = color.replace("-", " ")
            color = re.sub(re.compile(r"\bDms\b", re.IGNORECASE), "", color)
            color = " ".join(color.split())
            color = re.sub(re.compile(
                r"\bRed Stewart\/Black Watch\b", re.IGNORECASE), "Red/Black", color)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": description,
                "model": model,
                "color": color,
                # "product_color": record.get('product_color', ''),
                'collection': record.get('collection', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
