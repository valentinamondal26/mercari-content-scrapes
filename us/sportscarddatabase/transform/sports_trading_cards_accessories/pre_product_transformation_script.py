import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            id = id.replace('.com//', '.com/')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = ''
            price_info = record.get('price_info', '')
            if price_info:
                price = price_info[0].get('', '')

            image = record.get('image', '')

            title = record.get('title', '')
            title = re.sub(r'\*', '', title, flags=re.IGNORECASE)
            title = re.sub(r'\(.*\)', '', title, flags=re.IGNORECASE)
            title = re.sub(r'\(|\)', '', title, flags=re.IGNORECASE)

            product_lines = record.get('specs', {}).get('Card Set', [])
            players = record.get('specs', {}).get('Player(s)', [])
            if not players:
                match = re.findall(r'#\d+\s*(.*)$', title)
                if match:
                    players = list(map(lambda x: x.strip(), match[0].split('/')))


            model = title
            try:
                model = re.sub(r'\bTopps\b|^\s*>', '', model, flags=re.IGNORECASE)
                # model = re.sub(r'\(.*\)', '', model, flags=re.IGNORECASE)
                if players:
                    for player in players:
                        model = re.sub(r'(.*)(\b'+player+r'\b)(.*)', r'\2 \1 \3', model, flags=re.IGNORECASE)
                if len(players) > 1:
                    model = '/'.join(players) + ' ' + re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', players))), '', model, flags=re.IGNORECASE)
                model = re.sub(r'/\s*$|/', '', model)
                model = re.sub(r'-\s*$|-', '', model)
                if not re.findall(r'#\d+\s*$', model):
                    model = re.sub(r'(.*)((18|19|20)\d{2})(.*)(#\d+\s*)(.*)$', r'\1 \2 \4 \6 \5', model)
            except Exception as e:
                print('Error', e, model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "image": 'http://www.sportscarddatabase.com/' + image if image else '',

                "model": model,
                "sport": record.get('sports', ''),
                "product_line": ', '.join(product_lines),
                'player': ', '.join(players),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
