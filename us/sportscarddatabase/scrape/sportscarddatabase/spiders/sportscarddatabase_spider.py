'''
https://mercari.atlassian.net/browse/USCC-636 - Topps Baseball Sports Trading Cards & Accessories
https://mercari.atlassian.net/browse/USCC-637 - Topps Basketball Sports Trading Cards & Accessories
https://mercari.atlassian.net/browse/USCC-638 - Topps Football Sports Trading Cards & Accessories
https://mercari.atlassian.net/browse/USCC-639 - Topps Hockey Sports Trading Cards & Accessories
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import pandas as pd
import os


class SportscarddatabaseSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from sportscarddatabase.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Sports Trading Cards & Accessories
    
    brand : str
        brand to be crawled, Supports
        1) Topps

    sports : str
        brand to be crawled, Supports
        1) Baseball
        2) Basketball
        3) Football
        4) Hockey

    Command e.g:
    scrapy crawl sportscarddatabase -a category='Sports Trading Cards & Accessories' -a brand='Topps' -a sport='Baseball'
    scrapy crawl sportscarddatabase -a category='Sports Trading Cards & Accessories' -a brand='Topps' -a sport='Basketball'
    scrapy crawl sportscarddatabase -a category='Sports Trading Cards & Accessories' -a brand='Topps' -a sport='Football'
    scrapy crawl sportscarddatabase -a category='Sports Trading Cards & Accessories' -a brand='Topps' -a sport='Hockey'
    """

    name = 'sportscarddatabase'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
        'http://shp-mercari-us-d00016.tp-ns.com:80',
        'http://shp-mercari-us-d00017.tp-ns.com:80',
        'http://shp-mercari-us-d00018.tp-ns.com:80',
        'http://shp-mercari-us-d00019.tp-ns.com:80',
        'http://shp-mercari-us-d00020.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'sportscarddatabase.com'
    blob_name = 'sportscarddatabase.txt'

    base_url = 'http://www.sportscarddatabase.com'

    sports = ''

    url_dict = {
        'Sports Trading Cards & Accessories': {
            'Topps': {
                # 'url': [
                #     'http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=1',
                #     'http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=2',
                #     'http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=3',
                #     'http://www.sportscarddatabase.com/CardSearch.aspx?sp=4&k=topps',
                # ],
                'Baseball': {
                    'url': 'http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=1',
                },
                'Basketball': {
                    'url': 'http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=2',
                },
                'Football': {
                    'url': 'http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=3',
                },
                'Hockey': {
                    'url': 'http://www.sportscarddatabase.com/CardSearch.aspx?sp=4&k=topps',
                }
            }
        }
    }

    def __init__(self, category=None, brand=None, sport=None, *args, **kwargs):
        super(SportscarddatabaseSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            # self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand
        self.sports = sport

        if self.sports:
            self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get(self.sports, {}).get('url', '')
        else:
            self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])

        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    # def contracts_mock_function(self):
    #     self.sports = 'Baseball'

    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, list):
                urls = self.launch_url
            elif isinstance(self.launch_url, str):
                urls = [self.launch_url]

            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url http://www.sportscarddatabase.com/CardItem.aspx?id=8838029&view=2
        @scrape_values id title image breadcrumb price_info specs
        @meta {"use_proxy":"True"}
        """

        specs = {}
        for div in response.xpath('//table[@id="ctl00_MainContent_tblContent"]//td[@id="ctl00_MainContent_cellMiddle"]/div'):
            key = div.xpath('./text()').extract_first()
            value = []
            for child in div.xpath('./following-sibling::*'):
                if child.xpath('name()').get() == 'a':
                    value.append(child.xpath('./self::a/text()').get())
                elif child.xpath('name()').get() == 'div':
                    break
            specs[key] = value

        price_info = {}
        buy_it_now = response.xpath('//span[@id="ctl00_MainContent_CtlBuyNow_lblBuyNowCardSearch"]/table').get()
        if buy_it_now:
            df = pd.read_html(buy_it_now)[0]
            df.columns=df.iloc[1]
            price_info = df[2:].to_dict(orient='record')

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//h2/text()').extract_first(),
            'breadcrumb': ' | '.join(response.xpath('//h2//text()').extract()),
            'image': response.xpath('//table[@id="ctl00_MainContent_tblContent"]//td[@id="ctl00_MainContent_cellLeft"]/img/@src').extract_first(),

            'specs': specs,
            'price_info': price_info,
            'sports': self.sports,
        }


    def parse(self, response):
        """
        @url http://www.sportscarddatabase.com/CardSearch.aspx?p=1&k=topps&sp=1
        @returns requests 1 51
        @meta {"use_proxy":"True"}
        """

        for product in response.xpath('//table[@id="ctl00_MainContent_dgResults_dgResults"]//tr'):

            link = product.xpath('./td/a/@href').extract_first()
            if link:
                link = self.base_url + '/' + link
                request = self.create_request(url=link, callback=self.crawlDetail)
                yield request

        next_link = response.xpath('//span[@class="LinkDiv"]/a[contains(text(), "Next")]/@href').extract_first()
        if next_link:
            next_link = self.base_url + (next_link if next_link.startswith('/') else '/'+next_link)
            yield self.create_request(url=next_link, callback=self.parse)


    def create_request(self, url, callback, meta=None, use_cache=True):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool: 
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache": use_cache})
        return request

