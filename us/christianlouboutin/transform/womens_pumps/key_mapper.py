class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        heel_height = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "heel_height" == entity['name']:
                heel_height = entity["attribute"][0]["id"]

        key_field = model + heel_height
        return key_field
