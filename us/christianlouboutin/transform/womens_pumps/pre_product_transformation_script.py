import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            material = record.get('specs', {}).get('Material', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'Pump|\b' + material + r'\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "ner_query": ner_query,

                "description": record.get('description', ''),
                "model": model,
                "color": record.get('specs', {}).get('Color', ''),
                "material": material,
                'product_type': record.get('breadcrumb', '').split('|')[-1].strip(),
                'sku': record.get('specs', {}).get('Reference', ''),
                'collection': record.get('specs', {}).get('Collection', ''),
                'heel_height': record.get('specs', {}).get('Heel height', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
