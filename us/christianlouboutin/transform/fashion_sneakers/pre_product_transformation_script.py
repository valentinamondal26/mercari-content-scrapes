import hashlib
import re
from  titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = record.get('specs', {}).get('Color', '')
            color = re.sub(re.compile(r'\bVersion\b|\b/sv\b', re.IGNORECASE), '', color).strip()
            color = re.sub(re.compile(r'\bMulti\b|\bMulticolored\b', re.IGNORECASE), 'Multicolor', color).strip()
            color = re.sub(re.compile(r'\bbk\b', re.IGNORECASE), 'Black', color).strip()
            color = re.sub(re.compile(r'\bLoubi\b', re.IGNORECASE), 'Red', color).strip()
            color = '/'.join(dict.fromkeys(color.split('/')))
            color = color.title()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bpreorder\b|\bMen\'s\b|\bMen\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            material = record.get('specs', {}).get('Material', '')
            material = re.sub(re.compile(r'\bPatent Cl\b', re.IGNORECASE), '', material)
            material = re.sub(r'\bCl Patent\b', 'Patent Leather', material, flags=re.IGNORECASE)
            material = '/'.join(list(dict.fromkeys(material.split('/'))))
            material = material.title()

            product_type = record.get('breadcrumb', '').split('|')[-1].strip()
            if re.findall(re.compile('All Shoes|BLACK TIE', re.IGNORECASE), product_type):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "title": record.get('title', ''),
                "ner_query": ner_query,
                "description": record.get('description', ''),
                'image': record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                'product_type': product_type,
                'sku': record.get('specs', {}).get('Reference', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
