'''
https://mercari.atlassian.net/browse/USDE-645 - Christian Louboutin Fashion Sneakers
https://mercari.atlassian.net/browse/USDE-717 - Christian Louboutin Women's Pumps
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import urllib.parse as urlparse
from urllib.parse import parse_qs

class ChristianlouboutinSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from christianlouboutin.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Fashion Sneakers
        2) Women's Pumps

    brand : str
        brand to be crawled, Supports
        1) Christian Louboutin

    Command e.g:
    scrapy crawl christianlouboutin -a category="Fashion Sneakers" -a brand='Christian Louboutin'
    scrapy crawl christianlouboutin -a category="Women's Pumps" -a brand='Christian Louboutin'
    """

    name = 'christianlouboutin'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'christianlouboutin.com'
    blob_name = 'christianlouboutin.txt'

    url_dict = {
        'Fashion Sneakers': {
            'Christian Louboutin': {
                'url': 'http://us.christianlouboutin.com/us_en/shop-online-3/mens-1/mens-shoes.html',
            }
        },
        "Women's Pumps": {
            'Christian Louboutin': {
                'url': 'http://us.christianlouboutin.com/us_en/shop-online-3/women-1/women/pumps.html',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ChristianlouboutinSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.launch_url = 'http://us.christianlouboutin.com/us_en/shop-online-3/mens-1/mens-shoes.html'


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url + '?p=1', callback=self.parse)


    def crawlDetail(self, response):
        """
        @url http://us.christianlouboutin.com/us_en/shop/men/ivy-nanou-flat.html
        @meta {"use_proxy":"True"}
        @scrape_values title breadcrumb sizes specs.Reference specs.Color specs.Material specs.Collection description price
        """

        specs = {}
        for spec in response.xpath(u'//p[@class="content"]/text()').extract():
            spec_parts = spec.split(':')
            if len(spec_parts) > 1:
                specs[spec_parts[0].strip()] = spec_parts[1].strip()

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'description': response.xpath(u'normalize-space(//div[@class="description"]/text())').extract_first(default=''),
            'price': response.xpath('//span[@class="price"]/text()').extract_first(default=''),
            'title': response.xpath(u'normalize-space(//span[@class="title title-cap"]/text())').extract_first(default=''),
            'breadcrumb': ' | '.join(response.xpath('//div[@class="menu menu-left menu-list has-sublevel"]/ul/li/a/text()').extract()),
            'sizes': response.xpath('//div[@class="size"]/ul/li[@class="attribute-option"]/a').xpath('normalize-space(./text())').extract(),
            'specs': specs,
        }


    def parse(self, response):
        """
        @url http://us.christianlouboutin.com/us_en/shop-online-3/mens-1/mens-shoes.html?p=1
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        page_no = parse_qs(urlparse.urlparse(response.url).query)['p'][0]
        is_valid_page_data = False
        for product in response.xpath('//div[@class="product"]'):
            data_page_id = product.xpath('./@data-page-id').extract_first()
            if is_valid_page_data or data_page_id == page_no:
                is_valid_page_data = True
                yield self.create_request(url=product.xpath('./a/@href').extract_first(), callback=self.crawlDetail)

        if page_no and is_valid_page_data:
            yield self.create_request(url=self.launch_url + '?p={}'.format(int(page_no)+1), callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

