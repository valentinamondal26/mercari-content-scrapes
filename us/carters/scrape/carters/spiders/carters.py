'''
https://mercari.atlassian.net/browse/USCC-296 - Carter's Girls' One-Pieces (Newborn-5T)
https://mercari.atlassian.net/browse/USDE-1672 - Carter's Boys' One-Pieces (Newborn-5T)
'''

import random
from collections import OrderedDict
import os
import re
from datetime import datetime
import time
import json
from parsel import Selector

import scrapy
import hashlib
import requests
from scrapy import Request
from scrapy.exceptions import CloseSpider
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class CartersSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from carters.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Girls' One-Pieces (Newborn-5T)
        2) Boys' One-Pieces (Newborn-5T)

    brand : str
        category to be crawled, Supports
        1) Carter's

    Command e.g:
    scrapy crawl carters -a category="Girls' One-Pieces (Newborn-5T)" -a brand="Carter's"
    scrapy crawl carters -a category="Boys' One-Pieces (Newborn-5T)" -a brand="Carter's"

    """

    name = 'carters'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None
    launch_url = None
    driver = None
    source = 'carters.com'
    handle_httpstatus_list = [301, 302, 307]
    color_api = 'https://www.carters.com/{url}?attrType=color&ajax=true&quantity=1'
    filters = None
    item_count = 0
    load_more = 'https://www.carters.com/on/demandware.store/Sites-Carters-Site/default/Search-UpdateGrid?cgid=carters-baby-girl-one-pieces&prefn1=websiteBrand&prefv1=Carter%27s&hitsCount=176&pboostCount=0&start={start}&sz={end}'
    blob_name = 'carters.txt'
    base_url = 'https://www.carters.com'

    url_dict = {
        "Girls' One-Pieces (Newborn-5T)": {
            "Carter's": {
                'url': 'https://www.carters.com/carters-baby-girl-one-pieces?prefn1=websiteBrand&prefv1=Carter%27s&selectedRefId=websiteBrand',
                "filters": ['Sleeve Length', 'Pick your print!'],
            }
        },
        "Boys' One-Pieces (Newborn-5T)": {
            "Carter's": {
                'url': 'https://www.carters.com/carters-baby-boy-one-pieces?navID=header',
                "filters": ['Style', 'Color', 'Sleeve Length'],
            }
        },
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(CartersSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get("filters", '')
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Sleeve Length', 'Pick your print!']
        self.launch_url = 'https://www.carters.com/carters-baby-girl-one-pieces?prefn1=websiteBrand&prefv1=Carter%27s&selectedRefId=websiteBrand'


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url,
                                     callback=self.parse_filters,
                                     headers={'Referer': self.launch_url})


    def parse_filters(self, response):
        """
        @url https://www.carters.com/carters-baby-girl-one-pieces?prefn1=websiteBrand&prefv1=Carter%27s&selectedRefId=websiteBrand
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        if self.filters:
            for filter in self.filters:
                filter_urls = response.xpath(
                    '//div[@id="refinementsBar"]//div[contains(text(),"{}")]/following-sibling::div//li/button/@data-href'.format(filter)).getall()
                filter_urls = [
                    self.base_url+link if "https" not in link else link for link in filter_urls]
                labels = response.xpath(
                    '//div[@id="refinementsBar"]//div[contains(text(),"{}")]/following-sibling::div//li/button/@aria-label'.format(filter)).getall()

                for url in filter_urls:
                    index = filter_urls.index(url)
                    key = labels[index].replace(
                        'Refine by {}:'.format(filter), '').strip()
                    meta = {}
                    meta = meta.fromkeys([filter], key)
                    meta.update({'filter_name': filter})
                    print(key, " : ", url)
                    yield self.createRequest(url=url,
                                             callback=self.parse,
                                             meta=meta)

        yield self.createRequest(url=self.launch_url,
                                 callback=self.parse,
                                 headers={'Referer': self.launch_url})

    def parse(self, response):
        """
        @url https://www.carters.com/carters-baby-girl-one-pieces?prefn1=websiteBrand&prefv1=Carter%27s&selectedRefId=websiteBrand
        @meta {"use_proxy":"True"}
        @returns requests 36
        """
        ###Number of requests mentioned in contracts is 36 because 35 products per page and 1 Next Page url.

        all_links = response.xpath(
            '//a[@class="product-tile-information"]/@href').getall()
        all_links = [self.base_url +
                     link if "https" not in link else link for link in all_links]

        for link in all_links:
            yield self.createRequest(url=link,
                                     callback=self.parse_product,
                                     meta=response.meta)

        next_page_url = response.xpath(
            '//button[@data-cmp-id="loadMore"]/@data-url').get(default=None)
        if next_page_url:
            next_page_url = [
                self.base_url+link if "https" not in link else link for link in [next_page_url]][0]
            if next_page_url != '':
                yield self.createRequest(url=next_page_url,
                                         callback=self.parse,
                                         meta=response.meta)


    def parse_product(self, response):
        """
        @url https://www.carters.com/carters-baby-girl-dresses/V_1H512510.html
        @scrape_values image breadcrumb title price description model_sku material
        @meta {"use_proxy":"True"}
        """

        item = {}
        item['id'] = response.url
        item['crawl_date'] = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item['status_code'] = str(response.status)
        item['system_category'] = self.category
        item['brand'] = self.brand
        item['image'] = response.xpath(
            '//a[@class="js-click-zoom product-main-image-item-in"]/img/@data-yo-src').get(default='')
        item['breadcrumb'] = "|".join(response.xpath(
            '//nav[@aria-label="breadcrumbs"]//li/a/text()').getall()).replace('\n', '')
        script_data = response.xpath(
            '//script[contains(text(),"offers")]//text()').get(default='')
        try:
            script_json = json.loads(script_data)
        except:
            script_json = {}

        item['title'] = script_json.get('name', '').split('|')[0].strip()
        item['price'] = script_json.get('offers', {}).get('price', '')
        description = response.xpath(
            '//div[@class="product-details-info js-product-details-info collapse"]//text()').getall()
        description = [d.strip()
                       for d in description if re.findall(r"^[\s\n]+$", d) == []]
        item['description'] = description
        item['model_sku'] = script_json.get('sku', '')
        fabric = response.xpath(
            '//p[contains(text(),"Fabric & Care:")]/following-sibling::ul//li/text()').getall()
        material = ''
        for fab in fabric:
            if "%" in fab:
                material = re.sub(r"\d+\%", '', fab).strip()

        item['material'] = material

        meta = {}
        filter_name = response.meta.get('filter_name', '')
        if filter_name != '':
            meta = meta.fromkeys([filter_name], response.meta[filter_name])
        item.update(meta)

        colors = response.xpath(
            '//ul[@class="color-swatch-in"]//a/@data-attribute-value').getall()
        urls = response.xpath(
            '//ul[@class="color-swatch-in"]//a/@href').getall()
        for ctr, color in enumerate(colors):
            url = self.color_api.format(url=urls[ctr])
            # r = requests.get(
            #     url, headers={"referer": response.url, "User-Agent": "PostmanRuntime/7.24.1"})
            # products = r.json().get("productResult", {}).get("product", {})
            # item['id'] = url.replace(
                # "?attrType=color&ajax=true&quantity=1", "").strip()
            # item['image'] = products.get("images", {}).get(
            #     "hi-res", [""])[0].get("url", "")
            # item['title'] = products.get("productName", "")
            # item["price"] = products.get("price", {}).get(
            #     "sales", {}).get("decimalPrice", "")
            # item["model_sku"] = products.get("id", "")
            # item['color'] = color
            # yield item
            yield self.createRequest(
                url=url,
                callback=self.parse_color_variants,
                meta={'item': item},
                headers={"referer": response.url, "User-Agent": "PostmanRuntime/7.24.1"},
            )

        if colors == []:
            yield item


    def parse_color_variants(self, response):
        item = response.meta.get('item', {})
        j = json.loads(response.text)
        products = j.get("productResult", {}).get("product", {})
        item['image'] = products.get("images", {}).get(
            "hi-res", [""])[0].get("url", "")
        item['title'] = products.get("productName", "")
        item["price"] = products.get("price", {}).get(
            "sales", {}).get("decimalPrice", "")
        item['id'] = response.url.replace(
                "?attrType=color&ajax=true&quantity=1", "").strip()
        item["msrp"] = (products.get("price", {}).get("list", {}) or {}).get("decimalPrice", "")
        item["model_sku"] = products.get("id", "")
        item['color'] = products.get("color", "")
        yield item


    def createRequest(self, url=None, callback=None, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key, value in headers.items():
                request.headers[key] = value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request
