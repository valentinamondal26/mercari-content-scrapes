import re

import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            description = ', '.join(record.get('description', '')).strip(",")
            ner_query = description.strip(',')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title')

            # extract product_line from model
            style_meta = ['Overall Set', 'Jumpsuit', 'Sleep & Play', 'Rompers',
                          'Jumpsuits', 'Overall Sets', 'Casual Dresses',
                          'Sleep Bags & Gowns', 'Romper', 'Casual Dress',
                          'Sleep Bags & Gown']
            product_line = ''
            for style in style_meta:
                if re.findall(re.compile(r"{}".format(style),
                                         re.IGNORECASE), model) != []:
                    product_line = style
                    break

            if product_line == '':
                if re.findall(re.compile(r"Sleepe*r*\s*Gowns*|sleepe*r*\s*bags*",
                                         re.IGNORECASE), model) != []:
                    product_line = 'Sleep Bags & Gown'

            # product_line renaming
            style_change_meta = ["Rompers", "Jumpsuits", "Sleep Bags & Gowns",
                                 "Overall Sets", "Buntings", "Gift Sets",
                                 "Casual Dresses", "Sleeper Gowns",
                                 "Sleep Gowns", "Sleep Bags", "Sleeper Bags"]
            for word in style_change_meta:
                product_line = re.sub(re.compile(r"\b{}\b".format(
                    word), re.IGNORECASE), word[:-1], product_line)
                model = re.sub(re.compile(r"\b{}\b".format(
                    word), re.IGNORECASE), word[:-1], model)

            # color = list(set(re.findall(r"\w+", record.get('color', ''))))
            # color = ', '.join(color)
            color = record.get("color", "")
            color = re.sub(re.compile(
                r"\bMulti\b", re.IGNORECASE), "Multicolor", color)

            # material transformation
            material = re.sub(r"\b[\w\W\d\s]+\:+", '',
                              record.get('material', ''))
            material = re.sub(re.compile(
                r"jersey", re.IGNORECASE), '', material)
            material = ' '.join(material.split(", "))
            for word in re.findall(r"\w+", material):
                model = re.sub(re.compile(r"{}".format(word),
                                          re.IGNORECASE), '', model)

            material_remove = ["Soles Contain ", "New Lawn",
                               "Rib", "Ring", "Organic", "2x2", "Interlock"]
            for word in material_remove:
                material = re.sub(re.compile(r"\b{}\b".format(word), re.IGNORECASE),
                                  "", material)
            material = ' '.join(material.split())

            model = ' '.join(model.split())

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": "Girls' One-Pieces (Newborn-5T)",
                "breadcrumb": record.get('breadcrumb', ''),
                "description": description.replace('\n', ''),
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": ner_query,
                "msrp": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "sleeve_length": record.get('Sleeve Length', ''),
                "color": color,
                "product_line": product_line,
                "pattern": record.get('Pick your print!', ''),
                "model_sku": record.get("model_sku", ''),
                "material": material.title()
            }

            return transformed_record
        else:
            return None
