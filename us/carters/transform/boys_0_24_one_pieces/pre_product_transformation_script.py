import hashlib
import re
from colour import Color
from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:

            description = ', '.join(record.get('description', '')).strip(",")
            ner_query = description.strip(',')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title')

            # extract product_type from model
            style_meta = ['Overall Set', 'Jumpsuit', 'Sleep & Play', 'Rompers',
                          'Jumpsuits', 'Overall Sets', 'Casual Dresses',
                          'Sleep Bags & Gowns', 'Romper', 'Casual Dress',
                          'Sleep Bags & Gown']
            product_type = ''
            for style in style_meta:
                if re.findall(re.compile(r"{}".format(style),
                              re.IGNORECASE), model):
                    product_type = style
                    break

            if product_type == '':
                if re.findall(
                        re.compile(r"Sleepe*r*\s*Gowns*|sleepe*r*\s*bags*",
                                   re.IGNORECASE), model):
                    product_type = 'Sleep Bags & Gown'

            # product_type renaming and remove product line from model
            style_change_meta = ["Rompers", "Jumpsuits", "Sleep Bags & Gowns",
                                 "Overall Sets", "Buntings", "Gift Sets",
                                 "Casual Dresses", "Sleeper Gowns",
                                 "Sleep Gowns", "Sleep Bags", "Sleeper Bags"]
            for word in style_change_meta:
                product_type = re.sub(re.compile(r"\b{}\b".format(
                        word), re.IGNORECASE), word[:-1], product_type)
                model = re.sub(re.compile(r"\b{}\b".format(
                        word), re.IGNORECASE), word[:-1], model)

            # color transformations
            # color = list(set(re.findall(r"\w+", record.get('color', ''))))
            # color = ', '.join(color)
            color = record.get("color", "")
            if not color:
                color = record.get('Color', '')
            color = re.sub(re.compile(
                    r"\bMulti\b", re.IGNORECASE), "Multicolor", color)
            color = "/".join(color.split(", "))
            if len(color.split("/")) > 2:
                primary_colors = [i for i in color.split("/")
                                  if self.check_color(i)]
                to_exlude = ['Turquoise', 'Navy', '']
                primary_colors = [primary_color
                                  for primary_color in primary_colors
                                  if primary_color not in to_exlude]
                color = primary_colors[0] + "/Multicolor"

            if "Multicolor" in color:
                color = re.sub(re.compile(r"(.*)(\/*)multicolor(\/*)(.*)",
                               re.IGNORECASE), r"\2\1\4\3Multicolor", color)

            # material transformation
            material = re.sub(r"\b[\w\W\d\s]+\:+", '',
                              record.get('material', ''))
            material = re.sub(re.compile(
                    r"jersey", re.IGNORECASE), '', material)
            material = ' '.join(material.split(", "))

            # remove material from model
            for word in re.findall(r"\w+", material):
                model = re.sub(re.compile(r"{}".format(word),
                               re.IGNORECASE), '', model)

            material_remove = ["Soles Contain ", "New Lawn",
                               "Rib", "Ring", "Organic", "2x2", "Interlock"]
            for word in material_remove:
                material = re.sub(
                        re.compile(r"\b{}\b".format(word), re.IGNORECASE),
                        "", material)
            material = ' '.join(material.split())
            material = re.sub(r';', '/', material)
            material = re.sub(r'\s*/\s*','/',material)
            model = re.sub(r'TM|100%', '',model) ####Need to remove TM(upper case string alone) even if part of another word
            model = re.sub(r'\bCertified\b', '', model, flags=re.IGNORECASE)

            model = ' '.join(model.split())
            model = titlecase(model)
            # ignore the overlapping items
            if hex_dig in [
                'c7c9e6aa72dc0add3ba555b9bab8b178c436191b',
                '8c3d6dd6be3b936d4d53badf61992c2e5bb19d6b',
                '6551d2178fa0c0a5f40a940cd3ce3a4c57c378f1',
                ]:
                return None

            transformed_record = {
                    "id": record['id'],
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawl_date', ''),
                    "status_code": record.get('status_code', ''),

                    "category": record.get('system_category', ''),
                    "brand": record.get('brand', ''),

                    "breadcrumb": record.get('breadcrumb', ''),
                    "description": description.replace('\n', ''),
                    "image": record.get('image', ''),
                    "ner_query": ner_query,
                    "title": record.get('title', ''),

                    "model": model,
                    "color": color,
                    "sleeve_length": record.get('Sleeve Length', ''),
                    "product_type": product_type,
                    "material": material.title(),
                    "msrp": {
                            "currency_code": "USD",
                            "amount": record.get("msrp", "").strip('$').replace(
                                    ',', '')
                    },
                    "price": {
                            "currency_code": "USD",
                            "amount": record.get("price", "").strip(
                                    '$').replace(',', '')
                    },
            }

            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except Exception:
            return False
