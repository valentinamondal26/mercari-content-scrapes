'''
https://mercari.atlassian.net/browse/USCC-730 - RIP-IT Women's Baseball & Softball Clothing
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import copy


class RipitSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from ripit.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Baseball & Softball Clothing
    
    brand : str
        brand to be crawled, Supports
        1) RIP-IT

    Command e.g:
    scrapy crawl ripit -a category="Women's Baseball & Softball Clothing" -a brand="RIP-IT"
    """

    name = 'ripit'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'ripit.com'
    blob_name = 'ripit.txt'

    base_url = 'https://www.ripit.com'

    url_dict = {
        "Women's Baseball & Softball Clothing": {
            "RIP-IT": {
                'url': 'https://www.ripit.com/collections/all-products-filtered?page=1&sort_by=title-ascending',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(RipitSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.ripit.com/collections/all-products-filtered/products/3-inch-period-protection-compression-shorts-pro
        @meta {"use_proxy":"True"}
        @scrape_values id image title price colors types sizes description features color
        """

        colors = response.xpath('//div[@id="Color"]//input[@type="radio"]/@value').getall() \
                or response.xpath('//select[@name="options[Color]"]/option/@value').getall()

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'title': response.xpath('//h1[contains(@class, "product-single__title")]/text()').get(),
            'price': response.xpath(u'normalize-space(//span[@class="price-item price-item--regular"]/text())').get() \
                or response.xpath('//div[@class="product__price fs-body-base"]/span[@data-price]/text()').get(),
            'colors': colors,
            'types': response.xpath('//div[@id="Type"]//input[@type="radio"]/@value').getall() \
                or response.xpath('//select[@name="options[Type]"]/option/@value').getall(),
            'sizes': response.xpath('//div[@id="Size"]//input[@type="radio"]/@value').getall() \
                or response.xpath('//select[@name="options[Size]"]/option/@value').getall(),
            'description': response.xpath('//div[@id="col-right"]/p/text()').get(),
            'features': response.xpath('//div[@id="col-right"]/ul[@class="prod-features"]/li//text()[normalize-space(.)]').getall(),
        }

        variants = {}
        for option in response.xpath('//select[@name="id"]/option'):
            key = option.xpath(u'normalize-space(./text())').get().split('/')[0]
            key = re.sub(r'- Sold out', '', key, flags=re.IGNORECASE).strip()
            value = option.xpath('./@value').get()
            if not variants.get(key, '') and key in colors:
                variants[key] = value

        if variants:
            for color, variant_id in variants.items():
                variant = copy.deepcopy(item)
                variant['color'] = color
                variant['id'] = response.url + '?variant=' + variant_id
                yield variant
        else:
            yield item


    def parse(self, response):
        """
        @url https://www.ripit.com/collections/all-products-filtered?page=1&sort_by=title-ascending
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//ul[@class="grid grid--uniform grid--view-items"]/li') \
            or response.selector.xpath('//div[contains(@class, "boost-pfs-filter-products")]/div[contains(@class, "boost-pfs-filter-product-item-grid")]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//ul[@class="list--inline pagination"]/li//span[contains(text(), "Next page")]/parent::a/@href').get() \
            or response.xpath('//link[@rel="next"]/@href').get()
        if next_page_link:
            next_page_link = self.base_url + next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
