import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '') or ''
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')

            color = record.get('color', '')

            features = ', '.join(record.get('features', []))

            def wrap(words):
                return r'|'.join(list(map(lambda x: r'\b'+x+r'\b', words)))

            product_type = ''
            if re.findall(wrap(['pants', 'shorts', 'belt']), model, flags=re.IGNORECASE):
                product_type = 'clothing'
            elif re.findall(wrap(['headband', 'glove', 'helmet', 'mask']), model, flags=re.IGNORECASE):
                product_type = 'gear'
            elif re.findall(wrap(['backpack']), model, flags=re.IGNORECASE):
                product_type = 'bag'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description + ' ' + features,
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "product_type": product_type,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
