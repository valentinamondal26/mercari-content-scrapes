import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            specs = record.get('specs', {})

            brand = record.get('brand', '')

            color = ', '.join(specs.get('Color', ''))

            height = ', '.join(specs.get('Height', ''))
            length = ', '.join(specs.get('Length', ''))
            width = ', '.join(specs.get('Width', ''))

            model = record.get('title', '')
            keywords = []
            keywords.append(brand)
            if color:
                keywords.extend(color.split('/'))

            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), ' ', model)
            model = re.sub(r'\bL\b|\bXL\b|#', '', model)
            model = re.sub(r'(\bx\s)*(\.*\d+\.*-*\s*\d*\/*\d*\s(and|x|to)\s)*\.*\d+\.*-*\s*\d*\/*\d*\s*(in\.|ft\.)\s*(L\b|W\b|H\b|\/lbs\.|Dia\.)*(\s*x\s)*(?!\s*Blades)', '', model)

            # model = re.sub(r"(x*\s*\d+\.*\d*\/*)\s*(\-*)\s*(\d*\s*\/*\s*\d*\s*(?:in\.|ft\.))", r"\1\2\3", model)
            # # model = re.sub(r"(\d+\-) \d+/8 in.")
            # matches = re.findall(r"\bx*(\s*\d+\.*\d*\s*\/\d+\s*(?:and|x|to)\s*\d+\.*\d*\/\d+\s*(?:in\.|ft\.))|\bx*(\s*\d+\.*\d*\/*\-*\s*\d*\s*\/*\s*\d*\s*(?:in\.|ft\.))", model)
            # matches = list(set([mma.strip() for ma in matches for mma in ma if mma!='']))
            # for m in matches:
            #     position = model.rfind(m)
            #     model = model[0:position]+model[position+len(m)+1:]
            if re.findall(re.compile(r"Bandsaws", re.IGNORECASE), model):
                if model.count("Blade") > 1:
                    model = re.sub(re.compile(r"with Blades*", re.IGNORECASE), "", model)
            # model = re.sub(r"\bL\b|\bW\b|\bH\b|\bx\b|\bDia\.*\b", "", model)
            model = re.sub(re.compile(r"\bDia\.|\bL\b|\bW\b|\bH\b|\/lbs\.", re.IGNORECASE), "", model)
            pattern = re.compile(r'\b(\d+)\s*(pc\.|pc\b|pk\.|pk\b|pair\b)', re.IGNORECASE)
            match = re.findall(pattern, model)
            if match:
                if int(match[0][0]) <= 1:
                    model = re.sub(pattern, '', model)
            

            model = re.sub(r'\s\/\s|^\/\s|\s\/$|^\s*#\s|\s\.\s*$', '', model)
            model = " ".join([m[0].upper() + m[1:] for m in model.split()])
            model = re.sub(r'\s+', ' ', model).strip()
            

            d = {
                'SHOCKWAVE': 'Shockwave',
                'SPEED FEED': 'Speed Feed',
                'SWITCHBLADE': 'Switch Blade',
                'THUNDERBOLT': 'Thunderbolt',
                'TRUEVIEW': 'Trueview',
                'INKZALL':'Inkzall',
                'Bandsaws with Blades': 'Bandsaws',
                'DB': 'dB',
                'Ga': 'Ga',
                'Mm': 'mm',
                'Oz': 'oz',
                'Amps': 'amps',
                'Hp': 'hp',
                'Rpm': 'rpm',
                'And': 'and',
                'With': 'with',
                'Or': 'or'
                
            }
            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)
            model = re.sub(re.compile(r"JAM\-FREE", re.IGNORECASE), "Jam Free", model)
            model = re.sub(re.compile(r"SAWZALL", re.IGNORECASE), "Sawzall", model)

            model = re.sub(re.compile(r"pc\.", re.IGNORECASE), "Piece", model)

            model = re.sub(re.compile(r"(\d+)(\-*\s*pk\.*)", re.IGNORECASE), r"\1-Pack", model)
            model = re.sub(re.compile(r"(\d+)(\-*\s*pair\.*)", re.IGNORECASE), r"\1-Pack", model)

            model = re.sub(re.compile(r"(.*)\s(\d+-Pack)(.*)", re.IGNORECASE), r"\1\3 \2", model)
            model = re.sub(re.compile(r"^(\d+\s*-Pack)(.*)", re.IGNORECASE), r"\2 \1", model)
            model = re.sub(re.compile(r"(.*)\s(\d+\s*Piece)(.*)", re.IGNORECASE), r"\1\3 \2", model)
            model = re.sub(re.compile(r"^(\d+\s*Piece)(.*)", re.IGNORECASE), r"\2 \1", model)
            model = re.sub(r'\( or Scissor\)', 'or Scissor', model, flags=re.IGNORECASE) #NOTE:https://mercari.atlassian.net/browse/USCC-236?focusedCommentId=415930
            # Only scissor whitespace transformation is done. Special instruction for removing () is not done since this source items does not have any 
            # example items to apply the given transformation comments to check. In case any items comes with ()  in future because of re-crawl,
            #  Make sure to apply the transformation given in the comments
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": brand,

                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "description": record.get('description', ''),
                'sku': record.get('mpn', '').strip(),
                "price": {
                    "currencyCode": "USD",
                    "amount": price,
                },

                'color': color,
                'weight': ', '.join(specs.get('Weight', '')).replace('-', ' '),
                'height': height.replace('-', ' '),
                'length': length.replace('-', ' '),
                'width': width.replace('-', ' '),

                'product_family': record.get('Sub Brand', ''),
                'material': ', '.join(specs.get('Material', '')),
                'battery_type': ', '.join(specs.get('Battery Type', '')),
                'corded_cordless': ', '.join(specs.get('Corded or Cordless', '')),
                'charger_included': ', '.join(specs.get('Charger Included', '')),
                'number_of_tools': ', '.join(specs.get('Number of Tools', '')),
            }

            return transformed_record
        else:
            return None
