import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            brand = record.get('brand', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'\d+ volt\b|\d+ rpm\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\b'+brand+r'\b', re.IGNORECASE), '', model)
            model = re.sub(r',$', '.', model.strip())
            model = re.sub(r'\s+', ' ', model).strip()

            product_type = record.get('category_info', '').title()
            product_type =  product_type.replace("-"," ")

            specs = record.get('specs', {})

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": brand,
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": record.get('description', ''),
                "model": model,
                'model_sku': record.get('mpn', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
                'product_type': product_type,

                'weight': ', '.join(specs.get('Weight', '')),
                'length': ', '.join(specs.get('Length', '')),
                'width': ', '.join(specs.get('Width', '')),
                'color': ', '.join(specs.get('Color', '')),
                'number_of_speeds': ', '.join(specs.get('Speeds', '')),
                'max_output_power': ', '.join(specs.get('Power', '')),
                'battery_voltage': ', '.join(specs.get('Volts', '')),
                'maximum_speedrpm': ', '.join(specs.get('Maximum Speed', '')),
                'battery_type': ', '.join(specs.get('Battery Type', '')),
                'corded_cordless': ', '.join(specs.get('Corded or Cordless', '')),
                'cord_length': ', '.join(specs.get('Cord length', '')),
                'material': ', '.join(specs.get('Material', '')),
                'use': ', '.join(specs.get('Usage', '')),
                'magnetic': ', '.join(specs.get('Magnetic', '')),
                'water_resistant': ', '.join(specs.get('Water Resistant', '')),
                'lock_mechanism': ', '.join(specs.get('Lockable', '')),
                'frame_color': ', '.join(specs.get('Frame Color', '')),
                'uva_protection': ', '.join(specs.get('UV protection', '')),
                'lens_color': ', '.join(specs.get('Lens Color', '')),
            }

            return transformed_record
        else:
            return None
