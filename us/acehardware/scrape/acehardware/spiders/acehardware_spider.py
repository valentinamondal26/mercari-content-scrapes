'''
https://mercari.atlassian.net/browse/USDE-656 - DeWALT Tools & Workshop Equipment
https://mercari.atlassian.net/browse/USDE-718 - Milwaukee Tools & Workshop Equipment
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider

import os
import json


class AcehardwareSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from acehardware.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Tools & Workshop Equipment
    
    brand : str
        brand to be crawled, Supports
        1) DeWALT
        2) Milwaukee

    Command e.g:
    scrapy crawl acehardware -a category='Tools & Workshop Equipment' -a brand='DeWALT'
    scrapy crawl acehardware -a category='Tools & Workshop Equipment' -a brand='Milwaukee'
    """

    name = 'acehardware'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'acehardware.com'
    blob_name = 'acehardware.txt'

    url_dict = {
        'Tools & Workshop Equipment': {
            'DeWALT': {
                'url': 'https://www.acehardware.com/departments/tools/dewalt',
            },
            'Milwaukee': {
                'url': 'https://www.acehardware.com/departments/tools/milwaukee?facetValueFilter=tenant~A00413%3amilwaukee',
            }
        },
    }

    base_url = 'https://www.acehardware.com/'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AcehardwareSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand},url:{url}".format(category=self.category,brand=self.brand,url=self.launch_url))


    def contracts_mock_function(self):
        self.launch_url = "https://www.acehardware.com/departments/tools/dewalt"


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):

        # breadcrumb = '|'.join(response.xpath('//div[@class="mz-breadcrumbs  "]/a[@class="mz-breadcrumb-link"]/text()').extract())
        # title = response.xpath('//h1[@itemprop="name"]/text()').extract_first()
        # sku = response.xpath('//dd[@itemprop="sku"]/text()').extract_first()
        # mpn = response.xpath(u'normalize-space(//dd[@itemprop="mpn"]/text())').extract_first()
        # price = response.xpath(u'normalize-space(//span[contains(@class, "mz-price")]/text())').extract_first()
        # description = response.xpath(u'normalize-space(//div[@id="productDetailsContainer"]/div/text())').extract_first()
        # product_details = response.xpath('//div[@id="productDetailsContainer"]/ul/li/text()').extract()
        """
        @url https://www.acehardware.com/departments/tools/power-tools/circular-saws/2369643
        @meta  {"use_proxy":"True"}
        @scrape_values title description short_description sku price msrp upc mpn measurements specs category_info
        """
        product_info = json.loads(response.xpath('//script[@id="data-mz-preload-product"]/text()').extract_first())

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': product_info.get('content', {}).get('productName', ''),
            'description': product_info.get('content', {}).get('productFullDescription', ''),
            'short_description': product_info.get('content', {}).get('productShortDescription', ''),
            'url': product_info.get('url', ''),
            'sku': product_info.get('productCode', ''),
            'product_type': product_info.get('productType', ''),
            'price': str(product_info.get('price', {}).get('price', '')),
            'msrp': str(product_info.get('price', {}).get('msrp', '')),
            'upc': product_info.get('upc', ''),
            'mpn': product_info.get('mfgPartNumber', ''),
            'measurements': product_info.get('measurements', ''),
            'specs': {prop.get('attributeDetail', {}).get('name', ''): [value.get('value', '') for value in prop.get('values', [])] for prop in product_info.get('properties', [])},
            'category_info': json.loads(response.xpath('//script[contains(text(), "categorySlug")]/text()').extract_first(default='')).get('categorySlug', ''),
        }


    def parse(self, response):
        """
        @url https://www.acehardware.com/departments/tools/dewalt
        @meta  {"use_proxy":"True"}
        @returns requests 31
        """
        
        #response.xpath('//ul[@class="mz-productlist-list mz-l-tiles"]/li[@class="col-md-4 col-xs-6 mz-productlist-item"]//a'):
        #above xpath gives redundant product urls(60 product urls).so request count also gets increased for scrapy contracts.totally 30 products in the page and 1 next page url .
        for product in response.xpath("//div[@class='mz-productlisting-info']/a"):

            id = product.xpath('./@href').extract_first()

            if id:
                request = self.create_request(url=id, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('//a[@class="mz-pagenumbers-next"]/@href').extract_first()
        print(f'==>nextLink: {nextLink}')
        if nextLink:
            nextLink = self.launch_url.split('?')[0] + nextLink
            yield self.create_request(url=nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)

        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({"use_cache":True})
        return request

