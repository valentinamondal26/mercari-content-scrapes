# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class EbayItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    crawlDate = scrapy.Field()
    id = scrapy.Field()
    name = scrapy.Field()
    rating = scrapy.Field()
    logo = scrapy.Field()
    review = scrapy.Field()
    synonyms = scrapy.Field()
    sameAs = scrapy.Field()
