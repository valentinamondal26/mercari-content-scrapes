# -*- coding: utf-8 -*-
import scrapy
import random
import datetime
from ebay.items import EbayItem

class EbaySpiderSpider(scrapy.Spider):
    name = 'ebay'
    
    start_url = 'https://www.ebay.com/n/all-brands'

    def __init__(self):
        self.declare_xpath()

    def declare_xpath(self):

        self.allBrandsXpath =".//ul[contains(@class,'itemcols')]/li/a"

    def start_requests(self):
        
        request = scrapy.Request(url = self.start_url,callback=self.parse)
        request.meta['proxy'] = self.getProxy()
        yield request

    def parse(self, response):

        for brand in response.xpath(self.allBrandsXpath):
            item = EbayItem()
            href = brand.xpath("./@href").extract_first()
            name = brand.xpath("./text()").extract_first()

            item['crawlDate'] = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
            item['id'] = href
            item['name'] = name
            item['rating'] = ""
            item['logo'] = ""
            item['review'] = []
            item['synonyms'] = []
            item['sameAs'] = ""

            yield item

        




    proxy_list = [
            'http://shp-mercari-us-d00001.tp-ns.com/', 
            'http://shp-mercari-us-d00002.tp-ns.com/', 
            'http://shp-mercari-us-d00003.tp-ns.com/',
            'http://shp-mercari-us-d00004.tp-ns.com/', 
            'http://shp-mercari-us-d00005.tp-ns.com/',
            'http://shp-mercari-us-d00006.tp-ns.com/', 
            'http://shp-mercari-us-d00007.tp-ns.com/', 
            'http://shp-mercari-us-d00008.tp-ns.com/',
            'http://shp-mercari-us-d00009.tp-ns.com/', 
            'http://shp-mercari-us-d00010.tp-ns.com/',
            'http://shp-mercari-us-d00011.tp-ns.com/', 
            # 'http://shp-mercari-us-d00012.tp-ns.com/', 
            'http://shp-mercari-us-d00013.tp-ns.com/',
            'http://shp-mercari-us-d00014.tp-ns.com/', 
            'http://shp-mercari-us-d00015.tp-ns.com/',
            'http://shp-mercari-us-d00016.tp-ns.com/', 
            'http://shp-mercari-us-d00017.tp-ns.com/', 
            'http://shp-mercari-us-d00018.tp-ns.com/',
            'http://shp-mercari-us-d00019.tp-ns.com/', 
            # 'http://shp-mercari-us-d00020.tp-ns.com/',
        ]

    def getProxy(self):
        random_proxy = random.choice(self.proxy_list)
        return random_proxy
