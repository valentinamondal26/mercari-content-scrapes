def seperate_seller_details(filename):
    import pandas as pd
    with open(filename, 'r') as f:
        df = pd.read_json(f, dtype=False, lines=True)
        #print(df.info())

        # scrape data
        df.loc[~pd.isnull(df['title'])].dropna(axis='columns').to_json('bags_scrape.jl', orient='records', lines=True)
        # seller data
        df.loc[pd.isnull(df['title'])].dropna(axis='columns').to_json('seller_details.jl', orient='records', lines=True)

def merge_seller_details_and_seller_items(seller_details_path, seller_items_path, output_path):
    import pandas as pd
    import json
    with open(seller_details_path, 'r') as f, open(seller_items_path, 'r') as f1:
        print('Fetching seller details')
        df_seller = pd.read_json(f, dtype=False, lines=True)
        
        df_seller['itemsForSale'] = None
        print('Processing seller items')
        i=0 
        for line in f1:
            i = i + 1
            print(".{}".format(i), end ='')
            d = json.loads(line)
            seller = d.get('seller', '')
            
            def lambda_json_str_to_list(l, **args):
                data=args['data']
                
                if not l:
                    l = list()
                l.append(data)
                return l

            df_seller.loc[df_seller['seller']==seller, 'itemsForSale'] = df_seller.loc[df_seller['seller']==seller, 'itemsForSale'].apply(lambda_json_str_to_list, data=d, axis=1)
        df_seller.to_json(output_path, orient='records', lines=True)
        print('completed')


if __name__ == "__main__":
    #seperate_seller_details('ebay_seller_items_output-2019-09-28_19-35-52.json')

    path = '/home/salman/Desktop/debug/ebay/seller_items/2019-09-28-19_35_52/'
    # seller_items_path = path + 'seller_items_count_10.jl'
    seller_items_path = path + 'ebay_seller_items_output-2019-09-29_07-57-54.json'
    seller_details_path = path + 'seller_details.jl'
    output_path = path + 'items_for_sale.jl'
    merge_seller_details_and_seller_items(seller_details_path, seller_items_path, output_path)

