# -*- coding: utf-8 -*-
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import json
import itertools
from common.selenium_middleware.http import SeleniumRequest
from urllib.parse import urlparse, parse_qs, urlencode


# 2021-02-18 20:43:50 [scrapy.core.downloader.handlers.http11] WARNING: Got data loss in https://www.ebay.com/b/Collectible-Card-Games-Accessories/2536/bn_1852210?rt=nc&_pgn=157. If you want to process broken responses set the setting DOWNLOAD_FAIL_ON_DATALOSS = False -- This message won't be shown in further requests

class EbayTradingCardsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from ebay.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Sports Trading Cards
        2) Non-Sports Trading Cards
        3) Collectible Card Games
    
    brand : str
        brand to be crawled, Supports
        1) All

    Command e.g:
    scrapy crawl ebay_trading_cards -a category='Sports Trading Cards' -a brand='All'
    scrapy crawl ebay_trading_cards -a category='Non-Sports Trading Cards' -a brand='All'
    scrapy crawl ebay_trading_cards -a category='Collectible Card Games' -a brand='All'
    """

    name = 'ebay_trading_cards'
    
    category = 'Collectible Card Games'
    brand = 'All'
    source = 'ebay.com'
    blob_name = 'ebay.txt'

    base_url = 'https://www.ebay.com'

    filters = []

    BASE_FILE_PATH = ''

    url_dict = {
        'Sports Trading Cards': {
            'All': {
                'url': 'https://www.ebay.com/b/Sports-Trading-Cards-Accessories/212/bn_1859819',
                'filters': ['Year', 'Team', 'Condition']
            },
            'Detail': {
                'url': BASE_FILE_PATH + 'out/sports_trading_cards/sports_trading_cards.jl',
            }
        },
        'Non-Sports Trading Cards': {
            'All': {
                'url': 'https://www.ebay.com/b/Non-Sport-Trading-Cards-Accessories/182982/bn_1860331',
                # 'url': 'https://www.ebay.com/b/Non-Sport-Trading-Card-Singles/183050/bn_2313580',
                'filters': ['Year Manufactured', 'Year Published', 'Rarity', 'Manufacturer', 'Brand', 'Character', 'Type'],
            },
            'Detail': {
                'url': BASE_FILE_PATH + 'out/non_sports_trading_cards/non_sports_trading_cards.jl',
            }
        },
        'Collectible Card Games': {
            'All': {
                # Failed url list
                # 'url': [
                #   'https://www.ebay.com/b/Collectible-Card-Games-Accessories/2536/bn_1852210?rt=nc&_pgn=111',
                #   'https://www.ebay.com/b/Collectible-Card-Games-Accessories/2536/bn_1852210?rt=nc&_pgn=117',
                # ]
                'url': 'https://www.ebay.com/b/Collectible-Card-Games-Accessories/2536/bn_1852210',
                # 'url': 'https://www.ebay.com/b/Sealed-Collectible-Card-Game-Boxes/261044/bn_7117580260',
                'filters': ['Character', 'Year Manufactured', 'Rarity', 'Manufacturer', 'Brand', 'Year Published', ]
            },
            'Detail': {
                'url': BASE_FILE_PATH + 'out/collectible_card_games/collectible_card_games.jl',
            }
        }
    }

    proxy_pool = [
            'http://shp-mercari-us-d00001.tp-ns.com/', 
            'http://shp-mercari-us-d00002.tp-ns.com/', 
            'http://shp-mercari-us-d00003.tp-ns.com/',
            'http://shp-mercari-us-d00004.tp-ns.com/', 
            'http://shp-mercari-us-d00005.tp-ns.com/',
            'http://shp-mercari-us-d00006.tp-ns.com/', 
            'http://shp-mercari-us-d00007.tp-ns.com/', 
            'http://shp-mercari-us-d00008.tp-ns.com/',
            'http://shp-mercari-us-d00009.tp-ns.com/', 
            'http://shp-mercari-us-d00010.tp-ns.com/',
            'http://shp-mercari-us-d00011.tp-ns.com/', 
            'http://shp-mercari-us-d00012.tp-ns.com/',
            'http://shp-mercari-us-d00013.tp-ns.com/',
            'http://shp-mercari-us-d00014.tp-ns.com/', 
            'http://shp-mercari-us-d00015.tp-ns.com/',
            'http://shp-mercari-us-d00016.tp-ns.com/', 
            'http://shp-mercari-us-d00017.tp-ns.com/', 
            'http://shp-mercari-us-d00018.tp-ns.com/',
            'http://shp-mercari-us-d00019.tp-ns.com/', 
            'http://shp-mercari-us-d00020.tp-ns.com/',
        ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(EbayTradingCardsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.round_robin = itertools.cycle(self.proxy_pool)
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            if self.brand == 'Detail':
                with open(self.launch_url, 'r+') as f: 
                    for line in f.readlines(): 
                        url = json.loads(line).get('id', '')
                        if url:
                            yield self.create_request(url=url, callback=self.parse_detail)
            else:
                # yield self.create_request(url=self.launch_url, callback=self.parse, use_cache=False)
                yield self.create_request(url=self.launch_url, callback=self.parse_sub_categories, use_cache=False)


    def parse_sub_categories(self, response):
        for link in response.xpath('//div[@class="dialog__cell"]/section[@class="b-module b-list b-categorynavigations b-display--landscape"]/ul/li/strong[@class="bold"]/../following-sibling::li/a/@href').extract():
            yield self.create_request(url=link, callback=self.parse_filters)
            break


    def get_filter_refinements(self, url, pageci, filters={}):
        import requests

        query_params = {
            "_fsrp":"0",
            # "_aspectname":f"aspect-{filter_name}",
            "_sacat":"183454",
            "rt":"nc",
            "modules":"SEARCH_REFINEMENTS_MODEL_V2:fa",
            "pageci":pageci,
            "no_encode_refine_params":"1"
        }
        query_params.update(filters)

        headers = {
            'if-none-match': "W/\"1a6eea-UftERTQ5+aoatnWdge61oxlMeA4\"",
            'sec-fetch-dest': "empty",
            'sec-fetch-mode': "cors",
            'sec-fetch-site': "same-origin",
            'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36",
            'x-requested-with': "XMLHttpRequest",
            }

        # response = requests.request("GET", url, headers=headers, params=querystring)

        # return response.json()
        import urllib
        return self.create_request(url=url+'?'+urllib.parse.urlencode(query_params), headers=headers, dont_filter=True, callback=self.parse_filter_entries,
            # body=urllib.parse.urlencode(querystring),
            # body=json.dumps(querystring),
        )


    def parse_filters(self, response):
        # filters = ['Character', 'Year Manufactured', 'Rarity', 'Manufacturer', 'Brand', 'Year Published', ]
        # filters = response.xpath('//div[@class="b-carousel__header  sameline"]/div[@class="b-carousel__title"]/h2[@class="b-title "]/text()').extract()
        # filters = list(map(lambda x: x.replace('Shop by', '').strip(), filters))
        # print(filters)

        data = response.xpath('//div[@id="leftnav"]//li[@class="x-refine__main__list x-refine__main__list--aspect"]/div/h3[@class="x-refine__item"]/@data-track').extract_first(default='') or \
            response.xpath('//div[@id="leftnav"]//li[contains(@class, "x-refine__main__list")]/div/h3[@class="x-refine__item"]/@data-track').extract_first(default='') or \
            response.xpath('//nav[@class="b-breadcrumb"]/ol/li/a/@data-track').extract_first(default='')
        print(data)
        pageci = ''
        if data:
            pageci = json.loads(data)['eventProperty']['pageci']
        else:
            print('data field is empty', response.request.meta['proxy'])
        print(response.url, ', pageci:', pageci)
        if pageci:
            # response_refinements = self.get_filter_refinements(url=response.url, pageci=pageci) 
            yield self.get_filter_refinements(url=response.url, pageci=pageci)
        else:
            print(f'pageci is Empty for {response.url}')


    def parse_filter_entries(self, response):
        response_refinements = json.loads(response.text)
        group = list(filter(lambda group: group.get('fieldId', '') == 'aspectlist', response_refinements['group'])) 
        #print(len(group), len(group[0].get('entries', []))) 
        entry_names = [group_entries.get('label', {}).get('textSpans', [{}])[0].get('text', '') for group_entries in group[0].get('entries', [])] 
        print(entry_names) 
        filters = response.meta.get('filters', []) or self.filters 
        #list(filter(lambda f: f in entry_names, filters)) 
        facets = list(filter(lambda group: group.get('label', {}).get('textSpans', [{}])[0].get('text', '') in filters, group[0]['entries'])) 

        f={}
        for facet in facets: 
            f[facet.get('label', {}).get('textSpans', [{}])[0].get('text', '')] = [(entry.get('label', {}).get('textSpans', [{}])[0].get('text', ''), entry.get('label', {}).get('textSpans', [{}])[-1].get('text', '')) for entry in facet['entries']]

        a=list(filter(lambda f: f in filters, f.keys()))
        print(a) 
        b=list(map(lambda f: filters.index(f), a)) 
        ordered_filter = [x for _,x in sorted(zip(b,a))]        

        def get_facets(l, f, threshold=10000): 
            entries = [] 
            facet = l[0] 
            for e in f.get(facet, []): 
                #print(e) 
                if e[0] == 'Not Specified':
                    e = ('!', e[1])
                count = e[1].replace('(', '').replace(')', '').replace(',', '').strip() 
                #print(e, count) 
                if int(count) > threshold and len(l) > 1: 
                    #print(e) 
                    for sub in  get_facets(l[1:], f, threshold): 
                        entries.append(f'{facet}={e[0]}&{sub}') 
                else: 
                    entries.append(f'{facet}={e[0]}') 
            return entries 

        base_url = response.url.split('?')[0]
        for query_string in get_facets(ordered_filter, f):
            url = f'{base_url}?{query_string}'
            print(f'filtered_url: {url}')
            parts = urlparse(url)
            query_dict = parse_qs(parts.query)
            meta = dict(zip(list(query_dict.keys()), list(map(lambda x: ''.join(x), query_dict.values()))))
            # meta.update(query_dict)
            meta['filter_name'] = list(query_dict.keys())
            yield self.create_request(url=url, callback=self.parse, meta=meta)


    # def parse_filter_entries(self, response):
    #     response_refinements = json.loads(response.text)
    #     for group in response_refinements['group']: 
    #         if group.get('fieldId', '') == 'aspectlist': 
    #             for group_entries in group.get('entries', []): 
    #                 entry_name = group_entries.get('label', {}).get('textSpans', [{}])[0].get('text', '')
    #                 if entry_name in self.filters:
    #                     entries = [entries.get('label', {}).get('textSpans', [{}])[0].get('text', '') for entries in group_entries.get('entries', [])]
    #                     print(entry_name, entries) 
    #                     for entry in entries:
    #                         if entry == 'Not Specified':
    #                             entry = '!'
    #                         url = response.url.split('?')[0] + f'?{entry_name}={entry}'
    #                         print(f'filtered_url: {url}')
    #                         yield self.create_request(url=url, callback=self.parse, meta={entry_name: entry, 'filter_name': entry_name})
    #                     return

    def parse(self, response):
        match = re.findall(r'(\d+)-(\d+) of (\d+) Results', response.xpath('//h2[@class="srp-controls__count-heading"]/text()').extract_first(default='').replace(',', ''))
        if match:
            current_start_offset, current_end_offset, total_count = match[0]
            if current_end_offset > total_count:
                print('Item has already reached the total number of items ', current_start_offset, current_end_offset, total_count)
                # return

        links = []
        for product in response.xpath('//li[contains(@class, "s-item") or contains(@class, "sresult gvresult ")]'):
            link = product.xpath('.//a[@class="s-item__link"]/@href').extract_first(default='') or ''
            if link:
                links.append(link)
                # yield self.create_request(url=link, callback=self.parse_detail)
                item = {
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': str(response.status),
                    'category': self.category,
                    'brand': self.brand,
                    'id': link,
                    'page_link': response.url,

                    'title': product.xpath('.//h3[@class="s-item__title"]/text()').extract_first(), 
                    'price': product.xpath('.//span[@class="s-item__price"]//text()').extract_first(), 
                    'shipping_price': product.xpath('.//span[@class="s-item__shipping s-item__logisticsCost"]/text()').extract_first(), 
                    'image': product.xpath('.//img[@class="s-item__image-img"]/@src').extract_first(), 
                }
                filter_name = response.meta.get('filter_name', '')
                if filter_name:
                    if isinstance(filter_name, str):
                        item[filter_name] = response.meta.get(filter_name, '')
                    elif isinstance(filter_name, list):
                        for name in filter_name:
                            item[name] = response.meta.get(name, '')
                yield item

        
        next_page = response.xpath('//a[@rel="next"]/@href').extract_first(default='') or response.xpath('//div[@class="b-pagination"]/nav/@data-next').extract_first(default='') or ''
        print(f'next_page_url: {next_page}, products: {len(links)}, current_url: {response.url}')

        # error_message = response.xpath('//div[@class="pr_error_status pr_error_status--hide"]/div[@class="s-error"]/div[@class="srp-message srp-message--isLarge page-notice page-notice--priority"]/p[@class="page-notice__cell"]/text()').extract_first(default='') or ''
        # if error_message:
        #     print(f"Error Message: {error_message}, current_page_url: {response.url}, proxy: {response.request.meta['proxy']}")
        #     yield self.create_request(url=response.url, callback=self.parse, dont_filter=True, use_cache=False)
        #     return

        if next_page and next_page.strip('#'):
            yield self.create_request(url=next_page, callback=self.parse, meta=response.meta)
        else:
            if not links:
                print(f"No Products found in page: {response.url}, proxy: {response.request.meta['proxy']}")
                # retried = response.request.meta.get('retried', 0)
                # if retried <= 2:
                #     # yield self.create_request(url=response.url, callback=self.parse, dont_filter=True, use_cache=False)
                #     yield self.createDynamicRequest(url=response.url, callback=self.parse, dont_filter=True, use_cache=False, proxy=response.request.meta['proxy'], meta={'retried': retried+1})
                # else:
                next_page = re.sub(r'_pgn=(\d+)', lambda x: '_pgn='+str(int(x.group(1))+1), response.url, flags=re.IGNORECASE)
                print(f'modified_next_page_url: {next_page}')
                yield self.create_request(url=next_page, callback=self.parse, meta=response.meta)


    def parse_detail(self, response):
        item_specs={}
        for tr in response.xpath('//div[@class="section"]//tr'):
            for td in tr.xpath('.//td[contains(@class, "attrLabels")]'):
                key=td.xpath(u'normalize-space(./text())').extract_first(default='') or ''
                value=' '.join([a.strip() for a in td.xpath('./following-sibling::td[1]//text()[normalize-space(.)]').extract()]) or ''
                item_specs[key] = value

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//img[@itemprop="image"]/@src').extract_first(default='') or '',
            'breadcrumb': ' | '.join(response.xpath('//li[@itemprop="itemListElement"]//text()').extract()) or '',

            'title': response.xpath('//h1[@itemprop="name"]/text()').extract_first(default='') or '',
            'price': response.xpath('//span[@itemprop="price"]/text()').extract_first(),
            'shipping': response.xpath('//span[@id="fshippingCost"]/span/text()').extract_first(),
            'specs': item_specs,

            'item_number': response.xpath('//div[@id="descItemNumber"]/text()').extract_first(default='') or '',
            'upc': response.xpath('//h2[@itemprop="gtin13"]/text()').extract_first(default='') or '',
            'mpn': response.xpath('//h2[@itemprop="mpn"]/text()').extract_first(default='') or '',
        }

        filter_name = response.meta.get('filter_name', '')
        if filter_name:
            if isinstance(filter_name, str):
                item[filter_name] = response.meta.get(filter_name, '')
            elif isinstance(filter_name, list):
                for name in filter_name:
                    item[name] = response.meta.get(name, '')
        yield item


    def createDynamicRequest(self, url, callback, meta=None, wait_time=0, wait_until=None, perform_action=None, use_cache=True, proxy=None, dont_filter=False):
        request = SeleniumRequest(url=url, callback=callback,
            wait_time=wait_time,
            wait_until=wait_until,
            perform_action=perform_action,
            dont_filter=dont_filter,
        )

        if self.proxy_pool:
            # request.meta['proxy'] = random.choice(self.proxy_pool)
            request.meta['proxy'] = next(self.round_robin) if not proxy else proxy

        request.meta.update({"use_cache": use_cache})
        return request


    def create_request(self, url, callback, headers={}, meta=None, dont_filter=False, use_cache=True, body=None):
        request = scrapy.Request(url=url, callback=callback, headers=headers, meta=meta, dont_filter=dont_filter, body=body)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
            # request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache": use_cache})
        return request
