# -*- coding: utf-8 -*-
import scrapy
import random
import datetime
import pandas as pd
class EbaySellerItemsSpider(scrapy.Spider):
    name = 'ebay_seller_items'
    
    start_url = 'https://www.ebay.com/sch/169291/i.html?_from=R40&_nkw=bag'
    # 'https://www.ebay.com/sch/169291/i.html?_from=R40&_nkw=bag&rt=nc&Brand=Louis%2520Vuitton&_dcat=169291'

    proxy_pool = [
            'http://shp-mercari-us-d00001.tp-ns.com/', 
            'http://shp-mercari-us-d00002.tp-ns.com/', 
            'http://shp-mercari-us-d00003.tp-ns.com/',
            'http://shp-mercari-us-d00004.tp-ns.com/', 
            'http://shp-mercari-us-d00005.tp-ns.com/',
            'http://shp-mercari-us-d00006.tp-ns.com/', 
            'http://shp-mercari-us-d00007.tp-ns.com/', 
            'http://shp-mercari-us-d00008.tp-ns.com/',
            'http://shp-mercari-us-d00009.tp-ns.com/', 
            'http://shp-mercari-us-d00010.tp-ns.com/',
            'http://shp-mercari-us-d00011.tp-ns.com/', 
            'http://shp-mercari-us-d00012.tp-ns.com/',
            'http://shp-mercari-us-d00013.tp-ns.com/',
            'http://shp-mercari-us-d00014.tp-ns.com/', 
            'http://shp-mercari-us-d00015.tp-ns.com/',
            'http://shp-mercari-us-d00016.tp-ns.com/', 
            'http://shp-mercari-us-d00017.tp-ns.com/', 
            'http://shp-mercari-us-d00018.tp-ns.com/',
            'http://shp-mercari-us-d00019.tp-ns.com/', 
            'http://shp-mercari-us-d00020.tp-ns.com/',
        ]

    brands = [
        'PRADA',
        'Louis Vuitton',
        'Gucci',
        'Burberry',
        'Celine Dion',
        'Balenciaga',
        'Fendi',
        'Dior',
        'valentino',
        'Versace',
        'Yves Saint Laurent',
        'Coach',
        'Michael Kors',
        'kate spade new york'
    ]

    def __init__(self, crawl_for_seller_items=None, *args, **kwargs):
        super(EbaySellerItemsSpider,self).__init__(*args, **kwargs)

        self.crawl_seller_items = crawl_for_seller_items


    def start_requests(self):
        if self.crawl_seller_items:
            with open(self.crawl_seller_items, 'r') as f:
                df = pd.read_json(f, dtype=False, lines=True)
                for products_page_link in df['products_page_link']:
                    if products_page_link:
                        yield self.create_request(url=products_page_link, callback=self.parse)
        else:
            for brand in self.brands:
                url = self.start_url + '&Brand=' + brand.replace(' ', '%2520')
                yield self.create_request(url=url, callback=self.parse)
            # yield self.create_request(url=self.start_url, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request


    def parse(self, response):
        # links=[]
        for product in response.xpath('//li[@class="s-item   " or @class="sresult gvresult "]'):
            link = product.xpath('.//a[@class="s-item__link"]/@href').extract_first(default='') or ''
            if link:
                yield self.create_request(url=link, callback=self.parse_detail)
        
        next_page = response.xpath('//a[@rel="next"]/@href').extract_first(default='') or ''
        if next_page:
            yield self.create_request(url=next_page, callback=self.parse)


    def parse_detail(self, response):
        title = response.xpath('//h1[@itemprop="name"]/text()').extract_first(default='') or ''
        condition = response.xpath('//div[@itemprop="itemCondition"]/text()').extract_first(default='') or ''
        list_price = response.xpath(u'normalize-space(//span[@id="orgPrc"]/text())').extract_first(default='') or ''
        price = response.xpath('//span[@itemprop="price"]/text()').extract_first(default='').replace('US', '').strip() or ''
        location_country = ''
        location_state = ''
        location_city = ''
        location_zip = ''
        location = response.xpath('//div[@class="sh-loc"]/text()').extract()
        if location and len(location) > 1:
            addr = location[1].strip().split(',')
            if addr:
                if len(addr) > 1:
                    location_country = addr[-1].strip()
                    location_state = addr[-2].strip()
                if len(addr) > 2:
                    location_city = addr[-3].strip()
        item_number = response.xpath('//div[@id="descItemNumber"]/text()').extract_first(default='') or ''
        brand = response.xpath('//h2[@itemprop="brand"]/span[@itemprop="name"]/text()').extract_first(default='') or ''
        color = response.xpath('//h2[@itemprop="color"]/text()').extract_first(default='') or ''
        category = ''
        product_type = ''
        size = response.xpath('//td[contains(text(),"Size:")]/following-sibling::td/span/text()').extract_first(default='') or ''

        seller_link = response.xpath('//div[@class="mbg vi-VR-margBtm3"]/a/@href').extract_first(default='') or ''
        seller = response.xpath('//div[@class="mbg vi-VR-margBtm3"]/a/span[@class="mbg-nw"]/text()').extract_first(default='') or ''
        image = response.xpath('//img[@itemprop="image"]/@src').extract_first(default='') or ''
        number_of_bids = response.xpath('//span[@id="qty-test"]/text()').extract_first(default='') or ''

        ended_date = ''
        time_left = response.xpath('//span[@class="vi-tm-left"]/span/@timems').extract_first(default='') or ''
        if time_left:
            ended_date = datetime.datetime.fromtimestamp(int(time_left)/1000.0).strftime('%b %d, %Y')

        pattern = response.xpath('//td[contains(text(),"Pattern:")]/following-sibling::td/span/text()').extract_first(default='') or ''
        material = response.xpath('//td[contains(text(),"Material:")]/following-sibling::td/span/text()').extract_first(default='') or ''
        # material = ', '.join(response.xpath('//td[contains(text(),"Material:")]/following-sibling::td/span/text()').extract_first(deafult='').split(' x ')) or ''

        dimensions = response.xpath('//td[contains(text(),"Dimensions:")]/following-sibling::td/span/text()').extract_first(default='') or ''
        style = response.xpath('//td[contains(text(),"Style:")]/following-sibling::td/span/text()').extract_first(default='') or ''
        mpn = response.xpath('//h2[@itemprop="mpn"]/text()').extract_first(default='') or ''
        if 'does not apply' in mpn.lower():
            mpn = ''
        upc = response.xpath('//h2[@itemprop="gtin13"]/text()').extract_first(default='') or ''
        if 'does not apply' in upc.lower():
            upc = ''
        collection = response.xpath('//td[contains(text(),"Collection:")]/following-sibling::td/span/text()').extract_first(default='') or ''
        features = response.xpath('//td[contains(text(),"Features:")]/following-sibling::td/span/text()').extract_first(default='') or ''

        breadcrumb = ' | '.join(response.xpath('//li[@itemprop="itemListElement"]//text()').extract()) or ''
        if "Women's Bags" in breadcrumb:
            category = "women bags"
            product_type = "bags"
        
        # size = ''
        tags = ''
        box = ''
        # number_of_bids = ''
        listed_date = ''
        # ended_date = ''
        ships_from = ''
        # product_type = ''


        item_specs={}
        for tr in response.xpath('//div[@class="section"]//tr'):
            for td in tr.xpath('.//td[contains(@class, "attrLabels")]'):
                key=td.xpath(u'normalize-space(./text())').extract_first(default='') or ''
                value=''.join(td.xpath('./following-sibling::td[1]//text()[normalize-space(.)]').extract()) or ''
                item_specs[key] = value

        yield {
            "crawlDate": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "statusCode": str(response.status),
            "seller": seller,
            "id": response.url,
            "brand": brand,
            "title": title,
            "ships_from": ships_from,
            "list_price": list_price,
            "sell_price": price,
            "number_of_bids": number_of_bids,
            "image": image,
            "listed_date": listed_date,
            "ended_date": ended_date,
            "item_number": item_number,
            "category": category,
            "product_type": product_type,
            "box": box,
            "tags": tags,
            "size" : size, 
            "condition": condition,
            "material": material, 
            "style": style,
            "color/pattern": color + ' / ' if color and pattern else '' + pattern if pattern else '',
            "location_country": location_country,
            "locationi_city": location_city,
            "location_state": location_state,
            "location_zip5": location_zip,
            "catch_all": item_specs,

            'features': features,
            'collection': collection,
            'upc': upc,
            'mpn': mpn,
            'dimensions': dimensions,
            }

        if not self.crawl_seller_items and seller_link:
            yield self.create_request(url=seller_link, callback=self.seller_detail_crawl)


    def seller_detail_crawl(self, response):
        seller = response.xpath('//a[@class="mbg-id"]/text()').extract_first(default='') or ''
        seller_desc = response.xpath(u'normalize-space(//h2[@class="bio inline_value"]/text())').extract_first(default='') or ''
        seller_country = response.xpath('//span[@class="mem_loc"]/text()').extract_first(default='') or ''
        seller_state = ''
        seller_city = ''
        seller_zip = ''
        ratings = response.xpath('//a[@class="mbg-id"]/following-sibling::a/text()').extract_first(default='') or ''
        feedback = response.xpath(u'normalize-space(//div[@class="perctg"]/text())').extract_first(default='') or ''
        store_link = response.xpath('//a[@title="Visit store"]/@href').extract_first(default='') or ''
        items_link = response.xpath('//a[@title="Items for sale"]/@href').extract_first(default='') or ''
        products_page_link = store_link if store_link else (items_link + '&_dmd=2' if items_link else '')

        yield {
            "crawlDate": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "statusCode": str(response.status),
            "id": response.url,
            "seller": seller,
            "seller_desc": seller_desc,
            "seller_country": seller_country,
            "seller_state": seller_state,
            "seller_city": seller_city,
            "seller_zip5": seller_zip,
            "ratings": ratings,
            "feedBack": feedback,
            "products_page_link": products_page_link,
        }

        if products_page_link:
            # yield self.create_request(url=products_page_link, callback=self.parse)
            pass

        # "id": "https://www.ebay.com/usr/2nd-finds?_trksid=p2047675.l2559",
	    # "seller": "2nd-finds2nd-finds",
        # "seller desc": "Based in United States, 2nd-finds has been an eBay member since Jan 06, 2009",
        # "seller country" :"US",
        # "seller state": "FL", 
        # "seller city": "Miami",
        # "seller zip5": "", 
        # "ratings": "",
        # "feedBack": "",
        # "itemsForSale": [{"product_schema_here"},

    def response_html_path(self, request):
        """
        Args:
            request (scrapy.http.request.Request): request that produced the
                response.
        """
        id = request.url.split("?")[0]
        id = id.split("/")[-1]
        # id = id + "-" + str(hash(id))
        return 'html/{}.html'.format(id)


if __name__== "__main__":
    with open('output-2019-09-10 19-35-11.json', 'r') as f:
        df = pd.read_json(f, dtype=False, lines=True)

        # scrape data
        df.loc[~pd.isnull(df['title'])].dropna(axis='columns').to_json('bags_scrape.jl', orient='records', lines=True)
        # seller data
        df.loc[pd.isnull(df['title'])].dropna(axis='columns').to_json('seller_details.jl', orient='records', lines=True)

