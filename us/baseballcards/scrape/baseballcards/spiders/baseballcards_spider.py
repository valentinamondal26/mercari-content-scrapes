'''
https://mercari.atlassian.net/browse/USCC-331 - MLB Sports
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class BaseballcardsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from baseballcards.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Sports

    brand : str
        brand to be crawled, Supports
        1) MLB

    Command e.g:
    scrapy crawl baseballcards -a category='Sports' -a brand='MLB'
    """

    name = 'baseballcards'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'baseballcards.com'
    blob_name = 'baseballcards.txt'

    url_dict = {
        'Sports': {
            'MLB': {
                'url': 'https://baseballcards.mlb.com/iSynApp/allProduct.action?sid=1101343&selectedCatId=16840&query=&sort=createts_asc&qMode=$qMode&rc=50&layout=grid&pgmode1=&pgmode2=&pgmode3=&pgcust1=&pgcust3=&&qt[0].type=fieldmatch&qt[0].name=*&qt[0].value1=*&rs=0',
                'filters': [
                    "By Category", "By Team",
                ],
            }
        }
    }

    filter_list = []

    extra_item_infos = {}

    base_url = 'https://baseballcards.mlb.com'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BaseballcardsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def contracts_mock_function(self):
        self.filter_list = [
                    "By Category", "By Team",
                ]

    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_filter, dont_filter=True)

            # yield self.create_request(url=self.launch_url, callback=self.parse, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://baseballcards.mlb.com/iSynApp/productDisplay.action?sid=1101343&productId=1177356
        @meta {"use_proxy":"True"}
        @scrape_values title price specs image description
        """
        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//div[@class="item-title"]//h1/text()').extract_first(default=''),
            'price': response.xpath('//li[@class="item-price"]/span/text()').extract_first(default=''),
            'specs': {x.xpath('./th/text()').extract_first(): x.xpath('./td/text()').extract_first() for x in response.xpath('//div[@class="item-stats"]//tr')},
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(default=''),
            'description': response.xpath('//div[@id="item-description"]//text()').extract(),
        }


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse_filter(self, response):
        '''
        @url https://baseballcards.mlb.com/iSynApp/allProduct.action?sid=1101343&selectedCatId=16840&query=&sort=createts_asc&qMode=$qMode&rc=50&layout=grid&pgmode1=&pgmode2=&pgmode3=&pgcust1=&pgcust3=&&qt[0].type=fieldmatch&qt[0].name=*&qt[0].value1=*&rs=0
        @returns requests 1
        @meta {"use_proxy": "True"}
        '''
        for _filter in response.xpath('//ul[@class="filters"]/li'):
            filter_name = _filter.xpath('.//h2[@class="filter-title"]//text()').extract_first()
            # print(filter_name)

            if filter_name in self.filter_list:
                for filter_option in _filter.xpath('.//ul[contains(@id, "filter--available-")]/li[@class="filter-option"]'):
                    # print(_filter.xpath('./a/text()').extract_first())
                    # filter_name = _filter.xpath('./a/text()').extract_first()
                    # print(filter_name)
                    # if filter_name in self.filter_list:
                    # print([(x.xpath('./text()').extract_first(), x.xpath('./@href').extract_first()) for x in filter_option.xpath('.//following-sibling::li/ul[@class="list-filters__list list-filters__sublist"]/li/a')])
                    for filter_value in (filter_option.xpath('.//following-sibling::li/ul[@class="list-filters__list list-filters__sublist"]/li/a') or filter_option.xpath('./a')):
                        filter_value_name = filter_value.xpath('./text()').extract_first()
                        filter_value_url = filter_value.xpath('./@href').extract_first()
                        if filter_value and filter_value_url:
                            request = self.create_request(url=self.base_url + filter_value_url, callback=self.parse)
                            request.meta['extra_item_info'] = {
                                filter_name: filter_value_name,
                            }
                            request.meta.update({"page_number":1})
                            # print(request.meta['extra_item_info'])
                            yield request


    def parse(self, response):
        """
        @url https://baseballcards.mlb.com/iSynApp/allProduct.action?sid=1101343&rc=50&sort=createts_asc&query=&qMode=&selectedCatId=16840&pgmode1=teamsearch&pgcust1=diamondbacks&pgcust3=panname_teamname_s&qt[0].type=fieldmatch&qt[0].name=panname_teamname_s&qt[0].value1=diamondbacks
        @meta {"use_proxy":"True","page_number":1}
        @returns requests 51
        """
        # ###No of requests mentioned in contracts is 51 beacause 50 products per page and 1 Next Page url.

        page_number = response.meta["page_number"]
        pages = response.xpath("//nav[@class='pag pag-list']/ul/li/a/text()").getall()
        if response.meta.get("total_pages","") == "":
            if pages:
                num = [i.replace("…","") for i in pages if "…" in i]
                if num:   ####1,2,3,4....1256
                    total_pages  = int(num[0])
                else:
                    total_pages  =  max([int(i) for i in pages if i.isdigit()]) ##1,2,3,4,5
            else:
                total_pages = 1
        else:
            total_pages = response.meta.get("total_pages","")
            # print("page number count "+ str(total_pages))

        # print(response.url +" total pages "+ str(total_pages)+ " page number "+ str(page_number))

        if page_number <= total_pages:
            extra_item_info = response.meta.get('extra_item_info', {})

            for product in response.xpath('//ul[@class="item-list"]/li[@class="item-list__item"]'):

                id = product.xpath('.//a/@href').extract_first()

                if id:
                    id = self.base_url + id
                    if not self.extra_item_infos.get(id, None):
                        self.extra_item_infos[id] = {}

                    # self.extra_item_infos.get(id).update(extra_item_info)
                    self.update_extra_item_infos(id, extra_item_info)
                    request = self.create_request(url=id, callback=self.crawlDetail)
                    yield request

            next_link = response.xpath('//nav[@class="pag pag-list"]//i[@class="fa fa-arrow-right"]/parent::a/@href').extract_first()
            if next_link:
                next_link = self.base_url + next_link
                request  = self.create_request(url=next_link, callback=self.parse)
                request.meta['extra_item_info'] = extra_item_info
                request.meta["page_number"] = page_number+1
                request.meta["total_pages"] = total_pages
                yield request


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

