import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            
            price =  record.get("price","").replace("$","")

            model_sku  =  record.get("specs",{}).get("Item Number:","")
            
            item_condition = record.get("specs",{}).get("Condition:","")
            
            team = record.get("By Team","")
            team = ",".join(team)
            
            product_type =  record.get("By Category","")
            product_type = ",".join(product_type)

            title =  record.get("title","").strip()
            
            year = re.findall(re.compile(r'\d+'),title)[0]

            model = title.replace(year,"").strip()
            model = re.sub(re.compile(r'\bCard\b',re.IGNORECASE),"",model)
            
            description =  record.get("description","")
            description[0] =  description[0].strip()
            try:
                description.remove(title)
            except  ValueError:
                pass           
            description = ",".join(description)
            description = description.replace(":,",":").replace(".,",".").replace(",,",",").replace("..",".").replace('\r', ' ').replace('\n', ' ')

            transformed_record = {
                "title" : title,
                "model" : model,
                "category" : record.get('category',""),
                "brand" : record.get("brand",""),
                "image" : record.get("image",""),
                "description" : description,
                "model_sku" : model_sku,
                "status_code":record.get("statusCode",""),
                "id":record.get("id",""),
                "item_id" : hex_dig,
                "product_type" : product_type,
                "teambaseball": team,
                "year" : year,
                "crawl_date": record.get('crawlDate',""),
                "item_condition" : item_condition,
                "msrp":{
                    "currency_code" : "USD",
                    "amount" : price
                }
            }
            return transformed_record
