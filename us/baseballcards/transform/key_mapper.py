class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        year = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            if "year" == entity['name']:
                if entity["attribute"]:
                    year = entity["attribute"][0]["id"]

        key_field = model + year
        return key_field
