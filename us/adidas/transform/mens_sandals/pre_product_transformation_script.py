import hashlib
import re
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:

            title = record.get('title', '')
            description = record.get('description', '')
            features = record.get('features', '')
            ner_query = f'{description} {features} {title}'

            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color_filter = record.get('Color', '')
            color = record.get('color', '')
            color = re.sub(r'\s/\s', '/', color)
            colors = list(dict.fromkeys(color.split('/')))
            if len(colors) > 2:
                colors = [colors[0], 'Multicolor']
            color = '/'.join(colors)

            model = record.get('model', '')
            model = re.sub(re.compile(r"Gender Neutral", re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "title": title,
                "image": record.get('image', ''),
                "ner_query": ner_query.replace('\n', ' '),
                "rating": record.get('rating', ''),
                "model_number": record.get('model_number', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "features": features,
                "description": description.replace('\n', ' '),

                "color": color_filter or color,
                "model_sku": record.get('sku', ''),
                "model": model,
                "price": {
                    "currency_code": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('Collection', ''),
            }

            return transformed_record
        else:
            return None
