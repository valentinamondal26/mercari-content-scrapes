import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:

            title = record.get('title', '')
            description = record.get('description', '')
            features = record.get('features', '')
            ner_query = f'{description} {features} {title}'
            # print("Ner Query : {}".format(ner_query))

            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = record.get('color', '')
            color = re.sub(re.compile(r'\beqt\b|\bfcb\b', re.IGNORECASE), lambda m: m[0].upper() if m else '', color)
            color = re.sub(re.compile(r'(Ncaa-.*?\s|Mls-.*?\s|Ns-.*?(\s|$))', re.IGNORECASE), lambda m: m[0].upper() if m else '', color)
            color = re.sub(' +', ' ', color).strip()

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = record.get('model', '')
            model = re.sub(re.compile(r"Authentic|Jersey|"+pattern_word(color), re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            # activity = ''
            # match = re.findall(re.compile(r'Athletic|Casual', re.IGNORECASE), ner_query)
            # if match:
            #     activity = ', '.join(match)#match[0].title()
            
            # collection = ''
            # match = re.findall(re.compile(r'Tiro17|Condivo|Squadra|Tango|Teamwear|Tiro|Harden|Predator|adicolor|Liberty Cup|Nemeziz', re.IGNORECASE), ner_query)
            # if match:
            #     collection = ', '.join(match)

            # pattern = ''
            # match = re.findall(re.compile(r'Camo|Plaid', re.IGNORECASE), ner_query)
            # if match:
            #     pattern = ', '.join(match)
            
            # product_collaboration = ''
            # match = re.findall(re.compile(r'MLS|Club|Federation|College|NHL|Parley|NTS Radio|Bed JW Ford', re.IGNORECASE), ner_query)
            # if match:
            #     product_collaboration = ', '.join(match)

            # material = ''
            # match = re.findall(re.compile(r'Ocean Plastic|Recycled Polyester', re.IGNORECASE), ner_query)
            # if match:
            #     material = ', '.join(match)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "title": title,
                "image": record.get('image', ''),
                "ner_query": ner_query,
                # "product_line": record.get('product_line', ''),
                "rating": record.get('rating', ''),

                "color": color,
                "model_sku": record.get('sku', ''),
                "description": description,
                "model": model,
                "model_number": record.get('model_number', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "features": features,
                "price":{
                    "currency_code": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('filter_brand', ''),
                "type_of_sport": record.get('filter_sport', ''),
                "team": record.get('filter_sportSubSub', ''),

                'activity': record.get('Activity', '') \
                    or record.get('activity', ''),
                'collection': record.get('Collection', '') \
                    or record.get('sportsub_en_us', ''),
                'pattern': record.get('Pattern', '') \
                    or record.get('pattern', ''),
                'product_collaboration': record.get('Collaboration', '') \
                    or record.get('partner_en_us', ''),
                "material": record.get('Material', '') \
                    or record.get('base_material', ''),
            }

            return transformed_record
        else:
            return None
