import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:

            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record['price']
            description = record.get('description', '').replace('\n', '').replace('\r', '')

            model = record.get('model', '')
            keywords = ['shoes', 'shoe', 'adidas', 'Multicolor', ]
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = model.title()
            title_case_exception_words = {
                'gamecourt': 'GameCourt',
                'run': 'RUN',
                # 'vol.': 'Volume',
                # 'sensebounce +': 'Sensebounce+',
                'alphaedge': 'AlphaEDGE',
                'senseboost go': 'Senseboost GO',
                'gore-tex': 'GORE-TEX',
                'sl': 'SL',
                'xt': 'XT',
                'rc': 'RC',
                'dpr': 'DPR',
                'em': 'EM',
                'ppf': 'PPF',
                'tpu': 'TPU',
                'sp': 'SP',
                'dlx': 'DLX',
                'ck': 'CK',
                'vcs': 'VCS',
                'xc': 'XC',
                'Eps': 'EPS',

                'sk': 'SK',
                'ltd': 'LTD',
                'md': 'MD',
                'cp': 'CP',
                'byw': 'BYW',
                'ii': 'II',
                'rbn': 'RBN',
                'n3xt': 'N3xt',
                'l3v3l': 'L3v3l',
                'adv': 'ADV',
                'hd': 'HD',
                'summer': 'SUMMER',
                'rdy': 'RDY',
                'rbl': 'RBL',
                'purebounce': 'PureBOUNCE',
                'sr': 'SR',
                '90s': '90s',
                'sl20': 'SL20',
                'st': 'ST',
                'lt': 'LT',
                'Ax3': 'AX3',
                'gtx': 'GTX',
                'sb': 'SB',
                'dna': 'DNA',
                'pb': 'PB',
                'eps': 'EPS',
                'speedfactory': 'SPEEDFACTORY',
                'tr': 'TR',
                'ax3': 'AX3',
            }
            model = re.sub(re.compile(
                r'|'.join(list(map(lambda x: r'\b'+x+r'\b', title_case_exception_words.keys()))), re.IGNORECASE),
                lambda x: title_case_exception_words.get(x.group().lower(), x.group()), model)
            model = re.sub(re.compile(r'\bVol\.', re.IGNORECASE), 'Volume', model)
            model = re.sub(re.compile(r'\w+ \+', re.IGNORECASE), lambda x: re.sub(r'\s+', '', x.group()), model)
            model = re.sub(re.compile(r"\bWomen's\b", re.IGNORECASE), "Women's", model)
            model = re.sub(re.compile(r"\b4Orged\b", re.IGNORECASE), "4orged", model)

            model = re.sub(r'\s-\s', ' ', model)
            model = re.sub(r'\s/\s', ' ', model)
            model = re.sub(r'\s+|_', ' ', model).strip()

            features = record.get('features', '')
            color = record.get('color', '')
            color = re.sub(re.compile(r'\bColleg\b', re.IGNORECASE), 'College', color)
            color = '/'.join(list(dict.fromkeys([c.strip() for c in color.split('/')])))

            ner_query = description + ' ' + color + ' ' + features

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "image": record.get('image', ''),
                "title": record.get('title', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "rating": record.get('rating', ''),
                "features": features,
                "ner_query": ner_query,

                "sku": record.get('sku', ''),
                "model_number": record.get('model_number', ''),

                "description": features,
                "model": model,
                "product_line": record.get('Collection', ''),
                "color": color,
                "material": record.get('Material', ''),
                #"Leather,Canvas,Suede,Velvet,Satin,Fur,Shearling,Rubber,Acrylic,Wicker & Straw,Other",
                "msrp": {
                    "currency_code": "USD",
                    "amount": str(price),
                }
            }

            return transformed_record
        else:
            return None
