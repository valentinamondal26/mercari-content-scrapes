import hashlib
import re

from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:

            title = record.get('title', '')
            description = record.get('description', '')
            features = record.get('features', '')
            ner_query = f'{description} {features} {title}'

            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # color transformations
            color_filter = record.get('Color', '')
            color = record.get('color', '')
            if not color:
                color = color_filter
            color = re.sub(re.compile(r"Branded\s*\-\s*Add\s*\-\s*M\d+",
                           re.IGNORECASE), "", color)
            color = re.sub(re.compile(
                    r"(.*\/?)(Ncaa)\-[\w\d\-]+(\/?.*)", re.IGNORECASE),
                    r"\1 \2 \3", color).strip("/")
            color = re.sub(
                    re.compile(r"Multi\-*\s*color|\bmulti\b", re.IGNORECASE),
                    "Multicolor", color)
            color = "/".join([c.strip() for c in color.split("/")])
            color = re.sub(re.compile(r"(.*)(\/*Ncaa\/*)(.*)", re.IGNORECASE),
                           r"\1\3/\2", color).strip("/")
            if len(color.split("/")) > 2:
                color = color.split("/")[0] + "/Multicolor"
            if not re.findall(re.compile(r"\/*Multicolor$", re.IGNORECASE),
                              color):
                color = re.sub(re.compile(r"(.*)(\/*Multicolor\/*)(.*)",
                                          re.IGNORECASE), r"\1\3/\2",
                               color).strip("/")
            color = "/".join([c for c in sorted(set(color.split("/")),
                                                key=color.split("/").index)])

            # model transformations
            model = record.get('model', '')
            model = re.sub(re.compile(
                    r"\(PLUS SIZE\)|\(Gender Neutral\)|\badidas\b",
                    re.IGNORECASE), '', model)
            if re.findall(re.compile(r"\btank\b|\bcrop\b", re.IGNORECASE),
                          model):
                model = re.sub(re.compile(
                        r"(.*)(Tee|T\-*\s*shirt|tank top|crop top)(.*)",
                        re.IGNORECASE), r"\1 \3 \2", model)
            else:
                model = re.sub(re.compile(
                        r"(.*)(Tee|T\-*\s*shirt|top)(.*)", re.IGNORECASE),
                        r"\1 \3 \2", model)
            model = re.sub(re.compile(r"W Ls Gtp T", re.IGNORECASE),
                           "W Gtp Long Sleeve T-Shirt", model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            # extract fit, material
            fit_meta = [
                    'Tight fit', 'Fitted fit', 'Compression fit', 'Relaxed fit',
                    'Slim fit',
                    'Regular fit', 'Skinny fit', 'Loose fit',
                    'Regular tapered fit',
                    'Slim tapered fit', 'Oversize fit', 'Athletic fit',
                    'Boyfriend fit'
            ]
            fit = ''
            match = re.findall(r'|'.join(
                    list(map(lambda x: r'\b' + x + r'\b', fit_meta))), features,
                    flags=re.IGNORECASE)
            if match:
                fit = ', '.join(match)
                fit = fit.title()

            material = ''
            match = re.findall(
                    r'\d+%\s*(.*?)\s*[/,;]', features, flags=re.IGNORECASE)
            if match:
                material = ', '.join(list(map(lambda x: re.sub(
                        r'\binterlock\b', '', x, flags=re.IGNORECASE).strip(),
                        match)))
                material = material.title()

            to_remove = ['^ONE TEAM GRAPHIC LONG SLEEVE TEE$',
                        '^ID TEE$',
                        '^SHMOO LOGO TEE$']
            if re.compile(r"|".join(to_remove), re.IGNORECASE).match(
                record.get("title", "")):
                return None
            if re.compile(r"^(Tee|t\-*\s*shirt|Tank Top|crop top)$",
                          re.IGNORECASE).match(model):
                return None

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),
                    "category": record.get('category', ''),
                    "brand": record.get('brand', ''),

                    "image": record.get('image', ''),
                    "ner_query": ner_query.replace('\n', ' '),
                    "rating": record.get('rating', ''),
                    "model_number": record.get('model_number', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "features": features,
                    "description": description.replace('\n', ' '),
                    "title": title,
                    "model": model,
                    "color": color,
                    'style': record.get('Style', ''),
                    'fit': fit,
                    "model_sku": record.get('sku', ''),

                    "price": {
                            "currency_code": "USD",
                            "amount": str(price)
                    },
                    "product_line": record.get('filter_brand', ''),
                    'activity': record.get('Sport', ''),
                    "material": material,
            }

            return transformed_record
        else:
            return None
