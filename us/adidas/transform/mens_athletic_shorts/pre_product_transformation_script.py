import hashlib
import re

from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:

            title = record.get('title', '')
            description = record.get('description', '')
            features = record.get('features', '')
            ner_query = f'{description} {features} {title}'

            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # color transformations
            color_filter = record.get('Color', '')
            color = record.get('color', '')
            color = color_filter or color
            color = re.sub(r'^(Ncaa-.*?)/(.*)', r'\2/\1', color)
            colors = color.split('/')
            colors = list(map(lambda x: re.sub(r'^\s*(Ncaa)-(.*)', r'\1', x,
                              flags=re.IGNORECASE).strip(), colors))
            if len(colors) > 2:
                colors = [colors[0], 'Multicolor']
            color = '/'.join(colors)

            # model transformaions
            model = record.get('model', '')
            model = re.sub(r'®|™', '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r"\bSPRT\b", re.IGNORECASE),
                           "Sport", model)
            model = re.sub(re.compile(r"Two\-*\s*In\-*\s*One", re.IGNORECASE),
                           "2 in 1", model)
            model = re.sub(re.compile(r"(.*)(\d+\s*in\s*\d+)(.*)",
                           re.IGNORECASE), r"\2 \1 \3", model)
            if not re.findall(r'\bShorts\b|\bShort\b', model,
                              flags=re.IGNORECASE):
                model += ' Shorts'
            elif not re.findall(r'\bTights\b', model, flags=re.IGNORECASE):
                model = re.sub(r'(.*)(\bShorts\b|\bShort\b)(.*)', r'\1 \3 \2',
                               model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            # extract fit from meta and model
            fit_meta = [
                    'Tight fit', 'Fitted fit', 'Compression fit', 'Relaxed fit',
                    'Slim fit',
                    'Regular fit', 'Skinny fit', 'Loose fit',
                    'Regular tapered fit',
                    'Slim tapered fit', 'Oversize fit', 'Athletic fit',
            ]
            fit = ''
            match = re.findall(
                    r'|'.join(list(map(lambda x: r'\b' + x + r'\b', fit_meta))),
                    features, flags=re.IGNORECASE)
            if match:
                fit = ', '.join(match)
                fit = fit.title()

            material = ''
            match = re.findall(r'\d+%\s*(.*?)\s*[/,;]', features,
                               flags=re.IGNORECASE)
            if match:
                material = ', '.join(list(map(lambda x: x.strip(), match)))
                material = material.title()
            if re.compile(r"^shorts*$", re.IGNORECASE).match(model):
                return None

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),
                    "category": record.get('category', ''),
                    "brand": record.get('brand', ''),

                    "title": title,
                    "image": record.get('image', ''),
                    "ner_query": ner_query.replace('\n', ' '),
                    "rating": record.get('rating', ''),
                    "model_number": record.get('model_number', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "features": features,
                    "description": description.replace('\n', ' '),

                    "color": color,
                    'fit': fit,
                    "model_sku": record.get('sku', ''),
                    "model": model,
                    "price": {
                            "currency_code": "USD",
                            "amount": str(price)
                    },
                    "product_line": record.get('filter_brand', ''),
                    'activity': record.get('Sport', ''),
                    "length": record.get('Length', ''),
                    'material': material,
            }

            return transformed_record
        else:
            return None
