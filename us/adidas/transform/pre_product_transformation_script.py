import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:

            price = record['price']
            description = record.get('description', '')
            model = record.get('model', '')
            pattern = re.compile("shoes", re.IGNORECASE)
            model = pattern.sub('', model)
            if model:
                model = model.strip()
            features = record.get('features', '')
            color = record.get('color', '')
            ner_query = description + ' ' + color + ' ' + features
            print("Ner Query : {}".format(ner_query))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "category": "Men's Athletic Shoes",
                "brand": record['brand'],
                "image": record['image'],
                "product_line": record['product_line'],
                "color": color,
                "sku": record['sku'],
                "description": description,
                "title": record['title'],
                "model": model,
                "model_number": record['model_number'],
                "rating": record['rating'],
                "breadcrumb": record['breadcrumb'],
                "material": "Leather,Canvas,Suede,Velvet,Satin,Fur,Shearling,Rubber,Acrylic,Wicker & Straw,Other",
                "features": features,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": str(price)
                }
            }

            return transformed_record
        else:
            return None
