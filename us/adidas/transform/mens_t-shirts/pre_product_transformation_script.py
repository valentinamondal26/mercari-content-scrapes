import hashlib
import re

from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:

            title = record.get('title', '')
            description = record.get('description', '')
            features = record.get('features', '')
            ner_query = f'{description} {features} {title}'

            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace("$", "").strip()

            # color transformations
            color_filter = record.get('Color', '')
            color = record.get('color', '')
            color = re.sub(re.compile(r"Branded\s*\-\s*Add\s*\-\s*M\d+",
                           re.IGNORECASE), "", color)
            color = re.sub(r'\s/\s', '/', color)
            colors = list(dict.fromkeys(color.split('/')))
            color = '/'.join(colors)
            color = re.sub(
                    re.compile(r"(.*\/?)(Ncaa|Nhl|Mls)\-[\w\d\-]+(\/?.*)",
                               re.IGNORECASE), r"\1 \2 \3", color)
            color = re.sub(
                    re.compile(r"Multi\-*\s*color|\bmulti\b", re.IGNORECASE),
                    "Multicolor", color)
            color = "/".join([c.strip() for c in color.split("/")])
            color = re.sub(re.compile(r"(.*)(\/*(Ncaa|Nhl|Mls)\/*)(.*)",
                           re.IGNORECASE), r"\1\4/\2", color).strip("/")
            if not re.findall(re.compile(r"\/*Multicolor$", re.IGNORECASE),
                              color):
                color = re.sub(re.compile(r"(.*)(\/*Multicolor\/*)(.*)",
                               re.IGNORECASE), r"\1\3/\2", color).strip("/")
            if len(color.split("/")) > 2:
                color = color.split("/")[0] + "/Multicolor"
            color = "/".join([c for c in sorted(set(color.split("/")),
                             key=color.split("/").index)])

            # model transformations
            model = record.get('model', '')
            model = re.sub(re.compile(r"\(PLUS SIZE\)|\(Gender Neutral\)|"
                                      r"Adidas|™|®",
                           re.IGNORECASE), '', model)
            model = re.sub(r'\bAMPLIFIER\b', 'Amplifier', model)
            model = re.sub(r"(.*?)(T\-*\s*Shirt|Tee|shirt)(.*)", r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'T-*\s*Shirt', 'T-Shirt', model, flags=re.IGNORECASE)
            if not re.findall(r"T\-*\s*Shirt|Tee|shirt", model, flags=re.IGNORECASE):
                model += " Tee"

            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            model = re.sub('FREELIFT SOLID TENNIS T-SHIRT HEAT.RDY', 'Freelift Solid Tennis HEAT.RDY', model, flags=re.IGNORECASE)
            model = re.sub('FREELIFT SOLID TENNIS HEAT.RDY T-Shirt','Freelift Solid Tennis HEAT.RDY T-Shirt',model, flags=re.IGNORECASE)
            model = re.sub('MEN VOLLEYBALL GRAPHIC LOGO', 'Men Volleyball Graphic Logo', model, flags=re.IGNORECASE)
            # model = re.sub('LIQUIDAT', 'Liquidat', model, flags=re.IGNORECASE)


            # extract fit , material entitites
            fit_meta = [
                    'Tight fit', 'Fitted fit', 'Compression fit', 'Relaxed fit',
                    'Slim fit',
                    'Regular fit', 'Skinny fit', 'Loose fit',
                    'Regular tapered fit',
                    'Slim tapered fit', 'Oversize fit', 'Athletic fit',
                    'Boyfriend fit'
            ]
            fit = ''
            match = re.findall(
                    r'|'.join(list(map(lambda x: r'\b' + x + r'\b', fit_meta))),
                    features, flags=re.IGNORECASE)
            if match:
                fit = ', '.join(match)
                fit = fit.title()

            material = ''
            match = re.findall(r'\d+%\s*(.*?)\s*[/,;]', features,
                               flags=re.IGNORECASE)
            if match:
                material = ', '.join(list(map(lambda x: x.strip(), match)))
                material = material.title()

            if re.findall(re.compile(r"gender\s*\-*neutral|unisex|"
                                    r"3\-\s*STRIPES TEE",
                          re.IGNORECASE), title) \
                    or re.compile(r"^T\s*\-*Shirt$|^tee$|"
                                  r"^shirt$",
                                  re.IGNORECASE).match(model):
                return None

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),
                    "category": record.get('category', ''),
                    "brand": record.get('brand', ''),

                    "title": title,
                    "image": record.get('image', ''),
                    "ner_query": ner_query.replace('\n', ' '),
                    "rating": record.get('rating', ''),
                    "model_number": record.get('model_number', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "features": features,
                    "description": description.replace('\n', ' '),

                    "color": color_filter or color,
                    'fit': fit,
                    "model_sku": record.get('sku', ''),
                    "model": model,
                    "price": {
                            "currency_code": "USD",
                            "amount": str(price)
                    },
                    "product_line": record.get('Collection', ''),
                    'activity': record.get('Activity', ''),
                    "material": material,
            }

            return transformed_record
        else:
            return None
