'''
https://mercari.atlassian.net/browse/USCC-142 - Adidas Jerseys
https://mercari.atlassian.net/browse/USDE-1437 - Adidas Men's Athletic Shoes
https://mercari.atlassian.net/browse/USDE-1697 - Adidas Pants, tights, leggings
https://mercari.atlassian.net/browse/USDE-1698 - Adidas Women's Activewear Jackets
https://mercari.atlassian.net/browse/USDE-1701 - Adidas Women's Activewear Tops

https://mercari.atlassian.net/browse/USDE-1719 - Adidas Men's Hoodies & Sweatshirts
https://mercari.atlassian.net/browse/USDE-1720 - Adidas Women's Hoodies & Sweatshirts
https://mercari.atlassian.net/browse/USDE-1721 - Adidas Men's Athletic Pants
https://mercari.atlassian.net/browse/USDE-1722 - Adidas Men's Sandals
https://mercari.atlassian.net/browse/USDE-1723 - Adidas Women's Sandals
https://mercari.atlassian.net/browse/USDE-1724 - Adidas Men's Athletic Shorts
https://mercari.atlassian.net/browse/USDE-1725 - Adidas Women's Exercise Shorts
https://mercari.atlassian.net/browse/USDE-1729 - Adidas Baby & Toddler Shoes
https://mercari.atlassian.net/browse/USDE-1730 - Adidas Women's Tracksuits
https://mercari.atlassian.net/browse/USDE-1731 - Adidas Men's T-Shirts
https://mercari.atlassian.net/browse/USDE-1709 - Adidas Men's Athletic Jackets

'''

import scrapy
import json
from datetime import datetime
from scrapy.exceptions import CloseSpider
import random
import os
from scrapy import signals

class AdidasSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from adidas.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Womens Sneakers
        2) Womens Athletic Shoes
        3) Men's Athletic Shoes
        4) Jerseys
        5) Pants, tights, leggings
        6) Women's Activewear Jackets
        7) Women's Activewear Tops
        8) Men's Hoodies & Sweatshirts
        9) Women's Hoodies & Sweatshirts
        10) Men's Athletic Pants
        11) Men's Sandals
        12) Women's Sandals
        13) Men's Athletic Shorts
        14) Women's Exercise Shorts
        15) Baby & Toddler Shoes
        16) Women's Tracksuits
        17) Men's T-Shirts
        18) Men's Athletic Jackets

    brand : str
        category to be crawled, Supports
        1) Adidas

    Command e.g:
    scrapy crawl adidas_filter -a category='Jerseys' -a brand='Adidas'

    scrapy crawl adidas_filter -a category="Men's Athletic Shoes" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Pants, tights, leggings" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Women's Exercise Jackets & Vests" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Women's Activewear Tops" -a brand='Adidas'

    scrapy crawl adidas_filter -a category="Men's Hoodies & Sweatshirts" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Women's Hoodies & Sweatshirts" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Men's Athletic Pants" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Men's Sandals" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Women's Sandals" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Men's Athletic Shorts" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Women's Exercise Shorts" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Baby & Toddler Shoes" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Women's Tracksuits" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Men's T-Shirts" -a brand='Adidas'
    scrapy crawl adidas_filter -a category="Men's Athletic Jackets" -a brand='Adidas'

    """

    name = "adidas_filter"

    base_url = 'https://www.adidas.com{}'
    listing_page_api_url = 'https://www.adidas.com/api/search/taxonomy?sitePath=us&query={query}&start={start}'
    query = None
    details_page_api_url = 'https://www.adidas.com/api/products/{}?sitePath=us'

    # start_index = 0
    # count = 0

    category = "Womens Sneakers"
    brand = "Adidas"
    source = 'adidas.com'
    blob_name = 'adidas.txt'

    merge_key = 'sku'

    # NOTE: Earlier Doesn't work with proxies as Api's are irresponsive. But now its working.
    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Womens Sneakers': {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-continental_80',
            }
        },
        'Womens Athletic Shoes': {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-athletic_sneakers',
            }
        },
        "Men's Athletic Shoes": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-athletic-athletic_sneakers',
                # 'url': 'https://www.adidas.com/us/men-athletic_sneakers-shoes',
                'filters': ['Collection', 'Material', 'Color'],
            }
        },
        'Jerseys': {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-jerseys',
                'filters': [
                    'Activity', 'Collection', 'Pattern', 'Collaboration', 'Material',
                    # 'Sport', 'sportsubsub_en_us', 'brand_en_us',
                    ]
            }
        },
        'Pants, tights, leggings': {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-pants%7Ctights',
                'filters': [
                    'Color', 'Style', 'Rise', 'Sport', 'Collection', 'Material',
                ]
            }
        },
        "Women's Activewear Jackets": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-jackets',
                'filters': [
                    'Color', 'Style', 'Sport', 'Collection', 'Material',
                ]
            }
        },
        "Women's Activewear Tops": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-t_shirts',
                'filters': [
                    'Color', 'Style', 'Sport', 'Collection', 'Material',
                ]
            }
        },
        "Men's Hoodies & Sweatshirts": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-hoodies-hoodies_sweatshirts',
                'filters': [
                    'Color', 'Sport', 'Collection', 'Material',
                ]
            }
        },
        "Women's Hoodies & Sweatshirts": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-hoodies-hoodies_sweatshirts',
                'filters': [
                    'Color', 'Sport', 'Collection', 'Material',
                ]
            }
        },
        "Men's Athletic Pants": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-sweatpants%7Ctrack-pants%7Ctrack_suits',
                'filters': [
                    'Color', 'Sport', 'Collection', 'Material',
                ]
            }
        },
        "Men's Sandals": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-slides',
                'filters': [
                    'Color', 'Collection',
                ]
            }
        },
        "Women's Sandals": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-slides',
                'filters': [
                    'Color', 'Collection',
                ]
            }
        },
        "Men's Athletic Shorts": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-shorts',
                'filters': [
                    'Color', 'Collection', 'Sport', 'Length',
                ]
            }
        },
        "Women's Exercise Shorts": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-shorts',
                'filters': [
                    'Color', 'Collection', 'Sport', 'Length',
                ]
            }
        },
        "Baby & Toddler Shoes": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/kids-boys-infant_toddler-shoes',
                'filters': [
                    'Color', 'Collection', 'Activity'
                ]
            }
        },
        "Women's Tracksuits": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/women-track_suits',
                'filters': [
                    'Color', 'Collection', 'Activity'
                ]
            }
        },
        "Men's T-Shirts": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-t_shirts',
                'filters': [
                    'Color', 'Collection', 'Activity'
                ]
            }
        },
        "Men's Athletic Jackets": {
            'Adidas': {
                'url': 'https://www.adidas.com/us/men-jackets',
                'filters': [
                    'Color', 'Collection', 'Style', 'Sport',
                ]
            }
        },
    }

    filterList = []
    # filterList = [
    #     'activity',
    #     # 'sport_en_us',
    #     'sportsub_en_us',
    #     'pattern',
    #     'partner_en_us',
    #     # 'sportsubsub_en_us',
    #     'base_material',
    #     # 'brand_en_us',
    # ]

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if  os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        super(AdidasSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filterList = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.query = self.launch_url.rsplit('/', 1)[-1]
        self.logger.info(f'Crawling for category:{category} and brand:{brand}, url: {self.launch_url}, query:{self.query}')

    def contracts_mock_function(self):
        self.launch_url = 'https://www.adidas.com/us/men-athletic-athletic_sneakers'
        self.query = self.launch_url.rsplit('/', 1)[-1]
        self.filterList = ['Collection', 'Material', 'Color']
        self.listing_page_api_url = 'https://www.adidas.com/api/search/taxonomy?sitePath=us&query={query}&start={start}'
        self.details_page_api_url = 'https://www.adidas.com/api/products/{}?sitePath=us'


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url and self.query:
            url = self.listing_page_api_url.format(query=self.query, start=str(0))
            self.logger.info('url: {}'.format(url))
            yield self.create_request(url=url, callback=self.parse, dont_filter=True)
            yield self.create_request(url=url, callback=self.parseFilter, dont_filter=True)


    def parseFilter(self, response):
        """
        @url https://www.adidas.com/api/search/taxonomy?sitePath=us&query=men-athletic-athletic_sneakers&start=0
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        res = json.loads(response.text)
        filterList = res.get('filterList', [])
        for filter in filterList:
            if filter.get('title', '') in self.filterList:
                filter_key = filter.get('title', '')
                for filter_value in filter.get('values', []):
                    # item = dict()
                    # item[filter_key] = filter_value.get('value', '')
                    if filter_value.get('value', ''):
                        url = self.listing_page_api_url.format(query=self.query+'-'+filter_value.get('value', ''), start=str(0))
                        self.logger.info('url: {}'.format(url))
                        request = self.create_request(url=url, callback=self.parse)
                        request.meta['extra_item_info'] = {
                            filter_key: filter_value.get('displayName', '')
                        }
                        request.meta['filter_query'] = filter_value.get('value', '')
                        yield request


    def parse(self, response):
        """
        @url https://www.adidas.com/api/search/taxonomy?sitePath=us&query=men-athletic-athletic_sneakers&start=0
        @meta {"use_proxy":"True"}
        @returns requests 1
        @scrape_values meta.item.id meta.item.title meta.item.image meta.item.product_line meta.item.color meta.item.sku meta.item.model meta.item.rating 
        """        
        extra_item_info = response.meta.get('extra_item_info', {})
        res = json.loads(response.text)
        items = res['itemList']['items']
        count = res['itemList']['count']
        view_size = res['itemList']['viewSize']
        start_index = res['itemList']['startIndex']

        for item in items:
            id = self.base_url.format(item.get('link', ''))
            product_id = item.get('productId', '')

            if not self.extra_item_infos.get(product_id, None):
                self.extra_item_infos[product_id] = {}

            self.extra_item_infos.get(product_id).update(extra_item_info)

            image = ""
            image = item.get('image', {}).get('src', '')
            # product_id = item.get('productId', '')
            rating = item.get('rating', 0)
            product = {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'title': item.get('displayName', ''),
                'id': id,
                'image': image,
                'product_line': item.get('subTitle', ''),
                'color': item.get('color', ''),
                'sku': product_id,
                'model': item.get('displayName', ''),
                'rating': str(rating) if rating is not None else '',
                'material': "",
            }
            # product.update(item_info)
            url = self.details_page_api_url.format(product_id)
            self.logger.info('parse_detail_page: {}'.format(url))

            request = self.create_request(url, callback=self.crawl_detail_page)
            request.meta['item'] = product
            yield request


        start_index += view_size
        if start_index < count:
            filter_query = response.meta.get('filter_query', '')
            # query = self.query + ('-'+filter_query if filter_query else '')
            query = self.query + '-' + filter_query
            url = self.listing_page_api_url.format(query=query, start=str(start_index))
            request = self.create_request(url=url, callback=self.parse)
            import copy
            request.meta['extra_item_info'] = copy.deepcopy(extra_item_info)#{}.update(extra_item_info)
            request.meta['filter_query'] = filter_query
            yield request


    def crawl_detail_page(self, response):
        """
        @url https://www.adidas.com/api/products/FY3425?sitePath=us
        @meta {"use_proxy":"True"}
        @scrape_values description model_number price breadcrumb features filter_brand filter_sport filter_sportSubSub
        """
        res = json.loads(response.text)
        item = response.meta.get('item', {})

        breadcrumbs = res.get('breadcrumb_list', [])
        bc = []
        for breadcrumb in breadcrumbs:
            bc.append(breadcrumb.get('text', ''))
        bc.append(item.get('title', ''))

        specs = []
        specifications = res.get('product_description', {}).get('usps', [])
        for specification in specifications:
            specs.append(specification)


        price = res.get('pricing_information', {}).get('standard_price', '')
        item['price'] = str(price) if price is not None else ''
        item['model_number'] = res.get('model_number', '')
        item['description'] = res.get('product_description', {}).get('text', '')
        item['breadcrumb'] = ' | '.join(bc)
        item['features'] = ','.join(specs)
        item['filter_brand'] = res.get('attribute_list', {}).get('brand', '')
        # item['filter_category'] = res.get('attribute_list', {}).get('category', '')
        item['sub_text'] = res.get('attribute_list', {}).get('color', '')
        item['filter_sport'] = ', '.join(res.get('attribute_list', {}).get('sport', []))
        # item['filter_productType'] = ', '.join(res.get('attribute_list', {}).get('productType', []))
        item['filter_sportSubSub'] = ', '.join(res.get('attribute_list', {}).get('sportSubSub', []))

        yield item


    def create_request(self, url, callback, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

