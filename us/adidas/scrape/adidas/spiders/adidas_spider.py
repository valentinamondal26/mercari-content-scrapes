import scrapy
import json
from datetime import datetime
from scrapy.exceptions import CloseSpider
import random


class AdidasSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from adidas.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Womens Sneakers
        2) Womens Athletic Shoes
        3) Men Athletic Shoes
        4) Jerseys

    brand : str
        category to be crawled, Supports
        1) Adidas

    Command e.g:
    scrapy crawl adidas -a category='Jerseys' -a brand='Adidas'
    """

    name = "adidas"

    base_url = 'https://www.adidas.com{}'
    listing_page_api_url = 'https://www.adidas.com/api/search/taxonomy?sitePath=us&query={query}&start={start}'
    query = None
    details_page_api_url = 'https://www.adidas.com/api/products/{}?sitePath=us'

    start_index = 0
    count = 0

    category = "Womens Sneakers"
    brand = "Adidas"
    source = 'adidas.com'
    blob_name = 'adidas.txt'

    # NOTE: Doesn't work with proxies, Api's are irresponsive
    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Womens Sneakers': {
            'Adidas': 'https://www.adidas.com/us/women-continental_80',
        },
        'Womens Athletic Shoes': {
            'Adidas': 'https://www.adidas.com/us/women-athletic_sneakers',
        },
        'Men Athletic Shoes': {
            'Adidas': 'https://www.adidas.com/us/men-athletic_sneakers-shoes',
        },
        'Jerseys': {
            'Adidas': 'https://www.adidas.com/us/men-jerseys',
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AdidasSpider,self).__init__(*args, **kwargs)

        if not category:
            self.logger.error("Category should not be None")
            raise CloseSpider('category not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.query = self.launch_url.rsplit('/', 1)[-1]
        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url + ',query=' + self.query)


    def create_request(self, url, callback):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url and self.query:
            url = self.listing_page_api_url.format(query=self.query, start=str(self.start_index))
            self.logger.info('url: {}'.format(url))
            yield self.create_request(url=url, callback=self.parse)
    

    def get_url(self,category,brand):
        try:
            return self.url_dict[category][brand]
        except Exception as e:
            print('Exception occured:', e)
            raise(ValueError('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))


    def parse(self, response):
        res = json.loads(response.text)
        items = res['itemList']['items']
        self.count = res['itemList']['count']
        view_size = res['itemList']['viewSize']

        for item in items:
            image = ""
            image = item['image']['src']
            product_id = item['productId']
            rating = item.get('rating', 0)
            product = {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'title': item['displayName'],
                'id': self.base_url.format(item['link']),
                'image': image,
                'product_line': item['subTitle'],
                'color': item['color'],
                'sku': product_id,
                'model': item['displayName'],
                'rating': str(rating) if rating is not None else '',
                'material': "",
            }
            yield self.parse_detail_page(self.details_page_api_url.format(product_id), product)

        self.start_index += view_size
        if self.start_index < self.count:
            url = self.listing_page_api_url.format(query=self.query, start=str(self.start_index))
            yield self.create_request(url=url, callback=self.parse)


    def parse_detail_page(self, url, item):
        self.logger.info('parse_detail_page: {}'.format(url))

        request = self.create_request(url, callback=self.crawl_detail_page)
        request.meta['item'] = item
        return request


    def crawl_detail_page(self, response):
        res = json.loads(response.text)
        item = response.meta['item']

        breadcrumbs = res['breadcrumb_list']
        bc = []
        for breadcrumb in breadcrumbs:
            bc.append(breadcrumb['text'])
        bc.append(item['title'])

        specs = []
        specifications = res['product_description']['usps']
        for specification in specifications:
            specs.append(specification)


        price = res['pricing_information']['standard_price']
        item['price'] = str(price) if price is not None else ''
        item['model_number'] = res['model_number']
        item['description'] = res['product_description']['text']
        item['breadcrumb'] = ' | '.join(bc)
        item['features'] = ','.join(specs)
        item['filter_brand'] = res.get('attribute_list', {}).get('brand', '')
        item['filter_category'] = res.get('attribute_list', {}).get('category', '')
        item['filter_color'] = res.get('attribute_list', {}).get('color', '')
        item['filter_sport'] = ', '.join(res.get('attribute_list', {}).get('sport', []))
        item['filter_productType'] = ', '.join(res.get('attribute_list', {}).get('productType', []))
        item['filter_sportSubSub'] = ', '.join(res.get('attribute_list', {}).get('sportSubSub', []))

        yield item

