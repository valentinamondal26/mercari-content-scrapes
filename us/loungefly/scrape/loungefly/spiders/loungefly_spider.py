'''
https://mercari.atlassian.net/browse/USCC-261 - Loungefly Backpacks
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class LoungeflySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from loungefly.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Backpacks
    
    brand : str
        brand to be crawled, Supports
        1) Loungefly

    Command e.g:
    scrapy crawl loungefly -a category='Backpacks' -a brand='Loungefly'
    """

    name = 'loungefly'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'loungefly.com'
    blob_name = 'loungefly.txt'

    base_url = 'https://www.loungefly.com'
    query_params = '?is_ajax=1&p={page}&is_scroll=1'

    url_dict = {
        'Backpacks': {
            'Loungefly': {
                'url': 'https://www.loungefly.com/bags/backpacks.html',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(LoungeflySpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.launch_url = 'https://www.loungefly.com/bags/backpacks.html'

    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.loungefly.com/collections/bags-backpacks/products/hello-kitty-pumpkin-spice-aop-mini-convertible-backpack
        @scrape_values id title image price sku description breadcrumb
        @meta {"use_proxy":"True"}
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//div[@class="product-name"]/h1/text()').extract_first(default='') \
                or response.xpath('//h1[@itemprop="name"]/text()').extract_first(),
            'price': response.xpath('//div[@class="item-column"]//span[@class="regular-price"]/span[@class="price"]/text()').extract_first(default='') \
                or response.xpath(u'normalize-space(//span[@itemprop="price"]/text())').extract_first(),
            'sku': response.xpath('//p[@class="sku-product"]/span/text()').extract_first(default='') \
                or response.xpath('//div[@class="product-single-sku"]/span/text()').extract_first(),
            'description': ''.join(response.xpath('//div[@class="short-description"]/div[@class="std"]/text()[normalize-space(.)]').extract()) \
                or ''.join(response.xpath('//div[@id="product_tabs_description_contents"]//div[@class="std"]/text()[normalize-space(.)]').extract()) \
                or '\n'.join(response.xpath('//div[@itemprop="description"]//p//text()').extract()),
            'breadcrumb': ' | '.join(response.xpath('//div[@class="breadcrumbs row-fluid"]//li//text()[normalize-space(.)]').extract()) \
                or '|'.join(response.xpath('//div[@class="_breadcrumbs"]//text()[normalize-space(.)]').extract()),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
        }


    def parse(self, response):
        """
        @url https://www.loungefly.com/bags/backpacks.html
        @returns requests 1 10
        @meta {"use_proxy":"True"}
        """

        total = response.xpath('//div[@id="am-pager-count"]/text()').extract_first() \
            or response.xpath('//div[@id="bc-sf-filter-bottom-pagination"]//span')[-2].xpath('.//text()').extract_first()
        if total:
            for i in range(1, int(total)+1):
                yield self.create_request(url=self.launch_url+self.query_params.format(page=i),
                    callback=self.parse_listing_page)


    def parse_listing_page(self, response):
        """
        @url https://www.loungefly.com/bags/backpacks.html?is_ajax=1&p=1&is_scroll=1
        @returns requests 1 20
        @meta {"use_proxy":"True"}
        """

        for link in response.xpath('//div[@class="col-main"]//div[@class="item-inner content"]//h3[@class="product-name"]/a/@href').extract() \
            or response.xpath('//main[@id="MainContent"]//div[contains(@class,"grid__item")]//div[@class="product-card__info"]//a/@href').extract():
            if link:
                if not link.startswith(self.base_url):
                    link = self.base_url + link
                yield self.create_request(url=link, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
