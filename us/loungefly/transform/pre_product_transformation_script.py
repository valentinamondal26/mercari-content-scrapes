import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(
                r'New|exclusive|Backpack(s){0,1}|Loungefly|^x\b', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'^x\b', re.IGNORECASE),
                           '', model.strip())
            model = model.title()

            d = {
                'Dc': 'DC',
                'Lf': 'LF',
                'Pu': 'PU',
                'Aop': 'AOP',
                'Pokémon': 'Pokemon',
                'Bob Fett': 'Boba Fett',
                'Bb-8': 'BB-8'
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)
            model = re.sub(re.compile(r'(\d+)(Th)\b|(\d+)(St)\b|(\d+)(nd)\b|(\d+)(rd)\b',
                                      re.IGNORECASE), lambda x: x.group().lower(), model)
            model = re.sub(re.compile(
                r"\bLoungelfy X\b", re.IGNORECASE), "", model)

            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            height = ''
            width = ''
            depth = ''

            description = record.get('description', '').replace(
                '\r', ' ').replace('\n', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            match = re.findall(re.compile(
                r'W: (\d+)(.\d+){0,1}" X H: (\d+)(.\d+){0,1}" X D: (\d+)(.\d+){0,1}"', re.IGNORECASE), description)
            if match:
                group = match[0]
                width = group[0] + group[1]
                height = group[2] + group[3]
                depth = group[4] + group[5]

            match = re.findall(re.compile(
                r'THIS(.*?)BACKPACK', re.IGNORECASE), description)
            if not match:
                match = re.findall(re.compile(
                    r'THIS(.*?)BACK PACK', re.IGNORECASE), description)
            if not match:
                match = re.findall(re.compile(
                    r'(.*?)BACKPACK', re.IGNORECASE), description)
            if not match:
                match = re.findall(re.compile(
                    r'(.*?)BACK PACK', re.IGNORECASE), description)
            if not match:
                match = re.findall(re.compile(
                    r'THIS(.*?)BAG', re.IGNORECASE), description)
                if match:
                    material = match[-1]
            if match:
                material = match[0]
                if 'this' in material.lower():
                    match = re.findall(re.compile(
                        r'THIS(.*?)$', re.IGNORECASE), material)
                    if match:
                        material = match[0]
                if 'that is' in material.lower():
                    match = re.findall(re.compile(
                        r'THAT IS(.*?)BACKPACK', re.IGNORECASE), description)
                    if match:
                        material = match[0]

            material = re.sub(r'\s+', ' ', material).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),

                "description": description,
                "model": model,
                "material": material,
                "width": width,
                'height': height,
                'depth': depth,
                'sku': record.get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
