'''
https://mercari.atlassian.net/browse/USCC-685 - Nurse Mates Women's Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re


class NursematesSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from nursemates.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Shoes
    
    brand : str
        brand to be crawled, Supports
        1) Nurse Mates

    Command e.g:
    scrapy crawl nursemates -a category="Women's Shoes" -a brand="Nurse Mates"
    """

    name = 'nursemates'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'nursemates.com'
    blob_name = 'nursemates.txt'

    base_url = 'https://www.nursemates.com'

    url_dict = {
        "Women's Shoes": {
            "Nurse Mates": {
                'url': 'https://www.nursemates.com/ShopViewAll.aspx?CategoryID=0&GenderID=8&Size=0&Width=Z&Color=0&PageNumber=1&PageType=R&SortType=P&categorytype=shoes',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(NursematesSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                url = re.sub(r'PageNumber=\d+', 'PageNumber=1000', url, flags=re.IGNORECASE)
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.nursemates.com/Product.aspx?ProductID=19932
        @meta {"use_proxy":"True"}
        @scrape_values id image title sku price selected_color description features
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//div[@id="productcategory"]/p//text()').getall(),
            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'title': response.xpath('//div[@id="titles"]/h3/text()').get(),
            'sku': response.xpath('//div[@id="style-number"]/@data-style-number').get(),
            'price': response.xpath('//div[@id="style-number"]/p/span/text()').get(),
            'selected_color': response.xpath('//div[@id="style-number"]/p/em/text()').get(),
            'sizes': response.xpath('//div[@id="size"]/select[@id="size-select"]/option/text()').getall(),
            'description': response.xpath('//div[@class="details info-wrp"]/p/text()').get(),
            'features': response.xpath('//div[@class="details info-wrp"]/ul/li/text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.nursemates.com/ShopViewAll.aspx?CategoryID=0&GenderID=8&Size=0&Width=Z&Color=0&PageNumber=1000&PageType=R&SortType=P&categorytype=shoes
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@id="grid"]/ul/li'):
            product_url = product.xpath('./a/@href').extract_first()
            if product_url:
                product_url = f'{self.base_url}/{product_url}'
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
