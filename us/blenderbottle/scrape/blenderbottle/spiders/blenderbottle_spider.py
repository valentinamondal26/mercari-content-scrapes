'''
https://mercari.atlassian.net/browse/USCC-673 - BlenderBottle Fitness Accessories
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
from common.selenium_middleware.http import SeleniumRequest


class BlenderbottleSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from blenderbottle.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Fitness Accessories
    
    brand : str
        brand to be crawled, Supports
        1) BlenderBottle

    Command e.g:
    scrapy crawl blenderbottle -a category='Fitness Accessories' -a brand='BlenderBottle'
    """

    name = 'blenderbottle'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'blenderbottle.com'
    blob_name = 'blenderbottle.txt'

    base_url = 'https://www.blenderbottle.com'

    url_dict = {
        'Fitness Accessories': {
            'BlenderBottle': {
                'url': 'https://www.blenderbottle.com/collections/all-products',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BlenderbottleSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_dynamic_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.blenderbottle.com/products/classic
        @meta {"use_proxy":"True", "use_selenium":"True"}
        @scrape_values id color image title price
        """

        title = response.xpath('//h1[@class="header__title"]/text()').get()
        price = response.xpath('//span[@class="addcart__price"]/text()').get()
        size = response.xpath('//div[@class="size"]/select/option/text()').getall()

        details = {}
        for detail in response.xpath('//section[@class="product-details"]/div[@class="product-details__inner"]/div[contains(@class, "product-details__section sections-3")]'):
            key = detail.xpath('./div[@class="content-wrapper"]/div[@class="product-details__subject"]/text()').get()
            value = detail.xpath('./div[@class="content-wrapper"]/div[@class="product-details__content"]//text()').extract()
            if key:
                details[key] = value


        for variant in response.xpath('//div[@class="addcart__colors"]/div[@class="addcart__colors-content"]/label'):
            color = variant.xpath('./input/@value').get()
            image = variant.xpath('./img/@src').get()
            variant_id = variant.xpath('./input/@name').get().replace('addcart__color-item--', '').strip()
            url = response.url + f'?variant={variant_id}'

            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': url,

                'color': color,
                'image': image,
                'title': title,
                'price': price,
                'size': size,
                'details': details,
            }


    def parse(self, response):
        """
        @url https://www.blenderbottle.com/collections/all-products
        @meta {"use_proxy":"True", "use_selenium":"True"}
        @returns requests 1
        """

        for product in response.xpath('//div[@class="collection__container"]/div[@class="collection__items"]/a[@class="collection-card"]'):
            product_url = product.xpath('./@href').get()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_dynamic_request(url=product_url, callback=self.crawlDetail)


    def create_dynamic_request(self, url, callback, meta=None):
        request = SeleniumRequest(url=url, callback=callback, meta=meta, wait_time=20)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
