import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            def pipe_join_tokens(tokens):
                return r'|'.join(list(map(lambda x: r'\b'+x+r'\b', tokens)))

            model = record.get('title', '')
            model = re.sub(r'®|™|\s-\s|BlenderBottle', ' ', model, flags=re.IGNORECASE)
            model = re.sub(r'\bSleeves\b', 'Sleeve', model, flags=re.IGNORECASE)
            exception_words = ['Bag', 'Brush', 'Expansion', 'Replacement', 'Bottle', 'Sleeve']
            if not re.findall(pipe_join_tokens(exception_words), model, flags=re.IGNORECASE):
                model = f'{model} Bottle'
            model = re.sub(r'\s+', ' ', model).strip()

            color = record.get('color', '')
            color = re.sub(r'\bBlenderBottle Remix\b', '', color, flags=re.IGNORECASE)
            color = re.sub(r'Multi-Colored', 'Multicolor', color, flags=re.IGNORECASE)
            color = '/Multicolor'.join(list(map(lambda x: x.strip(), re.split(r'Multicolor', color, flags=re.IGNORECASE))))
            if re.findall(r'Arctic Special Edition Bottle', model, flags=re.IGNORECASE):
                model = re.sub(r'Bottle', f'{color} Bottle', model, flags=re.IGNORECASE)
                color = ''
            if re.findall(r'\bBatman\b', color, flags=re.IGNORECASE):
                model = re.sub('\bBatman\b', color, model, flags=re.IGNORECASE)
                model = re.sub('\bDC Comics\b', f'DC Comics {color}', model, flags=re.IGNORECASE)
                model = re.sub(r'80 Years 80th Anniversary', '80 Years', model, flags=re.IGNORECASE)
                color = ''
            if re.findall(r'\bMarvel\b', model, flags=re.IGNORECASE):
                words_color = re.split(r'\s-\s', color)
                if len(words_color) == 2:
                    model = model + words_color[0].strip()
                    color = words_color[1].strip()
                else:
                    model = model + ' ' + color
                    model = re.sub(r'\bIcon\b', ' ', model, flags=re.IGNORECASE)
                    d = {
                        'Black Panther': 'Black',
                        'Captain America': 'Black',
                        'Captain Marvel': 'White',
                        'Hulk': 'Black',
                        'Iron Man Icon': 'White',
                        'Iron Man': 'Black',
                        'Spider-Man Icon': 'White',
                        'Spider-Man': 'White',
                        'Thor': 'Black',
                    }
                    color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', d.keys()))), lambda x: d.get(x.group()), color, flags=re.IGNORECASE)

            if re.findall(r'Marvel Pro Series', model, flags=re.IGNORECASE):
                model = re.sub(r'Marvel Pro', f'Marvel {color} Pro', model, flags=re.IGNORECASE)
                color = ''
            
            if re.findall(r'DC Comics', model, flags=re.IGNORECASE):
                model = re.sub(r'DC Comics', f'DC Comics {color}', model, flags=re.IGNORECASE)
                color = ''

            colors_tobe_moved_to_model = [
                "Beast Mode", "Christmas Sweater", "Cupcake", "Dark Mark", "Darth Vader - Retro",
                "Darth Vader Suit", "Darth Vader", "Deathly Hallows - Gold", "Deathly Hallows",
                "Dementor", "Do You Even Lift?", "Dobby", "Donut Ever Give Up", "Dumbledore's Army",
                "Eggnog", "Empire Icon", "Expecto Patronum", "Free Spirit", "Frosty", "Gryffindor",
                "Jellyfish - Coral", "Jellyfish", "Kale Yeah", "Leia - Rebel",
                "Let's Taco 'Bout Fitness", "Lightspeed", "Luke-Leia", "Mando", "Mistletoned",
                "Platform 9 3/4", "Quidditch", "R2-D2", "Ravenclaw","Rebel - Black", "Rebel Icon",
                "Rebel Red Leader", "Slytherin", "Star Wars - Trench", "Star Wars Logo",
                "Stormtrooper", "Stronger Than You Think", "Superman - Navy", "The Child",
                "The Flash - Red", "The Mandalorian", "This Is the Way", "Will Work for Icecream",
                "Woke up on the Dark Side", "Yoda - Do or Do Not", "Yule Ball", "Hufflepuff",
                "I Solemnly Swear", "Icons", "Imperial Squadron", "Hedwig", "Hogwarts Crest",
                "Avocardio", "Aw Snap", "Back to Hogwarts",
            ]
            match = re.findall(pipe_join_tokens(colors_tobe_moved_to_model), color, flags=re.IGNORECASE)
            if match:
                actual_color = ''
                if re.findall(r'\s-\s', color):
                    c = ['Gold', 'Coral', 'Black', 'Navy']
                    colors = re.split(r'\s-\s', color)
                    color_match = re.findall(pipe_join_tokens(c), colors[-1], flags=re.IGNORECASE)
                    if color_match:
                        color = colors[0]
                        actual_color = color_match[0]
                color_match_words = re.findall(pipe_join_tokens(re.split(r'\s', color)), model, flags=re.IGNORECASE)
                if color_match_words:
                    color = re.sub(pipe_join_tokens(color_match_words), '', color, flags=re.IGNORECASE)
                model_keywords = ['Classic Bottle',  'Pro', 'Strada Insulated Stainless Steel Bottle',
                    'Radian Stainless Steel Bottle', 'Bottle', ]
                match = re.search(pipe_join_tokens(model_keywords), model, flags=re.IGNORECASE)
                if match:
                    def replace_span(old, new, span):
                        return old[:span[0]] + new + old[span[1]:]
                    model = replace_span(model, f'{color} {match.group()}', match.span())
                color = actual_color if actual_color else ''

            model = re.sub(r'Luke-Leia', 'Luke & Leia', model, flags=re.IGNORECASE)
            model = re.sub(r'Leia - Rebel', 'Rebel Leia', model, flags=re.IGNORECASE)

            if re.findall(r'^Harry Potter Bottle$', model, flags=re.IGNORECASE):
                if re.findall(r'^Harry Potter$', color, flags=re.IGNORECASE):
                    color = 'White/Gold'
                elif re.findall(r'^Harry Potter - Icons$', color, flags=re.IGNORECASE):
                    color = 'Clear'
            elif re.findall(r'^Harry Potter Radian Stainless Steel Bottle$', model, flags=re.IGNORECASE) and \
                re.findall(r'^Harry Potter$', color, flags=re.IGNORECASE):
                color = 'White'
            elif re.findall(r'\(Sleeve Only\)', model, flags=re.IGNORECASE):
                model = re.sub(r'\(Sleeve Only\)', '', model, flags=re.IGNORECASE)
                match = re.findall(r'Harry Potter \'*(\w+)\'*', color, flags=re.IGNORECASE)
                if match:
                    model = re.sub(r'Harry Potter', f'Harry Potter {match[0]}', model, flags=re.IGNORECASE)
                color = ''


            color = re.sub(r'\s-\s|^-', ' ', color)
            color = re.sub(r'Batman - Black', 'Orange/Black', color, flags=re.IGNORECASE)
            color = re.sub(r'\sand\s', '/', color, flags=re.IGNORECASE)
            color = re.sub(r'\s+', ' ', color).strip()

            model = re.sub(r'\s-\s', ' ', model)
            model = re.sub(r'(.*)(Bottle)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(r'Subscription|Gift Card|Color of The Month|T-Shirt', model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
