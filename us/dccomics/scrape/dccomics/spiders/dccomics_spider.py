'''
https://mercari.atlassian.net/browse/USCC-254 - DC Comics Comic Books
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json

from urllib.parse import urlparse, parse_qs, urlencode


class DccomicsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from dccomics.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Comic Books

    brand : str
        brand to be crawled, Supports
        1) DC Comics

    Command e.g:
    scrapy crawl dccomics -a category='Comic Books' -a brand='DC Comics'
    """

    name = 'dccomics'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'dccomics.com'
    blob_name = 'dccomics.txt'

    url_dict = {
        'Comic Books': {
            'DC Comics': {
                'url': 'https://www.dccomics.com/comics?all#browse',
                'listing_page_api_url': 'https://www.dccomics.com/proxy/search?page=1&type=comic%7Cgraphic_novel',
                'facet_url': 'https://www.dccomics.com/proxy/filters?module=comics',
                'filters': ['characters'],
            }
        }
    }

    base_url = 'https://www.dccomics.com'

    filter_list = []

    extra_item_infos = {}

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(DccomicsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        self.facet_url = self.url_dict.get(category, {}).get(brand, {}).get('facet_url', [])
        self.listing_page_api_url = self.url_dict.get(category, {}).get(brand, {}).get('listing_page_api_url', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.facet_url = 'https://www.dccomics.com/proxy/filters?module=comics'
        self.listing_page_api_url = 'https://www.dccomics.com/proxy/search?page=1&type=comic%7Cgraphic_novel'
        self.filter_list = ['characters']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.facet_url, callback=self.parse_filter)
            yield self.create_request(url=self.listing_page_api_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.dccomics.com/comics/action-comics-1938/action-comics-818
        @meta {"use_proxy":"True"}
        @scrape_values title price decription talent spec.Series:\s  spec.On\sSale\sDate:\s spec.Volume/Issue\s#:\s spec.Color/B&W:\s spec.Trim\sSize:\s
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//div[@class="panel-pane pane-node-title page-title"]/div[@class="pane-content"]/h1/text()').extract_first(),
            'price': response.xpath('//span[@class="price"]/text()').extract_first(),
            'decription': response.xpath('//div[@property="schema:summary schema:description content:encoded"]/p/text()').extract_first(),
            'talent': {a.xpath('./span[@class="views-label"]/text()').extract_first(): a.xpath('./span[@class="talent-row"]/a/text()').extract() for a in response.xpath('//div[@class="pane-content"]/div[contains(@class, "talent-list")]//div[contains(@class,"views-row")]/div[contains(@class,"views-field")]')},
            'spec': {a.xpath('./span[contains(@class,"views-label")]/text()').extract_first(): a.xpath('./div[@class="field-content"]//text()').extract_first() for a in response.xpath('//div[@class="pane-content"]/div[contains(@class, "spec-list")]//div[contains(@class,"views-row")]/div[contains(@class,"views-field")]')},
        }


    def parse_filter(self, response):
        """
        @url https://www.dccomics.com/proxy/filters?module=comics
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for _filter, values in json.loads(response.text).items():
            if _filter in self.filter_list:
                # print(_filter, [(value['nid'], value['title']) for value in values])
                filter_name = _filter
                for value in values:
                    filter_value = value.get('title', '')
                    nid = value.get('nid', '')
                    url = self.listing_page_api_url+ '&characters={nid}'.format(nid=nid)
                    if url:
                        request = self.create_request(url=url, callback=self.parse)
                        request.meta['extra_item_info'] = {
                                filter_name: filter_value,
                            }
                        yield request


    def parse(self, response):
        """
        @url https://www.dccomics.com/proxy/search?page=1&type=comic%7Cgraphic_novel
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        extra_item_info = response.meta.get('extra_item_info', {})

        data = json.loads(response.text)
        if data.get('result count', 0) > 0 and data.get('results', {}):
            for product in data.get('results', {}).values():

                id = product.get('fields', {}).get('url', '')

                if id:
                    if not self.extra_item_infos.get(id, None):
                        self.extra_item_infos[id] = {}

                    self.extra_item_infos.get(id).update(extra_item_info)

                    request = self.create_request(url=self.base_url + id, callback=self.crawlDetail)
                    yield request

            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            print(query_dict)
            current_page = int(query_dict.get('page', ['0'])[0])
            print(current_page)
            query_dict.update({'page':[current_page + 1]})
            url = parts._replace(query=urlencode(query_dict, True)).geturl()
            request = self.create_request(url=url, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

