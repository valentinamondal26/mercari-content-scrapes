import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '') or record.get('spec', {}).get('U.S. Price: ', '') or ''
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            # model = re.sub(re.compile(r'#\d+', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\bVol\.', re.IGNORECASE), 'Volume', model)
            model = re.sub(r"#", "Volume ", model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r'Preview|Sample|Sampler|Digital', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),

                "author": ','.join(record.get('talent', {}).get('Written by:', [])),
                "artist": ','.join(record.get('talent', {}).get('Art by:', [])),
                "cover_artist": ','.join(record.get('talent', {}).get('Cover by:', [])),
                "series": record.get('spec', {}).get('Series: ', ''),
                "date_of_publication": record.get('spec', {}).get('On Sale Date: ', ''),
                "volume": record.get('spec', {}).get('Volume/Issue #: ', ''),
                "rating": record.get('spec', {}).get('Rated: ', ''),
                "character": record.get('characters', ''),
                "color": record.get('spec', {}).get('Color/B&W: ', ''),
                "page_count": record.get('spec', {}).get('Page Count: ', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
