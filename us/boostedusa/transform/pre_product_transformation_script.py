import hashlib
import re
from colour import Color
class Mapper(object):

    def map(self, record):
        if record:
            description = ' '.join(record.get('description', [])).replace('\n', '').strip()
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = record.get('color', '')

            model = record.get('title', '')
            model = re.sub(r'\bBoosted\b', '', model)
            model = re.sub(r'^\s*-\s', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            model_words = model.split()
            colors = []
            secondary_color = ['Clear']
            
            for word in model_words:
                try:
                    Color(word)
                    model = re.sub(r'\b{word}\b'.format(word=word), '', model, flags=re.IGNORECASE).strip()
                    colors.append(word)
                except ValueError:
                    pass
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', secondary_color))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\b{col}\b'.format(col=color), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\-$', '', model)
            model = re.sub(r'\s*\/\s*', '/', model)
            model = re.sub(r'^\/', '', model).strip()
            model = re.sub(r'\s+', ' ', model).strip()
            
            colors = '/'.join(colors)
            if not color:
                color = colors
    
            product_type = ''
            if record.get('product_type', '') == 'Electric Skateboards':
                product_type = 'Skateboard'
            elif record.get('product_type', '') == 'Electric Scooters':
                product_type = 'Scooter'
            else:
                product_type = 'Accessory'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "product_type": product_type,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
