'''
https://mercari.atlassian.net/browse/USCC-729 - Boosted Electric Scooters
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import copy


class BoostedusaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from boostedusa.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Electric Scooters
    
    brand : str
        brand to be crawled, Supports
        1) Boosted

    Command e.g:
    scrapy crawl boostedusa -a category="Electric Scooters" -a brand="Boosted"
    """

    name = 'boostedusa'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'boostedusa.com'
    blob_name = 'boostedusa.txt'

    base_url = 'https://boostedusa.com'

    url_dict = {
        "Electric Scooters": {
            "Boosted": {
                'url': [
                    'https://boostedusa.com/collections/electric-skateboards',
                    'https://boostedusa.com/products/boosted-rev',
                    'https://boostedusa.com/collections/accessories',
                ]
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BoostedusaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                if '/products/' in url:
                    yield self.create_request(url=url, callback=self.crawlDetail)
                else:
                    yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://boostedusa.com/products/boosted-rev
        @meta {"use_proxy":"True"}
        @scrape_values id image title price product_type description
        """

        color_variants = response.xpath('//div[@class="select-container"]/label[@class="label" and text()="Color"]/..//select[@name="id"]/option')

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'title': response.xpath('//h1[@class="product_name title"]/text()').get(),
            'price': response.xpath('//p[@class="modal_price subtitle"]//span[@class="money"]/text()').get(),
            'product_type': response.xpath('//p[@class="product__collections-list tags"]//a/text()').get(),
            'description': response.xpath('//div[@class="description content has-padding-top"]//text()').getall(),
        }

        if color_variants:
            for variant_info in color_variants:
                variant = copy.deepcopy(item)
                variant.update({
                    'id' : response.url + '?variant=' + variant_info.xpath('./@value').get(),
                    'color': variant_info.xpath('./text()').get(),

                })
                yield variant
        else:
            yield item


    def parse(self, response):
        """
        @url https://boostedusa.com/collections/accessories
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="container collection-matrix"]/div'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//a[@class="pagination-next"]/@href').extract_first()
        if next_page_link:
            next_page_link = self.base_url + next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
