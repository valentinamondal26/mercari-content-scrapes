'''
-Microsoft:
https://mercari.atlassian.net/browse/USCC-370 - Xbox One Games https://myvideogamelist.com/platform/Xbox_One
https://mercari.atlassian.net/browse/USCC-270 - Xbox 360 Games https://myvideogamelist.com/platform/Xbox_360
https://mercari.atlassian.net/browse/USCC-268 - Xbox Games https://myvideogamelist.com/platform/Xbox

-Sony:
https://mercari.atlassian.net/browse/USCC-252 - Playstation 3 Games https://myvideogamelist.com/platform/Playstation_3
https://mercari.atlassian.net/browse/USCC-250 - Playstation 4 Games https://myvideogamelist.com/platform/Playstation_4
https://mercari.atlassian.net/browse/USCC-249 - Sony PSP Games https://myvideogamelist.com/platform/Sony_PSP
https://mercari.atlassian.net/browse/USCC-246 - Playstation 1 Games https://myvideogamelist.com/platform/Playstation_1
https://mercari.atlassian.net/browse/USCC-244 - Playstation 2 Games https://myvideogamelist.com/platform/Playstation_2
https://mercari.atlassian.net/browse/USCC-230 - Sony PS Vita Games https://myvideogamelist.com/platform/Sony_PS_Vita

-Sega:
https://mercari.atlassian.net/browse/USCC-148 - Sega Saturn Games https://myvideogamelist.com/platform/Sega_Saturn
https://mercari.atlassian.net/browse/USCC-147 - Sega Master System Games https://myvideogamelist.com/platform/Sega_Master_System
https://mercari.atlassian.net/browse/USCC-127 - Sega Game Gear Games https://myvideogamelist.com/platform/Sega_Game_Gear
https://mercari.atlassian.net/browse/USCC-124 - Sega Dreamcast Games https://myvideogamelist.com/platform/Sega_Dreamcast
https://mercari.atlassian.net/browse/USCC-122 - Sega Genesis/MegaDrive Games https://myvideogamelist.com/platform/Sega_Genesis/MegaDrive
https://mercari.atlassian.net/browse/USCC-104 - Sega CD Games https://myvideogamelist.com/platform/Sega_CD
https://mercari.atlassian.net/browse/USCC-102 - Sega 32X Games https://myvideogamelist.com/platform/Sega_32X

-Nintendo:
https://mercari.atlassian.net/browse/USCC-221 - Nintendo Wii U Games https://myvideogamelist.com/platform/Nintendo_Wii_U
https://mercari.atlassian.net/browse/USCC-220 - Nintendo Wii Games https://myvideogamelist.com/platform/Nintendo_Wii
https://mercari.atlassian.net/browse/USCC-212 - Nintendo DS Games https://myvideogamelist.com/platform/Nintendo_DS
https://mercari.atlassian.net/browse/USCC-209 - Nintendo 3DS Games https://myvideogamelist.com/platform/Nintendo_3DS
https://mercari.atlassian.net/browse/USCC-165 - Nintendo Switch Games https://myvideogamelist.com/platform/Nintendo_Switch
https://mercari.atlassian.net/browse/USCC-396 - Nintendo Gamecube Games https://myvideogamelist.com/platform/Nintendo_Gamecube
https://mercari.atlassian.net/browse/USCC-394 - Nintendo Gameboy Advance Games https://myvideogamelist.com/platform/Nintendo_Gameboy_Advance
https://mercari.atlassian.net/browse/USCC-393 - Nintendo Gameboy Games https://myvideogamelist.com/platform/Nintendo_Gameboy
https://mercari.atlassian.net/browse/USCC-86 - Nintendo Super NES Games https://myvideogamelist.com/platform/Nintendo_Super_NES
https://mercari.atlassian.net/browse/USCC-85 - Nintendo NES Games https://myvideogamelist.com/platform/Nintendo_NES
https://mercari.atlassian.net/browse/USCC-84 - Nintendo 64 Games https://myvideogamelist.com/platform/Nintendo_64
https://mercari.atlassian.net/browse/USCC-82 - Nintendo Gameboy Color Games https://myvideogamelist.com/platform/Nintendo_Gameboy_Color
'''

# -*- coding: utf-8 -*-
import scrapy
import datetime
import json
import random
from collections import OrderedDict
import os
from scrapy.exceptions import CloseSpider


class MyvideogamelistSpider(scrapy.Spider):

    """
     spider to crawl items in a provided category and sub-category from myvideogamelist.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Video Game Consoles
     launch_url : str
     brand : str

     Command e.g:

     scrapy crawl myvideogamelist  -a category='Video Games' -a brand="Microsoft"
     scrapy crawl myvideogamelist  -a category='Video Games' -a brand="Nintendo"
     scrapy crawl myvideogamelist  -a category='Video Games' -a brand="Sega"
     scrapy crawl myvideogamelist  -a category='Video Games' -a brand="Sony"

    """

    name = 'myvideogamelist'

    category = None
    brand = None
    launch_url = None
    platform = None

    source = 'myvideogamelist.com'
    blob_name = 'myvideogamelist.txt'

    base_url = 'https://myvideogamelist.com'

    url_dict = {
        'Video Games' : {
            'Microsoft': {
                'url': [
                    'https://myvideogamelist.com/platform/Xbox_One',
                    'https://myvideogamelist.com/platform/Xbox_360',
                    'https://myvideogamelist.com/platform/Xbox',
                ]
            },
            'Nintendo': {
                'url': [
                    'https://myvideogamelist.com/platform/Nintendo_Wii_U',
                    'https://myvideogamelist.com/platform/Nintendo_Wii',
                    'https://myvideogamelist.com/platform/Nintendo_DS',
                    'https://myvideogamelist.com/platform/Nintendo_3DS',
                    'https://myvideogamelist.com/platform/Nintendo_Switch',
                    'https://myvideogamelist.com/platform/Nintendo_Gamecube',
                    'https://myvideogamelist.com/platform/Nintendo_Gameboy_Advance',
                    'https://myvideogamelist.com/platform/Nintendo_Gameboy',
                    'https://myvideogamelist.com/platform/Nintendo_Super_NES',
                    'https://myvideogamelist.com/platform/Nintendo_NES',
                    'https://myvideogamelist.com/platform/Nintendo_64',
                    'https://myvideogamelist.com/platform/Nintendo_Gameboy_Color',
                ]
            },
            'Sega': {
                'url': [
                    'https://myvideogamelist.com/platform/Sega_Saturn',
                    'https://myvideogamelist.com/platform/Sega_Master_System',
                    'https://myvideogamelist.com/platform/Sega_Game_Gear',
                    'https://myvideogamelist.com/platform/Sega_Dreamcast',
                    'https://myvideogamelist.com/platform/Sega_Genesis/MegaDrive',
                    'https://myvideogamelist.com/platform/Sega_CD',
                    'https://myvideogamelist.com/platform/Sega_32X',
                ]
            },
            'Sony': {
                'url': [
                    'https://myvideogamelist.com/platform/Playstation_3',
                    'https://myvideogamelist.com/platform/Playstation_4',
                    'https://myvideogamelist.com/platform/Sony_PSP',
                    'https://myvideogamelist.com/platform/Playstation_1',
                    'https://myvideogamelist.com/platform/Playstation_2',
                    'https://myvideogamelist.com/platform/Sony_PS_Vita',
                ]
            },
        }
    }

    proxy_pool = [
            'http://shp-mercari-us-d00001.tp-ns.com:80',
            'http://shp-mercari-us-d00002.tp-ns.com:80',
            'http://shp-mercari-us-d00003.tp-ns.com:80',
            'http://shp-mercari-us-d00004.tp-ns.com:80',
            'http://shp-mercari-us-d00005.tp-ns.com:80',
            'http://shp-mercari-us-d00006.tp-ns.com:80',
            'http://shp-mercari-us-d00007.tp-ns.com:80',
            'http://shp-mercari-us-d00008.tp-ns.com:80',
            'http://shp-mercari-us-d00009.tp-ns.com:80',
            'http://shp-mercari-us-d00010.tp-ns.com:80',
            'http://shp-mercari-us-d00011.tp-ns.com:80',
            'http://shp-mercari-us-d00012.tp-ns.com:80',
            'http://shp-mercari-us-d00013.tp-ns.com:80',
            'http://shp-mercari-us-d00014.tp-ns.com:80',
            'http://shp-mercari-us-d00015.tp-ns.com:80',
            'http://shp-mercari-us-d00016.tp-ns.com:80',
            'http://shp-mercari-us-d00017.tp-ns.com:80',
            'http://shp-mercari-us-d00018.tp-ns.com:80',
            'http://shp-mercari-us-d00019.tp-ns.com:80',
            'http://shp-mercari-us-d00020.tp-ns.com:80',
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(MyvideogamelistSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK', ''):
            return

        if not category and not brand :
            self.logger.error('category and brand should not be None')
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])
        if not self.launch_url:
            self.logger.error(f'Spider is not supported for category: {category} and brand: {brand}')
            raise(CloseSpider(f'Spider is not supported for category: {category} and brand: {brand}'))
        self.logger.info(f'Crawling for category: {self.category}, url: {self.launch_url}')


    def start_requests(self):
         for url in self.launch_url:
            yield self.create_request(url=url, callback=self.parse_sub_category, meta={'launch_url': url})


    def parse_sub_category(self, response):
        """
        @url https://myvideogamelist.com/platform/Playstation_4
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        directory = response.meta.get('launch_url', '').split('/platform/')[-1]
        product_links = response.xpath('//div[@class="media-content"]/a/@href').getall()
        for link in product_links:
            if link:
                meta = dict.update(response.meta)
                meta.update({'directory': directory})
                link = self.base_url + link
                yield self.create_request(url=link, callback=self.parse_product, meta=meta)

        next_page_url = response.xpath('//ul[@class="pagination"]/li[@class="active"]/following-sibling::li/a/@href').get()
        if next_page_url:
            next_page_url = self.base_url + next_page_url
            yield self.create_request(url=next_page_url, callback=self.parse_sub_category, meta={'launch_url': response.meta.get('launch_url', '')})


    def parse_product(self, response):
        """
        @url https://myvideogamelist.com/gameprofile/49325/Shin_Megami_Tensei_III:_Nocturne_HD_Remaster
        @scrape_values title source_url image description also_exists_on
        """

        item = {}
        title = response.css('h6.panel-title::text').get(default='')
        if '"' in title:
            description = response.xpath("//div[div[h6[contains(text(),"+"'{}'".format(title)+")]]]/div[@class="+'"'+"panel-body"+'"'+"]//p//text()").getall()
            description = ''.join(description)
            source = response.xpath("//div[div[h6[contains(text(),"+"'{}'".format(title)+")]]]/div[@class="+'"'+"panel-body"+'"'+"]//a/@href").get(default='')
            item['source_url'] = source

        else:
            description = response.xpath('//div[div[h6[contains(text(),"{}")]]]/div[@class="panel-body"]//p//text()'.format(title)).getall()
            description = ''.join(description)
            source = response.xpath('//div[div[h6[contains(text(),"{}")]]]/div[@class="panel-body"]//a/@href'.format(title)).get(default='')
            item['source_url'] = source

        image = response.xpath('//div[ div[h6[contains(text(),"Game Information")]]]//img/@src').get(default='')
        if image:
            image = self.base_url + image

        item['id'] = response.url
        item['crawlDate'] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item['statusCode'] = str(response.status)
        item['title'] = title
        item['description'] = description
        item['image'] = image
        item['category'] = self.category
        item['brand'] = self.brand

        dt = response.css('dl dt')
        dd = response.css('dl dd')
        specifications = OrderedDict()
        keys = []
        values = []
        for tag in dt:
            if tag.css('::text').get(default='') != '' and tag.css('::text').get(default='') != ' ':
                keys.append(tag.css('::text').get())
        for tag in dd:
                values.append(tag.css('::text').get(default=''))

        specifications = specifications.fromkeys(keys, '')
        ctr = 0
        for key, value in specifications.items():
            specifications[key] = values[ctr]
            ctr += 1
        self.platform = specifications.get('Platform', '')

        item.update(specifications)
        also = response.xpath('//p[contains(text(),"This game also exists on:")]/a')
        also_exists_on = []
        for a in also:
            also_exists_on.append(a.css('span::text').get(default=''))
        also_exists_on = ','.join(also_exists_on)
        item['also_exists_on'] = also_exists_on

        yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
