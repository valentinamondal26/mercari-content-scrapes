#Key Mapper for myvideogamelist data - model + platform combination is used to de-dup items
class Mapper:
    def map(self, record):
        model = ''
        platform = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "platform" == entity['name']:
                platform = entity["attribute"][0]["id"]

        key_field = model + platform
        return key_field
