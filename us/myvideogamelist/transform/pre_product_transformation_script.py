import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            record = self.pre_process(record)
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            platform=record.get('Platform','')
            title=record.get('title','')
            pattern=r"[o|O]{1}[n|N]{1}\s{1}%s"%platform
            if re.findall(pattern,title)!=[]:
                title=title.replace(re.findall(pattern,title)[0],"")
            title=' '.join(title.split())
            genre=record.get('Genre(s)','')
            genre_words=genre.split(',')
            genre_words=[words.strip() for words in genre_words]
            pattern=r"[a-zA-Z]+\s*Person Shooter"
            shooter_words=[re.findall(pattern,word)[0] for word in genre_words if re.findall(pattern,word)!=[]]
            genre_words=['Shooter' if word in shooter_words else word for word in genre_words]                
            genre_prioriy=['Horror','Educational','Sports','Racing','Party','Music/Rhythm','Puzzle','Shooter','Strategy','Sandbox','RPG','Arcade','Action','Adventure']
            index=[genre_prioriy.index(word) if word in genre_prioriy else -1 for word in genre_words]
            index.sort()
            position=[ind for ind in index if ind>0]
            if position!=[]:
                genre=genre_prioriy[position[0]]
            else:
                genre='Other'

        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "title":record.get('title',''),
                "model": title,
                "item_title":title,
                "description": record.get('description', ''),
                "platform":record.get('Platform',''),
                "ner_query": ner_query,
                "genre":genre,
                "release_date":record.get('NA Release Date',''),
                "esrb_rating":record.get('ESRB Rating',''),
                "pegi_rating":record.get('PEGI Rating',''),
                "publisher":record.get('Publisher(s)',''),
                "developers":record.get('Developer(s)','')

            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None


    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
