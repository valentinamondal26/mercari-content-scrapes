import configparser
import os

pwd = os.path.dirname(__file__)
config_file = os.path.join(pwd, 'config.ini')

config = configparser.ConfigParser()
config.read(config_file)


#config_sections
DEFAULTS = config['DEFAULTS']
GCLOUD_ENDPOINTS = config['GCLOUD_ENDPOINTS']
KG_ENPOINTS = config['KG_ENPOINTS']
KG_APIS = config['KG_APIS']

#kg_endpoints
KG_STAGE_ENDPOINT = KG_ENPOINTS['stage']
KG_CANARY_ENDPOINT = KG_ENPOINTS['canary']
KG_PROD_ENDPOINT = KG_ENPOINTS['prod']

#gcloud_endpoints
metadata_identity_doc_url = GCLOUD_ENDPOINTS['metadata_identity_doc_url']
project_id_url = GCLOUD_ENDPOINTS['project_id_url']




sp_a_metadata_tree_configuration_read_all_params =  {
    "db": "metadatadb",
    "fn": "sp_a_metadata_tree_configuration_read_all",
}

sp_metadata_tree_read_params = params = {
    "db": "metadatadb",
    "fn": "sp_metadata_tree_read",
    "metadataTreeConfigurationId": None,
}

