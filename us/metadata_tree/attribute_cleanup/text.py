import re
import string

class TextPreprocessor(object):
    def __init__(self, ignore_ch=None, **kwargs):
        if ignore_ch:
            self.punctuation = string.punctuation
            for ch in ignore_ch:
                self.punctuation = self.punctuation.replace(ch, "")
        else:
            self.punctuation = string.punctuation

        self.re_al_num = re.compile(r"([a-zA-Z]+)([0-9]+)", flags=re.UNICODE)
        self.re_num_al = re.compile(r"([0-9]+)([a-zA-Z]+)", flags=re.UNICODE)
        self.re_punct = re.compile(r"([%s])+" % re.escape(self.punctuation), re.UNICODE)
        self.re_punct_left = re.compile(r"([a-zA-Z0-9]+)([%s])" % re.escape(self.punctuation), flags=re.UNICODE)
        self.re_punct_right = re.compile(r"([%s])([a-zA-Z0-9]+)" % re.escape(self.punctuation), flags=re.UNICODE)

    def pipe(self, texts, **kwargs):
        """Apply pipeline to text.

        :param lower (bool=True):
        :param newline (bool=True):
        :param unicode (bool=True):
        :param punct (bool=True):
        :param split (bool=True):
        :param whitespace (bool=True):
        :param tokenize (bool=False):
        """
        if isinstance(texts, str):
            return self.preprocess_text(texts, **kwargs)

        return [self.preprocess_text(text, **kwargs) for text in texts]

    def split_alphanum(self, s):
        s = self.re_al_num.sub(r"\1 \2", s)
        s = self.re_num_al.sub(r"\1 \2", s)
        return s

    def strip_punctuation(self, s):
        s = self.re_punct.sub(" ", s)
        return s

    def split_punct(self, s):
        """Splits on punctuation."""
        s = self.re_punct_left.sub(r"\1 \2", s)
        s = self.re_punct_right.sub(r"\1 \2", s)
        return s

    def preprocess_text(
            self,
            text="",
            lower=True,
            newline=True,
            unicode=True,
            punct=True,
            split=True,
            whitespace=True,
            tokenize=False,
            **kwargs
    ):
        """Preprocessing Pipeline."""
        if text is None:
            return text
        if lower:
            text = text.lower()
        if newline:
            text = text.replace("\n", " ")
        if punct:
            text = self.strip_punctuation(text)
        if split:
            text = self.split_alphanum(text)
            text = self.split_punct(text)
        if tokenize:
            text = text.split()

        return text
