import logging
from typing import List, Tuple, Dict
import json

import requests
from attribute_cleanup import configs as cfg
from attribute_cleanup import utils

from text import TextPreprocessor

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def fetch_attributes(date: str) -> List:
    """
    Returns the attribute-entity list with the date of creation/updation >= the specified date.
    :param date: attribute-cleanup initiate date
    :return: attributeList
    """
    url = cfg.KG_STAGE_ENDPOINT + f"/fn?db=entitydb&fn=sp_newly_mapped_att_ent_and_syn&start_date={date}"
    response = invoke_api(url)
    response.raise_for_status()
    return response.json()


def invoke_api(url: str) -> requests.Response:
    """
    Invoke the passed url and return the API response
    :param url: API url
    :return: response object
    """
    bearer_token = 'Bearer {}'.format(utils.get_id_token_from_compute_engine(cfg.KG_STAGE_ENDPOINT))
    headers = {
        'Authorization': bearer_token,
        'Content-Type': 'application/json'
    }
    response = requests.get(url=url, headers=headers)
    return response


def create_null_id(value: str) -> str:
    """Create a 000 prefixed values for unnormalized values."""
    PREP = TextPreprocessor()
    tokens = PREP.preprocess_text(value, tokenize=True)
    null_id = f'000_{"_".join(tokens)}'
    return null_id


def create_orphan_ids(attributes_list: List) -> Tuple[List, List]:
    attributes = [j.get("attribute") for j in attributes_list]
    entities = [j.get("entityId") for j in attributes_list]
    orphanids = [create_null_id(at) for at in attributes]
    return orphanids, entities


def filter_skus(response: requests.Response):
    return [res.get("sku_id") for res in response.json()]


def filter_sku_details(response: requests.Response):
    return [[res.get("sku_id"), res.get("entity_attribute_list")] for res in response.json()]


def filter_tree_details(response: requests.Response):
    return [[res.get("metadataTreeConfigurationId"), res.get("name")] for res in response.json()]


def get_list_from_list(filter_list: List, params: Dict, fieldname: str, filter_func) -> List:
    """
    Get the list of attributes/skus mapped for the given list of attributes in batch of 10
    :param filter_list: List of orphan ids of the attributes
    :param params: Dict of params for the db service
    :param fieldname: field-name for the filter procedure
    :param filter_func: function to filter the details from the response
    :return: List of skus related to the attributes
    """
    number_of_iteration = round(len(filter_list) / 10)
    return_list = []
    index = 0
    url = cfg.KG_STAGE_ENDPOINT + "/fn"
    for i in range(number_of_iteration):
        if i == number_of_iteration - 1:
            param = filter_list[index: len(filter_list)]
        else:
            param = filter_list[index:index + 10]
        param = "{" + ",".join(['"' + value + '"' for value in param]) + "}"
        params.update({fieldname: param})
        bearer_token = 'Bearer {}'.format(utils.get_id_token_from_compute_engine(cfg.KG_STAGE_ENDPOINT))
        headers = {
            'Authorization': bearer_token,
            'Content-Type': 'application/json'
        }
        response = requests.get(url=url, params=params, headers=headers)
        response.raise_for_status()
        if response.json():
            # return_list.extend([res.get("sku_id") for res in response.json()])
            return_list.extend(filter_func(response))
        index = index + 10

    if params.get("fn") != 'sp_sku_attribute_read_by_attributes' and not return_list:
        logger.error(f"Empty for {params.get('fn')}")
        raise Exception
    return_list = list(set(return_list))

    return return_list


def get_affecting_skus(sku_details: List, entity_attribute_map: List) -> List:
    """
    Return the List of skus matching the entity-attribute pairs
    :param sku_details: List of all skus with sku details
    :param entity_attribute_map:  List of entity-attribute pairs to match
    :return: List of sku ids matched
    """
    affecting_skus = []
    for sku in sku_details:
        if not isinstance(sku[1], dict):
            sku[1] = json.loads(sku[1].replace("'", '"'))
        for pair in sku[1].get("entityAttributes"):
            if pair in entity_attribute_map:
                affecting_skus.append(sku[0])
    return list(set(affecting_skus))


def get_afftecing_tree_list(sdate: str):
    """
    :param sdate: attribute-cleanup date
    :return:
    :rtype:
    """
    # fetch the attribute list for the cleanup job
    attributes_list = fetch_attributes(sdate)
    if attributes_list:
        # create orphan ids from the attributeList
        orphans, entities = create_orphan_ids(attributes_list)
        orphans = list(set(orphans))
        entity_attribute_map = [{"entityId": entities[orphans.index(j)],
                                "attributeId": j} for j in orphans]
        # get the skus with the orphan attribute ids
        params = {
            "fn": "sp_sku_attribute_read_by_attributes",
            "db": "metadatadb"
        }
        sku_tree = get_list_from_list(orphans, params, "attribute_ids", filter_skus)

        if sku_tree:
            # get the sku details for those sku_id
            params = {
                "fn": "sp_sku_read_by_sku_list",
                "db": "metadatadb"
            }
            sku_details = get_list_from_list(sku_tree, params, "sku_ids", filter_sku_details)

            # get the affecting skus alone
            skus_affecting = get_affecting_skus(sku_details, entity_attribute_map)

            if skus_affecting:
                # get tree ids of the affecting skus
                params = {
                    "fn": "sp_sku_tree_read_by_sku_list",
                    "db": "metadatadb"
                }
                tree_details = get_list_from_list(skus_affecting, params, "sku_ids", filter_tree_details)
                return tree_details
            return []
    else:
        return []