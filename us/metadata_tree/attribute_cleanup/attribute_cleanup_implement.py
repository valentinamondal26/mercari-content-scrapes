import argparse
import logging
import json
import traceback
import requests
from datetime import datetime, timedelta
import google.auth
from google.cloud import bigquery
from google.cloud import storage
from requests.auth import HTTPBasicAuth
import pandas as pd
import pandas_gbq as gbq

import sys
attribute_cleanup_path = '/home/airflow/gcs/dags/attribute_cleanup/'
if attribute_cleanup_path not in sys.path:
    sys.path.append(attribute_cleanup_path)

from attribute_cleanup import configs as cfg
from attribute_cleanup import utils

gcp_dag_dir = "/home/airflow/gcs/"

logger = None
logger_start_time = None

###################################################

def attribute_cleanup(date, gcs=None):
    

    """
    process flow :
       initiate Attribute Cleanup flow for 7 days before the execution date
    """

    print('Initiating Attribute clean up......')   
    
    url = cfg.KG_STAGE_ENDPOINT +"/attribute/cleanup/initiate"
    cfg.attribute_cleanup_params["date"]=date
    try:
        bearer_token = 'Bearer {}'.format(utils.get_id_token_from_compute_engine(cfg.KG_STAGE_ENDPOINT))
        headers = {
            'Authorization': bearer_token,
            'Content-Type': 'application/json'
        }
        cleanup_response = requests.get(url, headers=headers, params=cfg.attribute_cleanup_params)

        if cleanup_response.json().get("success") is False:
            raise Exception("Error occured during initiation of attribute clean up")                                                       
    except Exception:
        raise Exception("Error occured during initiation of attribute clean up")

def main(date, gcs=None):
        attribute_cleanup(date)

###################################################################
### Program entry
###################################################################

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--date', required=True, help='pass the date from which the cleanup has to be triggered')
    parser.add_argument('--gcs', required=False, help='use resources  from  gcs')

    args = parser.parse_args()
    print("Starting Job...")
    print(args)

    try:
        main(args.gcs)
    except Exception as err:
        print(err)
        traceback.print_exc()
