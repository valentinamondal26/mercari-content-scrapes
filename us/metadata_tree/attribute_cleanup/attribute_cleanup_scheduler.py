import logging
from datetime import datetime, timedelta
import base64

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.slack_operator import SlackAPIPostOperator
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils import timezone
## Add implementation namespace here to give dag definition access to you implemenation
from attribute_cleanup.attribute_cleanup_implement import attribute_cleanup
from attribute_cleanup.list_affecting_trees import get_afftecing_tree_list
from common.etl_register import register_etl
from common.utils import get_gcp_bigquery_project_id, get_default_google_cloud_connection_id, \
    get_file_name, get_slack_token, get_slack_user_name
from airflow.models import Variable


log = logging.getLogger(__name__)
# Get GCP Project
project_id = get_gcp_bigquery_project_id()
gcp_connection_id = get_default_google_cloud_connection_id()
def get_start_date(date_string: str) -> datetime:
    """
    Returns the start date of the dag by formatting the date string to utc datetime
    :param date_string: date string
    :return: utc formatted datetime
    """
    start_date = datetime.strptime(date_string, '%Y-%m-%d')
    start_date = start_date.replace(tzinfo=timezone.utc)
    return start_date
    
default_args = {
    'owner': '@here',
    'email': ['v-suresh@mercari-partner.com'],
    'depends_on_past': False,
    'start_date': get_start_date('2021-02-27'),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    # 'retry_delay': timedelta(seconds=randint(10, 300)),
}
dag_id = get_file_name(__file__)
dag = DAG(
    dag_id,
    default_args=default_args,
    max_active_runs=1,
    schedule_interval='0 0 * * 0')

response = register_etl(
    etl_name=dag_id,
    git_repo_link="",
    composer_link="https://l0f7e35792e252b57-tp.appspot.com ",
    slack_alert_channel="#de-content-ops",
    owner="v-suresh@mercari-partner.com")


def get_date(execution_date, days):
    execution_date_pacific = execution_date - timedelta(days=days)
    dt = str(execution_date_pacific)
    dt = dt.split(".")[0].replace('T', ' ')[:19]
    print("EXECUTION DAY " + str(dt))
    the_day = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
    return the_day


def initiate_attribute_cleanup(ds, **kwargs):
    print("EXECUTION EXTRACTION ")
    print("EXECUTION START DATE {}".format(kwargs['execution_date']))
    dt = get_date(kwargs['execution_date'], 7)
    sdate = dt.strftime('%Y-%m-%d')
    print("EXECUTION DAY " + sdate)
    print("JOB DAY " + sdate)
    # initiate_attribute_cleanup()
    attribute_cleanup(sdate)


def get_afftecting_trees(ds, **kwargs):
    print("EXECUTION EXTRACTION ")
    print("EXECUTION START DATE {}".format(kwargs['execution_date']))
    dt = get_date(kwargs['execution_date'], 7)
    current_dt = get_date(kwargs['execution_date'],0)
    current_date = current_dt.strftime('%Y-%m-%d')
    sdate = dt.strftime('%Y-%m-%d')
    print("EXECUTION DAY " + sdate)
    print("JOB DAY " + sdate)
    # initiate_attribute_cleanup()
    tree_details = get_afftecing_tree_list(sdate)
    if tree_details:
        tree_names = [detail[1] for detail in tree_details]
        message = f"""
        Attribute Cleanup Job completed between: {sdate} - {current_date}
        List of Affecting trees: {tree_names}
        """
    else:
        message = f"""
        None of the trees are affected in Attribute Cleanup Job between: {sdate} - {current_date}
        """
    process_alert(message)


# Slack task for alerting
token = Variable.get('SLACK_TOKEN_V2')
channel = '#de-content-ops'
username = get_slack_user_name()


def process_alert(message):
    slack_task = SlackAPIPostOperator(
        task_id='alert_slack',
        token=token,
        channel=channel,
        username='airflow',
        text=message
    )
    return slack_task.execute()


def slack_failed_task(context):
    slack_task = SlackAPIPostOperator(
        task_id='alert_slack',
        token=token,
        channel=channel,
        username='airflow',
        text="""
            Task Failed. 
            *Task*: {task}  
            *Dag*: {dag} 
            *Execution Time*: {exec_date}  
            *Log Url*: {log_url} 
            """.format(
            task=context.get('task_instance').task_id,
            dag=context.get('task_instance').dag_id,
            exec_date=context.get('execution_date'),
            log_url=context.get('task_instance').log_url)
    )
    return slack_task.execute(context=context)


do_attribute_cleanup = PythonOperator(
    dag=dag,
    task_id='initiate_attribute_cleanup',
    python_callable=initiate_attribute_cleanup,
    trigger_rule=TriggerRule.ALL_DONE,
    on_failure_callback=slack_failed_task,
    provide_context=True
)

get_afftecing_trees = PythonOperator(
    dag=dag,
    task_id='get_affecting_trees',
    python_callable=get_afftecting_trees,
    trigger_rule=TriggerRule.ALL_DONE,
    on_failure_callback=slack_failed_task,
    provide_context=True
)
do_attribute_cleanup >> get_afftecing_trees
