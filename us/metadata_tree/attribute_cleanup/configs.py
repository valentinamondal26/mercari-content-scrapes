import configparser
import os

pwd = os.path.dirname(__file__)
config_file = os.path.join(pwd, 'configs.ini')

config = configparser.ConfigParser()
config.read(config_file)

#config_sections
DEFAULTS = config['DEFAULTS']
GCLOUD_ENDPOINTS = config['GCLOUD_ENDPOINTS']
KG_ENPOINTS = config['KG_ENPOINTS']

#kg_endpoints
KG_STAGE_ENDPOINT = KG_ENPOINTS['stage']
KG_CANARY_ENDPOINT = KG_ENPOINTS['canary']
KG_PROD_ENDPOINT = KG_ENPOINTS['prod']

#gcloud_endpoints
metadata_identity_doc_url = GCLOUD_ENDPOINTS['metadata_identity_doc_url']
project_id_url = GCLOUD_ENDPOINTS['project_id_url']




attribute_cleanup_params =  {
    "date": None
}