from datetime import datetime
import logging
import requests
import json
import base64
from google.auth.transport.requests import Request
import urllib



kg_service_domain = "https://kg-stage.endpoints.mercari-us-de.cloud.goog"
metadata_identity_doc_url = "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity"
project_id_url = "http://metadata/computeMetadata/v1/project/numeric-project-id"


def get_id_token_from_compute_engine():
    request = Request()
    headers = {"Metadata-Flavor": "Google"}
    project_id = request(project_id_url, method='GET', headers=headers)
    target_audience = project_id.data.decode('utf-8') + '-compute@developer.gserviceaccount.com'
    url = metadata_identity_doc_url + "?audience=" + target_audience

    resp = request(url, method='GET', headers=headers)
    return resp.data.decode('utf-8')

def backup_and_restore(project_id, execution_time,context):
    logging.info("Invoking backupAndRestore API")
    url = kg_service_domain + "/db/backupAndRestore"
    bearer_token = 'Bearer {}'.format(get_id_token_from_compute_engine())
    headers = {
        "Content-Type": "application/json",
        "Authorization": bearer_token
    }
    body = {
        "backupDatabase": "knowledge_v0",
        "restoreDatabase": "knowledge_v1",
        "bucket": "content_us",
        "project": project_id,
        "jobId": "job_"+ execution_time.strftime("%Y_%m_%d-%H_%M_%S")
    }
    try:
        backupAndRestore_response = requests.post(url, data=json.dumps(body), headers=headers)
        backupAndRestore_response.raise_for_status()
        logging.info("BackupAndRestore API  Response:{}".format(backupAndRestore_response.text))
    except (requests.ConnectionError, requests.Timeout) as e:
        logging.info("Connection or Timeout error occurred:{}".format(e))
    except requests.exceptions.HTTPError as e:
        logging.info("HTTPError:{}".format(e))
    else:
        response_dict = backupAndRestore_response.json()
        if "success" in response_dict and response_dict["success"] == "true":
            gcsPath = response_dict["path"]
            o=urllib.parse.urlsplit(gcsPath)
            path = o.path.split('/', 1)[-1]
            bucket_name = o.netloc
            context["ti"].xcom_push(key="gcs_path", value=path)
            context["ti"].xcom_push(key="bucket_name", value=bucket_name)        
    
def main(execution_time, project_id,context):
    
    backup_and_restore(project_id = project_id, execution_time = execution_time,context=context)