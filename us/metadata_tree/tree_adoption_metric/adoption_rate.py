

import logging
from datetime import datetime, timedelta
from random import randint
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.slack_operator import SlackAPIPostOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
## Add implementation namespace here to give dag definition access to you implemenation
from tree_adoption_metric.adoption_rate_implement import extract_and_merge_trees


from common.utils import get_gcp_bigquery_project_id, get_default_google_cloud_connection_id, \
    get_file_name, get_alerting_slack_channel, get_slack_user_name
from common.etl_register import register_etl
import base64
from airflow.models import Variable



log = logging.getLogger(__name__)
# Get GCP Project
project_id = get_gcp_bigquery_project_id()
gcp_connection_id = get_default_google_cloud_connection_id()





default_args = {
    'owner': '@here',
    'email': ['v-suresh@mercari-partner.com'],
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    # 'retry_delay': timedelta(seconds=randint(10, 300)),
}
dag_id = get_file_name(__file__)
dag = DAG(
    dag_id,
    default_args=default_args,
    max_active_runs=1,
    schedule_interval='30 12 * * *')



response = register_etl(
    etl_name=dag_id,
    git_repo_link="",
    composer_link="https://l0f7e35792e252b57-tp.appspot.com ",
    slack_alert_channel="#de-content-ops",
    owner="v-suresh@mercari-partner.com")


def get_date(execution_date,days):
    execution_date_pacific = execution_date - timedelta(days=days)
    dt = str(execution_date_pacific)
    dt = dt.split(".")[0].replace('T',' ')[:19]
    print("EXECUTION DAY " + str(dt))
    the_day = datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')
    return the_day


def import_trees(ds, **kwargs ):
    print("EXECUTION EXTRACTION ")
    print("EXECUTION START DATE {}".format( kwargs['execution_date']))
    dt = get_date(kwargs['execution_date'],0)
    sdate = dt.strftime('%Y-%m-%d')
    print("EXECUTION DAY " + sdate)
    print("JOB DAY " + sdate)
    extract_and_merge_trees()

# Slack task for alerting
token = Variable.get('SLACK_TOKEN_V2')
channel = '#de-content-ops'
username = get_slack_user_name()

def slack_failed_task(context):
    slack_task = SlackAPIPostOperator(
        task_id='alert_slack',
        token = token,
        channel=channel,
        username='airflow',
        text = """
            Task Failed. 
            *Task*: {task}  
            *Dag*: {dag} 
            *Execution Time*: {exec_date}  
            *Log Url*: {log_url} 
            """.format(
            task=context.get('task_instance').task_id,
            dag=context.get('task_instance').dag_id,
            exec_date=context.get('execution_date'),
            log_url=context.get('task_instance').log_url)
    )
    return slack_task.execute(context=context)



do_import_trees = PythonOperator(
    dag=dag,
    task_id='import_trees',
    python_callable=import_trees,
    trigger_rule=TriggerRule.ALL_DONE,
    # on_failure_callback=slack_failed_task,
    provide_context=True
)
