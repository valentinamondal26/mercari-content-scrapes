import argparse
import logging
import json
import traceback
import requests
from datetime import datetime, timedelta
import google.auth
from google.cloud import bigquery
from google.cloud import storage
from requests.auth import HTTPBasicAuth
import pandas as pd
import pandas_gbq as gbq

import sys
tree_adoption_metric_path = '/home/airflow/gcs/dags/tree_adoption_metric/'
if tree_adoption_metric_path not in sys.path:
    sys.path.append(tree_adoption_metric_path)

from tree_adoption_metric import configs as cfg
from tree_adoption_metric import utils

gcp_dag_dir = "/home/airflow/gcs/"

logger = None
logger_start_time = None

###################################################

def extract_and_merge_trees(gcs=None):
    

    """
    process flow :
        get existing metadata_tree_config from prod
        get category details from stage db using the l2_id of each config
        create a new dataframe using the response 
        category0_id, category1_id, category2_id, brand_name, category0_name, category1_name, category2_name
        [
    {
        "create_date": "2020-08-11 04:29:45.516845+00:00",
        "mercari_l0_active": true,
        "mercari_l0_id": 1,
        "mercari_l0_name": "Women",
        "mercari_l1_active": true,
        "mercari_l1_id": 21,
        "mercari_l1_name": "Women's handbags",
        "mercari_l2_active": true,
        "mercari_l2_id": 252,
        "mercari_l2_name": "Shoulder Bags",
        "modified_date": "2020-08-11 04:29:45.516845+00:00"
    }
]
        push to BQ using the DF built.
    """

    print('EXTRACTING Tree DATA......')   
    trees_details = {   'corrupt_trees' : [],
                        'published_trees' : [],
                        'adoption_details':[],
                    }

    url = cfg.KG_PROD_ENDPOINT +"/fn"
    try:
        bearer_token = 'Bearer {}'.format(utils.get_id_token_from_compute_engine(cfg.KG_PROD_ENDPOINT))
        headers = {
            'Authorization': bearer_token,
            'Content-Type': 'application/json'
        }
        tree_config_response = requests.get(url, headers=headers, params=cfg.sp_a_metadata_tree_configuration_read_all_params)

        if "Error" in str(tree_config_response) or not tree_config_response:
            raise Exception("Error occured during extraction of trees from postgres")

        trees_details["published_trees"] = tree_config_response.json()
        for tree in trees_details["published_trees"]: 
            tree_name = tree.get("name")
            print("tree:",tree_name)
            config = tree.get("config")
            if not config:
                print("config not present for tree_name:",tree_name)
                trees_details["corrupt_trees"].append(tree_name)
                continue
            tree_mappings = config.get("tree_mappings", None)

            if not tree_mappings:
                print("tree mappings not present for tree_name:",tree_name)
                trees_details["corrupt_trees"].append(tree_name)
                continue

            for mapping in tree_mappings:
                print("tree_mappings")
                categoryId = mapping.get("CategoryId", "")
                brand = mapping.get("Brand")
                    
                category_info_response = requests.get(url, headers=headers, params=utils.get_read_category_info_payload(categoryId))
                if "Error" in str(category_info_response.json()) or not category_info_response.json():
                    print("category_info not present for tree_name:",tree_name)
                    trees_details["corrupt_trees"].append(tree_name)
                    continue

                print("get_category_info::",category_info_response.json())
                category_info = category_info_response.json()[0]

                create_date = datetime.strptime(tree.get("create_date").split(".")[0], '%Y-%m-%d %H:%M:%S')
                modified_date = datetime.strptime(tree.get("modified_date").split(".")[0], '%Y-%m-%d %H:%M:%S')

                adoption_info = {}
                adoption_info.update({"category0_id":category_info.get("mercari_l0_id")})
                adoption_info.update({"category1_id":category_info.get("mercari_l1_id")})
                adoption_info.update({"category2_id":category_info.get("mercari_l2_id")})
                adoption_info.update({"category0_name":category_info.get("mercari_l0_name")})
                adoption_info.update({"category1_name":category_info.get("mercari_l1_name")})
                adoption_info.update({"category2_name":category_info.get("mercari_l2_name")})
                adoption_info.update({"brand_name":brand})
                adoption_info.update({"tree_name":tree_name}) 
                adoption_info.update({"create_date":create_date}) 
                adoption_info.update({"modified_date":modified_date})
                trees_details["adoption_details"].append(adoption_info)

        adoption_df = pd.DataFrame(trees_details["adoption_details"])
        print(f"Total data : { adoption_df.shape}")
        print(f"Data : { adoption_df}")
        print(f"Corrupted Trees : {trees_details['corrupt_trees']}")                                                        
    except Exception:
        raise Exception("Error occured during extraction of trees from postgres")

    tableName = 'item_data.tree_adoption_metric'
    is_adoption_df_empty = adoption_df.empty
    if not is_adoption_df_empty:
        try:
            gbq.to_gbq(
            adoption_df, tableName, project_id='mercari-us-de', if_exists='replace')
        except Exception as e:
            logging.error('exception raised while inserting data to table: ' + str(tableName) + str(e))
            raise
    

def main(gcs=None):
        extract_and_merge_trees()

###################################################################
### Program entry
###################################################################

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--gcs', required=False, help='use resources  from  gcs')

    args = parser.parse_args()
    print("Starting Job...")
    print(args)

    try:
        main(args.gcs)
    except Exception as err:
        print(err)
        traceback.print_exc()
