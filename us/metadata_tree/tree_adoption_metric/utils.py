from google.auth.transport.requests import Request
import configs as cfg
import json
import requests
import logging
import traceback


bearer_token = None


def get_id_token_from_compute_engine(audience):
    request = Request()
    headers = {"Metadata-Flavor": "Google"}
    url = cfg.metadata_identity_doc_url + "?audience=" + audience
    resp = request(url, method='GET', headers=headers)
    return resp.data.decode('utf-8')


def get_headers_with_bearer_token(audience):
    global bearer_token
    if bearer_token is None:
        bearer_token = 'Bearer {}'.format(get_id_token_from_compute_engine(audience))
    headers = {
        'Authorization': bearer_token,
        'Content-Type': 'application/json'
    }
    return headers


def send_http_request_and_validate_response(audience, url, method, params=None, data=None, headers=None):
    """
    makes a http request and raise expection in case response is not 200*

    :param url: url to hit
    :param headers: headers
    :param method: GET, OPTIONS, HEAD, POST, PUT, PATCH, or DELETE.
    :param params: to add query params in url
    :param data: post method body, DICT
    :return: reponse.json()
    """
    counter = 0
    kwargs = {
        'url' : url,
        'headers' : headers if headers else get_headers_with_bearer_token(audience),
        'method' : method,
        'params' : params,
    }
    if data is not None:
        kwargs.update({ 'data' : json.dumps(data)})
    logging.info(f"Request Params : \n {kwargs}")
    try:
        response = requests.request(**kwargs)
        print(response.text)    
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 401 or e.response.status_code == 403:
            if (counter < 3):
                counter = counter + 1
                logging.info("Refreshing token:")
                global bearer_token
                bearer_token = 'Bearer {}'.format(get_id_token_from_compute_engine())
                response = send_http_request_and_validate_response(audience, url, method, params, data)
                return response.json()
            else:
                msg = f"ERROR !!! while making http request to {url}"
                logging.error(traceback.format_exc())
                logging.debug(e)
                raise Exception(msg)
    except Exception as err:
        msg = f"ERROR !!! while making http request to {url}"
        logging.error(traceback.format_exc())
        logging.debug(err)
        raise Exception(msg)
    else:
        return response.json()


def get_read_category_info_payload(category_id=None):
    cfg.sp_read_category_info["mercari_l2_name"] = category_id
    return cfg.sp_read_category_info