from datetime import timedelta
from random import randint
from airflow import DAG
from airflow.contrib.sensors.gcs_sensor import GoogleCloudStorageObjectSensor
from airflow.operators.python_operator import PythonOperator
from airflow.operators.slack_operator import SlackAPIPostOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from google.cloud import storage
from datetime import datetime
import json
import base64
from common.utils import  get_slack_user_name
from metadata_tree.KG_backup_and_restore import main
import logging
from airflow.models import Variable
import metadata_tree.metadata_tree
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from metadata_tree import dag_export_entitydb_to_higher_environment
# Get GCP Project
project_id = 'mercari-us-de'
default_config = {

}
config = Variable.get("backup_restore_config", default_var=default_config, deserialize_json=True)

# Slack task for alerting
token = Variable.get('SLACK_TOKEN_V2')
channel = "#de-content-ops"
username = get_slack_user_name()


def report_failure_to_slack(context):
    text = """
            :red_circle: *Task Failed*.
            *Project*: {project_id}
            *Dag*: {dag}
            *Task*: {task}
            *Execution Time*: {exec_date}
            *Log Url*: {log_url}
            """.format(
        project_id=project_id,
        task=context.get('task_instance').task_id,
        dag=context.get('task_instance').dag_id,
        exec_date=context.get('execution_date'),
        log_url=context.get('task_instance').log_url)

    failed_alert = SlackAPIPostOperator(
        task_id='report_failure_to_slack',
        channel=channel,
        token=token,
        text=text,
        username='airflow')
    return failed_alert.execute(context=context)


default_args = {
    'owner': 'v-suresh@mercari-partner.com',
    'email': ['v-suresh@mercari-partner.com'],
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retry_delay': timedelta(seconds=randint(30, 120)),
    'on_failure_callback': report_failure_to_slack
}

dag_id = 'kg_backup_restore'
dag = DAG(
    dag_id,
    default_args=default_args,
    max_active_runs=1,
    # this DAG runs every 10.30 AM (UTC), 4.00 PM IST, 6.30 AM EDT
    schedule_interval='30 10 * * *')


def orchestrate(**kwargs):
    logging.info("EXECUTION START DATE {}".format(kwargs['execution_date']))
    execution_date = kwargs['execution_date']
    # execution_date_string = str(execution_date)
    main(execution_date, project_id, kwargs)


backup_and_restore_task = PythonOperator(
    dag=dag,
    task_id="back_and_restore_task",
    python_callable=orchestrate,
    trigger_rule=TriggerRule.ALL_SUCCESS,
    provide_context=True
)

backup_import_status_task = GoogleCloudStorageObjectSensor(
    dag=dag,
    task_id='backup_import_status_task',
    bucket='{{ task_instance.xcom_pull(task_ids="back_and_restore_task", key="bucket_name") }}',
    object='{{ task_instance.xcom_pull(task_ids="back_and_restore_task", key="gcs_path") }}',
    google_cloud_conn_id='google_cloud_default',
    timeout=7200,
    poke_interval=600,
    mode='reschedule',
    trigger_rule=TriggerRule.ALL_SUCCESS
)
tree_job_trigger_task = TriggerDagRunOperator(
    task_id='tree_job_trigger_task',
    trigger_dag_id=metadata_tree.metadata_tree.dag_id,
    trigger_rule=TriggerRule.ALL_SUCCESS,
    dag=dag)

entitydb_export_to_higher_env_task = TriggerDagRunOperator(
    task_id='entitydb_export_to_higher_env',
    trigger_dag_id=dag_export_entitydb_to_higher_environment.dag_id,
    trigger_rule=TriggerRule.ALL_SUCCESS,
    dag=dag
)


entitydb_export_to_higher_env_task.set_downstream(backup_and_restore_task)
backup_and_restore_task.set_downstream(backup_import_status_task)
backup_import_status_task.set_downstream(tree_job_trigger_task)
