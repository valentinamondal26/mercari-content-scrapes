import base64
import logging
from datetime import timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator
from airflow.operators.slack_operator import SlackAPIPostOperator
from airflow.utils import timezone

from sku_validation import SkuValidation

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
SLACK_TOKEN = str(base64.b64decode(Variable.get('SLACK_TOKEN')), 'utf-8')
SLACK_CHANNEL = "#de-content-ops"


def days_ago(n: int) -> timezone:
    """
    Get a datetime object representing `n` days ago. By default the time is
    set to midnight.
    :param n: number of days ago
    :return timezone of n number of days before
    """
    today = timezone.utcnow()
    return today - timedelta(days=n)


def report_failure_to_slack(context):
    """
    Raise slack alert on job failure.
    :param context: Job context object
    """
    message = """
            :red_circle: *Task Failed*.
            *Dag*: {dag}
            *Task*: {task}
            *Execution Time*: {exec_date}
            *Log Url*: {log_url}
            """.format(
            task=context.get('task_instance').task_id,
            dag=context.get('task_instance').dag_id,
            exec_date=context.get('execution_date'),
            log_url=context.get('task_instance').log_url)

    failed_alert = SlackAPIPostOperator(
            task_id='report_failure_to_slack',
            channel=SLACK_CHANNEL,
            token=SLACK_TOKEN,
            text=message,
            username='airflow')
    return failed_alert.execute(context=context)


def process_slack_alert(gcs_path: str, sku_count: int,
                        valid_count: int, invalid_count: int) -> None:
    """
    Process Slack alert on job success
    :param gcs_path: report gcs path
    :param sku_count: total sku count
    :param valid_count: valid sku count
    :param invalid_count: in-valid sku count
    """
    logger.info("SLACK ALERT")
    message = f"""SKU Validation Report:
                    *Total No of SKUs*:{sku_count}
                    *No of Valid SKUs*: {valid_count}  
                    *No of Invalid SKUs*: {invalid_count} 
                    *Detailed SKU validation report*: 
https://storage.cloud.google.com/content_us/{gcs_path} 
                    """
    logger.info(f"Message sent to slack channel : {message}")

    alert = SlackAPIPostOperator(
            task_id='pipeline_message',
            channel=SLACK_CHANNEL,
            token=SLACK_TOKEN,
            text=message,
            username='airflow')
    alert.execute()


def main():
    validation = SkuValidation()
    gcs_path, sku_items_count, valid_count, invalid_count = validation.main()
    logger.info(f"Sku count: {sku_items_count}")
    logger.info(f"Valid Sku count: {valid_count}")
    logger.info(f"In-Valid Sku count: {invalid_count}")
    logger.info(f"Report path: https://storage.cloud.google.com"
                f"/content_us/{gcs_path}")
    process_slack_alert(gcs_path, sku_items_count,
            valid_count, invalid_count)


# Dag Config
default_args = {
        'owner': 'Airflow',
        'start_date': days_ago(0),
        'retries': 0,
}

# The dag is scheduled - weekly on sunday 
# at 8:00 am UTC therefore 12:00 am PST and 1:30 pm IST
dag = DAG("sku_validation", default_args=default_args,
        schedule_interval="0 8 * * 0",
        on_failure_callback=report_failure_to_slack)
# dag = DAG("sku_validation", default_args=default_args,
#           schedule_interval=None)

sku_validation = PythonOperator(task_id="SkuValidation",
        python_callable=main,
        dag=dag)
sku_validation
