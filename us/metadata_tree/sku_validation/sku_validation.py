import io
import json
import logging
import math
from datetime import datetime
from typing import List, Tuple

import pandas as pd
import requests
import sku_validation_utils
from airflow.models import Variable
from google.auth.transport.requests import Request
from google.cloud import storage

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

kg_endpoint = Variable.get("KG_END_POINT")
metadata_identity_doc_url = Variable.get("METADATA_IDENTITY_DOC_URL")
project_id_url = Variable.get("PROJECT_ID_URL")

# def get_id_token_from_compute_engine():
#     request = Request()
#     headers = {"Metadata-Flavor": "Google"}
#     project_id = request(project_id_url, method='GET', headers=headers)
#     target_audience = project_id.data.decode(
#             'utf-8') + '-compute@developer.gserviceaccount.com'
#     url = metadata_identity_doc_url + "?audience=" + target_audience
#
#     resp = request(url, method='GET', headers=headers)
#     return 'Bearer {}'.format(resp.data.decode('utf-8'))

bearer_token = ''


def get_id_token_from_compute_engine() -> str:
    """
    Returns the generated token from the computer engine for the given audience.
    :return: Bearer token
    """
    request = Request()
    headers = {"Metadata-Flavor": "Google"}
    url = metadata_identity_doc_url + "?audience=" + kg_endpoint.format('prod')
    resp = request(url, method='GET', headers=headers)
    return 'Bearer {}'.format(resp.data.decode('utf-8'))


class SkuValidation:
    ENV = 'prod'

    def __init__(self):
        self.valid_count = 0
        self.invalid_count = 0

    def get_sku_items_count(self) -> int:
        """
        Returns the total sku-item mappings count
        :return: sku_to_item_mappings table count
        """
        bearer_token = get_id_token_from_compute_engine()
        headers = {"Authorization": bearer_token}
        url = f'https://kg-{self.ENV}.endpoints.mercari-us-de.cloud.goog/fn'
        params = {'db': 'metadatadb',
                  'fn': 'sp_sku_to_item_mappings_count'}
        response = sku_validation_utils.send_http_request_and_validate_response(
                url=url,
                method='GET',
                params=params,
                headers=headers
        )
        response.raise_for_status()
        return response.json()[0].get("rows_count", 0)

    def get_skuitem_mappings(self, offset: int, size: int) -> List:
        """
        Get the sku-item mapping from DB.
        :return: mapping response
        """
        url = f'https://kg-{self.ENV}.endpoints.mercari-us-de.cloud.goog/fn'
        params = {'db': 'metadatadb',
                  'fn': 'sp_sku_to_item_mappings_read_page_desc',
                  'offset': offset,
                  'size': size}
        response = sku_validation_utils.send_http_request_and_validate_response(
                url=url,
                method='GET',
                params=params)
        response.raise_for_status()

        mappings = [(map.get("sku_id"),
                     json.loads(map.get("item_ids").replace("'", '"')))
                    for map in response.json()]
        return mappings

    def using_get_item_info(self, sku_item_mapping: List) -> requests.Response:
        """
        Get item info using IteminfoV2
        :param sku_item_mapping: sku-item mapping response
        :return: sku-item mapping response
        """
        params = {"skuId": sku_item_mapping[0]}
        url = f'https://kg-{self.ENV}.endpoints.mercari-us-de.cloud.goog/resolveSkuId'
        response = sku_validation_utils.send_http_request_and_validate_response(
                url=url,
                method='GET',
                params=params)
        response.raise_for_status()
        return response

    def validate_sku(self, sku_item_mappings: List,
                     report: List) -> Tuple[int, int]:
        """
        :param sku_item_mappings: sku-item mapping response
        :param report: list of sku reports
        :return: valid sku and invalid sku count
        """

        valid_count = 0
        invalid_count = 0
        for sku_item_mapping in sku_item_mappings:
            db_item_ids = sku_item_mapping[1].get("item_ids", [])
            api_items = self.using_get_item_info(sku_item_mapping).json()
            if isinstance(api_items, dict) and not api_items.get("response",
                    []):
                logger.info(f" THE SKU ID :{sku_item_mapping[0]} IS NOT VALID")
                logger.info(f"ITEM IDS MAPPED IN DB: f{db_item_ids}")
                logger.info(f"ITEM IDS MAPPED IN KG: f{api_items}\n")
                invalid = [sku_item_mapping[0], "Invalid",
                           "No items present in the KG for the SKU"]
                report.append(invalid)
                invalid_count += 1
            elif isinstance(api_items.get("response", None), str):
                logger.info(f" THE SKU ID :{sku_item_mapping[0]} IS NOT VALID")
                logger.info(f"ITEM IDS MAPPED IN DB: f{db_item_ids}")
                logger.info(f"ITEM IDS MAPPED IN KG: f{api_items}\n")
                invalid = [sku_item_mapping[0], "Invalid",
                           "No entityAttributeJson present for the sku"]
                report.append(invalid)
                invalid_count += 1
            else:
                api_items = [pair.get("itemId") for pair in
                             api_items["response"]]
                if not list(set(db_item_ids) & set(api_items)):
                    logger.info(
                            f" THE SKU ID :{sku_item_mapping[0]} IS NOT VALID")
                    logger.info(f"ITEM IDS MAPPED IN DB: f{db_item_ids}")
                    logger.info(f"ITEM IDS MAPPED IN KG: f{api_items}\n")
                    invalid = [sku_item_mapping[0], "Invalid",
                               "No common items mapped present original and "
                               "current state item-mappings"]
                    report.append(invalid)
                    invalid_count += 1
                else:
                    valid = [sku_item_mapping[0], "Valid", "Success"]
                    report.append(valid)
                    valid_count += 1
        return valid_count, invalid_count

    def upload_report_in_gcs(self, report: List, gcs_path: str) -> None:
        """
        Uploads the report to the gcs
        :param report: report json
        :param gcs_path: report path
        """
        client = storage.Client()
        bucket = client.get_bucket('content_us')
        blob = bucket.blob(gcs_path)
        if blob.exists():
            # df = pd.read_csv(f'gs://{bucket}/{gcs_path}')
            data = io.StringIO()
            df = blob.download_as_string().decode("utf-8")
            data.write(df)
            data.seek(0)
            df = pd.read_csv(data)
            for row in report:
                df = df.append(
                        pd.Series(row, index=['SkuId', 'Status', 'Message']),
                        ignore_index=True)
        else:
            df = pd.DataFrame(report, columns=['SkuId', 'Status', 'Message'])
        data = io.StringIO()
        df.to_csv(data, index=False)
        data.seek(0)
        try:
            blob.upload_from_string(data.getvalue(), content_type='csv')
            logger.info(f"Report uploaded to location: "
                        f"https://storage.cloud.google.com/content_us/{gcs_path}")
        except Exception as exeception:
            logger.error(exeception)
            raise Exception

    def main(self):
        batch_size = 100
        # validation = SkuValidation()
        sku_items_count = self.get_sku_items_count()
        logger.info(f"SKU items count: {sku_items_count}")
        valid_count = 0
        invalid_count = 0
        timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        gcs_path = f'sku_validation/report/{timestamp}/report.csv'
        if sku_items_count > 0:
            batches = math.ceil(sku_items_count / batch_size)
            track = 0
            for i in range(batches):
                logger.info(str(i) + "th Batch")
                if i == batches - 1:
                    batch_size = sku_items_count - track
                skuitemappings = self.get_skuitem_mappings(track, batch_size)
                track += batch_size
                if skuitemappings:
                    report = []
                    va_c, in_c = self.validate_sku(skuitemappings, report)
                    valid_count += va_c
                    invalid_count += in_c
                    self.upload_report_in_gcs(report, gcs_path)
                    logger.info(f"Report :{json.dumps(report)}")
                else:
                    logger.error("No sku-item mapping found")
                    raise Exception

            return gcs_path, sku_items_count, valid_count, invalid_count
        else:
            logger.error("No sku-item mapping found")
            raise Exception
