"""
Implementation:
1. Fetch the latest tree hash for the tree ids.
2. Fetch the list of skus for the latest hash mapped for the tree.
3. Insert/ Update the sku_upc_info Spanner table based on the getUPCInfo call.
"""
import json
import logging
from typing import Dict, List, Union, Tuple
from datetime import datetime
from requests import Response

import pandas as pd
from airflow.models import Variable
from google.cloud import spanner


from spanner_insert import utils

from spanner_insert.spanner_connector import SpannerConnector


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

db_service = "https://kg-{}.endpoints.mercari-us-de.cloud.goog/fn"
kg_endpoint = "https://kg-{}.endpoints.mercari-us-de.cloud.goog"

metadata_identity_doc_url = Variable.get("METADATA_IDENTITY_DOC_URL")
project_id_url = Variable.get("PROJECT_ID_URL")

env = 'stage'

bearer_token = ''




def get_sku_info(tree_hash: str) -> Union[List, Dict]:
    """
    Get the sku information for the given tree hash
    :param tree_hash: the latest tree hash to fetch the sku details
    :return: list of skus for the tree_id or the error response
    """
    read_sku_info = {
        'fn': 'sp_a_sku_tree_read_by_fieldname',
        'db': 'metadatadb',
        'fieldname': 'tree_hash',
        'value': tree_hash

    }

    response = utils.send_http_request_and_validate_response(
        url=db_service.format(env),
        method='GET',
        params=read_sku_info)
    response.raise_for_status()
    if len(response.json()) > 0:
        df = pd.DataFrame(response.json())
        return df["sku_id"].values.tolist()

    return {"error": True}


def get_latest_hash(tree_id: str) -> str:
    """
    Get the latest hash for the given tree id
    :param tree_id: metadataTreeConfigurationId
    :return: latest tree hash
    """
    params = {
        'fn': 'sp_a_metadata_tree_read_by_fieldname',
        'db': 'metadatadb',
        'fieldname': 'metadataTreeConfigurationId',
        'value': tree_id

    }
    response = utils.send_http_request_and_validate_response(
        url=db_service.format(env),
        method='GET',
        params=params)

    response.raise_for_status()
    if response.json():
        return response.json()[0].get("hash", "")

    return None


def get_created_date(sku_id: str,
                     sc: SpannerConnector) -> Union[datetime,
                                                    None]:
    """
    Get the created date for the sku if exists
    :param sku_id: sku id
    :param sc:spanner connector object
    :return: created date
    """
    query = f"SELECT sku_id,created_date_time from sku_upc_info where sku_id = '{sku_id}' "
    results = sc.execute_sql(query)
    results = list(results)
    if len(results) > 0:
        return results[0][1]
    return None


def get_upc_info(sku_id: str) -> Response:
    """
    Fetch the UPC info for the given sku
    :param sku_id: sku id
    :return: getUPCInfo response
    """
    param = {'sku_id': sku_id}
    response = utils.send_http_request_and_validate_response(
        url=kg_endpoint.format('stage') + "/getUPCInfo", method='GET', params=param)
    return response


def load_data_for_trees(tree_ids: List) -> None:
    """
    Load the upc info for the skus for the given list of tree ids.

    For the given list tree ids get the latest tree hash and fetch the skus
    mapped with the latest tree hash to insert the upc info for the skus.
    :param tree_ids: list of tree ids
    """
    sc = SpannerConnector("batch_db")
    for tree_id in tree_ids:
        # fetch the latest tree hash
        logger.info(f"Tree id: {tree_id}")
        tree_hash = get_latest_hash(tree_id)
        logger.info(f"Latest tree hash: {tree_hash}")
        # fetch the list of skus mapped with the latest hash
        sku_list = get_sku_info(tree_hash)
        sku_upc_map = []
        data_frame = pd.DataFrame(columns=[
            "sku_id", "created_date_time", "item_list",
            "last_upd_date_time"])
        for sku in sku_list:
            logger.info(f"SKU: {sku}")

            # get the created date for the sku if the entry is present
            created_date = get_created_date(sku, sc)
            logger.info(f"created_date :{created_date}")

            # insert record for the sku if upc info items present for the sku
            response = get_upc_info(sku)
            if isinstance(response.json().get("response", None),
                          list) and response.json().get("response"):
                # if entry for the sku is already present do not update a new
                # created date
                if not created_date:
                    created_date = datetime.utcnow().replace(microsecond=0)
                # convert DateTimeinNanoseconds to datetime format
                if created_date:
                    created_date = str(created_date).split("+")[0].split(".")[0]
                    created_date = datetime.strptime(created_date,
                                                     '%Y-%m-%d %H:%M:%S')
                mapping = [sku, created_date,
                           json.dumps(response.json().get("response", None)),
                           datetime.utcnow().replace(microsecond=0)]
                a_series = pd.Series(mapping, index=data_frame.columns)
                data_frame = data_frame.append(a_series, ignore_index=True)
            else:
                logger.info(f"No UPC info for the sku: {sku}")

        logger.info(f"Uploading bulk data for the tree: {tree_id}")
        # do no load empty dataframe
        if not data_frame.empty:
            sc.write_df(data_frame, "sku_upc_info")
        else:
            logger.error("Empty Dataframe cannot be loaded..")
            raise Exception
