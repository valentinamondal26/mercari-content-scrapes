"""
Google Spanner connector
"""

import logging

import pandas as pd

from google.cloud.spanner import Client
from multiprocessing import cpu_count
# from toolz import partition_all

DEFAULT_PROJECT_ID = 'mercari-us-double-dev'
DEFAULT_INSTANCE_ID = 'mercari-us'
DEFAULT_PING_QUERY = 'SELECT 1'
DEFAULT_NUM_JOBS = cpu_count() - 2 if cpu_count() > 2 else 8
DEFAULT_BATCH_SIZE = 100


LOG = logging.getLogger(__name__)


class SpannerConnector(object):

    def __init__(self, database_id, project_id=DEFAULT_PROJECT_ID, instance_id=DEFAULT_INSTANCE_ID, credentials=None):
        """Spanner connector

        Parameters
        ----------
        database_id : str
            Database to use
        project_id : str, optional
            Google project to use, by default `mercari-us-de`
        instance_id : str, optional
            Spanner instance to use, by default `mercari`
        credentials : google.oauth2.service_account.Credentials, optional
            GCP credentials, by default None; let gcloud use it's default authentication mechanism
        """
        self.project_id = project_id
        self.instance_id = instance_id
        self.database_id = database_id
        self.credentials = credentials
        self.client = Client(project=self.project_id, credentials=self.credentials)
        self.instance = self.client.instance(instance_id=self.instance_id)
        self.database = self.instance.database(database_id=self.database_id)

    def ping(self):
        """Check for a valid db connection

        Returns
        -------
        str
            'pong'

        Raises
        ------
        google.api_core.exceptions.NotFound
            If the database was not found
        """
        result = self.execute_sql(DEFAULT_PING_QUERY)
        for r in result:
            assert(r[0] == 1)
        return 'pong'

    def execute_sql(self, sql, params=None, param_types=None):
        """Run SQL and return results

        Parameters
        ----------
        sql : str
            SQL to run
        params : dict: str -> value, optional
            SQL name, values for substitution, by default None
        param_types : dict: str -> value, optional
            Explicit types for param values, by default None

        Returns
        -------
        google.cloud.spanner_v1.streamed.StreamedResultSet
            Results
        """
        with self.database.snapshot() as snapshot:
            return snapshot.execute_sql(sql=sql, params=params,
                                        param_types=param_types)

    def read_df(self, sql, params=None, param_types=None):
        """Run SQL and read results into a dataframe

        Parameters
        ----------
        sql : str
            SQL to run
        params : dict: str -> value, optional
            SQL name, values for substitution, by default None
        param_types : dict: str -> value, optional
            Explicit types for param values, by default None

        Returns
        -------
        pd.DataFrame
            Results
        """
        results = self.execute_sql(sql=sql, params=params,
                                   param_types=param_types)
        rows = [row for row in results]
        field_names = [field.name for field in results.fields]
        return pd.DataFrame(data=rows, columns=field_names)

    def upsert(self, table, columns, values):
        """Perform an update or insert

        Parameters
        ----------
        table : str
            Table to update
        columns : list[str]
            columns to update
        values : list[object]
            List of values to update with
        """
        with self.database.batch() as batch:
            batch.insert_or_update(table=table, columns=columns, values=values)
    
    def partition_data(self, df, batch_size=DEFAULT_BATCH_SIZE):
        """
        Partition the dataframe into n size of lists.
        :param df: dataframe to partition
        :param batch_size: split size
        :return: partition nested list
        """

        partitioned = df.values.tolist()
        partitioned = [partitioned[i:i + batch_size]
                       for i in range(0, len(partitioned), batch_size)]
        return partitioned

    def write_df(self, df, table):
        """Upload dataframe to table

        The upsert is run using multiple threads.

        Parameters
        ----------
        df : pd.DataFrame
            Dataframe to upload
        table : str
            Table to update
        """
        partitions = self.partition_data(df)
        for batch in partitions:
            self.upsert(table=table, columns=df.columns, values=batch)

