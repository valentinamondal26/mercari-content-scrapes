"""
Dag for the sku_upc_info insertion for the given list of tree ids.
"""
import base64
import logging
from random import randint
from typing import List
from datetime import timedelta
from datetime import datetime

from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow.utils import timezone
from airflow.operators.python_operator import PythonOperator
from airflow.operators.slack_operator import SlackAPIPostOperator

from spanner_insert import insert_data

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
SLACK_TOKEN = str(base64.b64decode(Variable.get('SLACK_TOKEN')), 'utf-8')
SLACK_TOKEN = Variable.get('SLACK_TOKEN_V2')
SLACK_CHANNEL = "#de-content-ops"


def get_start_date(date_string: str) -> datetime:
    """
    Returns the start date of the dag by formatting the date string to utc datetime
    :param date_string: date string
    :return: utc formatted datetime
    """
    start_date = datetime.strptime(date_string, '%Y-%m-%d')
    start_date = start_date.replace(tzinfo=timezone.utc)
    return start_date

def report_failure_to_slack(context):
    """
    Raise slack alert on job failure.
    :param context: Job context object
    """
    message = """
            :red_circle: *Task Failed*.
            *Dag*: {dag}
            *Task*: {task}
            *Execution Time*: {exec_date}
            *Log Url*: {log_url}
            """.format(
                task=context.get('task_instance').task_id,
                dag=context.get('task_instance').dag_id,
                exec_date=context.get('execution_date'),
                log_url=context.get('task_instance').log_url)

    failed_alert = SlackAPIPostOperator(
        task_id='report_failure_to_slack',
        channel=SLACK_CHANNEL,
        token=SLACK_TOKEN,
        text=message,
        username='airflow')
    return failed_alert.execute(context=context)


def process_slack_alert(tree_ids: List) -> None:
    """
    Process Slack alert on job success
    :param tree_ids: List of tree ids
    """
    tree_ids = ", ".join(tree_ids)
    logger.info("SLACK ALERT")
    message = f"""UPC data inserted in spanner table - *sku_upc_info*
                    """
    logger.info(f"Message sent to slack channel : {message}")

    alert = SlackAPIPostOperator(
        task_id='pipeline_message',
        channel=SLACK_CHANNEL,
        token=SLACK_TOKEN,
        text=message,
        username='airflow')
    alert.execute()


def main():
    # Trees: apple_bluetooth_headphones, apple_cell_phones_and_smartphones, apple_smart_watches
    tree_ids = ['8a2ae8a44c9f44ccab6d6266b26766d4','78bc9003a57641d0a523269c4e5096e3','2bc6d699d110445c85342785484267a2']
    insert_data.load_data_for_trees(tree_ids)
    logger.info("Inserted upc info for the trees..")
    process_slack_alert(tree_ids)


# Dag Config
default_args = {
        'owner': 'Airflow',
        # 'start_date': get_start_date('2021-03-21'),
        'start_date': days_ago(1),
        'depends_on_past': False,
        'retry_delay': timedelta(seconds=randint(30, 120))
}


# The DAG is scheduled to run at 1pm UTC , 6:30pm IST daily
dag = DAG("upc_info_to_spanner_v2",
          default_args=default_args,
          schedule_interval='00 13 * * *',
          max_active_runs=1,
          on_failure_callback=report_failure_to_slack,
          catchup=False)

upc_info_insert = PythonOperator(task_id="UPCInfoInsert",
                                 python_callable=main,
                                 dag=dag)
upc_info_insert
