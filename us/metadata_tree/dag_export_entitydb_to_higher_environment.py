from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from metadata_tree import export_entitydb_to_higher_environment as utils
dag_id = 'export_entitydb_to_higher_environment'

args = {
    "owner": "airflow",
    "start_date": days_ago(1),
    "concurrency": 1,
    "retries": 1,
    "provide_context": True,
}


with DAG(dag_id,
          default_args=args,
          max_active_runs=1,
          catchup=False,
          schedule_interval=None) as dag:
    export_entity_db_data_to_gcs = PythonOperator(task_id='exporting_entity_db_data_to_gcs',
                                 python_callable=utils.initiate_export,
                                 on_failure_callback=utils.process_slack_alert)
    clean_up_higher_env_entity_db = PythonOperator(task_id='deleting_table_records_after_backup',
                                                   python_callable=utils.clean_higher_end_db_instance,
                                                   on_failure_callback=utils.process_slack_alert)
    import_entity_db_data_to_gcs = PythonOperator(task_id='importing_entity_db_data_from_gcs',
                                 python_callable=utils.initiate_import,
                                 on_failure_callback=utils.process_slack_alert)


export_entity_db_data_to_gcs.set_downstream(clean_up_higher_env_entity_db)
clean_up_higher_env_entity_db.set_downstream(import_entity_db_data_to_gcs)
