"""
Utils for dag_export_entitydb_to_higher_environment

Notes:
    1) Higher environment where db data is imported should be empty
        hence taking backup of tables and deleting records on daily basis using function cleanup_tables_for_publish
    2) Make sure cleanup_tables_for_publish is avaible in higher db
        # possible error - > pre_import error
    3) Table Schema should match in both instance
    4) The sql service account should have access to upload and download from the bucket
        # unauthorized to make this request
"""

import requests
import json
import time
import base64
from google.auth.transport.requests import Request
from airflow.operators.slack_operator import SlackAPIPostOperator
from airflow.models import Variable



# environment and db instance mapping : naming and env details can be confusing
# canary db instance is named as stage
# stage db instance is named as contentdb

env_to_db_map = {
    'test' : 'metadata-stage-db',
    'dev' : 'mercari-de-dev',
    'stage' : 'content-db',
    'canary' : 'mercari-de-stage',
    'prod' : 'mercari-de-prod',
}


# initializations
project = 'mercari-us-de'
export_databases = "entitydb"
import_databases = "entitydb"
delete_records_url = "https://kg-{}.endpoints.mercari-us-de.cloud.goog/fn?fn=sp_cleanup_tables_for_publish&db={}"
metadata_identity_doc_url = "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity"
project_id_url = "http://metadata/computeMetadata/v1/project/numeric-project-id"

tables_to_be_published = ['attribute','attribute_entity', 'entity', 'product', 'product_entity',
                          'synonym', 'attribute_entity_synonym', 'attribute_cleanup_log']
table_export_queries = [f"select * from {table}" for table in tables_to_be_published]
export_file_paths = [f"gs://content_us/temp_db_backup/{table}.csv" for table in tables_to_be_published]
higher_environments = ['canary', 'prod']


def process_slack_alert():
    print("SLACK ALERT")
    message = "TEST----> ERROR !!! while exporting entity DB data "
    slack_token = Variable.get('SLACK_TOKEN_V2')
    slack_channel = "#de-content-ops"
    print("Message sent to slack channel : {}".format(message))

    alert = SlackAPIPostOperator(
        task_id='pipeline_message',
        channel=slack_channel,
        token=slack_token,
        text=message,
        username='airflow')
    alert.execute()

def get_id_token_from_compute_engine():
    request = Request()
    headers = {"Metadata-Flavor": "Google"}
    project_id = request(project_id_url, method='GET', headers=headers)
    target_audience = project_id.data.decode('utf-8') + '-compute@developer.gserviceaccount.com'
    url = metadata_identity_doc_url + "?audience=" + target_audience
    resp = request(url, method='GET', headers=headers)
    return resp.data.decode('utf-8')

# def get_access_token():
#     from subprocess import PIPE, Popen
#     process = Popen("gcloud auth print-access-token", stdout=PIPE, shell=True)
#     token = process.communicate()[0].decode("utf-8").rstrip('\n')
#     print(f"Token : {token}")
#     return token

def get_access_token():
    import google.auth.transport.requests
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    auth_req = google.auth.transport.requests.Request()
    print(credentials.valid)
    credentials.refresh(auth_req)
    print(credentials.valid)
    token = credentials.token
    return token


headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
}
headers['Authorization'] = 'Bearer {}'.format(get_access_token())  # TODO : use this for DAG


def clean_higher_end_db_instance(**kwargs):
    """
    triggers a db function which takes backup of tables with current_date
    and deletes the previous date backup tables
    and deletes all records from tables
    The table details are within the db function
    :return:
    """
    headers['Authorization'] = 'Bearer {}'.format(get_id_token_from_compute_engine())
    for env in higher_environments:
        url = delete_records_url.format(env, import_databases)
        print(f"Url for db cleanup : {url}")
        response = requests.get(url, headers=headers)
        if 'ErrorCode' in response.json() or response.status_code != 200:
            print(response.json())
            raise Exception("Error during higher env db cleanup...")


def validate_task_done(self_link):
    """
    validates the initiated database restore task
    :param self_link:
    :return:
    """

    time.sleep(20)
    for i in range(1, 6):
        print(f"checking status of task try no: {i}")
        response = requests.get(self_link, headers=headers)
        status = response.json()['status']
        print(f"status of Task = {status}")
        print(response.text)
        if status == 'DONE':
            if "error" in response.json():
                print(response.text)
                raise Exception (f"Error During The Task... \n {response.json()['error']}")
            return True
        else:
            wait_secs = 15
            print(f"waiting for {wait_secs} secs for task to complete...")
            time.sleep(wait_secs)
            continue
    return False


def initiate_export(**kwargs):
    """
    Initiates export for each of the table data
    and tests the export task status for completion
    """

    export_jobs_self_links = []
    env = 'stage' # Export happens only in stage as single source for canary and prod
    for export_query, export_file_path in zip(table_export_queries, export_file_paths):
        export_db_instance = env_to_db_map[env]
        print(f"--------> initiating export from env: {env} --> db : {export_db_instance} for : {export_query}, to file {export_file_path}")
        initiate_export_url = f"https://www.googleapis.com/sql/v1beta4/projects/{project}/instances/{export_db_instance}/export"
        export_payload = {
         "exportContext":
           {
              "fileType": "CSV",
              "uri": export_file_path,
              "databases": export_databases,
              "csvExportOptions":
               {
                   "selectQuery":export_query,
               }
           }
        }
        print("initiate_export_url :", initiate_export_url)
        print("export_payload :",  export_payload)
        res = requests.post(initiate_export_url, headers=headers, data=json.dumps(export_payload))
        assert res.status_code == 200 , f"Export initiation Failed \n {res.json()}"
        validate_task_done(res.json()['selfLink'])
        export_jobs_self_links.append(res.json()['selfLink'])
    return export_jobs_self_links

def initiate_import(**kwargs):
    """
    initiates the restore once the backup is done
    :param self_link:
    :return:
    """
    import_jobs_self_links = []
    for env in higher_environments:
        print(f"Importing Data for higher evnvironment --> {env}")
        for export_file_path, table in zip(export_file_paths, tables_to_be_published):
            import_db_instance = env_to_db_map[env]
            print(f"********> initiating import in env : {env} --> db : {import_db_instance} for : {table}, from file {export_file_path}")
            restore_url = f'https://www.googleapis.com/sql/v1beta4/projects/{project}/instances/{import_db_instance}/import'
            restore_payload = {
             "importContext":
               {
                  "fileType": "CSV",
                  "uri": export_file_path,
                  "database": import_databases,
                  "csvImportOptions":
                   {
                     "table": table
                   }
               }
            }
            print("restore_url :", restore_url)
            print("restore_payload :", restore_payload)
            res = requests.post(restore_url, data=json.dumps(restore_payload), headers=headers)
            assert res.status_code == 200 , f"Import initiation Failed \n {res.text}"
            validate_task_done(res.json()['selfLink'])
            import_jobs_self_links.append(res.json()['selfLink'])
    return import_jobs_self_links


# for local testing
# def main():
#     """
#     Starting point
#     :return:
#     """
#     clean_higher_end_db_instance()
#     initiate_export()
#     initiate_import()
#
# main()