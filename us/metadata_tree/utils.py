from google.auth.transport.requests import Request
import configs as cfg
import json
import requests
import logging
import traceback


bearer_token = None


def get_id_token_from_compute_engine():
    request = Request()
    headers = {"Metadata-Flavor": "Google"}
    url = cfg.metadata_identity_doc_url + "?audience=" + cfg.KG_STAGE_ENDPOINT
    resp = request(url, method='GET', headers=headers)
    return resp.data.decode('utf-8')


def get_headers_with_bearer_token():
    global bearer_token
    if bearer_token is None:
        bearer_token = 'Bearer {}'.format(get_id_token_from_compute_engine())
    headers = {
        'Authorization': bearer_token,
        'Content-Type': 'application/json'
    }
    return headers


def send_http_request_and_validate_response(url, method, params=None, data=None, headers=None):
    """
    makes a http request and raise expection in case response is not 200*

    :param url: url to hit
    :param headers: headers
    :param method: GET, OPTIONS, HEAD, POST, PUT, PATCH, or DELETE.
    :param params: to add query params in url
    :param data: post method body, DICT
    :return: reponse.json()
    """
    counter = 0
    kwargs = {
        'url' : url,
        'headers' : headers if headers else get_headers_with_bearer_token(),
        'method' : method,
        'params' : params,
    }
    if data is not None:
        kwargs.update({ 'data' : json.dumps(data)})
    logging.info(f"Request Params : \n {kwargs}")
    try:
        response = requests.request(**kwargs)
        print(response.text)
        response.raise_for_status()
            
    except requests.exceptions.HTTPError as e:
        print("Token expired.....")
        # print(e.response.status_code)
        print(counter, ":counter value")
        if e.response.status_code == 401 or e.response.status_code == 403:
            if (counter < 3):
                counter = counter + 1
                logging.info("Refreshing token:")
                global bearer_token
                bearer_token = 'Bearer {}'.format(get_id_token_from_compute_engine())
                response = send_http_request_and_validate_response(url, method, params, data)
                response.raise_for_status()
                return response.json()
            else:
                msg = f"ERROR !!! while making http request to {url}"
                logging.error(traceback.format_exc())
                logging.debug(e)
                raise Exception(msg)
    except Exception as err:
        msg = f"ERROR !!! while making http request to {url}"
        logging.error(traceback.format_exc())
        logging.debug(err)
        raise Exception(msg)
    else:
        return response.json()
