sp_metadata_tree_read
-- Function: public.sp_metadata_tree_read(character varying)

-- DROP FUNCTION public.sp_metadata_tree_read(character varying);

CREATE OR REPLACE FUNCTION public.sp_metadata_tree_read(IN "p_metadataTreeConfigurationId" character varying)
 RETURNS TABLE("metadataTreeId" character varying, active boolean, hash character varying, loaded boolean, "gcsPath" character varying, "metadataTreeConfigurationId" character varying, create_date timestamp with time zone, modified_date timestamp with time zone) AS
$BODY$

    DECLARE
       BEGIN

    RETURN QUERY SELECT "metadata_tree"."metadataTreeId", "metadata_tree"."active", "metadata_tree"."hash", "metadata_tree"."loaded", "metadata_tree"."gcsPath", "metadata_tree"."metadataTreeConfigurationId", "metadata_tree"."create_date", "metadata_tree"."modified_date"
                            FROM public."metadata_tree" where "metadata_tree"."metadataTreeConfigurationId"="p_metadataTreeConfigurationId" and "metadata_tree"."active" = true;

       END;


$BODY$
 LANGUAGE plpgsql VOLATILE
 COST 100
 ROWS 1000;
ALTER FUNCTION public.sp_metadata_tree_read(character varying)
 OWNER TO postgres;