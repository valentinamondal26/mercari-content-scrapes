import sys
import os
sys.path.append(os.path.dirname(__file__))
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from google.auth.transport.requests import Request

import base64
from dateutil.parser import parse
from datetime import datetime
from datetime import timedelta
import logging
import requests
from requests.auth import HTTPBasicAuth
from urllib.parse import urlparse, urlencode
import hashlib
from google.cloud import storage
import json
import google.auth
import google.auth.transport.requests
import uuid
from metadata_tree.gcs_iap_request_util import make_iap_request
from airflow.operators.slack_operator import SlackAPIPostOperator
import csv
from io import StringIO
import copy
import traceback
import pandas as pd
from airflow.models import Variable

from metadata_tree import configs as cfg
from metadata_tree import utils


dag_id = "metadata_tree_configuration_v1"

args = {
    "owner": "airflow",
    "start_date": days_ago(2),
    "concurrency": 1,
    "retries": 1,
    "provide_context": True,
}

DAG = DAG(dag_id, default_args=args, max_active_runs=1,catchup=False,
          schedule_interval=None)

gcs_path = "metadata_tree/"

bucket_name = "content_us"

kg_service_domain = "https://kg-stage.endpoints.mercari-us-de.cloud.goog"
metadata_tree_generate_url = kg_service_domain + "/metadata_tree/generate"

bearer_token = ''

# global variables
metadata_tree_meta_info = {}

metadata_tree_meta_info['metadata_tree_updated'] = 0
metadata_tree_meta_info['metadata_tree_created'] = 0
metadata_tree_meta_info['metadata_tree_no_change'] = 0

metadata_tree_meta_info['metadata_tree_fetching_failures'] = 0


details = {}
details['metadata_tree_updated'] = []
details['metadata_tree_created'] = []
details['metadata_tree_fetching_failures'] = []
details['metadata_tree_no_change'] = []



final_details = []
item_name_suggest_url = "https://ml-de-gateway.double.dev5s.com/ml-item-name-suggest/v1/create"
skutreeId_url = kg_service_domain + "/metadata/trees/skuTreeMap"


def process_slack_alert(report_gcs_path):
    global metadata_tree_meta_info
    token = Variable.get('SLACK_TOKEN_V2')
    channel = "#de-content-ops"
    alert = SlackAPIPostOperator(
        task_id="alert_slack",
        channel=channel,
        token=token,
        text="""
                    Metadata Tree Generation Daily Report. 
                    *Total No of metadata trees*:{total_no_of_trees}
                    *No of metadata tree updated*: {metadata_tree_updated}  
                    *No of new metadata tree generated*: {metadata_tree_created} 
                    *No of metadata tree with no changes*: {metadata_tree_no_change}  
                    *No of metadata tree generation failures*: {metadata_tree_fetching_failures}
                    *Metadata tree detailed report*: {metadata_tree_detailed_url}
                    """ .format(
            total_no_of_trees=metadata_tree_meta_info['metadata_tree_updated']+ metadata_tree_meta_info['metadata_tree_created']+metadata_tree_meta_info['metadata_tree_no_change'],           
            metadata_tree_updated=metadata_tree_meta_info['metadata_tree_updated'],
            metadata_tree_created=metadata_tree_meta_info['metadata_tree_created'],
            metadata_tree_no_change=metadata_tree_meta_info['metadata_tree_no_change'],
            metadata_tree_fetching_failures=metadata_tree_meta_info[
                'metadata_tree_fetching_failures'],
            metadata_tree_detailed_url=report_gcs_path
        ),
        username="airflow",
    )
    alert.execute()


def fetch_and_load_metadata_tree(**context):
    url = cfg.KG_STAGE_ENDPOINT +"/fn"
    try:
        bearer_token = 'Bearer {}'.format(utils.get_id_token_from_compute_engine())
        headers = {
            'Authorization': bearer_token,
            'Content-Type': 'application/json'
        }

        tree_config_response = utils.send_http_request_and_validate_response(url=url,
                                                                             method='GET',
                                                                             params=cfg.sp_a_metadata_tree_configuration_read_all_params,headers=headers)
    except Exception:
        process_report_info(context,
                            "Error while fetching tree configuration", tree_config_response.text, ''
                                                                                             "metadata_tree_fetching_failures")
    else:
        for tree_config in tree_config_response:
            if is_valid_tree_config_response(context, tree_config):
                generate_metadata_tree(context, tree_config, tree_config.get("name"))
    report_gcs_path = write_report(context)
    process_slack_alert(report_gcs_path)


def generate_metadata_tree(context, tree_config, tree_name):
    ele = tree_config.get("config").get("metadata_tree") # TODO :  remove unwanted params in body
    ele.update({"metadataTreeConfigurationId" : tree_config.get("metadataTreeConfigurationId")})
    ele.update({"load_custom_item_tool" : False})
    logging.info("Generating Metadata tree for {}".format(tree_name))
    try:
        metadata_tree_response = utils.send_http_request_and_validate_response(url=metadata_tree_generate_url,
                                                                               method='POST',
                                                                               data=ele)
        print("metadata_tree_response::",metadata_tree_response)
    except Exception:
        process_report_info(context,
                                "Error while fetching metadata tree:", tree_name,
                                "metadata_tree_fetching_failures")
    else:
      
        if not metadata_tree_response:
             process_report_info(context,
                                "Returns empty value, Error while generating metadata tree:", tree_name,
                                "metadata_tree_fetching_failures")
            

        elif metadata_tree_response.get("success") is False:
            process_report_info(context,
                                "Error while fetching metadata tree:" + metadata_tree_response.get("tree_response"," ").get("message"),
                                tree_name,
                                "metadata_tree_fetching_failures")
        else:
             process_report_info(context, "Successfully loaded the metadata tree for '{}' in GCS".format(tree_name),
                            tree_name,
                            metadata_tree_response.get("status",''))

def is_valid_tree_config_response(context, tree_config):
    if "active" in tree_config and "name" in tree_config:
        if "metadata_tree" in tree_config.get("config") and tree_config.get("active") is True:
            if type(tree_config.get("config").get("metadata_tree")) is dict:
                return True
            process_report_info(context, "Error while validating tree configuration", tree_config.get("name"),
                                "metadata_tree_fetching_failures")
            return False        
    else:
        process_report_info(context, "Error while validating tree configuration", "error",
                            "metadata_tree_fetching_failures")
        return False

def create_directory(context, directory_name, file_name, file_format):
    logging.info("Writing TO GCS")
    execution_date = context["execution_date"]
    directory_name = "reports"

    if blob_exists(bucket_name=bucket_name, path=gcs_path + directory_name + "/",):

        logging.info("Creating a directory based on the execution date")
        create_directory_in_gcs(bucket_name=bucket_name, destination_folder_name=gcs_path + directory_name + "/" + execution_date.strftime("%Y_%m_%d-%H_%M_%S/"))
    else:
        logging.info("directory does not exists")
        create_directory_in_gcs(bucket_name=bucket_name, destination_folder_name=gcs_path + directory_name + "/")
        create_directory_in_gcs(bucket_name=bucket_name, destination_folder_name=gcs_path + "reports" + "/" + execution_date.strftime("%Y_%m_%d-%H_%M_%S/"))
    
    gcs_final_path = (gcs_path + directory_name + "/" + execution_date.strftime("%Y_%m_%d-%H_%M_%S/") + file_name + "." + file_format)
    return gcs_final_path


def process_report_info(values, reason, name, state):
    global metadata_tree_meta_info, details
    values["ti"].xcom_push(key="name", value=name)
    if state == "metadata_tree_fetching_failures":
        values["ti"].xcom_push(key="exception", value=reason)
    else:
        values["ti"].xcom_push(key="reason", value=reason)
    current_value = create_dictionary_from_context(
        values, state)
    details[state].append(current_value)
    metadata_tree_meta_info[state] = metadata_tree_meta_info[state] + 1


# Creates a dictionary containing all the details of the given metadatatree request response
def create_dictionary_from_context(context, state):
    current_values = {}
    current_values["task"] = context.get("task_instance").task_id,
    current_values["dag"] = context.get("task_instance").dag_id,
    current_values["exec_date"] = context.get("execution_date"),
    current_values["log_url"] = context.get("task_instance").log_url
    current_values["name"] = context.get("task_instance").xcom_pull(
        key="name", task_ids="load_metadata_tree"
    )
    if state == "metadata_tree_fetching_failures":
        current_values["reason"] = context.get("task_instance").xcom_pull(key="exception", task_ids="load_metadata_tree")
    else:
        current_values["reason"] = context.get("task_instance").xcom_pull(key="reason", task_ids="load_metadata_tree")
    current_values['state'] = state

    return current_values

# To form the final path and calls the functions to prepare the data and write it as csv to gcs
def write_report(context):
    path = create_directory(context, "reports", "reports", "csv")
    rows = prepare_data()
    report_gcs_path = save_data_to_csv_to_gcs(path, rows)
    report_gcs_path = "https://storage.cloud.google.com/" + bucket_name + "/" + report_gcs_path
    return report_gcs_path

# prepares the data as list of lists so that it can be written as csv file
def prepare_data():
    csv_columns = ['Name', 'Message', 'State', 'Status']
    final_details.append(csv_columns)
    for key in details.keys():
        for x in details[key]:
            row = []
            row.append(x['name'])
            row.append(x['reason'])
            row.append(x['state'])
            if (key == 'metadata_tree_fetching_failures'):
                row.append('Failed')
            else:
                row.append('Success')
            final_details.append(row)
    return final_details

# save the data as csv to gcs
# arg - gcs_final_path - final path of gcs where file will be written
# rows - data to be written


def save_data_to_csv_to_gcs(gcs_final_path, rows):
    logging.info("Writing data to csv file at {}".format(gcs_final_path))
    report_data = StringIO()
    try:
        writer = csv.writer(report_data, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in rows:
            writer.writerow(row)
        write_gcs(gcs_final_path, report_data.getvalue())
    except Exception as err:
        logging.info('----- ERROR (writerow) ---------')
        logging.info(err)
    finally:
        report_data.close()

    return gcs_final_path


def blob_exists(bucket_name, path):
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(path)
    return blob.exists()


def create_directory_in_gcs(bucket_name, destination_folder_name):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_folder_name)
    blob.upload_from_string("")
    logging.info("Created {} .".format(destination_folder_name))


# writes the final output to gcs
# arg - report_path - url of gcs for uploading data
#        data = data to be written to the gcs

def write_gcs(report_path, data):
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    new_blob = bucket.blob(report_path)
    new_blob.upload_from_string(data)


metadata_tree_dag = PythonOperator(
    dag=DAG,
    task_id="load_metadata_tree",
    python_callable=fetch_and_load_metadata_tree,
    trigger_rule=TriggerRule.ALL_DONE,
)