import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            description = ", ".join(record.get('description', ''))
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
    
            model = record.get('title')
            model = re.sub(re.compile(r"\b{}\b".format(
                record.get("brand", "")), re.IGNORECASE), "", model)
            model_sku = record.get('model_number', '').strip("Model # \n \t")
            model = re.sub(re.compile(
                r"\-\s*{}\s*\-\s*{}".format(model_sku, "The Home Depot"),
                re.IGNORECASE), "", model)
            model = re.sub(r'\(|\)|w\/', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(\b\d+FT\b)', lambda x: x.group().replace('FT','ft.'), model)
            model = re.sub(r'(.*)(#\d+)(.*)(\s\d+-Piece\b)(.*)', r'\1 \3 \2 \4 \5', model, flags=re.IGNORECASE)
            model = re.sub(r'\btitle\b|\:', '', model, flags=re.IGNORECASE)
            model = " ".join(model.split())
          
            product_type = record.get("Department", "")
            if not product_type:
                product_type = record.get("breadcrumb", [])[1]
            if product_type != "Air Compressors, Tools & Accessories" \
                    and len(product_type.split(", ")) > 1:
                product_type = product_type.split(", ")[0]

            if not product_type:
                return None
            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                
                "category": record.get("category"),
                "brand": record.get('brand', ''),

                "breadcrumb": "|".join(record.get('breadcrumb', '')),
                "description": description.replace('\n', ''),
                
                "title": record.get('title', ''),
                "model": model,
                
                "image": record.get('image', ''),
                "ner_query": ner_query.replace('\n', ''),
                "price": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                
                "model_sku": model_sku,
                "corded_cordless": record.get("Cordless/ Corded", ""),
                "maximum_voltage_output": record.get("Voltage", ""),
                "product_type": product_type
            }

            return transformed_record
        else:
            return None
