'''
https://mercari.atlassian.net/browse/USCC-285 - RYOBI Tools & Workshop Equipment
'''

import scrapy
import datetime
import json
import random
import os
import re
from scrapy.exceptions import CloseSpider


class HomedepotSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from homedepot.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Tools & Workshop Equipment

    brand : str
        category to be crawled, Supports
        1) RYOBI

    Command e.g:
    scrapy crawl homedepot -a category="Tools & Workshop Equipment" -a brand='RYOBI'

    """

    name = 'homedepot'
    start_urls = [
        'https://www.homedepot.com'
    ]

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None

    source = 'homedepot.com'
    blob_name = 'homedepot.txt'

    base_url = 'https://www.homedepot.com'
    number_of_products = None

    url_dict = {
        "Tools & Workshop Equipment": {
            'RYOBI': {
                "url": 'https://www.homedepot.com/b/Tools/RYOBI/N-5yc1vZc1xyZm5d/Ntk-EnrichedProductInfo/Ntt-ryobi?NCNI-5&experienceName=default',
                'filters': ['Department', 'Voltage', 'Cordless/ Corded']
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(HomedepotSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            # self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.filters = self.url_dict.get(
            category, {}).get(brand, {}).get('filters', '')
        if category and brand:
            self.launch_url = self.url_dict.get(
                category, {}).get(brand, {}).get('url')
        if not self.launch_url:
            self.logger.error(
                'Spider is not supported for  category:{} and brand:{}'.format(
                    category, brand))
            raise(CloseSpider(
                'Spider is not supported for  category:{} and brand:{}'.format(
                    category, brand)))
        self.logger.info("Crawling for  category " +
                         category + ",url:" + self.launch_url)

    def start_requests(self):
        if self.launch_url:
            if self.filters:
                yield self.create_request(url=self.launch_url,
                                         callback=self.parse_filters)
            else:
                yield self.create_request(url=self.launch_url,
                                         callback=self.parse_filters)

    def parse_filters(self, response):
        """
        @url https://www.homedepot.com/b/Tools/RYOBI/N-5yc1vZc1xyZm5d/Ntk-EnrichedProductInfo/Ntt-ryobi?NCNI-5&experienceName=default
        @meta {"use_proxy": "True"}
        @returns requests 2
        """

        if self.filters:
            for filter in self.filters:
                filter_urls = response.xpath(
                    '//div[@class="plpleftrail"]//div[span[contains(text(),"{}")]]//following-sibling::ul//li[re:match(@data-active, "yes|no")]//a/@href'.format(filter)).getall()
                if not filter_urls:
                    filter_urls = response.xpath(
                        '//div[@class="plpleftrail"]//div[div[a[h2[contains(text(),"{}")]]]]//following-sibling::div//ul//li/a/@href'.format(filter)).getall()
                filter_urls = [
                    self.base_url+link if "https" not in link else link
                    for link in filter_urls]

                filter_values = response.xpath(
                    '//div[@class="plpleftrail"]//div[span[contains(text(),"{}")]]//following-sibling::ul//li[re:match(@data-active, "yes|no")]//a/@data-refinementvalue'.format(filter)).getall()
                if not filter_values:
                    filter_values = response.xpath(
                        '//div[@class="plpleftrail"]//div[div[a[h2[contains(text(),"{}")]]]]//following-sibling::div//ul//li/a/@data-refinementvalue'.format(filter)).getall()
                filter_values = [
                    re.sub(r"\(\s*\d+\s*\)", "", v).strip()
                    for v in filter_values]
                for url in filter_urls:
                    meta = {}
                    index = filter_urls.index(url)
                    meta = meta.fromkeys([filter], filter_values[index])
                    meta.update({'filter_name': filter})
                    yield self.create_request(url=url, callback=self.parse,
                                             meta=meta)

        yield self.create_request(url=response.url, callback=self.parse, meta=response.meta)

    def parse(self, response):
        all_links = response.xpath(
            '//div[@class="pod-inner"]//div[@data-podaction="product name"]/a/@href').getall()
        all_links = [self.base_url +
                     link if "https" not in link else link for link in all_links]
        for link in all_links:
            yield self.create_request(url=link, callback=self.parse_product,
                                     meta=response.meta)

        next_page_url = response.xpath(
            '//a[@title="Next"]/@href').get(default='')
        next_page_number = response.xpath(
            '//a[@title="Next"]/@data-pagenumber').get(default='')
        current_page_number = response.xpath(
            '//a[contains(@class,"active")]/@data-pagenumber').get()
        spacer = response.xpath(
            '//span[@class="hd-pagination__spacer"]/text()').get(default=None)
        if next_page_url and not spacer:
            if "https" not in next_page_url:
                next_page_url = self.base_url + next_page_url
                yield self.create_request(url=next_page_url, callback=self.parse,
                                         meta=response.meta)

    def parse_product(self, response):
        item = {}
        item["crawl_date"] = str(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"] = str(response.status)
        item['id'] = response.url
        item['image'] = response.xpath(
            '//meta[@property="og:image"]/@content').get(default='')
        item['brand'] = self.brand
        item['category'] = self.category
        script_data = response.xpath(
            '//script[contains(text(),"BREADCRUMB_JSON")]//text()').get(default="")
        script_data = script_data.split("var BREADCRUMB_JSON = ")[
            1].strip("\n \t \r ;")
        script_data = json.loads(script_data)
        item['breadcrumb'] = script_data.get(
            "bcEnsightenData", {}).get("contentSubCategory", "").split(">")
        item['title'] = response.xpath(
            '//meta[@property="og:title"]/@content').get(default='')
        item['model_number'] = response.xpath(
            '//h2[@class="product_details modelNo"]/text()').get(default='')
        item['model_internet_number'] = response.xpath(
            '//span[@id="product_internet_number"]/text()').get(default='')
        item['model_store_sku'] = response.xpath(
            '//span[@id="product_store_sku"]/text()').get(default='')
        item['model_store_so_sku'] = response.xpath(
            '//h2[@class="product_details"][contains(text(),"Store SO SKU")]/span/text()').get(default='')
        item['price'] = response.xpath(
            '//span[@class="price__dollars"]//text()').get(default='').strip("\n ")
        item['description'] = response.xpath(
            '//div[@class="buybox"]//li//text()').getall()
        meta = {}
        filter_name = response.meta.get('filter_name', '')
        if filter_name:
            meta = meta.fromkeys([filter_name], response.meta[filter_name])
            item.update(meta)
        yield item

    def create_request(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key, value in headers.items():
                request.headers[key] = value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request
