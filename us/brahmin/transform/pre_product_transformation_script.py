import hashlib
import re
from urllib.parse import unquote

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            id = unquote(id)
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = record.get('color', '')
            model = record.get('title', '')
            model = re.sub(re.compile(color, re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            weight = record.get('weight', '')

            depth = ''
            height = ''
            width = ''
            dimensions = record.get('dimensions', '')
            match = re.findall(re.compile(r'(\d+(.\d+){0,1})" W x (\d+(.\d+){0,1})" H x (\d+(.\d+){0,1})" D', re.IGNORECASE), dimensions)
            if match:
                width = match[0][0]
                height = match[0][2]
                depth = match[0][4]


            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,

                "title": record.get('title', ''),
                "description": record.get('description', ''),

                "model": model,
                "color": color,
                "material": record.get('material', ''),
                'product_line': record.get('product_line', ''),
                'depth': depth,
                'height': height,
                'width': width,
                'weight': weight,
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
