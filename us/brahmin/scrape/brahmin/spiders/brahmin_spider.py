'''
https://mercari.atlassian.net/browse/USCC-251 - Brahmin Shoulder Bags
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import re
import os

class BrahminSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from brahmin.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags

    brand : str
        brand to be crawled, Supports
        1) Brahmin

    Command e.g:
    scrapy crawl brahmin -a category='Shoulder Bags' -a brand='Brahmin'
    """

    name = 'brahmin'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'brahmin.com'
    blob_name = 'brahmin.txt'

    listing_page_url = '{url}?sz={size}&start=0'

    url_dict = {
        'Shoulder Bags': {
            'Brahmin': {
                'url': 'https://www.brahmin.com/handbags/shoulder-bags',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BrahminSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_filter)


    def crawlDetail(self, response):
        """
        @url https://www.brahmin.com/amelia-hutchinson/R101737.html?dwvar_R101737_color=Stardust&cgid=shoulderBags#start=1
        @meta {"use_proxy":"True"}
        @scrape_values  title price breadcrumb color description dimensions weight model_sku
        """

        extra_item_info = response.meta.get('extra_item_info', {})
        for d in response.xpath('//ul[@class="swatches color"]/li/a[@class="swatchanchor"]'):
            color = d.xpath('.//input[@id="swatchName"]/@value').extract_first()
            match = re.findall(re.compile(r'acChangeProductInformation\((.*?)\);', re.IGNORECASE), d.xpath('./@onclick').extract_first())
            if match:
                d1 = match[0]
                import json
                j=json.loads(d1)
                model_sku = j.get('productId', '')
                price = j.get('productUnitPrice', '')
                image = j.get('productImageUrl', '')
                url = j.get('productUrl', '')
                # print(model_sku, price, image, url, color)
            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                # 'id': response.url,
                'id': url,

                # 'image': response.xpath('//img[@itemprop="image"]/@src').extract_first(default=''),
                'image': image,
                'breadcrumb': ' | '.join(response.xpath('//a[@class="breadcrumb-element"]/text()').extract()),
                'rating': response.xpath('//div[@itemprop="aggregateRating"]/span[@itemprop="ratingValue"]/text()').extract_first(default=''),

                'title': response.xpath(u'normalize-space(//h1[@itemprop="name"]/text())').extract_first(default=''),
                # 'price': response.xpath('//span[@class="price-sales"]/text()').extract_first(default=''),
                'price': price,
                # 'color': response.xpath(u'normalize-space(//span[@class="color-swtch"]/text())').extract_first(default=''),
                'color': color,
                'details': response.xpath('//div[@class="text section"]//text()[normalize-space(.)]').extract(),
                'description': ''.join(response.xpath('//div[@itemprop="description"]/div[@class="text"]//text()[normalize-space(.)]').extract()),
                'dimensions': response.xpath(u'normalize-space(//div[@class="content"]/strong[contains(text(),"Product Dimensions")]/following-sibling::div/text())').extract_first(default=''),
                'weight': response.xpath(u'normalize-space(//div[@class="content"]/strong[contains(text(),"Product Weight")]/following-sibling::div/text())').extract_first(default=''),
                # 'model_sku' = response.xpath(u'normalize-space(//div[@class="content"]/strong[contains(text(),"STYLE NO")]/following-sibling::div/text())').extract_first(default=''),
                'model_sku': model_sku,
                # 'colors': response.xpath('//ul[@class="swatches color"]//input[@id="swatchName"]/@value').extract(),
                'material': extra_item_info.get('filter_material', ''),
            }


    def parse_filter(self, response):
        """
        @url https://www.brahmin.com/handbags/shoulder-bags
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for material_filter in response.xpath('//ul[@id="Material-dropdown"]//a[@class="refinement-link blwDropdownFocusable"]'):
            link = material_filter.xpath('./@href').extract_first(default='')
            material_tag = material_filter.xpath(u'normalize-space(./text())').extract_first()
            match = re.findall(re.compile(r'\(\d+\)'), material_tag)
            if link and material_tag and match:
                material = material_tag.replace(match[0], '').strip()
                count = match[0].replace('(', '').replace(')', '').strip()
                print(f'material: {material}, count: {count}, link:{link}')
                request = self.create_request(url=self.listing_page_url.format(url=link, size=count), callback=self.parse_all)
                request.meta['extra_item_info'] = {
                    'filter_material': material,
                }
                yield request


    def parse(self, response):
        total_count = response.xpath('//span[@id="totalCount"]/text()').extract_first()
        if total_count:
            yield self.create_request(url=self.listing_page_url.format(url=self.launch_url, size=total_count), callback=self.parse_all)


    def parse_all(self, response):
        """
        @url https://www.brahmin.com/handbags/shoulder-bags
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.xpath('//ul[@id="search-result-items"]//a[@class="name-link"]'):
            id = product.xpath('./@href').extract_first()
            if id:
                request = self.create_request(url=id, callback=self.crawlDetail)
                request.meta['extra_item_info'] = response.meta.get('extra_item_info', {})
                yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

