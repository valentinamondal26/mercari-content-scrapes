import hashlib
import re
import json
from colour import Color


class Mapper(object):
    
    
    def colour_format(self,color_words):
        for word in color_words:
            for w in color_words:
                if word!=w:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,w)!=[]:
                        return w
                    
    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawled_date','')
            status_code = record.get('status_code','')
            description=record.get('description','')
            image = record.get('image','')
            title = record.get('Item Name','')
            colour = record.get('Color','')
            price=re.findall(r"\d+\.*\d*",str(record.get('price','')))
            if price!=[]:
                price=price[0]
            else:
                price=''

            est_retail=re.findall(r"\d+\.*\d*",str(record.get('est_retail','')))
            if est_retail!=[]:
                est_retail=est_retail[0]
            else:
                est_retail=''

            brand=record.get('Brand','')
            condition=record.get('item_condition','')

            pattern=re.compile(r"[\w\d]+\-[a-zA-Z\d\-]*",re.IGNORECASE)
            colour=re.sub(pattern,'--',colour)
            colour=', '.join(colour.split('--'))
            if len(colour)>=2:
                if colour[len(colour)-2]==",":
                    colour=colour[:-2]
                    
            if "," not in colour and colour!='':
                color_words=re.findall(r"\w+",colour)
                wr=self.colour_format(color_words)
                if wr:
                    w1=' '.join(color_words[0:color_words.index(wr)])
                    w2=", ".join(color_words[color_words.index(wr)+1:])
                    if w2!='':
                        colour=w1+", "+w2
                    else:
                        colour=w1
                        
            if colour=='':
                    words_title=re.findall(r"\w+",record.get('title',''))
                    _color = [i for i in words_title if self.check_color(i)]
                    colour=", ".join(_color)

                    
            model=record.get('Item Name','')
            pattern=re.compile(r"women\'*s*|blouses*|[\:\-\_\+\,\&]|torrid|size\s*\d*|Leather",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            
            for word in colour.split(', '):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
                
            models_words=re.findall(r"\w+",model)
            _color = [i for i in models_words if self.check_color(i)]
            for word in _color:
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
                
            
            for word in re.findall(r"\w+",record.get('Size','')):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            
            for word in re.findall(r"\w+",record.get('Type','')):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
                
            model=' '.join(model.split())      
            
                
                

        
        

            ner_query = description + condition

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
         

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": "Women's Blouses",
                "brand": brand,
                "image": image,
                "ner_query": ner_query,
                "item_name": title,
                "title":record.get("title"),
                "color": colour,   
                "price":{
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "msrp":{
                    "currency_code": "USD",
                    "amount": est_retail.replace("$","")
                },
                "description":description,
             
                "item_condition":record.get('item_condition',''),
                "model":model

            }
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
    