import hashlib
import re


class Mapper(object):

    def get_dimensions(self, measurements):
        dimensions = measurements.replace("\"", ":", -1)
        dimensions = dimensions.split(" x")
        dim_dict = dict()

        for item in dimensions:
            if item.endswith('H'):
                height = item.split(":")
                val = {'unit': 'INCH', 'value': height[0]}
                dim_dict['height'] = val
            if item.endswith('W'):
                width = item.split(":")
                val = {'unit': 'INCH', 'value': width[0]}
                dim_dict['width'] = val

            if item.endswith('L'):
                length = item.split(":")
                val = {'unit': 'INCH', 'value': length[0]}
                dim_dict['length'] = val

        return dim_dict

    def get_price(self, input):
        numbers = re.findall("\d+(?:[\d,.]*\d)", input)
        numbers = [x.replace(",", "") for x in numbers]
        numbers = map(float, numbers)
        return max(numbers)

    def map(self, record):
        if record:
            id = record['id']
            crawl_date = record['crawled_date']
            status_code = record['status_code']
            category = record['category']
            brand = record['brand']
            image = record['image']
            model = record['model']
            condition = record['condition']
            material = record['material']
            breadcrumb = record['breadcrumb']
            measurements = self.get_dimensions(record['measurements'])
            color = record['color']
            description = record['description']
            title = record['title']
            style_name = record['style_name']
            price = record.get('price', None)
            #if price.strip() != "":
                #price = self.get_price(record['price'])
            est_retail = record.get('est_retail', None)
            if est_retail.strip() != "":
                est_retail = self.get_price(est_retail)
            ner_query = record['description'] + record['condition'] + record['color']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": category,
                "brand": brand,
                "image": image,
                "model": model,
                "condition": condition,
                "material": material,
                "breadcrumb": breadcrumb,
                "measurements": measurements,
                "color": color,
                "description": description,
                "ner_query": ner_query,
                "item_title": title,
                "price": {
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "est_retail": est_retail,
                "style_name":style_name
            }
            return transformed_record
        else:
            return None

if __name__ == "__main__":
    import json
    print(json.dumps(Mapper().map({"crawled_date": "2019-09-19 14:05:13", "status_code": "200", "category": "Women's Shoulder Bags", "brand": "Coach", "id": "https://www.tradesy.com/i/coach-willis-willis-murphy-black-leather-cross-body-bag/19359421/", "title": "Willis 'willis Murphy Black Leather Cross Body Bag", "model": "19359421", "material": "Leather", "condition": "Gently used, Leather aging, Hardware on lock is slightly discolored. Scuffs lightly on leather due to age and use. In great shape!", "breadcrumb": "Shop | Bags | Cross Body Bags | Coach Cross Body Bags", "description": "This is a classic piece, a really cool style Coach bag. My favorite thing about it besides the style is the really uniquely shaped hardware on the straps and attachments. It's in great shape considering it's a vintage piece but has a lot of use left!", "measurements": "7\"L x 3.25\"W x 10\"H", "style_name": "xyz", "color": "Black", "price": "$85.37", "est_retail": "$428.00", "image": "https://img-static.tradesy.com/item/19359421/coach-willis-willis-murphy-black-leather-cross-body-bag-0-1-540-540.jpg"})))