import hashlib
import re
import json
from colour import Color


class Mapper(object):
    
    def colour_format(self,color_words):
        for word in color_words:
            for w in color_words:
                if word!=w:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,w)!=[]:
                        return w

    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawled_date','')
            status_code = record.get('status_code','')
            description=record.get('description','')
            image = record.get('image','')
            title = record.get('Item Name','')
            colour = record.get('Color','')
            price=re.findall(r"\d+\.*\d*",str(record.get('price','')))
            if price!=[]:
                price=price[0]
            else:
                price=''

            est_retail=re.findall(r"\d+\.*\d*",str(record.get('est_retail','')))
            if est_retail!=[]:
                est_retail=est_retail[0]
            else:
                est_retail=''

            brand=record.get('Brand','')
            condition=record.get('item_condition','')
            material=record.get('Material','')
            #get style name key 
            keys=list(record.keys())
            style_key=[ x for x in keys if "Style Name" in x ]
            product_line=''
            if style_key!=[]:
                product_line=record.get(style_key[0],'')
            measurements=record.get('Measurements','')
            pattern=r"\d+\.*\d*\"[l|L]{1}|\d+\.*\d*\"[h|H]{1}|\d+\.*\d*\"[w|W]{1}"
            matches = re.findall(pattern,measurements)
            length=''
            width=''
            height=''
            for match in matches:
                if "l" in match or "L" in match:
                    length=match.split('"')[0]
                if "h" in match or "H" in match:
                    height=match.split('"')[0]
                if "w" in match or "W" in match:
                    width=match.split('"')[0]


            model=record.get('Item Name','')
            model_words=re.findall(r"\w+",model)
        
          
            # colors
            _color = [i for i in model_words if self.check_color(i)]
            color=''
            for c in _color:
                if c!='':
                    color=c
                    model=model.replace(color,'')
            
            material_words=re.findall(r"\w+",material)
            _color = [i for i in material_words if self.check_color(i)]
            color=''
            for c in _color:
                if c!='':
                    color=c
                    material=material.replace(color,'')
                    
            material_pattern=re.compile(r"\-|\/|satchel\s*s*|Trim|Quilted|with|Metal\s*Fixtures|Signature",re.IGNORECASE)
            material=re.sub(material_pattern,'',material)
            material=' '.join(material.split())
            if material=="Leather patent":
                material="Patent Leather"
                    
            pattern=re.compile(r"[\w\d]+\-[a-zA-Z\d\-]*",re.IGNORECASE)
            colour=re.sub(pattern,'--',colour)
            colour=', '.join(colour.split('--'))
            if len(colour)>=2:
                if colour[len(colour)-2]==",":
                    colour=colour[:-2]
                    
            if "," not in colour and colour!='':
                color_words=re.findall(r"\w+",colour)
                wr=self.colour_format(color_words)
                if wr:
                    w1=' '.join(color_words[0:color_words.index(wr)])
                    w2=", ".join(color_words[color_words.index(wr)+1:])
                    if w2!='':
                        colour=w1+", "+w2
                    else:
                        colour=w1

                
            
                    
            colour=re.sub(re.compile(r'satchel\s*s*|\-',re.IGNORECASE),'',colour)
            colour=colour.split(', ')
            colour=[clr.strip() for clr in colour]
            colour=', '.join(colour)
            
            #colors
            repalce_words=re.findall(r"\w+",colour)
            for word in repalce_words:
                model=model.replace(word,'')
            #product_line
            repalce_words=re.findall(r"\w+",product_line)
            for word in repalce_words:
                model=model.replace(word,'')
            #material
            repalce_words=re.findall(r"\w+",material)
            for word in repalce_words:
                model=model.replace(word,'')
                model=model.replace(word.upper(),'')
                model=model.replace(word.title(),'')
                model=model.replace(word.lower(),'')

    
        
            #patterns of brand dooney and bourke and satchels and new and leather
            pattern=r"[d|D][o|O][o|O][n|N][e|E][y|Y]\s*\&*\s*[and]*[AND]*[And]*\s*[b|B][o|O]*[u|U][r|R][k|K|n|N][e|E]|[s|S][a|A][t|T][c|C][h|H][e|E][l|L][s|S]*|[n|N][e|E][w|W]|\:|[l|L][e|E][a|A][t|T][h|H][e|E][r|R]"
            model=re.sub(pattern,'',model)
            # matches=re.findall(pattern,model)
            # for match in matches:
            #     model=model.replace(match,'')
            model=' '.join(model.split())
            

            ner_query = description + condition

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            if record.get("Type")!="Satchels":
                return None

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": "Satchel",
                "brand": brand,
                "image": image,
                "ner_query": ner_query,
                "item_name": title,
                "title":record.get("title"),
                "color": colour, 
                "material":material.title(),  
                "model":model,            
                "price":{
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "msrp":{
                    "currency_code": "USD",
                    "amount": est_retail.replace("$","")
                },
                "product_line":product_line,
             
                "item_condition":record.get('item_condition',''),
                "length":length,
                "height":height,
                "width":width

            }
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
    