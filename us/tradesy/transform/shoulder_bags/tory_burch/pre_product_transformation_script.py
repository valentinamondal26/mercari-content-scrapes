import hashlib
import re
import json
from colour import Color

class Mapper(object):
    
    
    def colour_format(self,color_words):
        for word in color_words:
            for w in color_words:
                if word!=w:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,w)!=[]:
                        return w
                    
    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawled_date','')
            status_code = record.get('status_code','')
            description=record.get('description','')
            image = record.get('image','')
            price=str(record.get('price',''))
            est_retail=str(record.get('est_retail'))
            pattern=r"\d+\.*\d*"
            if re.findall(pattern,price)!=[]:
                price=re.findall(pattern,price)[0]
            if re.findall(pattern,est_retail)!=[]:
                est_retail=re.findall(pattern,est_retail)[0]
            measurements=record.get('Measurements','')
            pattern=r"\d+\.*\d*\"[l|L]{1}|\d+\.*\d*\"[h|H]{1}|\d+\.*\d*\"[w|W]{1}"
            matches = re.findall(pattern,measurements)
            length=''
            width=''
            height=''
            for match in matches:
                if "l" in match or "L" in match:
                    length=match.split('"')[0]
                if "h" in match or "H" in match:
                    height=match.split('"')[0]
                if "w" in match or "W" in match:
                    width=match.split('"')[0]


            model=record.get('Item Name','')            
            colour=record.get('color','')
            
            pattern=re.compile(r"[\w\d]+\-[a-zA-Z\d\-]*",re.IGNORECASE)
            colour=re.sub(pattern,'--',colour)
            colour=', '.join(colour.split('--'))
            if len(colour)>=2:
                if colour[len(colour)-2]==",":
                    colour=colour[:-2]
                    
            if "," not in colour and colour!='':
                color_words=re.findall(r"\w+",colour)
                wr=self.colour_format(color_words)
                if wr:
                    w1=' '.join(color_words[0:color_words.index(wr)])
                    w2=", ".join(color_words[color_words.index(wr)+1:])
                    if w2!='':
                        colour=w1+", "+w2
                    else:
                        colour=w1
                        
            if colour=='':
                    words_title=re.findall(r"\w+",record.get('title',''))
                    _color = [i for i in words_title if self.check_color(i)]
                    colour=", ".join(_color)
                    
            
            for word in re.findall(r"\w+",record.get('Style Name','')):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            
            for word in re.findall(r"\w+",record.get('Material','')):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            
            for word in colour.split(", "):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
                
            pattern=re.compile(r"Leather|shoulder|Handbags*|bags*|Tory|Burch|[\_\-\+\,\(\)\[\]\\\/\.\*]|cross\s*body|tote|\$\s*\d+\.*\d*",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            

            

            model=' '.join(model.split())
            pattern=r"^[a|A][n|N][d|D]$"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')
            model=' '.join(model.split())
            

            ner_query = description + record.get('item_condition','')

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": "Shoulder Bags",
                "brand": record.get('Brand'),
                'description':description,
                "image": image,
                "ner_query": ner_query,
                "item_name": record.get('Item Name',''),
                "title":record.get("title"),
                "color": colour,   
                "model":model,
                "material":record.get('Material',''),            
                "price":{
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "msrp":{
                    "currency_code": "USD",
                    "msrp": est_retail.replace("$","")
                },
                "item_condition":record.get('item_condition',''),
                "product_line":record.get('Style Name',''),
                "length":length,
                "height":height,
                "width":width


            }
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

