import hashlib
import re
import json
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawled_date','')
            status_code = record.get('status_code','')
            category = record.get('category','')
            description=record.get('description','')
            image = record.get('image','')
            colour = record.get('Color','')
            price=str(record.get('price',''))
            position=price.find("$")
            price=price[position+1:]
            measurements=record.get('Measurements','')
            pattern=r"\d+\.*\d*\"[l|L]{1}|\d+\.*\d*\"[h|H]{1}|\d+\.*\d*\"[w|W]{1}"
            matches = re.findall(pattern,measurements)
            length=''
            width=''
            height=''
            for match in matches:
                if "l" in match or "L" in match:
                    length=match.split('"')[0]
                if "h" in match or "H" in match:
                    height=match.split('"')[0]
                if "w" in match or "W" in match:
                    width=match.split('"')[0]
            condition=record.get('item_condition','').replace('\n',' ')


            model=record.get('Item Name','')            
            colors=colour.split(' ')
            model_words=model.split()
            for word in colors:
                if word.lower() in model_words:
                    del model_words[model_words.index(word.lower())]
                if word.upper() in model_words:
                    del model_words[model_words.index(word.upper())]
                elif word in model_words:
                    del model_words[model_words.index(word)]

            model=model.replace(record.get('Style Name',''),'')
            model=model.replace(record.get('Material',''),'')
            model=model.replace(record.get('item_condition',''),'')
            #pattern to remove words burke,shoulder bags,leather
            words_to_remove=r"[B|b][u|U][r|R][k|K][e|E]|[l|L][e|E][a|A][t|T][h|H][e|E][r|R]|[s|S][h|H][o|O][u|U][l|L][d|D][e|E][r|R]\s*[B|b][a|A][g|G][s|S]*|[D|d][o|O][n|N][n|N][e|E][y|Y]|[B|b][o|O][u|U][r|R][n|N][e|E]"
            matches=re.findall(words_to_remove,model)
            for match in matches:
                model=model.replace(match,'')
            #patterns of brand dooney and bourke
            pattern=r"[d|D][o|O][o|O][n|N][e|E][y|Y]\s*\&*\s*[and]*[AND]*[And]*\s*[b|B][o|O][u|U][r|R][k|K|n|N][e|E]"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')
            
            category_one=category[:-1]
            category_words=re.findall(r"\w+",record.get('Brand',''))
            for word in category_words:
                model=model.replace(word.lower(),'')
                model=model.replace(word.upper(),'')
                model=model.replace(word.title(),'')
            
            words_model=re.findall(r"\w+",model)
            _color = [i for i in words_model if self.check_color(i)]
            for color in _color:
                if color:
                    model = model.replace(color, '')

            model=model.replace(',','')
            model=model.replace('-','')
            model=model.replace('+','')
            model=model.replace('&','')
            model=model.replace(category,'')
            model=model.replace(category.lower(),'')
            model=model.replace(category.upper(),'')
            model=model.replace(category_one,'')
            model=model.replace(category_one.lower(),'')
            model=model.replace(category_one.upper(),'')

            model=' '.join(model.split())
            pattern=r"^[a|A][n|N][d|D]$"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')
            model=' '.join(model.split())
            

            ner_query = description + record.get('item_condition','')

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": "Shoulder Bags",
                "brand": record.get('Brand'),
                'description':description,
                "image": image,
                "ner_query": ner_query,
                "item_name": record.get('Item Name',''),
                "title":record.get("title"),
                "color": colour,   
                "model":model,
                "material":record.get('Material',''),            
                "price":{
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "item_condition":condition,
                "product_line":record.get('Style Name',''),
                "length":length,
                "height":height,
                "width":width


            }
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

