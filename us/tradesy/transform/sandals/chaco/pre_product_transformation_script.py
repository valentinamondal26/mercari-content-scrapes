import hashlib
import re
import json


class Mapper(object):
    def map(self, record):
        if record:
            id = record.get('id', '')
            crawl_date = record.get('crawled_date', '')
            status_code = record.get('status_code', '')
            category = record.get('category', '')
            description = record.get('description', '')
            image = record.get('image', '')
            title = record.get('Item Name', '')
            colour = record.get('Color', '')
            price = str(record.get('price', ''))
            size = record.get('Size', '')
            condition = record.get('item_condition', '')
            position = price.find("$")
            price = price[position+1:]

            model = record.get('Item Name', '')

            colors = colour.split(' ')
            model_words = model.split()
            for word in colors:
                if word.lower() in model_words:
                    del model_words[model_words.index(word.lower())]
                if word.upper() in model_words:
                    del model_words[model_words.index(word.upper())]
                elif word in model_words:
                    del model_words[model_words.index(word)]
            model = ' '.join(model_words)

            # pattern to remove Gender [ Women's,Men's,women's,men's,WOMEN'S,MEN'S,chacos,CHACOS,Chacos,chaco's,CHACO's,Chaco's,SIZE,size]
            pattern = r"[w|W]{1}[o|O]{1}[m|M]{1}[e|E]{1}[n|N]{1}\'*[s|S]*|[m|M]{1}[e|E]{1}[n|N]{1}\'[s|S]*|[c|C]{1}[h|H]{1}[a|A]{1}[c|C]{1}[o|O]{1}\'*[s|S]*|[s|S]{1}[i|I]{1}[z|Z]{1}[e|E]{1}"
            matches = re.findall(pattern, model)
            if matches != []:
                for match in matches:
                    model = model.replace(match, '')
            size_words = size.split()

            if len(size_words) > 1:
                #pattern to remove eg: SIZE 9.00 M ....<model name>
                pattern = r"%s[\.]*[0]{0,2}\s*M{1}" % size_words[1]
                matches = re.findall(pattern, model)
                for match in matches:
                    model = model.replace(match, '')
            for size_word in size_words:
                model = model.replace(size_word, '')
            category_one = category[:-1]
            model = model.replace(',', '')
            model = model.replace('-', '')
            model = model.replace(category, '')
            model = model.replace(category.lower(), '')
            model = model.replace(category.upper(), '')
            model = model.replace(category_one, '')
            model = model.replace(category_one.lower(), '')
            model = model.replace(category_one.upper(), '')

            model = ' '.join(model.split())

            ner_query = description + condition

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": "Sandals",
                "brand": record.get('Brand'),
                "image": image,
                "ner_query": ner_query,
                "item_name": title,
                "title": record.get("title"),
                "color": colour,
                "model": model,
                "price": {
                    "currency_code": "USD",
                    "amount": price.replace("$", "")
                },
                "us_shoe_size_womens": size,
                "heel_height": record.get('Heel Height', ''),
                "item_condition": record.get('item_condition', ''),
                "shoe_width": record.get('Width', '')


            }
            return transformed_record
        else:
            return None
