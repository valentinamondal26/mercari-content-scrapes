#Key Mapper for tardesy data - model + material + color  combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]
                    break
        for entity in entities:
            if "material" == entity['name']:
                if entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]
                    break

        for entity in entities:
            if "color" == entity['name']:
                if entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]
                    break

        return key_field
