BOT_NAME = 'tradesy'

SPIDER_MODULES = ['tradesy.spiders']
NEWSPIDER_MODULE = 'tradesy.spiders'

USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

DOWNLOAD_DELAY = 0.5

#By default auto_throttle strat - max delay is 5s to 60s
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_TARGET_CONCURRENCY = 64

ROBOTSTXT_OBEY = True

COOKIES_ENABLED = False

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'tradesy.pipelines.GCSPipeline': 300,
}

FEED_FROMAT = "jsonlines"

# GCS settings - actual value be 'raw/tradesy.com/shoulder_bags/coach/2019-02-06_00_00_00/json/tradesy.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}