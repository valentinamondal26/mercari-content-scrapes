import scrapy
import datetime
import unicodedata
import os
import random
import re
from collections import OrderedDict
from scrapy.exceptions import CloseSpider


class TradesySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from tradesy.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags

    brand : str
        category to be crawled, Supports
        1) Coach
        2) Micheal Kors

    Command e.g:
    scrapy crawl tradesy -a category='Shoulder Bags' -a brand='Coach'
    """

    name = 'tradesy'
    category = None
    brand = None
    source = 'tradesy.com'
    blob_name = 'tradesy.txt'
    base_url="https://www.tradesy.com"
    start_urls=["https://www.tradesy.com/coach-shoulder-bags/"]

    proxy_pool = [
           'http://shp-mercari-us-d00001.tp-ns.com/',
            'http://shp-mercari-us-d00002.tp-ns.com/',
            'http://shp-mercari-us-d00003.tp-ns.com/',
            # 'http://shp-mercari-us-d00004.tp-ns.com/',
            'http://shp-mercari-us-d00005.tp-ns.com/',
            'http://shp-mercari-us-d00006.tp-ns.com/',
            'http://shp-mercari-us-d00007.tp-ns.com/',
            'http://shp-mercari-us-d00008.tp-ns.com/',
            'http://shp-mercari-us-d00009.tp-ns.com/',
            'http://shp-mercari-us-d00010.tp-ns.com/',
            'http://shp-mercari-us-d00011.tp-ns.com/',
            'http://shp-mercari-us-d00012.tp-ns.com/',
            'http://shp-mercari-us-d00013.tp-ns.com/',
            'http://shp-mercari-us-d00014.tp-ns.com/',
            'http://shp-mercari-us-d00015.tp-ns.com/',
            'http://shp-mercari-us-d00016.tp-ns.com/',
            'http://shp-mercari-us-d00017.tp-ns.com/',
            'http://shp-mercari-us-d00018.tp-ns.com/',
            'http://shp-mercari-us-d00019.tp-ns.com/',
            'http://shp-mercari-us-d00020.tp-ns.com/',
    ]
    url_dict = {
        "Women's Shoulder Bags" : {
            'Coach': 'https://www.tradesy.com/coach-shoulder-bags/',
            'Micheal Kors': 'https://www.tradesy.com/michael-kors-shoulder-bags/',
            'Kate Spade':'https://www.tradesy.com/kate-spade-shoulder-bags/',
            'Dooney & Bourke':'https://www.tradesy.com/dooney-and-bourke-bags/'
        },
            "Shoulder Bags":{
                'Tory Burch':'https://www.tradesy.com/tory-burch-shoulder-bags/'
            },

        'Sandals':{
           'Chaco':'https://www.tradesy.com/chaco-womens-sandals/',
           'Chanel':'https://www.tradesy.com/chanel-womens-sandals/',
           'Gucci':'https://www.tradesy.com/gucci-womens-sandals/',
           'Tory Burch':'https://www.tradesy.com/tory-burch-womens-sandals/'
        },
        'Bracelets':{
            'Alex and Ani':'https://www.tradesy.com/alex-and-ani-womens-bracelets/',
            'David Yurman':'https://www.tradesy.com/david-yurman-womens-bracelets/'
        },
        'Earrings':{
            'Chanel':'https://www.tradesy.com/chanel-womens-earrings/',
            'Kendra Scott':'http://tradesy.com/kendra-scott-womens-earrings/'
            },
        'Tote Bags':{
             'Coach' :'https://www.tradesy.com/coach-totes/',
             'Kate Spade':'https://www.tradesy.com/kate-spade-totes/',
             'Dooney & Bourke':'https://www.tradesy.com/dooney-and-bourke-bags/',
             'Tory Burch':"https://www.tradesy.com/tory-burch-totes/"
            },
        'Crossbody Bags':{
            'Coach':'https://www.tradesy.com/coach-cross-body-bags/',
            'Dooney & Bourke':'https://www.tradesy.com/dooney-and-bourke-bags/',
            'Kate Spade':'https://www.tradesy.com/kate-spade-bags/',
            'Tory Burch':'https://www.tradesy.com/tory-burch-cross-body-bags/'
            },
        'Satchel':{
            'Coach':"https://www.tradesy.com/coach-satchels/",
            'Dooney & Bourke':"https://www.tradesy.com/dooney-and-bourke-bags/",
            'Kate Spade':'https://www.tradesy.com/kate-spade-bags/'
            },
        'Wallets':{
            'Coach':'https://www.tradesy.com/coach-womens-wallets/',
            'Kate Spade':'https://www.tradesy.com/kate-spade-womens-wallets/'
            },
        'sunglasses':{
            'Gucci':'https://www.tradesy.com/gucci-womens-sunglasses/',
            'Quay Australia':'https://www.tradesy.com/quay-womens-sunglasses/',
            'Ray-Ban':'https://www.tradesy.com/ray-ban-womens-sunglasses/'
            },
        'Pumps':{
            'Christian Louboutin':'https://www.tradesy.com/christian-louboutin-womens-pumps/'
            },
        'Rings':{
            'David Yurman':'https://www.tradesy.com/david-yurman-womens-rings/',
            'Kay Jewelers':'https://www.tradesy.com/kay-jewelers-womens-rings/'
            },
        "Women's Boots":{
            'Dr. Martens':"https://www.tradesy.com/dr-martens-womens-boots-and-booties/",
            'Hunter':'https://www.tradesy.com/hunter-womens-boots-and-booties/',
            'UGG Australia':'https://www.tradesy.com/ugg-australia-womens-boots-and-booties/'
            },
        'Backpacks':{
            'Fjallraven':'https://www.tradesy.com/fjallraven-backpacks/',
            'Michael Kors':'https://www.tradesy.com/michael-kors-bags/',
            "PINK":"https://www.tradesy.com/pink-backpacks/",
            "Vera Bradley":"https://www.tradesy.com/vera-bradley-backpacks/"
            },
        'Necklaces':{
                'Kendra Scott':'https://www.tradesy.com/kendra-scott-womens-necklaces/'
                },
        'Above knee, mini':{
                'Lilly Pulitzer':'https://www.tradesy.com/lilly-pulitzer-womens-casual-dresses-short/'
                },
        "Women's Blouses":{
                'Torrid':'https://www.tradesy.com/torrid-womens-blouses/'
                }



    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TradesySpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        print('launch_url', self.launch_url)
        if self.launch_url:
            yield scrapy.Request(url=self.launch_url, callback=self.parse)


    def get_url(self,category,brand):
        try:
            return self.url_dict[category][brand]
        except Exception as e:
            print('Exception occured:', e)
            raise(ValueError('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))


    def parse(self, response):
        """
        @url https://www.tradesy.com/coach-satchels/
        @returns requests 37
        @meta {"use_proxy":"True"}
        """
        for link in response.xpath('//a[@class="item-image"]/@href').getall():
                yield self.createRequest(url=self.base_url+ link, callback=self.parse_spec)


        next_page_url = response.xpath('//li[@class="page-link"]/a[@id="page-next"]/@href').get()
        print(next_page_url,"\n\nNEXT PAGE\n\n")
        if next_page_url:
            yield self.createRequest(self.base_url+next_page_url, self.parse)

    def parse_spec(self,response):
        """
        @url https://www.tradesy.com/i/coach-swagger-fatiguebrown-leather-satchel/25861221/
        @scrape_values title price item_condition description est_retail
        @meta {"use_proxy":"True"}
        """
        ###addd the spec items in scrape_values

        item = dict()
        item["crawled_date"] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"] = str(response.status)
        item["category"] = self.category
        item['image']=response.xpath('//img[@id="product_page_image_0"]/@src').get(default='')
        #item["brand"] = self.brand
        item["id"] = response.url
        item["title"] =response.xpath('normalize-space(//span[@id="idp-title"]/text())').get(default='')

        #original listing price
        price = response.xpath('normalize-space(//div[@class="item-price-original-list"]/span/text())').get()
        if not price:
            #final sale
            price=response.xpath('normalize-space(//div[@class="item-price"]/text())').get()

        est_retail=response.xpath('normalize-space(//div[@class="item-price-retail"]/text())').get(default='')

        price = unicodedata.normalize("NFKD",price)
        item['price']=price
        item_condition=response.xpath('//div[@class="idp-condition idp-info-accordion"]//div//text()').getall()
        condition=[]
        for con in item_condition:
            if len(re.findall(r"\w+",con))>=1:
                condition.append(con)
        item['item_condition']=' '.join(condition)
        item['description']=response.xpath('normalize-space(//div[@class="idp-description idp-info-accordion"]//p//text())').get(default='')
        item['est_retail']=est_retail
        details=response.xpath('//div[@class="idp-details idp-info-accordion"]/div[1]/div')
        spec=OrderedDict()
        keys=[]
        values=[]
        for div in details:
            key=div.xpath('./p[1]/text()').getall()
            l1=[]
            for k in key:
                if len(re.findall(r"\w+",k))>=1:
                    k=k.replace(":",'')
                    l1.append(k.strip())
            keys.append(' '.join(l1))

            if  div.xpath('./p[2]/text()').get().strip()=='':
                value=div.xpath('./p[2]/a/text()').getall()
            else:
                value=div.xpath('./p[2]/text()').getall()

            l1=[]
            for v in value:
                if len(re.findall(r"\w+",v))>=1:
                    l1.append(v.strip())
            values.append(' '.join(l1))

        spec=spec.fromkeys(keys,'')
        for i in range(len(keys)):
            if spec[keys[i]]=='':
                spec[keys[i]]=values[i]

        item.update(spec)
        yield item


    def parse_products(self, response):
        crawled_date = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        status_code = str(response.status)

        est_retail = ''
        retail_price = response.xpath('//div[@class="item-price-retail"]//text()').extract_first()
        if retail_price:
            retail_price = unicodedata.normalize("NFKD",retail_price)
            est_retail = ''.join(re.findall(r'\$\s?\d+.\d+', retail_price))

        price = response.xpath('//div[@class="item-price-original-list"]/span/text()').extract_first()
        if not price:
            item_price = response.xpath('normalize-space(//div[@class="item-price"]/text())').extract_first()
            price = ''.join(re.findall(r'\$\s?\d+.\d+', item_price)) if item_price else ''

        if price:
            price = unicodedata.normalize("NFKD",price)

        details_list = response.css('div.idp-details p')
        material = ''
        measurements = ''
        color = ''
        style_name=''
        for position,details in enumerate(details_list):
                if details.css("p::text").get() == "Color:":
                    color = ', '.join(details_list[position+1].css("p a::text").getall()).strip()
                if details.css("p::text").get() == "Material:":
                    material = details_list[position+1].css("p::text").get().strip()
                if details.css("p::text").get() == "Measurements:":
                    measurements = details_list[position+1].css("p::text").get().strip()
                if details.css("p::text").get() == "Style Name:":
                    style_name = details_list[position+1].css("p::text").get().strip()


        description = response.xpath('normalize-space(//div[@class="idp-description idp-info-accordion"]//p)').get()
        breadcrumb = ' | '.join(response.xpath('//ul[@id="idp-breadcrumbs"]//text()[normalize-space(.)]').extract())
        condition = ', '.join(response.xpath('//div[@class="idp-condition idp-info-accordion"]/div//text()[normalize-space(.)]').extract())
        title = response.xpath('normalize-space(//span[@id="idp-title"]/text())').get()
        model = ''
        model_number = response.xpath('normalize-space(//span[@class="item-id"]/text())').get()
        if model_number:
            model = ''.join(re.findall(r'\d+', model_number))
        image = response.css("img#product_page_image_0::attr(src)").get()

        item = dict()
        item["crawled_date"] = crawled_date
        item["status_code"] = status_code
        item["category"] = self.category
        item["brand"] = self.brand
        item["id"] = response.url
        item["title"] = title
        item["model"] = model
        item["material"] = material
        item["condition"] = condition
        item["breadcrumb"] = breadcrumb
        item["description"] = description
        # item["item_dimensions"] = json.loads(item_dimensions)
        item['measurements'] = measurements
        item['style_name'] = style_name
        item['color'] = color
        item["price"] = price
        item['est_retail'] = est_retail
        item["image"] = image

        yield item


    def createRequest(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request