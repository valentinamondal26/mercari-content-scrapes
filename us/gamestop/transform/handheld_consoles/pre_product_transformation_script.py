import re

import hashlib
from colour import Color
class Mapper(object):

    def map(self, record):
        if record:

            record = self.pre_process(record)

            ner_query = record.get('description', record.get(
                'title', '')) + ' ' + record.get('features', '')
            ner_query = ner_query.replace('\n', ' ').replace('\r', ' ')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")

            title = record.get('title', '')
            title = re.sub(re.compile(r'^Nintendo 2DS XL', re.IGNORECASE), 'New Nintendo 2DS XL', title)

            colors_meta = [
                'Teal', 'Bronze', 'Burgundy',
                # 'Emblem Blue',
                # 'Pikachu Yellow', 'Pokemon Yellow',
                'Clear Ice', 'Pearl White', 'Crystal Black', 'Crystal Red', 'Crystal Blue',
                'Electric Yellow', 'Electric Blue', 'Electric Green', 'Scarlet Red',
                'Peach Pink', 'Sea Green', 'Aqua Blue', 'Flame Red', 'Cobalt Blue', 'Colbalt Blue',
                'Ice Blue', 'Solid Black', 'Midnight blue', 'Atomic Purple',
            ]
            colors = []
            if ' - ' in title:
                colors.append(title.rsplit(' - ', 1)[-1])
            match = re.findall(re.compile('|'.join(list(map(lambda x: r'\b'+x+r'\b', colors_meta))), re.IGNORECASE), title)
            _title = title
            if match:
                _title = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', match))), re.IGNORECASE), '', title)
                colors.extend(match)
            color_str = ', '.join(colors)
            colors.extend(
                [i for i in re.compile(r'\s|\/').split(_title) if self.check_color(i) and i not in color_str]
            )
            colors = [color.strip() for color in colors if color]
            colors = list(dict.fromkeys(colors))
            color = ', '.join(colors)
            color = re.sub(re.compile(r'\bSolid Black\b', re.IGNORECASE), 'Black', color)
            color = re.sub(re.compile(r'\bColbalt Blue\b', re.IGNORECASE), 'Cobalt Blue', color)
            color = re.sub(r'\s+', ' ', color).strip()

            if not color:
                color = record.get('Color', '')

            keywords = [
                'Only at Gamestop', 'Recharged', 'Premium', 'Refurbished', 'Gamestop Premium Refurbished',
                'Nintendo Refurbished', 'GameStop', 'System', 'with AC',
                'Premimum', 'Adapter and Stylus', 'Stylus'
            ]
            # keywords.extend(colors_meta)
            if colors:
                keywords.extend(colors)
            model = title.rsplit('-', 1)[0]
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = re.sub(r'-\s*$|\(\s*\)|\band\s*$', '', model)
            model = re.sub(r'\s\s+and\s\s+|\s\s+and\s+|\s+and\s\s+|\s+-\s+', ' ', model)
            if not re.findall(re.compile(r'\bNintendo\b', re.IGNORECASE), model):
                model = f'Nintendo {model}'
            model = re.sub(r'\s+', ' ', model).strip()

            meta_platform = [
                'NEW Nintendo 3DS XL', 'NEW Nintendo 3DS', 'Nintendo 3DS', 'Nintendo 3D XL',
                'NEW Nintendo 2DS XL', 'Nintendo 2DS',
                'Nintendo DS Lite', 'Nintendo DS', 'Nintendo DSI XL', 'Nintendo DSi',
                'Game Boy Color', 'Game Boy Advance SP', 'Game Boy Advance', 'Game Boy'
            ]
            platform = record.get('platform', '').strip()
            if not platform:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', meta_platform))), re.IGNORECASE), title)
                if match:
                    platform = match[0]

            platform_expected = ''
            if re.findall(re.compile(r'\bGame Boy Advance\b', re.IGNORECASE), title):
                platform_expected = 'Nintendo Game Boy Advance/Advanced SP'
            elif re.findall(re.compile(r'\bGame Boy\b', re.IGNORECASE), title) \
                and not re.findall(re.compile(r'\bAdvance\b', re.IGNORECASE), title):
                platform_expected = 'Nintendo Game Boy/Pocket/Color'
            elif re.findall(re.compile(r'\bNew Nintendo\b', re.IGNORECASE), title):
                platform_expected = 'NEW Nintendo 2DS XL, 3DS, 3DXL'
            elif re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', \
                ['DSi XL', 'DSi', 'DS Lite', 'DS']))), re.IGNORECASE), title):
                platform_expected = 'Nintendo DS/DS Lite/DSi/DSi XL'
            elif re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', \
                ['2DS', '3DS']))), re.IGNORECASE), title) \
                and not re.findall(re.compile(r'\bNEW Nintendo\b', re.IGNORECASE), title):
                platform_expected = 'Nintendo 2DS, 3DS, 3DS XL'


            if re.findall(re.compile(r"Blast from the past", re.IGNORECASE), title):
                return None

            category = ''
            if record['brand'] == 'Nintendo' and record['category'] == 'Handheld Consoles':##https://mercari.atlassian.net/browse/USCC-465?focusedCommentId=428819
                category = 'Video Game Consoles'
                if color:
                    colors = color.split(', ')
                    color = '/'.join(colors)
                
                if not re.findall(r'\bNintendo\b', model, flags=re.IGNORECASE):
                    model = f'Nintendo {model}'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', '').split(',')[0],
                "statusCode": record.get('statusCode', ''),

                "category": category or record.get('category'),
                "brand": record.get('brand', ''),

                "title": title,
                "breadcrumb": record.get('breadcrumb', ''),
                'image': record.get('image', ''),
                # "features": record.get('features', ''),
                "ner_query": ner_query,
                "description": record.get('description', ''),

                "model": model,
                "color": color,
                # "platform": platform,
                'platform': platform_expected,
                "item_condition": record.get('condition', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "est_retail": {
                    "currencyCode": "USD",
                    "amount": est_retail_price
                },
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
