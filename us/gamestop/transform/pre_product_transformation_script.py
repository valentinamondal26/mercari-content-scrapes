import re

import hashlib
from colour import Color
class Mapper(object):

    def map(self, record):
        if record:

            record = self.pre_process(record)
            transformed_record = dict()

            ner_query = record.get('description', record.get(
                'title', '')) + ' ' + record.get('features', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")
            title = record.get('title', '')
            platform = record.get('platform', '')

            storage_capacity = ''

            storage_capacity_pattern = re.compile(
                r"\d+\s*GB|\d+\s*MB|\d+\s*TB|\d+\s*KB", re.IGNORECASE)
            match = re.findall(storage_capacity_pattern, title)
            if match:
                storage_capacity = match[0]

            if not storage_capacity and platform == 'Nintendo Wii U':
                storage_capacity = '32GB'

            colors = [i for i in re.compile(
                r'\s|\/').split(title) if self.check_color(i)]
            title = re.sub(re.compile(
                '|'.join(colors), re.IGNORECASE), '', title)
            color = ','.join(colors)

            title = title.replace(storage_capacity, '')
            title = re.sub(re.compile(
                r"\s-\s|\s-|-\s|\s\/\s|\s\/|\/\s"), ' ', title)
            pattern = r"\({0,1}[g|G]{1}ame[s|S]{1}top\s*([p|P]{1}remium){0,1}\s*[r|R]{1}efurbished\){0,1}"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            title = re.sub(re.compile(r'®|™|[c|C]{1}onsole[s]*|system|Blast From The Past Bundle|Blast from the Past|(\(){0,1}supercharged Refurbished(\)){0,1}|'
                                      r'Includes \$\d+ in Added Value\!|(bc)|(\(){0,1}ReCharged Refurbished(\)){0,1}|\sand\s*$',
                                      re.IGNORECASE), '', title)
            title = re.sub(' +', ' ', title).strip()
            title = re.sub(re.compile(r"\bplay\s*station\b",
                                      re.IGNORECASE), "PlayStation", title)
            title = ' '.join(title.split())

            if platform == 'Nintendo 3DS and 3DS XL and 2DS':
                title_lower = title.lower()
                if '3DS XL'.lower() in title_lower:
                    platform = 'Nintendo 3DS XL'
                elif '3DS'.lower() in title_lower:
                    platform = 'Nintendo 3DS'
                elif '2DS XL'.lower() in title_lower:
                    platform = 'Nintendo 2DS XL'
                elif '2DS'.lower() in title_lower:
                    platform = 'Nintendo 2DS'

            if re.findall(re.compile(r"Geek squad certified|name your game bundle", re.IGNORECASE), record.get("title")) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', ''),
                "model": title,
                "color": color,
                "platform": platform,
                "features": record.get('features', ''),
                "condition": record.get('condition', ''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "est_retail": {
                    "currencyCode": "USD",
                    "amount": est_retail_price
                },
                "storage_capacity": storage_capacity,
                "memory": record.get('memory', '')
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
