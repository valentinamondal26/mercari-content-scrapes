import re

import hashlib
from colour import Color


class Mapper(object):

    def map(self, record):
        if record:

            record = self.pre_process(record)
            transformed_record = dict()

            ner_query = record.get('description', record.get(
                'title', '')) + ' ' + record.get('features', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")
            title = record.get('title', '')
            platform = record.get('platform', '')

            storage_capacity = ''

            storage_capacity_pattern = re.compile(
                r"\d+\s*GB|\d+\s*MB|\d+\s*TB|\d+\s*KB", re.IGNORECASE)
            match = re.findall(storage_capacity_pattern, title)
            if match:
                storage_capacity = match[0]

            color = ''
            if not re.findall(re.compile(r"Red Dead Redemption", re.IGNORECASE), title):
                colors = [i for i in re.compile(
                    r'\s|\/').split(title) if self.check_color(i)]
                for ctr, word in enumerate(colors):
                    if re.findall(re.compile(f"[{word}]*\s*Camouflage\s*[{word}]*|[{word}]*\s*Azurite\s*[{word}]*|[{word}]*\s*Glacier\s*[{word}]*", re.IGNORECASE), title):
                        colors[ctr] = re.findall(re.compile(
                            f"[{word}]*\s*Camouflage\s*[{word}]*|[{word}]*\s*Azurite\s*[{word}]*|[{word}]*\s*Glacier\s*[{word}]*", re.IGNORECASE), title)[0].strip()
                title = re.sub(re.compile(
                    '|'.join(colors), re.IGNORECASE), '', title)
                color = ','.join(colors)

            color_meta = ['Wood', 'Bullet Hole']
            if color == '':
                for word in color_meta:
                    if re.findall(re.compile(r"\b{}\b".format(word), re.IGNORECASE), title):
                        color = word
                        title = re.sub(re.compile(r"\b{}\b".format(
                            word), re.IGNORECASE), "", title)
            if color == '':
                color = record.get("Color", "")

            title = title.replace(storage_capacity, '')
            title = re.sub(re.compile(
                r"\s-\s|\s-|-\s|\s\/\s|\s\/|\/\s"), ' ', title)
            pattern = r"\({0,1}[g|G]{1}ame[s|S]{1}top\s*([p|P]{1}remium){0,1}\s*[r|R]{1}efurbished\){0,1}"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            if not re.findall(r'\bPlaystation 5\b', platform, flags=re.IGNORECASE):
                title = re.sub(r'\bDigital\b', '', title, flags=re.IGNORECASE)

            title = re.sub(re.compile(r'with wireless controller|\bComplete\b|Refurbished|GameStop Premium Refurbished|\(\s*\)|Credit|gamestop|®|™|[c|C]{1}onsole[s]*|system|'
                                      r'Blast From The Past Bundle|Blast from the Past|(\(){0,1}supercharged Refurbished(\)){0,1}|'
                                      r'Includes \$\d+ in Added Value\!|(bc)|(\(){0,1}ReCharged Refurbished(\)){0,1}|\sand\s*$|Only at GameStop|Sony Refurbished|Refurbished|\bPallet\b|',
                                      re.IGNORECASE), '', title)
            title = re.sub(' +', ' ', title).strip()
            title = re.sub(re.compile(r"\bplay\s*station\b",
                                      re.IGNORECASE), "PlayStation", title)
            title = re.sub(re.compile(r"PlayStation\s*(\d)",
                                      re.IGNORECASE), r"PlayStation \1", title)
            if re.compile(r"PlayStation 2 System Complete \(GameStop Refurbished\)|"
                          r"PlayStation2 System \- Silver \(GameStop Premium Refurbished\)|"
                          r"PlayStation2 System \- White \(GameStop Premium Refurbished\)", re.IGNORECASE).match(record.get('title', '')):
                title = re.sub(re.compile(r"PlayStation\s*(\d)", re.IGNORECASE),
                               r"PlayStation \1 Slim", title)
            if re.compile(r"PlayStation3 12GB System \(GameStop Premium Refurbished\)|"
                          r"PlayStation3 250GB System \- Azurite Blue \(GameStop Premium Refurbished\)|"
                          r"PlayStation3 500GB System \- White \(GameStop Premium Refurbished\)|"
                          r"PlayStation3 500GB System \- Red \(GameStop Premium Refurbished\)", re.IGNORECASE).match(record.get('title', '')):
                title = re.sub(re.compile(r"PlayStation\s*(\d)", re.IGNORECASE),
                               r"PlayStation \1 Super Slim", title)
            title = re.sub(re.compile(
                r"\bSLIM\b", re.IGNORECASE), "Slim", title)
            title = re.sub(re.compile(r"\bBackward\b",
                                      re.IGNORECASE), "Backwards", title)
            title = ' '.join(title.split())
            if re.compile(r"^PlayStation 4 Pro Monster Hunter: World Limited Edition Bundle$", re.IGNORECASE).match(title):
                title = re.sub(re.compile(
                    r"\bBundle\b", re.IGNORECASE), "", title)
            title = ' '.join(title.split())

            title_color_map = {
                "PlayStation": "Gray",
                "PlayStation 2": "Black",
                "PlayStation 2 Slim": "Black",
                "PlayStation 3": "Black",
                "PlayStation 3 2 USB": "Black",
                "PlayStation 3 Backwards Compatible": "Black",
                "PlayStation 3 Slim": "Black",
                "PlayStation 3 Super Slim": "Black",
                "PlayStation 4 Only on PlayStation Bundle": "Black",
                "PlayStation Classic": "Gray",
                "PlayStation Slim": "Gray",
                "PlayStation 3 Slim Arctic": "Arctic Camouflage",
                "PlayStation 3 Slim Camo": "Pink Camouflage",
                "PlayStation 3 Slim Jungle": "Jungle Green",
                "PlayStation 4 Call of Duty\: Ops III": "",
                "PlayStation 4 Destiny\: The Taken King Limited Edition": "",
                "PlayStation 4 Pro 500 Million Limited Edition": "",
                "PlayStation 4 Pro Call of Duty\: Modern Warfare Bundle": "",
                "PlayStation 4 Pro Kingdom Hearts III Limited Edition": "",
                "PlayStation 4 Pro Monster Hunter: World Limited Edition": "",
                "PlayStation 4 Pro Red Dead Redemption II Bundle": "",
                "PlayStation 4 Pro Spider-Man Limited Edition": "",
                "PlayStation 4 Pro Star Wars: Battlefront II": "",
                
            }
            for key, value in title_color_map.items():
                if re.compile(r"^{}$".format(key), re.IGNORECASE).match(title) and (color == '' or value == ''):
                    color = value
                    break
                if re.compile(r"^{}$".format(key), re.IGNORECASE).match(title) and re.findall(re.compile(r"\bArctic\b|\bcamo\b|\bjungle\b", re.IGNORECASE), title):
                    color = value
                    break
            title = re.sub(re.compile(
                r"\bArctic\b|\bcamo\b|\bjungle\b", re.IGNORECASE), "", title)
            title = " ".join(title.split())
            if platform == '' or not re.findall(r"\w+", platform):
                platform = record.get("breadcrumb", "").split("|")[-2]

            if not storage_capacity and platform.lower() == 'PlayStation 5'.lower():
                storage_capacity = '825GB'

            if (record.get("condition", "") in ["Pre-Owned", "Refurbished"] or platform.lower() == 'PlayStation 5'.lower()) \
                and re.findall(r"basics bundle|bundle", record.get("title", ""), flags=re.IGNORECASE):
                # or (platform.lower() == 'PlayStation 5'.lower() and re.findall(r"bundle", record.get("title", "") flags=re.IGNORECASE)):
                return None

            if re.findall(re.compile(r"Geek squad certified|name your game bundle|Blast from the past", re.IGNORECASE), record.get("title")) != []:
                return None

            models_not_to_include = ["PlayStation 4 Slim Black 1TB Spider-Man GOTY Basics System Bundle",
                                     "PlayStation 4 Slim Black 1TB Call of Duty: Modern Warfare Basics System Bundle",
                                     "PlayStation VR"]
            if re.findall(re.compile("|".join(models_not_to_include), re.IGNORECASE), record.get('title', '')) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', ''),
                "model": title,
                "color": color,
                "platform": platform,
                "features": re.sub(r"\n|\t|\r", "", record.get('features', '')).strip(),
                "condition": record.get('condition', ''),
                "ner_query": re.sub(r"\n|\t|\r", "", ner_query).strip(),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "est_retail": {
                    "currencyCode": "USD",
                    "amount": est_retail_price
                },
                "storage_capacity": storage_capacity.replace(" ", ""),
                "memory": record.get('memory', '')
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
