import hashlib
from colour import Color
import re


class Mapper(object):

    def map(self, record):
        if record:

            record = self.pre_process(record)
            transformed_record = dict()

            ner_query = record.get('description', record.get(
                'title', '')) + ' ' + record.get('features', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")
            title = record.get('title', '')
            platform = record.get('platform', '')

            storage_capacity = ''

            storage_capacity_pattern = re.compile(
                r"\d+\s*GB|\d+\s*MB|\d+\s*TB|\d+\s*KB", re.IGNORECASE)
            match = re.findall(storage_capacity_pattern, title)
            if match:
                storage_capacity = match[0]

            if not storage_capacity and platform == 'Nintendo Wii U':
                storage_capacity = '32GB'

            colors = []
            if re.findall(re.compile(r"Neon\s\w*\s", re.IGNORECASE), title):
                for color in re.findall(re.compile(r"Neon\s\w*\s", re.IGNORECASE), title):
                    colors.append(color.strip())
                    title = re.sub(re.compile(r"\b{}\b".format(
                        color), re.IGNORECASE), "", title)
            primary_colors = [i.strip() for i in re.compile(
                r'\s|\/').split(title) if self.check_color(i)]
            title = re.sub(re.compile(
                '|'.join(list(map(lambda x: r'\b'+x+r'\b', primary_colors))), re.IGNORECASE), '', title)
            colors.extend(primary_colors)
            colors = list(filter(None, colors))
            color = '/'.join(colors)

            title = title.replace(storage_capacity, '')
            title = re.sub(re.compile(
                r"\s-\s|\s-|-\s|\s\/\s|\s\/|\/\s"), ' ', title)
            pattern = r"\({0,1}[g|G]{1}ame[s|S]{1}top\s*([p|P]{1}remium){0,1}\s*[r|R]{1}efurbished\){0,1}"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            title = re.sub(re.compile(r'®|™|[c|C]{1}onsole[s]*|system|Blast From The Past Bundle|Blast from the Past|(\(){0,1}supercharged Refurbished(\)){0,1}|'
                                      r'Includes \$\d+ in Added Value\!|(bc)|(\(){0,1}ReCharged Refurbished(\)){0,1}|\sand\s*$',
                                      re.IGNORECASE), '', title)
            title = re.sub(r'\s+', ' ', title).strip()
            title = re.sub(re.compile(
                r"\bwith Joy\s*\-*\s*Cons*|Joy\s*\-*\s*Cons*|\bwith and\b", re.IGNORECASE), "", title)
            title = re.sub(re.compile(
                r"Ships\s*by\s*\w+\s*\d+", re.IGNORECASE), "", title)
            title = re.sub(r'\s+', ' ', title).strip()
            if re.compile(r"^Nintendo Wii U$", re.IGNORECASE).match(title):
                title = title + " " + storage_capacity
                title = " ".join(title.split())

            condition = record.get('condition', '')

            if not title \
                    or re.findall(re.compile(r"\beShop\b|\bCredit\b|\bDigital\b|^Nintendo Switch Animal Crossing\: New Horizons Edition with Digital Animal Crossing\: New Horizons Bundle$", re.IGNORECASE), record.get("title", "")) \
                    or (re.findall(re.compile(r'\bpre-owned\b|\brefurbished\b', re.IGNORECASE), condition) and re.findall(re.compile(r'\bBundle\b', re.IGNORECASE), record.get("title", ""))):
                return None
            
            if not re.findall(r'\bNintendo\b', title, flags=re.IGNORECASE):
                title = f'Nintendo {title}'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "title": record.get('title', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', '').replace("\n", "").replace("\r", ""),
                "features": record.get('features', '').replace("\n", "").replace("\r", "").strip(" ,"),
                "ner_query": ner_query.replace("\n", "").replace("\r", ""),

                "model": title,
                "color": color,
                "platform": platform,
                "condition": condition,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "est_retail": {
                    "currencyCode": "USD",
                    "amount": est_retail_price
                },
                "storage_capacity": storage_capacity,
                "memory": record.get('memory', '')
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
