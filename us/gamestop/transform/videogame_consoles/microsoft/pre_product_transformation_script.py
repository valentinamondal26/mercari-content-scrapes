import hashlib
from colour import Color
import re


class Mapper(object):

    def map(self, record):
        if record:

            record = self.pre_process(record)
            transformed_record = dict()

            description = record.get('description', '')
            description = description.replace(
                '\n', ' ').replace('\r', ' ').replace('\t', ' ')
            description = re.sub(r'\s+', ' ', description)

            features = record.get('features', '')
            features = features.replace('\n', ' ').replace(
                '\r', ' ').replace('\t', ' ')
            features = re.sub(r'\s+', ' ', features)

            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")
            title = record.get('title', '')
            platform = record.get('platform', '')

            storage_capacity = ''

            storage_capacity_pattern = re.compile(
                r"\d+\s*GB|\d+\s*MB|\d+\s*TB|\d+\s*KB", re.IGNORECASE)
            match = re.findall(storage_capacity_pattern, title)
            if match:
                storage_capacity = match[0]

            if not storage_capacity and platform == 'Nintendo Wii U':
                storage_capacity = '32GB'

            colors = [i for i in re.compile(
                r'\s|\/').split(title) if self.check_color(i)]
            title = re.sub(re.compile(
                '|'.join(colors), re.IGNORECASE), '', title)
            colors = list(filter(None, colors))
            color = ', '.join(colors)

            if not color:
                color = record.get('Color', '')

            if re.compile(r"Xbox One X NBA 2K20 Special Edition Bundle 1TB|"
                          r"Xbox One X Fallout 76 Bundle 1TB\s*Only at GameStop",
                          re.IGNORECASE).match(record.get('title', '')) and color == '':
                color = "White"
            if re.compile(r"Xbox 360 White with Wired Controller|Xbox 360 System with Wired Controller", re.IGNORECASE).match(record.get("title", "")):
                color = "Black, White"
            if re.compile(r"Xbox 360 \(E\) System 4GB", re.IGNORECASE).match(record.get("title")) and color == "":
                color = "Black"
            if re.findall(re.compile(r"Deep\s*\w+\s*Special Edition|\w+\s*Rush\s*Special Edition", re.IGNORECASE), record.get("title", "")):
                color = re.findall(re.compile(
                    r"Deep\s*\w+\s*Special Edition|\w+\s*Rush\s*Special Edition", re.IGNORECASE), record.get("title", ""))[0]
                title = re.sub(re.compile(r"|".join(re.findall(
                    r"\w+", color)), re.IGNORECASE), "", title)
            if re.findall(re.compile(r"Jungle|Camo|Wood|Arctic|Bullet Hole", re.IGNORECASE), title) and color != '':
                color = re.findall(re.compile(
                    r"Jungle|Camo|Wood|Arctic|Bullet Hole", re.IGNORECASE), title)[0]+" "+color
                title = re.sub(re.compile(
                    r"Jungle|Camo|Wood|Arctic|Bullet Hole|Pallet", re.IGNORECASE), "", title)

            title = title.replace(storage_capacity, '')
            title = re.sub(re.compile(
                r"\s-\s|\s-|-\s|\s\/\s|\s\/|\/\s"), ' ', title)
            pattern = r"\({0,1}[g|G]{1}ame[s|S]{1}top\s*([p|P]{1}remium){0,1}\s*[r|R]{1}efurbished\){0,1}"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            title = re.sub(re.compile(r'®|™|[c|C]{1}onsole[s]*|system|Blast From The Past Bundle|Blast from the Past|(\(){0,1}supercharged Refurbished(\)){0,1}|'
                                      r'Includes \$\d+ in Added Value\!|(bc)|(\(){0,1}ReCharged Refurbished(\)){0,1}|\sand\s*$',
                                      re.IGNORECASE), '', title)
            title = re.sub(re.compile(
                r"\bwith Xbox Wireless Controller White, and Xbox One Chat Communicator Headset\b|"
                r"\bOnly at GameStop\b|\bonline only\b|\bwith Chat Headset\b|"
                r"\bwith Original Controller\b|\bMicrosoft Refurbished\b|"
                r"\bwith 3.5mm Jack Controller\b|\bPremium Refurbished\b|"
                r"\bwith Wired Controller\b|"
                r"\bwith Wireless Controller\b|\bwith Wi Controller\b|\(|\)",
                re.IGNORECASE), "", title)
            title = re.sub(r'\s+', ' ', title).strip()
            title = ' '.join(title.split())
            if title == 'Xbox 360 S Star Wars':
                title = 'Xbox 360 S Star Wars Edition'
            title = re.sub(r'(.*)(\bXbox Series \w\b)(.*)', r'\2 \1\3', title, flags=re.IGNORECASE)
            title = re.sub(r'\s+', ' ', title).strip()

            storage_capacity = re.sub(r'\s+', '', storage_capacity)

            if re.findall(re.compile(r"Geek squad certified|name your game bundle' \
                r'|Blast From the Past|^Xbox One Best\-of Shooter Bundle$",
                                     re.IGNORECASE), record.get("title")) or \
                    ('Kinect Sensor' in title and 'with Kinect Sensor' not in title):
                return None

            if not re.findall(r'\bXbox Series X\b', platform, flags=re.IGNORECASE) and re.findall(r'\bDigital\b', title, \
                                flags=re.IGNORECASE):
                return None

            if not color and platform.lower() == 'Xbox Series X'.lower():
                if re.findall('Xbox Series S Digital Edition|Xbox Series S Ultimate Launch Day Bundle|Xbox Series S Starter Launch Day Bundle', \
                    title, flags=re.IGNORECASE):
                    color = 'White'
                elif re.findall('Xbox Series X', title, flags=re.IGNORECASE):
                    color = 'Black'

            if not storage_capacity and platform.lower() == 'Xbox Series X'.lower():
                if re.findall('Xbox Series S Digital Edition', title, flags=re.IGNORECASE):
                    storage_capacity = '500GB'
                elif re.findall('Xbox Series X', title, flags=re.IGNORECASE):
                    storage_capacity = '1TB'

            if platform.lower() == 'Xbox Series X'.lower() and re.findall('All Access', record.get('title', ''), flags=re.IGNORECASE):
                return None
            
            platform = re.sub(r'^Xbox Series X$', 'Xbox Series X|S', platform, flags=re.IGNORECASE)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', '').split(', ')[0],
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "title": record.get('title', ''),
                "model": title,
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": re.sub(r"\r|\n|\t", "", description).strip(),
                "features": re.sub(r"\r|\n|\t", "", features).strip(),
                "ner_query": re.sub(r"\r|\n|\t", "", f'{description} {title} {features}').strip(),

                "color": color,
                "platform": platform,
                "condition": record.get('condition', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "est_retail": {
                    "currencyCode": "USD",
                    "amount": est_retail_price
                },
                "storage_capacity": storage_capacity,
                "memory": record.get('memory', '')
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
