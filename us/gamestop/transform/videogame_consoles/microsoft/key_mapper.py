#Key Mapper for Gamestop data - model + storage_capacity  combination is used to de-dup items
class Mapper:
    def map(self, record):
        model = ''
        storage_capacity = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "storage_capacity" == entity['name']:
                if entity["attribute"]:
                    storage_capacity = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = model + storage_capacity + color
        return key_field
