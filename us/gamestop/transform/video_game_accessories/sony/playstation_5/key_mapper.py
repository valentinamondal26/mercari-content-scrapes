# Key Mapper for Gamestop data - model + storage_capacity + color combination is used to de-dup items
class Mapper:
    def map(self, record):
        platform = ''
        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "platform" == entity['name'] and entity["attribute"]:
                platform = entity["attribute"][0]["id"]
            elif "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name'] and entity["attribute"]:
                color = entity["attribute"][0]["id"]

        key_field = platform + model + color
        return key_field
