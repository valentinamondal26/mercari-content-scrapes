import re

import hashlib
from colour import Color


class Mapper(object):

    def map(self, record):
        if record:

            record = self.pre_process(record)

            description = record.get('description', '')
            description = description.replace(
                '\n', ' ').replace('\r', ' ').replace('\t', ' ')
            description = re.sub(r'\s+', ' ', description)

            features = record.get('features', '')
            features = features.replace('\n', ' ').replace(
                '\r', ' ').replace('\t', ' ')
            features = re.sub(r'\s+', ' ', features)

            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")
            image = record.get('image', '')
            sku = record.get('sku', '')


            title = record.get('title', '')
            brand = record.get('brand', '')
            model = title

            colors = []
            color_meta = ["Berry Blue", "Red Camo", "LalaLime", "Glacier White",
                          "Midnight Blue", "Magma Red", "Rose Gold", "Steel Black",
                          "Sunset Orange", "Titanium Blue", "Green Camo", "Red Camo",
                          "Copper", "Crystal Red", "Crystal Blue", "Crystal",
                          "Electric Purple", "Metallic Blue", "Metallic Red",
                          "Gun Metal","Blue Camo","Arctic Camo","Alpine Green",
                          "Wave Blue"]
            if re.findall(re.compile(r"|".join(color_meta), re.IGNORECASE), model):
                colors.append(re.findall(re.compile(
                    r"|".join(color_meta), re.IGNORECASE), model)[0])
            for cl in colors:
                model = re.sub(re.compile(
                    r"\b{}\b".format(cl), re.IGNORECASE), "", model)
            if not re.findall(re.compile(r"Gold Wireless Headset|New Gold \w+ Wireless Headset", re.IGNORECASE), model):
                colors += [i.strip() for i in re.findall(
                    r"\w+", model) if self.check_color(i)]
            colors = list(filter(None, colors))
            color = ', '.join(colors)

            if not color:
                color = record.get('Color', '')

            meta_platform = [
                'PlayStation 5', 'PlayStation 4', 'PlayStation 3', 'PlayStation 2',
                'PlayStation',
            ]
            # taking platform value form breadcrumb
            try:
                platform = re.findall(re.compile(r"|".join(meta_platform),re.IGNORECASE),record.get("breadcrumb",""))[0]
            except IndexError:
                platform=''
            
            if not image:
                image = f"https://media.gamestop.com/i/gamestop/{sku}/{platform.replace(' ', '-')}-{title.replace(' ','-')}?$pdp$"


            keywords = list()
            keywords.append(brand)
            keywords.append(platform)

            if not re.findall(re.compile(r"Black Ops", re.IGNORECASE), model):
                for cl1 in color.split(", "):
                    for cl2 in color.split(", "):
                        if re.findall(re.compile(r"{}\s*\band\b\s*{}".format(cl1, cl2), re.IGNORECASE), model):
                            model = re.sub(re.compile(
                                r"{}\s*\band\b\s*{}".format(cl1, cl2), re.IGNORECASE), " ", model)
                for cl in color.split(", "):
                    model = re.sub(re.compile(
                        r"\b{}\b".format(cl), re.IGNORECASE), "", model)
            model = re.sub(re.compile(r"\bMultitap\b",
                                      re.IGNORECASE), "Multi Tap", model)
            model = re.sub(r"\b(\S+)\s*\-\s*(\S+)", r"\1-\2", model)
            model = re.sub(re.compile(
                r"\bGameStop\b|\bGameStop\s*\/\s*Playstation One\b|"
                r"\bRefurbished\b|\bOnly at GameStop\b|\bPlayStation\s*\d*|"
                r"Assorted Colors|Recertified Custom|xbox one|xbox",
                re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                r"Dualshock", re.IGNORECASE), "DUALSHOCK", model)
            model = " ".join(model.split())
            if re.findall(r"\W", model[0]):
                model = model[1:]
            if re.findall(r"\s\W", model[-2:]) or model[-1] == "-":
                model = model[:-1]
            model = re.sub(re.compile(r'|'.join(
                list(map(lambda x: r'\b' + x + r'\b', keywords))),
                re.IGNORECASE), '', model)
            model = " ".join(model.split())
            model = re.sub(re.compile(r"^DUALSHOCK 4 Controller$", re.IGNORECASE), "DUALSHOCK 4 Wireless Controller", model)
            model = re.sub(re.compile(r"\bWirless\b", re.IGNORECASE), "Wireless", model)
            model = re.sub(r'\bfor\b$', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            keywords = [
                'bundle', 'blast', 'guide', 'Assortment', 'Dongle Required',
            ]
            if re.findall(re.compile(
                r"|".join(list(map(lambda x: r'\b' + x + r'\b', keywords)
                )),re.IGNORECASE), title) \
                    or platform not in meta_platform \
                        or re.findall(re.compile(r"Remote Cover", re.IGNORECASE),
                        model):
                return None
            
            if not platform.lower() == 'playstation 5':
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', '').split(',')[0],
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": brand,

                "title": title,
                "breadcrumb": record.get('breadcrumb', ''),
                'image': image,
                # "features": record.get('features', ''),
                "ner_query": f'{description} {title} {features}',
                "description": description,

                "model": model,
                "sku": record.get('sku', ''),
                "color": color,
                "platform": platform,
                "item_condition": record.get('condition', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "est_retail": {
                    "currencyCode": "USD",
                    "amount": est_retail_price
                },
            }
            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key] = ''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
