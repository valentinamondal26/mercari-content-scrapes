
class Mapper:
    def map(self, record):
        model = ''
        platform = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "platform" == entity['name']:
                if entity["attribute"]:
                    platform = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = platform + model + color
        return key_field
