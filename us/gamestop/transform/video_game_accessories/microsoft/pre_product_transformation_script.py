import re

import hashlib
from colour import Color
from titlecase import titlecase
class  Mapper(object):
   
   def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')
            model = record.get('title', '')
            platform = record.get('platform', '')

            color = record.get('Color', '')
            if not color:
                colors = []
                for word in title.split():
                    try:
                        Color(word)
                        colors.append(word)
                    except ValueError:
                        pass
                color = '/'.join(colors)
            
            meta_colors = ['Carbon', 'Matte']
            color_matching = re.findall(r'|'.join(list(map(lambda x: r'\b'+x +' '+ color +r'\b', meta_colors))), title, flags=re.IGNORECASE)
            if color_matching:
                color = color_matching[0]
            colors = color.split('/')
            model = re.sub(r'|'.join(list(map(lambda x: r'\b' + x + r'\b', colors))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMicrosoft\b|\bFor Windows 10\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bfor {platform}\b'.format(platform=platform), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\b{platform}\b'.format(platform=platform), '', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(\b\d+TB\b)(.*)', r'\2 \1 \3', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            platform = re.sub(r'\bXbox Series X\b', 'Xbox Series X|S', platform, flags=re.IGNORECASE)

            meta_product_type = ['Sensor', 'Screen', 'Adapter', 'Adaptor', 'Hard Drive', 'Stage Kit', 'Charge Kit', 'Headset', 'DVD Player', \
                'Controller', 'Memory Unit', 'Remote', 'Camera', 'Media Drive', 'Charger', 'Game Drive', 'Locker', 'Charging', 'Storage']
            
            transform_meta_product_type ={
                'controller': 'Controllers & Controller Accessories',
                'remote': 'Controller',
                'media drive': 'Hard Drives',
                'memory unit': 'Hard Drives',
                'hard drive': 'Hard Drives',
                'game drive': 'Hard Drives',
                'storage': 'Storage Device',
                'adapter': 'Cables & Networking',
                'adaptor': 'Cables & Networking',
                'charge kit': 'Batteries & Chargers',
                'charger': 'Batteries & Chargers',
                'charging': 'Batteries & Chargers',
                'locker': 'Locker',
                'headset': 'Headsets',
                'remote': 'Controller',
                'camera': 'Cameras & Sensors',
                'sensor': 'Cameras & Sensors'
            }
            product_types = re.findall(r'|'.join(list(map(lambda x: r'\b' + x +r'\b', meta_product_type))), title, flags=re.IGNORECASE)
            if product_types:
                product_type = product_types[0].lower()
                product_type = transform_meta_product_type.get(product_type, product_type)
                product_type =  titlecase(product_type)
            else:
                product_type = ''
            
            if not product_type:
                controller_and_accessories_items = ['Movie Playback Kit', 'Blaster', 'Action Replay', 'Gun Bundle']
                cables_and_networking_items = ['AV Pack']
                stage_kit = ['Microphone']
                cameras_sensors = ['Kinectimals']
                if re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', controller_and_accessories_items))), title, flags=re.IGNORECASE):
                    product_type = 'Controllers & Controller Accessories'
                elif  re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', cables_and_networking_items))), title, flags=re.IGNORECASE):
                    product_type = 'Cables & Networking'
                elif re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', stage_kit))), title, flags=re.IGNORECASE):
                    product_type = 'Stage Kit'
                elif re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', cameras_sensors))), title, flags=re.IGNORECASE):
                    product_type = 'Cameras & Sensors'

            transformed_record ={
                'item_id': hex_dig,
                'id': record.get('id', ''),
                'crawlDate': record.get('crawlDate', '').split(', ')[0],
                'statusCode': record.get('statusCode', ''),

                'category': record.get('category'),
                'brand': record.get('brand', ''),
                'title': record.get('title', ''),


                'platform': platform,
                'color': color,
                'model': model,
                'condition': record.get('condition', '') or record.get('Condition', ''),
                'price': {
                    'currencyCode': 'USD',
                    'amount': record.get('price', '').strip('$')
                },
                'product_type': product_type,
                'description': ' '.join(record.get('description', '').split())
            }
            return transformed_record
        else:
            return None
