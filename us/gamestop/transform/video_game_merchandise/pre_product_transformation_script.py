import hashlib
from colour import Color
import re
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('title', '')) + ' ' + record.get('features', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            est_retail_price = record.get('est_retail', '')
            est_retail_price = est_retail_price.replace("$", "")
            model=record.get('title','')
            # _color = [i for i in record.get('title', '').split(' ') if self.check_color(i)]
            # color=''
            # for c in _color:
            #     if c!='':
            #         color=c
            #         title=title.replace(color,'')
            breadcurmb=record.get('breadcrumb','').split('|')
            product_type=''
            product_types=['Third-Party Gift Cards','Outdoors','Lighting','Kitchen','Bedroom','Figures','Plush','Construction','Blind Bags','Watches','Clothing','Accessories','Wallets','Lanyards','Key Chains & Key Caps','Hats']
            for word in breadcurmb:
                if word in product_types:
                    product_type=word
                    break
            franchises=['Metroid','Star Fox','The Legend of Zelda','Donkey Kong', 'Super Mario Bros']
            franchise=''
                
            for word in franchises:
                if word in model:
                    franchise=word
                    break
            if franchise=='':
                if 'Nintendo' in model:
                    franchise='Nintendo'
                    
            pattern=re.compile(r"Nintendo|only at game\s*stop",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            model=' '.join(model.split())
            if re.findall(re.compile(r"Assortment|eShop|\$|amiibo",re.IGNORECASE),model)!=[]:
                return None
        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "brand": record.get('brand', ''),
                "title":record.get('title',''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', '').replace("\n",''),
                "model": model,
                "manufacturer":record.get('manufacturer',''),
                "product_type":product_type,
                "size": record.get('size', '').strip(),
                "condition": record.get('condition', ''),
                "ner_query": ner_query.replace("\n",''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "color":record.get('Color','').replace('Refine by Color: ',''),
                "franchise":franchise
            }

            return transformed_record
        else:
            return None
    
    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False


