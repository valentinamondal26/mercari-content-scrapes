# -*- coding: utf-8 -*-
'''
https://mercari.atlassian.net/browse/USDE-256 - Sony PlayStation 4, PlayStation 3, PlayStation 2, PlayStation Video Game Consoles
https://mercari.atlassian.net/browse/USDE-257 - Microsoft Xbox One, Microsoft Xbox 360 Video Game Consoles
https://mercari.atlassian.net/browse/USDE-259 - Nintendo Switch, Nintendo Wii U Video Game Consoles
https://mercari.atlassian.net/browse/USDE-794 - Sony Video Game Accessories
https://mercari.atlassian.net/browse/USDE-1436 - Nintendo Handheld Consoles
https://mercari.atlassian.net/browse/USCC-191 - Nintendo Video Game Merchandise
https://mercari.atlassian.net/browse/USCC-514 - Sony Playstation 5 PlayStation Video Game Consoles
https://mercari.atlassian.net/browse/USCC-512 -  Microsoft Video Game Consoles
https://mercari.atlassian.net/browse/USCC-517 - Sony PlayStation 5 Accessories
https://mercari.atlassian.net/browse/USCC-307 - Sony PlayStation 1,PlayStation 2, PlayStation 3, PlayStation 4 Accessories
https://mercari.atlassian.net/browse/USCC-516 - Microsoft Video Game Accesories
'''

import scrapy
import datetime
import itertools

import re
from scrapy.exceptions import CloseSpider

from common.selenium_middleware.http import SeleniumRequest
import os


class GamestopSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and sub-category from gamestop.com for the specified brand

    Attributes

    category : str
        category to be crawled, Supports
        1) Video Game Consoles
        2) Handheld Consoles
        3) Video Game Merchandise
        4) Video Game Accessories

    brand : str
        brand/sub-categories to be crawled, Supports
        1) Xbox One
        2) PlayStation 4
        3) PlayStation 3
        4) Xbox 360
        5) Nintendo Switch
        6) Nintendo Wii U
        7) Nintendo DS
        8) PlayStation 5
        9) Xbox Series X
        10) Sony
        11) Xbox
        12) Nintendo


    Command e.g:
    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='PlayStation 3'
    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='PlayStation 4'
    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='PlayStation 5'

    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='Xbox One'
    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='Xbox 360'
    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='Xbox Series X|S'

    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='Nintendo Switch'
    scrapy crawl GamestopSpider -a category='Video Game Consoles' -a brand='Nintendo Wii U'

    scrapy crawl GamestopSpider -a category='Video Game Accessories' -a brand='Sony'
    scrapy crawl GamestopSpider -a category='Video Game Accessories' -a brand='Microsoft'

    scrapy crawl GamestopSpider -a category='Video Game Merchandise' -a brand='Nintendo'

    scrapy crawl GamestopSpider -a category='Handheld Consoles' -a brand='Nintendo'
    """

    name = 'GamestopSpider'

    source = 'gamestop.com'
    blob_name = 'gamestop.txt'

    merge_key = 'id'

    category = None
    brand = None
    sub_category = None
    filters = None
    launch_url = None

    base_url = 'https://www.gamestop.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        # 'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    url_dict = {
        'Video Game Consoles' : {
            'Microsoft': {
                'Xbox One': {
                    'url': 'https://www.gamestop.com/video-games/xbox-one/consoles',
                    'filters': ['Color', 'Condition', ],
                },
                'Xbox 360': {
                    'url': 'https://www.gamestop.com/video-games/xbox-360/consoles',
                    'filters': ['Color', 'Condition', ],
                },
                'Xbox Series X|S': {
                    'url': 'https://www.gamestop.com/video-games/xbox-series-x/consoles',
                    'filters': ['Color']
                }
            },
            'Sony': {
                'PlayStation 5': {
                    'url': 'https://www.gamestop.com/video-games/playstation-5/consoles',
                    'filters': ['Color', 'Condition', ],
                },
                'PlayStation 4': {
                    'url': 'https://www.gamestop.com/video-games/playstation-4/consoles',
                },
                'PlayStation 3': {
                    'url': 'https://www.gamestop.com/video-games/playstation-3/consoles',
                },
            },
            'Nintendo': {
                'Nintendo Switch': {
                    'url': 'https://www.gamestop.com/video-games/switch/consoles',
                    'filters': ['Color', 'Condition', ],
                },
                'Nintendo Wii U': {
                    'url': 'https://www.gamestop.com/video-games/more-platforms/wii-u/consoles',
                    'filters': ['Color', 'Condition', ],
                },
            },
        },
        'Handheld Consoles': {
            'Nintendo': {
                # 'All': {
                    'url': [
                        'https://www.gamestop.com/video-games/more-platforms/nintendo-3ds/consoles',
                        'https://www.gamestop.com/video-games/more-platforms/nintendo-ds/consoles',
                        # 'https://www.gamestop.com/search/?q=SWITCH+LITE+CONSOLES&lang=default',
                        'https://www.gamestop.com/video-games/retro-gaming/game-boy?q=systems',
                        'https://www.gamestop.com/video-games/retro-gaming/game-boy-advance?q=systems',
                    ],
                    'filters': ['Color', 'Condition', ],
                # }
            }
        },
       'Video Game Merchandise': {
           'Nintendo': {
                "url": [
                    'https://www.gamestop.com/toys-collectibles?q=nintendo', ###SITE DESIGN IS CHANGED .
                    'https://www.gamestop.com/clothing?q=nintendo',
                ],
                "filters": ['Color'],
           },
       },
       'Video Game Accessories': {
           'Sony': {
               'url': [
                    'https://www.gamestop.com/video-games/playstation-4/accessories',
                    'https://www.gamestop.com/video-games/playstation-3/accessories',
                    'https://www.gamestop.com/video-games/retro-gaming/playstation-2?q=accessory',
                    'https://www.gamestop.com/video-games/retro-gaming/playstation?q=accessory',
                    'https://www.gamestop.com/video-games/playstation-5/accessories'
               ],
                'filters': ['Color', 'Condition', ],
           },
           'Microsoft': {
               'url': 'https://www.gamestop.com/video-games/xbox-series-x/accessories',
               'filters': ['Color']
            }
       }
    }

    query = None
    crawl_url = 'https://www.gamestop.com/on/demandware.store/Sites-gamestop-us-Site/default/Search-UpdateGrid?{query}&prefn1=includedChannels&prefv1=GS_US&prefn2=isEmbargoed&prefv2=false&prefn3=tradeOnly&prefv3=false&start=0&sz={size}'

    brand_map = {
        'Sony': [
            'PlayStation 5', 'PlayStation 4', 'PlayStation 3', 'PlayStation 2', 'PlayStation',
        ],
        'Nintendo': [
            'Nintendo Switch', 'Nintendo Wii U',
            # 'Nintendo 3DS', 'Nintendo 3D XL', 'Nintendo 2DS', 'Nintendo 2DS', 'Nintendo DS Lite', 'Nintendo DSi'
        ],
        'Microsoft': [
            'Xbox One', 'Xbox 360', 'Xbox Series X|S'
        ],
        'Xbox': []
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        '''
        1) For consistency with other scrape project, brand is used as
        the argument name but sub_category/platform is the optimal name here.

        This site responds 403 when using scrapy.Request,
        so using SeleniumRequest instead wherever necessary.
        '''
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        super(GamestopSpider,self).__init__(*args, **kwargs)

        if not category or not brand :
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category or brand not found')
        self.category = category
        for brand_name, sub_categories in self.brand_map.items():
            if brand in brand_name:
                self.brand = brand
                break
            elif brand in sub_categories:
                self.brand = brand_name
                self.sub_category = brand
                break

        if not brand:
            self.logger.error("brand should not be None")
            raise CloseSpider('brand not found')

        d = self.url_dict.get(self.category, {}).get(self.brand, {})
        if self.sub_category:
            d = d.get(self.sub_category, {})

        self.launch_url = d.get('url', '')
        self.filters = d.get('filters', [])
        self.round_robin = itertools.cycle(self.proxy_pool)
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{}'.format(category))
            raise(CloseSpider('Spider is not supported for  category:{}'.format(category)))

        self.logger.info(f"Crawling for category:{category} and brand:{brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        self.filters = ['Color']
        self.round_robin = itertools.cycle(self.proxy_pool) 


    def start_requests(self):
    	if self.launch_url:
            if isinstance(self.launch_url, list):
                for url in self.launch_url:
                    yield self.createDynamicRequest(url=url, callback=self.parse_filters,meta={'index':self.launch_url.index(url)})
                    url = self.get_parse_callback_func_url(url=url)
                    meta ={'start': 0}
                    yield self.createDynamicRequest(url=url,callback=self.parse_links,meta={'start': 0})
            else:
               yield self.createDynamicRequest(url=self.launch_url,callback=self.parse_filters)
               url = self.get_parse_callback_func_url(url=self.launch_url)
               meta ={'start': 0}
               yield self.createDynamicRequest(url=url,callback=self.parse_links, meta=meta)

    def get_parse_callback_func_url(self, url):
        print(url)
        if re.findall(r'\?', url):
            url = f'{url}&start=0&sz=300'
        else:
            url = f'{url}?start=0&sz=300'
        return url

    def parse_filters(self,response):
        """
        @url https://www.gamestop.com/video-games/playstation-4/accessories?start=0&sz=300
        @meta {"use_proxy":"True", "use_selenium": "True"}
        @returns requests 1
        """
        url=response.url

        for filter in self.filters:
            filter_urls=response.xpath('//div[@class="card-header"][contains(text(),"{}")]/following-sibling::div/ul/li/a/@href'.format(filter)).getall()
            filter_urls=[self.base_url+link if "https" not in link else link for link in filter_urls ]
            filter_values=response.xpath('//div[@class="card-header"][contains(text(),"{}")]/following-sibling::div/ul/li/@title'.format(filter)).getall()
            for url in filter_urls:
                meta={}
                index=filter_urls.index(url)
                filter_value = filter_values[index]
                filter_value = filter_value.replace('Refine by {}: '.format(filter), '')
                meta = meta.fromkeys([filter], filter_value)
                meta.update({'filter_name': filter})
                meta.update({'start': 0})
                yield self.createDynamicRequest(url=f'{url}?start=0&sz=300&format=ajax', callback=self.parse_links, meta=meta)


    def parse_links(self, response):
        """
        @url https://www.gamestop.com/video-games/playstation-4/accessories?start=0&sz=300
        @meta {"use_proxy":"True", "use_selenium": "True"}
        @returns requests 1
        """
        response_meta = response.meta
        product_tiles = response.xpath("//div[@class='row product-grid']//div[contains(@class,'product-tile-header')]")
        for product_tile in product_tiles:
            url = product_tile.xpath('.//a/@href').extract_first()
            platform = product_tile.xpath('.//span[@class="pr-1"]/text()').extract_first()
            if url:
                if "https" not in url:
                    url = self.base_url + url
                response_meta['platform'] = platform
                yield self.createDynamicRequest(url=url, callback=self.parse_products, meta=response_meta)

        if product_tiles:
            start = response_meta.get('start', '')
            if start:
                start+=300
                response_meta.update({'start': start})
                url = re.sub(r'\bstart=\d+\b', 'start={start}'.format(start=start), response.url)
                yield self.createDynamicRequest(url=url, callback=self.parse_links, meta=response_meta)


    def parse_products(self,response):
        """
        @url https://www.gamestop.com/toys-collectibles/collectibles/figures/products/super-mario-odyssey-world-of-nintendo-action-figure-5-pack-only-at-gamestop/10162751.html?rt=productDetailsRedesign&utm_expid=.h77-PyHtRYaskNpc14UbmA.1&utm_referrer=https%3A%2F%2Fwww.gamestop.com%2Ftoys-collectibles%3Fq%3Dnintendo%26rule%3Dbest-matches
        @meta {"use_proxy":"True", "use_selenium": "True"}
        @scrape_values image title description manufacturer condition price breadcrumb sku
        """
        ### Since gamestop has lot of categories and brand and All the categories are crawled in one spider we cannot \
        # check all the specs in one product url.In contracts we are checking some items only before running the spider  make sure the given spec in the ticket \
        # is not broken in spider
        response_meta=response.meta
        url=response.url

        item={}
        item["crawlDate"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["statusCode"]=str(200)
        item['id']=url
        item['image']=response.xpath('//div[@class="product-image-only-carousel"]/div/img/@src').extract_first(default='') or response.xpath("//div[@class='product-image-carousel ']/img/@src").get(default='')
        item['brand']=self.brand
        item['category']=self.category
        item['platform'] = self.sub_category or response_meta.get('platform', '')
        # item['platform_listing'] = response_meta.get('platform', '')
        title=response.xpath("//div[@class='product-name-section']/h1/text()").get(default='').strip()
        item['title']=title
        item['description']=response.xpath('//div[@class="short-description"]//text()').get(default='').strip()
        bundle_includes=response.xpath('//div//h4[contains(text(),"Includes:")]//text()').get(default='') or ', '.join(response.xpath("//div[@class='bundle-includes-carousel']/div[@class='bundle-product-tile']/a/img/@title").getall())

        item['bundle_includes']=bundle_includes
        if bundle_includes!='':
            features=response.xpath('//div[h4[contains(text(),"Includes:")]]//text()').getall()
            try:
                position=features.index(bundle_includes)
                features=features[position+1:]
            except:
                pass
            features=','.join(features)
            item['features']=features
        else:
            features = []
            features.append(response.xpath('//div[@class="short-description"]//following-sibling::div//text()').getall())
            features.append(response.xpath('//div[@class="product-features container"]//ul//text()').getall())
            features.append(response.xpath("//div[@class='full-description']/ul/li/text()").getall())
            feature = ', '.join(list(itertools.chain(*features)))
            item['features']=feature
        item['est_retail']=response.xpath('//div[@class="primary-details-row"]//span[@class="strike-through list"]/span/@content').get(default='')
        needed_script=response.css('script:contains("Page Loaded")::text').get(default='')
        item['manufacturer']=response.xpath('//div[@class="product-publisher"]/span[@class="pr-1"]/text()').get(default='')
        if item['est_retail']!='':
            if 'condition' in needed_script:
                need=needed_script.split('"condition":')[1]
                match = re.findall(re.compile(r'\"(.*?)\"'), need)
                if match:
                    item['condition']=match[0].strip('"')
                else:
                    item['condition']=''
        else:
            item['condition']=response.xpath('//div[@class="card-body"]/span/text()').get(default='')

        item['price']=response.xpath('//span[@class="sales"]/span/@content').get(default='')
        breadcrumbs=response.xpath('//ol[@class="breadcrumb"]/li')
        breadcrumb=''
        for x in breadcrumbs:
            breadcrumb+=x.css('a::text').get(default='').strip()+"|"
        breadcrumb=breadcrumb[:-1]
        item['breadcrumb']=breadcrumb
        # if re.findall(r"\d+\s*[A-Z]{2}",title):
        #     item['storage_capacity']=re.findall(r"\d+\s*[A-Z]{2}",title)[0]
        # else:
        #     item['storage_capacity']=''
        #title=title.replace(item['storage_capacity'],'')
        #_color = [i for i in title.split(' ') if self.check_color(i)]
        # item['color']=''
        # for color in _color:
        #     if color!='':
        #         item['color']=color
        # title=title.replace(item['color'],'')
        item['title']=title
        item['memory']=response.xpath('//td[contains(text(),"Memory")]/following-sibling::td//text()').get(default='').strip()
        item['size']=''
        pid=response.xpath('//div[@class="page "]/@data-querystring').get(default='')
        pid=pid.split('pid=')
        if len(pid)>1:
            sku=pid[1]
            item['sku']=sku
            # category = self.category.replace(' ', '_').lower()
            # sub_category = self.sub_category.replace(' ', '_').lower()
            # filename = "html/{}/{}/{}.html".format(category,sub_category,sku)
            # import os
            # if not os.path.exists(filename):
            #     os.makedirs(filename)
            # with open(filename, "w+") as f:
            #     f.write(response.text)
            #     f.close()
        meta={}
        filter_name=response_meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response_meta[filter_name])
            item.update(meta)

        if response.xpath('//select[@class="custom-select form-control select-size"]').get(default=None):
            items={}
            items_varations_url=response.xpath('//select[@class="custom-select form-control select-size"]/option[@data-is-selectable="true"]/@value').getall()
            items_varations_url=[self.base_url+x if "https" not in x else x for x in items_varations_url]
            items_varations_value=response.xpath('//select[@class="custom-select form-control select-size"]/option[@data-is-selectable="true"]/text()').getall()
            for url in items_varations_url:
                item['id']=url
                item['size']=items_varations_value[items_varations_url.index(url)]
                items.update(item)
                yield items
        else:
            yield item


    def createDynamicRequest(self, url, callback, errback=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, wait_time=10, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request


    def createRequest(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request

