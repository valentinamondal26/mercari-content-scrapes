# -*- coding: utf-8 -*-

# Scrapy settings for gamestop project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'gamestop'

SPIDER_MODULES = ['gamestop.spiders']
NEWSPIDER_MODULE = 'gamestop.spiders'

USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

HTTPERROR_ALLOWED_CODES  =[403]

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

CONCURRENT_REQUESTS = 64

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'
# GCS settings - actual value be 'raw/gamestop.com/video_game_consoles/playstation_4/2019-02-06_00_00_00/json/gamestop.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

DOWNLOADER_MIDDLEWARES = {
    'common.selenium_middleware.middlewares.SeleniumMiddleware': 1000,
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}