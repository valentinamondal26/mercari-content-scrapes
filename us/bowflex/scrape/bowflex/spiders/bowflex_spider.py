'''
https://mercari.atlassian.net/browse/USDE-1710 - Bowflex Fitness Accessories
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json
import re

class BowflexSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bowflex.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Fitness Accessories

    brand : str
        brand to be crawled, Supports
        1) Bowflex

    Command e.g:
    scrapy crawl bowflex -a category='Fitness Accessories' -a brand='Bowflex'
    """

    name = 'bowflex'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'bowflex.com'
    blob_name = 'bowflex.txt'

    url_dict = {
        'Fitness Accessories': {
            'Bowflex': {
                'url': 'https://www.bowflex.com/fitness-accessories/',
                'filters': ['Fitness Accessories'],
            }
        }
    }

    extra_item_infos = {}

    base_url = 'https://www.bowflex.com'

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BowflexSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.filters = ['Fitness Accessories']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse, dont_filter=True)
            yield self.create_request(url=self.launch_url, callback=self.parse_filters, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://www.bowflex.com/accessories/5-way-hand-grip-ankle-cuffs/17100.html
        @scrape_values id title image price description
        @meta {"use_proxy":"True"}
        """

        image = ''
        try:
            image = json.loads(response.xpath('//script[@type="application/ld+json"]/text()').extract_first())[0].get('image', '')
        except:
            pass

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//div[@class="breadcrumb-product"]//span/text()').extract(),
            'title': response.xpath('//h1[@class="product-name standard"]/text()').extract_first() \
                or response.xpath('//div[@class="product-info-top"]//h1/text()').extract_first(),
            'price': response.xpath('//div[@class="product-price"]/span[@class="price-sales"]/text()').extract_first(),
            'image': response.xpath('//li[@class="pdp-main-image"]/a/@href').extract_first() \
                or image,
            'description': response.xpath(u'normalize-space(//div[@class="product-information"]/div[@class="info-content"]/text())').extract_first() \
                or ''.join(response.xpath('//div[@class="product-information"]/div[@class="info-content"]').xpath('.//text()[normalize-space(.)]').extract()),
            'features': response.xpath('.//div[@class="product-features-alt"]/div[@class="feature-container"]/ul/li/text()').extract(),
        }


    # def crawlDetail(self, response):
    #     """
    #     @url https://www.bowflex.com/accessories/5-way-hand-grip-ankle-cuffs/17100.html
    #     @scrape_values id title image price description
    #     @meta {"use_proxy":"True"}
    #     """

    #     image = ''
    #     try:
    #         image = json.loads(response.xpath('//script[@type="application/ld+json"]/text()').extract_first())[0].get('image', '')
    #     except:
    #         pass

    #     title = response.xpath('//h1[@class="product-name standard"]/text()').extract_first() \
    #             or response.xpath('//div[@class="product-info-top"]//h1/text()').extract_first()

    #     item {
    #         'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
    #         'statusCode': str(response.status),
    #         'category': self.category,
    #         'brand': self.brand,
    #         'id': response.url,

    #         'breadcrumb': response.xpath('//div[@class="breadcrumb-product"]//span/text()').extract(),
    #         'title': title,
    #         'price': response.xpath('//div[@class="product-price"]/span[@class="price-sales"]/text()').extract_first(),
    #         'image': response.xpath('//li[@class="pdp-main-image"]/a/@href').extract_first(default='') \
    #             or image,
    #         'description': response.xpath(u'normalize-space(//div[@class="product-information"]/div[@class="info-content"]/text())').extract_first() \
    #             or ''.join(response.xpath('//div[@class="product-information"]/div[@class="info-content"]').xpath('.//text()[normalize-space(.)]').extract()),
    #         'features': response.xpath('.//div[@class="product-features-alt"]/div[@class="feature-container"]/ul/li/text()').extract(),
    #     }

    #     match = re.findall(r'(Power Rod) (\d+) lb\. Upgrade (\d+) to (\d+) lbs\.', title, flags=re.IGNORECASE)
    #     if match:
    #         match = match[0]
    #         for d in match[1:]:
    #             item['title'] = f'{match[0]} {d} lbs.'
    #             item['id'] = f'{response.url}?weight={d}'
    #             yield item
    #     else:
    #         yield item


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse_filters(self, response):
        """
        @url https://www.bowflex.com/fitness-accessories/
        @returns requests 1 50
        @meta {"use_proxy":"True"}
        """

        for filter_item in response.xpath('//div[@class="refinements-holder"]//ul[@id="category-level-1"]/li[contains(@class, "expandable")]'):
            filter_name = filter_item.xpath(u'normalize-space(./a[contains(@class, "refinement-link")]/text())').extract_first()
            if filter_name in self.filters:
                for filter_value_item in filter_item.xpath('.//ul[@id="category-level-2"]/li'):
                    filter_value = filter_value_item.xpath(u'normalize-space(./a[@class="refinement-link "]/text())').extract_first()
                    url = filter_value_item.xpath('./a[@class="refinement-link "]/@href').extract_first()
                    # print(filter_name, filter_value, url)
                    request = self.create_request(url=url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                        filter_name: filter_value
                    }
                    yield request


    def parse(self, response):
        """
        @url https://www.bowflex.com/fitness-accessories/
        @returns requests 1 50
        @meta {"use_proxy":"True"}
        """

        extra_item_info = response.meta.get('extra_item_info', {})
        for product in response.xpath('//ul[@id="search-result-items"]/li[@class="grid-tile"]'):

            id = product.xpath('.//a[@class="thumb-link"]/@href').extract_first()

            if id:
                id = self.base_url + id
                id = id.split('?')[0]

                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                # self.extra_item_infos.get(id).update(extra_item_info)
                self.update_extra_item_infos(id, extra_item_info)

                request = self.create_request(url=id, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('//div[@class="pagination"]/ul/li[@class="first-last last"]/a[@class="page-next"]/@href').extract_first()
        if nextLink:
            request = self.create_request(url=nextLink, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
