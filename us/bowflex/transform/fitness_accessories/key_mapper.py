class Mapper:
    def map(self, record):
        key_field = ""

        model = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                for attribute in entity["attribute"]:
                    model += attribute["id"]
                break

        key_field = model
        return key_field
