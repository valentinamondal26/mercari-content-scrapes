import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            description = re.sub(r'[\n\t\r]', ' ', description)
            description = re.sub(r'\s+', ' ', description).strip()
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bBowFlex\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s(-)\s', ' ', model)
            model = re.sub(r'\s+', ' ', model).strip()

            # special condition
            match = re.findall(r'(Power Rod) (\d+) lb\. Upgrade (\d+) to (\d+) lbs\.', model, flags=re.IGNORECASE)
            if match:
                models = []
                match = match[0]
                for d in match[1:]:
                    models.append(f'{match[0]} {d} lbs.')
                model = ' , '.join(models)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                "product_type": ', '.join(record.get('Fitness Accessories', [])),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
