'''
https://mercari.atlassian.net/browse/USDE-1259 - Vera Bradley Backpacks
'''

import scrapy
from datetime import datetime
import re
from scrapy.exceptions import CloseSpider
import itertools
import os

class VeraBradleySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from verabradley.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Handbags
        2) Shoulder Bags

    brand : str
        Brand to be crawled, Supports
        1) Vera Bradley

    Command e.g:
    scrapy crawl verabradley -a category='Shoulder Bags' -a brand='Vera Bradley'
    """

    name = 'verabradley'

    source = 'verabradley.com'
    blob_name = 'verabradley.txt'

    category = None
    brand = None

    merge_key = 'id'

    spider_project_name = "verabradley"
    gcs_cache_file_name = 'verabradley.tar.gz'

    BASE_URL = 'https://www.verabradley.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Handbags': {
            'Vera Bradley': {
                'url':'https://www.verabradley.com/us/browse/bags-2043137398',
                'filter': [],
            }
        },
        'Shoulder Bags': {
            'Vera Bradley': {
                'url' :'https://www.verabradley.com/us/browse/bags/shoulder-bags-3889312485',
                'filter': [],
            }
        },
        'Backpacks': {
            'Vera Bradley': {
                'url': 'https://www.verabradley.com/us/browse/backpacks-2988215917',
                'filter': ['Categories', 'Collections', 'Color'],
            }
        }
    }


    def __init__(self, category=None, *args, **kwargs):
        super(VeraBradleySpider,self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category:
            self.logger.error("Category should not be None")
            raise CloseSpider('category not found')

        self.category = category
        self.brand = self.brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url = self.url_dict.get(category, {}).get('brand', {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get('brand', {}).get('filter', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Categories', 'Collections', 'Color']


    def start_requests(self):
        print('launch_url', self.launch_url)
        if self.launch_url:
            yield scrapy.Request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        # item = response.meta['item']
        """
        @url https://www.verabradley.com/us/product/ReActive-Sling-Backpack/26679-26679L97
        @scrape_values title price image rating style description breadcrumb print_name measurements material
        @meta {"use_proxy":"True"}
        """
        meta = response.meta
        item = dict()
        item['title'] = response.xpath("//meta[@property='og:title']/@content").get(default='')
        item['price'] = response.xpath("//span[@class='price current-price']/text()").get(default='')
        item['image'] = response.xpath("//div[@class='gallery-frame gallery-frame-primary peppermint']/figure/img/@data-src").get(default='') 
        if item['image']:
            img = re.sub(r'hei=.*','',item['image'])
            item['image'] = "https:"+img+"hei=1108&wid=1108"
                    
        item['crawlDate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['statusCode'] = str(response.status)
        item['category'] = self.category
        item['brand'] = self.brand
        item['id'] = response.url

        item['rating'] = response.xpath('//div[@class="product-rating-snippet"]/div[@class="rating-stars"]/div[@class="for-desktop"]/div/a/span[@class="full-stars"]/text()').extract_first()
        # item['image'] = response.xpath('//div[@class=" peppermint-slides"]/figure/img/@src').extract_first()
        item['style'] = response.xpath('//span[@class="js-refresh-sku"]/text()').extract_first()
        item['description'] = ", ".join(response.xpath('//div[@class="slot slot-pdp-secondary"]/div[@class="slot for-desktop"]/div[@class="content-item"]/div[@class="product-content"]//text()[normalize-space(.)]').extract())
        if not item['description']:
            description = response.xpath('//div[@class="content-item"]/div[@class="product-content"]//text()[normalize-space(.)]').extract()
            item['description'] = ', '.join([ des.strip() for des in description if '{display' not in des])

        item['breadcrumb'] = " | ".join(response.xpath('//div[@class="breadcrumbs"]/ol/li//text()').extract()).replace("| Home",", Home")
        item['print_name'] = response.xpath('//span[@class="js-refresh-pattern-name"]/text()').extract_first()
        item['measurements'] = response.xpath('//div[@class="product-content"]//strong[contains(text(), "Dimensions:")]/following-sibling::text()').extract_first()

        contents = response.xpath('//div[@class="content-item"]')
        for content in contents:
            if 'Product Fabric Details' in content.xpath('./div[@class="item-heading"]/h2/text()').extract_first():
                item['material'] = content.xpath(u'normalize-space(./p/strong/text())').extract_first()
        
        filter_name = meta.get('filter', '')
        if filter_name:
            meta_data = dict.fromkeys([filter_name], meta[filter_name])
            item.update(meta_data)
        
        yield item
        
        if not meta.get('crawl_product_variant', ''):
            meta = {}
            meta.update({'crawl_product_variant':True}) 
            url = self.crawl_product_variant(response)
            if url:
                request = self.createRequest(url=url, callback=self.crawlDetail,meta = meta)
                yield request
            

    def crawl_product_variant(self,response):
        item_variants = response.xpath('//script[contains(text(),"['+"'product_url'"+'].push")]/text()').getall() 
        for variant in item_variants: 
            try:
                url = (re.findall(r'\/us.*\"',variant)[0].strip('"'))
            except IndexError:
                url = ''    
            if url:
                if self.BASE_URL not in url:
                    url = self.BASE_URL + url
                if response.url != url:       
                    return url
                    

    def parse(self, response):
        """
        @url https://www.verabradley.com/us/browse/backpacks-2988215917
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        meta = response.meta
        viewAll = response.xpath('//div[@class="item-count"]/p/a/@href').extract_first()
        if viewAll:
            yield self.createRequest(self.BASE_URL + viewAll, callback=self.crawlAllBags,meta=meta)
        else:
            for request in self.crawlAllBags(response):
                yield request
        
        if self.filters and not meta.get('filter',''):
            headings = self.get_headings(response)
            for filter_data in self.filters: 
                ind = headings.index(filter_data) 
                category = response.xpath("//div[@class='facet-content']/ul")[ind] 
                category_data = category.xpath(".//a")
                for cat in category_data: 
                    filter_url, meta = self.get_filter_urls(cat,filter_data)
                    request = self.createRequest(url=filter_url, callback=self.parse,meta=meta)
                    yield request

                viewall_category = category.xpath(".//following-sibling::div")
                if viewall_category:
                    category_data = viewall_category[0].xpath(".//a")
                    for cat in category_data:
                        filter_url, meta = self.get_filter_urls(cat,filter_data)
                        request = self.createRequest(url=filter_url, callback=self.parse,meta=meta)
                        yield request


    def get_headings(self,response):
        headings=[]
        headers = response.xpath("//div[@class='facet-toggle']/button/h2")
        for head in headers: 
            headings.append(head.xpath(".//text()").get(default='').title()) 
        return headings    


    def get_filter_urls(self, cat, filter_data):
        filter_name = cat.xpath(".//text()").get().strip()
        if not filter_name:
            filter_name = cat.xpath(".//@title").get()
        url = cat.xpath(".//@href").get()
        filter_url = self.BASE_URL+url

        meta = {'filter':filter_data}
        meta.update({meta['filter']:filter_name})

        return filter_url, meta


    def crawlAllBags(self, response):
        """
        @url https://www.verabradley.com/us/browse/backpacks-2988215917?No=0&Nr=Site_ID%3AVBUS&Nrpp=137&ViewAll=true
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        meta = response.meta
        product_urls = response.xpath('//div[@class="product-tile-inner"]/a/@href').getall()
        for product in product_urls:
            item_page_url = self.BASE_URL + product
            request = self.createRequest(url=item_page_url, callback=self.crawlDetail,meta=meta)
            yield request

        nextLink = response.xpath('//li[@class="next"]/a/@href').extract_first()
        if nextLink:
            yield self.createRequest(url=self.BASE_URL + nextLink, callback=self.crawlAllBags,meta=meta)


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] =  next(self.round_robin)

        meta.update({'use_cache':True})
        return request
