import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')
            model = re.sub(re.compile(r'\Bkpk\b', re.IGNORECASE), 'Backpack', title)
            match = re.findall(r'^Backpack\b', model, re.IGNORECASE)
            if match:
                model = re.sub(r'^Backpack\b', '', model, re.IGNORECASE) + ' Backpack'
            model = re.sub(r'\s+', ' ', model).strip()

            price = record.get('price','').replace('$', '').strip()
            categories = record.get('Categories', '').replace('Backpacks', 'Backpack').strip()
            pattern = record.get('print_name', '')
            product_type = record.get('Collections', '')
            description = record.get('description', '')
            description = description.replace('\n', ' ')
            try:
                model_sku = re.findall(r'SKU:,\s*\d+\w+,', description)[0]
                model_sku = re.sub(r'[:|,|SKU|\s+]', '', model_sku)
            except IndexError:
                model_sku = ''
            
            try:
                capacity = re.findall(r'\(\d+\s*L\)', description)[0]
                capacity = re.sub(r'[(|)]', '', capacity).strip()
            except IndexError:
                capacity = ''    

            dimensions = re.findall(r'(\d+\.\d+"\s*)([w|h|d])', record.get('measurements', ''))
            dimension_data = {}
            for dim in dimensions:
                if 'w' in dim[1].lower():
                    dimension_data['width'] = dim[0].strip()
                elif 'h' in dim[1].lower():
                    dimension_data['height'] = dim[0].strip()
                elif 'd' in dim[1].lower():
                    dimension_data['depth'] = dim[0].strip()

            if re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['set', 'custom']))), re.IGNORECASE), title):
                return None

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statusCode',''),

                'brand': record.get('brand', ''),
                'category': record.get('category', ''),

                'title': title,
                'image': record.get('image', ''),
                'description': description,

                'model': model,
                'bag_type': categories,
                'pattern': pattern,
                'model_sku': model_sku,
                'produtct_type': product_type,
                'color_family':record.get('Color', ''),
                'width': dimension_data.get('width', ''),
                'height': dimension_data.get('height', ''),
                'depth': dimension_data.get('depth', ''),
                'capacity': capacity,
                'msrp': {
                    'currencyCode': 'USD',
                    'amount': price,
                },

            }
            return transformed_record
        else:
            return None
