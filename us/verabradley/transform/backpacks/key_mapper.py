class Mapper:
    def map(self, record):

        model = ''
        pattern = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "pattern" == entity['name']:
                if entity["attribute"]:
                    pattern = entity["attribute"][0]["id"]

        key_field = model + pattern
        return key_field
