'''
https://mercari.atlassian.net/browse/USDE-1671 - Colourpop Cosmetics Makeup Palettes
'''

import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
from urllib.parse import urlparse, parse_qs, urlencode
import re


class ColourpopSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from colourpop.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Makeup Palettes

    brand: str
        1) ColourPop Cosmetics

    Command e.g:
    scrapy crawl colourpop -a category='Makeup Palettes' -a brand='ColourPop Cosmetics'
    """

    name = "colourpop"

    category = None
    brand = None
    source = 'colourpop.com'
    blob_name = 'colourpop.txt'

    proxy_pool = [
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
    ]

    url_dict = {
        'Makeup Palettes': {
            'ColourPop Cosmetics': {
                'url': 'https://colourpop.com/pages/search-results?type=product&q=PALETTES',
            }
        }
    }

    base_url = 'https://www.colourpop.com/'

    # This api is used only if the url has search results.
    # If the url is belongs to some collections/category different api is used
    # e.g site https://colourpop.com/collections/value-sets
    api_base_url = 'https://colourpopcs.ksearchnet.com/cloud-search/n-search/search?'

    query_string_parameters = {
        'ticket': '',
        'term': 'PALETTES',
        'paginationStartsFrom': 0,
        'sortPrice': 'false',
        'ipAddress': 'undefined',
        'analyticsApiKey': '',
        'showOutOfStockProducts': 'true',
        'klevuFetchPopularTerms': 'false',
        'klevu_priceInterval': 500,
        'fetchMinMaxPrice': 'true',
        'klevu_multiSelectFilters': 'true',
        'noOfResults': 200,
        'klevuSort': 'rel',
        'enableFilters': 'true',
        'filterResults': '',
        'visibility': 'search',
        'category': 'KLEVU_PRODUCT',
        'klevu_filterLimit': 50,
        'sv': 121,
        'lsqt': '',
        'responseType': 'json',
        'klevu_loginCustomerGroup': ''
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ColourpopSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("Category should not be None")
            raise CloseSpider('category  not found')

        self.launch_url = self.url_dict.get(category,{}).get(brand,{}).get('url',[])
        self.category = category
        self.brand = brand
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{}'.format(category))
            raise CloseSpider('launch url  not found')

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def get_url(self):
        parts = urlparse(self.api_base_url)
        query_dict = parse_qs(parts.query)
        query_dict.update(self.query_string_parameters)
        url = parts._replace(query=urlencode(query_dict, True)).geturl()
        return url


    def parse(self, response):
        """
        @url https://colourpop.com/pages/search-results?type=product&q=PALETTES
        @returns requests 1
        @meta {"use_proxy": "True"}
        """

        script = response.xpath('//script[contains(text(), "var urls =")]/text()').get(default='')

        urls = []
        match = re.findall(r'var urls = \[(.*?)\];', script)
        if match:
            urls = match[0].split(',')

        ticket_id = ''
        for u in urls:
            match = re.findall(r'klevuapi=(.*?)\\u0026', u)
            if match:
                ticket_id = match[0]

        serach_keyword = ''
        match = re.findall(r'q=(\w+\+*\w*)', response.url)
        if match:
            serach_keyword = match[0]
        if ticket_id and serach_keyword:
            self.query_string_parameters['term'] = serach_keyword
            self.query_string_parameters['ticket'] = ticket_id
            self.query_string_parameters['analyticsApiKey'] = ticket_id

            url = self.get_url()
            yield self.create_request(url=url, callback=self.parse_listing_page)
        else:
            raise CloseSpider('search keyword or ticket_id not found in launch url')


    def parse_listing_page(self, response):
        # TODO: removed existing scrapy contract. Will needs to be restored by finding a way to
        # dynamically inject the header params from the scrapy contracts

        ### Note: In the above contracts minimum requests is based on the results shown
        # by the launch url. Total Products may vary.

        print(response.url)
        response_json = json.loads(response.text)
        products = response_json['result']
        for pro in products:
            url = pro['url'] + '?view=json'
            yield self.create_request(url = url,callback=self.parse_detail_page)

        if response_json.get('meta',{}).get('totalResultsFound','') > response_json.get('meta',{}).get('noOfResults',''):
            self.query_string_parameters['paginationStartsFrom'] = self.query_string_parameters['paginationStartsFrom'] + 200
            url = self.get_url()
            yield self.create_request(url=url, callback=self.parse_listing_page)


    def parse_detail_page(self, response):
        """
        @url https://colourpop.com/products/miss-bliss-pinky-coral-shadow-palette?view=json
        @meta {"use_proxy": "True"}
        @scrape_values title subtitle image product_type_description badges sale_price original_price value_price sku \
            variant_id vendor shade_description net_weight handle description display_description \
                application_tips breadcrumb tags
        """
        ###min_kit_net_weight, child_desc, upsell are not included because in most of the cases it is empty. If one spec is present another spec is not present. Specs mentioned in the ticket are included.
        response_json = json.loads(response.text)
        product_type_description = response_json.get('type_description','')
        if not product_type_description:
            product_type_description=''

        item = {
            'id': response.url.replace("?view=json",'').strip(),
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,

            'title': response_json.get('title',''),
            'subtitle': response_json.get('type',''),
            'image': "https:" +response_json.get('images',[])[0],
            'product_type_description': product_type_description,
            'badges':  ', '.join([badge.get('title','').title() for badge in response_json.get('badges',{})]),
            'sale_price': str(response_json.get('sale_price','')),
            'original_price': str(response_json.get('original_price','')),
            'value_price': str(response_json.get('value_price','')),
            'sku': response_json.get('sku',''),
            'variant_id': response_json.get('variant_id',''),
            'vendor': response_json.get('vendor',''),
            'shade_description': response_json.get('shade_description',''),
            'net_weight': response_json.get('net_weight',''),
            # 'ingredients': response_json.get('ingredients',''),
            'mini_kit_net_weight': response_json.get('mini_kit_net_weight',''),
            'handle': response_json.get('handle',''),
            'description': response_json.get('description',''),
            'display_description': response_json.get('display_description',''),
            'upsell': response_json.get('upsell',''),
            'application_tips': response_json.get('application_tips',''),
            'breadcrumb': response_json.get('breadcrumb',''),
            'child_description': response_json.get('child_description',''),
            'tags': ','.join(response_json.get('tags',[]))
        }
        yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request