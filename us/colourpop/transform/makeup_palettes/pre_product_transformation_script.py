import hashlib
import re
from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title','')
            model = re.sub(re.compile(r'\bPalette\b',re.IGNORECASE),'',model)
            model = model+ " " +record.get('subtitle','')
            model = re.sub(r'&#\d+;','',model)
            model = re.sub('&amp;', '&', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+',' ',model).strip()
            model = titlecase(model)

            def description_decoding(description):
                description = re.sub(r'\<br\>|&nbsp;',', ',description)
                description = re.sub(r'\<\w+>|\</\w+>|\<\w+/>|↵|\n|;|:',' ',description)
                description = re.sub(r'&#x\d+\w+;',' ',description)
                description = description.encode('ascii', 'ignore').decode('ascii')
                description  = re.sub(r'\s+',' ',description).strip()
                return description

            description = description_decoding(record.get('description',''))

            finish = re.findall(r'Finish(.*)Shade',record['product_type_description'])
            shade =  re.findall(r'Shade(.*)Net Weight',record['product_type_description'])
            net_weight = re.findall(r'\d+.\d+\s*oz',record['product_type_description'])
            if finish:
                finish = description_decoding(finish[0])

            if shade:
                shade = description_decoding(shade[0])

            weight = 0
            for weigh in net_weight:
                weigh=re.findall(r'\d.\d+',weigh)[0]
                weight += float(weigh)

            if finish and finish[0]==',':
                finish = finish[1:].strip()
            elif not finish:
                finish = ''

            if shade and shade[0]==',':
                shade = shade[1:].strip()
            elif not shade:
                shade = ''

            if weight == 0:
                weight = ''
            else:
                weight = str(weight) +" oz"

            if len(record['sale_price'])==3:
                sale_price = str(record['sale_price'][0])
            elif len(record['sale_price'])==4:
                sale_price = str(record['sale_price'][0:2])

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statusCode',''),

                'category': record.get('category',''),
                'brand': record.get('brand',''),

                'title': record.get('title',''),
                'image': record.get('image',''),
                'description': description,

                'model': model,
                'weight': weight,
                'finish': finish,
                'shade': shade,
                'msrp':{
                    'currency_code':'USD',
                    'amount':sale_price
                }
            }
            return transformed_record
        else:
            return None
