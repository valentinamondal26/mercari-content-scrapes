'''
https://mercari.atlassian.net/browse/USDE-1669 - Brandy Melville Women's Tops
https://mercari.atlassian.net/browse/USDE-1670 - Brandy Melville Women's Tank Tops
'''

import scrapy
import random
from datetime import datetime
import os
from scrapy.exceptions import CloseSpider
from math import ceil

from urllib.parse import urlparse, parse_qs, urlencode


class BrandymelvilleSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from brandymelville.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Tops
        2) Women's Tank Tops

    brand : str
        brand to be crawled, Supports
        1) Brandy Melville

    Command e.g:
    scrapy crawl brandymelville -a category='Women's Tops' -a brand='Brandy Melville'
    scrapy crawl brandymelville -a category="Women's Tank Tops" -a brand='Brandy Melville'
    """

    name = 'brandymelville'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'brandymelville.com'
    blob_name = 'brandymelville.txt'

    url_dict = {
        "Women's Tops": {
            'Brandy Melville': {
                'url': 'https://us.brandymelville.com/basics/t-shirts',
            }
        },
        "Women's Tank Tops": {
            'Brandy Melville': {
                'url': 'https://us.brandymelville.com/basics/tanks',
            }
        },
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK', ''):
            return
        
        super(BrandymelvilleSpider,self).__init__(*args, **kwargs)
        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)

        
    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://us.brandymelville.com/ashley-top-698p-002-ez.html
        @meta {"use_proxy": "True"}
        @scrape_values title image price description sizes sku specs specs.Fabrics specs.Measurement
        """
        specs = {} 
        for spec in response.xpath('//div[@class="product-attributes-custom-wrapper"]/div'): 
            specs[spec.xpath('./span[@class="attribute-name"]/text()').extract_first()] = spec.xpath('./span[@class="attribute-value"]/text()').extract_first() 

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//h1[@class="page-title"]/span/text()').extract_first(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'price': response.xpath('//div[@class="product-info-price"]//span[@class="price"]/text()').extract_first(),
            'description': response.xpath('//div[@class="product-attribute attribute-code-description "]').xpath('.//text()[normalize-space()]').extract(),
            'sizes': response.xpath('//div[@class="product-attribute attribute-code-size "]/div[@class="attribute-value"]/span/text()').extract(),
            'sku': response.xpath('//div[@class="product-attribute attribute-code-sku "]/div[@class="attribute-value"]/span/text()').extract_first(),
            'specs': specs,
        }


    def parse(self, response):
        """
        @url https://us.brandymelville.com/basics/t-shirts
        @meta {"use_proxy": "True"}
        @returns requests 1
        """
        for product in response.xpath('//ol[@class="products list items product-items"]/li[@class="item product product-item"]'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                yield self.create_request(url=id, callback=self.crawlDetail)

        page_numbers = response.xpath('//div[@class="toolbar toolbar-products"][1]/p[@id="toolbar-amount"]/span[@class="toolbar-number"]/text()').extract()
        self.logger.info(f'{page_numbers}: page_numbers')

        if page_numbers and len(page_numbers) > 2 and int(page_numbers[1]) < int(page_numbers[2]):
            total_products = int(page_numbers[2])
            items_per_page = 32
            total_pages = ceil(total_products / items_per_page)
            url = f'{response.url}?p={total_pages}'
            yield self.create_request(url=url, callback=self.parse)


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

