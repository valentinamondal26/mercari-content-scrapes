import hashlib
import re
from colour import Color

class Mapper(object):
    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def map(self, record):
        if record:
            description = record.get('description', [''])[0].replace('\n', ' ').replace('\r', ' ')
            description = re.sub(r'\s+', ' ', description)
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\s+', ' ', model).strip()

            colors = [word.strip('.') for word in re.split(r'\s', description) if self.check_color(word.strip('.'))]
            colors = list(filter(None, colors))
            colors = list(dict.fromkeys(colors))
            color = sorted(colors)[0] if colors else ''
            color = color.title()

            material=''
            fabric = record.get('specs', {}).get('Fabrics', '')
            match = re.findall(r'\d+%\s*(.*)', fabric)
            if match:
                material = ', '.join(match)

            if not material:
                match = re.findall(r'\d+%\s*(.*?)\r',
                    '\r'.join(list(record.get('specs', {}).values())) \
                        or ' '.join(record.get('description', [])))
                if match:
                    material = ', '.join(match)
            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()

            measurement = record.get('specs', {}).get('Measurement', '')
            if not re.findall(r'(\d+")', measurement):
                measurement = '\r'.join(list(record.get('specs', {}).values())) \
                    or ' '.join(record.get('description', []))
            length = ''
            match = re.findall(r'(\d+") length', measurement)
            if match:
                length = match[0]
            if not length:
                match = re.findall(r'(\d+) length', measurement)
                if match:
                    length = match[0] + '"'

            chest_width = ''
            match = re.findall(r'(\d+") bust', measurement)
            if match:
                chest_width = match[0]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                'length': length,
                'chest_width': chest_width,
                'model_sku': record.get('sku', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
