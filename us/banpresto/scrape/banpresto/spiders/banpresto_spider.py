'''
https://mercari.atlassian.net/browse/USCC-715 - Banpresto Plush Figures
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

class BanprestoSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from banpresto.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Plush Figures

    brand : str
        brand to be crawled, Supports
        1) Banpresto

    Command e.g:
    scrapy crawl banpresto -a category='Plush Figures' -a brand='Banpresto'
    """

    name = 'banpresto'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'banpresto.com'
    blob_name = 'banpresto.txt'

    url_dict = {
        'Plush Figures': {
            'Banpresto': {
                'url': 'https://www.banpresto.jp/prize/index.html',
            }
        }
    }

    base_url = 'https://www.banpresto.jp'
    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BanprestoSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.banpresto.jp/prize/0114.html
        @meta {"use_proxy":"True"}
        @scrape_values id title image franchise
        """
        series = response.meta.get("series", "") or response.xpath('//div[@class="page-title -product"]/text()').extract_first()
        for item in response.xpath("//section[@class='product-content']//figure[@class='grid-item']"):
            image = item.xpath(".//img/@src").extract_first()
            title = item.xpath('normalize-space(.//p)').extract_first()
            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': response.url+f'?title={title}',

                'title': title,
                'image': image,
                'franchise': series
            }


    def parse(self, response):
        """
        @url https://www.banpresto.jp/prize/index.html
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for series in response.xpath("//figure[@class='grid-item']"):
            series_url = series.xpath('./a/@href').extract_first()
            series = series.xpath('.//p/text()').extract_first()
            if series_url:
                yield self.create_request(url=self.base_url+series_url, callback=self.crawlDetail, meta={'series':series})

        next_page_link = response.xpath('//@data-grid-url').extract_first()
        if next_page_link:
            yield self.create_request(url=next_page_link, callback=self.parse)



    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
