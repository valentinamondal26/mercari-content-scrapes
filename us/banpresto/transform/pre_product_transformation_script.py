import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            image = record.get('image', '')
            if not image.startswith('https://www.banpresto.jp/'):
                image = 'https://www.banpresto.jp/' + image

            franchise = record.get("franchise", "")
            franchise = franchise.title()

            model = record.get('title', '')
            if franchise:
                model = re.sub(rf'\b{franchise}\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPlush\b|\bFigure\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            category = ''
            if re.findall(r'\bplush\b', record.get('title', ''), flags=re.IGNORECASE):
                category = 'Plush Figures'
            else:
                category = 'Action Figures'


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": category,
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": image,

                "model": model,
                "franchise": franchise,
            }

            return transformed_record
        else:
            return None
