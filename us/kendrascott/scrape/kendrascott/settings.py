# -*- coding: utf-8 -*-

# Scrapy settings for kendrascott project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'kendrascott'
DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'


SPIDER_MODULES = ['kendrascott.spiders']
NEWSPIDER_MODULE = 'kendrascott.spiders'

ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 10

DOWNLOAD_DELAY = 6

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
   'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}
