# -*- coding: utf-8 -*-
'''
Crawl Requirements:
https://mercari.atlassian.net/browse/USDE-1071 - Kendra Scott Earrings
https://mercari.atlassian.net/browse/USDE-1072 - Kendra Scott Necklaces
'''
import scrapy
import math
from datetime import datetime
import os
from scrapy.exceptions import CloseSpider
import itertools


class KendrascottSpider(scrapy.Spider):

    """
     spider to crawl items in a provided category and sub-category from kendrascott.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Earrings
        2) Necklaces

     brand : str
      brand to be crawled, Supports
       1) Kendra Scott

     Command e.g:
     scrapy crawl kendrascott -a category='Earrings' -a brand='Kendra Scott'
     scrapy crawl kendrascott -a category='Necklaces' -a brand='Kendra Scott'

    """
    name = "kendrascott"

    source = 'kendrascott.com'
    blob_name = 'kendrascott.txt'

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None

    base_url = 'https://kendrascott.com'

    url_dict = {
        "Earrings": {
            "Kendra Scott": {
                "url" : "https://www.kendrascott.com/jewelry/categories/earrings/",
                "filters": ["Style", "Metal", "Color"]
            }
        },
        "Necklaces": {
            "Kendra Scott": {
                "url" : "https://www.kendrascott.com/jewelry/categories/necklaces/",
                "filters": ["Style", "Metal", "Color"]
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(KendrascottSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.round_robin = itertools.cycle(self.proxy_pool)

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')
        if not self.launch_url:
            self.logger.error(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.category = 'Earrings'
        self.filters = ["Style", "Metal", "Color"]
        self.round_robin = itertools.cycle(self.proxy_pool)


    def start_requests(self):
        if self.launch_url:
            yield scrapy.Request(url=self.launch_url, callback=self.total_filters)


    def total_filters(self, response):
        """
        @url https://www.kendrascott.com/jewelry/categories/earrings/
        @returns requests 10 70
        @meta {"use_proxy":"True"}
        """

        items_count = response.xpath(
            "//span[@class='item-count']/text()").get()
        items_count = [int(i) for i in items_count.split() if i.isdigit()][0]
        total_pages = math.ceil(items_count/100)
        start = 0
        size = 100
        # meta = response.meta
        for i in range(0, total_pages):
            main_page_url = "https://www.kendrascott.com/jewelry/categories/" + \
                self.category.lower()+"/?start="+str(start)+"&sz=" + \
                str(size)+"&format=page-element"
            # meta.update({"proxy":random.choice(self.proxy_pool)})
            yield self.createRequest(url=main_page_url, callback=self.filter_page, meta=response.meta)
            start += 100
            size += 100

        for filter in self.filters:
            filter = filter.title()
            total_style_filters = response.xpath(
                "//section[@class ='plp-filters']//div//div//ul//li[contains(@id, '"+filter+"')]//ul/li/a/@href").getall()
            total_style_name = response.xpath(
                "//section[@class ='plp-filters']//div//div//ul//li[contains(@id,'"+filter+"')]//ul/li/a/@title").getall()

            i = 0
            for filter_url in total_style_filters:
                style_name = total_style_name[i].replace("Refine by:", "")
                #meta = {"filter": filter, "filter_name": style_name}
                # meta.update({"proxy":random.choice(self.proxy_pool)})
                yield self.createRequest(url=filter_url, callback=self.filter_page_request,
                    meta={"filter": filter  ,"filter_name": style_name})
                i += 1


    def filter_page_request(self, response):
        """
        @url https://www.kendrascott.com/jewelry/categories/earrings/?prefn1=color&prefv1=Gray
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        start_index = 0
        size = 100
        items_count = response.xpath(
            "//span[@class='item-count']/text()").get()
        items_count = [int(i) for i in items_count.split() if i.isdigit()][0]
        total_pages = math.ceil(items_count/100)
        # print(response.url +" "+str(total_pages))

        for i in range(0, total_pages):
            style_url_api = response.url+"&start=" + \
                str(start_index)+"&sz="+str(size)+"&format=page-element"
            # meta.update({"proxy":random.choice(self.proxy_pool)})
            yield self.createRequest(url=style_url_api, callback=self.filter_page, meta=response.meta)
            start_index += 100
            size += 100


    def filter_page(self, response):
        """
        @url https://www.kendrascott.com/jewelry/categories/earrings/?prefn1=color&prefv1=Gray&start=0&sz=100&format=page-element
        @returns requests 1
        """

        # meta = response.meta
        items = response.xpath(
            "//div[@class='product-name']/h3/a/@href").getall()
        for item_url in items:
            if "https" not in item_url:
                item_url = 'https://www.kendrascott.com' + item_url
            # meta.update({"proxy":random.choice(self.proxy_pool)})
            yield self.createRequest(item_url, callback=self.parse_items, meta=response.meta)


    def parse_items(self, response):
        """
        @url https://www.kendrascott.com/jewelry/categories/earrings/842177101070.html
        @scrape_values item_name price description image item_color
        """

        meta = response.meta
        if response.request.meta.get('redirect_urls'):
            redirected_from_url = response.request.meta['redirect_urls'][0]
        else:
            redirected_from_url = ""
        item_name = response.xpath(
            "//h1[@class='product-name']/text()").get(default="")
        id = response.url
        try:
            breadcrumb = meta["filter"] + "|" + \
                meta["filter_name"] + '|' + item_name
        except:
            breadcrumb = ""
        image = response.xpath("//meta[@property='og:image']/@content").get()
        details = response.xpath(
            "//div[@class='product-expanded-description']/p/text()").getall()
        price = response.xpath(
            "//span/@data-sales-price-value").get(default="")
        if price != "":
            price = "$"+price
        description = response.xpath(
            "//div[@class='product-expanded-description']/p/text()").getall()
        try:
            item_name = item_name.title()
            item_color = item_name.split("In")[1].strip()
        except:
            item_color = ""
        metal = response.xpath('//ul[@class="swatches metal"]/li[contains(@class, "selected")]/a/@title').extract_first(default='').replace('Select Metal: ', '')
        data = {
            "item_name":  item_name,
            "id": response.url,
            # "breadcrumb":breadcrumb,
            "price": price,
            "brand": self.brand,
            "category": self.category,
            "description": description,
            "image": image,
            "status_code": str(response.status),
            "crawl_date": (datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            "item_color": item_color,
            'metal': metal,
            "redirected_from_url": redirected_from_url
        }
        filter = meta.get("filter", "")
        meta_data = {}
        if filter:
            meta_data = meta_data.fromkeys(
                [filter], response.meta["filter_name"])
            data.update(meta_data)
        data["metal_type"] = response.xpath( '//ul[@class="swatches metal"]//li[contains(@class,"selected")]//img/@alt').get(default="")
        yield data


    def createRequest(self, url, callback, errback=None, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, errback=errback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            # request.meta['proxy'] = random.choice(self.proxy_pool)
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request

