import hashlib
import re

class Mapper(object):
    def map(self, record):

        def measurements(measurement_details):
            try:
                width = re.findall(re.compile(r'\d+\.*\d*\"\s*W'),measurement_details)[0].replace("W","")
            except IndexError:
                width = ""

            try:
                length = re.findall(re.compile(r'\d+\.*\d*\"\s*L'),measurement_details)[0].replace("L","")
            except IndexError:
                length = ""

            size={
                "length":length,
                "width":width
            }
            return size

        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model_name =  record.get("item_name","")

            for metal_type in re.findall(r"\w+", record.get("metal_type"))+re.findall(r"\w+", record.get("Metal")):
                model_name = re.sub(re.compile(r"\b{}\b".format(metal_type), re.IGNORECASE),"",model_name)     
            for style_type in record.get("Style","").split(","):
                model_name = re.sub(re.compile(r"\b{}\b".format(style_type), re.IGNORECASE),"",model_name)
            

            metal_type = record.get("metal_type","")
            if not metal_type:
                metal_type = record.get("Metal","").split(", ")[0].strip()
            
            model_name = re.sub(re.compile(r'necklaces*',re.IGNORECASE),"",model_name).strip()
            model_name = re.sub(re.compile(r"\band$|\bin$", re.IGNORECASE),"",model_name)
           

            if record.get("Style","") == '':
                style_filter = ["Necklace Extender","Accent","Gift Set","Statement","Multi Strand","Link & Chain","Long Strand","Choker/Collar","Y & Lariat","Charm Necklace","Pendant"]
                try:
                    style = [filter for filter in style_filter if filter in record.get("item_name","")][0]
                except IndexError:
                    style = ''
            else:
                style = record.get("Style","")

            style = re.sub(re.compile(r"Necklaces*", re.IGNORECASE), "", style)
            style = re.sub(r'\s+'," ", style).strip()
            for style_type in style.split(","):
                model_name = re.sub(re.compile(r"\b{}\b".format(style_type), re.IGNORECASE),"",model_name)
            
            model_name = re.sub(r'\s+'," ",model_name).strip()

            if style=="Y & Lariat":
                model_name = re.sub(re.compile(r"\bY\b", re.IGNORECASE), "", model_name)
            if style =="Long Strand":
                model_name = re.sub(re.compile(r"\bLong\b", re.IGNORECASE), "", model_name)

            model_name = re.sub(r'\s+'," ",model_name).strip()
            description_details = record.get("description","")
            description_details = ",".join(description_details)
            size_data = measurements(description_details)

            if size_data["length"] == '' and size_data["width"] == '':
                measurement_details=record.get("measurements","")
                size_data = measurements(measurement_details)

            try:
                description = record.get("description","")[0].replace('\n', '').replace('\r', '')
            except IndexError:
                description = ""

            transformed_record={
                "id": record.get("id",""),
                "item_id": hex_dig,
                "statusCode": record.get("status_code",""),
                "crawlDate": record.get("crawl_date","").split(",")[0],

                "image": record.get("image",""),
                

                "brand": record.get('brand', ''),
                "category": record.get('category', ''),
                "title": record.get("item_name",""),
                "model": model_name,
                "metal_type": metal_type,
                 "product_type": style,
                  "color": record.get("Color",""),

                "msrp": {
                    "currencyCode" : "USD",
                    "amount" : record.get("price","").replace("$","").strip()
                },
                "description": description,
                "width": size_data.get("width",""),
                "length": size_data.get("length",""),
               
               
            }
            
            if transformed_record["product_type"] == 'Kit' or transformed_record["product_type"]  == 'Gift Set' or transformed_record["product_type"] =='Statement' or transformed_record["product_type"] =="Necklace Extender":
                return None
            else:                
                return transformed_record
        else:
            return None
