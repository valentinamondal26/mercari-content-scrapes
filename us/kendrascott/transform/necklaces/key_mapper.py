class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        metal_type = ''

        entities = record['entities']
        for entity in entities:
            if "metal_type" == entity['name']:
                metal_type = entity["attribute"][0]["id"]
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]

        key_field = model + metal_type
        return key_field
