import hashlib
import re

class Mapper(object):
    def map(self, record):
        def measurements(measurement_details):
            try:
                width = re.findall(re.compile(r'\d*\.*\d*\"\s*W'),measurement_details)[0].replace("W","")
            except IndexError:
                width = ""
            
            try:
                length = re.findall(re.compile(r'\d*\.*\d*\"\s*L'),measurement_details)[0].replace("L","")
            except IndexError:
                length = ""
            
            size={
                "length":length,
                "width":width
            }
            return size

        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model_name =  record.get("item_name","")

            for style_type in record.get("Style","").split(","):
                model_name = re.sub(re.compile(r"\b{}\b".format(style_type), re.IGNORECASE),"",model_name)
            model_name = re.sub(re.compile(r'earrings*',re.IGNORECASE),"",model_name).strip()
            model_name = re.sub(re.compile(r"and$|in$", re.IGNORECASE),"",model_name)
            model_name = re.sub(r'\s+'," ",model_name).strip()

            if record.get("Style","") == '':
                style_filter = ["Kit","Ear Climber","Gift Set","Huggie","Linear","Statement","Hoop","Stud","Drop"]
                try:
                    style = [filter for filter in style_filter if filter in record.get("item_name","")][0]
                except IndexError:
                    style = ''
            else:
                style = record.get("Style","")    

            for style_type in style.split(","):
                model_name = re.sub(re.compile(r"\b{}\b".format(style_type), re.IGNORECASE),"",model_name)
            
            model_name = re.sub(r'\s+'," ",model_name).strip()
            
            
            metal_type = record.get("metal_type","")
            if not metal_type:
                metal_type = record.get("Metal","").split(", ")[0].strip()

            metal_type_meta = ['Silver', 'Rose Gold', 'Gunmetal', 'Mixed Metal', 'Vintage Silver', 'Vintage Gold', 'Gold']
            if not metal_type:
                if re.findall(re.compile(r"|".join(metal_type_meta), re.IGNORECASE), record.get("item_name","")):
                    metal_type = re.findall(re.compile(r"|".join(metal_type_meta), re.IGNORECASE), record.get("item_name","")) [0]
            
            description_details = record.get("description","")
            description_details = ",".join(description_details)
            size_data = measurements(description_details)


            if style.replace("Ear Climber","Climber").strip() == "Climber":
                model_name = re.sub(re.compile(r"Ear|Climbers*", re.IGNORECASE), "", model_name)
            
            def to_lower(match):
                return match.group(0).lower()
            model_name = re.sub(r"(\'\w)", to_lower, model_name)
            model_name = re.sub(r'\s+'," ",model_name).strip()
            
            if size_data["length"] == '' and size_data["width"] == '':
                measurement_details=record.get("measurements","")
                size_data = measurements(measurement_details)

            try:
                description = record.get("description","")[0].replace('\n', '').replace('\r', '')
            except IndexError:
                description = ""

            if re.findall(re.compile(r"\bset\b", re.IGNORECASE), record.get("item_name")):
                return None

            transformed_record={
                "id": record.get("id",""),
                "item_id": hex_dig,
                "statusCode": record.get("status_code",""),
                "crawlDate": record.get("crawl_date","").split(",")[0],

                "title": record.get("item_name",""),
                "image": record.get("image",""),

                "model": model_name,
                "msrp": {
                    "currencyCode" : "USD",
                    "amount" : record.get("price","").replace("$","").strip()
                },
                "description": description,
                "brand": record.get('brand', ''),
                "category": record.get('category', ''),
                "width": size_data.get("width",""),
                "length": size_data.get("length",""),
                "metal_type": metal_type,
                "color": record.get("Color",""),
                "product_type": style.replace("Ear Climber","Climber").strip(),
            }
            if transformed_record["product_type"] == 'Kit' or transformed_record["product_type"]  == 'Gift Set' or transformed_record["product_type"] =='Statement':
                return None
            else:                
                return transformed_record
        else:
            return None
