'''
https://mercari.atlassian.net/browse/USCC-201 - Alphalete Pants, tights, leggings
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
from urllib.parse import urlparse, parse_qs, urlencode


class AlphaleteathleticsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from alphaleteathletics.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Pants, tights, leggings

    sub_category : str
        brand to be crawled, Supports
        1) Alphalete

    Command e.g:
    scrapy crawl alphaleteathletics -a category='Pants, tights, leggings' -a brand='Alphalete'
    """

    name = "alphaleteathletics"

    category = None
    brand = None
    source = 'alphaleteathletics.com'
    blob_name = 'alphaleteathletics.txt'

    base_url = 'https://alphaleteathletics.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    url_dict = {
        'Pants, tights, leggings': {
            'Alphalete': {
                'url': 'https://alphaleteathletics.com/collections/most-anticipated',
            }
        }
    }


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AlphaleteathleticsSpider,self).__init__(*args, **kwargs)
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.category = 'Pants, tights, leggings'
        self.brand = 'Alphalete'


    def parse(self, response):
        """
        @url https://alphaleteathletics.com/collections/most-anticipated
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        detail_urls = response.xpath('//div[@class="grid-product__content"]/a[@class="grid-product__link "]/@href').extract()
        for detail_url in detail_urls:
            request = self.create_request(self.base_url + detail_url, self.parse_detail)
            yield request


    def parse_detail(self, response):
        """
        @url https://alphaleteathletics.com/collections/womens-leggings/products/surface-pocket-laser-cut-legging-olive
        @scrape_values title price image color material description
        """

        material = response.xpath('//div[@itemprop="description"]//strong[contains(text(),"Materials & Washing Directions:")]/../following-sibling::ul/li//text()[normalize-space()]').extract_first(default="") or ""
        if not material:
            material = response.xpath('//div[@itemprop="description"]//strong[contains(text(),"Materials & Washing Directions:")]//following-sibling::ul/li//text()[normalize-space()]').extract_first(default="") or ""

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': '200',
            'category': self.category,
            'id': response.xpath('//head//meta[@property="og:url"]/@content').get(default="") or "",
            'brand': self.brand,

            'description': response.xpath('//meta[@name="description"]/@content').extract_first(default="") or "",
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(default="") or "",

            'title': response.xpath('//h1[@class="product-single__title new-product-title"]/text()').extract_first(default="") or "",
            'color': response.xpath('//h1[@class="new-product-color"]/text()').extract_first(default="") or "",
            'price': response.xpath(u'normalize-space(//span[@itemprop="price"]/text())').extract_first(default="") or "",
            'material': material,
        }


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
