import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            
            title = record.get("title","")
            
            # price =  record.get("price","").replace("$","")
            description_data = record.get("description","") 
            description = description_data[0]

            specification_data  = record["specification"]
            material_index = specification_data.index("Material:")
            material = specification_data[material_index+1]
            material = re.findall(r'\d+\%\s+(\w+)',material)
            material = ', '.join(material)
            material = re.sub(r'\s+', ' ', material).strip()
            
            try:
                activity_index = description_data.index("Activity:")
                activity = description_data[activity_index+1].strip()
            except ValueError:
                activity = ''

            try:
                volume_index = specification_data.index("Volume:")
                volume = specification_data[volume_index+1].strip()
            except ValueError:
                volume = ''
            
            try:
                height_index = specification_data.index("Height:")
                height = specification_data[height_index+1]
                height = height.replace(',', '.').strip()
            except ValueError:
                height = ''
            
            try:
                width_index = specification_data.index("Width:")
                width = specification_data[width_index+1]
                width = width.replace(',', '.').strip()
            except ValueError:
                width = ''
            
            try:
                weight_index = specification_data.index("Weight:")
                weight = specification_data[weight_index+1]
                weight = weight.replace(',', '.').strip()
            except ValueError:
                weight = ''

            try:
                depth_index = specification_data.index("Depth:")
                depth = specification_data[depth_index+1]
                depth = depth.replace(',', '.').strip()
            except ValueError:
                depth = ''

            model_sku = record.get("model_sku","").split("::")[0].strip()

            color = record.get("color", "")
            color = color.split(' - ')[-1]
            color = ', '.join(list(dict.fromkeys(color.split('-'))))
            match = re.findall(re.compile(r'\bLava\b', re.IGNORECASE), color)
            if match and len(match) > 1:
                color = ', '.join(list(filter(lambda x: x.lower() != 'lava', color.split(', '))))
                print(color)
            color = re.sub(r'\s+', ' ', color).strip()

            model = re.sub(re.compile(r'\bBackpack\b',re.IGNORECASE),"",title)
            model = re.sub(re.compile(volume.replace('l', '').strip(), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'No\.', re.IGNORECASE), '', model)
            d = {
                'M-L': 'M/L',
                'S-M': 'S/M',
            }
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b{}\b'.format(x), d.keys()))), re.IGNORECASE), lambda x: d.get(x.group()), model)
            model = re.sub(r'\s+'," ",model).strip()

            if re.findall(r"Laptop Case|Hip Pack|Shoulder bag|Duffel|Laptop", model):
                return None

            transformed_record = {
                "title" : title,
                "model" : model,
                "category" : record.get('category',""),
                "brand" : record.get("brand",""),
                "image" : record.get("image",""),
                "description" : description,
                "status_code":record.get("status_code",""),
                "id":record.get("id",""),
                "item_id" : hex_dig,
                "model_sku" :model_sku,
                "activity" : activity,
                "material" : material,
                "crawl_date": record.get('crawl_date',"").split(",")[0],
                "color": color,
                "height": height,
                "depth": depth,
                "width": width,
                "volume": volume,
                "weight": weight,
                "product_line": record.get("Family",""),
                "color_family": record.get("Color",""),
                "price": {
                    "currency_code" : "USD",
                    "amount" : record.get("price","")
                }
            }

            return transformed_record
        else:
            return None
