'''
https://mercari.atlassian.net/browse/USDE-1212 - Fjallraven Backpacks
'''
import scrapy
import json

from scrapy.http import FormRequest
import hashlib
import os
import math
from scrapy import signals
from datetime import datetime
import re
from scrapy.exceptions import CloseSpider
import random
import itertools


class FjallravenSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and sub-category from Fjallraven.com

    Attributes

    category : str
        category to be crawled, Supports
        1) Backpacks 
        

    brand : str
    brand to be crawled, Supports
        1) Fjallraven

    Command e.g:
    scrapy crawl fjallraven  -a category='Backpacks' -a brand='Fjallraven' 

    """
    name = "fjallraven"

    source = 'fjallraven.com'
    blob_name = 'fjallraven.txt'

    category = None
    brand = None

    merge_key = 'id'

    filters = None
    launch_url = None

    spider_project_name = "fjallraven"
    gcs_cache_file_name = 'fjallraven.tar.gz'

    base_url = 'https://www.fjallraven.com'

    url_dict = {
        "Backpacks":{
            "Fjallraven":{
                "url": "https://www.fjallraven.com/us/en-us/bags-gear/backpacks-bags",
                "filter": ["Color","Family"]
            }
        }
    }

    proxy_pool = [
            'http://shp-mercari-us-d00001.tp-ns.com:80',
            'http://shp-mercari-us-d00002.tp-ns.com:80',
            'http://shp-mercari-us-d00003.tp-ns.com:80',
            'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(FjallravenSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.round_robin = itertools.cycle(self.proxy_pool)
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand,{}).get('url','')
            self.filters=self.url_dict.get(category,{}).get(brand,{}).get('filter','')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ["Color","Family"]
        self.round_robin = itertools.cycle(self.proxy_pool)


    def start_requests(self):
        meta = {"use_filter":True}
        request = self.createRequest(self.launch_url,callback = self.parse,meta=meta)
        yield request


    def parse(self,response):
        """
        @url https://www.fjallraven.com/us/en-us/bags-gear/backpacks-bags
        @returns requests 19
        @meta {"use_proxy":"True"}
        """
        # Number of request mentioned in contracts is 19 beacause 18 items per page and 1 Next Page url
        meta = response.meta
        product_urls = response.xpath("//div[@class='product--images']/a/@href").getall()
        for product_url in product_urls:
            meta.update({"use_cache":True})
            if self.base_url not in product_url:
                product_url = self.base_url + product_url
            request = self.createRequest(url = product_url,callback = self.crawldetail,meta=meta)
            yield request
        next_page_url = response.xpath("//div[@class='pager']/a[@class='pagination-item js-search-trigger next']/@href").get()
        if next_page_url:
            request = self.createRequest(url = next_page_url,callback = self.parse,meta=meta)
            yield request
        else:
            if meta.get('use_filter',''):
                # meta.update({"use_cache":True})
                request = self.createRequest(response.url,callback = self.filter_page,meta =meta)
                yield request
            

    def crawldetail(self,response):
        """
        @url https://www.fjallraven.com/us/en-us/bags-gear/backpacks-bags/daypacks/high-coast-rolltop-26?v=F23224%3a%3a7323450598259
        @scrape_values title description specification image color price model_sku
        """
        meta = response.meta
        script_data = response.xpath('//script[contains(text(),"JSON.stringify")]/text()').get()
        script_data=re.sub(r"[\r\n\t\;]+",'',script_data).strip().split("JSON.stringify(")[1].strip(')        head.appendChild(script)')
        json_data = json.loads(script_data)
        title =  json_data["name"]
        description1 = response.xpath("//div[@class='description--text']/text()").getall()
        description1= [" ".join(description1).strip()]
        description2 = response.xpath("//div[@class='description--text']/span//text()").getall()
        description2 = [des.strip() for des in description2 if des.strip() !=""]
        description =  description1+description2
        desc_specs = response.xpath("//div[@class='description--specification']/p//text()").getall() 
        specification = [specs.strip() for specs in desc_specs if specs.strip() !=""]
        try:
            product_data = json_data["model"][0]
        except KeyError:
            product_data = json_data["model"]
        image = product_data["image"]
        color = product_data['color']
        price = product_data['offers']["price"]
        model_sku = product_data['sku']

        data = {
            "title" : title,
            "description" : description,
            "specification" : specification,
            "category" : self.category,
            "brand" : self.brand,
            "status_code": str(response.status),
            "crawl_date" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "id" :response.url,
            "image" : image,
            "color" : color,
            "price" : price,
            "model_sku" : model_sku,
        }
        filter = meta.get("filter","")
        meta_data={}
        if filter!="":
            meta_data = meta_data.fromkeys([filter],meta[filter])
            data.update(meta_data)
        
        yield data


    def filter_page(self,response):
        """
        @url https://www.fjallraven.com/us/en-us/bags-gear/backpacks-bags
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for filter in self.filters:
            filter_page_urls = response.xpath('//label[@for="menu-'+filter+'"]/following-sibling::ul/li/a/@href').getall()

            filter_names = response.xpath('//label[@for="menu-'+filter+'"]/following-sibling::ul/li/a/text()').getall()
            for filter_page_url,filter_name in zip(filter_page_urls,filter_names):
                meta = {}
                meta["filter"] = filter
                # meta.update({"use_cache":True})
                meta.update({meta["filter"]:filter_name.strip()})
                request = self.createRequest(url = filter_page_url,callback = self.parse,meta=meta)
                yield request


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] =  next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request
