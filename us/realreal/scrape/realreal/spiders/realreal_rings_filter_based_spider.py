'''
https://mercari.atlassian.net/browse/USCC-326 - David Yurman Bracelets
https://mercari.atlassian.net/browse/USCC-247 - Earrings Chanel
https://mercari.atlassian.net/browse/USCC-330 - Rings David Yurman
'''

# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
import re
import os
from collections import OrderedDict
import random
from scrapy.exceptions import CloseSpider


class RealrealFilterSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category  from realreal.com for the specified brand

    Attributes

    category : str
        category to be crawled, Supports
        1) Rings
        2) Bracelets
        3) Earrings

    brand : str
        brand to be crawled, Supports
        1) David Yurman
        2) Chanel

    Command e.g:
    scrapy crawl realreal_filter -a category='Rings' -a brand='David Yurman
    scrapy crawl realreal_filter -a category='Bracelets' -a brand='David Yurman'
    scrapy crawl realreal_filter -a category='Earrings' -a brand='Chanel
    """

    name = 'realreal_filter'

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None

    source = 'realreal.com'
    blob_name = 'realreal.txt'

    base_url='https://www.therealreal.com'

    url_dict = {
        'Rings': {
            'David Yurman': {
                'url': 'https://www.therealreal.com/designers/david-yurman/jewelry/rings?gender%5B%5D=58974',
            }
        },
        'Earrings': {
            'Chanel': {
                'url': 'https://www.therealreal.com/designers/chanel/jewelry/earrings?gender%5B%5D=58974',
            }
        },
        'Bracelets': {
            'David Yurman': {
                'url': 'https://www.therealreal.com/designers/david-yurman/jewelry/bracelets',
            }
        }
    }

    proxy_pool = [
            'http://shp-mercari-us-d00001.tp-ns.com:80',
            'http://shp-mercari-us-d00002.tp-ns.com:80',
            'http://shp-mercari-us-d00003.tp-ns.com:80',
            'http://shp-mercari-us-d00004.tp-ns.com:80',
            'http://shp-mercari-us-d00005.tp-ns.com:80',
            'http://shp-mercari-us-d00006.tp-ns.com:80',
            'http://shp-mercari-us-d00007.tp-ns.com:80',
            'http://shp-mercari-us-d00008.tp-ns.com:80',
            'http://shp-mercari-us-d00009.tp-ns.com:80',
            'http://shp-mercari-us-d00010.tp-ns.com:80',
            'http://shp-mercari-us-d00011.tp-ns.com:80',
            'http://shp-mercari-us-d00012.tp-ns.com:80',
            'http://shp-mercari-us-d00013.tp-ns.com:80',
            'http://shp-mercari-us-d00014.tp-ns.com:80',
            'http://shp-mercari-us-d00015.tp-ns.com:80',
            'http://shp-mercari-us-d00016.tp-ns.com:80',
            'http://shp-mercari-us-d00017.tp-ns.com:80',
            'http://shp-mercari-us-d00018.tp-ns.com:80',
            'http://shp-mercari-us-d00019.tp-ns.com:80',
            'http://shp-mercari-us-d00020.tp-ns.com:80',
        ]


    def create_request(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            for key,value in meta.items():
                request.meta[key]=value
        return request


    def __init__(self, category=None,brand=None, *args, **kwargs):
        super(RealrealFilterSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK', ''):
            return

        if not category and not brand :
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')

        if not self.launch_url:
            self.logger.error(f'Spider is not supported for category:{category}, brand:{brand}')
            raise(CloseSpider(f'Spider is not supported for category:{category}, brand:{brand}'))

        self.logger.info(f'Crawling for category {self.category} and brand {self.brand}, url: {self.launch_url}')


    def start_requests(self):
         if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://www.therealreal.com/designers/david-yurman/jewelry/rings?gender%5B%5D=58974
        @returns requests 13
        """
        color_filter={
            'Black':'2',
            'Brown':'4',
            'Red':'13',
            'Blue':'3',
            'Purple':'13',
            'Pink':'12',
            'Orange':'11',
            'Green':'8',
            'Grey':'7',
            'Yellow':'17',
            'White':'16',
            'Gold':'6',
            'Silver':'15'
        }
        for key,value in color_filter.items():
            url=response.url+"&color%5B%5D={color_value}".format(color_value=value)
            yield self.create_request(url=url,callback=self.parse_listing,meta={'meta_color':key})
            # break


    def parse_listing(self,response):
        """
        @url https://www.therealreal.com/designers/david-yurman/jewelry/rings?gender%5B%5D=58974&color%5B%5D=15
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        all_links=response.css('a.product-card.js-plp-product-card::attr(href)').getall()
        all_links=[self.base_url+x for x in all_links]
        for link in all_links:
            yield self.create_request(url=link,callback=self.parse_products,meta=response.meta)
            # break
        next_page_url=response.xpath('//a[@class="pagination__nav-link js-pagination__nav-link "]/@href').getall()
        if next_page_url!=[]:
            next_page_url=next_page_url[len(next_page_url)-1]
            if next_page_url:
                yield self.create_request(url=self.base_url+next_page_url,callback=self.parse)


    def parse_products(self,response):
        """
        @url https://www.therealreal.com/products/jewelry/rings/band/david-yurman-two-tone-wheaton-band-7pckv?position=1
        @scrape_values image title brand size price est_retail description  breadcrumb
        """

        item={}
        item['id'] = response.url
        item['crawlDate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['statusCode'] = str(response.status)
        item['category'] = self.category
        item['image']=response.xpath('//div[@class="pdp-desktop-images"]//img//@src').get(default='')
        item['title']=response.xpath('//div[@class="product-name"]//text()').get(default='')
        item['brand']=self.brand
        item['size']=response.xpath('//div[@class="pdp-title__size"]//text()').get(default='').strip('Size: ')
        item['price']=response.css('div.product-price::text').get(default='')
        item['est_retail']=response.css('div.product-msrp::text').get(default='').strip('Est. Retail ')
        item['description']=response.xpath('//div[@class="description"]//text()').getall()
        item['description']=' '.join(item['description'])
        item['category']=self.category
        breadcrumb=response.xpath('//div[@class="breadcrumbs"]//text()').getall()
        breadcrumbs=[]
        for word in breadcrumb:
            if re.findall(r"\w+",word)!=[]:
                breadcrumbs.append(re.findall(r"\w+",word)[0])

        item['breadcrumb']='|'.join(breadcrumbs)
        item['product_type']=breadcrumbs[-1:]
        item['meta_color']=response.meta.get('color','')

        keys=[]
        values=[]
        details=response.xpath('//dl[@class="description-list js-glossary-terms-replaceable"]/dt[contains(text(),"Details")]//following-sibling::dd/ul')
        for ul in details:
            list=ul.css('li')
            for li in list:
                val=li.css('::text').getall()
                keys.append(val[0].strip())
                values.append(val[1].strip())
        details=response.xpath('//dl[@class="description-list js-glossary-terms-replaceable"]/dt[contains(text(),"Condition")]//following-sibling::dd//text()').getall()
        details=' '.join(details)
        keys.append("condition")
        values.append(details)
        details=OrderedDict()
        details=details.fromkeys(keys,'')
        for i in range(len(keys)):
            if details[keys[i]]=='':
                details[keys[i]]=values[i]
            else:
                details[keys[i]]+=","+values[i]

        item.update(details)
        yield item
