# -*- coding: utf-8 -*-
import scrapy
import random
import datetime
from scrapy.exceptions import CloseSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings


class RealrealSpider(scrapy.Spider):
    name = 'realreal'
    source = 'realreal.com'
    blob_name = 'realreal.txt'

    base_url = 'https://www.therealreal.com'
    login_url = 'https://www.therealreal.com/sessions'
    # gucci_handbag_url = 'https://www.therealreal.com/products?taxons[]=493&keywords=Gucci+Handbags'
    # selected_proxy = 'http://shp-mercari-us-d00001.tp-ns.com/'

    proxy_list = [
            'http://shp-mercari-us-d00001.tp-ns.com/', 
            'http://shp-mercari-us-d00002.tp-ns.com/', 
            'http://shp-mercari-us-d00003.tp-ns.com/',
            'http://shp-mercari-us-d00004.tp-ns.com/', 
            'http://shp-mercari-us-d00005.tp-ns.com/',
            'http://shp-mercari-us-d00006.tp-ns.com/', 
            'http://shp-mercari-us-d00007.tp-ns.com/', 
            'http://shp-mercari-us-d00008.tp-ns.com/',
            'http://shp-mercari-us-d00009.tp-ns.com/', 
            'http://shp-mercari-us-d00010.tp-ns.com/',
            'http://shp-mercari-us-d00011.tp-ns.com/', 
            # 'http://shp-mercari-us-d00012.tp-ns.com/', 
            # 'http://shp-mercari-us-d00013.tp-ns.com/',
            # 'http://shp-mercari-us-d00014.tp-ns.com/', 
            'http://shp-mercari-us-d00015.tp-ns.com/',
            'http://shp-mercari-us-d00016.tp-ns.com/', 
            'http://shp-mercari-us-d00017.tp-ns.com/', 
            'http://shp-mercari-us-d00018.tp-ns.com/',
            'http://shp-mercari-us-d00019.tp-ns.com/', 
            # 'http://shp-mercari-us-d00020.tp-ns.com/',
        ]

    url_dict = {
        'handbags' : {
            'gucci' : 'https://www.therealreal.com/products?taxons[]=493&keywords=Gucci+Handbags',
            'coach' : 'https://www.therealreal.com/products?taxons[]=493&keywords=coach'

        },
        'sneakers' : {
            'coach' : 'https://www.therealreal.com/products?keywords=coach%20sneakers'
        },
        'sandals' : {
            'tory-burch' : 'https://www.therealreal.com/products?taxons[]=494&keywords=tory+burch'
        },
        'Shoulder Bags' : {
            'Louis Vuitton': 'https://www.therealreal.com/products?taxons[]=537&keywords=Louis+Vuitton',
            'Kate Spade': 'https://www.therealreal.com/products?taxons[]=537&keywords=Kate+Spade+New+York',
            'Gucci': 'https://www.therealreal.com/shop/women/handbags/shoulder-bags?designers%5B%5D=544',
            'Michael Kors': 'https://www.therealreal.com/shop/women/handbags/shoulder-bags?designers%5B%5D=912'
        }
    }


    def __init__(self,category=None,brand=None,username=None,pwd=None,*args, **kwargs):
        super(RealrealSpider,self).__init__(*args, **kwargs)

        if(category is not None and brand is not None):
            self.launch_url = self.get_url(category,brand)
            self.category = category
            self.brand = brand

            self.logger.info("Crawling for category "+category+" and brand "+brand)
        else :
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        if(username is not None and pwd is not None):
            self.username = username
            self.pwd = pwd
        else:
            self.logger.error("RealReal needs user authentication.  Please provide username and password")
            raise CloseSpider('User credentials not found')

        
        # self.declare_xpath()

    def declare_xpath(self):
        
        self.getAllProductsXpath = "//div[contains(@class,'product-card-wrapper js-product-card-wrapper')]"

        self.brandXpath =".//div[contains(@class,'product-card__brand')]/text()"
        self.titleXpath = ".//div[contains(@class,'product-card__description')]/text()"
        self.priceXpath = ".//div[contains(@class,'product-card__price')]/text()"

        self.productXpath = ".//a[contains(@class,'product-card js-plp-product-card')]/@href" 
    
        self.modelXpath = ".//ul[contains(@class,'product-details-group')]/li/strong[contains(text(),'Item')]/../text()"
        self.measurementsXpath = '//ul[@class="product-details-group"][1]//text()'
        self.conditionXpath = '//dl[@class="description-list js-glossary-terms-replaceable"]/dt[@class="expanded description-list__item-title js-accordion-trigger" and contains(text(), "Condition")]/following-sibling//text()'
        self.descXpath = ".//p[contains(@class,'description')]/text()"
        
        
        self.imageXpath = ".//div[contains(@class,'main-image')]/figure/img/@src"
        self.retailPriceXpath = ".//div[contains(@class,'product-card__msrp')]/text()"

        self.nextNavXpath = ".//i[contains(@class,'chevron-arrow-right')]/../../a[not(contains(@class,'pagination__nav-link--disabled'))]/@href"

        self.productStatusXpath = ".//div[contains(@class,'product-card__status-label')]/text()"

    
    def start_requests(self):
        self.logger.info("Declaring xpath..")
        self.declare_xpath()
        
        request = scrapy.Request(url = self.base_url,callback=self.login)
        request.meta['proxy'] = self.getProxy()
        
        yield request


    def login(self,response):
        # print("Coming into Login Function")
        token = response.css('input[name="_csrf_token"]::attr(value)').extract_first()
        utf8 = response.css('input[name="_utf8"]::attr(value)').extract_first()
        data = {
            '_csrf_token' : token,
            '_utf8' : utf8,
            'user[email]':self.username,
            'user[password]':self.pwd,
            'user[remember_me]':'false'
        }

        # print("*******!!!!!!!!!!~~~~~~~~~~~~~~~")
        # print("*******!!!!!!!!!!~~~~~~~~~~~~~~~")
        # print(token)
        self.logger.info("Logging into realreal")
        formRequest = scrapy.FormRequest(url = self.login_url,formdata =  data,callback=self.loginResponse)
        formRequest.meta['proxy'] = self.getProxy()
        yield formRequest


    def loginResponse(self,response):
        
        if (response.status == 200):
            self.logger.info("Login Successfull " + response.meta['proxy'])

            request = scrapy.Request(url = self.launch_url,callback=self.parseListingPage)
            # request = scrapy.Request(url = self.gucci_handbag_url,callback=self.parseListingPage)
            request.meta['proxy'] = self.getProxy()
            yield request
        else :
            self.logger.info("Logging Failed")
            raise CloseSpider('Authentication Failed in RealReal')
            # print("login failed  with response code "+str(response.status)) 
            # yield None   


    def parseListingPage(self,response):

        for productWrapper in response.xpath(self.getAllProductsXpath):
            
            item = {}
            item['brand'] = productWrapper.xpath(self.brandXpath).extract_first()
            item['price'] = productWrapper.xpath(self.priceXpath).extract_first()
            item['title'] = productWrapper.xpath(self.titleXpath).extract_first()

            productURL = productWrapper.xpath(self.productXpath).extract()
            productURL = self.base_url + str(productURL[0])
            item['id'] = productURL

            retailPrice = productWrapper.xpath(self.retailPriceXpath).extract_first()
            

            if retailPrice :
                item['est_retail'] = retailPrice[retailPrice.index("$"):len(retailPrice)]
            else :
                item['est_retail'] =  ""

            status = productWrapper.xpath(self.productStatusXpath).extract_first()

            if(status):
                item['sell_status'] = status
            else:
                item['sell_status'] = 'available'

            item['model'] = ''
            item['style'] = ""
            item['collection']  = ""
            item['material'] = ""

            request = scrapy.Request(url = productURL,callback=self.parseProductPage,errback=self.errorHandler)
            request.meta['proxy'] = self.getProxy()
            request.meta['item'] = item
                        
            yield request

        next_nav = response.xpath(self.nextNavXpath)

        
        if(next_nav):
            self.page_label = False
            # print(" ...... Redirecting to next page ...... ")
            
            next_nav_href = next_nav.extract_first()
            next_page_url = self.base_url + str(next_nav_href)
            # print(next_page_url)
            request = scrapy.Request(url = next_page_url,callback=self.parseListingPage)
            request.meta['proxy'] = self.getProxy()
            yield request


    def errorHandler(self,failure):
        self.logger.warn("Handling failed request "+ failure.request.url)
        alt_proxy = 'http://shp-mercari-us-d00020.tp-ns.com/'
        
        failed_proxy = failure.request.meta['proxy']

        item = failure.request.meta['item']
    
        request = scrapy.Request(url = failure.request.url,
            callback=self.parseProductPage,dont_filter=True)
        request.meta['proxy'] = alt_proxy
        request.meta['item'] = item
        yield request

    
    def parseProductPage(self,response):

        item = response.meta['item']

        item['statusCode'] = str(response.status)

        if(response.status == 200):

            item['description'] = response.xpath(self.descXpath).extract_first()
            item['image'] = response.xpath(self.imageXpath).extract_first()
            item['category'] = self.category
            item['style'] = response.xpath(self.modelXpath).extract_first() or ''
            item['crawlDate'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            item['measurements'] = ' '.join(response.xpath(self.measurementsXpath).extract()) or ''
            item['condition'] = response.xpath(self.conditionXpath).extract_first() or ''
            
        else:
            self.logger.warn("Request failed with status code "+str(response.status))
           
        print(item)
        yield item


    def getProxy(self):
        random_proxy = random.choice(self.proxy_list)
        return random_proxy


    def get_url(self,category,brand):
        url = self.url_dict[category][brand]
        return url


def crawl_from_airflow(category,brand,username,pwd):
    process = CrawlerProcess(get_project_settings())
    process.crawl(RealrealSpider,category=category,brand=brand,username=username,pwd=pwd)
    process.start()  # the script will block here until the crawling is finished  

   