#Key Mapper for realreal data - model + metal_color combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]
            elif "metal_color" == entity['name'] and entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]

        return key_field
