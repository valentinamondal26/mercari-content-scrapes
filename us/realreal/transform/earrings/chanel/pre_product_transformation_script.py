import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            condition = record.get('condition', '')
            ner_query = description + ' ' + condition
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')

            # extract measurements
            length = ''
            width = ''
            measurements = record.get('Measurements:', '')
            length_pattern = re.compile(r'length\s*\d+\.*\d*', re.IGNORECASE)
            if re.findall(length_pattern, measurements):
                length = re.findall(length_pattern, measurements)[0]
                length = re.sub(r"[a-zA-Z]+\s*", '', length)

            width_pattern = re.compile(r'width\s*\d+\.*\d*', re.IGNORECASE)
            if re.findall(width_pattern, measurements):
                width = re.findall(width_pattern, measurements)[0]
                width = re.sub(r"[a-zA-Z]+\s*", '', width)

            # product type transformations
            product_type = record.get('product_type', '')[0]
            if product_type == "Clip":
                product_type = "Clip-On"
            if product_type == "Ear":
                product_type = "Ear Climber"

            # model transformations
            pattern = r"Chanel|" + \
                      product_type + \
                      "s*|\bEarring\s*s{0,1}\b|Est\s*\.*\s*Retail\s*\$*" \
                      "\s*\d+\.*\d*|\-{1}\s*On"
            pattern = re.compile(pattern, re.IGNORECASE)

            # model and product type fixing for empty models
            if record.get('title', '') == "Earrings":
                product_type = "Stud"
                model = "Large Stud with Large Pearls"
            if "CC Hoop" in record.get("title",
                                       "") and product_type == "Earrings":
                product_type = "Drop"

            # model transformation - adding earrings if not present in model
            model = re.sub(re.compile(r"\bEarring\b", re.IGNORECASE),
                           "Earrings", model)
            model = re.sub(pattern, '', model)
            if not re.findall(re.compile(r"\bEarrings\b",
                              re.IGNORECASE), model):
                model += " Earrings"
            model = ' '.join(model.split())

            transformed_record = {
                    "id": record['id'],
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),
                    "category": "Earrings",
                    "description": description,
                    "item_condition": condition,
                    "title": record.get('title', ''),
                    "model": model,
                    "brand": record.get('brand', ''),
                    "image": record.get('image', ''),
                    "ner_query": ner_query,
                    "price": {
                            "currency_code": "USD",
                            "amount": record.get("price", "").strip(
                                    '$').replace(',', '')
                    },
                    "msrp": {
                            "currency_code": "USD",
                            "amount": record.get("est_retail", "").strip(
                                    '$').replace(',', '')
                    },
                    "metal_type": record.get("Metal Type:", ""),
                    "metal_color": record.get('metal_color', ''),
                    "metal_finish": record.get('Metal Finish:', ''),
                    "product_type": product_type,
                    "authentication": record.get('Hallmark:', ''),
                    "location": record.get('Location:', ''),
                    "weight": record.get('Total Item Weight (g):', ''),
                    "material": record.get('Non-Gem Materials:', ''),
                    "width": width,
                    "length": length,
                    "model_sku": record.get('Item #', ''),

            }

            return transformed_record
        else:
            return None
