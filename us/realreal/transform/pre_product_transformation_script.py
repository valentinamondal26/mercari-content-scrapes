import hashlib 


class Mapper(object):

    def get_dimensions(self, measurements):
        dimensions = measurements.split("\"")
        dim_dict = dict()

        for item in dimensions:
            if len(item)>0:
                item_val = item.split(":")
                val = {'unit': 'INCH', 'value': item_val[1].strip()}
                key = self.cleanup_keys(item_val[0])
                dim_dict[key] = val

        return dim_dict

    def cleanup_keys(self, key):
        return key.strip().lower().replace(" ","_")

    def isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v)>0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)
            dim = 0
            if 'measurements' in record:
                dim = self.get_dimensions(record['measurements'])
            ner_query= record['description']+" " +record['condition']+" "+record['material']
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawlDate": record['crawlDate'],
                "statusCode": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "collection": record['collection'],
                "description": record['description'],
                "title": record['title'],
                "style": record['style'],
                "ner_query": ner_query,
                "price":{
                    "currencyCode": "USD",
                    "amount": price
                },
                "estimated_original_price": record['est_retail'],
                "sell_status": record['sell_status'],
                "measurements": record['measurements'],
                "condition": record['condition'],
                "material": record['material']
            }

            if dim:
                transformed_record['itemDimensions'] = dim
            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None