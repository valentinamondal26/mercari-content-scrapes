import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            condition = record.get('condition', '')
            ner_query = description + ' ' + condition
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            brand=record.get('brand','')
            if brand=="Earrings" or brand=="Engagement Ring" or brand=="Ring":
                brand="Generic"

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": "Rings",
                "description": description,
                "condition": condition,
                "title": record.get('title',''),
                "brand": brand,
                "image": record.get('image',''),
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "est_retail":{
                     "currency_code": "USD",
                    "amount": record.get("est_retail","").strip('$').replace(',','')
                },
                "size" :record.get("size",""),
                "metal_type":record.get("Metal Type",""),
                "collection" : record.get('Collection',''),
                "gemstone": record.get('Gemstone',''),
                "carat_total_weight": record.get('Carat Total Weight',''),
                "stone_shape": record.get('Stone Shape',''),
                "color_grade" : record.get('Color Grade',''),
                "clarity_grade":record.get('Clarity Grade',''),


                }

            return transformed_record
        else:
            return None
