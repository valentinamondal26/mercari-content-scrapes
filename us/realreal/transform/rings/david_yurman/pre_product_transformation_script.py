import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            condition = record.get('condition', '')
            ner_query = description + ' ' + condition
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            stone_dimensions=record.get('Stone Dimensions (mm):','')

            condition=condition[:condition.find('.')]
            model=record.get('title','')
            length=''
            width=''
            band_width=''
            length_pattern= re.compile(r'length\s*\d+\.*\d*', re.IGNORECASE)
            if re.findall(length_pattern,stone_dimensions)!=[]:
                length=re.findall(length_pattern,stone_dimensions)[0]
                length=re.sub(r"[a-zA-Z]+\s*",'',length)

            width_pattern= re.compile(r'width\s*\d+\.*\d*', re.IGNORECASE)
            if re.findall(width_pattern,stone_dimensions)!=[]:
                width=re.findall(width_pattern,stone_dimensions)[0]
                width=re.sub(r"[a-zA-Z]+\s*",'',width)

            meaurements=record.get('Measurements:','')
            if re.findall(r"Band Width \d+\.*\d*mm",meaurements)!=[]:
                band_width=re.findall(r"Band Width \d+\.*\d*mm",meaurements)[0]
                band_width=re.sub(r"Band Width\s*",'',band_width)

            transparency=record.get('Transparency:','')
            transparency=re.sub(r'\,{1}\w*\d*','',transparency)
            product_type=record.get('product_type','')[0]
            carat=record.get('Carat Total Weight:','')

            product_type=record.get('product_type','')[0]
            if product_type=="Rings":
                product_type="Ring"

            pattern=r"David\s*Yurman|"+product_type+"|Ring\s*s{0,1}|Est\s*\.*\s*Retail\s*\$*\s*\d+\.*\d*|"+carat+"|\d+\s*k{1}"
            if carat=='':
                pattern=r"David\s*Yurman|"+product_type+"|Ring\s*s{0,1}|Est\s*\.*\s*Retail\s*\$*\s*\d+\.*\d*|\d+\s*k{1}"
            pattern=re.compile(pattern+carat,re.IGNORECASE)
            model=re.sub(pattern,'',model)
            model=' '.join(model.split())

            gemstone=', '.join(record.get('Gemstone:','').split(','))



        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": "Rings",
                "description": description,
                "item_condition": condition,
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("est_retail","").strip('$').replace(',','')
                },
                "ring_size" :record.get("size",""),
                "metal_type":record.get("Metal Type:",""),
                "metal_color":record.get('metal_color',''),
                "metal_finish":record.get('Metal Finish:',''),
                "product_type":product_type,
                "authenticaton":record.get('Hallmark:',''),
                "location":record.get('Location:',''),
                "weight":record.get('Total Item Weight (g):',''),
                "collection" : record.get('Collection:',''),
                "material":record.get('Non-Gem Materials:',''),
                "gemstone": gemstone,
                "width":width,
                "length":length,
                "total_carat_weight": record.get('Carat Total Weight:',''),
                "stone_count":record.get('Stone Count:',''),
                "stone_shape": record.get('Stone Shape:',''),
                "stone_color":record.get('Stone Color:',''),
                "transparency":transparency,
                "band_width":band_width,
                "model_sku" : record.get('Item #',''),

                }

            return transformed_record
        else:
            return None
