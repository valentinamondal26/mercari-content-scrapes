# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


from io import BytesIO
from datetime import datetime
from google.cloud import storage
from scrapy.exporters import JsonLinesItemExporter


class GCSPipeline(object):

    def __init__(self, gcs_blob_prefix):
        self.gcs_blob_prefix = gcs_blob_prefix
        self.bucket_name = 'content_us'
        self.client = None
        self.bucket = None
        self.items = []

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            gcs_blob_prefix=crawler.settings.get('GCS_BLOB_PREFIX'),
        )

    def open_spider(self, spider):
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(self.bucket_name)

    def close_spider(self, spider):
        category = spider.category
        brand = spider.brand

        if not category or not brand:
            spider.logger.error('category or brand is empty, so skip uploading result to gcs')
            return

        source = spider.source
        timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        filename = spider.blob_name
        destination_blob_name = self.gcs_blob_prefix.format(category=category,
            brand=brand, source=source, timestamp=timestamp, filename=filename)

        blob = self.bucket.blob(destination_blob_name)
        f = self._make_fileobj()
        blob.upload_from_file(f)

        spider.logger.info('File uploaded to {}.'.format(destination_blob_name))

    def process_item(self, item, spider):

        # cleanse the item values - replace None values with empty string ""
        image = item['image']
        item['image'] = image if image is not None else ""
        description = item['description']
        item['description'] = description if description is not None else ""
        style = item['style']
        item['style'] = style if style is not None else ""
        rating = item['rating']
        item['rating'] = rating if rating is not None else ""

        self.items.append(item)

        return item

    def _make_fileobj(self):
        """
        Build file object from items.
        """

        bio = BytesIO()
        f = bio

        # Build file object using ItemExporter
        exporter = JsonLinesItemExporter(f)
        exporter.start_exporting()
        for item in self.items:
            exporter.export_item(item)
        exporter.finish_exporting()

        # Seek to the top of file to be read later
        bio.seek(0)

        return bio
