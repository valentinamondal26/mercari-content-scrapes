import scrapy
from scrapy.exceptions import CloseSpider

import re
import json
from datetime import datetime


class PradaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from prada.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Handbags

    Command e.g:
    scrapy crawl prada -a category='Handbags'
    """

    name = "prada"

    url = None
    get_products_api = '{base_url}.glb.getProductsByPartNumbers.json?partNumbers={part_numbers}'

    page_index = 1

    category = None
    brand = 'Prada'
    source = 'prada.com'
    blob_name = 'prada.txt'

    url_dict = {
        'Handbags': 'https://www.prada.com/us/en/women/bags.html',
    }


    def __init__(self, category=None, *args, **kwargs):
        super(PradaSpider,self).__init__(*args, **kwargs)

        if not category:
            self.logger.error("Category should not be None")
            raise CloseSpider('category not found')

        try:
            self.launch_url = self.url_dict[category]
        except Exception as e:
            self.logger.error('Exception occured:', e)
            raise(CloseSpider('Spider is not supported for the category:{}'.format(category)))
        self.category = category

        self.logger.info("Crawling for category "+category+" and brand "+self.brand)


    def start_requests(self):
        yield scrapy.Request(url=self.launch_url, callback=self.parse_start_url)


    def parse_start_url(self, response):
        script = response.xpath('//script[contains(text(), "window.plpPath")]').extract_first()
        if script:
            plp_paths = re.findall(r'\'(.*?)\'', script)
            if plp_paths:
                self.url = self.launch_url.rsplit('.', 1)[0] + plp_paths[0] + '.{page_index}.sortBy_0.html'

        if self.url:
            yield scrapy.Request(url=self.url.format(page_index=self.page_index), callback=self.parse)
        else:
            self.logger.error('Failed to get the start_url')
            CloseSpider('Failed to get the start_url')


    def parse(self, response):
        products = response.css('div.component-productBox')
        part_numbers = []
        for product in products:
            part_numbers.append(product.css('::attr(data-part-number)').extract_first())

        if part_numbers:
            yield scrapy.Request(url=self.get_products_api.format(
                base_url=self.launch_url.rsplit('.', 1)[0], part_numbers=';'.join(part_numbers)),
                callback=self.parse_products)

        load_more = response.css('button.more-button')
        if load_more:
            self.page_index = self.page_index + 1
            yield scrapy.Request(self.url.format(page_index=self.page_index), callback=self.parse)


    def parse_products(self, response):
        res = json.loads(response.text)
        products = res['response']['catalogEntryView']

        for product in products:
            item = {
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': "200",
                        'category': self.category,
                        'brand': self.brand,
                        'breadcrumb':'',
                        'style': "",
                        'rating':''
                    }

            #https://www.prada.com/us/en/women/bags/products.prada_sidonie_leather_shoulder_bag.1BD168_2AIX_F0QNJ_V_OJH.html
            id = '{base_url}/products.{name_path}.{part_number}.html'
            item['description'] = product.get('longDescription', '')
            item['image'] = product['fullImage_ovr']
            item['price'] = product['price_USD_10003']
            item['title'] = product['name']

            part_number = product['mfPartNumber_ntk']
            name_path = product['name'].replace(' ','_')
            item['id'] = id.format(base_url=self.launch_url.rsplit('.', 1)[0], name_path=name_path, part_number=part_number)
            item['model'] = part_number
            item['color'] = product['colors']
            material, collection = self.get_material_collection(product)
            item['material'] = material
            item['product_line'] = collection

            yield item


    def get_material_collection(self, product):
        material = ''
        collection = ''
        for attribute in product['attributes']:
            if attribute['identifier'] == 'MaterialGroup':
                material = attribute['values'][0]['value']
            if attribute['identifier'] == 'PradaType':
                collection = attribute['values'][0]['value']
        return material, collection

