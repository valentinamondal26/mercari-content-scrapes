'''
https://mercari.atlassian.net/browse/USCC-150 - Apple Cell Phone Cables & Adapters
'''

import scrapy
import random
import os
from datetime import datetime
from scrapy.exceptions import CloseSpider


class AppleSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from apple.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Cell Phone Cables & Adapters
    
    brand : str
        brand to be crawled, Supports
        1) Apple

    Command e.g:
    scrapy crawl apple -a category='Cell Phone Cables & Adapters' -a brand='Apple'
    """

    name = 'apple'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'apple.com'
    blob_name = 'apple.txt'

    url_dict = {
        'Cell Phone Cables & Adapters': {
            # 'Apple': 'https://www.apple.com/shop/iphone/iphone-accessories/power-cables?page=1#!&f=adapter-apple-cable&fh=458e%2B45c4%2B45b0%2B3214'
            'Apple': 'https://www.apple.com/shop/iphone/iphone-accessories/power-cables?page=1&fh=458e%2B45c4%2B45b0%2B3214'
        }
    }

    base_url = "https://www.apple.com"

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AppleSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))
       


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.apple.com/shop/product/MKQ42AM/A/usb-c-to-lightning-cable-2-m?fnode=385346d3fc03916395e3ce605864a4c5fb8fabc4bc24c3e710b9cf96ceba0404102f23f01653d7ca828fc0ec9974f73f0e46da026852914d4410fa550d0d51fe875440c1b89a9481e3b1ba472048621b14bf14b052f8fab92e571e9a58e8bcd73c6cba27d4b5f9d79890adcf6c01f60a&fs=fh%3D458e%252B45c4%252B45b0%252B3214
        @scrape_values Whats_in_the_box title description  tech_specs
        """

        overview = response.xpath('//div[contains(@class,"as-productinfosection-sidepanel")][h3[contains(text(),"Overview")]]/following-sibling::div[contains(@class,"as-productinfosection-mainpanel")]//p/text()').getall()
        tech_specs = response.xpath('//div[contains(@class,"as-productinfosection-sidepanel")][h3[contains(text(),"Tech Specs")]]/following-sibling::div[contains(@class,"as-productinfosection-mainpanel")]//p/text()').getall()
        attachments = response.xpath('//div[contains(@class,"as-productinfosection-sidepanel")][h3[contains(text(),"What’s in the Box")]]/following-sibling::div[contains(@class,"as-productinfosection-mainpanel")]//p/text()').getall()
        if attachments != []:
            i=0
            while i <len(attachments):
                attachments[i] =  attachments[i].replace("\n","").strip()
                i+=1
        attachments = ','.join(attachments)
        
        i=0
        tech_dict ={}
        while i <len(tech_specs):
            tech_specs[i] =  tech_specs[i].replace("\n","").strip()
            if ":" in tech_specs[i]:
                specs =  tech_specs[i].split(":")
                tech_dict.update({specs[0] : specs[1]})
            i+=1
        i=0
        while i <len(overview):
            overview[i] =  overview[i].replace("\n","").strip()
            i+=1
        overview = ''.join(overview)
        if not tech_dict:
            tech_dict['tech_specs_str'] =  "\n".join(tech_specs)

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.css("div#title h1::text").extract_first(default='').replace("\n","").replace("\t",""),
            'description': overview,
            'tech_specs': tech_dict,
            'Whats_in_the_box': attachments,
        }


    def parse(self, response):
        """
        @url https://www.apple.com/shop/iphone/iphone-accessories/power-cables?page=1&fh=458e%2B45c4%2B45b0%2B3214
        @returns requests 1
        """

        for product in response.css("h3.as-producttile-name a"):
            product_url = product.xpath("./@href").extract_first()
            if product_url:
                yield self.create_request(url=self.base_url+product_url, callback=self.crawlDetail)

        nextLink = response.css('li#next-tab a::attr(href)').extract_first()
        if nextLink:
            yield self.create_request(url=nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
