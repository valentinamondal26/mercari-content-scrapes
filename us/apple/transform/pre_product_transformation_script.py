class Mapper(object):
    def map(self, record):
        if record:
            record = self.pre_process(record)
            transformed_record = dict()
            for key in record:
                if key == 'miscellaneous':
                    transformed_record['item_id'] = record['miscellaneous']
                else:
                    transformed_record[key] = record[key]
            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
