import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'Apple|\(\d+\s*m\)', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            cable_length = ''
            match = re.findall(re.compile(r'\((\d+\s*m)\)', re.IGNORECASE), record.get('title', ''))
            if match:
                cable_length = match[0]
                cable_length = re.sub(r'\s+', ' ', cable_length).strip()

            model_number = ''
            match = re.findall(re.compile(r'/product/(.*?)/', re.IGNORECASE), record.get('id', ''))
            if match:
                model_number = match[0]

            usb_type = record.get('tech_specs', {}).get('Connections', '')
            if not usb_type:
                match = re.findall(re.compile(r'usb.*\n?', re.IGNORECASE),
                    record.get('tech_specs', {}).get('tech_specs_str', ''))
                if match:
                    usb_type = match[0]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),

                "cable_length": cable_length,
                "model_number": model_number,
                "description": record.get('description', ''),
                "usb_type": usb_type,
                "accessories_included": record.get('Whats_in_the_box', ''),
                "connector_type": record.get('tech_specs', {}).get('Cable type', ''),

                "model": model,
            }

            return transformed_record
        else:
            return None
