'''
https://mercari.atlassian.net/browse/USCC-145 - Hydro Flask Canteens, Bottles & Flasks
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import json


class HydroflaskSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from hydroflask.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Canteens, Bottles & Flasks
    
    brand : str
        brand to be crawled, Supports
        1) Hydro Flask

    Command e.g:
    scrapy crawl hydroflask -a category="Canteens, Bottles & Flasks" -a brand='Hydro Flask'
    """

    name = 'hydroflask'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'hydroflask.com'
    blob_name = 'hydroflask.txt'

    url_dict = {
        'Canteens, Bottles & Flasks': {
            'Hydro Flask': 'https://www.hydroflask.com/bottles',
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(HydroflaskSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.hydroflask.com/12-oz-kids-wide-mouth-bottle
        @meta {"use_proxy":"True"}
        @scrape_values title price product_details description specs color
        """
        title = response.xpath('//h1[@class="page-title"]/span/text()').extract_first(default='')
        price = response.xpath('//span[@class="price"]/text()').extract_first(default='')
        product_details = response.xpath('//div[contains(@class, "product-tab-specs-description")]/ul/li//text()').extract()
        description = response.xpath('//div[contains(@class, "description-pdp-content-text")]/p//text()').extract() or response.xpath('//div[@id="product_intro"]//p/text()').extract()
        specs = {y[0]: y[1] for y in (tr.xpath('./th/text()').extract() for tr in response.xpath('//table[@class="product-details-attributes"]/tbody/tr'))}
        script_data=response.xpath('//div[@class="swatch-opt product-view-swatches"]/following-sibling::script/text()').get()
        color_variant=[]
        if script_data!='':
            data = json.loads(script_data)
            data = data.get('[data-role=swatch-options]',{}).get('swatchRendererPdpExtend',{}).get('jsonConfig',{}).get('attributes',{})
            data = data.get(list(data.keys())[0],{})
            color_class= data.get("id","")
        for color_variant in data.get("options",[]):
            color = color_variant.get("label","")
            url = response.url.strip('/')+'/color,{color},a,{a},o,{o}'.format(
                color=color,
                o=color_variant.get("id",""),
                a=color_class
            )

            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': url,

                'color': color,
                'title': title,
                'price': price,
                'specs': specs,
                'product_details': product_details,
                'description': description,
            }


    def parse(self, response):
        """
        @url https://www.hydroflask.com/bottles
        @meta {"use_proxy":"True"}
        @returns requests 17
        """
        #####add next page
        for product in response.xpath('//div[contains(@class,"product-item-details")]'):

            id = product.xpath('./a/@href').extract_first()

            if id:
                request = self.create_request(url=id, callback=self.crawlDetail)
                yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

