import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            volume = ''
            if re.findall(re.compile(
                    r'\d+ oz', re.IGNORECASE), record.get("title", "")) != []:
                volume = re.findall(re.compile(
                    r'\d+ oz', re.IGNORECASE), record.get("title", ""))[0]

            model = record.get('title', '')
            model = re.sub(re.compile(r'\d+ oz|™', re.IGNORECASE), '', model)
            model = re.sub(re.compile(
                r"\bw\/", re.IGNORECASE), "with", model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "description": '. '.join(record.get('description', '')),

                "title": record.get('title', ''),
                "model": model,
                "color": record.get('color', ''),
                "volume": volume,
                # "mouth_diameter": record.get('specs', {}).get('Mouth Diameter', ''),
                "diameter": record.get('specs', {}).get('Diameter', ''),
                "height": record.get('specs', {}).get('Height', ''),
                "weight": record.get('specs', {}).get('Weight', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
