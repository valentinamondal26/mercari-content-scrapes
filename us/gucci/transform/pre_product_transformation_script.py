import hashlib
import re


class Mapper(object):

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v)>0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            ner_query = record['description'] + " " + record['material']

            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record['price']
            price = price.replace("$", "")
            price = price.replace(" ", "")

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawlDate": record['crawlDate'],
                "statusCode": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "model": record['model'],
                "image": record['image'],
                "description": record['description'],
                "title": record['title'],
                "style": record['style'],
                "color": record['color'],
                "breadcrumb": record['breadcrumb'],
                "rating": record['rating'],
                "ner_query": ner_query,
                "price":{
                    "currencyCode": "USD",
                    "amount": price
                },
                "measurement": record['measurement'],
                "material": record['material']
            }

            # ToDo We consider width, height and depth in the same order for all the records
            if 'measurement' in record:
                dimensions = re.findall(r"[-+]?\d*\.\d+|\d+", record.get("measurement",None))
                dim = ["width", "height","length"]
                dim_dict = dict()
                if dimensions:
                    i=0
                    for item in dimensions:
                        dim_dict[dim[i]] = {'unit': 'INCH', 'value': item}
                        i = i+1
                    transformed_record['itemDimensions'] = dim_dict
            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None