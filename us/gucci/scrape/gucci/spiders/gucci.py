'''
https://mercari.atlassian.net/browse/USDE-201 - Gucci Shoulder Bags
'''

import scrapy
import random
import datetime
import os
from scrapy.exceptions import CloseSpider


class GucciSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from gucci.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags

    brand : str
        brand to be crawled, Supports
        1) Gucci

    Command e.g:
    scrapy crawl gucci -a category='Shoulder Bags' -a brand='Gucci'
    """

    name = 'gucci'

    category = None
    brand = None

    source = 'gucci.com'
    blob_name = 'gucci.txt'

    BASE_URL = 'https://www.gucci.com'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Shoulder Bags' : {
            'Gucci': {
                'url': 'https://www.gucci.com/us/en/ca/women/womens-handbags/womens-shoulder-bags-c-women-handbags-shoulder-bags',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(GucciSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.gucci.com/us/en/pr/women/handbags/crossbody-bags-for-women/gg-marmont-small-shoulder-bag-p-4476320OLFX9085
        @scrape_values image style title price description measurement color breadcrumb
        @meta {"use_proxy":"True","item":{}}
        """

        item = response.meta['item']

        item['category'] = self.category
        item['brand'] = self.brand
        item['crawlDate'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['statusCode'] = str(response.status)

        image = response.xpath('//picture[@class="product-thumb"]//img/@srcset').extract_first()
        if image:
            item['image'] = 'https:' + image
        style = response.xpath('//div[@class="style-number-title"]/span/text()').extract_first()
        if style:
            item['style'] = style.replace('\xa0\u200e', '')
        item['title'] = response.xpath('//div[@class="product-name product-detail-product-name"]/text()').extract_first()
        item['model'] = item['title']
        item['price'] = response.xpath('//span[@class="price"]/text()').extract_first()

        item['description'] = ', '.join(response.xpath(u'//div[@class="product-detail"]//text()[normalize-space()]').extract())
        item['measurement'] = response.xpath('//div[@class="product-detail"]//li[contains(text(), "size")]/text()').extract_first()
        item['color'] = response.xpath(u'normalize-space(//span[@class="color-material-name"]/text())').extract_first()
        item['material'] = item['color']
        item['breadcrumb'] = " | ".join(response.xpath('//nav[@data-module="breadcrumbs"]//a/text()').extract())

        yield item


    def parse(self, response):
        """
        @url https://www.gucci.com/us/en/ca/women/handbags/shoulder-bags-for-women-c-women-handbags-shoulder-bags
        @returns requests 37
        """

        # Number of request mentioned in contracts is 37 beacause 36 items per page and 1 Next Page url
        for product in response.css('article.product-tiles-grid-item'):

            item = dict()

            id = product.css('a.product-tiles-grid-item-link::attr(href)').extract_first()

            if id:
                item['id'] = self.BASE_URL + id
                request = self.createRequest(url=item['id'], callback=self.crawlDetail)
                request.meta['item'] = item
                yield request

        next_link = response.xpath('//link[@rel="next"]/@href').extract_first()
        if next_link:
            yield self.createRequest(url=next_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
