import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            msrp = record.get('max_retail_price', '')
            msrp = msrp.replace("$", "").strip()

            color = record.get('color',"")
            color = color.title()

            model = record.get('item_name', '')
            model = re.sub(re.compile(r"\bWomen's\b|\b:\b", re.IGNORECASE), '', model)
            model = model.title()
            model = re.sub(r'\bBoot\b','Boots',model)
            model = re.sub(re.compile(r'\d+\s*MM',re.IGNORECASE),'',model)
            model =  re.sub(re.compile(color,re.IGNORECASE),"",model).strip()
            model = re.sub(re.compile(r'\bX Hunter\b',re.IGNORECASE),'',model).strip()
            model = re.sub(re.compile(r'\bHunter\b',re.IGNORECASE),'',model)
            model = re.sub(r'\s+', ' ', model).strip()

            color = re.sub(r'/ ','/',color) ###Color transformation is done after removing color from model because if we do transformation in color , color attribute may be missed in title, so we are doing the transformation  in the last after removing color from the title.
            color = re.sub(r' /','/',color)
            color = re.sub(re.compile(r'Exploded Logo',re.IGNORECASE),'',color)

            resistances= ["Waterproof","Water resistant"]
            features = record.get("feature","")
            try:
                water_resistance = [resistant for resistant in resistances if resistant in features][0]
            except IndexError:
                water_resistance = ''

            description = record.get("description","")
            description = ",".join(description)
            try:
                desc = description.split(",size_and_care")[0].strip()
            except IndexError:
                desc =""

            pattern = re.compile(r'\sCrafted from \w+\s*\w*\s*rubber',re.IGNORECASE)
            try:
                material = re.findall(pattern,description)[0]
                material = material.replace("Crafted from","").replace("crafted from",'').strip()
            except IndexError:
                material = ""


            # if material == "":
            #     pattern = re.compile(r'HandCrafted from \w+\s*\w*\s*rubber',re.IGNORECASE)
            #     try:
            #         material = re.findall(pattern,description)[0]
            #         material = material.replace("Handcrafted from","").replace("handcrafted from","").strip()
            #     except IndexError:
            #         material = ""

            model_sku = record.get("product_code","")
            model_sku = model_sku.replace('::', '-')

            exclude_items_substring_list = ['slides','flip','flops', 'loafers', 'loafer', 'flats']
            if any(substring in record.get('item_name', '').lower() for substring in exclude_items_substring_list):
                return None

            include_items_substring_list = ['boot', 'boots', 'rain']
            if not any(substring in record.get('item_name', '').lower() for substring in include_items_substring_list):
                return None

            transformed_record = {
                "id": record.get("id",""),
                "item_id": hex_dig,
                "status_code": record.get("status_code",""),
                "crawl_date": record.get("crawl_date","").split(",")[0],

                "brand": record.get("brand",""),
                "category": record.get("category",""),

                "title": record.get('item_name', ''),
                "image": record.get("image",""),
                "description": desc,

                "model_sku": model_sku,
                "water_resistance": water_resistance,
                "color":color,
                "model": model,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "price": msrp
                }
            }

            return transformed_record
        else:
            return None
