'''
https://mercari.atlassian.net/browse/USDE-1085 - Hunter Women's Boots
'''

import scrapy
import json
import os
from datetime import datetime
import re
from scrapy.exceptions import CloseSpider
import random

class HunterbootsSpider(scrapy.Spider):
    
    """
     spider to crawl items in a provided category and sub-category from Hunterboots.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Women's Boots

     brand : str
      brand to be crawled, Supports
       1) Hunter

     Command e.g:
     scrapy crawl hunterboots  -a category="Women's Boots" -a brand='Hunter' 

    """
    name = 'hunterboots'

    category =  None
    brand =  None

    merge_key = 'id'

    source = 'hunterboots.com'
    blob_name = 'hunterboots.txt'

    launch_url = None
    filters = None
    base_url = 'https://www.hunterboots.com'

    count = 0

    url_dict={
        "Women's Boots": {
            'Hunter': {
                "url": "https://www.hunterboots.com/us/en_us/api/catalog/products/womens-footwear/us/EUPG01/en_US/?page[number]=1&page[size]=80",
                #'url': 'https://www.hunterboots.com/us/en_us/womens-footwear?gclid=EAIaIQobChMI2vTT76jS6AIVENRkCh0nNgXiEAAYASAAEgLBU_D_BwE',
                'filters': []
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(HunterbootsSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category=self.category, brand=self.brand, url=self.launch_url))


    def start_requests(self):
        if self.launch_url:
            meta = {'page_number': 1}
            yield self.create_request(url=self.launch_url, callback=self.parse_page, meta=meta)


    def parse_page(self,response):
        """ 
        @url https://www.hunterboots.com/us/en_us/api/catalog/products/womens-footwear/us/EUPG01/en_US/?page[number]=1&page[size]=80
        @returns requests 1
        @meta {"use_proxy":"True","page_number":1}
        @headers {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36"}
        """

        meta=response.meta
        page_number = meta["page_number"]
        response_json = response.body
        items_page = json.loads(response_json)
        item_ids = []
        for item in items_page["data"]:
            item_ids.append(item["attributes"]["productReference"])
            siblings = item.get('attributes', {}).get('siblings', [])
            for sibling in siblings:
                item_ids.append(sibling.get('productReference', ''))

        total_items = items_page["meta"]["page"]["results"]
        for item in item_ids:
            self.count += 1
            url = "https://www.hunterboots.com/us/en_us/api/product/view/pdp/EUPG01/"+item
            yield self.create_request(url,callback=self.parse_items, meta=meta)

        if total_items > page_number*100:
            print("item count "+str(self.count))
            page_number += 1
            meta.update({"page_number": page_number})
            url = "https://www.hunterboots.com/us/en_us/api/catalog/products/womens-footwear/us/EUPG01/en_US/?page[number]="+str(
                page_number)+"&page[size]=80"
            yield self.create_request(url, callback=self.parse_page, meta=meta)
        else:
            print("item count "+str(self.count))


    def parse_items(self,response):
        """
        @url https://www.hunterboots.com/us/en_us/api/product/view/pdp/EUPG01/WFT1064RNP::NFP 
        @meta {"use_proxy":"True","page_number":1}
        @headers {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36"}
        @scrape_values item_name id minimum_price max_retail_price color product_siblings image description product_code breadcrumb
        """

        response_json =  response.body
        items_json =  json.loads(response_json)
        sizeandcare = items_json.get("productDetails",{}).get("sizeAndCare","")
        cleanr = re.compile('<.*?>')
        sizeandcare = re.sub(cleanr, '', sizeandcare)
        description = []
        description.append(items_json.get("structuredData","").get("description",""))
        description.append("size_and_care")
        description.append(sizeandcare)
        # colors_data = items_json.get('siblings',{})
        # colorslist = [ sub.get("colorName",'') for sub in colors_data ]
        # colors = list(filter(None, colorslist))

        breadcrumbs = items_json.get("breadcrumbs", {}).get("full", "")
        breadcrumbs = [sub.get("name", '') for sub in breadcrumbs]
        breadcrumbs = "|".join(breadcrumbs)

        feature = items_json.get("icons", {}).get("feature", "")
        feature = [sub.get("description", '') for sub in feature]
        feature = list(filter(None, feature))

        product_siblings = items_json.get("siblings", "")
        product_siblings = [sub.get("url", '') for sub in product_siblings]
        product_siblings = list(filter(None, product_siblings))

        colors = items_json.get("colorName", "")

        yield {
            "crawl_date": (datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            "status_code": str(response.status),
            "category": self.category,
            "brand": self.brand,
            "id": items_json.get("url", ""),

            "item_name": items_json.get("name", ""),
            'meta_title': items_json.get('meta', {}).get('title', ''),
            "minimum_price": items_json.get("pricing", {}).get("min", ""),
            "max_retail_price": items_json.get("pricing", {}).get("maxRetail", ""),
            "color": colors,
            "product_siblings": product_siblings,
            "image": items_json.get("structuredData", "").get("image", ""),
            "description": description,
            "product_code": items_json.get("reference", ""),
            "feature": feature,
            "breadcrumb": breadcrumbs,
        }


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
