'''
https://mercari.atlassian.net/browse/USCC-367 - American Eagle Women's Shorts
https://mercari.atlassian.net/browse/USCC-260 - American Eagle Skinny Jeans for Women
'''

import datetime
import json
import random
import os
import re
import requests

import scrapy
from scrapy import Request
from scrapy.exceptions import CloseSpider

from common.selenium_middleware.http import SeleniumRequest


class AeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from ae.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Shorts
        2) Skinny Jeans for Women

    brand : str
        category to be crawled, Supports
        1) American Eagle

    Command e.g:
    scrapy crawl ae -a category="Women's Shorts" -a brand='American Eagle'
    scrapy crawl ae -a category="Skinny Jeans for Women" -a brand='American Eagle'

    """

    name = 'ae'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    source = 'ae.com'
    blob_name = 'ae.txt'

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    access_token = None
    driver = None
    filters = None
    number_of_products = None
    base_url = 'https://www.ae.com/us/en'

    url_dict = {
        "Women's Shorts": {
            'American Eagle': {
                'url': 'https://www.ae.com/us/en/c/women/bottoms/shorts/cat380159?pagetype=plp',
                'filters': ['Stretch','Rise','Wash','Destruction','Color'],
            }
        },
        "Skinny Jeans for Women": {
            "American Eagle": {
                'url': 'https://www.ae.com/us/en/c/women/jeans/skinny-jeans/cat1990002?pagetype=plp',
                'filters': ['Stretch','Rise','Wash','Destruction','Color'],
            }
        }
    }

    source_url = '{launch_url}?No=0&Nrpp={count}&Ns=&isFiltered=false&results=&showHidden='

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(AeSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
            self.filters = self.url_dict.get(category, {}).get(brand, {}).get("filters", [])
        if not self.launch_url:
            self.logger.error(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for  category " +
                         category + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.contracts_running = True
        self.filters = ['Stretch','Rise','Wash','Destruction','Color']
        self.source_url = 'https://www.ae.com/ugp-api/catalog/v1/category/cat380159/?No=0&Nrpp={}&Ns=&isFiltered=false&results=&showHidden='
        self.access_token = ''


    def start_requests(self):
        if self.launch_url:
            yield self.createDynamicRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self, response):
        """
        @url https://www.ae.com/us/en/c/women/bottoms/shorts/cat380159?pagetype=plp
        @returns requests 1
        @meta {"use_selenium":"True"}
        """
        if not self.contracts_running:
            driver = response.meta.get('driver','')
            token_json=json.loads(driver.execute_script("return localStorage.getItem('aeotoken')"))
            self.access_token=token_json.get('access_token','')

        item_count = response.xpath(
            '//span[@class="filter-item-count"]/text()').get().strip('items').strip()
        headers = {
            "aelang": "en_US",
            "aesite": "AEO_US",
            "content-type": "application/json",
            "referer": "https//www.ae.com/us/en/c/women/bottoms/shorts/cat380159?pagetype=plp",
            "requestedurl": "plp",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
            "x-access-token": self.access_token,
            "x-newrelic-id": "VwMPUFJVGwIDUlNSAQIAUg==",
            "x-requested-with": "XMLHttpRequest"
        }
        r=requests.get(self.source_url.format(launch_url=self.launch_url.split('?')[0], count=item_count),headers=headers)
        js=r.json()
        if r.status_code==200:
            site_content=js.get('data',{}).get('contents',{})[0].get('sideContent',{})
            print(site_content)
            if self.filters:
                navigation=site_content[-1].get('navigation')
                print(navigation)
                for nav in navigation:
                    ref=nav.get('refinements',{})
                    name=nav.get('name','')

                    if name in self.filters:
                        print(name)
                        for r in ref:
                            filter_url=r.get('navigationState','')
                            category_id=filter_url.split('?Nrpp')[0].split("/")[-1]
                            filter_value=r.get('label','')
                            count=str(r.get('count',''))
                            url="https://www.ae.com/ugp-api/catalog/v1/category/{}/results?No=0&Nrpp={}&Ns=&isFiltered=true&results=results&showHidden="
                            r=requests.get(url.format(category_id,count),headers=headers)
                            js=r.json()
                            meta={}
                            meta=meta.fromkeys([name],filter_value)
                            meta.update({'filter_name':name})
                            response.meta.update(meta)
                            product_data=js.get('data',{}).get('contents',{})[0].get('productContent',{})[0].get('records',[])
                            all_links=[]
                            for product in product_data:
                                if product.get("detailsAction",{}).get("recordState",'') !='':
                                    all_links.append(product.get("detailsAction",{}).get("recordState",''))
                            all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
                            for link in all_links:
                                yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)

            product_data=js.get('data',{}).get('contents',{})[0].get('productContent',{})[0].get('records',[])
            all_links=[]
            for product in product_data:
                if product.get("detailsAction",{}).get("recordState",'') !='':
                    all_links.append(product.get("detailsAction",{}).get("recordState",''))
                all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
                for link in all_links:
                    yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)


    def parse_product(self, response):
        """
        @url https://www.ae.com/us/en/p/women/curvy-shorts/curvy-high-waisted-short-shorts/ae-curvy-high-waisted-short-short/0545_6017_486?menu=cat4840004
        @meta {"use_proxy":"True"}
        @scrape_values image product_color breadcrumb title price description
        """

        item = {}
        item["crawl_date"] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"] = str(response.status)
        item['id'] = response.url
        item['image'] = response.xpath('//div[@class="carousel-wrapper"]//div//picture//img/@src').get(default='')
        item['brand'] = self.brand
        item['category'] = self.category
        item['product_color']=response.xpath('//span[@class="product-color"]/text()').get(default='')
        item['breadcrumb'] = '|'.join(response.xpath('//ol[@class="ancestor-breadcrumbs"]//li//a/span/text()').getall())
        item['title'] = response.xpath('//h1[@class="product-name"]//text()').get(default='').strip()
        item['price'] = response.xpath('//div[@class="product-list-price product-list-price-on-sale ember-view"]/text()').get(default='')
        des=response.xpath('//div[contains(@class,"product-equity clearfix")]//text()').getall()
        description=[]
        for d in des:
            if re.findall(r"^[\s\n]+$",d)==[]:
                description.append(d)
        item['description']=description

        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)

        yield item


    def createRequest(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key, value in headers.items():
                request.headers[key] = value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value
        return request


    def createDynamicRequest(self, url, callback, errback=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request
