import re
import hashlib
from colour import Color
class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            #ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title')

            material=[]
            pattern=re.compile(r"Materials & Care",re.IGNORECASE)
            matches=[re.findall(pattern,d) for d in description]
            position=[matches.index(d) for d in matches if d!=[]]
            if position!=[] and position[0]+1<len(description):
                materials=description[position[0]+1].split("%")
                for mat in materials:
                    pattern=re.compile(r"[\sa-zA-Z\W]+",re.IGNORECASE)
                    if re.findall(pattern,mat)!=[]:
                        material.append(re.findall(pattern,mat)[0].replace(",","").strip())
                
                for ma in material:
                    if ma=="t" or ma=="T":
                        del material[material.index(ma)]
                
                material=", ".join(material)
                material=re.sub(re.compile(r"\|\s*Machine\s*wash",re.IGNORECASE),'',material)
                material=', '.join([m.strip() for m in material.split(", ")])
            
            



            pattern=re.compile(r"\d+\.*\d*\s*\"\s*leg\s*opening",re.IGNORECASE)
            leg_opening=''
            for d in description:
                if re.findall(pattern,d)!=[]:
                    leg_opening=re.findall(pattern,d)[0].strip("leg opening")
            
            pattern=re.compile(r"\d+\.*\d*\s*\"\s*unrolled\s*inseam",re.IGNORECASE)
            inseam=''
            for d in description:
                if re.findall(pattern,d)!=[]:
                    inseam=re.findall(pattern,d)[0].strip("unrolled inseam")

            stretch=record.get('Stretch','')
            if stretch=='':
                pattern=re.compile(r"ne\s*\(\s*x\s*\)\s*t\s*level|super\s*stretch",re.IGNORECASE)
                for d in description:       
                    if re.findall(pattern,d)!=[]:
                        stretch=re.findall(pattern,d)[0]

            wash=''
            if record.get('Wash','')=='':
                was_des=description[:description.index('Materials & Care')]
                if was_des!=[]:
                    pattern=re.compile(r"[\w\d\s\-\_\+\'\"]+\s*wash",re.IGNORECASE)
                    for d in was_des:
                        if re.findall(pattern,d)!=[]:
                            wash=re.findall(pattern,d)[0]
                            break
            else:
                wash=record.get('Wash','')
                if "wash" not in wash and "Wash" not in wash:
                    wash+=" wash"
                    
            color=record.get('Color','')
            if color=='':
                colors=re.findall(r"\w+",record.get('product_color',''))
                colour=[]
                for word in colors:
                    try:
                        Color(word)
                        colour.append(word)
                    except:
                        pass
                color=', '.join(colour)
            
            if color=='':
                for d in description:
                    pattenr=re.compile(r"tie\s*dye",re.IGNORECASE)
                    if re.findall(pattern,d)!=[]:
                        color='Tie dye'
            
            


            if "Need Help Finding Your Size?" in description:
                description=', '.join(description[:description.index('Need Help Finding Your Size?')])



            if id=="https://www.ae.com/us/en)":
                return None

            transformed_record = {
            
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date',''),
                "status_code": record.get('status_code',''),
                "category": record.get('category',''),
                "breadcrumb":record.get('breadcrumb',''),
                "description": description,
                "item_title": record.get('title',''),
                "model":record.get("title",''),
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query":description,
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "color":color.title(),
                "design_features":stretch,
                "rise":record.get('Rise',''),
                # "wash":wash,
                "accents":record.get('Destruction',''),
                "material":material,
                "opening":leg_opening,
                "inseam":inseam

                }

            return transformed_record
        else:
            return None