import hashlib
import re


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            model = re.sub(re.compile(r'®|™|\(|\)| — BOB Gear|\bBob\b',re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            if model == 'Gear Quick Release for Bike Trailers':
                model = 'Quick Release for Bike Trailers'
            price_data = record.get('price', '').split(' | ')
            price = [price for price in price_data if 'MSRP' in price][0]
            price = re.findall(r'\$(\d+\.*\d*)', price)[0]

            product_data = record.get('description', '')

            dimensions_data = re.findall(
                r'(EXTERIOR PRODUCT.*), Handlebar Height', ', '.join(product_data))
            if dimensions_data:
                dimensions_data = dimensions_data[0]
                dimension_data = dimensions_data.split(', ')[1:]
                for dimension in dimension_data:
                    inches = re.findall(r'\d+\.*\d+\s*in', dimension)[0]
                    cm = re.findall(r'\d+\.*\d+\s*cm', dimension)[0]
                    if 'Length' in dimension:
                        length = inches + ' (' + cm + ')'
                    elif 'Width' in dimension:
                        width = inches + ' (' + cm + ')'
                    elif 'Height' in dimension:
                        height = inches + ' (' + cm + ')'
            else:
                length = ''
                width = ''
                height = ''

            try:
                stroller_weight = re.findall(
                    r'Stroller Weight:\s*(\d+\.*\d+\s*lbs)', ', '.join(product_data))[0]
            except IndexError:
                stroller_weight = ''

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image': record.get('image', ''),
                'description': '. '.join(record.get('description', '')),

                'model': model,
                'color': record.get('color', ''),
                'product_type': record.get('product_type', ''),
                'width': width,
                'height': height,
                'length': length,
                'weight': stroller_weight,

                'msrp': {
                    'currency_code': 'USD',
                    'amount': price
                }
            }
            return transformed_record
        else:
            return None
