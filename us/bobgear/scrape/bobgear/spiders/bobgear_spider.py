'''
https://mercari.atlassian.net/browse/USDE-1683 - BOB Strollers Stroller Joggers
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import itertools
import json
import unicodedata

class BobgearSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bobgear.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stroller Joggers

    brand : str
        brand to be crawled, Supports
        1) BOB Strollers

    Command e.g:
    scrapy crawl bobgear -a category='Stroller Joggers' -a brand='BOB Strollers'
    """

    name = 'bobgear'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    source = 'bobgear.com'
    blob_name = 'bobgear.txt'

    base_url = 'https://www.bobgear.com'

    url_dict = {
        'Stroller Joggers': {
            'BOB Strollers': {
                'url': 'https://www.bobgear.com',
                'product_types': [
                    'Single Strollers', 'Duallie Strollers', 'Accessories', 'Travel Systems'
                ]
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):        
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        super(BobgearSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.product_types = self.url_dict.get(category, {}).get(brand, {}).get('product_types', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def contracts_mock_function(self):
        self.launch_url = 'https://www.bobgear.com'
        self.product_types = [
                    'Single Strollers', 'Duallie Strollers', 'Accessories', 'Travel Systems'
                ]
        self.category = 'Stroller Joggers'
        self.brand = 'BOB Strollers'

    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.get_filter_urls)

    def get_filter_urls(self, response):
        """
        @url  https://www.bobgear.com
        @returns requests 4 
        @meta {"use_proxy": "True"}
        """
        #For contracts No. of requests specified is 4 because length of  product type is 4 so number of request to be send must be equal to total number of product types. If product type gets increased number of request also should be increased and vice-versa.
        for product_name in self.product_types:
            product_url = response.xpath("//div[@class='Header-nav-inner']/a[contains(text(),'"+product_name+"')]/@href").get(default='')
            if product_name == 'Duallie Strollers':
               product_name = 'Double Stroller'
            meta = {'product_type': product_name}
            yield self.create_request(url= self.base_url+product_url, callback=self.parse_listing_page, meta=meta)


    def parse_listing_page(self, response):
        """
        @url https://www.bobgear.com/travel-systems
        @returns requests 1
        @meta {"use_proxy": "True"}
        """
        meta = response.meta
        products_urls = []
        products_urls.append(response.xpath("//div[@class='sqs-block-content']/div/a/@href").getall())
        products_urls.append(response.xpath("//div[@data-test='image-block-inline-outer-wrapper']/figure/a/@href").getall())
        products_urls.append(response.xpath("//div[@class='image-button sqs-dynamic-text']/div/a/@href").getall())
        products_urls = list(itertools.chain(*products_urls))
        for product_url in products_urls:
            if product_url[0] != '/' and 'https' not in product_url:
                product_url = '/'+product_url

            if self.base_url not in product_url:
                product_url = self.base_url+product_url

            yield self.create_request(url=product_url, callback=self.crawldetails, meta=meta)


    def crawldetails(self, response):
        """
        @url https://www.bobgear.com/revolution-flex-30-duallie
        @meta {"use_proxy": "True", "product_type": "Double Stroller"}
        @scrape_values title product_type image description color price id
        """
        product_details = response.xpath("//div[@class='row sqs-row']/div/div/div//h3[@style='text-align:center;white-space:pre-wrap;']/text()").getall()
        product_data = []
        product_data.append(', '.join(response.xpath("//div[@class='sqs-block-content']/p/strong[contains(text(),'MSRP')]/../../p[1]/text()").getall()))
        for detail in product_details:
            produt_desc = ", ".join(response.xpath("//div[@class='sqs-block-content']/h3[contains(text(),'"+detail+"')]/../ul/li/p/text()").getall() or response.xpath("//div[@class='sqs-block-content']/h3[contains(text(),'"+detail+"')]/../p/text()").getall())
            if produt_desc:
                product_data.append(detail+":")
                product_data.append(produt_desc)
        product_data.append(', '.join(response.xpath("//div[@class='sqs-block-content']/h3[contains(text(),'Product Details')]/../ul/li/p/text()").getall()))
        color_data = response.xpath("//div[@class='slide']/div/a[@role='presentation']/../div/text()").getall()
        item = {
            'title': ' '.join(response.xpath("//div[@class='sqs-block-content']/h1[@data-preserve-html-node='true']/text()").getall()) or  response.xpath("//div[@class='sqs-block-content']/h1[@style='text-align:center;white-space:pre-wrap;']/text()").get(default='') or response.xpath("//meta[@property='og:title']/@content").get(),
            'category': self.category,
            'brand': self.brand,
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'product_type': response.meta.get('product_type',''),
            'image':  response.xpath("//div[@class='sqs-gallery']/div/img/@data-src").get(),
            'description': product_data,
        }

        if 'product details' in item['title'].lower():
            title = response.xpath("//div[@data-preserve-html-node='true']/h1/text()").get(default='')
            if title:
                item.update({'title':title})
            else:
                item.update({'title':response.xpath("//meta[@property='og:title']/@content").get(default='')})

        price_data = response.xpath("//div[@class='sqs-block-content']/p/strong[contains(text(),'MSRP')]/text()").getall()
        if color_data:
            for color in  color_data:
                items = {}
                color = ''.join((c for c in unicodedata.normalize('NFD', color) if unicodedata.category(c) != 'Mn'))
                new_url=response.url+"?color={}".format(color)
                new_url = new_url.replace(' ','%20')
                items.update(item)
                items.update({'id':new_url})
                items.update({'color':color})
                if len(price_data)>1:
                    price = [ price for price in price_data if color in price]
                    if price:
                        price = price[0]
                    else:
                        price =[ price for price in price_data if 'MSRP' in price]
                        price = price_data[0]
                    items.update({'price': price})
                else:
                    items.update({'price': price_data[0]})
                yield items
        else:
            item.update({'price': price_data[0]})
            item.update({'color': ''})
            item.update({'id': response.url})
            yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
