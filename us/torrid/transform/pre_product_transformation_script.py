import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get("title","")

            price =  record.get("price","").replace("$","").strip()

            description = record.get("description","").encode('ascii',errors='ignore').decode('utf-8').strip()
            detailed_description = record.get("detailed_description","")
            if description == "":
                if re.findall(r'(.*)\.\s',detailed_description):
                    description = re.findall(r'(.*)\.\s',detailed_description)[0]
                elif re.findall(r'(.*)\!\s',detailed_description):
                    description = re.findall(r'(.*)\!\s',detailed_description)[0]
                else:
                    description = ''

            try:
                material_data = detailed_description.split("CONTENT + CARE")[1].strip()
                if ":" in material_data:
                    material = material_data.split("  ")[0]
                else:
                    material = material_data.split(" ")[0]
            except IndexError:
                material = ""
            
            if material == '':
                try:
                    material_data = detailed_description.split("CONTENT +CARE")[1].strip()
                    material = material_data.split(" ")[0]
                except IndexError:
                    material = ""

            materials = [x.split(':')[-1].strip() for x in material.split(';')]
            materials = [x for x in materials if x and x!='Self &amp']
            material = '/'.join(list(dict.fromkeys(materials)))
            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()

            try:
                neckline =  re.findall(r'\s{1}\w+-*\s*neck',detailed_description)[0]
                neckline = neckline.strip().title()
            except IndexError:
                neckline = ''
            neckline = re.sub(r'V-\s*Neck', 'V-Neck', neckline, re.IGNORECASE)

            if "sleeveless" in detailed_description.lower():
                sleeve_length = 'Sleeveless'
            else:
                try:
                    sleeve_length = re.findall(r'\d/\d\s\w*\s*-*\w*\s*sleeves', detailed_description)[0]
                    # if sleeve_length == 'and sleeves' and re.findall(r'smocked', detailed_description)[0]:
                    #     sleeve_length = 'Smocked Sleeves'
                except IndexError:
                    sleeve_length = ''

            if not sleeve_length:
                try:
                    sleeve_length = re.findall(r'\w+\s+sleeves', detailed_description)[0]
                except IndexError:
                    sleeve_length = ''

            if sleeve_length == 'and sleeves' and re.findall(r'smocked', detailed_description)[0]:
                sleeve_length = 'Smocked Sleeves'

            if not sleeve_length and re.findall(re.compile(r'strapless', re.IGNORECASE), title):
                sleeve_length = 'Sleeveless'

            sleeve_length = sleeve_length.strip().title()

            color = record.get('Color', '') or record.get('color', '')
            color = re.sub(re.compile(r'Floral - Black', re.IGNORECASE), 'Black/Floral', color)
            color = re.sub(re.compile(r'Cloud Dancer', re.IGNORECASE), 'White', color)
            color = re.sub(re.compile(r'Multi', re.IGNORECASE), 'Multicolor', color)
            if not color:
                color_meta = ['Leopard', 'Mauve Pink']
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b{}\b'.format(x), color_meta))), re.IGNORECASE), title)
                if match:
                    color = '/'.join(sorted(match))

            color = re.sub(r'\s+', ' ', color).strip()
            color = color.title()


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "status_code": record.get('status_code', ''),
                "crawl_date": record.get('crawl_date', '').split(',')[0],

                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "title": title,
                "image": record.get('image', ''),

                "model": title,
                "description": description,
                "neckline": neckline,
                "material": material,
                # "color": record.get("color",""),
                "product_line": record.get('Collection', ''),
                "color": color,
                "sleeve_length": sleeve_length,
                "msrp": {
                    "currency_code": 'USD',
                    "amount": price
                }
            }
            return transformed_record
