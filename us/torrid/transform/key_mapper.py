class Mapper:
    def map(self, record):
        color = ''
        material = ''
        sleeve_length = ''
        neckline = ''

        entities = record['entities']
        for entity in entities:
            if "color" == entity['name']:
                color = entity["attribute"][0]["id"]
            elif "material" == entity['name']:
                material = entity["attribute"][0]["id"]
            elif "sleeve_length" == entity['name']:
                if entity["attribute"]:
                    sleeve_length = entity["attribute"][0]["id"]
            elif "neckline" == entity['name']:
                if entity["attribute"]:
                    neckline = entity["attribute"][0]["id"]

        key_field = color + material + sleeve_length + neckline
        return key_field
