'''
https://mercari.atlassian.net/browse/USDE-1210 - Torrid Women's Blouses
'''

import scrapy
import os
from datetime import datetime
from scrapy.exceptions import CloseSpider
import itertools


class TorridSpider(scrapy.Spider):

    """
    spider to crawl items in a provided category and sub-category from torrid.com

    Attributes

    category : str
    category to be crawled, Supports
        1) Women's Blouses


    brand : str
    brand to be crawled, Supports
        1) Torrid

    Command e.g:
    scrapy crawl torrid  -a category="Women's Blouses" -a brand='Torrid' 

    """
    name = "torrid"

    source = 'torrid.com'
    blob_name = 'torrid.txt'

    category = None
    brand = None

    merge_key = 'id'

    filters = None
    launch_url = None

    spider_project_name = "torrid"
    gcs_cache_file_name = 'torrid.tar.gz'

    base_url='https://www.torrid.com'

    url_dict = {
        "Women's Blouses": {
            "Torrid": {
                "url": "https://www.torrid.com/clothing/tops/shirts-blouses/",
                "filter": ["Color","Collection"]
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TorridSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.round_robin = itertools.cycle(self.proxy_pool)
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand,{}).get('url','')
            self.filters=self.url_dict.get(category,{}).get(brand,{}).get('filter','')
        print(self.launch_url)
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ["Color","Collection"]
        self.round_robin = itertools.cycle(self.proxy_pool)


    def start_requests(self):
        meta = {"use_filter":True}
        request = self.createRequest(self.launch_url,callback = self.parse,meta=meta)
        yield request


    def parse(self,response):
        """
        @url https://www.torrid.com/clothing/tops/shirts-blouses/
        @returns requests 61
        @meta {"use_proxy":"True","use_filter":"True"}
        """
        ###Number of requests mentioned in contract is 61 because 60 products per page and 1 next page url

        meta = response.meta
        product_urls = response.xpath("//div[@class='product-name']/h2/a/@href").getall()
        for product_url in product_urls:
            request = self.createRequest(url = product_url,callback = self.crawldetail,meta=meta)
            yield request
        next_page_url = response.xpath("//a[@class='page-next']/@href").get()  
        if next_page_url:
            request = self.createRequest(url = next_page_url,callback = self.parse,meta=meta)
            yield request
        else:
            if meta.get("use_filter",""):
                request = self.createRequest(response.url,callback = self.filter_page,meta =meta)
                yield request
            else:
                print("filter requst is sent")


    def crawldetail(self,response):
        """
        @url https://www.torrid.com/product/harper---black-georgette-pullover-blouse/10381277.html?cgid=Clothing_Tops_ShirtsBlouses#start=2
        @scrape_values title description price image color detailed_description
        @meta {"use_proxy":"True"}
        """

        meta = response.meta
        title = response.xpath("//h1[@class='product-name font-weight-normal']/text()").get(default="")
        description = response.xpath("//div[@itemprop='description']/text()").get(default="").strip() or \
            response.xpath("//div[@itemprop='description']/p/text()").get(default="") or \
            response.xpath("//div[@itemprop='description']/div/text()").get(default="")    
        price = response.xpath("//span[@class='price-standard font-weight-semibold']/text()").get(default="")
        image = response.xpath("//img[@itemprop='image']//@src").get(default="")
        color = response.xpath("//span[@class='selectedValueContainer']/text()").get(default='')
        detailed_description = response.xpath("//meta[@name='description']/@content").get(default='')
        item = {
            'title': title,
            "description": description,
            "price": price,
            "category": self.category,
            "brand": self.brand,
            "image": image,
            "id": response.url,
            "color": color,
            "detailed_description": detailed_description,
            "status_code": response.status,
            "crawl_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        }
        filter = meta.get("filter","")
        meta_data={}
        if filter:
            meta_data = meta_data.fromkeys([filter],meta[filter])
            item.update(meta_data)
        yield item


    def filter_page(self,response):
        """
        @url https://www.torrid.com/clothing/tops/shirts-blouses/
        @returns requests 15 30
        @meta {"use_proxy":"True"}
        """

        for filter in self.filters:
            if filter == "Color":
                filter_page_urls = response.xpath("//a[@class='refinment-color']/@href").getall()
                filter_names = response.xpath("//a[@class='refinment-color']/@title").getall()   
            else:
                filter_page_urls = response.xpath('//div[@id="refineGroup-'+filter+'"]/div/ul/li/a/@href').getall() 
                filter_names = response.xpath('//div[@id="refineGroup-'+filter+'"]/div/ul/li/a/@title').getall()

            for filter_page_url,filter_name in zip(filter_page_urls,filter_names):
                filter_name = filter_name.replace('Refine by:',"")
                meta = {}
                meta["filter"] = filter
                meta.update({meta["filter"]:filter_name.strip()})
                request = self.createRequest(url = filter_page_url,callback = self.parse,meta=meta)
                yield request


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] =  next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request
