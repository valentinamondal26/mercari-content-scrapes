'''
https://mercari.atlassian.net/browse/USCC-223 - PANDORA Bracelets
https://mercari.atlassian.net/browse/USCC-347 - PANDORA Women's Rings
https://mercari.atlassian.net/browse/USCC-386 - PANDORA Jewelry Charms
'''

# -*- coding: utf-8 -*-
import scrapy
from collections import OrderedDict
import datetime
import os
import re
import unicodedata
import random
from scrapy.exceptions import CloseSpider

class PandoraSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and sub-category from pandora.com for the specified brand

    Attributes

    category : str
        category to be crawled, Supports
        1) Women's Rings
        2) Jewelry Charms
        3) Bracelets

    brand : str
        category to be crawled, Supports
        1) PANDORA

    Command e.g:
    scrapy crawl pandora -a category='Bracelets' -a brand="PANDORA"
    scrapy crawl pandora -a category="Women's Rings" -a brand="PANDORA"
    scrapy crawl pandora -a category='Jewelry Charms' -a brand="PANDORA"
    """


    name = 'pandora'

    source = "pandora.com"
    blob_name = "pandora.txt"

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None

    base_url = "https://us.pandora.net"

    url_dict = {
        'Bracelets': {
            'PANDORA': {
                'url': 'https://us.pandora.net/en/jewelry/bracelets/',
            }
        },
        "Women's Rings": {
            'PANDORA': {
                'url':'https://us.pandora.net/en/jewelry/rings/',
                'filters': ['Metal Color', 'Color', 'Collection'],
            }
        },
        'Jewelry Charms': {
            'PANDORA': {
                'url':'https://us.pandora.net/en/jewelry/charms/charms/',
            }
        },
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    listing_page_url_params = '?sz=30&start={}&format=page-element'

    extra_item_infos = {}


    def __init__(self, category=None,brand=None, *args, **kwargs):
        super(PandoraSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category :
            self.logger.error("category and Brand should not be None")
            raise CloseSpider('category and Brand not found')
        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{}'.format(category))
            raise(CloseSpider('Spider is not supported for  category:{}'.format(category)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def contracts_mock_function(self):
        self.launch_url = 'https://us.pandora.net/en/jewelry/rings/?sz=30&start={}&format=page-element'


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse, dont_filter=True)
            yield self.createRequest(url=self.launch_url, callback=self.parse_filters, dont_filter=True)


    def parse_listing_page(self, response):
        extra_item_info = response.meta.get('extra_item_info', {})

        for id in response.xpath('//div[contains(@class, "product-tile")]//a/@href').getall():
            if id:
                id = self.base_url + id
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                # self.extra_item_infos.get(id).update(extra_item_info)
                self.update_extra_item_infos(id, extra_item_info)

                yield self.createRequest(url=id, callback=self.parse_products)


    def parse(self, response):
        """
        @url https://us.pandora.net/en/jewelry/rings/
        @returns requests 1
        """

        extra_item_info = response.meta.get('extra_item_info', {})

        product_count = response.xpath('//span[@class="products-count js-products-count"]/text()').get(default='')
        product_count = int(re.findall(r"\d+",product_count)[0])

        start_count = 0
        while start_count < product_count :
            url = response.url + self.listing_page_url_params.format(start_count)
            request = self.createRequest(url=url, callback=self.parse_listing_page)
            request.meta['extra_item_info'] = extra_item_info
            yield request
            start_count += 30


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse_filters(self, response):
        for filter_item in response.xpath('//div[@class="SearchRefinements__options js-refinement-options"]/div'):
            filter_name = filter_item.xpath(u'normalize-space(.//h2/text())').extract_first()
            if filter_name in self.filters:
                for filter_value_item in filter_item.xpath('.//li'):
                    filter_value = filter_value_item.xpath(u'normalize-space(.//a/@data-filtervalue)').extract_first() or \
                        filter_value_item.xpath(u'normalize-space(.//a/@title)').extract_first()
                    url = filter_value_item.xpath('.//a/@href').extract_first()
                    request = self.createRequest(url=url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                        filter_name: filter_value
                    }
                    yield request


    def parse_products(self,response):
        """
        @url https://us.pandora.net/en/jewelry/rings/beaded-seashell-band-ring/198943C00.html?cgid=rings#start=1&cgid=rings&productscount=132
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb title image size price metal_color description

        """
        item={}
        item["crawlDate"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["statusCode"]=str(response.status)
        item['id']=response.url
        breadcrumb=response.xpath('//a[@class="breadcrumb-element"]//text()').getall()
        breadcrumbs=''
        for crumb in breadcrumb:
            crumb=unicodedata.normalize("NFKD",crumb)
            if re.match(r"[a-zA-Z]+",crumb):
                breadcrumbs+=crumb+"|"
        item['breadcrumb']=breadcrumbs[:-1]
        item['brand']=self.brand
        item['title']=response.css('h1.product-name::text').get(default='').strip()
        item['image']=response.xpath('//*[@id="pdp-simple-carousel"]//img/@data-src').get(default='')
        item['size']=','.join(response.css('div.sizeLinksContainer a::text').getall())
        if item['size']=='':
            size_list=response.xpath('//select[@class="sizeSelect custom-select variations-select"]/option')
            sizes=[]
            for option in size_list:
                if option.css('::text').get() !="Select a Size":
                    sizes.append(option.css('::text').get(default='').strip('US').strip())
            item['size']=','.join(sizes)
        item['price']=response.xpath('//div[@class="pdp-title-product"]//div[@class="product-price"]/span/text()').get(default='').strip('$').replace('\n','')
        if item['price']=='':
            item['price']=response.xpath('//div[@class="pdp-title-product"]//div[@class="product-price"]//span//text()').get(default='').strip('$').replace('\n','')
        item['category']=self.category
        item['metal_color']=' '.join(response.xpath('//span[span[@class="metalType"]]/text()').getall()).replace("\n","") \
                            or ', '.join(response.xpath('//div/@data-metalgroup').getall())
        product_details=response.xpath('//div[@class="productDetails"]//div[@class="detailsColumn center"]//ul')
        details=OrderedDict()
        details_list=product_details.css('li')
        keys=[]
        values=[]
        for li in details_list:
            keys.append(' '.join(li.css('span.label::text').getall()).strip().replace("\n",""))
            values.append(' '.join(li.css('span.value::text').getall()).strip().replace("\n",""))
        details=details.fromkeys(keys,'')
        ctr=0
        for key ,value in details.items():
            details[key]=values[ctr]
            ctr+=1
        if "Item#" in details:
            details["item"]=details['Item#']
            del details["Item#"]
        item.update(details)
        item['description']=' '.join(response.css('div.desktop-detail-text::text').getall())

        specs = {}
        for attribute in response.xpath('//ul[@class="section-content"]/li[@class="attribute"]'):
            key = attribute.xpath(u'normalize-space(./span[@class="label"]/text())').extract_first()
            value = attribute.xpath('.//span[@class="value"]').xpath(u'normalize-space(./text())').extract()
            specs[key] = value
        item['specs'] = specs

        yield item


    def createRequest(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
