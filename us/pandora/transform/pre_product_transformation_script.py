import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            for key in record:
                if key == 'category':
                    transformed_record['category'] = 'Rings'
                else:
                    transformed_record[key] = record[key]

            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price=record.get('price','')
            price = price.replace("$", "")
            price=''.join(price.split())
            price=price.replace(",","")
            title=record.get('title','')
            


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": 'Rings',
                "brand": record.get('brand',''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "metal": record.get('Metal',''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "collection": record.get('Collection',''),
                "metal_color":record.get('metal_color',''),
                "stone": record.get('Stone',''),
                "stone_color": record.get('Color','')

            }

            return transformed_record
        else:
            return None


