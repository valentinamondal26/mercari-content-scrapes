import hashlib
import html
import re

from colour import Color


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            dimensions = record.get('specs', {}).get('Dimensions:', [])
            dim = {}
            for d in dimensions:
                try:
                    dim[d.split(':')[0]] = d.split(':')[1].strip()
                except Exception:
                    pass
            depth = dim.get('Depth', '')
            width = dim.get('Width', '')
            height = dim.get('Height', '')

            model_sku = ', '.join(record.get('specs', {}).get('Item', []))
            product_line = ', '.join(record.get('Collection', '')) \
                           or ', '.join(
                    record.get('specs', {}).get('Collection', []))
            # color and metal_color extraction
            metal_color = ', '.join(record.get('Metal Color', []))
            metal_color = ", ".join(sorted(set(metal_color.split(", ")),
                                    key=metal_color.split(", ").index))
            color = ', '.join(record.get('Color', [])) \
                    or ', '.join(record.get('specs', {}).get('Color', []))

            metal_type = ', '.join(record.get('specs', {}).get('Metal', []))
            gemstone = ', '.join(record.get('specs', {}).get('Stone', []))

            color = '/'.join(list(dict.fromkeys(list(
                    filter(lambda x: x and x.lower() != 'clear',
                           [metal_color, color])))))
            if not metal_color:
                metal_color = record.get("metal_color", "")
                metal_color = ", ".join(sorted(set(metal_color.split(", ")),
                                        key=metal_color.split(", ").index))
                color = "/".join([m.strip() for m in
                                  metal_color.split(", ") + color.split(
                                          "/")]).strip("/")
                color = "/".join(sorted(set(color.split("/")),
                                 key=color.split("/").index))
                if len(re.findall(re.compile(r"\brose gold\b|\bgold\b",
                                  re.IGNORECASE), color)) == 2:
                    color = re.sub(re.compile("\/gold$|\/gold\/|^gold\/",
                                   re.IGNORECASE), "", color)

            # model transformations
            model = record.get('title', '')
            model = re.sub(r'\s-\s|\bFINAL SALE\b|\bof PANDORA\b|™|®'
                           r'|PANDORA Rose|PANDORA Shine|,',
                           '', model, flags=re.IGNORECASE)
            model = re.sub('(.*)(rings|ring)(.*)', r'\1 \3 \2', model,
                           flags=re.IGNORECASE)
            if not re.findall(r'\b(rings|ring)\b', model, flags=re.IGNORECASE):
                model += ' Ring'
            pattern_gemstone = r'|'.join(
                    list(filter(None, re.split(r'[,\s]', gemstone))))
            if pattern_gemstone:
                gemstone_info = re.findall(pattern_gemstone, model,
                                           flags=re.IGNORECASE)
                if gemstone_info:
                    words_to_remove = []
                    for word in model.split():
                        try:
                            Color(word)
                            words_to_remove.append(word)
                        except Exception:
                            pass
                    if words_to_remove:
                        model = re.sub(r'({})(\s*(&|and)*\s*{})'.format(
                                gemstone_info[0], words_to_remove[0]), r'\1',
                                model, flags=re.IGNORECASE)

            model = re.sub(r'^\s*of\b', '', model, flags=re.IGNORECASE)
            model = re.sub('&nbsp;', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),

                    "category": record.get('category', ''),
                    "brand": record.get('brand', ''),

                    "image": record.get('image', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "description": html.unescape(
                            record.get('description', '')).strip(),
                    "title": record.get('title', ''),
                    "ner_query": html.unescape(ner_query).strip(),

                    "model": model,
                    "price": {
                            "currencyCode": "USD",
                            "amount": price
                    },
                    "product_line": product_line,
                    "metal_color": metal_color,
                    "metal_type": metal_type,
                    'gemstone': gemstone,
                    "color": color,
                    "model_sku": model_sku,
                    "depth": depth,
                    "height": height,
                    "width": width,
            }

            return transformed_record
        else:
            return None
