#Key Mapper for pandora data - model + color + material combination is used to de-dup items
class Mapper:
    def map(self, record):
        model = ''
        color = ''
        gemstone = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity['attribute']:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity['attribute']:
                    color = entity["attribute"][0]["id"]
            elif "gemstone" == entity['name']:
                if entity['attribute']:
                    gemstone = entity["attribute"][0]["id"]

        key_field = model + color + gemstone
        return key_field
