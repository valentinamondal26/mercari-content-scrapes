import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            for key in record:
                if key == 'category':
                    transformed_record['category'] = 'Bracelets'
                else:
                    transformed_record[key] = record[key]

            ner_query = record.get('description')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price=record.get('price','')
            price = price.replace("$", "")
            price=''.join(price.split())
            price=price.replace(",","")
            title=record.get('title','')
            model=title
            collection=record.get('Collection','')
            pattern=r"\w+"
            collection_words=re.findall(pattern,collection)
            for word in collection_words:
                model=model.replace(word,'')
            dimensions=record.get('Dimensions:','')
            pattern_depth=r"Depth\s*\:*\s*[\d\.]+"
            pattern_height=r"Height\s*\:*\s*[\d\.]+"
            pattern_width=r"Width\s*\:*\s*[\d\.]+"
            depth=re.findall(pattern_depth,dimensions)
            if depth!=[]:
                depth=depth[0].split('Depth:')[1].strip()
            else:
                depth=''
            height=re.findall(pattern_height,dimensions)
            if height!=[]:
                height=height[0].split('Height:')[1].strip()
            else:
                height=''
            width=re.findall(pattern_width,dimensions)
            if width!=[]:
                width=width[0].split('Width:')[1].strip()
            else:
                width=''

            product_line=record.get('Collection','')
            if product_line=="-":
                product_line=''
            else:
                product_line_words=product_line.split(", ")
                product_line_priority=['Disney x Pandora','Pandora Moments','Pandora Reflexions','Pandora Signature','Pandora Shine','Pandora Rose','Pandora Me','Product Care']
                index=[product_line_priority.index(word) if word in product_line_priority else -1 for word in product_line_words]
                index.sort()
                position=[ind for ind in index if ind>0]
                if position!=[]:
                    product_line=product_line_priority[position[0]]

            model=model.replace(",",'')
            model=model.replace('™','')
            model=' '.join(model.split())



            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": 'Bracelets',
                "brand": 'PANDORA',
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description','').replace("\n",''),
                "title": record.get('title', ''),
                "metal": record.get('Metal',''),
                "ner_query": ner_query.replace("\n",''),
                "model":model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "product_line": product_line,
                "metal_color":record.get('metal_color',''),
                "stone": record.get('Stone',''),
                "color": record.get('Color',''),
                "model_sku":record.get('Item',''),
                "depth":depth,
                "height":height,
                "width":width

            }

            return transformed_record
        else:
            return None


