#Key Mapper for pandora data - model + color + material combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        model_sku = ''

        entities = record['entities']
        for entity in entities:
            if "model_sku" == entity['name']:
                if entity['attribute']!=[]:
                    model_sku = entity["attribute"][0]["id"]

        key_field = model_sku
        return key_field
