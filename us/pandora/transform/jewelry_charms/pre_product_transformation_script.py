import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            for key in record:
                if key == 'category':
                    transformed_record['category'] = 'Rings'
                else:
                    transformed_record[key] = record[key]

            ner_query = record.get('description')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price=record.get('price','')
            price = price.replace("$", "")
            price=''.join(price.split())
            price=price.replace(",","")
            title=record.get('title','')
            model=title
            
            collection=record.get('Collection','')
            pattern=r"\w+"
            collection_words=re.findall(pattern,collection)
            for word in collection_words:
                model=model.replace(word,'')

            dimensions=record.get('Dimensions:','')
            pattern_depth=r"Depth\s*\:*\s*[\d\.]+"
            pattern_height=r"Height\s*\:*\s*[\d\.]+"
            pattern_width=r"Width\s*\:*\s*[\d\.]+"
            depth=re.findall(pattern_depth,dimensions)
            if depth!=[]:
                depth=depth[0].split('Depth:')[1].strip()
            else:
                depth=''
            height=re.findall(pattern_height,dimensions)
            if height!=[]:
                height=height[0].split('Height:')[1].strip()
            else:
                height=''
            width=re.findall(pattern_width,dimensions)
            if width!=[]:
                width=width[0].split('Width:')[1].strip()
            else:
                width=''
            
            model=model.replace(",",'')
            model=model.replace('™','')
            model=html.unescape(model)
            model=re.sub(re.compile(r"\b&nbsp;\b",re.IGNORECASE),'',model)
            model=' '.join(model.split())
            
            


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": 'Jewelry Charms',
                "brand": 'PANDORA',
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": html.unescape(record.get('description','')).strip(),
                "title": record.get('title', ''),
                "ner_query": html.unescape(ner_query).strip(),
                "model":model,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "product_line": html.unescape(record.get('Collection','')),
                "metal_color":html.unescape(record.get('metal_color','')),
                "stone": html.unescape(record.get('Stone','')),
                "color": html.unescape(record.get('Color','')),
                "model_sku":html.unescape(record.get('Item','')),
                "theme":html.unescape(record.get('Themes','')),
                "depth":html.unescape(depth),
                "height":html.unescape(height),
                "width":html.unescape(width)

            }

            return transformed_record
        else:
            return None


