'''
https://mercari.atlassian.net/browse/USCC-736 - SASKI Women's Swimwear
https://mercari.atlassian.net/browse/USCC-737 - SASKI Women's Sports Bras
https://mercari.atlassian.net/browse/USCC-738 - SASKI Women's Tops
https://mercari.atlassian.net/browse/USCC-739 - SASKI Women's Shorts
https://mercari.atlassian.net/browse/USCC-740 - SASKI Women's Leggings
https://mercari.atlassian.net/browse/USCC-741 - SASKI Women's Coats, Jackets & Vests
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

import re
import json
from urllib.parse import urlparse, parse_qs


class SaskicollectionSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from saskicollection.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Swimwear
        2) Women's Sports Bras
        3) Women's Tops
        4) Women's Shorts
        5) Women's Leggings
        6) Women's Coats, Jackets & Vests

    brand : str
        brand to be crawled, Supports
        1) SASKI

    Command e.g:
    scrapy crawl saskicollection -a category="Women's Swimwear" -a brand="SASKI"
    scrapy crawl saskicollection -a category="Women's Sports Bras" -a brand="SASKI"
    scrapy crawl saskicollection -a category="Women's Tops" -a brand="SASKI"
    scrapy crawl saskicollection -a category="Women's Shorts" -a brand="SASKI"
    scrapy crawl saskicollection -a category="Women's Leggings" -a brand="SASKI"
    scrapy crawl saskicollection -a category="Women's Coats, Jackets & Vests" -a brand="SASKI"
    """

    name = 'saskicollection'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'saskicollection.com'
    blob_name = 'saskicollection.txt'

    merge_key = 'id'

    api_key = ''
    product_type = ''
    collection = ''

    listing_api_url = 'https://www.searchanise.com/getresults?api_key={api_key}&q=&sortBy=created&sortOrder=desc&restrictBy%5Bproduct_type%5D={product_type}&startIndex={start_index}&maxResults=32&collection={collection}'

    url_dict = {
        "Women's Swimwear": {
            "SASKI": {
                'url': 'https://us.saskicollection.com/collections/all-products?page=1&rb_product_type=Swim',
            }
        },
        "Women's Sports Bras": {
            "SASKI": {
                'url': 'https://us.saskicollection.com/collections/all-products?page=1&rb_product_type=Sports+Bra%7CSports+bra%7CSports+Bras',
            }
        },
        "Women's Tops": {
            "SASKI": {
                'url': 'https://us.saskicollection.com/collections/all-products?page=1&rb_product_type=Tops',
            }
        },
        "Women's Shorts": {
            "SASKI": {
                'url': 'https://us.saskicollection.com/collections/all-products?page=1&rb_product_type=Shorts',
            }
        },
        "Women's Leggings": {
            "SASKI": {
                'url': 'https://us.saskicollection.com/collections/bottoms?page=1&rb_product_type=Leggings',
            }
        },
        "Women's Coats, Jackets & Vests": {
            "SASKI": {
                'url': 'https://us.saskicollection.com/collections/hoodies-jackets',
            }
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SaskicollectionSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://us.saskicollection.com/collections/all-products/products/elle-top-pink-snake
        @meta {"use_proxy":"True"}
        @scrape_values description
        """

        item = response.meta.get('item_info', {})
        item.update({
            'description': ', '.join(
                response.xpath('//div[@itemprop="description"]/ul/li/text()').getall() \
                + response.xpath('//div[@itemprop="description"]/p/text()').getall() \
                + response.xpath('//div[@itemprop="description"]/p/span/text()').getall()
            ),
            'variants': response.xpath('//fieldset[@class="variant-input-wrap"]/a/@title').getall(),
        })
        yield item


    def parse_listing_page(self, response):
        """
        @url https://www.searchanise.com/getresults?api_key=1K2G0A5J0c&q=&sortBy=created&sortOrder=desc&restrictBy%5Bproduct_type%5D=Swim&startIndex=0&maxResults=32&collection=all-products
        @meta {"use_proxy":"True"}
        # @scrape_values id title description price list_price image product_code product_id variant_id tags
        @returns requests 1
        """

        res = json.loads(response.text)

        total_items_count = res.get('totalItems', 0)
        start_index = res.get('startIndex', 0)
        item_count = res.get('currentItemCount', 0)
        # items_per_page = res.get('itemsPerPage', 0)

        if start_index + item_count < total_items_count:
            url = self.listing_api_url.format(api_key=self.api_key, product_type=self.product_type,
                start_index=start_index+item_count, collection=self.collection)
            yield self.create_request(url=url, callback=self.parse_listing_page)

        for item in res.get('items'):
            item_info = {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,

                'id': item.get('link', ''),
                'title': item.get('title', ''),
                'description': item.get('description', ''),
                'price': item.get('price', ''),
                'list_price': item.get('list_price', ''),
                'image': item.get('image_link', ''),
                'product_code': item.get('product_code'),
                'product_id': item.get('product_id', ''),
                'variant_id': item.get('add_to_cart_id', ''),
                'tags': item.get('tags', ''),
            }

            yield self.create_request(url=item.get('link', ''), callback=self.crawlDetail, meta={'item_info': item_info})


    def parse(self, response):
        """
        @url https://us.saskicollection.com/collections/all-products?page=1&rb_product_type=Swim
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        script_url = response.xpath('//div[@class="grid__item one-whole"]/script/@src').get()
        self.api_key = ''.join(parse_qs(urlparse(script_url).query).get('a', []))
        self.product_type = ''.join(parse_qs(urlparse(response.url).query).get('rb_product_type', []))
        self.collection = re.findall(r'collections/(.*)', urlparse(response.url).path)[0]
        url = self.listing_api_url.format(api_key=self.api_key, product_type=self.product_type,
            start_index=0, collection=self.collection)
        yield self.create_request(url=url, callback=self.parse_listing_page)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
