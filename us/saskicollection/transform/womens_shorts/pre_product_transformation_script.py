import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '').replace('\n', ' ')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            title_info = record.get('title', '').split('|')
            model = title_info[0].strip()

            color = ''
            if len(title_info) == 2:
                color = title_info[1].strip()

            title = record.get('title', '')
            if not color:
                color = ''
                other_titles = record.get('variants', [])
                if other_titles:
                    remove_words = title.split(' ')
                    for other_title in other_titles:
                        remove_words = set(remove_words) & set(other_title.split(' '))
                    color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', remove_words))), '', title, flags=re.IGNORECASE)
                    color = re.sub(r'\s+', ' ', color).strip()

                model = title
                if color:
                    model = re.sub(r'\b'+color+r'\b', '', model, flags=re.IGNORECASE)
                model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            material_meta = ['cotton', 'elastane', 'nylon', 'polyester', 'polyomide']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', material_meta))), description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(list(dict.fromkeys(match)))
                material = material.title()

            if re.findall(r'\bSample\b', title, flags=re.IGNORECASE):
                return None


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
