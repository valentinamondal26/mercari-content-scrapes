import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '').replace('\n', ' ')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            title_info = record.get('title', '').split('|')
            model = title_info[0].strip()

            color = ''
            if len(title_info) == 2:
                color = title_info[1].strip()

            material = ''
            match = re.findall(r'(\w+) \(\d+%\)', description, flags=re.IGNORECASE)
            if not match:
                match = re.findall(r'\d+% (\w+)', description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(match)


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
