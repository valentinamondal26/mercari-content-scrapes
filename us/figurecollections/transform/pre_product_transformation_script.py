import hashlib
import re
from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title','')
            model = re.findall(r'^-(.*)\($', model)[0].strip()
            model = re.sub(r'(.*)(\bAction Figures*\b)(.*)', r'\1\3\2', model, flags=re.IGNORECASE)
            if not re.findall(r'\bAction Figures*\b|\bSet\b|\bKit\b', model, flags=re.IGNORECASE):
                model = f'{model} Action Figure'
            if re.findall(r'\bPack\b', model, flags=re.IGNORECASE):
                model = re.sub(r'\bAction Figure\b', 'Action Figures', model, flags=re.IGNORECASE)
            
            release_year_data = record.get('released_year_description','')
            release_year_desc = ', '.join([desc.strip() for desc in release_year_data if desc])
            try:
                release_year = re.findall(r'Year Released:\s*(\d+\-*\d*)',release_year_desc)[0]
            except IndexError:
                release_year = ''

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statusCode',''),

                'category': record.get('category',''),
                'brand': record.get('brand',''),

                'title': record.get('title',''),
                'image': record.get('image',''),
                
                'model': model,
                'series': record.get('series', ''),
                'product_line': record.get('product_line', ''),
                'release_year': release_year,

            }
            return transformed_record
        else:
            return None
