class Mapper:
    def map(self, record):

        model = ''
        series = ''
        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity['attribute']:
                model = entity["attribute"][0]["id"]
            elif "series" == entity['name'] and entity['attribute']:
                series = entity["attribute"][0]["id"]

        key_field = model + series
        return key_field
