'''
https://mercari.atlassian.net/browse/USCC-473 - Mattel Action Figures
'''
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import re
import os

class FigurecollectionsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from figurecollections.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Action Figures
    
    brand : str
        brand to be crawled, Supports
        1) Mattel

    Command e.g:
    scrapy crawl figurecollections -a category='Action Figures' -a brand='Mattel'
    """
    
    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    name = 'figurecollections'
    category = ''
    brand = ''
    source = 'figurecollections.com'
    blob_name = 'figurecollections.txt'
    base_url ='http://figurecollections.com'

    url_dict = {
        'Action Figures': {
            'Mattel': {
                'url': 'http://figurecollections.com/brand.php?id=10'
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            return

        super(FigurecollectionsSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand},url:{url}".format(category=self.category,brand=self.brand,url=self.launch_url))


    def start_requests(self):
        yield self.create_request(url=self.launch_url, callback=self.product_lines_listing_page)


    def product_lines_listing_page(self, response):
        """
        @url http://figurecollections.com/brand.php?id=10
        @returns requests 1
        @meta {"use_proxy": "True"}
        """

        for item in response.xpath("//td[@class='MainText']/p/a"):
            product_url = item.xpath(".//@href").get(default='')
            product_line= item.xpath('.//text()').get(default='')
            meta = {
                'product_line': product_line,
            }
            if re.findall(r'line\.php', product_url, flags=re.IGNORECASE):
                if self.base_url not in product_url:
                    product_url = f'{self.base_url}{product_url}'  
                yield self.create_request(url=product_url, callback=self.series_page, meta=meta)


    def series_page(self, response):
        """
        @url http://figurecollections.com/line.php?id=278
        @returns requests 1
        @meta {"use_proxy": "True"}
        """

        meta = response.meta
        product_description = response.xpath("//td[@class='MainText']/p/text()").get(default='')
        for series  in response.xpath("//td[@class='MainText']/p//a"):
            series_url = series.xpath(".//@href").get(default='')
            series_name = series.xpath(".//b/font/text()").get(default='')

            meta.update({'series_name': series_name})
            meta.update({'product_description': product_description})
            if series_name and series_url:
                yield self.create_request(url=f'{self.base_url}{series_url}', callback=self.crawldetails, meta=meta)


    def crawldetails(self, response):
        """
        @url http://figurecollections.com/series.php?id=1417
        @meta {"use_proxy": "True"}
        @scrape_values id image model_word_in_url title released_year_description
        """

        meta = response.meta
        year_released =  response.xpath("//p[@align='justify'][contains(text(), 'Year Released')]/text()").getall()
        model_name = response.xpath("//tr/td[contains(text(),'Checklist')]/../following-sibling::tr/td/p/text()").getall()
        for model in model_name:
            model = model.strip()
            if re.findall(r'\w+', model):
                model_word_for_url = re.findall(r'^-(.*)\($', model)[0].strip()
                url = f'{response.url}#model={model_word_for_url}'
                try:
                    image = response.xpath('//td/center/a[contains(text(),"'+model_word_for_url+'")]/img/@src').get(default='')
                except ValueError:
                    image = ''
                if image:
                    image = self.base_url+image

                yield {
                    'id': url,
                    'brand': self.brand,
                    'category': self.category,
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': str(response.status),

                    'image': image,
                    'model_word_in_url': model_word_for_url,
                    'title': model,
                    'series': meta.get('series_name', ''),
                    'product_line':  meta.get('product_line', ''),
                    'product_line_description': meta.get('product_description', ''),
                    'released_year_description': year_released
                }


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)

        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({'use_cache': True})
        return request
