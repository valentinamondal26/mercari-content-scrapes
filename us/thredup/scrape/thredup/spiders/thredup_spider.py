'''
https://mercari.atlassian.net/browse/USDE-1211 - Lilly Pulitzer Women's Dresses
https://mercari.atlassian.net/browse/USCC-218 - LuLaRoe Tunic Tops for Women
https://mercari.atlassian.net/browse/USCC-210 - LuLaRoe T-Shirts for Women
https://mercari.atlassian.net/browse/USCC-189 - LuLaRoe Pants, tights, leggings
https://mercari.atlassian.net/browse/USCC-176 - LuLaRoe Women's Knee Length Dresses
https://mercari.atlassian.net/browse/USCC-162 - LuLaRoe Women's High Low Dresses
https://mercari.atlassian.net/browse/USCC-214 - LuLaRoe Women's Cardigans
'''

# -*- coding: utf-8 -*-
import scrapy
import datetime
import random
import os
from scrapy.exceptions import CloseSpider


class ThredupSpider(scrapy.Spider):
    """
     spider to crawl items in a provided category and sub-category from thredup.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Pants, tights, leggings
        2) Women's Cardigans
        3) Women's High Low Dresses
        4) Women's Knee Length Dresses
        5) T-Shirts for Women
        6) Tunic Tops for Women
        7) Women's Dresses

     brand : str
       1) LuLaRoe
       2) Lilly Pulitzer

     Command e.g:
     scrapy crawl thredup  -a category="Pants, tights, leggings" -a brand="LuLaRoe"
     scrapy crawl thredup  -a category="Tunic Tops for Women" -a brand="LuLaRoe"
     scrapy crawl thredup  -a category="T-Shirts for Women" -a brand="LuLaRoe"
     scrapy crawl thredup  -a category="Women's Knee Length Dresses" -a brand="LuLaRoe"
     scrapy crawl thredup  -a category="Women's High Low Dresses" -a brand="LuLaRoe"

     scrapy crawl thredup  -a category="Women's Dresses" -a brand='Lilly Pulitzer'

    """

    name = 'thredup'

    category = None
    brand = None

    merge_key = 'id'

    source = 'thredup.com'
    blob_name = 'thredup.txt'

    filters = None
    filter_base_url = None

    base_url = "https://thredup.com"

    filter_dict = {
    "cut_type": {
    "Skinny Leg": "women-pants-skinny",
    "Straight Leg": "women-pants-straight-leg",
    "Boot Cut": "women-pants-bootcut",
    "Flared Leg": "women-pants-flare",
    "Trouser / Wide Leg": "women-pants-wide-leg",
    "Relaxed / Boyfriend": "women-pants-boyfriend",
    "Culottes": "women-pants-culottes",
    "Harem Pants": "women-pants-harem",
    "Kick Flares": "women-pants-kick-flares"
     },
    "inseam": {
    '13-19"': "chars_inseam_in=%5B13%3A19%5D",
    '20-23"': "chars_inseam_in=%5B20%3A23%5D",
    '24-26"': "chars_inseam_in=%5B24%3A26%5D",
    '27-29"': "chars_inseam_in=%5B27%3A29%5D",
    '30-32"': "chars_inseam_in=%5B30%3A32%5D",
    '33-34"': "chars_inseam_in=%5B33%3A34%5D",
    '35-38"': "chars_inseam_in=%5B35%3A38%5D"
    },
    "rise": {
    "low": "chars_waist=low%20rise%2Csuper%20low%20rise",
    "mid": "chars_waist=mid-reg%20rise",
    "high": "chars_waist=high%20rise"
    },
    "color": {
    "Black": "color_names=black",
    "Gray": "color_names=grey",
    "White": "color_names=white",
    "Ivory": "color_names=ivory",
    "Tan": "color_names=tan",
    "Brown": "color_names=brown",
    "Purple": "color_names=purple",
    "Blue": "color_names=blue",
    "Teal": "color_names=teal",
    "Green": "color_names=green",
    "Red": "color_names=red",
    "Pink": "color_names=pink",
    "Orange": "color_names=orange",
    "Yellow": "color_names=yellow"
    },
    "item_condition": {
    "New with Tags": "condition=q1_nwt",
    "Like-New": "condition=q1_only",
    "Gently Used": "condition=q2_only",
    "Signs of Wear": "condition=q3_only"
    },
    "style":{
        "A-Line":"women-dresses-a-line",
        "Sheath":"women-dresses-sheath",
        "Shift":"women-dresses-shift",
        "Maxi":"women-dresses-maxi",
        "Midi":"women-dresses-midi",
        "Mini":"women-dresses-mini",
        "Bodycon":"women-dresses-bodycon",
        "Fit & Flare":"women-dresses-fit-and-flare",
        "Shirtdress":"women-dresses-shirtdress",
        "High/Low":"women-dresses-high-low",
        "Wrap":"women-dresses-wrap",
        "Drop Waist":"women-dresses-drop-waist",
        "Slip dress":"women-dresses-slip-dress",
        "Popover":"women-dresses-popover",
        "Pencil":"women-dresses-pencil"
    },
    "pattern": {
    "Animal Print": "chars_pattern=animal%20print",
    "Argyle": "chars_pattern=argyle",
    "Camo": "chars_pattern=camo",
    "Checkered & Gingham": "chars_pattern=checkered-gingham",
    "Chevron & Herringbone": "chars_pattern=chevron-herringbone",
    "Color Block": "chars_pattern=color%20block",
    "Floral": "chars_pattern=floral",
    "Graphic": "chars_pattern=graphic",
    "Hearts": "chars_pattern=hearts",
    "Houndstooth": "chars_pattern=houndstooth",
    "Metallic": "chars_pattern=metallic",
    "Paisley": "chars_pattern=paisley",
    "Plaid": "chars_pattern=plaid",
    "Polka Dots": "chars_pattern=polka%20dots",
    "Print": "chars_pattern=print",
    "Solid": "chars_pattern=solid",
    "Stars": "chars_pattern=stars",
    "Stripes": "chars_pattern=stripes",
    "Tie-dye": "chars_pattern=tie-dye",
    "Tropical": "chars_pattern=tropical",
    "Tweed": "chars_pattern=tweed"
    },
    "accents": {
    "Beaded": "chars_accent=beaded",
    "Bows": "chars_accent=bows",
    "Chains": "chars_accent=chains",
    "Cut Outs": "chars_accent=cut%20outs",
    "Embroidered": "chars_accent=embroidered",
    "Flannel": "chars_accent=flannel",
    "Frayed Edges": "chars_accent=frayed%20edges",
    "Fringe": "chars_accent=fringe",
    "Fur": "chars_accent=fur",
    "Glitter": "chars_accent=glitter",
    "Peplum": "chars_accent=peplum",
    "Ripped": "chars_accent=ripped",
    "Ruched": "chars_accent=ruched",
    "Ruffles": "chars_accent=ruffles",
    "Sequins & Rhinestones": "chars_accent=sequins-rhinestones",
    "Shearling": "chars_accent=shearling",
    "Studded": "chars_accent=studded",
    "Velvet": "chars_accent=velvet",
    "Zippers": "chars_accent=zippers"
    },
    "neckline":{
        "Crew Neck":"chars_neckline=crew%20neck",
        "V-Neck":"chars_neckline=v%20neck",
        "Scoop Neck":"chars_neckline=scoop%20neck",
        "Cowl Neck":"chars_neckline=cowl%20neck",
        "Boatneck":"chars_neckline=boatneck",
        "Off The Shoulder":"chars_neckline=off%20the%20shoulder",
        "Cold Shoulder":"chars_neckline=cold%20shoulder",
        "Mock":"chars_neckline=mock",
        "Square":"chars_neckline=square",
        "Keyhole":"chars_neckline=keyhole",
        "One Shoulder":"chars_neckline=one%20shoulder",
        "Henley":"chars_neckline=henley",
        "Halter":"chars_neckline=halter"
    },
    "occasion":{
        "Casual":"women-dresses-casual",
        "Cocktail & Party":"women-dresses-cocktail",
        "Formal":"women-dresses-formal",
        "Little Black Dresses":"women-dresses-little-black",
        "Work":"women-dresses-work",
        "Wedding Guests":"women-dresses-wedding",
        "Bridesmaids":"women-dresses-bridesmaid",
        "Vacation":"women-dresses-vacation",
        "Sweater Dresses":"women-dresses-sweater",
        "Rompers & Jumpsuits":"women-dresses-rompers-and-jumpsuits"
    },
    "shoulder_cut":{
        "Off The Shoulder":"chars_neckline=off%20the%20shoulder",
        "One Shoulder":"chars_neckline=one%20shoulder",
        "Cold Shoulder":"chars_neckline=cold%20shoulder"
    },
    "sleeve_length":{
        "Sleeveless":"chars_sleeve_length=sleeveless",
        "Short Sleeve":"chars_sleeve_length=short%20sleeve",
        "3/4 Sleeve":"chars_sleeve_length=3-4%20sleeve",
        "Long Sleeve":"chars_sleeve_length=long%20sleeve",
        "Strapless":"chars_sleeve_length=strapless"

    }
    }
    url_dict = {
        "Pants, tights, leggings": {
            "LuLaRoe": {
                "url": "https://www.thredup.com/products/women/active-pants/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-pants%2Cwomen-pants-active&sort=Recently%20Discounted",
                "filter_base_url": "https://www.thredup.com/products/women/active-pants/lularoe-black?brand_name_tags=Lularoe&category_group_key=women-pants-active&department_tags=women&search_tags=women-pants%2Cwomen-pants-active&sort=Recently%20Discounted&{filter}",
                "filters": ['cut_type','inseam','rise','color','item_condition','pattern','accents'],
                "entities": {
                    "product_type":"Pants",
                    "style":"Active"
                }
            }
        },
        "Women's Cardigans": {
            "LuLaRoe": {
                "url": "https://www.thredup.com/products/women/cardigan-sweaters/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-sweaters%2Cwomen-sweaters-cardigan-sweaters&sort=Recently%20Discounted",
                "filter_base_url": "https://www.thredup.com/products/women/cardigan-sweaters/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-sweaters%2Cwomen-sweaters-cardigan-sweaters&sort=Recently%20Discounted&{filter}",
                "filters": ['color','neckline','item_condition','pattern','accents'],
                "entities": {
                    "category":"Cardigans"
                }
            }
        },
        "Women's High Low Dresses": {
            "LuLaRoe": {
                "url": "https://www.thredup.com/products/women/dresses/lularoe?brand_name_tags=Lularoe&category_group_key=women-dresses&department_tags=women&search_tags=women-dresses&sort=Recently%20Discounted&chars_skirt_dress_name=high%20low",
                "filter_base_url": "https://www.thredup.com/products/women/rompers-and-jumpsuits/lularoe?brand_name_tags=Lularoe&category_group_key=women-dresses&department_tags=women&search_tags=women-dresses&chars_skirt_dress_name=high%20low&sort=Recently%20Discounted&{filter}",
                "filters": ['occasion','color','neckline','item_condition','pattern','accents','style'],
                "entities": {}
            }
        },
        "Women's Knee Length Dresses": {
            "LuLaRoe": {
                "url": "https://www.thredup.com/products/women/dresses/lularoe?brand_name_tags=Lularoe&category_group_key=women-dresses&department_tags=women&search_tags=women-dresses&chars_skirt_dress_name=knee%20length&sort=Recently%20Discounted",
                "filter_base_url": "https://www.thredup.com/products/women/dresses/lularoe-red?brand_name_tags=Lularoe&category_group_key=women-dresses&department_tags=women&search_tags=women-dresses&chars_skirt_dress_name=knee%20length&sort=Recently%20Discounted&{filter}",
                "filters": ['occasion','color','neckline','item_condition','pattern','accents','style'],
                "entities": {}
            }
        },
        "T-Shirts for Women": {
            "LuLaRoe": {
                "url": "https://www.thredup.com/products/women/t-shirts/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-tops%2Cwomen-tops-t-shirts&sort=Recently%20Discounted",
                "filter_base_url": "https://www.thredup.com/products/women/t-shirts/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-tops%2Cwomen-tops-t-shirts&sort=Recently%20Discounted&{filter}",
                "filters": ['shoulder_cut','sleeve_length','color','neckline','item_condition','pattern','accents'],
                "entities": {
                    "category":"Tops",
                }
            }
        },
        "Tunic Tops for Women": {
            "LuLaRoe": {
                "url": "https://www.thredup.com/products/women/tunic-tops/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-tops%2Cwomen-tops-tunic&sort=Recently%20Discounted",
                "filter_base_url": "https://www.thredup.com/products/women/tunic-tops/lularoe?brand_name_tags=Lularoe&department_tags=women&search_tags=women-tops%2Cwomen-tops-tunic&sort=Recently%20Discounted&{filter}",
                "filters": ['shoulder_cut','sleeve_length','color','neckline','item_condition','pattern','accents'],
                "entities": {
                    "category":"Tops",
                }
            }
        },
        "Women's Dresses": {
            'Lilly Pulitzer': {
                'url': 'https://www.thredup.com/products/women/mini-dresses?search_tags=women-dresses%2Cwomen-dresses-mini&department_tags=women&brand_name_tags=Lilly%20Pulitzer',
                'filter_base_url': 'https://www.thredup.com/products/women/dresses?search_tags=women-dresses%2Cwomen-dresses-mini&department_tags=women&brand_name_tags=Lilly%20Pulitzer&{filter}',
                'filters': ['color'],
                'entities': {},
            }
        }
    }

    proxy_pool = [
           'http://shp-mercari-us-d00001.tp-ns.com/',
            'http://shp-mercari-us-d00002.tp-ns.com/',
            'http://shp-mercari-us-d00003.tp-ns.com/',
            'http://shp-mercari-us-d00004.tp-ns.com/',
            'http://shp-mercari-us-d00005.tp-ns.com/',
            'http://shp-mercari-us-d00006.tp-ns.com/',
            'http://shp-mercari-us-d00007.tp-ns.com/',
            'http://shp-mercari-us-d00008.tp-ns.com/',
            'http://shp-mercari-us-d00009.tp-ns.com/',
            'http://shp-mercari-us-d00010.tp-ns.com/',
            'http://shp-mercari-us-d00011.tp-ns.com/',
            'http://shp-mercari-us-d00012.tp-ns.com/',
            'http://shp-mercari-us-d00013.tp-ns.com/',
            'http://shp-mercari-us-d00014.tp-ns.com/',
            'http://shp-mercari-us-d00015.tp-ns.com/',
            'http://shp-mercari-us-d00016.tp-ns.com/',
            'http://shp-mercari-us-d00017.tp-ns.com/',
            'http://shp-mercari-us-d00018.tp-ns.com/',
            'http://shp-mercari-us-d00019.tp-ns.com/',
            'http://shp-mercari-us-d00020.tp-ns.com/',
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ThredupSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.filter_base_url = self.url_dict.get(category, {}).get(brand, {}).get('filter_base_url', '')
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['color']
        self.filter_base_url = 'https://www.thredup.com/products/women/dresses?search_tags=women-dresses%2Cwomen-dresses-mini&department_tags=women&brand_name_tags=Lilly%20Pulitzer&{filter}'


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)
            yield self.create_request(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self, response):
        """
        @url  https://www.thredup.com/products/women/mini-dresses?search_tags=women-dresses%2Cwomen-dresses-mini&department_tags=women&brand_name_tags=Lilly%20Pulitzer
        @meta  {"use_proxy":"True"}  
        @returns requests 1
        """

        for filter in self.filters:
            filter_url=self.filter_dict.get(filter,{})
            for key,value in filter_url.items():
                meta={}
                meta=meta.fromkeys([filter],key)
                meta.update({'filter_name':filter})
                yield self.create_request(
                    url=self.filter_base_url.format(filter=value),
                    callback=self.parse,
                    meta=meta,
                    headers={'Referer':self.filter_base_url.format(filter=value)}
                )


    def parse(self, response):
        """
        @url  https://www.thredup.com/products/women/mini-dresses?search_tags=women-dresses%2Cwomen-dresses-mini&department_tags=women&brand_name_tags=Lilly%20Pulitzer
        @meta  {"use_proxy":"True"}
        @returns requests 51
        """

        meta = response.meta
        product_urls = response.xpath('//a[div[@class="item-title"]]/@href').getall() \
            or response.xpath("//div[@class='grid']//a[@class='_1di0il_2VkBBwWJz9eDxoJ']/@href").getall()
        for product_url in product_urls:
            if self.base_url not in product_url:
                product_url = self.base_url + product_url
            yield self.create_request(url=product_url, callback=self.parse_products, meta=meta)

        next_page_url = response.xpath('//a[@class="_1f2Ip"]/@href').get(default='') \
            or response.xpath('//link[contains(@rel,"next")]/@href').get(default='')
        if next_page_url:
            if self.base_url not in next_page_url:
                next_page_url = self.base_url + next_page_url
            yield self.create_request(url=next_page_url, callback=self.parse, meta=meta)


    def parse_products(self,response):
        """
        @url https://www.thredup.com/product/women-polyester-lilly-pulitzer-blue-casual-dress/78343476
        @scrape_values image breadcrumb title occasion striked_price price est_retail material measurements description condition sku
        """

        meta = response.meta
        data = {
            'id': response.url,
            'statusCode': str(response.status),
            'crawlDate': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),

            'system_category': self.category,
            'brand': self.brand,

            'image': response.xpath("//div[@class='_17adkz6zswDjAoZ8PhDmPr']/div/img/@data-src").get(default='') \
                or response.xpath("//div[@class='_139h8vwTwz4vzrz8E2JRFI _3LJJ0WbcbPX_m4vq5E8Urv detail']/img/@data-src").get(default=''),
            'breadcrumb': ' | '.join(response.xpath("//nav[@class='_3p7XtL0LlyGSi8UgI6EU3j _12-L0I76mLCOu9b4N_SCPU']/a/text()").getall()),

            'title': response.xpath("//a[@class='_32zNmjMSfxcoBWGGlzPobp']/@title").get(default=''),
            'occasion': response.xpath("//span[@class='_2gP239lLt_bs4iB60hEm85']/text()").get(default=''),
            'striked_price': ''.join(response.xpath("//span[@class='RFcwwL7_eda8sdWkyafAr _2vi-pUlDsNv8xzxZL7yBjd striked-price']/text()").getall()),
            'price': ''.join(response.xpath("//span[@class='RFcwwL7_eda8sdWkyafAr spot-coral price']/text()").getall()) \
                or ''.join(response.xpath("//span[@class='RFcwwL7_eda8sdWkyafAr price']/text()").getall()),
            'est_retail': "".join(response.xpath("//div[@class='spot-grey-5 savings']/span[contains(text(),'$')]/text()").getall()) \
                or "".join(response.xpath("//span[@class='_1jSFxP9HAJ6rr_KTsaPRYp']/text()").getall()),
            'material': ', '.join(response.xpath("//div[@class='_2SuzvN3bcsWRtNcNiSLMGq']/h2[contains(text(),'Materials')]/following-sibling::p/text()").getall()),
            'measurements': ', '.join(response.xpath("//div[@class='_2SuzvN3bcsWRtNcNiSLMGq']/h2[contains(text(),'Measurements')]/following-sibling::ul/li/text()").getall()),
            'description': ', '.join(response.xpath("//div[@class='_2SuzvN3bcsWRtNcNiSLMGq']/h2[contains(text(),'Description')]/following-sibling::ul/li/text()").getall()),
            'condition': ', '.join(response.xpath("//div[@class='_2SuzvN3bcsWRtNcNiSLMGq']/h2[contains(text(),'Condition')]/following-sibling::p/text()").getall()),
            'sku': response.xpath("//h2[contains(text(),'Item #')]/following-sibling::p/text()").get(default=''),
        }
        filter_name = meta.get("filter_name","")
        if filter_name:
            meta_data =  dict.fromkeys([filter_name], meta[filter_name])
            data.update(meta_data)
        yield data


    # NOTE: site updated the UI and so the older scrape of detail page is not working
    # def parse_products(self,response):
    #     item={}
    #     item['id']=response.url
    #     item['crawlDate']=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    #     item['statusCode']=str(response.status)
    #     item['system_category']=self.category
    #     item['brand']=self.brand
    #     image=response.xpath('//div[@class="display-photo remove-overflow"]//img/@src').getall()
    #     if len(image)>=1:
    #         item['image']=image[1]
    #     else:
    #         item['image']=''
    #     price=response.xpath('//div[@class="primary-info-prices"]/div[@class="primary-info-row price-current-previous spot-red"]/following-sibling::div//span//text()').getall()
    #     price=[x for x in price if "$" in x]
    #     if price!=[]:
    #         item['price']=price[0]
    #     else:
    #         item['price']=''
    #     item['est_reatil']=response.xpath('//div[@class="primary-info-prices"]/div[@class="primary-info-row price-current-previous spot-red"]//span//text()').get(default='')
    #     item['description']=' '.join(response.xpath('//div[@class="pdp-details-section"]//div[strong[contains(text(),"Description")]]//li//text()').getall())
    #     item['measurements']=' '.join(response.xpath('//div[@class="pdp-details-section"]//div[strong[contains(text(),"Measurements")]]//li//text()').getall())
    #     item['material']=' '.join(response.xpath('//div[@class="pdp-details-section"]//div[strong[contains(text(),"Materials")]]//text()').getall())
    #     item['sku']=' '.join(response.xpath('//div[@class="pdp-details-section"]//div[strong[contains(text(),"Item#")]]/text()').getall())
    #     item['condition']=' '.join(response.xpath('//div[@class="pdp-details-section"]//div[strong[contains(text(),"Condition")]]/p//text()').getall())
    # 	meta={}
    # 	filter_name=response.meta.get('filter_name','')
    # 	meta=meta.fromkeys([filter_name],response.meta[filter_name])
    # 	meta.update(self.url_dict.get(self.category,{}).get(self.brand,{}).get("entities",{}))
    # 	item.update(meta)
    # 	yield item


    def create_request(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback, headers=headers, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({'use_cache':True})
        return request

