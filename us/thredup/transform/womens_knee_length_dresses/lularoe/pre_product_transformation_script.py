import re
import hashlib
from colour import Color

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            materials=record.get('material','')
            materials=re.findall(r"\d+\.*\d*%\s*[a-zA-Z]+",materials)
            material=''
            for mate in materials:
                mate=re.sub(r"\d+\.*\d*%\s*",'',mate)
                if mate!='':
                    material+=mate+", "
            if ", " in material:
                material=material[:-2]
            measurements=record.get('measurements','')
           
           
          
            pattern=re.compile(r"[\w\s\(\)]*Sizing chart",re.IGNORECASE)
            size=re.findall(pattern,measurements)
            if size!=[]:
                pattern=re.compile(r"\s*Sizing chart*|size",re.IGNORECASE)
                size=re.sub(pattern,'',size[0])
            else:
                size=''
            size=' '.join(size.split()).title()

            pattern=re.compile(r"[\d\.\"]*\s*length",re.IGNORECASE)
            length=re.findall(pattern,measurements)
            if length!=[]:
                pattern=re.compile(r"\s*length",re.IGNORECASE)
                length=re.sub(pattern,'',length[0])
            else:
                length=''

            pattern=re.compile(r"[\d\.\"]*\s*Chest",re.IGNORECASE)
            chest_width=re.findall(pattern,measurements)
            if chest_width!=[]:
                pattern=re.compile(r"\s*Chest",re.IGNORECASE)
                chest_width=re.sub(pattern,'',chest_width[0])
            else:
                chest_width=''

            neckline=record.get("neckline",'')
            necklines=["Crew","V-","Scoop","Cowl","Boat","Off The Shoulder","Cold Shoulder","Mock","Square","Keyhole","One Shoulder","Henley","Halter"]
            neckline_meta=["Strapless","Sweetheart","One Shoulder","Off The Shoulder","Halter","V-Neck","Crew Neck","Scoop Neck","Boatneck","Plunge","Square","Cowl Neck","Keyhole","Mock","Cold Shoulder","Tie Neck","Turtleneck","Henley"]
            if neckline=='':
                neckline=[]
                for word in neckline_meta:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        neckline.append(word)
                for word in necklines:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        neckline.append(word)

                neckline=', '.join(neckline)
                

                
                if neckline not in necklines:
                    for neck in necklines:
                         if neck in neckline:
                             pos=neckline.find(neck)
                             neckline=neckline[pos:pos+len(neck)]
        

                
                if "Crew" in neckline or "V-" in neckline or "Scoop" in neckline or "Cowl" in neckline:
                        neckline+=" Neck"
                if "Boat" in neckline:
                    neckline+="neck"

            

            condition=record.get('item_condition','')
            if condition=='':
                condition=record.get('condition','')
                if "tags" in condition:
                    condition="New with Tags"
                elif "excellent" in condition:
                    condition="Like-New"
                elif "gently" in condition:
                    condition="Gently Used"

            color=record.get('color','')
            if color=='':
                colors = [i for i in re.findall(r"\w+",description) if self.check_color(i)]
                color=[]
                if colors!=[]:
                    for cl in colors:
                        if cl!='':
                            color.append(cl)
                if color==[]:
                    if re.findall(re.compile(r"Burgundy",re.IGNORECASE),description)!=[]:
                        color.append(re.findall(re.compile(r"Burgundy",re.IGNORECASE),description)[0])
                color=', '.join(color)
            
            color=', '.join(list(set(color.split(", "))))
            material=', '.join(list(set(material.split(", "))))
           

            
            patterns=record.get('pattern','')
            if patterns=='':
                patterns=[]
                meta_pattern=["Animal Print","Argyle","Camo","Checkered & Gingham","Chevron & Herringbone","Chevron/Herringbone","Color Block","Floral","Graphic","Hearts","Houndstooth","Metallic","Paisley","Plaid","Polka Dots","Print","Solid","Stars","Stripes","Tie-dye","Tropical","Tweed","Striped"]
                for word in meta_pattern:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        patterns.append(word)
                patterns=", ".join(patterns)


        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "system_category": "Women's Knee Length Dresses",
                "category":record.get("category",''),
                "brand":record.get('brand',''),
                "image":record.get("image",''),
                "sku":record.get('sku',''),
                "description": description,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("est_reatil","").strip('$').replace(',','')
                },
                "color":color,
                "material":material,
                "size":size,
                "style":record.get("style",''),
                "item_condition":condition,
                "pattern":patterns,
                "accents":record.get('accents',''),
                "length":length,
                "neckline":neckline,
                "occasion":record.get("occasion",'').strip("Dresses"),
                "chest_width":chest_width
               
                }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False