import re
import hashlib
from colour import Color
import difflib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            materials=record.get('material','')
            materials=re.findall(r"\d+\.*\d*%\s*[a-zA-Z]+",materials)
            material=''
            for mate in materials:
                mate=re.sub(r"\d+\.*\d*%\s*",'',mate)
                material+=mate+", "
            material=material[:-2]
            measurements=record.get('measurements','')
            inseam=record.get('inseam')
            if inseam=='':       
                measurement=re.findall(r"\d+\.*\d*\"\s*Inseam",measurements)
                if measurement!=[]:
                    inseam=measurement[0]
                    inseam=float(re.sub(r"\"\s*[a-zA-Z]+",'',inseam))
                    if inseam >= 13 and inseam<=19:
                        inseam='13-19"'
                    elif inseam > 19 and inseam<=23:
                        inseam='20-23"'
                    elif inseam >23 and inseam<=26:
                        inseam='24-26"'
                    elif inseam >26 and inseam<=29:
                        inseam='27-29"'
                    elif inseam >29 and inseam<=32:
                        inseam='30-32"'
                    elif inseam >32 and inseam<=34:
                        inseam='33-34"'
                    elif inseam >34 and inseam<=38:
                        inseam='35-38"'
            rise=record.get('rise','')
            if rise=='':
                pattern=re.compile(r"mid|high|low",re.IGNORECASE)
                rise=re.findall(pattern,description)
                if rise!=[]:
                    rise=rise[0]
                else:
                    rise=''

            pattern=re.compile(r"\d+\.*\d*\"\s*waist",re.IGNORECASE)
            waist_size=re.findall(pattern,measurements)
            if waist_size!=[]:
                pattern=re.compile(r"\s*waist",re.IGNORECASE)
                waist_size=re.sub(pattern,'',waist_size[0])
            else:
                waist_size=''

            pattern=re.compile(r"[\w\s\(\)]*Sizing chart",re.IGNORECASE)
            size=re.findall(pattern,measurements)
            if size!=[]:
                pattern=re.compile(r"\s*Sizing chart*|size",re.IGNORECASE)
                size=re.sub(pattern,'',size[0])
            else:
                size=''
            size=' '.join(size.split()).title()



            color=record.get('color','')
            if color=='':
                words_description=re.findall(r"\w+",description)
                _color = [i for i in words_description if self.check_color(i)]
                if _color!=[]:
                    color=_color[0]
            if color=='':
                colors=["Black","Gray","White","Ivory","Tan","Brown","Purple","Blue","Teal","Green","Red","Pink","Orange","Yellow","Burgundy"]
                for cl in colors:
                    pattern=re.compile(r"%s"%cl,re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        color=re.findall(pattern,description)[0]

            pattern=re.compile(r"[\d\.\"]*\s*length",re.IGNORECASE)
            length=re.findall(pattern,measurements)
            if length!=[]:
                pattern=re.compile(r"\s*length",re.IGNORECASE)
                length=re.sub(pattern,'',length[0])
            else:
                length=''


            neckline=record.get('neckline','')
            if neckline=='':
                necklines=["Crew","V-Neck","Scoop","Cowl","Boat","Off The Shoulder","Cold Shoulder","Mock","Square","Keyhole","One Shoulder","Henley","Halter","Plunge"]
                for pat in necklines:
                    pattern_re=re.compile(r"%s"%pat,re.IGNORECASE)
                    pattern_arr=re.findall(pattern_re,description)
                    if pattern_arr!=[]:
                        neckline+=pattern_arr[0]+", "
                description_words=re.findall(r"\w+",description)
                matches=[]
                for pat in necklines:
                    if difflib.get_close_matches(pat,description_words,cutoff=0.7)!=[]:
                        matches.append(difflib.get_close_matches(pat,description_words,cutoff=0.7))
                        for match in matches:
                            if difflib.get_close_matches(match[0],necklines,cutoff=0.7)!=[]:
                                if not re.search(r'%s'%difflib.get_close_matches(match[0],necklines,cutoff=0.7)[0], neckline, re.IGNORECASE):
                                    neckline+=difflib.get_close_matches(match[0],necklines,cutoff=0.7)[0]+", "
                neckline=neckline[:-2]
                if "Neck" not in neckline and "neck" not in neckline and neckline!='':
                    neckline+=" Neck"

            if neckline=='':
                pattern=re.compile(r"[\w\s\W]*neckline",re.IGNORECASE)
                neckline=re.findall(pattern,description)
                if neckline!=[]:
                    pattern=re.compile(r"neckline|\s*",re.IGNORECASE)
                    neckline=re.sub(pattern,'',neckline[0])+" Neck"
                else:
                    neckline=''
                
           
               

            

            condition=record.get('item_condition','')
            if condition=='':
                condition=record.get('condition','')
                if "tags" in condition:
                    condition="New with Tags"
                elif "excellent" in condition:
                    condition="Like-New"
                elif "gently" in condition:
                    condition="Gently Used"



        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "system_category": "Women's Cardigans",
                "category":record.get("category",''),
                "brand":record.get('brand',''),
                "image":record.get("image",''),
                "sku":record.get('sku',''),
                "description": description,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("est_reatil","").strip('$').replace(',','')
                },
                "color":color,
                "material":material,
                "size":size,
                "item_condition":condition,
                #"pattern":record.get('pattern',''),
                #"accents":record.get('accents',''),
                "length":length,
                "neckline":neckline.title()
               
                }

            return transformed_record
        else:
            return None
        
    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
