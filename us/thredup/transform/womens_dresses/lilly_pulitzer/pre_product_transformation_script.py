import re
import hashlib
from colour import Color
import difflib

class Mapper(object):

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            materials = record.get('material', '')
            materials = re.findall(r'\d+\.*\d*%\s*[a-zA-Z]+', materials)
            material = ''
            for mate in materials:
                mate = re.sub(r'\d+\.*\d*%\s*', '', mate)
                material += mate + ', '
            material = material[:-2]

            measurements = record.get('measurements', '')

            material_meta = [
                'Acrylic', 'Cashmere', 'Cotton', 'Faux Leather', 'Leather', 'Linen', 'Nylon',
                'Polyester', 'Rayon', 'Silk', 'Spandex', 'Wool'
            ]
            if not material:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', material_meta))), re.IGNORECASE), description)
                if match:
                    material = ', '.join(material)

            color = record.get('color', '')
            if not color:
                color = []
                words_description = re.findall(r"\w+", description)
                _color = [i for i in words_description if self.check_color(i)]
                if _color:
                    color.append(_color[0])

                if not color:
                    colors = ['Teal', 'Burgundy']
                    for word in colors:
                        if re.findall(re.compile(r"{}".format(word), re.IGNORECASE), description):
                            color.append(word)
                color = ', '.join(color)

            size = ''
            match = re.findall(re.compile(r'[\w\s\(\)]*Sizing chart', re.IGNORECASE), measurements)
            if match:
                size = re.sub(re.compile(r'\s*Sizing chart*|size', re.IGNORECASE), '', match[0])
            size = ' '.join(size.split()).title()

            length = ''
            match = re.findall(re.compile(r'[\d\.\"]*\s*length', re.IGNORECASE), measurements)
            if match:
                length = re.sub(re.compile(r'\s*length', re.IGNORECASE), '', match[0])

            chest_width = ''
            match = re.findall(re.compile(r'[\d\.\"]*\s*Chest', re.IGNORECASE), measurements)
            if match:
                chest_width = re.sub(re.compile(r'\s*Chest', re.IGNORECASE), '', match[0])

            entity_pattern = record.get('pattern', '')
            if not entity_pattern:
                patterns_meta = [
                    "Animal Print", "Argyle", "Camo", "Checkered & Gingham", "Chevron & Herringbone",
                    "Color Block", "Floral", "Graphic", "Hearts", "Houndstooth", "Metallic", "Paisley",
                    "Plaid", "Polka Dots", "Print", "Solid", "Stars", "Stripes", "Tie-dye",
                    "Tropical", "Tweed"
                ]
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', patterns_meta))), re.IGNORECASE), description)
                if match:
                    entity_pattern = ', '.join(match)
                description_words = re.findall(r'\w+', description)
                matches = []
                cutoff = 0.7
                for pat in patterns_meta:
                    close_match_in_description = difflib.get_close_matches(pat, description_words, cutoff=cutoff)
                    if close_match_in_description:
                        matches.append(close_match_in_description)
                        for match in matches:
                            close_match_in_patterns_meta = difflib.get_close_matches(match[0], patterns_meta, cutoff=cutoff)[0]
                            if close_match_in_patterns_meta:
                                if not re.search(r'%s'%close_match_in_patterns_meta, entity_pattern, re.IGNORECASE):
                                    entity_pattern += ', ' + close_match_in_patterns_meta if entity_pattern else close_match_in_patterns_meta

            entity_pattern = entity_pattern.title()
            entity_pattern = ', '.join(list(dict.fromkeys(entity_pattern.split(', '))))
            entity_pattern = re.sub(r'\s+', ' ', entity_pattern).strip()

            neckline = record.get('neckline', '')
            necklines = [
                'Crew', 'V-', 'Scoop', 'Cowl', 'Boat', 'Off The Shoulder', 'Cold Shoulder', 'Mock',
                'Square', 'Keyhole', 'One Shoulder', 'Henley', 'Halter'
            ]
            neckline_meta = [
                'Strapless', 'Sweetheart', 'One Shoulder', 'Off The Shoulder', 'Halter', 'V-Neck',
                'Crew Neck', 'Scoop Neck', 'Boatneck', 'Plunge', 'Square', 'Cowl Neck', 'Keyhole',
                'Mock', 'Cold Shoulder', 'Tie Neck', 'Turtleneck', 'Henley'
            ]
            if not neckline:
                neckline = []
                for word in neckline_meta:
                    pattern = re.compile(r"{}".format(word), re.IGNORECASE)
                    if re.findall(pattern,description):
                        neckline.append(word)
                for word in necklines:
                    pattern = re.compile(r"{}".format(word), re.IGNORECASE)
                    if re.findall(pattern, description):
                        neckline.append(word)

                neckline = ', '.join(neckline)

                if neckline not in necklines:
                    for neck in necklines:
                         if neck in neckline:
                             pos = neckline.find(neck)
                             neckline = neckline[pos:pos+len(neck)]

            if neckline in ['Crew', 'V-', 'Scoop', 'Cowl']:
                neckline += ' Neck'
            if neckline in ['Boat']:
                neckline += 'neck'

            neckline = re.sub(re.compile(r"V\- Neck|V\-Neck", re.IGNORECASE), 'V Neck', neckline)

            condition = record.get('item_condition', '')
            if not condition:
                condition = record.get('condition', '')
                if 'tags' in condition:
                    condition = 'New with Tags'
                elif 'excellent' in condition:
                    condition = 'Like-New'
                elif 'gently' in condition:
                    condition = 'Gently Used'

            occasion = record.get('occasion', '').replace('Dresses', '').replace('Dress', '')
            if occasion[-1].lower() == 's':
                occasion = occasion[0:-1]

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', '').split(', ', 1)[0],
                "status_code": record.get('statusCode', ''),

                "category": record.get('system_category', ''),
                "brand": record.get('brand', ''),

                "image": record.get("image", ''),
                "ner_query": ner_query,

                "sku": record.get('sku', ''),
                "description": description,
                "price": {
                    "currency_code": 'USD',
                    "amount": record.get('price', '').strip('$').replace(',', '')
                },
                "msrp":{
                    "currency_code": 'USD',
                    "amount": record.get('est_retail', '').strip('$').replace(',', '')
                },
                "color": color,
                "material": material,
                "size": size,
                "style": record.get('style', ''),
                "item_condition": condition,
                "pattern": entity_pattern,
                "accents": record.get('accents', ''),
                "length": length,
                "neckline": neckline,
                "occasion": occasion,
                "chest_width": chest_width,

            }

            return transformed_record
        else:
            return None
