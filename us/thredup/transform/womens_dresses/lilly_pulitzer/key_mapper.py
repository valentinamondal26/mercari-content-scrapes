#Key Mapper for thredup data - color + material  combination is used to de-dup items
class Mapper:
    def map(self, record):

        color = ''
        material = ''
        occasion= ''

        entities = record['entities']
        for entity in entities:
            if "color" == entity['name']:
                if entity['attribute']:
                    color = entity["attribute"][0]["id"]
            elif "material" == entity['name']:
                if entity['attribute']:
                    material = entity["attribute"][0]["id"]
            elif "occasion" == entity['name']:
                if entity['attribute']:
                    occasion = entity["attribute"][0]["id"]

        key_field = color + material + occasion
        return key_field
