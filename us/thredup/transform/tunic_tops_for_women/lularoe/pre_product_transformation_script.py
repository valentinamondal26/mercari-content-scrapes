import re
import hashlib
import difflib
class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            materials=record.get('material','')
            materials=re.findall(r"\d+\.*\d*%\s*[a-zA-Z]+",materials)
            material=''
            for mate in materials:
                mate=re.sub(r"\d+\.*\d*%\s*",'',mate)
                material+=mate+", "
            material=material[:-2]
            measurements=record.get('measurements','')


            pattern=re.compile(r"[\d\.\"]*\s*length",re.IGNORECASE)
            length=re.findall(pattern,measurements)
            if length!=[]:
                pattern=re.compile(r"\s*length",re.IGNORECASE)
                length=re.sub(pattern,'',length[0])
            else:
                length=''


            pattern=re.compile(r"[\d\.\"]*\s*Chest",re.IGNORECASE)
            chest_width=re.findall(pattern,measurements)
            if chest_width!=[]:
                pattern=re.compile(r"\s*Chest",re.IGNORECASE)
                chest_width=re.sub(pattern,'',chest_width[0])
            else:
                chest_width=''

            neckline=record.get("neckline",'')
            if neckline=='':
                pattern=re.compile(r"[\w\s\W]*neckline",re.IGNORECASE)
                neckline=re.findall(pattern,description)
                if neckline!=[]:
                    pattern=re.compile(r"neckline|\s*",re.IGNORECASE)
                    neckline=re.sub(pattern,'',neckline[0])
                    if "Crew" in neckline or "V-" in neckline or "Scoop" in neckline or "Cowl" in neckline or "Boat" in neckline:
                        neckline+=" Neck"
                else:
                    neckline=''

            pattern=re.compile(r"[\w\s\(\)]*Sizing chart",re.IGNORECASE)
            size=re.findall(pattern,measurements)
            if size!=[]:
                pattern=re.compile(r"\s*Sizing chart*|size",re.IGNORECASE)
                size=re.sub(pattern,'',size[0])
            else:
                size=''
            size=' '.join(size.split()).title()
            

            condition=record.get('item_condition','')
            if condition=='':
                condition=record.get('condition','')
                if "tags" in condition:
                    condition="New with Tags"
                elif "excellent" in condition:
                    condition="Like-New"
                elif "gently" in condition:
                    condition="Gently Used"
                    
                    
            entity_pattern=record.get('pattern','')
            if entity_pattern=='':
                patterns=["Animal Print","Argyle","Camo","Checkered & Gingham","Chevron & Herringbone","Color Block","Floral","Graphic","Hearts","Houndstooth","Metallic","Paisley","Plaid","Polka Dots","Print","Solid","Stars","Stripes","Tie-dye","Tropical","Tweed"]
                for pat in patterns:
                    pattern_re=re.compile(r"%s"%pat,re.IGNORECASE)
                    pattern_arr=re.findall(pattern_re,description)
                    if pattern_arr!=[]:
                        entity_pattern+=pattern_arr[0]+", "
                description_words=re.findall(r"\w+",description)
                matches=[]
                for pat in patterns:
                    if difflib.get_close_matches(pat,description_words,cutoff=0.7)!=[]:
                        matches.append(difflib.get_close_matches(pat,description_words,cutoff=0.7))
                        for match in matches:
                            if difflib.get_close_matches(match[0],patterns,cutoff=0.7)!=[]:
                                if not re.search(r'%s'%difflib.get_close_matches(match[0],patterns,cutoff=0.7)[0], entity_pattern, re.IGNORECASE):
                                    entity_pattern+=difflib.get_close_matches(match[0],patterns,cutoff=0.7)[0]+", "
                entity_pattern=entity_pattern[:-2]



        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "system_category": "Tunic Tops for Women",
                "category":record.get('category',''),
                "brand":record.get('brand',''),
                "image":record.get("image",''),
                "sku":record.get('sku',''),
                "description": description,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("est_reatil","").strip('$').replace(',','')
                },
                "color":record.get('color',''),
                "sleeve_length":record.get("sleeve_length",''),
                "length":length,
                "chest_width":chest_width,
                "material":material,
                "neckline":neckline,
                "size":size,
                "item_condition":condition,
                "pattern":entity_pattern.title()
               
                }

            return transformed_record
        else:
            return None
