import re
import hashlib
from colour import Color
import difflib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            materials=record.get('material','')
            materials=re.findall(r"\d+\.*\d*%\s*[a-zA-Z]+",materials)
            material=''
            for mate in materials:
                mate=re.sub(r"\d+\.*\d*%\s*",'',mate)
                material+=mate+", "
            material=material[:-2]
            measurements=record.get('measurements','')



            material_meta=["Acrylic","Cashmere","Cotton","Faux Leather","Leather","Linen","Nylon","Polyester","Rayon","Silk","Spandex","Wool"]
            if material=='':
                material=[]
                for word in material_meta:
                    pattern=re.compile(r"\b{}\b".format(word),re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        material.append(word)

                material=', '.join(material)
            color=record.get('color','')
            if color=='':
                color=[]
                words_description=re.findall(r"\w+",description)
                _color = [i for i in words_description if self.check_color(i)]
                if _color!=[]:
                    color.append(_color[0])
                    
                    
                if color==[]:
                    colors=['Teal','Burgundy']
                    for word in colors:
                        if re.findall(re.compile(r"{}".format(word),re.IGNORECASE),description)!=[]:
                            color.append(word)
                
                color=', '.join(color)
           
           
          
            pattern=re.compile(r"[\w\s\(\)]*Sizing chart",re.IGNORECASE)
            size=re.findall(pattern,measurements)
            if size!=[]:
                pattern=re.compile(r"\s*Sizing chart*|size",re.IGNORECASE)
                size=re.sub(pattern,'',size[0])
            else:
                size=''
            size=' '.join(size.split()).title()

            pattern=re.compile(r"[\d\.\"]*\s*length",re.IGNORECASE)
            length=re.findall(pattern,measurements)
            if length!=[]:
                pattern=re.compile(r"\s*length",re.IGNORECASE)
                length=re.sub(pattern,'',length[0])
            else:
                length=''

            pattern=re.compile(r"[\d\.\"]*\s*Chest",re.IGNORECASE)
            chest_width=re.findall(pattern,measurements)
            if chest_width!=[]:
                pattern=re.compile(r"\s*Chest",re.IGNORECASE)
                chest_width=re.sub(pattern,'',chest_width[0])
            else:
                chest_width=''


            entity_pattern=record.get('pattern','')
            if entity_pattern=='':
                patterns=["Animal Print","Argyle","Camo","Checkered & Gingham","Chevron & Herringbone","Color Block","Floral","Graphic","Hearts","Houndstooth","Metallic","Paisley","Plaid","Polka Dots","Print","Solid","Stars","Stripes","Tie-dye","Tropical","Tweed"]
                for pat in patterns:
                    pattern_re=re.compile(r"%s"%pat,re.IGNORECASE)
                    pattern_arr=re.findall(pattern_re,description)
                    if pattern_arr!=[]:
                        entity_pattern+=pattern_arr[0]+", "
                description_words=re.findall(r"\w+",description)
                matches=[]
                for pat in patterns:
                    if difflib.get_close_matches(pat,description_words,cutoff=0.7)!=[]:
                        matches.append(difflib.get_close_matches(pat,description_words,cutoff=0.7))
                        for match in matches:
                            if difflib.get_close_matches(match[0],patterns,cutoff=0.7)!=[]:
                                if not re.search(r'%s'%difflib.get_close_matches(match[0],patterns,cutoff=0.7)[0], entity_pattern, re.IGNORECASE):
                                    entity_pattern+=difflib.get_close_matches(match[0],patterns,cutoff=0.7)[0]+", "
                entity_pattern=entity_pattern[:-2]

            neckline=record.get("neckline",'')
            necklines=["Crew","V-","Scoop","Cowl","Boat","Off The Shoulder","Cold Shoulder","Mock","Square","Keyhole","One Shoulder","Henley","Halter"]
            neckline_meta=["Strapless","Sweetheart","One Shoulder","Off The Shoulder","Halter","V-Neck","Crew Neck","Scoop Neck","Boatneck","Plunge","Square","Cowl Neck","Keyhole","Mock","Cold Shoulder","Tie Neck","Turtleneck","Henley"]
            if neckline=='':
                neckline=[]
                for word in neckline_meta:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        neckline.append(word)
                for word in necklines:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,description)!=[]:
                        neckline.append(word)

                neckline=', '.join(neckline)
                
                if neckline not in necklines:
                    for neck in necklines:
                         if neck in neckline:
                             pos=neckline.find(neck)
                             neckline=neckline[pos:pos+len(neck)]

                   
                
            if neckline=="Crew" or neckline=="V-" or neckline=="Scoop" or neckline=="Cowl":
                        neckline+=" Neck"
            if neckline=="Boat":
                    neckline+="neck"

            neckline=re.sub(re.compile(r"V\- Neck|V\-Neck",re.IGNORECASE),"V Neck",neckline)

            

            condition=record.get('item_condition','')
            if condition=='':
                condition=record.get('condition','')
                if "tags" in condition:
                    condition="New with Tags"
                elif "excellent" in condition:
                    condition="Like-New"
                elif "gently" in condition:
                    condition="Gently Used"



        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "system_category": "Women's High Low Dresses",
                "category":record.get("category",''),
                "brand":record.get('brand',''),
                "image":record.get("image",''),
                "sku":record.get('sku',''),
                "description": description,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("est_reatil","").strip('$').replace(',','')
                },
                "color":color,
                "material":material,
                "size":size,
                "style":record.get("style",''),
                "item_condition":condition,
                "pattern":entity_pattern.title(),
                "accents":record.get('accents',''),
                "length":length,
                "neckline":neckline,
                "occasion":record.get("occasion",'').strip("Dresses"),
                "chest_width":chest_width
               
                }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False