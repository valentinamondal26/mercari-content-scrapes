import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            materials=record.get('material','')
            materials=re.findall(r"\d+\.*\d*%\s*[a-zA-Z]+",materials)
            material=''
            for mate in materials:
                mate=re.sub(r"\d+\.*\d*%\s*",'',mate)
                material+=mate+", "
            material=material[:-2]
            measurements=record.get('measurements','')
            inseam=record.get('inseam')
            if inseam=='':       
                measurement=re.findall(r"\d+\.*\d*\"\s*Inseam",measurements)
                if measurement!=[]:
                    inseam=measurement[0]
                    inseam=float(re.sub(r"\"\s*[a-zA-Z]+",'',inseam))
                    if inseam >= 13 and inseam<=19:
                        inseam='13-19"'
                    elif inseam > 19 and inseam<=23:
                        inseam='20-23"'
                    elif inseam >23 and inseam<=26:
                        inseam='24-26"'
                    elif inseam >26 and inseam<=29:
                        inseam='27-29"'
                    elif inseam >29 and inseam<=32:
                        inseam='30-32"'
                    elif inseam >32 and inseam<=34:
                        inseam='33-34"'
                    elif inseam >34 and inseam<=38:
                        inseam='35-38"'
            rise=record.get('rise','')
            if rise=='':
                pattern=re.compile(r"mid|high|low",re.IGNORECASE)
                rise=re.findall(pattern,description)
                if rise!=[]:
                    rise=rise[0]
                else:
                    rise=''
            
            if rise=='':
                pattern=re.compile(r"[\w|\W|\d]+\s*waist",re.IGNORECASE)
                if re.findall(pattern,description)!=[]:
                    rise=re.findall(pattern,description)[0]
                    pattern=re.compile(r"\s*waist",re.IGNORECASE)
                    rise=re.sub(pattern,'',rise)

            pattern=re.compile(r"\d+\.*\d*\"\s*waist",re.IGNORECASE)
            waist_size=re.findall(pattern,measurements)
            if waist_size!=[]:
                pattern=re.compile(r"\s*waist",re.IGNORECASE)
                waist_size=re.sub(pattern,'',waist_size[0])
            else:
                waist_size=''

            pattern=re.compile(r"[\w\s\(\)]*Sizing chart",re.IGNORECASE)
            size=re.findall(pattern,measurements)
            if size!=[]:
                pattern=re.compile(r"\s*Sizing chart*|size",re.IGNORECASE)
                size=re.sub(pattern,'',size[0])
            else:
                size=''
            size=' '.join(size.split()).title()
            

            condition=record.get('item_condition','')
            if condition=='':
                condition=record.get('condition','')
                if "tags" in condition:
                    condition="New with Tags"
                elif "excellent" in condition:
                    condition="Like-New"
                elif "gently" in condition:
                    condition="Gently Used"



        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": "Pants, tights, leggings",
                "brand":record.get('brand',''),
                "image":record.get("image",''),
                "sku":record.get('sku',''),
                "description": description,
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("est_reatil","").strip('$').replace(',','')
                },
                "color":record.get('color',''),
                "material":material,
                "product_type":record.get('product_type',''),
                "style":record.get('style',''),
                "cut_type":record.get('cut_type',''),
                "inseam":inseam,
                "rise":rise.title(),
                "waist_size":waist_size,
                "size":size,
                "item_condition":condition,
                "pattern":record.get('pattern',''),
                "accents":record.get('accents','')
               
                }

            return transformed_record
        else:
            return None
