import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title','')
            model = re.sub(r'[^\x00-\x7F]+',' ',model).strip()

            description = '. '.join([des.strip() for des in record.get('description','') if des.strip()])
            item_page_description = record.get('item_page_description',[])
            item_page_description = '. '.join([des.strip() for des in item_page_description if des.strip()])
            description =   description + ' '+item_page_description

            price = record.get('price','').replace('$','')
            price = re.sub(r'\.\d+','',price)

            transformed_record = {

                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statuscode',''),

                'brand': record.get('brand',''),
                'category': record.get('category',''),

                'title': record.get('title',''),
                'image': record.get('image',''),
                'description': description,

                'model': model,
                'product_type': record.get('product_type',''),
                'price': {
                    'currency_code':'USD',
                    'amount':price
                },
            }
            return transformed_record
        else:
            return None
