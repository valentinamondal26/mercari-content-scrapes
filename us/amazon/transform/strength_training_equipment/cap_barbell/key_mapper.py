class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''
        weight = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "weight" == entity['name']:
                if entity["attribute"]:
                    weight = entity["attribute"][0]["id"]

        key_field = model + color + weight
        return key_field
