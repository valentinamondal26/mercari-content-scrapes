import hashlib
import re
from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            brand = record.get('brand','')
            model = record.get('title','')

            price = record.get('price','').replace('-','').strip()

            description = '. '.join([des.strip() for des in record.get('description','') if des.strip()])
            color_data = record.get('color','')
            weight = ''
            if 'LB' in color_data:
                color_data = ''
                weight =  re.findall(r'(\d+)LB',record['color'])
                if weight:
                    weight = weight[0]+' lbs'

            if not weight:
                weight = record.get('item_display_weight','').replace('Pounds','lbs')


            def get_weight(weight_data):
                try:
                    weight = re.findall(re.compile(r'(\d+)\s*-*Pound',re.IGNORECASE),weight_data)[0].strip()
                    weight = weight +' lbs'
                except IndexError:
                    weight = ''
                return weight

            if not weight:
                weight = get_weight(color_data)

            if not weight:
                weight = get_weight(record.get('title',''))
                if not weight:
                    weight = re.findall(r'(\d+)\s*-*lbs*',record.get('title',''))
                    if weight:
                        if len(weight)==1:
                            weight = weight[0]+ ' lbs'
                        else:
                            weight = [we+' lbs' for we in weight if we]
                            weight = ', '.join(weight)
                    else:
                        weight = ''

            if not color_data:
                color_data = record.get("title", "")

            extra_colors = ['Silver Zinc', 'Zinc', 'Light Blue', 'Chrome']
            colors = []
            for cl in extra_colors:
                if re.findall(re.compile(r"\b{}\b".format(cl), re.IGNORECASE), color_data):
                    colors.append(cl)
                    color_data = re.sub(re.compile(r"\b{}\b".format(cl), re.IGNORECASE), "", color_data)

            colors += [i.strip() for i in re.findall(r"\w+", color_data)
                      if self.check_color(i.strip())]

            color = '/'.join(list(set(colors)))
            color = titlecase(color)

            # model = re.sub(r'[^\x00-\x7F]+',' ',model).strip()
            model = re.sub(re.compile(r', Multiple Color Options|, Options',re.IGNORECASE),'',model)
            model = re.sub(r'\||,','',model)
            model = re.sub(r'\((\d+)\sPounds\)','',model)
            model = re.sub(r'\d+-*Pounds*','',model)
            model = re.sub(r'\d+\s*lbs','',model)
            model = re.sub(color,'',model)
            # model = re.sub(re.compile(r'Chrome',re.IGNORECASE),'',model)
            for col in re.findall(r'\w+',color):
                model = re.sub(re.compile(r'{}'.format(col),re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'{}'.format(brand),re.IGNORECASE),'',model)
            model = re.sub(r'\(.*\)','',model)
            model = re.sub(r'\s+',' ',model).strip()
            if model.strip() == 'Olympic Trap Bar Hex Bar Shrug Bar Deadlift Bar Available':
                model = 'Olympic Trap/Hex/Shrug/Deadlift Bar'

            model = re.sub(
                r'\bPair or Single Multiple Colors\b|\bAvailable\b|\bMultiple Colors\b|\bPair of 2 Heavy Dumbbells Choose Weight\b'
                r'|Training and Exercise Equipment-|\bColor Options\b', '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\bCAP\b|\bChoose Weight\b|\b702556301555 by\b|\bby\b$',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'\bw/',re.IGNORECASE),'with',model)
            model = re.sub(r'/$','',model).strip()
            model = re.sub(r'\s+',' ',model).strip()
            model = re.sub(re.compile(r'Workout Bar Weighted Workout Bar Exercise Bar',re.IGNORECASE),'Weighted Workout Bar',model)
            model = re.sub(r'\bfor Weightlifting and Power Lifting Various Specialty Bars\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'(\bIdeal for\b.*)', '', model, flags=re.IGNORECASE)

            models = []
            if weight:
                weights = weight.split(', ')
                for w in weights:
                    models.append(f'{model} {w}')
                model = ' , '.join(models)
            
            if re.findall(r'Set\s+\d+\s+to\s+\d+\s+Pounds\s+\d+\s*lbs', model, flags=re.IGNORECASE):
                model = re.sub(r'\d+\s*lbs', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+',' ',model).strip()

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statuscode',''),

                'brand': record.get('brand',''),
                'category': record.get('category',''),

                'title': record.get('title',''),
                'image': record.get('image',''),
                'description': description,

                'model': model,
                'color': color,
                'weight': weight,
                'price': {
                    'currency_code': 'USD',
                    'amount': price.replace('$','')
                },
            }
            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
