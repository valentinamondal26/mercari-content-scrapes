'''
https://mercari.atlassian.net/browse/USDE-1614 - Bowflex Strength Training Equipment
https://mercari.atlassian.net/browse/USDE-1663 - CAP Barbell Strength Training Equipment
https://mercari.atlassian.net/browse/USCC-412 - Brahmin Shoulder Bags
https://mercari.atlassian.net/browse/USCC-425 - Beats by Dr. Dre Headphones
'''

import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import requests
import os
from urllib.parse import urlparse, parse_qs, urlencode
import re
from common.selenium_middleware.http import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd

class AmazonSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from amazon.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Strength Training Equipment
        2) Shoulder Bags
        3) Headphones

    brand: str
        1) Bowflex
        2) CAP Barbell
        3) Brahmin
        4) Beats by Dr. Dre

    Command e.g:
    scrapy crawl amazon -a category="Strength Training Equipment" -a brand='Bowflex'
    scrapy crawl amazon -a category="Strength Training Equipment" -a brand='CAP Barbell'
    scrapy crawl amazon -a category="Shoulder Bags" -a brand='Brahmin'
    scrapy crawl amazon -a category="Headphones" -a brand='Beats by Dr. Dre'
    """

    name = "amazon"

    source = 'amazon.com'
    blob_name = 'amazon.txt'

    merge_key = 'id'

    category = None
    brand = None

    base_url = 'https://www.amazon.com'

    url_dict = {
        'Strength Training Equipment': {
            'CAP Barbell': {
                'url': 'https://www.amazon.com/stores/page/9FB958C4-EDFC-426E-8000-9DA449A61463?ingress=0&visitId=399ee5f6-2ba8-4b45-9fc0-992fc79b9189&productGridPageIndex=2',
            },
            'Bowflex': {
                'url': 'https://www.amazon.com/stores/page/7C9CA3D9-539E-4DE2-89B4-B06B6BF4C782?ingress=2&visitId=cad5b6ce-af30-4b6b-bb86-7234371ccf7c&ref_=ast_bln',
                'product_types': ['Max Trainers', 'SelectTech', 'Treadmill', 'Home Gym', 'Ellipticals', 'TreadClimbers'] ##make sure of  whitespaces are correctly given in product_type
            }
        },
        'Shoulder Bags': {
            'Brahmin': {
                'url': 'https://www.amazon.com/s?k=brahmin+handbags&i=fashion-womens-handbags&bbn=15743631&rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A15743631%2Cn%3A3421075011%2Cp_89%3ABRAHMIN&dc&qid=1603228790&rnid=15743631&ref=sr_nr_n_5',
                'parse_listing_page': True,
            }
        },
        'Headphones': {
            'Beats by Dr. Dre': {
                'url': 'https://www.amazon.com/stores/page/395474F7-47DA-4331-929B-3ED7C7DC2C43?ingress=2&visitId=8fab7a89-713b-4c3a-bcf6-bb8266b63b59&ref_=ast_bln',
                'product_types': [
                    'Solo Pro', 'Studio3 Wireless', 'Solo3 Wireless', 'Beats EP',
                    # 'Noise Cancelling Headphones',
                    'Powerbeats Pro', 'Powerbeats', 'Beats Flex', 'urBeats',
                    # 'Fitness Earphones',
                ]
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    listing_page_api = 'https://www.amazon.com/juvec'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        super(AmazonSpider,self).__init__(*args, **kwargs)

        if not category and not brand:
            self.logger.error("Category should not be None")
            raise CloseSpider('category  not found')

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.product_types = self.url_dict.get(category, {}).get(brand, {}).get('product_types', [])
        self.parse_listing_page = self.url_dict.get(category, {}).get(brand, {}).get('parse_listing_page', False)
        self.category = category
        self.brand = brand
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{}'.format(category))
            raise CloseSpider('launch url  not found')

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def contracts_mock_function(self):
        self.product_types = ['Max Trainers', 'SelectTech', 'Treadmill', 'Home Gym', 'Ellipticals', 'TreadClimbers']
        url = 'https://www.amazon.com/stores/page/9FB958C4-EDFC-426E-8000-9DA449A61463?ingress=0&visitId=399ee5f6-2ba8-4b45-9fc0-992fc79b9189&productGridPageIndex=2'
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'}
        response = requests.get(url=url, headers=headers)
        self.request_payload = self.get_request_payload(response)


    def start_requests(self):
        if self.product_types:
            yield self.create_get_request(url=self.launch_url, callback=self.get_product_type_urls)
        elif self.parse_listing_page:
            yield self.create_get_request(url=self.launch_url, callback=self.parse)
        else:
            yield self.create_get_request(url=self.launch_url, callback=self.parse_listing_page_url)


    def parse(self, response):
        """
        @url https://www.amazon.com/s?k=brahmin+handbags&i=fashion-womens-handbags&bbn=15743631&rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A15743631%2Cn%3A3421075011%2Cp_89%3ABRAHMIN&dc&qid=1603228790&rnid=15743631&ref=sr_nr_n_5
        @returns requests 49
        @meta {"use_proxy": "True"}
        """
        for product in response.xpath('//span[@data-component-type="s-search-results"]/div[@class="s-main-slot s-result-list s-search-results sg-row"]/div[@data-component-type="s-search-result"]'):
            url = product.xpath('.//a/@href').extract_first()
            if not url.startswith(self.base_url):
                url = self.base_url + url
            yield self.create_get_request(url=url, callback=self.item_page_crawldetails, meta={'crawl_variants':True})

        next_page_url = response.xpath('//ul[@class="a-pagination"]/li[@class="a-last"]/a/@href').extract_first()
        if next_page_url:
            if not next_page_url.startswith(self.base_url):
                next_page_url = self.base_url + next_page_url
            yield self.create_get_request(url=next_page_url, callback=self.parse)


    def get_product_type_urls(self, response):
        """
        @url https://www.amazon.com/stores/page/7C9CA3D9-539E-4DE2-89B4-B06B6BF4C782?ingress=2&visitId=cad5b6ce-af30-4b6b-bb86-7234371ccf7c&ref_=ast_bln
        @returns requests 6
        @meta {"use_proxy": "True"}
        """
        ##Number of request mentioned is contracts is 6 because total number of request generated will always be equal to the total number of product_types in this case.
        
        item_page_url_wait_until = EC.visibility_of_element_located((By.XPATH,"//div[@class='style__tagline__3aa7g']"))
        # item_page_url_wait_until = EC.visibility_of_element_located((By.XPATH, "//div[@class='style__row__3wS8d double']/div[@class='style__half__3vpnl']/div[@class='style__tile__OEwwB style__small__3Atts style__product__2RTVJ style__grouped__WPoUF']"))
        for product_type in self.product_types:
            product_url = response.xpath("//span[@class='style__linkText__OI_YN'][contains(text(),'"+product_type+"')]/../@href").get(default='')
            if self.base_url not in product_url:
               product_url = self.base_url+product_url
            meta ={'product_type':product_type}
            yield self.createDynamicRequest(url=product_url, callback=self.parse_product_types_page_url, wait_time=10, wait_until=item_page_url_wait_until,meta=meta)


    def parse_product_types_page_url(self, response):
        """
        @url https://www.amazon.com/stores/page/63038805-DD89-4A8A-888F-26878C4B525C?ingress=2&visitId=817e92c9-0caa-41b0-8b71-a6b90c552df1&ref_=ast_bln
        @meta {"use_proxy": "True"}
        @returns requests 1
        @scrape_values meta.item.title meta.item.description meta.item.price meta.item.image 
        """
        meta = response.meta
        product_type = meta.get('product_type','')
        for res in response.xpath("//div[@class='style__row__3wS8d double']/div[@class='style__half__3vpnl']/div[@class='style__tile__OEwwB style__small__3Atts style__product__2RTVJ style__grouped__WPoUF']") \
            or response.xpath('//div[@data-widgettype="ProductGrid"]/div[contains(@id,"ProductGrid-")]//ul/li'):
            item_data = {
                'category': self.category,
                'brand': self.brand,
                'statuscode': str(response.status),
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),

                'id': "https://www.amazon.com"+res.xpath(".//a/@href").get(default=''),
                'title': res.xpath(".//div/div/div/h2/text()").get(default=''),
                'description': res.xpath(".//div/div/div/h3/text()").getall(),
                'price':res.xpath(".//div/div/div/div/div/div/span/span/@aria-label").get(default=''),
                'image': res.xpath(".//div/div/div/div/div[2]/img/@src").get(default=''),
                'product_type': product_type,
                'response_url':response.url
            }
            meta = {'item': item_data}
            # yield self.create_get_request(url=item_data['id'], callback=self.crawl_items, meta=meta)
            yield self.create_get_request(url=item_data['id'], callback=self.item_page_crawldetails, meta=meta)


    def crawl_items(self, response):
        """
        @url https://www.amazon.com/Bowflex-M3-Max-Trainer-Black/dp/B085N9LTWF?ref_=ast_sto_dp
        @meta {"use_proxy": "True"}
        @scrape_values item_page_description style_name
        """
        meta = response.meta
        item = meta.get('item',{})
        item_page_data = {
            'item_page_description': response.xpath("//div[@id='feature-bullets']/ul/li/span/text()").getall(),
            'style_name': response.xpath("//div[@id='variation_style_name']/div/span/text()").get(default='')
        }
        item.update(item_page_data)
        yield item


    def parse_listing_page_url(self, response):
        """
        @url https://www.amazon.com/stores/page/9FB958C4-EDFC-426E-8000-9DA449A61463?ingress=0&visitId=399ee5f6-2ba8-4b45-9fc0-992fc79b9189&productGridPageIndex=2
        @meta {"use_proxy": "True"}
        @returns requests 1
        """
        self.request_payload = self.get_request_payload(response)
        yield self.create_post_request(url= self.listing_page_api, callback=self.parse_listing_page_api)


    def parse_listing_page_api(self, response):
        """
        @url https://www.amazon.com/juvec
        @meta {"use_proxy": "True", "request_method": "POST"}
        @headers {}
        @body {}
        @returns requests 1
        """
        ##Headers and body part for the contracts request url will be added dynamically while running the contracts. 
        # Body and Headers are added in the mock config json and contracts will add it while running the contracts.

        response_body = json.loads(response.text)
        for product in response_body.get('products',[]):
            item_url = product.get('links',{}).get('viewOnAmazon',{}).get('url','')
            meta = {'crawl_variants':True}
            if  self.base_url not in item_url:
                item_url  = self.base_url+item_url
            yield self.create_get_request(url=item_url, callback=self.item_page_crawldetails, meta=meta)


    def item_page_crawldetails(self, response):
        """
        @url https://www.amazon.com/CAP-Barbell-SDK2B-035-Kettlebell-Black/dp/B00WHIGJPI?ref_=ast_sto_dp
        @scrape_values title id price color description item_display_weight image
        @returns requests 1
        @meta {"use_proxy": "True"}
        """
        specs = {}
        for tr in response.xpath('//div[@data-feature-name="productOverview"]//table/tr'):
            data = tr.xpath('./td/span/text()').extract()
            if len(data) == 2:
                specs[data[0]] = data[1]

        product_technical_details = {}
        try:
            product_technical_details = pd.read_html(response.xpath('//table[@id="productDetails_techSpec_section_1"]').getall()[0])[0].set_index(0).to_dict(orient='dict').get(1)
        except:
            pass

        item = {
            'id': response.url,
            'statuscode': str(response.status),
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'category': self.category,
            'brand': self.brand,

            'title': response.xpath(u'normalize-space(//span[@id="productTitle"]/text())').extract_first(default=''),
            'list_price': response.xpath(u'normalize-space(//span[@class="priceBlockStrikePriceString a-text-strike"]/text())').extract_first(),
            'price': response.xpath(u"normalize-space(//span[@class='a-size-base a-color-price']/text())").get(default='') \
                or response.xpath(u"normalize-space(//span[@id='priceblock_ourprice']/text())").get(default='') \
                or response.xpath(u"normalize-space(//span[@id='priceblock_dealprice']/text())").get(default=''),
            'color':  response.xpath(u"normalize-space(//div[@id='variation_color_name']/div/span/text())").get(default='') \
                or response.xpath(u"normalize-space(//div[@id='variation_color_name']/div//span[@class='selection']/text())").get(default=''),
            'description': response.xpath('//div[@id="feature-bullets"]/ul/li').xpath(u'normalize-space(.//text())').extract(),
            'item_display_weight': response.xpath("//span[@id='dropdown_selected_item_display_weight']/span/span/span/text()").get(default=''),
            'image': response.xpath("//div[@id='imgTagWrapperId']/img/@data-old-hires").get(default='') or \
                list(json.loads(response.xpath("//div[@id='imgTagWrapperId']/img/@data-a-dynamic-image").get(default='{"":""}')).keys())[0],
            'breadcrumb': ' | '.join(response.xpath(u'//div[@id="wayfinding-breadcrumbs_feature_div"]/ul/li/span/a').xpath('normalize-space(./text())').extract()),
            'product_description': response.xpath('//div[@id="productDescription_feature_div"]/div[@id="productDescription"]').xpath('.//text()[normalize-space(.)]').extract(),
            'product_details': response.xpath('//div[@id="detailBullets"]//div[@id="detailBullets_feature_div"]/ul/li//text()[normalize-space(.)]').extract(),
            'product_technical_details': product_technical_details,
            'specs': specs,

            'item_page_description': response.xpath("//div[@id='feature-bullets']/ul/li/span/text()").getall(),
            'style_name': response.xpath("//div[@id='variation_style_name']/div/span/text()").get(default=''),
        }

        meta_item_info = response.meta.get('item', {})
        for key, value in meta_item_info.items():
            if not item.get(key, ''):
                item[key] = value
        yield item

        if response.meta.get('crawl_variants', ''):
            variant_urls = response.xpath("//ul[@class='a-unordered-list a-nostyle a-button-list a-declarative a-button-toggle-group a-horizontal a-spacing-top-micro swatches swatchesSquare imageSwatches']/li/@data-dp-url").getall()
            base_url = re.sub(r'/dp.*', '', response.url)
            for variant in variant_urls:
                if variant:
                    url = base_url + variant
                    yield self.create_get_request(url=url, callback=self.item_page_crawldetails, meta=response.meta)


    def get_request_payload(self, response):
        payload = re.findall(r"\"pageContext\":.*}};\n\s+ReactDOM",response.text)[0]
        payload = "{"+re.sub(r';\n\s+ReactDOM','',payload)
        payload = json.loads(payload)
        payload.update({'includeOutOfStock': 'true', 'productGridType': 'ma', 'endpoint': 'ajax-data'})
        asin_list =  re.findall(r'\"ASINList\":(\[.*\"\]),\"include',response.text)[0].replace('[','').replace(']','').replace('"','').split(",")
        payload.update({'ASINList':asin_list})
        payload = json.dumps(payload)
        return payload


    def create_post_request(self, url, callback, meta=None):
        headers = {'content-type':' application/json; charset=UTF-8'}
        request = scrapy.Request(url=url, method='POST', body=self.request_payload,
            callback=callback, meta=meta, headers=headers)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request


    def create_get_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request


    def createDynamicRequest(self, url, callback,  wait_time=None, wait_until=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, wait_time=wait_time, wait_until=wait_until, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request
