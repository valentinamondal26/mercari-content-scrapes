import time
import hashlib

class Mapper(object):

    def get_GTIN(self, gtin):
        itemIds = {'PARENT_ASIN': [''], 'UPC': [''], 'MPN': [''], 'ASIN': [''], 'EAN': gtin}
        return itemIds

    def get_item_dimensions(self, features):
        item_dimensions = {}

        for feature in features['Features']:
            feature_name = feature['Feature']['Name']['Value']
            if feature_name is not None and feature_name in ['Weight', 'Height', 'Width', 'Depth']:
                if feature_name == 'Depth':
                    feature_name = 'length'
                feature_name = feature_name.lower()
                item_dimensions[feature_name] = {"value": feature['RawValue'],
                                                 "unit": feature['Feature']['Measure']['Signs']['_']}

        return item_dimensions

    def get_price(self):
        price_json = {}
        price_json["currencyCode"] = "USD"
        price_json["amount"] = "TBD - From Algopix"
        return price_json

    def map(self, record):
        if record:
            data = record['data']
            title = data['GeneralInfo']['Title']
            id = data['GeneralInfo']['IcecatId']
            brand = data['GeneralInfo']['Brand']
            model = data['GeneralInfo']['ProductName']
            desc = ''

            if data['GeneralInfo']['Description']:
                desc = data['GeneralInfo']['Description']['LongDesc']
            image = ''

            if data['Image']:
                if 'HighPic' in data['Image']:
                    image = data['Image']['HighPic']

            category = data['GeneralInfo']['Category']['Name']['Value']
            itemIds = self.get_GTIN(data['GeneralInfo']['GTIN'])

            feature_groups = data['FeaturesGroups']
            entities = []
            item_dimensions = {}
            for feature_group in feature_groups:
                if feature_group['FeatureGroup']['ID'] == "14":
                    item_dimensions = self.get_item_dimensions(feature_group)
                else:
                    entities.append(feature_group)

            attributes = {
                'TBD': 'TBD'
            }

            price = self.get_price()

            app_key = ''
            shop_name = ''

            at_id = "https://live.icecat.biz/api/?shopname={shop_name}&lang=us&content=&app_key={app_key}&icecat_id={icecat_id}".format(shop_name=shop_name, app_key=app_key, icecat_id=id)
            b = at_id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                '@id': at_id,
                'crawlDate': time.strftime('%Y-%m-%d %H:%M:%S'),
                'item_id': hex_dig,
                'statusCode': "200",
                'name': title,
                'category': category,
                'brand': brand,
                'model': model,
                'description': desc,
                'image': image,
                'itemIds': itemIds,
                'itemDimensions': item_dimensions,
                'featureGroups': entities,
                'attributes': attributes,
                'price': price,
                'rating': ""
            }

            return transformed_record
        else:
            return dict()
