class Mapper(object):
    def __init__(self):
        return

    def map(self, record, mapped_obj):

        del record['featureGroups']
        record['entities'] = mapped_obj

        return record
