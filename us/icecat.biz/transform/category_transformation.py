import json

class Mapper(object):

    def map(self, input_df):

        id = input_df['@id']
        category = input_df['category']
        icecat_id = id.split('=')[-1] if id else ""
        return json.dumps({'icecat_id':icecat_id, 'category': category})
