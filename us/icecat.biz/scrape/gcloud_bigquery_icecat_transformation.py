# -*- coding: utf-8 -*-
"""
@author arunedwin
@date 2018-jan-31

This is a script file reads data from google cloud storage and transforms the data to
a flat json file. the faltten json file is upload for big query.

pending items:
 - logging  done
 - parmeterized functions  done
 - embeding the params
 - implement arg parse
 
"""

import pandas as pd
import gcsfs
import json
from google.cloud import storage
from google.cloud import bigquery
import datetime
import time
import logging
import numpy as np


    
    
# get current timestamp.  
def get_current_time():
    return datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S")

def log_setup():
    log_handler = logging.FileHandler('/tmp/icecat_{}.log'.format(get_current_time()))
    formatter = logging.Formatter(
        '%(asctime)s program_name [%(process)d]: %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

log_setup()


# read the data from gcs and store it to local filesystem.
def save_source_file_to_local_path(project_id, gcs_file_source_path, local_file_path='output.txt'):
    logging.info("Saving source file to the local directory path.")
    fs = gcsfs.GCSFileSystem(project=project_id)
    with fs.open(gcs_file_source_path, 'rb') as f:
        logging.info("Reading the file from the gcloud storage and decoding the for UTF-8.")
        txt = f.read()
        txt = txt.decode('utf-8', "ignore")
    with open(local_file_path, "w") as fout:
        fout.write(txt)
     

# Sample file to test the scipt.
def read_sample_file(sample_file_source_path):
    logging.info("Loading the sample data from local file path.")
    prod_df = pd.read_table(sample_file_source_path, encoding = 'utf8')
    clean_prod_df = prod_df[prod_df.columns.drop(list(prod_df.filter(regex='Unnamed:')))]  
    return clean_prod_df.to_json(orient = 'records')
    

# This function reads the data from gs , remove un-named columns and creates a dataframe.
def read_data_from_gcs(project_id, gcs_file_source_path,local_file_path='output.txt'):
    logging.info("Loading data from gcs project: {}, decode and create a dataframe.".format(project_id))
    fs = gcsfs.GCSFileSystem(project=project_id)
    with fs.open(gcs_file_source_path, 'rb') as f:
        logging.info("Reading file...")
        txt_file = f.read()
        logging.info("Start decoding...")
        txt_file = txt_file.decode('utf-8', "ignore")
        logging.info("Decoding completed successfully..")
    with open(local_file_path, "w") as fout:
        fout.write(txt_file)
    logging.info("Loading prod_id data into prod_df dataframe.")    
    prod_df = pd.read_table(local_file_path, encoding = 'utf8')
    clean_prod_df = prod_df[prod_df.columns.drop(list(prod_df.filter(regex='Unnamed:')))]  
    logging.info("loading process completed sucessfully.")
    return clean_prod_df.to_json(orient = 'records')


# write the flatten json into the gcloud.
def write_data_to_gcs(gsc_file_target_path, project_id, json_string ):
    logging.info("Write data to gcs.")
    fs = gcsfs.GCSFileSystem(project=project_id)
    with fs.open(gsc_file_target_path, 'wb') as f:
        byte_file = json_string.encode('utf-8')
        f.write(byte_file)
        logging.info("Successfully loaded data into gcs.")
    fs.du(gsc_file_target_path)


# create a new database_key if it is not there already.
def create_new_bq_dataset(dataset_key='icecat'):
    logging.info("Creating a database_id.")
    client = bigquery.Client()
    # Create a DatasetReference using a chosen dataset ID.
    # The project defaults to the Client's project if not specified.
    dataset_ref = client.dataset(dataset_key)
    # Construct a full Dataset object to send to the API.
    dataset = bigquery.Dataset(dataset_ref)
    # Specify the geographic location where the dataset should reside.
    dataset.location = "US"
    dataset = client.create_dataset(dataset)  # API request
    print (type(dataset))
    
    
# load the dataframe into bigquery.
def load_data_into_bigquery(transformed_json, dataset_id='icecat', table_name='prod_table'):
    logging.info("Load data into bigquery.")
    client = bigquery.Client()
    dataset_ref = client.dataset(dataset_id)
    table_ref = dataset_ref.table(table_name)
    load_df=pd.DataFrame(transformed_json)
    logging.info("Starting to load data into bigquery.")
    job = client.load_table_from_dataframe(clean_prod_df, table_ref, location="US",)
    job.result()  # Waits for table load to complete. 
    assert job.state == "DONE"
    logging.info("Loading data into bigquery table was successful.")
    return job.state

    


# main function orchestrat btw the functions.
def main():
    
    logging.info('Initializing the variables...')
    source_file_name='prodid_d'
    target_file_name='prodid_d'
    project_id='mercari-us-imaginea-dev'
    gcs_file_source_path='imaginea_content_us/{}.txt'.format(source_file_name)
    local_file_path='/Users/arunedwin/work/mercari/git/mercari-content-scrapes/us/icecat.biz/scrape/output.txt'
    gsc_file_target_path='content_us/raw/icecat.biz/json/{}/{}.json'.format(get_current_time(),target_file_name)
    
    # read the flat file from gcs.
    transformed_json=read_data_from_gcs(project_id, gcs_file_source_path)
    
    # load the transformed json file into gcs.
    #write_data_to_gcs(gsc_file_target_path, project_id, transformed_json )
    
    # load the transformed data into bigquery.
    load_data_into_bigquery(project_id, transformed_json,'icecat','prod_table')
    logging.info("Main function exited sucessfully.")
    
#    json_tranformed = read_sample_file()
#    with open("transformed_json.json", "w") as fout:
#        fout.write(json_tranformed)
#    #json_tranformed = read_data_from_gcs()
#    write_data_to_gcs(json_tranformed)
   
    
# load the dataframe into bigquery.
def load_data_into_bigquery_as_dataframe(project_id, transformed_dataframe, dataset_id='icecat', table_name='prod_table'):
    logging.info("Load data into bigquery.")
    transformed_dataframe = transformed_dataframe.applymap(str)
    transformed_dataframe.to_gbq(project_id='mercari-us-imaginea-dev',destination_table='icecat.prod_table', if_exists='replace')
    
    
# Sample file to test the scipt.
def read_sample_file_dataframe(sample_file_source_path):
    logging.info("Loading the sample data from local file path.")
    prod_df = pd.read_table(sample_file_source_path, encoding = 'utf8')
    prod_df.columns = prod_df.columns.str.replace(' ', '_')
    prod_df = prod_df.replace(np.nan, '', regex=True)
    clean_prod_df = prod_df[prod_df.columns.drop(list(prod_df.filter(regex='Unnamed:')))]  
    logging.info("print the type of clean_prod_df: {}".format(type(clean_prod_df)))
    return clean_prod_df

def load_data_into_bigquery():
    
    logging.info('Initializing the variables...')
    
    source_file_name='prodid_d'
    target_file_name='prodid_d'
    project_id='mercari-us-imaginea-dev'
    gcs_file_source_path='imaginea_content_us/{}.txt'.format(source_file_name)
    local_file_path='/Users/arunedwin/work/mercari/git/mercari-content-scrapes/us/icecat.biz/scrape/output.txt'
    gsc_file_target_path='content_us/raw/icecat.biz/json/{}/{}.json'.format(get_current_time(),target_file_name)
    #transformed_df = read_sample_file_return_dataframe(project_id, gcs_file_source_path)
    transformed_df = read_sample_file_dataframe("/Users/arunedwin/work/mercari/git/mercari-content-scrapes/us/icecat.biz/scrape/output.txt")
    
    load_data_into_bigquery_as_dataframe(project_id, transformed_df,'icecat','prod_table')
    logging.info("load_data_into_bigquery: function exited sucessfully.")

if __name__ == '__main__':
    #main()
    load_data_into_bigquery()