import os
import requests
from requests.auth import HTTPBasicAuth
import argparse
import traceback

from datetime import datetime
from google.cloud import storage

import pandas as pd

import datetime as dt
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

BASE_URL = 'https://data.icecat.biz/export/level4/US/{}'
GCS_BLOB_PREFIX = 'raw/{}/json/{}/{}'
BUCKET_NAME = 'imaginea_content_us'
app_key = ''
shop_name = ''


def download_file(url):
    local_filename = url.split('/')[-1]
    # get from metadata
    user = ''
    secret = ''
    with requests.get(url, auth=HTTPBasicAuth(user, secret), stream=True) as r:
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
    return local_filename


def create_seed_urls(file_name="daily.export_urls.txt.gz"):
    dataset = pd.read_csv(file_name, delimiter="\t", compression='gzip', error_bad_lines=False, usecols=['product_id'])
    data_url = 'https://live.icecat.biz/api/?shopname={shop_name}&lang=us&content=&app_key={app_key}&icecat_id={icecat_id}'
    seed_url_file = None
    with open('seed_urls.txt', 'w') as f:
        seed_url_file = f.name
        for prod_id in dataset.product_id:
            seed_url = data_url.format(shop_name=shop_name, app_key=app_key, icecat_id=prod_id)
            f.write(seed_url + os.linesep)
    return seed_url_file


def extract_urls(file_name='daily.export_urls.txt.gz'):
    global BASE_URL
    url = BASE_URL.format(file_name)
    local_filename = download_file(url)
    seed_url_file = create_seed_urls(local_filename)

    return seed_url_file

def load_to_gcs(seed_file):
    global GCS_BLOB_PREFIX, BUCKET_NAME

    client = storage.Client()
    bucket = client.get_bucket(BUCKET_NAME)

    source = 'icecat.biz'
    timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
    destination_blob_name = GCS_BLOB_PREFIX.format(source, timestamp, seed_file)

    blob = bucket.blob(destination_blob_name)

    with open(seed_file, 'r') as f:
        blob.upload_from_file(f)
        print('File uploaded to {}.'.format(destination_blob_name))


def orchestrate():
    try:
        # Extract the urls from source file
        seed_file = extract_urls()

        #load the seed file to gcs
        load_to_gcs(seed_file)

    except Exception as err:
        print(err)
        traceback.print_exc()

###################################################################
### DAG Configuration
###################################################################

default_args = {
    'owner': 'airflow',
    'start_date': dt.datetime(2019, 3, 7, 00, 00, 00),
    'concurrency': 1,
    'retries': 0
}
with DAG('icecat_daily_extraction_job', catchup = False, default_args = default_args, schedule_interval = '30 13 * * *',) as dag:
    icecat_extraction_dag = PythonOperator(task_id='orchestrate', python_callable=orchestrate)

icecat_extraction_dag

###################################################################
### Program entry
###################################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename', required=False, help='Name of the gzip file to extract from export directory')
    args = parser.parse_args()

    print("Starting Job with : ")
    print(args)

    try:
        # Extract the urls from source file
        seed_file = extract_urls() if args.filename is None else extract_urls(args.filename)

        #load the seed file to gcs
        load_to_gcs(seed_file)

    except Exception as err:
        print(err)
        traceback.print_exc()
