import requests


class Scraper:
    def scrape_func(self, record, auth, proxy):

        try:
            record = "https://us-central1-mercari-us-de.cloudfunctions.net/algopix?mode=algopix&query={}". \
                format(record.split(';')[0])
            with requests.get(record, auth=auth,
                              proxies={"http": proxy, "https": proxy}) as r:
                if r.status_code == requests.codes.ok:
                    return r.json()
                else:
                    print("FAILED Algopix scrape func: Status_code {0} - {1} - {2}".format(r.status_code, r.text, record))
                    return None
        except Exception as e:
            print("Algopix Scraper Exception Raised for {} with exception {}".format(record, str(e)))  # the exception instance
            return None
