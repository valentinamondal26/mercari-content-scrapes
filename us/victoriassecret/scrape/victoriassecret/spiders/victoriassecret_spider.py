'''
https://mercari.atlassian.net/browse/USDE-1084 - VS PINK Backpacks
https://mercari.atlassian.net/browse/USDE-618 - PINK Women's Tracksuits & sweats
https://mercari.atlassian.net/browse/USCC-137 - Victoria's Secret Bikinis
https://mercari.atlassian.net/browse/USCC-338 - PINK Women's Bras & Bra Sets
https://mercari.atlassian.net/browse/USCC-178 - PINK Women's Shorts
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import json
import uuid
from urllib.parse import urlparse, parse_qs
import urllib
import os

from parsel import Selector

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from common.selenium_middleware.http import SeleniumRequest


class VictoriassecretSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from victoriassecret.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Tracksuits & sweats
        2) Pants, tights, leggings
        3) Women's Hoodies
        4) T-Shirts for Women
        5) Bikinis
        6) Women's Bras & Bra Sets
        7) Fragrances for Women
        8) Women's Panties
        9) Backpacks
        10) Women's Shorts

    brand : str
        brand to be crawled, Supports
        1) PINK
        2) Victoria's Secret

    Command e.g:
    scrapy crawl victoriassecret -a category="Fragrances for Women" -a brand="Victoria's Secret"
    scrapy crawl victoriassecret -a category="Backpacks" -a brand="PINK"
    scrapy crawl victoriassecret -a category="Women's Tracksuits & sweats" -a brand="PINK"
    scrapy crawl victoriassecret -a category="Bikinis" -a brand="Victoria's Secret"
    scrapy crawl victoriassecret -a category="Women's Bras & Bra Sets" -a brand="PINK"
    scrapy crawl victoriassecret -a category="Women's Shorts" -a brand="PINK"

    """

    name = 'victoriassecret'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'victoriassecret.com'
    blob_name = 'victoriassecret.txt'

    base_url = 'https://www.victoriassecret.com'
    api_base_url = 'https://api.victoriassecret.com/products/v9/page/{uuid}'

    api_listing_page = 'https://api.victoriassecret.com/stacks/v9/?brand={brand}&collectionId={collection_id}&filter={filter}&orderBy=REC&activeCountry=US'
    api_filter_page = 'https://api.victoriassecret.com/filters/v3/?brand={brand}&collectionId={collection_id}&filter={filter}&orderBy=REC&activeCountry=US'

    guuid = uuid.uuid4()

    url_dict = {
        "Women's Tracksuits & sweats": {
            'PINK': {
                'url': 'https://www.victoriassecret.com/us/pink/tops-and-bottoms/sweatpants',
                'filters': ['Style', 'Color'],
            }
        },
        "Pants, tights, leggings": {
            'PINK': {
                # TODO: url broken
                'url': 'https://www.victoriassecret.com/pink/bottoms/shop-all?filter=subclass%3AFull%20Length%20Pants%7CLeggings%20%26%20Tights&scroll=true',
                'filters': [],
            }
        },
        "Women's Hoodies": {
            'PINK': {
                # TODO: url broken
                'url': 'https://www.victoriassecret.com/pink/tops/hoodies',
                'filters': ['Style'],
            }
        },
        "T-Shirts for Women": {
            'PINK': {
                # TODO: url broken
                'url': 'https://www.victoriassecret.com/pink/tops/shop-all-tees?scroll=true',
                'filters': ['Style'],
            }
        },
        'Bikinis': {
            "Victoria's Secret": {
                'url': 'https://www.victoriassecret.com/swimwear/bikinis/shop-all?scroll=true&filter=subbrand%3AVictoria%27s%20Secret',
                'filters': ['Style'],
            }
        },
        "Women's Bras & Bra Sets": {
            "Victoria's Secret": {
                'url': 'https://www.victoriassecret.com/vs/bras/shop-by-size?scroll=true',
                'filters': ['Style', 'Collection', 'Lining Level'],
            },
            'PINK': {
                'url': 'https://www.victoriassecret.com/us/pink/bras/shop-all',
                'filters': ['Color', 'Style', 'Lining Level'],
            }
        },
        'Fragrances for Women': {
            "Victoria's Secret": {
                'url': 'https://www.victoriassecret.com/vs/beauty/all-fragrance?scroll=true&filter=class%3AFragrances',
                'filters': ['Style'],
            }
        },
        "Women's Panties": {
            "Victoria's Secret": {
                'url': 'https://www.victoriassecret.com/vs/panties/shop-all-panties',
                'filters': ['Style', 'Collection'],
            }
        },
        'Backpacks': {
            'PINK': {
                'url': 'https://www.victoriassecret.com/pink/accessories/backpacks',
                'filters': ['Style'],
            }
        },
        "Women's Shorts": {
            'PINK': {
                'url': 'https://www.victoriassecret.com/us/pink/tops-and-bottoms/shorts?scroll=true',
                'filters': ['Color', 'Style'],
            }
        }
    }

    filters = []

    query_filter = ''


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(VictoriassecretSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        o = urlparse(self.launch_url)
        d = parse_qs(o.query)
        if d:
            self.query_filter = "|".join(d.get('filter', []))
        print(f'==> self.query_filter: {self.query_filter}')
        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.launch_url = 'https://www.victoriassecret.com/us/pink/tops-and-bottoms/sweatpants'
        self.filters = ['Style', 'Color']
        o = urlparse(self.launch_url)
        d = parse_qs(o.query)
        self.query_filter = "|".join(d.get('filter', [])) if d else ''


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.createDynamicRequest(
                url=self.launch_url,
                callback=self.parse,
                wait_time=30,
                wait_until=EC.element_to_be_clickable((By.CLASS_NAME, 'fabric-grid-component-item')))


    def crawlDetailApi(self, response):
        """
        @url https://api.victoriassecret.com/products/v9/page/5f4603de-acd3-4aec-bd28-dee7cd0ddcd0?brand=pink&collectionId=1a62d1fb-d841-466e-9b0d-9c27898b3de5&limit=180&orderBy=REC&productId=72a0f96d-b4e7-4023-9e70-2679aa658391&stackId=4b3f681b-be7d-47bb-90bf-2083ce3a29b5&activeCountry=US
        @scrape_values id title image price color model_sku description product_id
        @meta {"use_proxy":"True"}
        """

        res = json.loads(response.text)

        product = res.get('product', {})
        url = product.get('url', '')

        #variants:
        for model_sku, model_info in product.get('productData', {}).items():
            fabrication = model_info.get('fabrication', '')
            description = model_info.get('longDescription', '')
            if not description:
                for long_description in product.get('longDescription', [{}]):
                    if long_description.get('genericId', '') == model_sku:
                        description = long_description.get('html', '')

            for choice_id, variant_info in model_info.get('choices', {}).items():
                color = variant_info.get('label', '')
                image = ''
                images = variant_info.get('images', [{}])
                if images:
                    image = images[0].get('image', '')
                if image:
                    image = f'{self.base_url}/p/760x1013/{image}.jpg'
                available_sizes = list(variant_info.get('availableSizes', {}).keys())
                url = f"{url.split('?')[0]}?choice={choice_id}&genericId={model_sku}"
                if not url.startswith(self.base_url):
                    url = self.base_url + url

                item = {
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': '200',
                    'category': self.category,
                    'brand': self.brand,
                    'id': url,
                    'old_ids': [self.base_url + product.get('url', '')],

                    'breadcrumb': ' | '.join([link.get('text') for link in res.get('breadcrumbs', {}).get('links', [{}])]),
                    'brandName': product.get('brandName', ''),
                    'product_category_info': product.get('sizeAndFitCategory', ''),

                    'title': product.get('shortDescription', ''),
                    'image': image,
                    'price': list(variant_info.get('availableSizes', {}).values())[0].get('originalPrice', ''),
                    'sale_price': list(variant_info.get('availableSizes', {}).values())[0].get('salePrice', ''),
                    'description': '\n'.join(Selector(description).xpath(u'//text()[normalize-space(.)]').extract()).strip(),
                    'model_sku': model_sku,
                    'color': color,
                    # 'scent_variants': scents,
                    # 'scents': list(set(scents.values())),
                    # 'scent': scents.get(selected_color_code, ''),
                    'sizes': available_sizes,
                    'product_id': product.get('id', ''),
                    'fabrication': fabrication,
                    'response_url': [response.url],
                    'product_url': [response.meta.get('product_url', '')],
                }
                product_url = response.meta.get('product_url', '')
                _model_sku = ''.join(parse_qs(urlparse(product_url).query).get('genericId', []))
                _choice = ''.join(parse_qs(urlparse(product_url).query).get('choice', []))
                if model_sku == _model_sku and choice_id == _choice:
                    item.update(response.meta.get('extra_item_info', {}))

                yield item


    def parse_filter(self, response):
        """
        @url https://api.victoriassecret.com/filters/v3/?brand=pink&collectionId=1a62d1fb-d841-466e-9b0d-9c27898b3de5&filter=&orderBy=REC&activeCountry=US
        @returns requests 1 50
        @meta {"use_proxy":"True"}
        """

        o = urlparse(response.url)
        d = parse_qs(o.query)
        brand = d.get('brand', [])[0]
        collection_id = d.get('collectionId', [])[0]

        res = json.loads(response.text)
        for facet in res.get('facets', []):
            if facet.get('label', '') in self.filters:
                for option in facet.get('options', []):
                    if option.get('isAvailable', True):
                        request = self.create_request(
                            url=self.api_listing_page.format(
                                brand=brand,
                                collection_id=collection_id,
                                filter=f"{facet.get('key', '')}%3A{option.get('value', '')}"),
                            callback=self.parse_listing_page)
                        request.meta['extra_item_info'] = {
                            facet.get('label', ''): option.get('value', ''),
                        }
                        print(f"==>{facet.get('label', '')}: {option.get('value', '')}")
                        yield request


    def parse_listing_page(self, response):
        """
        @url https://api.victoriassecret.com/stacks/v9/?brand=pink&collectionId=1a62d1fb-d841-466e-9b0d-9c27898b3de5&filter=&orderBy=REC&activeCountry=US
        @returns requests 1 30
        @meta {"use_proxy":"True"}
        """

        extra_item_info = response.meta.get('extra_item_info', {})
        res = json.loads(response.text)
        for data in res:
            for item in data.get('list', []):
                product_id = item.get('id', '')

                id = item.get('url', '')
                if id:
                    url = id.split('?', 1)[-1]
                    request = self.create_request(
                        url=self.api_base_url.format(uuid=self.guuid) + '?' +url+'&activeCountry=US',
                        callback=self.crawlDetailApi,
                        meta={'product_url': self.base_url + id, 'extra_item_info': extra_item_info, 'product_id': product_id})
                    yield request


    def parse(self, response):
        """
        @url https://www.victoriassecret.com/us/pink/tops-and-bottoms/sweatpants
        @returns requests 2
        @meta {"use_proxy":"True","use_selenium":"True"}
        """

        brand = response.xpath('//a[@class="fabric-link-element"]/@href').extract_first(default='').replace('/', '').strip()
        collection_id = response.xpath('//li[@class="fabric-grid-component-item"]//button/@data-collection-id').extract_first(default='')
        query_filter = urllib.parse.quote(self.query_filter)

        listing_page_url = self.api_listing_page.format(brand=brand, collection_id=collection_id, filter=query_filter)
        filter_page_url = self.api_filter_page.format(brand=brand, collection_id=collection_id, filter=query_filter)

        print(f'listing_page_url: {listing_page_url}', f'filter_page_url: {filter_page_url}', sep='\n')
        if collection_id:
            yield self.create_request(url=listing_page_url, callback=self.parse_listing_page)

            yield self.create_request(url=filter_page_url, callback=self.parse_filter)


    def createDynamicRequest(self, url, callback, errback=None, meta=None, wait_time=0, wait_until=None, perform_action=None):
        request = SeleniumRequest(url=url, callback=callback,
            wait_time = wait_time,
            wait_until = wait_until,
            perform_action = perform_action,
        )

        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
