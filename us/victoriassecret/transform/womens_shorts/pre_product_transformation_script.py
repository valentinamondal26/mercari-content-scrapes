import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', '')).replace('\n', ',')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace('$', '').strip()

            title = record.get('title', '').split(', ')[0]
            model = title
            model = re.sub(r'\bShort\b', 'Shorts', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            description = record.get('description', '')

            rise = ''
            match = re.findall(r'(\w+ waist\b)', description, flags=re.IGNORECASE)
            if match:
                rise = match[0]
            rise = rise.title()

            material = ''
            fabrication = record.get('fabrication', '')
            match = re.findall(r'<li>(Body\/Pocket|Body\/LINING\/GUSSET|Body):(.*?)</li>', fabrication, flags=re.IGNORECASE)
            if match:
                material = match[0][1]
                material = re.sub(r'\d+%', '', material, flags=re.IGNORECASE)
            if not material:
                match = re.findall(r'Imported (.*)$', description, flags=re.IGNORECASE)
                if match:
                    material = ', '.join(match[0].split('/'))
            material = re.sub('&Nbsp;', '', material, flags=re.IGNORECASE)
            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()

            inseam = ''
            match = re.findall(re.compile(r'(\d+[½]{0,1})(.\d+){0,1}["”]{0,1} inseam', re.IGNORECASE), description)
            if match:
                inseam = match[0][0] + (match[0][1] if len(match[0]) > 1 else '') + '"'
                inseam = inseam.replace('½', '.5').strip()

            color = record.get('color', '')
            color_filter = record.get('Color', '')

            color = re.sub(r'\s*with\s*', '/', color, flags=re.IGNORECASE)
            color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Logo Graphic', 'Graphic', 'Classic Logo', 'Gradient Logo', 'Shine']))), '', color, flags=re.IGNORECASE)
            color = re.sub(r'\bSt\b', 'State', color, flags=re.IGNORECASE)
            color = re.sub(r'\bFlorida Intl\b', 'FIU', color, flags=re.IGNORECASE)
            color = re.sub(r'/\s*$', '', color)
            color = re.sub(r'\s+', ' ', color).strip()


            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawlDate': record.get('crawlDate', '').split(', ')[0],
                'statusCode': record.get('statusCode', ''),

                'category': record.get('category'),
                'brand': record.get('brand', ''),

                'ner_query': ner_query,
                'title': title,
                'description': description.replace('\n', ','),
                'image': record.get('image', '').split(', ')[0],

                'model': model,
                'color': color,
                'sku': record.get('model_sku', ''),
                'msrp': {
                    'currencyCode': 'USD',
                    'amount': price,
                },

                'rise': rise,
                'inseam': inseam,
                'material': material,
            }

            return transformed_record
        else:
            return None
