import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', '')).replace('\n', ',')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace('$', '').strip()

            title = record.get('title', '').split(', ')[0]
            model = title
            model = re.sub(r'NEW!', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            description = record.get('description', '')

            rise = ''
            match = re.findall(re.compile(r'Regular rise|Mid-rise|High Rise|Low Rise', re.IGNORECASE), description)
            if match:
                rise = match[0]

            silhouette = ''
            match = re.findall(re.compile(r'Boot cut|skinny|relaxed leg|Relaxed fit|Slim fit|Oversized fit|Slim\,*\s*\-*tapered fit', re.IGNORECASE), description)
            if match:
                silhouette = match[0]
            silhouette = silhouette.title()

            length_type = ''
            match = re.findall(re.compile(r'Full length|Customizable length', re.IGNORECASE), description)
            if match:
                length_type = match[0]

            material = ''
            match = re.findall(r'\d+%\s*(\w+)\b', record.get('fabrication'), flags=re.IGNORECASE)
            if match:
                material = '/'.join(match)
            material = material.title()
            lining_material = ''
            # lining_material = material.split('/', 1)[-1]

            inseam = ''
            match = re.findall(re.compile(r'(\d+[½]{0,1})(.\d+){0,1}["”]{0,1} inseam', re.IGNORECASE), description)
            if match:
                inseam = match[0][0] + (match[0][1] if len(match[0]) > 1 else '') + '"'
                inseam = inseam.replace('½', '.5').strip()

            color = record.get('color', '')
            color_filter = record.get('Color', '')

            if re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Set', 'Gift', 'Top']))), model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawlDate': record.get('crawlDate', '').split(', ')[0],
                'statusCode': record.get('statusCode', ''),

                'category': record.get('category'),
                'brand': record.get('brand', ''),

                'ner_query': ner_query,
                'title': title,
                'description': description.replace('\n', ','),
                'image': record.get('image', '').split(', ')[0],

                'model': model,
                'color': color_filter or color,
                'sku': record.get('model_sku', ''),
                'msrp': {
                    'currencyCode': 'USD',
                    'amount': price,
                },

                'rise': rise,
                'silhouette': silhouette,
                'inseam': inseam,
                'material': material,
                'lining_material': lining_material,
                'length_type': length_type,
            }

            return transformed_record
        else:
            return None
