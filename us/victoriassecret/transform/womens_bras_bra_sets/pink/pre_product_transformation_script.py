import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', '')).replace('\n', ',')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace('$', '').strip()

            brand = record.get('brand', '')

            title = record.get('title', '').split(', ')[0]
            model = title
            model = re.sub(r'NEW!|\bin\b', '', model, flags=re.IGNORECASE)
            if not re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Bra', 'Bralette', 'Racerbacks', 'Racerback', 'Petals']))), model, flags=re.IGNORECASE):
                model = f'{model} Bra'
            model = re.sub(r'\s+', ' ', model).strip()

            color = record.get('color', '')
            color_filter = record.get('Color', '')

            description = record.get('description', '')

            material = ''
            fabrication = record.get('fabrication', '')
            match = re.findall(r'<li>Body.*?:(.*?)</li>', fabrication, flags=re.IGNORECASE)
            if match:
                material = match[0]
            if not material:
                match = re.findall(r'<p>Body.*?:(.*?)(Cup Lining|Mesh)', fabrication, flags=re.IGNORECASE)
                if match:
                    material = match[0][0]
            material = re.sub('&Nbsp;', '', material, flags=re.IGNORECASE)
            material = re.sub('Exclusive of Decoration', '', material, flags=re.IGNORECASE)
            material = re.sub(r'\d+%', '', material, flags=re.IGNORECASE)
            material = re.sub(r'\s+', ' ', material).strip()

            if record.get('product_category_info') == 'panties':
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawlDate': record.get('crawlDate', '').split(', ')[0],
                'statusCode': record.get('statusCode', ''),
                'category': record.get('category'),
                'brand': brand,

                'ner_query': ner_query,
                'title': title,
                'description': description.replace('\n', ','),
                'image': record.get('image', '').split(', ')[0],

                'model': model,
                'color': color_filter or color,
                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },

                'bra_lining_level': record.get('Lining Level', ''),
                'model_sku': record.get('model_sku', ''),
                'material': material,
            }

            return transformed_record
        else:
            return None
