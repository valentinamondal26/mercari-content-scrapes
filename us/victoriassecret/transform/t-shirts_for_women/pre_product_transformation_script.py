import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            brand = record.get('brand', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'NEW!|'+brand, re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            description = record.get('description', '')

            material = ''
            match = re.findall(re.compile(r'Imported cotton/polyester|Imported cotton/spandex'
                r'|Imported viscose/polyester/spandex|Imported cotton/modal|Imported polyester'
                r'|Imported nylon/polyester|Imported polyester/spandex|Imported cotton',
                re.IGNORECASE), description)
            if match:
                material = match[0]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": brand,

                "ner_query": ner_query,

                "title": record.get('title', ''),
                "description": description.replace('\n', ','),
                "model": model,
                "color": record.get('color', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'silhouette': record.get('subclass', ''),
                "material": material,
            }

            return transformed_record
        else:
            return None
