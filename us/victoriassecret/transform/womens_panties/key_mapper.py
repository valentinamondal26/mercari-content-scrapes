class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity.get("attribute", []):
                    model = entity.get("attribute", [{}])[0].get("id", '')
            elif "color" == entity['name']:
                color = entity["attribute"][0]["id"]

        key_field = model + color
        return key_field
