import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = record.get('color', '')
            style = record.get('subclass', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'NEW!|panty|panties|'+style+'|'+color, re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()
            model = model.title()
            if model == 'V' and style == 'Brazilian':
                model = 'V Brazilian'
            
            if not model and  style == 'Brazilian':
                model = 'Brazilian'

            description = record.get('description', '')

            rise = ''
            match = re.findall(re.compile(r'Regular rise|Mid-rise|Mid rise|High Rise|Low Rise', re.IGNORECASE), description)
            if match:
                rise = match[0]

            material = ''
            match = re.findall(re.compile(r'Elastic :(.*?)\n', re.IGNORECASE), description)
            if match:
                material = match[0].strip()
            if not material:
                match = re.findall(re.compile(r'Elastic:(.*?)\n', re.IGNORECASE), description)
                if match:
                    material = match[0].strip()
            material = re.sub(re.compile(r'%|,'), lambda m: m[0] + ' ', material)
            material = re.sub(r'\s+', ' ', material).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,

                "title": record.get('title', ''),
                "description": description.replace('\n', ','),
                "model": model,
                "color": color,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'rise': rise,
                "material": material,
                'style': style,
                'product_family': record.get('collection', ''),
            }

            return transformed_record
        else:
            return None
