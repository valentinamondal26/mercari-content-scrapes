import hashlib
import re
import fractions

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            # model = re.sub(re.compile(r'NEW!', re.IGNORECASE), '', model)
            # model = re.sub(' +', ' ', model).strip()

            description = record.get('description', '')

            def mixed_to_float(x):
                return float(sum(fractions.Fraction(term) for term in x.split()))

            length = ''
            width = ''
            height = ''

            dimensions = description
            match = re.findall(re.compile(r'Dimensions:(.*?)\n', re.IGNORECASE), description)
            if match:
                dimensions = match[0]
            match = re.findall(re.compile(r'(\d+\s*(\d+\/\d+){0,1})?"\s*[LD]', re.IGNORECASE), dimensions)
            if match:
                length = match[0][0]
                if '/' in length:
                    length = str(round(mixed_to_float(length), 2))
                length = length + '"'
            match = re.findall(re.compile(r'(\d+\s*(\d+\/\d+){0,1})?"\s*W', re.IGNORECASE), dimensions)
            if match:
                width = match[0][0]
                if '/' in width:
                    width = str(round(mixed_to_float(width), 2))
                width = width + '"'
            match = re.findall(re.compile(r'(\d+\s*(\d+\/\d+){0,1})?"\s*H', re.IGNORECASE), dimensions)
            if match:
                height = match[0][0]
                if '/' in height:
                    height = str(round(mixed_to_float(height), 2))
                height = height + '"'

            material = ''
            match = re.findall(re.compile(r'Imported polyester|Imported metallic polyurethane', re.IGNORECASE|re.DOTALL), description)
            if match:
                material = match[0]

            # material = materials.split('/')[0]

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description.replace('\n', ','),
                'sku': record.get('model_sku', ''),

                "model": model,
                "color": record.get('color', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },

                "product_type": record.get('subclass', 'Backpack'),
                "material": material,
                'length': length,
                'width': width,
                'height': height,
            }

            return transformed_record
        else:
            return None
