import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            brand = record.get('brand', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'NEW!|'+brand, re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            description = record.get('description', '')

            rise = ''
            match = re.findall(re.compile(r'\bHigh waist\b|\bDrawstring waist\b|\bFoldover Waist\b|\bFlat Waist\b', re.IGNORECASE), description+' '+model)
            if match:
                rise = match[0]
                rise = re.sub(re.compile(r'waist', re.IGNORECASE), '', rise).strip()


            inseam = ''
            match = re.findall(re.compile(r'(\d+[½]{0,1})(.\d+){0,1}["”]{0,1} inseam', re.IGNORECASE), description)
            if match:
                inseam = match[0][0] + (match[0][1] if len(match[0]) > 1 else '') + '"'
                inseam = inseam.replace('½', '.5').strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": brand,

                "ner_query": ner_query,

                "title": record.get('title', ''),
                "description": description.replace('\n', ','),
                "model": model,
                "color": record.get('color', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'rise': rise,
                'inseam': inseam,
            }

            return transformed_record
        else:
            return None
