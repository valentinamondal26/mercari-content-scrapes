class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        scent = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "scent" == entity['name']:
                if entity.get("attribute", []):
                    scent = entity.get("attribute", [{}])[0].get("id", '')

        key_field = model + scent
        return key_field
