import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            brand = record.get('brand', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'NEW!', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            description = record.get('description', '')

            size = ''
            match = re.findall(re.compile(r'(\d+){0,1}(.\d+){0,1}(\s+fl){0,1}(\s+oz)', re.IGNORECASE), description)
            if match:
                group = match[0]
                size = ''.join(group)
            
            fragrance_type = ''
            match = re.findall(re.compile(r'Fragrance type:(.*?)\n', re.IGNORECASE), description)
            if match:
                fragrance_type = match[0].strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": brand,

                "ner_query": ner_query,

                "title": record.get('title', ''),
                "description": description.replace('\n', ','),
                "model": model,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'item_type': record.get('subclass', ''),
                'fragrance_type': fragrance_type,
                'size': size,
                # 'scents': ','.join(list(filter(None, record.get('scents', [])))),
                'scent': record.get('scent', ''),
            }

            return transformed_record
        else:
            return None
