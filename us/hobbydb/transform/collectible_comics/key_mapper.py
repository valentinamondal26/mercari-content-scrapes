class Mapper:
    def map(self, record):

        model = ''
        product_line = ''
        series = ''
        release_date = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            elif "product_line" == entity['name'] and entity["attribute"]:
                product_line = entity["attribute"][0]["id"]
            elif "series" == entity['name'] and entity["attribute"]:
                series = entity["attribute"][0]["id"]
            elif "release_date" == entity['name'] and entity["attribute"]:
                release_date = entity["attribute"][0]["id"]

        key_field = model + product_line + series + release_date
        return key_field
