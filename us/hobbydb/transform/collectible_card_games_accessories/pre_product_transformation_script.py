import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            base_url = 'https://www.hobbydb.com'
            if not id.startswith(base_url):
                id = base_url + id
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '') or ''
            price = price.replace("$", "").strip()

            def pipe_tokens(tokens):
                return r'|'.join(list(map(lambda x: r'\b'+x+r'\b', tokens)))

            model = record.get('title', '')
            model = re.sub(r'\s[-–]\s', ' ', model)
            model = re.sub(r'\(|\)|"|\[|\]|aka Trish Stratus', '', model, flags=re.IGNORECASE)
            model = re.sub(r'w/|W/', 'with', model)
            model = re.sub(r'\s/\s', '/', model)
            model = re.sub(r'Wall-E', 'WALL-E', model, flags=re.IGNORECASE)
            model = re.sub(r'PRE-RELEASED', 'Pre-released', model, flags=re.IGNORECASE)
            model = re.sub(r'DIce', 'Dice', model, flags=re.IGNORECASE)
            model = re.sub(r'(^Funko\s)(?!Freddy|Fridays)', '', model, flags=re.IGNORECASE)
            words = ['PROTOTYPE', 'CONVENTION', 'CONCEPT', 'COLOURED', 'Making Fun replica movie premiere ticket']
            model = re.sub(pipe_tokens(words), lambda x: x.group().title(), model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            product_line = record.get('product_line', '')

            if re.findall(r'\bFunkO\'s\b', model, flags=re.IGNORECASE):
                model = re.sub(r'\bFunkO\'s\b', '', model, flags=re.IGNORECASE)
                product_line = "FunkO's"
            elif re.findall(pipe_tokens(['Bag', 'Tote', 'Pouch', 'Carrying Case']), model, flags=re.IGNORECASE):
                product_line = 'Bags, Totes & Pouches'

            # series = ', '.join(list(map(lambda x: re.sub(r'\s+', ' ', re.sub(r'\bFunko\b', '', x, flags=re.IGNORECASE)).strip(), record.get('series', []))))
            series = ', '.join(record.get('series', []))

            if re.findall(r'Pop! Cereal Bowls', series, flags=re.IGNORECASE):
                product_line = 'Pop! Cereal Bowls'
                model = re.sub(r'\bCereal Bowl\b', '', model, flags=re.IGNORECASE)
            elif re.findall(r'Tin-Tastic', series, flags=re.IGNORECASE):
                product_line = 'Tin-Tastic'
                if not re.findall(r'^Creative Activity Set', model, flags=re.IGNORECASE):
                    model = f'Creative Activity Set {model}'

            if re.findall(pipe_tokens(['Skateboards', 'Skateboard']), model, flags=re.IGNORECASE):
                product_line = 'Skateboards'
            elif re.findall(pipe_tokens(['Funko Fundays']), model, flags=re.IGNORECASE):
                product_line = 'Fundays'

            if re.findall(r'Whatever Else', product_line, flags=re.IGNORECASE):
                product_line = 'Accessories'

            if hex_dig == 'f8ab5adf0341c8a3fd5f07c8ff4948f8270fbbaf':
                model = 'Tracer Stocking'
            elif hex_dig == '9f19e5caeba0cb70e93d5714c7684051fdd56091':
                model = 'Mei Stocking'
            elif hex_dig == 'c838184f6024bf76960ec66dbda3cb9828cf4589':
                model = 'Marvel Fidget Spinner'

            release_date = record.get('date_from', '') or \
                record.get('released', '') or \
                re.sub(r'From:', '', ''.join(record.get('production_fields', {}).get('Produced', [])), flags=re.IGNORECASE)
            release_date = release_date.strip()

            category = ''
            if product_line in ['Puzzles']:
                category = 'Puzzles'
            elif product_line in ['Board Games', 'Card Games', 'Playing Cards']:
                category = 'Collectible Card Games & Accessories'

            if product_line not in ['Board Games', 'Card Games', 'Playing Cards', 'Puzzles']:
                return None

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": category,
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                'product_line': product_line,
                'series': series,
                'release_date': release_date,
                'upc': record.get('upc', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
