'''
[Epic] https://mercari.atlassian.net/browse/USCC-720 - Funko
https://mercari.atlassian.net/browse/USCC-772 - Funko Action Figures
https://mercari.atlassian.net/browse/USCC-773 - Funko Plush Toys
https://mercari.atlassian.net/browse/USCC-774 - Funko Collectible Card Games & Accessories, Funko Puzzles
https://mercari.atlassian.net/browse/USCC-775 - Funko Collectible Comics
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import json
import copy
from parsel import Selector


class HobbydbSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from hobbydb.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Action Figures

    brand : str
        brand to be crawled, Supports
        1) Funko

    Command e.g:
    scrapy crawl hobbydb -a category='Action Figures' -a brand='Funko'
    """

    name = 'hobbydb'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
        'http://shp-mercari-us-d00016.tp-ns.com:80',
        'http://shp-mercari-us-d00017.tp-ns.com:80',
        'http://shp-mercari-us-d00018.tp-ns.com:80',
        'http://shp-mercari-us-d00019.tp-ns.com:80',
        'http://shp-mercari-us-d00020.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'hobbydb.com'
    blob_name = 'hobbydb.txt'

    url_dict = {
        'Action Figures': {
            'Funko': {
                'url': 'https://www.hobbydb.com/marketplaces/hobbydb/subjects/funko-brand',
            }
        }
    }

    base_url = 'https://www.hobbydb.com'

    search_filter_id = 380
    items_per_page = 1000
    listing_page_api_url = 'https://www.hobbydb.com/api/catalog_items' \
        '?filters=%7B%22related_to%22:{search_filter_id},%22in_collection%22:%22all%22,%22in_wishlist%22:%22all%22,%22on_sale%22:%22all%22%7D' \
        '&id={search_filter_id}'\
        '&include_cit=true'\
        '&include_count=true'\
        '&include_main_images=true'\
        '&market_id=hobbydb'\
        '&order=%7B%22name%22:%22name%22,%22sort%22:%22asc%22%7D'\
        '&page={page_no}'\
        '&per={items_per_page}'\
        '&serializer=CatalogItemPudbSerializer'\
        '&subject_id={search_filter_id}'\
        '&subvariants=false'
    price_info_api_url = 'https://www.hobbydb.com/api/price_guide?catalog_item_id={catalog_item_id}'
    product_details_api_url = 'https://www.hobbydb.com/api/catalog_items/{catalog_item_id}?fields=true&related_subjects=true&market_id=hobbydb'
    product_metadata_api_url = 'https://www.hobbydb.com/api/catalog_items/{catalog_item_id}?metadata=true&market_id=hobbydb'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(HobbydbSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            url = self.listing_page_api_url.format(search_filter_id=self.search_filter_id, page_no=1, items_per_page=self.items_per_page)
            yield self.create_request(url=url, callback=self.parse)


    def parse_product_metadata(self, response):
        """
        @url https://www.hobbydb.com/api/catalog_items/544601?metadata=true&market_id=hobbydb
        @meta {"use_proxy":"True"}
        @scrape_values upc
        """

        res = json.loads(response.text)
        products = res.get('data', [])
        if products:
            product = products[0]
            attributes = product.get('attributes', {})
            item = response.meta.get('item_info', {})
            item.update({
                'upc': attributes.get('upc', ''),
            })
            yield item


    def parse_product_details(self, response):
        """
        @url https://www.hobbydb.com/api/catalog_items/544601?fields=true&related_subjects=true&market_id=hobbydb
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        res = json.loads(response.text)
        products = res.get('data', [])
        if products:
            product = products[0]
            attributes = product.get('attributes', {})
            item = response.meta.get('item_info', {})
            def get_data(fields):
                return dict(map(lambda x: (x.get('name', ''), list(map(lambda y: y.get('value', ''), x.get('data', [])))), fields))
            item.update({
                'released': attributes.get('released', ''),
                'fields': get_data(attributes.get('fields', [])),
                'production_fields': get_data(attributes.get('productionFields', [])),
            })

            catalog_item_id = item.get('catalog_item_id', '')

            product_metadata_url = self.product_metadata_api_url.format(catalog_item_id=catalog_item_id)
            yield self.create_request(url=product_metadata_url, callback=self.parse_product_metadata,
                meta={'item_info': copy.deepcopy(item)})


    def parse_pricing_info(self, response):
        """
        @url https://www.hobbydb.com/api/price_guide?catalog_item_id=544601
        @meta {"use_proxy":"True"}
        @headers {"referer":"https://www.hobbydb.com"}
        @returns requests 1
        """

        res = json.loads(response.text)
        products = res.get('data', [])
        if products:
            product = products[0]
            attributes = product.get('attributes', {})
            item = response.meta.get('item_info', {})
            item.update({
                'estimated_value': attributes.get('estimated_value', ''),
            })

            catalog_item_id = item.get('catalog_item_id', '')

            product_details_url = self.product_details_api_url.format(catalog_item_id=catalog_item_id)
            yield self.create_request(url=product_details_url, callback=self.parse_product_details,
                meta={'item_info': copy.deepcopy(item)})


    def parse(self, response):
        """
        @url https://www.hobbydb.com/api/catalog_items?filters=%7B%22related_to%22:380,%22in_collection%22:%22all%22,%22in_wishlist%22:%22all%22,%22on_sale%22:%22all%22%7D&id=380&include_cit=true&include_count=true&include_main_images=true&market_id=hobbydb&order=%7B%22name%22:%22name%22,%22sort%22:%22asc%22%7D&page=1&per=6&serializer=CatalogItemPudbSerializer&subject_id=380&subvariants=false
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        res = json.loads(response.text)
        products = res.get('data', [])
        for product in products:
            attributes = product.get('attributes', {})
            catalog_item_id = attributes.get('id', '')
            price = attributes.get('estimated_value', '')
            url = attributes.get('urls', {}).get('catalog_item_link_url', '')
            if not url.startswith(self.base_url):
                url = self.base_url + url
            item = {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,

                'id': url,
                'product_line': attributes.get('catalog_item_type_name', ''),
                'description': ' '.join(Selector((attributes.get('description', '') or '')).xpath('//text()').getall()),
                'price': price,
                'title': attributes.get('name', ''),
                'brand_name': (attributes.get('brand', {}) or {}).get('name', ''),
                'date_from': attributes.get('date_from', ''),
                'image': attributes.get('images', {}).get('detail_url', ''),
                'series': list(map(lambda x: x.get('name', ''), attributes.get('series', []))),
                'catalog_item_id': catalog_item_id,
            }
            if catalog_item_id:
                if not price:
                    pricing_info_url = self.price_info_api_url.format(catalog_item_id=catalog_item_id)
                    yield self.create_request(url=pricing_info_url, callback=self.parse_pricing_info,
                        meta={'item_info': copy.deepcopy(item)}, headers={'referer': self.base_url})
                else:
                    product_details_url = self.product_details_api_url.format(catalog_item_id=catalog_item_id)
                    yield self.create_request(url=product_details_url, callback=self.parse_product_details,
                        meta={'item_info': copy.deepcopy(item)})

        match = re.findall(r'page=(\d+)', response.url)
        if products and match:
            current_page_no = int(match[0])
            next_page_url = self.listing_page_api_url.format(search_filter_id=self.search_filter_id, page_no=current_page_no + 1, items_per_page=self.items_per_page)
            yield self.create_request(url=next_page_url, callback=self.parse)


    def create_request(self, url, callback, headers={}, meta=None):
        request = scrapy.Request(url=url, callback=callback, headers=headers, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request