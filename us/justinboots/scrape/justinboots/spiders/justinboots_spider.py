'''
https://mercari.atlassian.net/browse/USCC-686 - Justin Boots Men's Shoes
https://mercari.atlassian.net/browse/USCC-687 - Justin Boots Women's Boots
https://mercari.atlassian.net/browse/USCC-688 - Justin Boots Women's Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

class JustinbootsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from justinboots.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Shoes
        2) Women's Boots
        3) Women's Shoes

    brand : str
        brand to be crawled, Supports
        1) Justin Boots

    Command e.g:
    scrapy crawl justinboots -a category="Men's Shoes" -a brand='Justin Boots'
    scrapy crawl justinboots -a category="Women's Boots" -a brand='Justin Boots'
    scrapy crawl justinboots -a category="Women's Shoes" -a brand='Justin Boots'
    """

    name = 'justinboots'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'justinboots.com'
    blob_name = 'justinboots.txt'

    url_dict = {
        "Men's Shoes": {
            'Justin Boots': {
                'url': 'https://www.justinboots.com/en-US/Products?gender=4&productline=60',
            }
        },
        "Women's Boots": {
            'Justin Boots': {
                'url': [
                    'https://www.justinboots.com/en-US/Products?gender=8&productline=60&maincategory=64',
                    'https://www.justinboots.com/en-US/Products?gender=8&productline=60&maincategory=66'
                ]
            }
        },
        "Women's Shoes": {
            'Justin Boots': {
                'url': 'https://www.justinboots.com/en-US/Products?gender=8&productline=60&maincategory=81',
            }
        },
    }

    base_url = "https://www.justinboots.com/"

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(JustinbootsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls = [self.launch_url]
            elif isinstance(self.launch_url, list):
                urls = self.launch_url

            for url in urls:
                yield self.create_request(url=url, callback=self.parse)

    def crawlDetail(self, response):
        """
        @url https://www.justinboots.com/en-US/Product/Details?stylenumber=CR4010
        @meta {"use_proxy":"True"}
        @scrape_values id title images price color specs model_sku description
        """
        specs = {}
        details = response.xpath("//ul[@class='descDetails__list']/li/text()").getall()
        for detail in details:
            if ":" in detail:
                key, value = detail.split(":")
                specs[key] = value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath("//h1[@id='main-title']/span/text()").get(),
            'images': response.xpath("//span[@class='productImage productImages__image productImage--quaternary']/span/img/@data-src").getall(),
            'price': response.xpath("//span[@class='sizeGrid__retailPrice']/s/text()").get() or response.xpath("//span[@class='sizeGrid__price']/text()").get(),
            'color': response.xpath("//span[@class='swatches__color']/text()").get(),
            'specs': specs,
            'model_sku': response.xpath("normalize-space(//span[@class='sizeGrid__stockNumber'])").get(),
            'description': response.xpath("normalize-space(//div[@class='descDetails__column'])").get()
        }


    def parse(self, response):
        """
        @url https://www.justinboots.com/en-US/Products?gender=4&productline=60
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        page_size = int(response.xpath("//span[@class='resultsSummary__pageSize']/text()").get())
        total_items = int(response.xpath("//span[@class='resultsSummary__resultsSize']/text()").get())
        total_pages = (total_items//total_items)+1

        for page in range(0,total_pages+1):
            url = response.url+"&page="+str(page)
            yield self.create_request(url=url, callback=self.parseLinks)


    def parseLinks(self, response):
        for product in response.selector.xpath("//a[@class='productCaption__anchor']")[:-1]:
            product_url = product.xpath('./@href').extract_first()
            if product_url:
                yield self.create_request(url=self.base_url+product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
