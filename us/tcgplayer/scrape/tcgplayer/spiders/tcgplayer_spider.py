'''
https://mercari.atlassian.net/browse/USDE-1492 - Animation Collectable Trading Cards & Pokemon
https://mercari.atlassian.net/browse/USCC-634 - Sci-Fi Collectable Trading Cards & Magic: The Gathering
https://mercari.atlassian.net/browse/USCC-635 - Animation Collectable Trading Cards &  Konami
'''

# -*- coding: utf-8 -*-
import scrapy
import datetime
import copy
import json
from datetime import datetime
import itertools
import os
from scrapy.exceptions import CloseSpider


class TcgplayerSpider(scrapy.Spider):
    """
        spider to crawl items in a provided category and brand from Tcgplayer.com

        Attributes

        category : str
        category to be crawled, Supports
        1) Pokémon Trading Card Game Cards & Merchandise

        brand : str
        brand to be crawled, Supports
        1) Pokemon

        Command e.g:
        scrapy crawl tcgplayer  -a category='Animation Collectable Trading Cards' -a brand='Pokemon'
        scrapy crawl tcgplayer  -a category='Sci-Fi Collectable Trading Cards' -a brand='Magic The Gathering'
        scrapy crawl tcgplayer  -a category='Animation Collectable Trading Cards' -a brand='Konami'

    """
    name = 'tcgplayer'

    category = None
    brand = None

    merge_key = 'id'

    source = 'tcgplayer.com'
    blob_name = 'tcgplayer.txt'

    launch_url = None
    filters = None

    base_url='https://www.tcgplayer.com'

    url_dict = {
        'Sci-Fi Collectable Trading Cards': {
            'Magic The Gathering': {
                'url': 'https://www.tcgplayer.com/search/magic/product?productLineName=magic',
                'productline_name': 'magic',
                'filters': ['productTypeName']
            }
        },
        'Animation Collectable Trading Cards':{
            'Konami': {
                'url': 'https://www.tcgplayer.com/search/yugioh/product?productLineName=yugioh',
                'productline_name': 'yugioh',
                'filters': ['productTypeName']
            },
            'Pokemon': {
               'url': 'https://www.tcgplayer.com/search/pokemon/product?productLineName=pokemon',
               'productline_name': 'pokemon',
               'filters': ['productTypeName']
            }
        }
    }

    filter_requests_payload =  {"algorithm":"","from":0,"size":480,"filters":{"term":{"productLineName":[]},"range":{}},"listingSearch":{"filters":{"term":{},"range":{},"exclude":{"channelExclusion":0}}},"context":{"cart":{},"shippingCountry":"IN"},"sort":{}}
    filter_url = 'https://www.tcgplayer.com/search/{productline_name}/{filter_value}?productLineName=pokemon&page={page_no}&{filter_applied}={filter_value}'    
    initial_request_payload= {"algorithm":"","from":0,"size":480,"filters":{"term":{"productLineName":[]},"range":{}},"listingSearch":{"filters":{"term":{},"range":{},"exclude":{"channelExclusion":0}}},"context":{"cart":{},"shippingCountry":"IN"},"sort":{}}

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com/',
        'http://shp-mercari-us-d00002.tp-ns.com/',
        'http://shp-mercari-us-d00003.tp-ns.com/',
        'http://shp-mercari-us-d00004.tp-ns.com/',
        'http://shp-mercari-us-d00005.tp-ns.com/',
    ]

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TcgplayerSpider, self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')
        self.productline_name = self.url_dict.get(category, {}).get(brand, {}).get('productline_name', '')
        self.listing_page_api = 'https://mpapi.tcgplayer.com/v2/search/request?q=&isList=false'
        self.round_robin = itertools.cycle(self.proxy_pool)
        if not self.launch_url and not self.productline_name:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def contracts_mock_function(self):
        self.headers = {
            'referer': 'https://www.tcgplayer.com/search/pokemon/league-and-championship-cards?productLineName=pokemon&setName=league-and-championship-cards&page=1',
            'Content-Type':'application/json; charset=UTF-8'
        }
        self.listing_page_api = 'https://mpapi.tcgplayer.com/v2/search/request?q=&isList=false'
        self.filter_url = 'https://www.tcgplayer.com/search/pokemon/{set_url_name}?productLineName=pokemon&page={page_no}&setName={set_url_name}'
        self.initial_request_payload = {"algorithm":"","from":0,"size":480,"filters":{"term":{"productLineName":["pokemon"]},"range":{}},"listingSearch":{"filters":{"term":{},"range":{},"exclude":{"channelExclusion":0}}},"context":{"cart":{},"shippingCountry":"IN"},"sort":{}}
        self.filter_request_payload = {"algorithm":"","from":0,"size":480,"filters":{"term":{"productLineName":["pokemon"],"setName":["league-and-championship-cards"]},"range":{}},"listingSearch":{"filters":{"term":{},"range":{},"exclude":{"channelExclusion":0}}},"context":{"cart":{},"shippingCountry":"IN"},"sort":{}}
        self.productline_name = 'pokemon'

    def start_requests(self):
        if self.launch_url:
            meta={}
            # meta['page'] = 1
            meta['initial_request_payload'] = True
            yield self.create_post_request(url=self.listing_page_api,callback=self.get_filter_names, meta=meta)


    def get_filter_names(self,response):
        """
        @url https://mpapi.tcgplayer.com/v2/search/request?q=&isList=false
        @meta {"request_method":"POST","use_proxy":"True"}
        @body {"algorithm":"","from":0,"size":480,"filters":{"term":{"productLineName":["pokemon"]},"range":{}},"listingSearch":{"filters":{"term":{},"range":{},"exclude":{"channelExclusion":0}}},"context":{"cart":{},"shippingCountry":"IN"},"sort":{}}
        @headers {"referer":"https://www.tcgplayer.com/search/pokemon/product?productLineName=pokemon&page=1","Content-Type":"application/json; charset=UTF-8"}
        @returns requests 1
        """
        meta = response.meta
        meta.pop('initial_request_payload',None)
        response_data = response.body.decode('utf-8')
        response_json = json.loads(response_data)
        results_set_name = response_json.get('results',[{}])[0].get('aggregations',{}).get('setName',[])
        self.setnames = []       
        for setname in results_set_name:
            self.setnames.append(setname['urlValue'])


        for filter_name in self.filters:

            try:
                filter_values  = response_json.get('results',[])[0].get('aggregations',{}).get(filter_name,[])
            except IndexError:
                filter_values=[]

            if filter_values:
                for fil_value in filter_values:
                    url_value = fil_value.get('urlValue','')
                    meta['page'] = 1
                    meta['from'] = 0
                    meta['filter'] = filter_name
                    meta[filter_name] = url_value
                    yield self.create_post_request(url = self.listing_page_api,callback=self.parse_listing_page,meta=meta)


    def parse_listing_page(self,response):
        """
        @url https://mpapi.tcgplayer.com/v2/search/request?q=&isList=false
        @meta {"use_proxy":"True","request_method":"POST","from":480,"page":1}
        @body {"algorithm": "", "from": 0, "size": 480, "filters": {"term": {"productLineName": ["pokemon"], "setName": ["mcdonalds-promos-2011_new"]}, "range": {}}, "listingSearch": {"filters": {"term": {}, "range": {}, "exclude": {"channelExclusion": 0}}}, "context": {"cart": {}, "shippingCountry": "IN"}, "sort": {}}
        @headers {"referer":"https://www.tcgplayer.com/search/pokemon/mcdonalds-promos-2011_new?productLineName=pokemon&page=1&setName=mcdonalds-promos-2011_new","Content-Type":"application/json; charset=UTF-8"}
        @returns requests 1
        """


        meta = response.meta
        response_data = response.body.decode('utf-8')
        response_json = json.loads(response_data)

        try:
            total_products = response_json.get('results',[])[0].get('totalResults',{})
        except IndexError:
            ''

        try:
            products_json = response_json.get('results',[])[0].get('results',[])
        except IndexError:
            products_json = []

        for product_details in products_json:
            meta_data = {}
            meta_data['rarity'] = product_details.get('rarityName', '')
            meta_data['custom_attributes'] = product_details.get('customAttributes', {})
            meta_data['setId'] = product_details.get('setId', '')
            meta_data['productId'] = product_details.get('productId', '')
            meta_data['productLineName'] = product_details.get('productLineName', '')
            producturl_name = product_details.get('productUrlName','').lower().replace(' ','-')
            seturl_name = product_details.get('setUrlName','').lower().replace(' ','-')
            meta_data['market_price'] = product_details.get('marketPrice','')

            selling_price = product_details.get('listings',[])
            if selling_price:
                meta_data['selling_price'] = selling_price[0].get('price','') ##first index matched with the selling price in many cases

            if producturl_name and seturl_name:
                meta.update({'product_meta': meta_data})
                product_url = f'https://shop.tcgplayer.com/{self.productline_name}/{seturl_name}/{producturl_name}'
                yield self.create_get_request(url=product_url, callback=self.crawldetails, meta=meta)

        if total_products > 4800:##THIS IS THE MAXIMUM VIEWABLE ITEMS IN THE PRODUCTS. TO GET THE REST OF THE PRODUCTS WE ARE APPLYING CUMULATIVE FILTERS TO GET THE PRODUCTS
            for set_name in self.setnames:
                meta['set_name'] = set_name
                meta['from'] = 0
                yield self.create_post_request(url=self.listing_page_api, callback=self.parse_listing_page, meta=meta)
        elif  meta['from'] < 4800 and total_products < 4800 :
            meta['page'] += 1
            meta['from'] += 480
            yield self.create_post_request(url=self.listing_page_api,callback=self.parse_listing_page,meta=meta)


    def crawldetails(self,response):
        """
        @url https://shop.tcgplayer.com/pokemon/swsh01-sword-and-shield-base-set/quick-ball
        @scrape_values title image product_line market_price  product_details.Card\sNumber\s/\sRarity: product_details.Card\sType: product_details.Card\sText:
        @meta {"use_proxy":"True"}
        """
        meta = response.meta
        product_meta = meta['product_meta']
        title = response.xpath("//div[@class='product-details__header-mobile']/h1/text()").get(default='')
        image = response.xpath("//div[@class='product-details__image ']/img/@src").get(default='')
        product_line =  response.xpath("//div[@class='product-details__set']/a/text()").get(default='')

        normal_market_price = response.xpath("//div[@class='price-point price-point--market']//th[contains(text(),'Normal')]/following-sibling::td/text()").get(default='')
        normal_foil_price = response.xpath("//div[@class='price-point price-point--market']//th[contains(text(),'Foil')]/following-sibling::td/text()").get(default='')
        
        buylist_normal_market_price = response.xpath("//div[@class='price-point price-point--buylist']/table/tbody/tr/th[contains(text(), 'Normal')]/../td/text()").get(default='')
        buylist_foil_market_price = response.xpath("//div[@class='price-point price-point--buylist']/table/tbody/tr/th[contains(text(), 'Foil')]/../td/text()").get(default='')
        
        median_normal_price = response.xpath("//div[@class='price-point price-point--listed-median']/table/tbody/tr/th[contains(text(), 'Normal')]/../td/text()").get(default='')
        median_foil_price = response.xpath("//div[@class='price-point price-point--listed-median']/table/tbody/tr/th[contains(text(), 'Foil')]/../td/text()").get(default='')
        
        prod_details = {}
        for key,value in zip(response.xpath("//dt[@class='product-description__term']"), response.xpath("//dd[@class='product-description__value']")): 
            prod_details.update({key.xpath(".//text()").get() : value.xpath(".//text()").getall()}) 
            
        set_name = response.xpath("//div[@class='product-details__set']/a/text()").get(default='')
        item = {
            'id': response.url,
            'status_code':str(response.status),
            'crawl_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'brand': self.brand,
            'category': self.category,

            'title': title,
            'image': image,
            'product_line': product_line,
            'normal_market_price': normal_market_price,
            'normal_foil_price': normal_foil_price,
            'buylist_normal_market_price': buylist_normal_market_price,
            'buylist_foil_market_price': buylist_foil_market_price,
            'median_normal_price': median_normal_price,
            'median_foil_price': median_foil_price,
            'set_name': set_name,
            'product_details': prod_details
        }
        filter_value = meta.get('filter', '')
        if filter_value:
            filter_data = dict.fromkeys([meta['filter']], meta[filter_value])
            item.update(filter_data)
        item.update(product_meta)
        yield item
    
    def create_get_request(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta, headers=headers)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)
        request.meta.update({'use_cache': True})
        return request


    def create_post_request(self, url, callback, meta=None,headers=None):
        if meta.get('initial_request_payload',''):
            request_payload = copy.deepcopy(self.initial_request_payload)
            request_payload['filters']['term']['productLineName'] = [self.productline_name]
            referer = self.launch_url+"&page=1"#+str(meta['page'])
        else:
            request_payload = copy.deepcopy(self.filter_requests_payload)
            filter_applied = meta['filter']
            request_payload['filters']['term'][filter_applied]=[meta[filter_applied]]
            request_payload['filters']['term']['productLineName'] = [self.productline_name]
            if meta.get('set_name', ''):
                request_payload['filters']['term']['setName'] = [meta['set_name']]
            request_payload['from'] = meta['from']
            # if meta['from'] == 0:
            #     meta['from'] += 480
            # referer = self.filter_url.format(productline_name=self.productline_name, filter_applied=filter_applied, filter_value=meta[filter_applied], page_no=meta['page'])

        headers = {
            'referer': 'https://www.tcgplayer.com/',
            'Content-Type':'application/json; charset=UTF-8'
        }
        request = scrapy.Request(url = url, callback = callback, method = 'POST', body = str(request_payload),headers = headers,meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)
        request.meta.update({'use_cache': True})
        return request