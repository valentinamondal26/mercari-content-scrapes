class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        product_line = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity['attribute']:
                    model = entity["attribute"][0]["id"]
            elif "series" == entity['name']:
                if entity['attribute']:
                    series = entity["attribute"][0]["id"]

        key_field = model + series
        return key_field
