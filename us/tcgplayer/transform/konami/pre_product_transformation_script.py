import re
import hashlib
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')
            title = record.get('title', '')

            card_number = record.get('product_details', {}).get('Number:', '')
            if card_number:
                card_number = card_number[0]
            if not card_number:
                card_number = record.get('custom_attributes', {}).get('number', '')
            card_num = re.sub(r'\?', '\?', card_number, flags=re.IGNORECASE)

            model = re.sub(r'\(|\)|\[|\]|\s+\-\s+', ' ', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)
            model = re.sub(r'^\bYu-Gi-Oh\b\!|^\bYu-Gi-Oh\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\b{card_num}\b'.format(card_num=card_num), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            price = record.get('median_normal_price', '').replace('N/A', '') or record.get('normal_market_price', '').replace('N/A', '')
            price = price.replace('$', '').strip()
            
            set_name = record.get('set_name', '').strip()
            
            product_type_names = ['Cards',  'Booster Box', 'Booster Pack', 'Tin', 'Intro Pack', 'Fat Pack', 'Box Sets', 'YGO Start Decks', 'Magic Booster Box Case']
            product_type_name = record.get('productTypeName', '')
            if re.findall(r'^Cards$', product_type_name):
                product_type_name = 'Single Cards'
            elif re.findall(r'\bYGO Start Decks\b', product_type_name):
                product_type_name = 'Starter Decks'
            
            if re.findall(r'\bBooster Pack\b', model, flags=re.IGNORECASE):
                product_type_name = 'Booster Pack'

            if re.findall(r'\bSealed Products\b', product_type_name, flags=re.IGNORECASE):
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', product_type_names))), title, flags=re.IGNORECASE)
                if match:
                    product_type_name = match[0]
                else:
                    product_type_name = 'Single Cards'

            card_type = record.get('custom_attributes', {}).get('cardType', '')
            if card_type:
                card_type = card_type[0]
            
            rarity = record.get('custom_attributes', {}).get('rarityDbname', '') or record.get('rarity', '')
            if not rarity:
                try:
                    rarity = record.get('product_details', {}).get('Rarity:', [])[0]
                except IndexError:
                    rarity = ''

            description = ' '.join(record.get('product_details', {}).get('Description:', []))

            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date','').split(', ')[0],
                'status_code': record.get('status_code',''),

                'category': record.get('category',''),
                'brand': record.get('brand',''),

                'title': record.get('title',''),
                'image': record.get('image',''),

                'model': model,

                'series': set_name,
                'product_type': product_type_name,
                'card_type': card_type,
                'card_number': card_number,
                'rarity': rarity,
                
                'description': description,
                "price": {
                    "currency_code": "USD",
                    "amount": price,
                },
            }
            return transformed_record
        else:
            return None
