import re
import hashlib
from titlecase import titlecase

class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')
            title = record.get('title', '')

            model = re.sub(r'\(|\)|\[|\]|\s+\-\s+', ' ', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)
            
            price = record.get('median_normal_price', '').replace('N/A', '') or record.get('normal_market_price', '').replace('N/A', '')
            price = price.replace('$', '').strip()
            
            set_name = record.get('set_name', '').strip()
            
            product_type_name = record.get('productTypeName', '')
            if re.findall(r'^Cards$', product_type_name):
                product_type_name = 'Single Cards'
            elif re.findall(r'\bYGO Start Decks\b', product_type_name):
                product_type_name = 'Starter Decks'
            elif re.findall(r'\bAll 5 Intro Packs\b', product_type_name):
                product_type_name = 'Intro Pack'

            if re.findall(r'\bSealed Products\b', product_type_name, flags=re.IGNORECASE):
                product_type_name = self.get_product_type_name(title)
            elif not product_type_name:
                product_type_name = self.get_product_type_name

            card_type = record.get('custom_attributes', {}).get('cardType', '')
            if card_type:
                card_type = card_type[0]
            
            rarity = record.get('custom_attributes', {}).get('rarityDbname', '') or record.get('rarity', '')
            if not rarity:
                try:
                    rarity = record.get('product_details', {}).get('Rarity:', [])[0]
                except IndexError:
                    rarity = ''

            card_number = record.get('product_details', {}).get('Number:', '')
            if card_number:
                card_number = card_number[0]
            if not card_number:
                card_number = record.get('custom_attributes', {}).get('number', '')
            
            color = record.get('custom_attributes', {}).get('color', '')
            if color:
                color = '/'.join(color)

            description = ' '.join(record.get('product_details', {}).get('Description:', []))
            
            if set_name == 'Magic: The Gathering Apparel' or product_type_name == 'Intro Pack Display':
                return None
            
            transformed_record = {
                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date','').split(', ')[0],
                'status_code': record.get('status_code',''),

                'category': record.get('category', ''),
                'brand': 'Magic: The Gathering',

                'title': record.get('title',''),
                'image': record.get('image',''),

                'model': model,

                'series': set_name,
                'product_type': product_type_name,
                'card_type': card_type,
                'card_number': card_number,
                'rarity': rarity,
                'color': color,
                
                'description': description,
                "price": {
                    "currency_code": "USD",
                    "amount": price,
                },
            }
            return transformed_record
        else:
            return None

    def get_product_type_name(self, title):
        product_type_names = ['Cards',  'Intro Pack', 'Booster Pack', 'Booster Box', 'Fat Pack', 'Precon/Event Decks', \
        'Magic Deck Peck', 'Magic Booster Box Case', 'All 5 Intro Packs', 'Intro Pack Display',\
        'Booster Battle Pack', 'Box Sets', '3x Magic Booster Packs']
        match = re.findall(r'|'.join(list(map(lambda x: r'\b'+ x + r'\b', product_type_names))), title, flags=re.IGNORECASE)
        if match:
            product_type_name = match[0]
        else:
            product_type_name = 'Single Cards'
        return product_type_name
