import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title')
            
            pattern=re.compile(r"®|\(|\)|skin|care|products|mary\s*\-*kay",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            
            for word in record.get('Collection','').split(', '):
                pattern=re.compile("{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            
            for word in record.get('Skin Type','').split(', '):
                pattern=re.compile("{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            
            model=' '.join(model.split())

        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date',''),
                "status_code": record.get('status_code',''),
                "category": "Skin Care Products",
                "description": description.replace('®',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query.replace('\n',''),
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "size" : record.get('size',''),
                "health_concern":record.get('Concern',''),
                "skin_type":record.get('Skin Type',''),
                "use":record.get("use",''),
                "product_line":record.get('Collection',''),

                }

            return transformed_record
        else:
            return None
