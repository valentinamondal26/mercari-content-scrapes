'''
https://mercari.atlassian.net/browse/USCC-293 - Mary Kay Skin Care Products
'''

import scrapy
import datetime
import random
import os
import re
from scrapy.exceptions import CloseSpider

class marykaypider(scrapy.Spider):
    """
    spider to crawl items in a provided category from marykay.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Skin Care Products

    brand : str
        category to be crawled, Supports
        1) Mary Kay

    Command e.g:
    scrapy crawl marykay -a category="Skin Care Products" -a brand='Mary Kay'

    """

    name = 'marykay'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None
    number_of_products = None
    source = 'marykay.com'
    blob_name = 'marykay.txt'
    base_url = 'https://www.marykay.com'

    url_dict={
        "Skin Care Products": {
            'Mary Kay': {
                "url": 'https://www.marykay.com/en-us/products/skincare?product-type=face&product-filter=body-part-target-area%3Aface&body-part-target-area=face',
                'filters': ['Concern','Skin Type','Collection']
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(marykaypider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        self.filters=self.url_dict.get(category,{}).get(brand,{}).get('filters','')
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand,{}).get('url')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Concern','Skin Type','Collection']


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self,response):
        """
        @url https://www.marykay.com/en-us/products/skincare?product-type=face&product-filter=body-part-target-area%3Aface&body-part-target-area=face
        @returns requests 2
        @meta {"use_proxy":"True"}
        """

        if self.filters:
            for filter in self.filters:
                filter_values=[]
                values=response.xpath('//div[@class="filter-listing"]/h4[contains(text(),"{}")]/following-sibling::ul/li/text()'.format(filter)).getall()
                for val in values:
                     if re.findall(r"\w+",val)!=[]:
                         filter_values.append(re.findall(r"\w+",val)[0])
                filter_options=response.xpath('//div[@class="filter-category"]/@data-id').getall()
                url_data= response.xpath('//div[@class="filter-listing"]/h4[contains(text() ,"{}")]/following-sibling::ul/li/@data-id'.format(filter)).getall()
                for data in url_data:
                    index=self.filters.index(filter)

                    url="https://www.marykay.com/en-us/products/skincare?product-type=face&product-filter=body-part-target-area%3Aface&body-part-target-area=face&{}={}".format(filter_options[index],data)
                    meta={}
                    index=url_data.index(data)
                    meta=meta.fromkeys([filter],filter_values[index])
                    meta.update({'filter_name':filter})
                    print(meta)
                    yield self.createRequest(url=url,callback=self.parse,meta=meta)

        yield self.createRequest(url=response.url,callback=self.parse)


    def parse(self, response):
            """
            @url https://www.marykay.com/en-us/products/skincare?product-type=face&product-filter=body-part-target-area%3Aface&body-part-target-area=face&collection=timewise
            @returns requests 1
            """

            all_links=response.xpath('//a[@class="product-name"]/@href').getall()
            all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
            for link in all_links:
                yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)


    def parse_product(self,response):
        """
        @url https://www.marykay.com/en-us/products/skincare/timewise-pore-minimizer-100741
        @meta {"use_proxy":"True"}
        @scrape_values image title price size description
        """

        item={}
        item["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"]=str(response.status)
        item['id']=response.url
        item['image']="https:"+response.xpath('//div[contains(@class,"product-image")]//img/@src').get(default='')
        item['brand']=self.brand
        item['category']=self.category
        item['title']=response.xpath('//div[div[@class="flags"]]/h1/text()').get(default='')
        item['price']=response.xpath('//input[@id="PDP-ProductPrice"]/@value').get(default='')
        item['size']=response.xpath('normalize-space(//div[@class="included-sizes"]/text())').get(default='')
        item['use']='Face'

        des=response.xpath('//div[@class="resp-tabs-container"]/div[@class="tab"]/div[@class="row"]')
        description=[]

        count=0
        for d in des:
            if count<2:
                description.append(' '.join(d.xpath('.//text()').getall()))
                count+=1
        item['description']=' '.join(' '.join(description).replace("\r",'').replace("\n","").split())
        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)
        yield item


    def createRequest(self, url, callback,meta=None,headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key,value in headers.items():
                request.headers[key]=value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request