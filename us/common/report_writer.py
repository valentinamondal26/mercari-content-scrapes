
class ReportWriter(object):

    report = None

    def __init__(self):
        self.report = {
            'header': {},
            'schema': {},
            'error_logs': [],
            'footer': {}
        }

    def add_data(self, key, line=None, dict=None):
        if line:
            self.report['error_logs'].append(line)
        else:
            self.report[key] = dict
        return

    def return_report(self):
        return self.report
