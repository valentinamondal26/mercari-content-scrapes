import os
import sys
import json
import re
import argparse
import subprocess
from datetime import datetime
from google.cloud import storage

import logging
# from crawl_trigger import log_subprocess_output_pipe
from common.configs import Config
import requests

def scrapy_check(project=None):
    cmd = 'scrapy check'
    cmd = cmd.split()
    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT,
                         stdin=subprocess.PIPE,
                         )
    # parsed_result = []
    # with p.stdout:
    #     log_subprocess_output_pipe(p.stdout)
    #     out = p.stdout.seek(0)
    #     parsed_result = parse_result(out)
    # exitcode = p.wait() # 0 means success
    # logging.info(f'process exited with code: {exitcode}')
    out, _ = p.communicate()
    results = out.decode('utf-8').split('\n')
    for l in results:
        print(l)
    return parse_results(results)


def parse_results(results):
    # result = result.decode('utf-8')
    # result = result.split("\n")
    # print(f'parse_result: {results}')
    report = []
    for r in results:
        if not r:
            continue
        try:
            report_dicts = json.loads(r)
            if 'status' in report_dicts and 'reason' in report_dicts and 'spider_name' in report_dicts:
                report.append(report_dicts)
        except ValueError:
            pass
        except TypeError:
            pass
    return report


def process_result(results):
    processed_result = []
    print(results)
    for result in results:
        try:
            app_name = list(result.keys())[0]
            status = True
            spider_name = result.get(app_name, [{}])[0].get('spider_name', '')
            timestamp = result.get(app_name, [{}])[0].get('timestamp', '')
            reason = []
            for spider_result in result.get(app_name, []):
                spider_status = spider_result.get('status', False)
                if spider_status != 'Success':
                    reason.append(f"{spider_result.get('callback_name', '')}(): {spider_result.get('reason', '')}")
                status &= spider_status == 'Success'
            processed_result.append(
                {
                    'app_name': app_name,
                    'status': status,
                    'spider_name': spider_name,
                    'reason': '\n'.join(reason),
                    'timestamp': timestamp,
                }
            )
        except Exception as e:
            # import traceback
            # traceback.print_exc()
            print(f'Exception occured: {e}')
            processed_result.append(
                {
                    'app_name': list(result.keys())[0],
                    'status': False,
                    'spider_name': '',
                    'reason': f'Exception occured while processing result: {e}',
                    'timestamp': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                }
            )

    return processed_result


def gcs_upload_data(result, destination_blob_name=None):
    if not destination_blob_name:
        destination_blob_name = 'raw/unit_test_results/report/results.json'
    destination_blob_name = re.sub(re.compile(r"['&,]"), '', destination_blob_name)
    destination_blob_name = re.sub(r'\s+', '_', destination_blob_name).replace(':','_') .strip().lower()
    client = storage.Client()
    bucket_name = 'content_us'
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_string(result)
    print(f'File uploaded to gs://{bucket_name}/{destination_blob_name}')


def gcs_upload_file(local_filepath, destination_blob_name=None):
    destination_blob_name = re.sub(re.compile(r"['&,]"), '', destination_blob_name)
    destination_blob_name = re.sub(r'\s+', '_', destination_blob_name).replace(':','_') .strip().lower()
    client = storage.Client()
    bucket_name = 'content_us'
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    # blob.upload_from_file(open(local_filepath, 'r+'))
    blob.upload_from_filename(local_filepath)
    print(f'File uploaded to gs://{bucket_name}/{destination_blob_name}')


def notify_slack_alert(message, crawl_id, env):
    from google.cloud import pubsub_v1
    publisher = pubsub_v1.PublisherClient()
    topic_path = 'projects/mercari-us-de/topics/mario-api'
    message_json = {
        'details': {
            'action': 'slack_alert',
            'channel': '#de-content-ops',
            'crawl_id': crawl_id,
            'env': env,
            'description': message
        }
    }
    data = json.dumps(message_json).encode('utf-8')
    future = publisher.publish(topic_path, data=data)
    print('Published {} of message ID {}.'.format(data, future.result()))


def get_schedule_history(crawl_id, env):
    """
    Gets the crawl history details from the schedule history table
    @param spider: spider object of the spider which was closed
    @return: response of the query [ crawl schedule history ]
    """

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog' + "/fn"
    querystring = {
        "db": "contentdb",
        "fn": "sp_a_crawl_schedule_history_read_by_fieldname",
        "fieldname": "crawlScheduleHistoryId",
        "value": crawl_id
    }
    headers = {
        'Authorization': 'Bearer {}'.format(Config().get_jwt_token(env=env).decode("utf-8"))
    }
    response = requests.request("GET", url=url, params=querystring, headers=headers)
    print('Crawl data get resposne from DB: {}'.format(response.text))
    return response


def update_schedule_history(crawl_id, response, crawl_complete_status, report, env):
    """
    Upload the crawl history with the report to the schedule history table.
    @param spider: crawl end time
    @param response: crawler history to upload
    @param crawl_complete_status: crawl complete status based on schema
                                    check
    """
    response = response[0]
    payload = {
        "crawlScheduleHistoryId": crawl_id,
        "crawl_type": response.get('crawl_type', ''),
        "targetResourceType": response.get('targetResourceType'),
        "name": crawl_id,
        # "settings": {"crawl_settings": response.get('settings')},
        "settings": response.get('settings', {}),
        "task_starting_date": response.get('task_starting_date', ''),
        # "task_starting_date": datetime.strptime(
        #         self.start_time, "%Y-%m-%d-%H-%M-%S").strftime(
        #         "%Y-%m-%d %H:%M:%S+0000"),
        "task_ending_date": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S+0000"),
        "node_id": response.get('node_id', ''),
        "task_completed": crawl_complete_status,
        "details": {"crawl_details": report},
        "size": response.get('size', ''),
        "crawl_triggered_by": response.get('crawl_triggered_by', ''),
        "crawlDataId": response.get('crawlDataId', ''),
        "active": "true",
    }

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog' + "/fn"
    querystring = {
        "db": "contentdb",
        "fn": "sp_a_crawl_schedule_history_update",
    }
    headers = {
        'Content-Type': "application/json",
        'Authorization': 'Bearer {}'.format(Config().get_jwt_token(env=env).decode("utf-8"))
    }
    response = requests.request("POST", url=url, params=querystring, data=json.dumps(payload), headers=headers)
    print('Crawl data posted in DB: {}'.format(response.text))


def construct_contract_result(status, reason, callback_name):
    callback_name = str(callback_name)
    try:
        spider_name = re.findall(r"'\w+'", callback_name)[0].replace("'", '')
    except IndexError:
        spider_name = re.findall(r"'\w+", callback_name)[0].replace("'", '')
    function_name = re.findall(r"\w+ \bof\b", callback_name)[0]
    function_name = re.sub(r'\bof\b', '', function_name)
    spider_name = re.sub(re.compile(r"\bof\b|<|'"), '', spider_name)

    data = {
        'status': status,
        'reason': reason,
        'spider_name': spider_name.strip(),
        # 'spider_class_name': spider_class_name.strip(),
        'callback_name': function_name.strip(),
        'timestamp': datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    }
    print(json.dumps(data))
    return data


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--app_name', required=True,
                        help='enter the scrapy project/app name)')
    args = parser.parse_args()
    app_name = args.app_name

    # from os.path import dirname as up
    project_dir = os.path.abspath(os.path.dirname(sys.argv[0])).split('/common')[0]

    os.chdir(f"{project_dir}/{app_name}/scrape/")
    # directory = os.path.join(os.getcwd()+'/{}'.format(project), 'scrape')
    # os.chdir(directory)
    report = scrapy_check()
    print( {app_name: report} )