from scrapy.contracts import Contract
from scrapy.exceptions import ContractFail
import json
from scrapy import contracts
import random
from scrapy.item import BaseItem
# from scrapy.http import Request
from common.scrapy_contracts.contracts_util import construct_contract_result
from scrapy.contracts.default import ReturnsContract
import re
import os
from functools import wraps
from google.cloud import storage
import os
import urllib


def set_contracts_mock_values(func):
    @wraps(func)
    def wrapper(self, response):
        meta = response.meta
        for key, value in meta.get('add_to_meta', {}).items():        
            setattr(self, key, value)       
        return func(self, response)
    return wrapper

class AddRequestArgs(Contract):

    # adjust_request_args

    # {'callback': , 'method': 'GET', 'headers': None, 'body': None, 'cookies': None, 'meta': None, 'encoding': 'utf-8', 'priority': 0, 'dont_filter': True, 'errback': None, 'flags': None, 'cb_kwargs': None, 'url': ''}


    """ Contract to set the keyword arguments for the request.
        The value should be a JSON-encoded dictionary, e.g.:

        @meta {"key": "some value"}
    """

    name = 'meta'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    def adjust_request_args(self, args):
        meta_values = json.loads(' '.join(self.args))
        if meta_values.get('use_proxy', '') == 'True':
            meta_values.pop('use_proxy', None)
            meta_values.update({'proxy': random.choice(self.proxy_pool)})
        args.update({'meta': meta_values})

        if 'request_method' in meta_values:
            args['method'] = meta_values.get("request_method","GET")
        return args


class Headers(Contract):
    """ Contract to set the keyword arguments for the request.
        The value should be a JSON-encoded dictionary, e.g.:

        @headers {"key": "some value"}
    """

    name = 'headers'

    def adjust_request_args(self, args):

        headers, add_to_meta = UtilClass.get_request_args(str(args['callback']), 'headers')

        if headers:
            args['headers'] = headers 
            args['meta'].update({'headers': headers})
        else:
            args['headers'] = json.loads(' '.join(self.args))

        if add_to_meta:
            args['meta'].update({"add_to_meta": add_to_meta})
        return args


class UtilClass:
    """
    Download the config file and returns the request argument needed for the contracts url
    """
    
    def get_spider_configs():
        env = os.getenv('env', 'dev')
        cwd = os.getcwd()
        base_directory = cwd.split("/us/")[0]
        destination_filename = f'{base_directory}/us/common/scrapy_contracts/mock/spider_configs.json'
        gcs_path = f'gs://content_us/raw/spider_contracts/{env}/configs/mock/spider_configs.json'
        if not os.path.exists(destination_filename):
            o=urllib.parse.urlsplit(gcs_path)
            path = o.path.split('/', 1)[-1]
            bucket_name = o.netloc
            client = storage.Client()
            bucket = client.get_bucket(bucket_name)
            blob = bucket.blob(path)
            blob.download_to_filename(destination_filename)

        with open(destination_filename) as config_file:
            config_json = json.load(config_file)
            return config_json


    def get_request_args(callback, argument):
        spider_and_callback_name  = re.findall(r'bound method (.*) of <', callback, flags=re.IGNORECASE)[0].split('.')
        spiderclass_name = spider_and_callback_name[0]
        callback_name = spider_and_callback_name[1]
        cwd = os.getcwd()
        base_directory = cwd.split("/us/")[0]
        cwd = os.getcwd()
        config_json = UtilClass.get_spider_configs()
        request_arg = config_json.get(spiderclass_name, {}).get(callback_name, {}).get('mock_data', {}).get(argument, '')
        add_to_meta = config_json.get(spiderclass_name, {}).get(callback_name, {}).get('add_to_meta', {})

        return request_arg, add_to_meta


class AddBody(Contract):
    """ Contract to set the keyword arguments for the request.
        The value should be a JSON-encoded dictionary, e.g.:

        @body {"key": "some value"}
    """

    name = 'body'

    def adjust_request_args(self, args):
        body, add_to_meta = UtilClass.get_request_args(str(args['callback']), 'body')
        if body:
            args['body'] = json.dumps(body) 
        else:
            args['body'] = ''.join(self.args)

        if add_to_meta:
            args['meta'].update({"add_to_meta": add_to_meta})
        return args


class ScrapesValues(Contract):
    """ Contract to check presence of fields in scraped items
        @scrapes_values page_name page_body
    """

    name = 'scrape_values'

    def adjust_request_args(self, args):
        self.passed_arguments = args
        return args


    def post_process(self, output):
        
        if not output:
            construct_contract_result(status='Failure', reason='Missing Item:value is empty', callback_name=self.passed_arguments['callback'])
            raise ContractFail("Missing Item:value is empty")

        missing_field = []
        missing_values = []
        x = output[0]
        if isinstance(x, (BaseItem, dict)):
            missing_field, missing_values =  self.scrapped_items(x)
        else:
            item = {'meta': x.meta} # It should be initialized like this because in self.args we will get the item name as it as
                                    # mentioned in the @scrape_values contracts in spider docstring.
                                    # Suppose we mentioned it as meta.title  we will get as meta.title in self.args incase we are 
                                    # storing the title in meta['item_info]['title'],
                                    #  we will declare it as meta.item_info.title in docstring and receive it as it as in self.args. 
                                    # Here if we assign x.meta to the meta key again, we will use 
                                    # the same logic we used for checking the nested dict item values. if we don't assign to
                                    #  meta we need to remove "meta." in self.args items in the tuple and need to do some more code changes.
                                    #  By assigning x.meta to the meta key we can reuse the code we used for checking nested dict.
            missing_field, missing_values =  self.scrapped_items(item)

        if missing_field:

            fields_present = list(set(self.args) - set(missing_field))
            construct_contract_result(status='Failure', reason='Missing Fields are '+', '.join(missing_field)+'. Fields Present are '+', '.join(fields_present),
                            callback_name=self.passed_arguments['callback'])
            raise ContractFail(
                "Missing fields: %s" % ", ".join(missing_field))
        else:
            construct_contract_result(status='Success', reason='All the Fields are present. Checked fields are '+ ', '.join(self.args),
                            callback_name=self.passed_arguments['callback'])

        if missing_values:
            values_present = list(set(self.args) - set(missing_values))
            construct_contract_result(status='Failure', reason='Missing Values are '+', '.join(missing_values)+'. Values are present for these fields: '+', '.join(values_present),

                            callback_name=self.passed_arguments['callback'])
            raise ContractFail(
                "Missing values: %s" % ", ".join(missing_values))
        else:
            construct_contract_result(status='Success', reason='All the Values are present for Mentioned Fields. Checked fields are '+ ', '.join(self.args),
                        callback_name=self.passed_arguments['callback'])


    def scrapped_items(self, x):
        missing_values = []
        missing_field = []
        for arg in self.args:
            if "." in arg:
                keys = arg.split('.')
                for i in range(0, len(keys)):
                    keys[i] = keys[i].replace('\s',' ')
            else:
                keys = [arg.replace('\s',' ')]

            arg = arg.replace('\\s',' ')

            def recursive_get(d, keys):
                if len(keys) == 1:
                    try:
                        return d[keys[0]]
                    except KeyError:
                        missing_field.append(arg)
                else:
                    try:
                        return recursive_get(d[keys[0]], keys[1:])
                    except KeyError:
                        missing_field.append(arg)

            returned_values = recursive_get(x, keys)
            if not returned_values:
                missing_values.append(arg)

        return missing_field, missing_values



class CustomReturnsContract(ReturnsContract):
    """ Contract to check the output of a callback

        general form:
        @returns request(s)/item(s) [min=1 [max]]

        e.g.:
        @returns request
        @returns request 2
        @returns request 2 10
        @returns request 0 10
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def adjust_request_args(self, args):
        self.passed_arguments = args
        return args

    def post_process(self, output):
        occurrences = 0
        for x in output:
            if (hasattr(self, 'obj_type') and isinstance(x, self.obj_type)) or (hasattr(self, 'obj_type_verifier') and self.obj_type_verifier(x)):
                occurrences += 1

        assertion = (self.min_bound <= occurrences <= self.max_bound)

        if self.min_bound == self.max_bound:
            expected = self.min_bound
        else:
            expected = '%s..%s' % (self.min_bound, self.max_bound)

        if not assertion:
            construct_contract_result(status='Failure', reason="Returned %s %s, expected %s" % (
                occurrences, self.obj_name, expected), callback_name=self.passed_arguments['callback'])
            raise ContractFail(
                "Returned %s %s, expected %s" % (occurrences, self.obj_name, expected))
        else:
            construct_contract_result(status='Success', reason='Expected Number of {type} are {expected}.Total Number of {type} are {occurence}'.format(
                type=self.obj_name, occurence=occurrences,expected=expected), callback_name=self.passed_arguments['callback'])

