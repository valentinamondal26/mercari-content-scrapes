from __future__ import print_function

import gzip
import logging
import os
from email.utils import mktime_tz, parsedate_tz
from importlib import import_module
from time import time
from warnings import warn
from weakref import WeakKeyDictionary
from google.cloud import storage
from six.moves import cPickle as pickle
from w3lib.http import headers_raw_to_dict, headers_dict_to_raw
from scrapy.exceptions import ScrapyDeprecationWarning
from scrapy.http import Headers, Response
from scrapy.responsetypes import responsetypes
from scrapy.utils.httpobj import urlparse_cached
from scrapy.utils.project import data_path
from scrapy.utils.python import to_bytes, to_unicode, garbage_collect
from scrapy.utils.request import request_fingerprint

import tarfile
import shutil
from zipfile import ZipFile


logger = logging.getLogger(__name__)


class FilesystemCacheStorage(object):

    def __init__(self, settings):
        self.cachedir = data_path(settings['HTTPCACHE_DIR'])
        self.expiration_secs = settings.getint('HTTPCACHE_EXPIRATION_SECS')
        self.http_cache_gcs_path = settings.get('GCS_CACHE_LOCATION')
        self.use_gzip = settings.getbool('HTTPCACHE_GZIP')
        self.http_enabled = settings.get('HTTPCACHE_ENABLED')
        # self.spider_project_name = settings.get('BOT_NAME')
        self._open = gzip.open if self.use_gzip else open


    def open_spider(self, spider):
        if self.http_enabled:
            self.cachedir = self.cachedir.format(category=spider.category, brand=spider.brand)
            if self.http_cache_gcs_path:
                spider_project_name = spider.name # self.spider_project_name
                cachedir = os.path.join(os.getcwd(), ".scrapy", "httpcache", spider.category, spider.brand)

                if not os.path.isdir(cachedir):
                    os.makedirs(cachedir)

                storage_client = storage.Client()
                gcs_cache_file_name = spider_project_name+".tar.gz"
                bucket = storage_client.get_bucket("content_us")
                blob = bucket.blob(self.http_cache_gcs_path.split('content_us/')[1])

                if blob.exists():
                    local_filename = cachedir + "/cache_file.tar.gz"
                    blob.download_to_filename(local_filename)
                    my_tar = tarfile.open(local_filename)
                    my_tar.extractall(cachedir)
                    os.remove(local_filename)
                    source = os.path.join(cachedir, gcs_cache_file_name, spider_project_name)
                    dest = os.path.join(os.getcwd(), ".scrapy", "httpcache", spider.category, spider.brand)
                    try:
                        shutil.rmtree(os.path.join(cachedir, spider_project_name)) ###removing default cache memory folder
                    except OSError:
                        pass
                    # shutil.rmtree(os.path.join(cachedir, 'cache_file.tar.gz'))
                    shutil.move(source, dest) ###moving downloaded file to removed defaullt cache memory location '/httpcache'
                    shutil.rmtree(os.path.join(cachedir, gcs_cache_file_name))
                else:
                    logger.debug("Blob doesn't exist")

                logger.debug("Using filesystem cache storage in %(cachedir)s" % {'cachedir': self.cachedir},
                            extra={'spider': spider})
            else:
                logger.debug("cache memory is not downloaded from gcs")
                logger.debug("Data in local storage used for caching")


    def close_spider(self, spider):
        pass


    def retrieve_response(self, spider, request):
        """Return response if present in cache, or None otherwise."""

        metadata = self._read_meta(spider, request)
        if metadata is None or request.meta.get("use_cache",False) == False or metadata.get('status',"") != 200:
            return  # not cached
        rpath = self._get_request_path(spider, request)
        with self._open(os.path.join(rpath, 'response_body'), 'rb') as f:
            body = f.read()
        with self._open(os.path.join(rpath, 'response_headers'), 'rb') as f:
            rawheaders = f.read()
        url = metadata.get('response_url')
        status = metadata['status']
        headers = Headers(headers_raw_to_dict(rawheaders))
        respcls = responsetypes.from_args(headers=headers, url=url)
        response = respcls(url=url, headers=headers, status=status, body=body)
        return response


    def store_response(self, spider, request, response):
        """Store the given response in the cache."""

        if response.status == 200: ###storing response only for status code 200
            rpath = self._get_request_path(spider, request)
            if not os.path.exists(rpath):
                os.makedirs(rpath)
            metadata = {
                'url': request.url,
                'method': request.method,
                'status': response.status,
                'response_url': response.url,
                'timestamp': time(),
            }
            with self._open(os.path.join(rpath, 'meta'), 'wb') as f:
                f.write(to_bytes(repr(metadata)))
            with self._open(os.path.join(rpath, 'pickled_meta'), 'wb') as f:
                pickle.dump(metadata, f, protocol=2)
            with self._open(os.path.join(rpath, 'response_headers'), 'wb') as f:
                f.write(headers_dict_to_raw(response.headers))
            with self._open(os.path.join(rpath, 'response_body'), 'wb') as f:
                f.write(response.body)
            with self._open(os.path.join(rpath, 'request_headers'), 'wb') as f:
                f.write(headers_dict_to_raw(request.headers))
            with self._open(os.path.join(rpath, 'request_body'), 'wb') as f:
                f.write(request.body)


    def _get_request_path(self, spider, request):
        key = request_fingerprint(request)
        return os.path.join(self.cachedir, spider.name, key[0:2], key)


    def _read_meta(self, spider, request):
        rpath = self._get_request_path(spider, request)
        metapath = os.path.join(rpath, 'pickled_meta')
        if not os.path.exists(metapath):
            return  # not found
        mtime = os.stat(metapath).st_mtime
        if 0 < self.expiration_secs < time() - mtime:
            return  # expired
        with self._open(metapath, 'rb') as f:
            return pickle.load(f)
