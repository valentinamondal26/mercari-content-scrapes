import time
import argparse
import google.auth.crypt
import google.auth.jwt
import os

class Config:
    prod_key = os.getenv('GOOGLE_APPLICATION_CREDENTIALS')

    prod_account = 'content-pipeline@mercari-us-de.iam.gserviceaccount.com'

    headers = {'Content-Type': "application/json"}

    contentdb_endpoint = 'https://kg-{env}.endpoints.mercari-us-de.cloud.goog'



    def generate_jwt(self,sa_keyfile,
                    sa_email='account@project-id.iam.gserviceaccount.com',
                    audience='your-service-name',
                    expiry_length=3600):
        """
        Generate the jwt token for the given service account
        :param sa_keyfile: service account key file path
        :param sa_email: service account mail id
        :param audience: endpoint
        :param expiry_length: in seconds
        :return:
        """
        now = int(time.time())
        payload = {
            'iat': now,
            "exp": now + expiry_length,
            'iss': sa_email,
            'aud':  audience,
            'sub': sa_email,
            'email': sa_email
        }
        signer = google.auth.crypt.RSASigner.from_service_account_file(sa_keyfile)
        jwt = google.auth.jwt.encode(signer, payload)

        return jwt

    def get_jwt_token(self, env):
        key = self.prod_key
        service_account = self.prod_account
        jwt = self.generate_jwt(sa_keyfile=key,
                        sa_email=service_account,
                        audience=self.contentdb_endpoint.format(env=env),
                        expiry_length=3600)
        return jwt


