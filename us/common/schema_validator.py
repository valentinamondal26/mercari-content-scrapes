import json
import jsonschema
from jsonschema import validate


class SchemaValidator:
    record = None

    def __init__(self, record):
        self.record = record
        return

    def process(self, schema):
        schema_filter = Filter(schema)
        schema_valid = schema_filter.filter(self.record)
        return schema_valid

"""[Schema Validator]

Returns:
    [Boolean] -- [Returns the boolean value for the validation of the record against the schema]
"""
class Filter:
    schema = None

    def __init__(self, schema):
        self.schema = schema
        return

    def filter(self, record):
        try:
            validate(record, self.schema)
        except jsonschema.ValidationError as e:
            return False
        except json.decoder.JSONDecodeError as e:
            return False
        return True
