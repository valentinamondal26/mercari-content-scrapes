import argparse
import json
import os
import subprocess
import logging
import sys
from datetime import datetime
from scrapy_contracts.contracts_util import scrapy_check, gcs_upload_data, gcs_upload_file, process_result, notify_slack_alert, get_schedule_history, update_schedule_history
import re

def initialize_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    file_handler = logging.FileHandler('debug.log', mode='w+')
    formatter = logging.Formatter(
        "%(asctime)s - [%(filename)s:%(lineno)s - %(funcName)20s() ] - %(levelname)s - %(message)s")
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.INFO)
    logger.addHandler(file_handler)

    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(formatter)
    logger.addHandler(consoleHandler)


class Main(object):

    def __init__(self, payload):
        super().__init__()
        self.payload = payload

    def execute_command(self, preexec_fn=False) -> None:
        """
        Executes the scrapy crawl command based on the app_name in the payload
        @param preexec_fn: boolean flag for child processes to be trivial
        """

        project_dir = os.getcwd()
        os.environ['project_dir'] = project_dir
        logging.info(f'Project Directory: {project_dir}')

        os.environ['is_slack_alert_notification_enabled'] = str(True)

        env = os.getenv('env', '')
        logging.info(f'Environment: {env}')
        if not env:
            logging.error('Environment(env) is not set or missing, so could not able to proceed')
            exit(1)

        payload_json = json.loads(self.payload)
        logging.info(f'payload:{payload_json}')

        execution_timestamp = payload_json.get('time_stamp') \
            or datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        logging.info(f'Execution Timestamp: {execution_timestamp}')

        crawl_id = payload_json.get('crawl_id','')

        app_name = payload_json.get('app_name', '')

        response = None
        if crawl_id:
            response = get_schedule_history(crawl_id=crawl_id, env=env)

        if app_name == "crawl_monitor":
            app_list = list(map(lambda x: x.strip(), payload_json.get('app_list', '').split(',')))
            results = []
            for app in app_list:
                os.chdir(f"{project_dir}/{app}/scrape/")
                logging.info(os.getcwd())
                report = scrapy_check()
                results.append( {app: report} )
            processed_result = process_result(results)
            logging.info(f'processed result:{processed_result}')
            result = json.dumps(processed_result)
            daily_reports_path = f'raw/unit_test_results/{env}/daily_report/{execution_timestamp}/reports.txt'
            daily_results_path = f'raw/unit_test_results/{env}/daily_report/{execution_timestamp}/results.json'
            logs_path = f'raw/unit_test_results/{env}/daily_report/{execution_timestamp}/debug.log'
            gcs_upload_data(
                json.dumps(results),
                destination_blob_name=daily_reports_path
            )
            gcs_upload_data(
                result,
                destination_blob_name=daily_results_path
            )
            gcs_upload_data(
                result,
                destination_blob_name=f'raw/unit_test_results/{env}/report/results.json'
            )

            # upload logs
            gcs_upload_file(
                f'{project_dir}/debug.log',
                destination_blob_name=logs_path
            )

            report = {
                'daily_reports_path': daily_reports_path,
                'daily_results_path': daily_results_path,
                'logs_path': logs_path,
            }
            if crawl_id and response:
                # uploading the crawl status to crawl schedule history
                update_schedule_history(crawl_id=crawl_id, response=json.loads(response.text),
                    crawl_complete_status=str(True).lower(), report=report, env=env)

            # slack alert notification
            is_slack_alert_notification_enabled = bool(os.getenv('is_slack_alert_notification_enabled', ''))
            if is_slack_alert_notification_enabled:
                notification_message = f"Crawl Monitor Job Completed Successfully. Report Written to path gs://content_us/raw/unit_test_results/{env}/report/results.json"
                notify_slack_alert(message=notification_message, crawl_id=crawl_id, env=env)

        elif app_name == "generic":
            os.chdir(f"{project_dir}/generic/scrape/")
            cmd = 'python crawl_script.py  --brand="{brand}" --xpath_json="{xpath_json}" --timestamp="{time_stamp}"'
            cmd=cmd.split()
            cmd='%'.join(cmd).format(brand=payload_json.get('brand', ''), xpath_json=payload_json.get('xpath_json', ''), time_stamp=execution_timestamp)
            cmd=cmd.split('%')
            self.crawl_spider(cmd, payload_json, preexec_fn, response)
        else:
            os.chdir(project_dir + "/{app_name}/scrape/".format(app_name=payload_json.get('app_name', '')))
            cmd = 'python -m scrapy crawl {spider_name} -a category={category} -a brand={brand}  -s CRAWL_ID={crawl_id} -s GCS_BLOB_TIMESTAMP={time_stamp} -s MERGE_KEY={merge_key} -s GCS_CACHE_LOCATION={gcs_cache_location}'
            cmd=cmd.split()
            cmd='%'.join(cmd).format(
                    category=payload_json.get('category',''),
                    crawl_id=payload_json.get('crawl_id',''),
                    brand=payload_json.get('brand', ''),
                    spider_name=payload_json.get('spider_name', ''),
                    time_stamp=execution_timestamp,
                    merge_key=payload_json.get('merge_key', ''),
                    gcs_cache_location=payload_json.get('gcs_cache_location', '')
                )
            cmd=cmd.split('%')
            for key, value in payload_json.items():
                cmd.append("-a")
                cmd.append(f"{key}={value}")
            self.crawl_spider(cmd, payload_json, preexec_fn, response)


    def crawl_spider(self, cmd, payload_json, preexec_fn, response=None):
        logging.info('Executing {} crawl : {}\n'.format(payload_json.get('spider_name',''), cmd))
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             stdin=subprocess.PIPE,
                             preexec_fn=os.setsid if preexec_fn else None)

        with p.stdout:
            log_subprocess_output_pipe(p.stdout, payload_json, response)
        exitcode = p.wait() # 0 means success
        logging.info(f'process exited with code: {exitcode}')


    def main(self, payload):
        self.execute_command()


def log_subprocess_output_pipe(pipe, payload_json, response=None):
    for line in iter(pipe.readline, b''): # b'\n'-separated lines
        # logging.info(f"got line from subprocess: {line.decode('utf-8').strip()}")
        line = line.decode('utf-8').strip()
        if re.findall(r'\bscrapy\.exceptions\b|Error', line, flags=re.IGNORECASE):
            crawl_id = payload_json.get('crawl_id','')
            env=os.environ.get('env', '')
            if crawl_id:
                update_schedule_history(crawl_id=crawl_id, response=json.loads(response.text),
                    crawl_complete_status=str(False).lower(), report= "Category and brand Mismatch with spider configs and crawler configuration id in DB", env=env)

        logging.info(line)


if __name__ == '__main__':
    """Execute a crawler
    Sample Payload: '{"app_name":"","brand":"","category":"","crawlSource":"","spider_name":""}'

    Args:
        payload: payload to be passed to the crawler

    Returns:
        The result will be uploaded to the gcs bucket
    """

    initialize_logging()

    parser = argparse.ArgumentParser(description='Crawler Process')
    parser.add_argument('--payload', required=True, help='Enter Payload')
    args = parser.parse_args()
    Main(args.payload).execute_command()
