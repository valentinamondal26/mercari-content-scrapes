from scrapy.exceptions import NotConfigured

import os
from w3lib.http import basic_auth_header
import base64


class HttpProxyMiddleware(object):

    import enum
    class ProxyRotationType(enum.Enum):
        RANDOM = 'RANDOM'
        ROUND_ROBIN = 'ROUND_ROBIN'

    def __init__(self, proxy_enabled=True, proxy_list=[], proxy_rotation_type=ProxyRotationType.RANDOM):
        self.proxy_enabled = proxy_enabled
        self.proxies = proxy_list
        # self.proxy_rotation_type = ProxyRotationType[proxy_rotation_type]
        self.creds = basic_auth_header(
            base64.b64decode(os.environ.get('PROXY_USERNAME', '')).decode('utf-8'),
            base64.b64decode(os.environ.get('PROXY_PWD', '')).decode('utf-8')
        )

    @classmethod
    def from_crawler(cls, crawler):
        proxy_enabled = crawler.settings.getbool('HTTPPROXY_ENABLED')
        # if not proxy_enabled:
        #     raise NotConfigured

        proxy_list = crawler.settings.get('HTTPPROXY_LIST')
        proxy_rotation_type = crawler.settings.get('HTTPPROXY_ROTATION_TYPE')
        return cls(proxy_enabled, proxy_list, proxy_rotation_type)


    def process_request(self, request, spider):
        if not self.proxy_enabled:
            return

        # ignore if proxy is already set
        if 'proxy' in request.meta:
            if request.meta['proxy'] is None:
                return

            # add Proxy-Authorization if not present
            if self.creds and not request.headers.get('Proxy-Authorization'):
                request.headers['Proxy-Authorization'] = self.creds
            return
        elif self.proxies:
            import random
            request.meta['proxy'] = random.choice(self.proxies)
            if self.creds:
                request.headers['Proxy-Authorization'] = self.creds
            return


    # def _get_proxy(self):
    #     proxy = ''
    #     if ProxyRotationType.RANDOM:
    #         import random
    #         proxy = random.choice(self.proxies)
    #     elif ProxyRotationType.ROUND_ROBIN:
    #         proxy = next(self.proxies)


    # def _set_proxy(self, request, scheme):
    #     creds, proxy = self.proxies[scheme]
    #     request.meta['proxy'] = proxy
    #     if creds:
    #         request.headers['Proxy-Authorization'] = b'Basic ' + creds
