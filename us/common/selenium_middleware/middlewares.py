"""This module contains the ``SeleniumMiddleware`` scrapy middleware"""

from importlib import import_module

from scrapy import signals
from scrapy.exceptions import NotConfigured
from scrapy.http import HtmlResponse

from .http import SeleniumRequest

from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.common.exceptions import TimeoutException

# from pylru import lrucache

import os
import base64


class SeleniumMiddleware:
    """Scrapy middleware handling the requests using selenium"""

    def __init__(self, driver_name='chrome', user_agent=''):
        """Initialize the selenium webdriver
        Parameters
        ----------
        driver_name: str
            The selenium ``WebDriver`` to use
        user_agent: str
            user-agent

        CAUTION: A new driver will be created per proxy, so this will increase the resource
        utilization, which decrease the stability of the driver. So please be cautious while
        using multiple proxies

        """
        # self.drivers = lrucache(max_concurrent_driver, self.on_driver_removed)
        self.drivers = {}
        self.user_agent = user_agent

        # self.drivers[''] = self.create_driver(proxy=None, user_agent=self.user_agent)


    # def enter_proxy_auth(self, proxy_username, proxy_password): 
    #     import time 
    #     import pyautogui 
    
    #     time.sleep(1) 
    #     pyautogui.typewrite(proxy_username) 
    #     pyautogui.press('tab') 
    #     pyautogui.typewrite(proxy_password) 
    #     pyautogui.press('enter') 


    def get_proxy_chrome_extension_encoded(self, proxy, proxy_username, proxy_password):
        PROXY_HOST = proxy.rsplit(':', 1)[0].replace('http://', '').replace('https://', '')  # rotating proxy or host
        PROXY_PORT = int(proxy.rsplit(':', 1)[-1]) # port
        PROXY_USER = proxy_username # username
        PROXY_PASS = proxy_password # password

        manifest_json = """
        {
            "version": "1.0.0",
            "manifest_version": 2,
            "name": "Chrome Proxy",
            "permissions": [
                "proxy",
                "tabs",
                "unlimitedStorage",
                "storage",
                "<all_urls>",
                "webRequest",
                "webRequestBlocking"
            ],
            "background": {
                "scripts": ["background.js"]
            },
            "minimum_chrome_version":"22.0.0"
        }
        """

        background_js = """
        var config = {
                mode: "fixed_servers",
                rules: {
                singleProxy: {
                    scheme: "http",
                    host: "%s",
                    port: parseInt(%s)
                },
                bypassList: ["localhost"]
                }
            };

        chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

        function callbackFn(details) {
            return {
                authCredentials: {
                    username: "%s",
                    password: "%s"
                }
            };
        }

        chrome.webRequest.onAuthRequired.addListener(
                    callbackFn,
                    {urls: ["<all_urls>"]},
                    ['blocking']
        );
        """ % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)

        import base64
        from io import BytesIO
        import zipfile

        mem_zip = BytesIO()
        with zipfile.ZipFile(mem_zip, mode="w", compression=zipfile.ZIP_DEFLATED) as zf:
            zf.writestr("manifest.json", manifest_json)
            zf.writestr("background.js", background_js)
        return base64.b64encode(mem_zip.getvalue()).decode('utf-8')


    @classmethod
    def from_crawler(cls, crawler):
        """Initialize the middleware with the crawler settings"""

        driver_name = crawler.settings.get('SELENIUM_DRIVER_NAME', '')
        user_agent = crawler.settings.get('USER_AGENT', '')

        middleware = cls(
            driver_name=driver_name,
            user_agent=user_agent,
        )

        crawler.signals.connect(middleware.spider_closed, signals.spider_closed)

        return middleware


    # def on_driver_removed(self, proxy, driver):
    #     """
    #     Closes the webdriver when evicted from the cache.
    #     :param proxy: the proxy, not used
    #     :param driver: the driver being evicted
    #     """
    #     driver.quit()


    def create_driver(self, user_agent, proxy):
        """
        Creates a new driver, with optional proxy.
        :param proxy: the proxy, which should be something like http://... or https://... or socks://
        :return: a webdriver created with the provided proxy

        """

        options = Options()
        options.headless = True

        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument(f'--user-agent={user_agent}')

        from seleniumwire import webdriver
        seleniumwire_options = {}
        if proxy:
            proxy_username=base64.b64decode(os.environ.get('PROXY_USERNAME', '')).decode('utf-8')
            proxy_password=base64.b64decode(os.environ.get('PROXY_PWD', '')).decode('utf-8')

            seleniumwire_options = {
                'proxy': {
                    'http': f"http://{proxy_username}:{proxy_password}@{proxy.split('://')[-1]}",
                    'https': f"https://{proxy_username}:{proxy_password}@{proxy.split('://')[-1]}",
                    'no_proxy': 'localhost,127.0.0.1' # excludes
                }
            }

            # NOTE: There is a limitation in Chrome Extension of not working/not supported
            # for the headless mode. So seleniumwire is used instead to solve this problem.
            # But need to find a way to send the proxy authentication using simply selenium driver.

            # options.add_encoded_extension(
            #     self.get_proxy_chrome_extension_encoded(
            #         proxy,
            #         proxy_username=proxy_username,
            #         proxy_password=proxy_password
            #     )
            # )

        # driver = webdriver.Chrome(chrome_options=options)
        driver = webdriver.Chrome(chrome_options=options, seleniumwire_options=seleniumwire_options)
        return driver


    def process_request(self, request, spider):
        """Process a request using the selenium driver if applicable"""

        if not isinstance(request, SeleniumRequest):
            if os.environ.get('SCRAPY_CHECK', '') and request.meta.get('use_selenium', False):
                return  SeleniumRequest(url=request.url,
                    callback=request.callback,
                    wait_time=request.meta.get('wait_time', None),
                    wait_until=request.meta.get('wait_until', None),
                    screenshot=request.meta.get('screenshot', None),
                    script=request.meta.get('script', None),
                    perform_action=request.meta.get('perform_action',
                    None)
                )
            else:
                return None

        # request a proxy:
        proxy = request.meta.get('proxy', '')
        if proxy not in self.drivers:
            # this proxy is new, create a driver with this proxy
            driver = self.create_driver(proxy=proxy, user_agent=self.user_agent)
            self.drivers[proxy] = driver

        driver = self.drivers[proxy]

        # fetch the url
        driver.get(request.url)

        #enter the proxy authentication url
        # self.enter_proxy_auth(proxy_username='', proxy_password='')

        for cookie_name, cookie_value in request.cookies.items():
            driver.add_cookie(
                {
                    'name': cookie_name,
                    'value': cookie_value
                }
            )

        if request.wait_until:
            try:
                WebDriverWait(driver, request.wait_time).until(
                    request.wait_until
                )
            except TimeoutException:
                spider.logger.error(f'Error: Exception while processing url: {request.url}')
                raise
        
        if request.perform_action:
            for action in request.perform_action:
                element = driver.find_element(action['element_type'], action['element_path'])
                if action['type'] == 'click':
                    element.click()


        if request.screenshot:
            request.meta['screenshot'] = driver.get_screenshot_as_png()

        if request.script:
            driver.execute_script(request.script)

        body = driver.page_source

        # Expose the driver via the "meta" attribute
        request.meta.update({'driver': driver})

        headers = {
            'Content-Encoding': 'identity'
        }

        return HtmlResponse(
            driver.current_url,
            body=body,
            encoding='utf-8',
            request=request,
            headers=headers
        )

    def spider_closed(self):
        """Shutdown the driver when spider is closed"""

        for driver in self.drivers.values():
            driver.quit()
        # self.drivers.clear()

