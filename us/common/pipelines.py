import json
import re
from datetime import datetime
from io import BytesIO
from itertools import groupby
from operator import itemgetter
from typing import List, Dict, Tuple

import numpy as np
import pandas as pd
import requests
from genson import SchemaBuilder
from google.cloud import storage
from pydispatch import dispatcher
from requests import Response
from scrapy import Spider
from scrapy import signals
from scrapy.exporters import JsonLinesItemExporter

from .configs import Config
from .report_writer import ReportWriter
from .schema_validator import SchemaValidator

# from jsonschema import validate
# from scrapy.exporters import JsonLinesItemExporter
# from scrapy.crawler import CrawlerProcess
# from scrapy.crawler import CrawlerProcess
# from scrapy.exceptions import CloseSpider
import tarfile
import os


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


class GCSPipeline(object):


    def __init__(self, gcs_blob_prefix, gcs_blob_timestamp, crawl_id, stats, merge_key,
            http_cache_directory='httpcache'):

        self.gcs_blob_prefix = gcs_blob_prefix
        self.gcs_blob_timestamp = gcs_blob_timestamp
        self.http_cache_directory = http_cache_directory
        self.crawl_id = crawl_id
        self.bucket_name = 'content_us'
        self.client = None
        self.end_time = None
        self.bucket = None
        self.schema = None
        self.start_time = None
        self.items = []
        self.merge_key = merge_key
        self.cfg = Config()
        self.stats = stats
        self.report = None
        # dispatcher.connect(self.close_spider, signals.spider_closed)

        self.kg_service_endpoint = 'https://kg-{env}.endpoints.mercari-us-de.cloud.goog'.format(env=os.getenv('env', ''))


    @classmethod
    def from_crawler(cls, crawler):

        return cls(
            gcs_blob_prefix=crawler.settings.get('GCS_BLOB_PREFIX'),
            gcs_blob_timestamp=crawler.settings.get('GCS_BLOB_TIMESTAMP'),
            crawl_id=crawler.settings.get('CRAWL_ID'),
            stats=crawler.stats,
            merge_key=crawler.settings.get("MERGE_KEY"),
            http_cache_directory=crawler.settings.get("HTTPCACHE_DIR"),
        )


    def get_schedule_history(self, spider: Spider) -> Response:
        """
        Gets the crawl history details from the schedule history table
        @param spider: spider object of the spider which was closed
        @return: response of the query [ crawl schedule history ]
        """

        url = self.kg_service_endpoint + "/fn"
        querystring = {
            "db": "contentdb",
            "fn": "sp_a_crawl_schedule_history_read_by_fieldname",
            "fieldname": "crawlScheduleHistoryId",
            "value": self.crawl_id
        }
        headers = {
            'Authorization': 'Bearer {}'.format(self.cfg.get_jwt_token(env=os.getenv('env', '')).decode("utf-8"))
        }
        response = requests.request("GET", url=url, params=querystring, headers=headers)
        spider.logger.info('Crawl data get resposne from DB: {}'.format(response.text))
        return response


    def update_schedule_history(self, spider: Spider, response: Dict,
                                 crawl_complete_status: str) -> None:
        """
        Upload the crawl history with the report to the schedule history table.
        @param spider: crawl end time
        @param response: crawler history to upload
        @param crawl_complete_status: crawl complete status based on schema
                                      check
        """
        response = response[0]
        payload = {
            "crawlScheduleHistoryId": self.crawl_id,
            "crawl_type": response.get('crawl_type', ''),
            "targetResourceType": response.get('targetResourceType'),
            "name": self.crawl_id,
            # "settings": {"crawl_settings": response.get('settings')},
            "settings": response.get('settings', {}),
            "task_starting_date": datetime.strptime(
                    self.start_time, "%Y-%m-%d-%H-%M-%S").strftime(
                    "%Y-%m-%d %H:%M:%S+0000"),
            "task_ending_date": datetime.strptime(
                    self.end_time, "%Y-%m-%d-%H-%M-%S").strftime(
                    "%Y-%m-%d %H:%M:%S+0000"),
            "node_id": response.get('node_id', ''),
            "task_completed": crawl_complete_status,
            "details": {"crawl_details": self.report},
            "size": response.get('size', ''),
            "crawl_triggered_by": response.get('crawl_triggered_by', ''),
            "crawlDataId": response.get('crawlDataId', ''),
            "active": "true",
        }

        url = self.kg_service_endpoint + "/fn"
        querystring = {
            "db": "contentdb",
            "fn": "sp_a_crawl_schedule_history_update",
        }
        headers = {
            'Content-Type': "application/json",
            'Authorization': 'Bearer {}'.format(self.cfg.get_jwt_token(env=os.getenv('env', '')).decode("utf-8"))
        }
        response = requests.request("POST", url=url, params=querystring, data=json.dumps(payload).replace("'", "''"), headers=headers)
        spider.logger.info('Crawl data posted in DB: {}'.format(response.text))


    def push_to_library(self, spider: Spider, response: Dict, gcs_upload_path: str) -> None:
        """
        Upload the crawl history with the report to the library table
        @param spider: spider object of the spider which was closed
        @param response: crawler history to upload
        @param gcs_upload_path: gcs path of the crawl o/p
        """
        response = response[0]

        payload = {
                "libraryId": self.crawl_id,
                "crawlDataId": self.crawl_id,
                "sourceUrl": gcs_upload_path,
                "name": self.crawl_id,
                "settings": {"crawl_settings": response.get('settings')},
                "active": "true",
                "status": "Finished"
        }

        url = self.kg_service_endpoint + "/fn"
        querystring = {
            "db": "contentdb",
            "fn": "sp_a_library_create",
        }
        headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer {}'.format(self.cfg.get_jwt_token(env=os.getenv('env', '')).decode("utf-8"))
        }
        response = requests.request("POST", url=url, params=querystring, data=json.dumps(payload).replace("'", "''"), headers=headers)
        spider.logger.info('Crawl data posted in DB: {}'.format(response.text))


    def merge_items(self, spider: Spider, items: List, merge_key: str) -> List:
        """
        Merges the crawled items based on the unique key provided
        @param spider: Spider object of the crawler
        """
        if not items or not merge_key:
            return items

        extra_item_infos = getattr(spider, 'extra_item_infos', {})
        if extra_item_infos:
            for item in items:
                item.update(extra_item_infos.get(item.get(merge_key, ''), {}))

        items_merged = []
        df = pd.DataFrame(items)
        unique_item_keys = list(set(df[merge_key]))
        for key in unique_item_keys:
            group_df = df[df[merge_key] == key].replace({np.nan: ''})
            group_list = group_df.to_dict(orient='records')
            item = {}
            try:
                filters = spider.filters
            except Exception:
                filters = None
            if filters:
                keys = list(group_list[0].keys())
                for filter_value in filters:
                    keys.append(filter_value)
                keys = list(set(keys))
            else:
                keys = list(group_list[0].keys())
            item = item.fromkeys(keys, '')
            for d in group_list:
                for item_key, value in d.items():
                    if item_key != "id":
                        if isinstance(value, str) and item[item_key] == '':
                            item[item_key] += str(value)
                        elif not isinstance(value,
                                            str) and item[item_key] == '':
                            item[item_key] = value
                        elif isinstance(value,
                                        str) and value not in item[item_key]:
                            item[item_key] += ", " + str(value)
                    else:
                        item['id'] = d['id']
            items_merged.append(item)
        return items_merged


    def build_schema(self, keys: List) -> Dict:
        """
        Build a json schema from the schema keys in the spider
        @param keys: item keys for the schema check.
        @return: the build schema json
        """
        builder = SchemaBuilder()

        if not keys:
            builder.add_object({})
            schema = builder.to_schema()
            schema.update({"properties": {}})
            return schema
        else:
            schema_json = dict.fromkeys(keys, '')
            builder.add_object(schema_json)
            schema = builder.to_schema()
            prop = schema.get('properties', {})
            for _key, value in prop.items():
                update = {
                        "type": ["array", "string", "integer", "object",
                                 "boolean"],
                        "minLength": 1,
                        "minProperties": 1,
                        "minItems": 1,
                        "minimum": 0
                }
                value.update(update)
            return schema


    def generate_schema(self, spider: Spider) -> None:
        """
        Generate jsonschema for the spider
        @param spider: the spider object of the crawler
        @return: the generation schema
        """
        keys = getattr(spider, "schema_keys", []) or []
        self.schema = self.build_schema(keys)


    def get_schema_items(self) -> Tuple[int, int]:
        """
        Returns the schema valid and invalid items count
        @return: List of valid and in-valid counts
        """
        true_set = 0
        false_set = 0
        grouped = groupby(sorted(self.items, key=itemgetter(
                'valid')), key=itemgetter('valid'))
        for x in grouped:
            if x[0]:
                true_set = len(list(x[1]))
            else:
                false_set = len(list(x[1]))

        # we can drop the valid key from the items
        # self.items = [{k:v for k,v in d.items() if k!="valid"}
        #               for d in self.items]
        return true_set, false_set


    def generate_report_dict(self, spider: Spider, gcs_upload_path: str) -> Dict:
        """
        Generate the report json
        @param spider: spider object of the spider which was closed
        @param gcs_upload_path: gcs path of crawl o/p
        @return: Generated report json
        """
        report = ReportWriter()

        # header
        start_time = datetime.strptime(
            str(self.stats.get_value('start_time')), "%Y-%m-%d %H:%M:%S.%f")
        self.start_time = str(start_time.strftime("%Y-%m-%d-%H-%M-%S"))
        header = {
            'source': spider.source,
            'category': spider.category,
            'brand': spider.brand,
            'start_time': self.start_time,
            'spider': spider.name,
        }
        report.add_data('header', dict=header)

        # schema
        f_passed_lines, f_failed_lines = self.get_schema_items()
        item_scraped_count = self.stats.get_value('item_scraped_count', 0)
        pass_percentage = 0
        try:
            pass_percentage = (f_passed_lines/item_scraped_count)*100
        except Exception as e:
            spider.logger.error(e)
        schema = {
            'schema_passed': f_passed_lines,
            'schema_failed': f_failed_lines,
            'item_scraped_count': item_scraped_count,
            'pass_percentage': pass_percentage,
        }
        report.add_data('schema', dict=schema)

        # footer
        finish_time = self.stats.get_value('finish_time') or datetime.utcnow()
        end_time = datetime.strptime(
            str(finish_time), "%Y-%m-%d %H:%M:%S.%f")
        self.end_time = str(end_time.strftime("%Y-%m-%d-%H-%M-%S"))
        time_diff = end_time-start_time
        scrapy_crawler_stats = json.loads(json.dumps(self.stats.get_stats(), cls=DateTimeEncoder))
        footer = {
            'end_time': self.end_time,
            'crawl_time': str(time_diff),
            'gcs_upload_path': "gs://content_us/"+str(gcs_upload_path),
            'scrapy_stats': scrapy_crawler_stats,
        }
        report.add_data('footer', dict=footer)
        return report.return_report()


    def open_spider(self, spider: Spider) -> None:
        """
        Runs the below statements when the spider starts the crawl
        @param spider: spider object of the crawler
        """
        category = getattr(spider, 'category', '')
        brand = getattr(spider, 'brand', '')
        self.http_cache_directory = self.http_cache_directory.format(category=category, brand=brand)
        self.http_cache_directory = os.path.join(".scrapy", self.http_cache_directory)
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(self.bucket_name)

        if not self.merge_key:
            # if MERGE_KEY is not available from spider settings(may also dynamically passed from
            # crawl trigger command line args provided by the payload), try to get it from the spider
            self.merge_key = getattr(spider, 'merge_key', '')


    def close_spider(self, spider: Spider) -> None:
        """
        Runs the below statements when the spider finishes the crawl
        @param spider: spider object of the crawler
        """
        source = spider.source
        category = spider.category.replace(' ', '_').replace("'", '').replace('&', '').lower()
        brand = spider.brand.replace(' ', '_').replace("'", '').replace('&', '').lower()
        timestamp = self.gcs_blob_timestamp or datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        file_name = spider.blob_name

        if not category or not brand:
            spider.logger.error('category or brand is empty, so skip uploading result to gcs')
            return

        # If there's no time stamp passed in setting - then the crawl running is
        # considered as local test and current time is generated
        if not timestamp:
            timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")

        # build gcs upload paths
        gcs_reports_path = self.get_destination_blob_name(category=category, brand=brand,
            source=source, timestamp=timestamp, format="report", filename="report.json")

        gcs_upload_path = self.get_destination_blob_name(category=category, brand=brand,
            source=source, timestamp=timestamp, format="json", filename=file_name)

        gcs_logs_path = self.get_destination_blob_name(category=category, brand=brand,
            source=source, timestamp=timestamp, format="log", filename=f'{spider.name}.log')

        # generate the report json
        self.report = self.generate_report_dict(spider=spider, gcs_upload_path=gcs_upload_path)
        spider.logger.info(f'report: {self.report}')

        # check if pass percentage / schema passed is > =50 and fix the crawl
        # complete status
        crawl_complete_status = "false"
        if self.report.get('schema', {}).get('pass_percentage', 0) >= 50.0:
            crawl_complete_status = "true"

        # upload report and items json to gcs
        self.upload(upload_format="report", spider=spider, upload_path=gcs_reports_path)
        self.upload(upload_format="json", spider=spider, upload_path=gcs_upload_path)

        if self.http_cache_directory:
            self.upload_cached_response(spider, category, brand, source, timestamp)

        # If there's no crawl_id passed in the settings then the crawl is
        # considered as local test and crawl status will not be updated in the DB.
        if self.crawl_id:
            # uploading the crawl status to crawl schedule history
            response = self.get_schedule_history(spider=spider)
            self.update_schedule_history(spider=spider, response=json.loads(response.text),
                crawl_complete_status=crawl_complete_status)
            # self.push_to_library(spider=spider,response=json.loads(response.text),gcs_upload_path=gcs_upload_path)

        is_slack_alert_notification_enabled = bool(os.getenv('is_slack_alert_notification_enabled', ''))
        if is_slack_alert_notification_enabled and  crawl_complete_status == "true":
            self.notify_slack_alert(spider)

        self.upload(upload_format="log", spider=spider, upload_path=gcs_logs_path)


    def notify_slack_alert(self, spider):
        from google.cloud import pubsub_v1
        publisher = pubsub_v1.PublisherClient()
        topic_path = 'projects/mercari-us-de/topics/mario-api'
        message = {
            'details': {
                'action': 'slack_alert',
                'channel': '#de-content-ops',
                'crawl_id': self.crawl_id,
                'env': os.getenv('env', ''),
                'description': f"Crawl Job completed Successfully. Output Written to path {self.report.get('footer', {}).get('gcs_upload_path')}"
            }
        }
        data = json.dumps(message).encode('utf-8')
        future = publisher.publish(topic_path, data=data)
        print('Published {} of message ID {}.'.format(data, future.result()))


    def get_destination_blob_name(self, category: str, brand: str, source: str,
            timestamp: str, format: str, filename: str) -> str:
        destination_blob_name = self.gcs_blob_prefix.format(category=category,
            brand=brand, source=source, timestamp=timestamp, format=format, filename=filename)
        destination_blob_name = re.sub(re.compile(r"['&,]"), '', destination_blob_name)
        destination_blob_name = re.sub(r'\s+', '_', destination_blob_name).strip().lower()
        return destination_blob_name


    def upload_cached_response(self, spider: str, category: str, brand: str, source: str, timestamp: str) -> None:
        spider_project_name = spider.name # self.spider_project_name

        filename = spider_project_name + ".tar.gz"
        destination_blob_name = self.get_destination_blob_name(category=category, brand=brand,
            source=source, timestamp=timestamp, format='html', filename=filename)
        blob = self.bucket.blob(destination_blob_name)

        tar = tarfile.open(filename, "w:gz")
        cache_file_path = os.path.join(os.getcwd(), self.http_cache_directory)
        try:
            tar.add(cache_file_path, arcname=filename)
            tar.close()
            file_path= os.path.join(os.getcwd(), filename)
            blob.upload_from_filename(file_path)
            os.remove(file_path)

            spider.logger.info('Html cache uploaded to gs://{bucket}/{blob}'.format(
                bucket=self.bucket_name, blob=destination_blob_name))
        except FileNotFoundError:
            spider.logger.info(f'Html Cache directory is not found in the {cache_file_path}')
            

    def upload(self, spider=None, upload_path=None, upload_format=None) -> None:
        """
        Upload the file object to given gcs path
        @param upload_path: gcs path for file upload
        @param spider: spider object of the spider which was closed
        @param upload_format: differentiate crawl items json or
                              report json uploading
        """
        destination_blob_name = upload_path
        if upload_format == "report":
            json_data = json.dumps(self.report)
            binary_data = json_data.encode()
            f = BytesIO(binary_data)
            f.seek(0)
        elif upload_format == "log":
            blob = self.bucket.blob(destination_blob_name)
            log_local_filepath = f"{os.getenv('project_dir')}/debug.log"
            from os import path
            if (path.exists(log_local_filepath)):
                blob.upload_from_filename(log_local_filepath)
                spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
                    bucket=self.bucket_name, blob=destination_blob_name))
            else:
                spider.logger.error(f'Log file not found, {log_local_filepath}')
            return
        else:
            # def drop_duplicates(items):
            #     df = pd.DataFrame(items)
            #     df.drop_duplicates(subset=['id'], inplace=True)
            #     return list(df.T.to_dict().values())
            # self.items = drop_duplicates(self.items)

            # check if the merge key is given call the merge function and
            # pass the appropriate items list for file object making
            if self.merge_key:
                items_merged = self.merge_items(spider=spider, items=self.items, merge_key=self.merge_key)
                f = self._make_fileobj(items_merged)
            else:
                f = self._make_fileobj(self.items)

        blob = self.bucket.blob(destination_blob_name)
        blob.upload_from_file(f)
        spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
                bucket=self.bucket_name, blob=destination_blob_name))


    def process_item(self, item: Dict, spider: Spider) -> Dict:
        """
        Returns item with schema validated results.
        @param item: individual crawl item
        @param spider: spider object of the spider which was closed
        @return: item updated with validated result
        """
        if not self.schema:
            self.generate_schema(spider)

        if self.schema:
            schema_validator = SchemaValidator(item)
            valid = schema_validator.process(self.schema)
            item.update({"valid": valid})
            self.items.append(item)
        else:
            self.items.append(item)

        return item


    def _make_fileobj(self, items: List) -> BytesIO:
        """
        Build file object from crawled items.
        @param items: list of crawled items
        @return: byte file object
        """
        bio = BytesIO()
        f = bio

        # Build file object using ItemExporter
        exporter = JsonLinesItemExporter(f)
        exporter.start_exporting()
        for item in items:
            exporter.export_item(item)
        exporter.finish_exporting()

        # Seek to the top of file to be read later
        bio.seek(0)
        return bio
