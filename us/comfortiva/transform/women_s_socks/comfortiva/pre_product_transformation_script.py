import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ','.join(record.get("description",""))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '').title().strip()

            pattern = record.get('color', '').strip().title()

            image = record.get('image', '')
            if not image.startswith('https:'):
                image = 'https:' + image


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": image,

                "model": model,
                "pattern": pattern,
                "description": description,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
