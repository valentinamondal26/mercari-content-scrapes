import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            description = ','.join(record.get("description",""))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '').title().strip()

            heel = ''
            match = re.findall(r"Heel h*H*eight:\s*(\d+\s*\d*\/*\d*)\s*",record.get("description","")[-1]) \
                or re.findall(r"Heel h*H*eight:\s*(\d+\s*\d*\/*\d*)\s*",record.get("description","")[-2])
            if match:
                heel = match[0]
                heel = heel.replace(" 1/2",".5").replace(" 1/4",".25")
                heel += " in."

            color = record.get('color', '').strip()

            image = record.get('image', '')
            if not image.startswith('https:'):
                image = 'https:' + image

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": image,

                "model": model,
                "color": color,
                "description": description,
                "heel_height": heel,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
