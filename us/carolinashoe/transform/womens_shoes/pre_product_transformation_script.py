import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            descriptions = record.get('description', '')
            description = ', '.join(descriptions)
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('detail_title', '')
            bread_crumbs = record.get('bread_crumbs','')
            product_type = ''
            for category in bread_crumbs:
                if category.strip().title()=='Shoes':
                    product_type = 'Shoes'
            if product_type == '':
                product_type = 'Boots'

            toe_type = ''
            if 'toe' in descriptions[1].lower():
                toe_type = descriptions[1]
            else:
                for desc_info in descriptions:
                    if 'toe' in desc_info.lower() and ':' not in desc_info.lower():
                        toe_type = desc_info


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('main_image', ''),

                "model": model,
                "color": record.get('color', '').title(),
                'description': description,
                'model_sku': record.get('model_sku', '').replace("Style #: ",""),
                'product_type': product_type,
                'toe_type': toe_type,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
