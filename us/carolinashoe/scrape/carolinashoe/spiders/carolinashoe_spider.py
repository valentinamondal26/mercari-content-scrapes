'''
https://mercari.atlassian.net/browse/USCC-695 - Carolina Women's Shoes
https://mercari.atlassian.net/browse/USCC-696 - Carolina Men's Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

class CarolinashoeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from carolinashoe.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Shoes
        2) Men's Shoes

    brand : str
        brand to be crawled, Supports
        1) Carolina

    Command e.g:
    scrapy crawl carolinashoe -a category="Women's Shoes" -a brand='Carolina'
    scrapy crawl carolinashoe -a category="Men's Shoes" -a brand='Carolina'
    """

    name = 'carolinashoe'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'carolinashoe.com'
    blob_name = 'carolinashoe.txt'

    url_dict = {
        "Women's Shoes": {
            'Carolina': {
                'url': 'https://www.carolinashoe.com/Products?Sorting=Popularity&brand=Carolina&gender=Womens&page=1'
            }
        },
        "Men's Shoes": {
            'Carolina': {
                'url': 'https://www.carolinashoe.com/Products?Sorting=Popularity&brand=Carolina&gender=Mens&page=100',
            }
        },
    }
    base_url = "https://www.carolinashoe.com"

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(CarolinashoeSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.carolinashoe.com/CA8528
        @meta {"use_proxy":"True"}
        @scrape_values id title images color price model_sku description detail_title bread_crumbs main_image
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath("//div[@id='style-details']//span[@class='stylenum']/text()").get(),
            'images': response.xpath("//div[@id='alt-views']/ul/li/input/@src").getall(),
            'color': response.meta.get("color",""),
            'price': response.xpath("//div[@id='style-details']//span[@class='regprice']/text()").get() or response.xpath("//div[@id='style-details']//s[@class='regprice']/text()").get(),
            'model_sku': response.xpath("//div[@id='style-details']//span[@class='style-name']/text()").get(),
            'description': response.xpath("//div[@id='details']//li/text()").getall(),
            'detail_title': response.xpath("//div[@id='details']/p[@class='detail-title']/strong/text()").get(),
            'bread_crumbs': response.xpath("//ol[@class='bread']//a/text()").getall(),
            'main_image': response.xpath("//img[@id='mainShoe']/@src").get()
        }


    def parse(self, response):
        """
        @url https://www.carolinashoe.com/Products?Sorting=Popularity&brand=Carolina&gender=Mens&page=100
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath("//a[@class='productCard']"):
            product_url = product.xpath('./@href').extract_first()
            color = product.xpath(".//div[@class='more-details']/p/strong/text()").get()
            if product_url:
                yield self.create_request(
                    url=self.base_url+product_url,
                    callback=self.crawlDetail,
                    meta={"color":color}
                )


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
