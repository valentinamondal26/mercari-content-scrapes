import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            spf=''

            colors=record.get('color','')
            colors=colors.split(", ")
            color=[]
            for cl in colors:
                color.append(' '.join(cl.split()))
            color=", ".join(color)
            if re.findall(r"^\d+\.*\d*$|No Color", color):
                color =''


            pattern=re.compile(r"SPF\s*\d+",re.IGNORECASE)
            if re.findall(pattern,model)!=[]:
                spf=re.findall(pattern,model)[0]
            for word in spf.split():
                pattern=re.compile("{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)

            description=description.replace("MAC is working toward a cruelty-free world. MAC does not test on animals and never asks others to test on the brand's behalf. MAC advocates for ending animal testing globally and is taking steps toward that goal every day. , Because MAC shares your commitment to the environment, they have the Back-to-MAC Program. By returning six [6] MAC primary packaging containers to a MAC counter, you'll receive a free MAC lipstick of your choice as a thanks to you. Note, Lipsticks provided at no charge cannot be returned or exchanged. Back-to-M·A·C is only available in stores.",'')
            description=description.replace("Visit the MAC counter at your local Belk store for a free 10-day supply of foundation personalized to your individual formula and shade.,",'')
            description=description.replace("Â¿",'')

            product_type=record.get("Face Makeup",'')
            product_type=re.sub(re.compile(r"highlighters",re.IGNORECASE),'Highlighter',product_type)
            product_type=re.sub(re.compile(r"sprays",re.IGNORECASE),'Spray',product_type)
            product_type=re.sub(re.compile(r"concealers",re.IGNORECASE),'Concealer',product_type)  
            product_type=re.sub(re.compile(r"primers",re.IGNORECASE),'primer',product_type)
            model = re.sub(re.compile(r"\-\s*\$\s*\d+\s*value\!|\bMAC\b", re.IGNORECASE), "", model)
            model = re.sub(r"(\w+)\s*\/\s*(\w+)",r"\1/\2",model)
            model = re.sub(r"(\'\w)", lambda match:r"{}".format(match.group(1).lower()),model)           
            model=' '.join(model.split())
            color = re.sub(re.compile(r"(\w+)\s*(\d+)", re.IGNORECASE), r"\1\2", color)
            color = color.title()
            color = re.sub(r"(\'\w)", lambda match:r"{}".format(match.group(1).lower()),color)
        

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date',''),
                "status_code": record.get('status_code',''),
                "category": "Face Makeup Products",
                "breadcrumb":record.get('breadcrumb',''),
                "description": description.replace('\n',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": description.replace('\n',''),
                "price":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "size" : record.get('size',''),
                "color":color,
                "gender":record.get('gender',''),
                "product_type":product_type,
                "upc":record.get('upc',''),
                "spf":spf.strip('SPF ')

                }

            return transformed_record
        else:
            return None
