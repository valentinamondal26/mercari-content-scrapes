'''
https://mercari.atlassian.net/browse/USCC-376 - MAC Cosmetics Face Makeup Products
'''

import scrapy
import datetime
import json
import random
import os
from scrapy.exceptions import CloseSpider

class belkSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from belk.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Face Makeup Products

    brand : str
        category to be crawled, Supports
        1) MAC Cosmetics

    Command e.g:
    scrapy crawl belk -a category='Face Makeup Products' -a brand='MAC Cosmetics'

    """

    name = 'belk'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    filters = None
    launch_url = None
    source = 'belk.com'
    blob_name = 'belk.txt'
    base_url = 'https://belk.com'

    url_dict = {
        "Face Makeup Products": {
            'MAC Cosmetics': {
                'launch_url': 'https://www.belk.com/beauty/makeup/face-makeup/?prefn1=brand&prefv1=MAC',
                'filters': ['Face Makeup'],
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(belkSpider, self).__init__(*args, **kwargs)
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand,{}).get('launch_url','')
            self.filters=self.url_dict.get(category,{}).get(brand,{}).get('filters','')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        self.filters = ['Face Makeup']


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self,response):
        """
        @url https://www.belk.com/beauty/makeup/face-makeup/?prefn1=brand&prefv1=MAC
        @returns requests 1
        """

        if self.filters!=[]:
            for filter in self.filters:
                filter_urls=response.xpath('//div[@class="refinements"]//h2[contains(text(),"{filter_name}")]/following-sibling::div/ul/li//a[@data-layout="categoryproducthits"]/@href'.format(filter_name=filter)).getall()
                filter_urls=[self.base_url+link if "https" not in link else link for link in filter_urls ]
                filter_data=response.xpath('//div[@class="refinements"]//h2[contains(text(),"{filter_name}")]/following-sibling::div/ul/li//a[@data-layout="categoryproducthits"]/h3/text()'.format(filter_name=filter)).getall()
                for url in filter_urls:
                    meta={}
                    index=filter_urls.index(url)
                    meta=meta.fromkeys([filter],filter_data[index])
                    meta.update({'filter_name':filter})
                    yield self.createRequest(url=url,callback=self.parse,meta=meta)

        yield self.createRequest(url=response.url,callback=self.parse)


    def parse(self, response):
        """
        @url https://www.belk.com/beauty/makeup/face-makeup/foundation/?prefn1=brand&prefv1=MAC
        @returns requests 1
        """

        script_data=response.xpath('//script[contains(text(),"window.pageData=")]/text()').get()
        js=json.loads(script_data.split('window.pageData=')[1][:-1])
        all_links=[]
        for js_pr in js.get('products',[]):
            all_links.append(js_pr.get('url'))
        all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
        for link in all_links:
            yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)


    def parse_product(self,response):
        """
        @url https://www.belk.com/p/mac-studio-fix-powder-plus-foundation/5900090M510.html?dwvar_5900090M510_color=264100054745#prefn1=brand&prefv1=MAC&start=1
        @scrape_values image breadcrumb title price color size upc gender
        """

        item={}
        item["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"]=str(response.status)
        item['id']=response.url
        item['image']=response.xpath('//div[@class="item active"]/img/@src').get(default='')
        item['brand']=self.brand
        item['category']=self.category
        breadcrumb=response.xpath('//div[@class="breadcrumb"]//a//text()').getall()
        breadcrumb=[br.strip() for br in breadcrumb]
        item['breadcrumb']='|'.join(breadcrumb)
        item['image']=response.xpath('//div[@class="product-primary-image"]//img/@src').get(default='')
        item['title']=response.xpath('//div[@class="brand-name"]/text()').get(default='').strip()
        item['price']=response.xpath('//div[@class="standardprice"]/span/text()').get(default='')
        description=response.xpath('//div[@class="copyline-description"]/text()').getall()
        for des in description:
            if des=='\n':
                del description[description.index(des)]
        item['description']=', '.join(description)

        item['color']="| ".join(response.xpath('//ul[@class="swatches color"]//div[@class="swatchanchor"]/img/@alt').getall())
        item['size']=response.xpath('//select[@id="va-size"]/option[@selected="selected"]/text()').get(default='').strip()
        item['upc']=response.xpath('//span[@class="product-UPC"]/text()').get(default='').strip('UPC: ')
        sc=response.xpath('//script[contains(text(),"utag_data")]/text()').get()
        js=json.loads(sc.split('utag_data = ')[1][:-2])
        item['gender']=js.get('gender',[''])[0]

        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)

        yield item


    def createRequest(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
