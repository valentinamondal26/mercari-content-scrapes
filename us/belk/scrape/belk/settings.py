# -*- coding: utf-8 -*-

# Scrapy settings for belk project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'belk'

SPIDER_MODULES = ['belk.spiders']
NEWSPIDER_MODULE = 'belk.spiders'

USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

ROBOTSTXT_OBEY = False

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

# GCS settings - actual value be 'raw/belk.com/shoulder_bags/coach/2019-02-06_00_00_00/json/tradesy.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'


DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    # 'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}
