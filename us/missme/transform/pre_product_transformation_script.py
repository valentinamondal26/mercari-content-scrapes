import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '').title()
            model = re.sub(re.compile(r'\bboot\s*cut\b|\bjeans*\b|\bboot cut jeans*\b|\bboot\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            def lower_case(match):
                return "'"+match.groups()[0].lower()
            model = re.sub(r"\'(\w+\s)", lower_case, model)
            model = re.sub(r"\'(\w+$)", lower_case, model)
            model = re.sub(r'\s+', ' ', model).strip()




            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            meta_wash = ['LIGHT BLUE', 'MEDIUM BLUE', 'DARK BLUE', 'BLACK', 'WHITE', 'COLORED']
            meta_look = ['ABSTRACT', 'BIG STITCH', 'BORDER', 'CROSS', 'DISTRESSED', 'FLAP POCKET',
                'FLEUR DE LIS', 'FLORAL', 'INSERT', 'NOVELTY', 'SIMPLE', 'STUDDED', 'WINGS']
            collections = ','.join(record.get('collections', []))
            product_details = '\n'.join(record.get('product_details', []))

            wash = ''
            look = ''

            match = re.findall(re.compile(pattern_words(meta_wash), re.IGNORECASE), collections)
            if match:
                wash = match[0]
                wash = re.sub(r'\s+', ' ', wash).strip()

            match = re.findall(re.compile(pattern_words(meta_look), re.IGNORECASE), collections)
            if match:
                look = match[0]
                look = re.sub(r'\s+', ' ', look).strip()


            material = ''
            front_rise = ''
            back_rise = ''
            inseam = ''

            match = re.findall(re.compile(r'Front Rise:\s*(.*?)[,;\n]', re.IGNORECASE), product_details)
            if match:
                front_rise = match[0]
                front_rise = re.sub(r'\s+', ' ', front_rise).strip()

            match = re.findall(re.compile(r'Back Rise:\s*(.*?)[;\n]', re.IGNORECASE), product_details)
            if match:
                back_rise = match[0]
                back_rise = re.sub(r'\s+', ' ', back_rise).strip()

            match = re.findall(re.compile(r'Inseam:\s*(.*)', re.IGNORECASE), product_details)
            if match:
                inseam = match[0]
                inseam = re.sub(r'\s+', ' ', inseam).strip()

            match = re.findall(re.compile(r'\d+%\s*\w+', re.IGNORECASE), product_details)
            if match:
                material = ', '.join(match)
                material = re.sub(r'\s+', ' ', material).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "description": record.get('description', ''),
                "color": record.get('filter', {}).get('color', ''),
                'size': record.get('filter', {}).get('size', ''),
                'model_sku': record.get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                'wash': wash,
                'style': look,
                "material": material,
                'front_rise': front_rise,
                'back_rise': back_rise,
                'inseam': inseam,
            }

            return transformed_record
        else:
            return None
