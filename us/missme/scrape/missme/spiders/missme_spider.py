'''
https://mercari.atlassian.net/browse/USCC-325 - Miss Me Bootcut Jeans for Women
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json
import re


class MissmeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from missme.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Bootcut Jeans for Women
    
    brand : str
        brand to be crawled, Supports
        1) Miss Me

    Command e.g:
    scrapy crawl missme -a category='Bootcut Jeans for Women' -a brand='Miss Me'
    """

    name = 'missme'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'missme.com'
    blob_name = 'missme.txt'

    url_dict = {
        'Bootcut Jeans for Women': {
            'Miss Me': {
                'url': 'https://www.missme.com/collections/bootcut',
            }
        }
    }

    listing_page_url = 'https://services.mybcapps.com/bc-sf-filter/filter?page={page}&shop=ctmissme.myshopify.com&limit={size}&collection_scope={collection_id}'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(MissmeSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.missme.com/products/bright-spirits-bootcut-jeans?variant=31270994149434
        @scrape_values description product_details
        @meta {"use_proxy":"True"}
        """
        
        item = response.meta.get('item_info', {})
        item['description'] = response.xpath('//ul[@class="tabscontent"]/li[@id="tab1"]/p//text()').extract_first()
        item['product_details'] = response.xpath('//ul[@class="tabscontent"]/li[@id="tab1"]/ul[@class="list"]/li//text()').extract()

        yield item


    def parse(self, response):
        """
        @url https://www.missme.com/collections/bootcut
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        script = response.xpath('//script[contains(text(), "collection_id")]/text()').extract_first()
        total_count = ''
        match = re.findall(re.compile(r'collection_count:\s*(.*?),'), script.replace('\n', ''))
        if match:
            total_count = match[0]
        collection_id = ''
        match = re.findall(re.compile(r'collection_id:\s*(.*?),'), script.replace('\n', ''))
        if match:
            collection_id = match[0]
        print(f'total_count:{total_count}, collection_id:{collection_id}')

        if total_count and collection_id:
            total_count = int(total_count)
            size = 50
            page = 0
            count = 0
            while count < total_count:
                page = page + 1
                count = count + size
                yield self.create_request(url=self.listing_page_url.format(page=page, size=size, collection_id=collection_id), callback=self.parse_listing_page)


    def parse_listing_page(self, response):
        """
        @url https://services.mybcapps.com/bc-sf-filter/filter?page=2&shop=ctmissme.myshopify.com&limit=50&collection_scope=78852259898
        @returns requests 1
        @meta {"use_proxy":"True"}
        @scrape_values meta.item_info.price meta.item_info.title meta.item_info.image meta.item_info.sku meta.item_info.filter \
            meta.item_info.collections meta.item_info.vendor meta.item_info.product_type meta.item_info.id
        """

        res = json.loads(response.text)
        for product in res.get('products', []):
            for variant in product.get('variants', []):
                if product.get('handle', '') and variant.get('id', ''):
                    id = 'https://www.missme.com/products/{}?variant={}'.format(product['handle'], variant['id'])
                    request = self.create_request(url=id, callback=self.crawlDetail)
                    request.meta['item_info'] = {
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': str(response.status),
                        'category': self.category,
                        'brand': self.brand,
                        'id': id,

                        'price': variant.get('price', ''),
                        'sku': variant.get('sku', ''),
                        'image': variant.get('image', ''),
                        'filter': {a.split(':')[0]:a.split(':')[-1] for a in variant.get('merged_options', [])},

                        #'price_min': product['price_min'],
                        #'price_max': product['price_max'],
                        'collections': [a.get('title', '') for a in product.get('collections', [])],
                        'vendor': product.get('vendor', ''),
                        'product_type': product.get('product_type', ''),
                        'title': product.get('title', ''),
                        #'image': product['images'][0],
                        #'skus': product['skus'],
                        #'handle': product['handle'],
                    }
                    yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request