import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            for key, value in record.get('specs', {}).items():
                description += f" {key}: {', '.join(value)}."

            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            # price and msrp extraction
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            try:
                msrp = list(dict.fromkeys(record.get("msrp", [])))[0].replace(
                        "$", "").strip()
            except Exception:
                msrp = ''

            # extract weight
            specs_info = ', '.join(record.get('specs', {}).get('Specs', []))
            weight = ''
            regex_weight = r'Weight:\s*(.*?)[,]'
            match = re.findall(regex_weight, specs_info)
            if match and not re.findall('TBD', ', '.join(match)):
                weight = ', '.join(match)

            # extract model sku
            model_sku = ''
            match = re.findall(r'Style #:\s*(.*?)[,]', specs_info,
                               flags=re.IGNORECASE)
            if match:
                model_sku = match[0]
            if not model_sku:
                match = re.findall(r'Style #:\s*(.*?$)', specs_info,
                                   flags=re.IGNORECASE)
                if match:
                    model_sku = match[0]

            # model transformations
            model = record.get('title', '')
            model = re.sub(re.compile(r'®|™|Men’s|Men\'s|\.', re.IGNORECASE), '',
                           model)
            model = re.sub(r'\b(UA Armour|UA|Armour)\b', 'Under Armour', model,
                           re.IGNORECASE)
            if not re.findall(r'^\s*Under Armour', model, flags=re.IGNORECASE):
                model = 'Under Armour ' + ''.join(
                        [x.replace('Under Armour', '') for x in
                         model.split('Under Armour', 1)])
            model = re.sub(re.compile(r"\bUnder Armour\b", re.IGNORECASE),
                           "", model)
            model = re.sub(r'\s+', ' ', model).strip()

            # color transformations
            color_filter = ', '.join(record.get('Color', []))
            color = record.get('selected_color', '').split('-')[0]
            color = '/'.join(list(dict.fromkeys(
                    list(map(lambda x: x.strip(), color.split('/'))))))
            color = re.sub(r'Select Color', '', color, flags=re.IGNORECASE)
            color = re.sub(re.compile(r"\bX\b", re.IGNORECASE), "X-Ray", color)
            color = color.title()
            if len(color.split("/")) > 2:
                color = color.split("/")[0]+"/Multicolor"

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawlDate": record.get('crawlDate', ''),
                    "statusCode": record.get('statusCode', ''),

                    "category": record.get('category'),
                    "brand": record.get('brand', ''),

                    "ner_query": ner_query,
                    "title": record.get('title', ''),
                    "description": description,
                    'image': record.get('image', ''),

                    "model": model,
                    "price": {
                            "currencyCode": "USD",
                            "amount": price,
                    },
                    'msrp': {
                            'currencyCode': "USD",
                            "amount": msrp,
                    },

                    "model_sku": model_sku,
                    "color": color_filter or color,
                    "weight": weight,
                    "activity": record.get('breadcrumb', [])[-1],
            }

            return transformed_record
        else:
            return None
