class Mapper:
    def map(self, record):
        key_field = ""

        age = ''
        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "age" == entity['name']:
                if entity["attribute"]:
                    age = entity["attribute"][0]["id"]
            elif "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = age + model + color
        return key_field
