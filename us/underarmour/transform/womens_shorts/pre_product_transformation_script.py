import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            for key, value in record.get('specs',{}).items():
                description += f" {key}: {', '.join(value)}."

            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            msrp = list(dict.fromkeys(record.get("msrp", [])))[0].replace("$", "").strip()

            specs_info =  ', '.join(record.get('specs',{}).get('Specs', []))
            material = ''
            regex_material = r'\d+%\s*(.*?)[\/,\.]'
            match = re.findall(regex_material, specs_info)
            if not match:
                match = re.findall(regex_material, description)
            if match:
                material = ', '.join(list(filter(lambda x: len(x) < 15, sorted(dict.fromkeys(match)))))

            model_sku = ''
            match = re.findall(r'Style #:\s*(.*?)[,]', specs_info, flags=re.IGNORECASE)
            if match:
                model_sku = match[0]
            if not model_sku:
                match = re.findall(r'Style #:\s*(.*?$)', specs_info, flags=re.IGNORECASE)
                if match:
                    model_sku = match[0]

            inseam = ''
            regex_inseam = r'Inseam\s*(\((outer\s*)*shorts*\))*:\s*(.*?")'
            match = re.findall(regex_inseam, specs_info, flags=re.IGNORECASE)
            if not match:
                match = re.findall(regex_inseam, description, flags=re.IGNORECASE)
            if match:
                inseam = match[0][-1]


            model = record.get('title', '')
            model = re.sub(re.compile(r'®|™|Women’s|Women\'s', re.IGNORECASE), '', model)
            model = re.sub(r'\b(UA Armour|UA|Armour)\b', 'Under Armour', model, re.IGNORECASE)
            if not re.findall(r'^\s*Under Armour', model, flags=re.IGNORECASE):
                model = 'Under Armour ' + ''.join([x.replace('Under Armour', '') for x in model.split('Under Armour', 1)])
            model = re.sub(r'\s+', ' ', model).strip()

            color_filter = ', '.join(record.get('Color', []))
            color = record.get('selected_color', '').split('-')[0]
            color = re.sub(r'\bSelect Color\b', '', color, flags=re.IGNORECASE)
            color = '/'.join(list(dict.fromkeys(list(map(lambda x: x.strip(), color.split('/'))))))
            color = color.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price,
                },
                'msrp':{
                    'currencyCode':"USD",
                    "amount" : msrp,
                },

                "model_sku": model_sku,
                "color": color_filter or color,
                "material": material,
                "fit": ', '.join(record.get('Fit', [])),
                "activity": ', '.join(record.get('Sports', [])),
                'inseam': inseam,
            }

            return transformed_record
        else:
            return None
