import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            msrp = record.get("msrp", "").replace("$", "").strip()

            descriptions = record.get('description', [])

            materials =  record.get('fabricleagals',"")
            try:
                material = [material for material in materials if "%" in material][0]
            except IndexError:
                material = ''

            if not material:
                try:
                    materials.remove("Imported")
                    material = materials[0]
                except ValueError:
                    material = ''
            if not material:
                try:
                    descriptions.remove("Imported")
                    material = descriptions[-1]
                except ValueError:
                    material = ''
            material = '/'.join(sorted([ re.sub(r'\d+%\s*|\d+\.*\d*\s*oz\.*\s*', '', m, re.IGNORECASE).strip() for m in material.split('/') ]))
            material = re.sub(r'Body\s*:\s*|Shell\s*:\s*', '', material, re.IGNORECASE)
            material = re.sub(r'\s+', ' ', material).strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'®|™|Pre-school|—|¼|½|Tri-Color|Tri-Blend', re.IGNORECASE), '', model)
            model = re.sub(r'\b(UA Armour|UA|Armour)\b', 'Under Armour', model, re.IGNORECASE)
            model = 'Under Armour'.join([x.replace('Under Armour', '') for x in model.split('Under Armour', 1)])
            model = re.sub(r"Boys['’]", 'Boys', model, re.IGNORECASE)
            model = re.sub(r"Color Blocked", 'Colorblock', model, re.IGNORECASE)
            if material:
                model = re.sub('|'.join(list(map(lambda x: r'\b'+x+r'\b', material.split('/')))), '', model, re.IGNORECASE)
            model = re.sub(r'-\s*$|–\s*$', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            color = record.get('primaryColor', '')
            secondary_color = record.get('primaryColor', '')
            if secondary_color:
                color = f'{color}/{secondary_color}'
            color = '/'.join(list(dict.fromkeys(list(map(lambda x: x.strip(), color.split('/'))))))
            color = color.title()

            inseam = ''
            description = record.get("description","")
            try:
                inseam = [desc for desc in description if "Inseam" in desc][0]
                inseam = inseam.replace('Inseam:',"").strip()
            except IndexError:
                inseam = ''

            product_type = record.get('Category', '')

            if re.findall(re.compile(r'\bSet\b', re.IGNORECASE), model) or \
                re.findall(re.compile(r'\bHoodies\b', re.IGNORECASE), product_type):
                return None


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                'msrp':{
                    'currencyCode':"USD",
                    "amount" : msrp
                },

                "age" : record.get("Age Group",""),
                "model_sku": record.get('model_sku', ''),
                "color": color,
                "size": ','.join(record.get('sizes', [])),
                "material": material,
                "description": '. '.join(descriptions),
                "fit": record.get('Fit', ''),
                "product_type": product_type,

                "activity": record.get('Sport', ''),
                "technology": record.get('Technology', ''),
                "inseam": inseam,

            }

            return transformed_record
        else:
            return None
