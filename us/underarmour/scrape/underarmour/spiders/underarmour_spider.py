'''
https://mercari.atlassian.net/browse/USDE-759 - Under Armour Boys' Tops & T-Shirts (Sizes 4 & Up)
https://mercari.atlassian.net/browse/USDE-1690 - Under Armour Women's Activewear Tops
https://mercari.atlassian.net/browse/USDE-1691 - Under Armour Pants, tights, leggings
https://mercari.atlassian.net/browse/USDE-1692 - Under Armour Women's Shorts
https://mercari.atlassian.net/browse/USDE-1693 - Under Armour Women's Sports Bras
https://mercari.atlassian.net/browse/USDE-1694 - Under Armour Men's Athletic Shorts
https://mercari.atlassian.net/browse/USDE-1695 - Under Armour Men's Activewear Tops
https://mercari.atlassian.net/browse/USDE-1696 - Under Armour Men's Athletic Shoes

https://mercari.atlassian.net/browse/USDE-1732 - Under Armour Women's Hoodies & Sweatshirts
https://mercari.atlassian.net/browse/USDE-1734 - Under Armour Men's Hoodies & Sweatshirts
https://mercari.atlassian.net/browse/USDE-1735 - Under Armour Women's Athletic Shoes
https://mercari.atlassian.net/browse/USDE-1736 - Under Armour Girls' Tops & T-Shirts (Sizes 4 & Up)
https://mercari.atlassian.net/browse/USDE-1737 - Under Armour Boys' Pants (Sizes 4 & Up)
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
from urllib.parse import urlparse, parse_qs, urlencode

class UnderarmourSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from underarmour.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Boys' Tops & T-Shirts (Sizes 4 & Up)
        2) Women's Activewear Tops
        3) Pants, tights, leggings
        4) Women's Shorts
        5) Women's Sports Bras
        6) Men's Athletic Shorts
        7) Men's Activewear Tops
        8) Men's Athletic Shoes
        9) Women's Hoodies & Sweatshirts
        10) Men's Hoodies & Sweatshirts
        11) Women's Athletic Shoes
        12) Girls' Tops & T-Shirts (Sizes 4 & Up)
        13) Boys' Pants (Sizes 4 & Up)

    brand : str
        brand to be crawled, Supports
        1) Under Armour

    Command e.g:
    scrapy crawl underarmour -a category="Boys' Tops & T-Shirts (Sizes 4 & Up)" -a brand='Under Armour'

    scrapy crawl underarmour -a category="Women's Activewear Tops" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Pants, tights, leggings" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Women's Shorts" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Women's Sports Bras" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Men's Athletic Shorts" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Men's Activewear Tops" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Men's Athletic Shoes" -a brand='Under Armour'

    scrapy crawl underarmour -a category="Women's Hoodies & Sweatshirts" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Men's Hoodies & Sweatshirts" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Women's Athletic Shoes" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Girls' Tops & T-Shirts (Sizes 4 & Up)" -a brand='Under Armour'
    scrapy crawl underarmour -a category="Boys' Pants (Sizes 4 & Up)" -a brand='Under Armour'

    NOTE: This site has throttling issue, when continuously crawling redirecting to a page with text '404 Error, Hit a wall?'.
    Upon dynamic crawling the url again we end up in re-captcha page.
    Even introducing a delapy of 1 min and concurrent request set to 1, we end up in the same situation as above.
    So, Currently i'm locally solving the re-captcha images manually with selenium headless mode set to False.

    """

    name = 'underarmour'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'underarmour.com'
    blob_name = 'underarmour.txt'

    url_dict = {
        "Boys' Tops & T-Shirts (Sizes 4 & Up)": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/boys/tops/little-kids-4-7/g/3fljj?lsg=030805ab-5e9a-4227-abc3-9292594f3d26',
                    'https://www.underarmour.com/en-us/boys/tops/kids-8-20/g/3fljm?lsg=d52d357c-d239-469d-a88f-7479fc5e7302',
                ],
                'filters': ['Category', 'Age Group', 'Color', 'Fit', 'Sport', 'Technology'],
            }
        },
        "Women's Activewear Tops": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/womens/clothing/tops/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Pants, tights, leggings": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/womens/clothing/pants/',
                    'https://www.underarmour.com/en-us/c/womens/clothing/leggings/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Women's Shorts": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/womens/clothing/shorts/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Women's Sports Bras": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/womens/clothing/sports-bras/',
                ],
                'filters': ['Sports', 'Color', 'Fit', 'Support Level'],
            }
        },
        "Men's Athletic Shorts": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/mens/clothing/shorts/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Men's Activewear Tops": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/mens/clothing/tops/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Men's Athletic Shoes": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/mens/shoes/cleats+sneakers/?prefn1=experienceType&prefv1=premium',
                ],
                'filters': ['Sports', 'Color'],
            }
        },

        "Women's Hoodies & Sweatshirts": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/womens/clothing/hoodies-sweatshirts/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Men's Hoodies & Sweatshirts": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/mens/clothing/hoodies-sweatshirts/',
                ],
                'filters': ['Sports', 'Color', 'Fit'],
            }
        },
        "Women's Athletic Shoes": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/womens/shoes/',
                ],
                'filters': ['Color'],
            }
        },
        "Girls' Tops & T-Shirts (Sizes 4 & Up)": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/girls/clothing/tops/',
                ],
                'filters': ['Sports', 'Color', 'Fit', 'Age Group'],
            }
        },
        "Boys' Pants (Sizes 4 & Up)": {
            'Under Armour': {
                'url': [
                    'https://www.underarmour.com/en-us/c/boys/clothing/bottoms/',
                ],
                'filters': ['Sports', 'Color', 'Fit', 'Age Group'],
            }
        },
    }

    limit = 60

    listing_page_url = "https://www.underarmour.com/on/demandware.store/Sites-US-Site/en_US/Search-UpdateGrid?start={offset}&sz={limit}"

    base_url = 'https://www.underarmour.com'

    filter_list = []

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(UnderarmourSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', [])
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + str(self.launch_url))


    def contracts_mock_function(self):
        self.filter_list = ['Sports', 'Color', 'Fit']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, list):
                urls = self.launch_url
            elif isinstance(self.launch_url, str):
                urls = [self.launch_url]


            for url in urls:
                yield self.create_request(url=url, callback=self.parse, dont_filter=True)


    def crawlDetail(self, response):
        #NOTE: when end up with re-captcha page du to throttling issue,
        # this code helps to send the request to selenium and helps us to solve the 
        # re-captcha manually.
        if response.meta.get("is_selenium_request", False):
            print(f'<-----xxxxx----->{response.status}: {response.request.url}')
        selected_color = response.xpath(u'normalize-space(//span[@class="b-product_attribute-title js-selected-color m-color"]/text())').extract_first()
        if response.status == 302 or not selected_color or selected_color == "Select Color":
            print(f'selected_color: {selected_color}')
            print(f"==> failed: {response.meta.get('proxy', '')}, url: {response.request.url}")
            print(f'==> using SeleniumRequest for {response.request.url}')
            from common.selenium_middleware import SeleniumRequest
            request = SeleniumRequest(url=response.request.url, callback=self.parse, dont_filter=True)
            # request.meta.update({"use_cache":True})
            request.meta.update({"is_selenium_request":True})
            request.meta.update({'dont_redirect': True,'handle_httpstatus_list': [302]})
            request.meta['proxy'] =  response.meta.get('proxy', '')
            yield request
            return

        specs = {}
        for tabs in response.xpath('//div[@class="b-product_description-items g-accordion--mobile g-tabs-content"]/div'):
            key = tabs.xpath(u'normalize-space(.//div[contains(@class, "b-product_description-acc_heading g-accordion-header")]/text())').extract_first()
            value = tabs.xpath('.//ul[@class="t-tabs_data"]/li/text()').extract()
            specs[key] = value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.request.url,

            'breadcrumb': response.xpath('//ol[@class="b-breadcrumbs-list"]/li/a').xpath('normalize-space(./text())').extract(),
            'title': response.xpath('//div[@class="b-product_name"]/h1/text()').extract_first(),
            'price': response.xpath('//div[@class="b-price"]/span/@content').extract_first(),
            'msrp': response.xpath('//div[@class="b-price"]/span/@content').extract(),
            'selected_color': response.xpath(u'normalize-space(//span[@class="b-product_attribute-title js-selected-color m-color"]/text())').extract_first(),
            'sizes': response.xpath('//div[@class="b-select-size-outer"]/ul/li/a/@data-attr-value').extract(),
            'description': response.xpath(u'normalize-space(//div[@class="b-product_description-content"]/text())').extract_first(),
            'specs': specs,
            'image': response.xpath('//img[@itemprop="image"]/@src').extract_first(),
        }


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse_listing_page(self, response):
        """
        @url https://www.underarmour.com/on/demandware.store/Sites-US-Site/en_US/Search-UpdateGrid?start=0&sz=99&cgid=men-clothing-shorts
        @meta {"use_proxy": "True"}
        @returns requests 2
        """

        extra_item_info = response.meta.get('extra_item_info', {})

        #NOTE: when end up with re-captcha page du to throttling issue,
        # this code helps to send the request to selenium and helps us to solve the 
        # re-captcha manually.
        if response.status == 302:
            print(f"==> failed: {response.meta.get('proxy', '')}, url: {response.request.url}")
            print(f'==> using SeleniumRequest for {response.request.url}')
            from common.selenium_middleware import SeleniumRequest
            request = SeleniumRequest(url=response.request.url, callback=self.parse, dont_filter=True)
            request.meta.update({"use_cache":True})
            request.meta.update({"is_selenium_request":True})
            request.meta.update({'dont_redirect': True,'handle_httpstatus_list': [302]})
            request.meta['proxy'] =  response.meta.get('proxy', '')
            request.meta['extra_item_info'] = extra_item_info
            yield request
            return


        for product in response.xpath('//div[@data-cmp="productTile"]'):
            for color_variant in product.xpath('.//div[@class="b-tile-info"]//ul/li'):
                id = color_variant.xpath('.//a/@href').extract_first()
                color_filter = color_variant.xpath('.//a/@alt').extract_first()
                color_code = color_variant.xpath('.//a/@aria-label').extract_first()
                # print(f'{color_filter}-{color_code}: {url}')

                if extra_item_info.get('Color', '') and extra_item_info.get('Color', '') != color_filter:
                    continue
                else:
                    if id:
                        id = self.base_url + id
                        if not self.extra_item_infos.get(id, None):
                            self.extra_item_infos[id] = {}

                        self.update_extra_item_infos(id, extra_item_info)
                        # self.extra_item_infos.get(id).update(extra_item_info)
                        yield self.create_request(url=id, callback=self.crawlDetail)


    def parse(self, response):
        """
        @url https://www.underarmour.com/en-us/c/mens/clothing/shorts/
        @meta {"use_proxy": "True"}
        @returns requests 2
        """

        query_string = response.xpath('//div[@id="bodyPage"]/@data-querystring').extract_first()
        total = response.xpath('//div[@class="b-plp_header-results_count js-products_count"]/@data-analytics-plp-count').extract_first()

        url = self.listing_page_url.format(limit=total, offset=0) + f'&{query_string}'
        yield self.create_request(url=url, callback=self.parse_listing_page)

        # parse filter
        for filter in response.xpath('//div[@class="b-refinements-container js-canonical-url"]/div[contains(@class, "b-refinements-item js-refinements-item")]'):
            filter_name = filter.xpath('./@data-analytics-plp-filter-title').extract_first()
            if filter_name in self.filter_list:
                for filter_button in filter.xpath('.//ul/li/button'):
                    filter_value = filter_button.xpath('./@data-analytics-plp-filter-value').extract_first()
                    filter_url = filter_button.xpath('./@data-href').extract_first()
                    if filter_url:
                        filter_url = self.base_url + filter_url

                        parts = urlparse(filter_url)
                        query_dict = parse_qs(parts.query)
                        query_dict.update({'sz':[total], 'start': ['0']})
                        filter_url = parts._replace(query=urlencode(query_dict, True)).geturl()

                        url = self.listing_page_url.split('?')[0] + '?' + filter_url.split('?')[-1]
                        print(url)
                        request = self.create_request(url=url, callback=self.parse_listing_page)
                        request.meta['extra_item_info'] = {
                                filter_name: filter_value,
                            }
                        yield request


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        request.meta.update({'dont_redirect': True,'handle_httpstatus_list': [302]})
        return request

