import scrapy
import json
from datetime import datetime


class ConverseSpider(scrapy.Spider):

    name = "converse"
    api_url = 'https://store.nike.com/html-services/gridwallData?country=US&lang_locale=en_US&gridwallPath=womens-converse-shoes/7ptZpirZoi3&pn={}'
    page = 1
    start_urls = [api_url.format(page)]

    category = "Womens Sneakers"
    brand = "Converse"
    source = 'converse.com'
    blob_name = 'converse.txt'

    def start_requests(self):
        for url in self.start_urls:
            request = scrapy.Request(url=url, callback=self.parse)
            #request.meta['proxy'] = "http://shp-mercari-us-d00001.tp-ns.com:80"
            yield request

    def parse(self, response):
        res = json.loads(response.text)
        items = res['sections'][0]['items']

        for item in items:
            image = ""
            if item.get('inWallContentCard', False):
                continue
            if item.get('colorways', None) is not None:
                image = item['colorways'][0]['imageUrl']
            product = {
                        'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'statusCode': "200",
                        'category': self.category,
                        'brand': self.brand,
                        'title': item['title'],
                        '@id': item['pdpUrl'],
                        'price': item['localPrice'],
                        'image': image,
                        'collection': "",
                        'material': ""
                       }
            yield self.parse_detail_page(product['@id'], product)

        if res['nextPageDataService']:
            self.page = self.page + 1
            request = scrapy.Request(self.api_url.format(self.page), callback=self.parse)
            #request.meta['proxy'] = "http://shp-mercari-us-d00001.tp-ns.com:80"
            yield request

    def parse_detail_page(self, url, item):
        print('parse_detail_page: ', url)

        request = scrapy.Request(url, callback=self.crawl_detail_page)
        request.meta['item'] = item
        #request.meta['proxy'] = "http://shp-mercari-us-d00001.tp-ns.com:80"
        return request

    def crawl_detail_page(self, response):
        item = response.meta['item']
        image = response.css('div.css-6loeq5 > picture > img.css-10f9kvm ::attr(src)').extract_first()
        item['image'] = image if image is not None else item['image']
        item['description'] = response.css('div.description-preview > p ::text').extract_first()
        item['style'] = response.css('ul.description-preview__features > li.description-preview__style-color ::text').extract_first()
        item['rating'] = response.css('div.product-review > p.d-sm-ib ::text').extract_first()
        yield item
