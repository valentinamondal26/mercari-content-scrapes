import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')

            series_number =  record.get('series_number', '')
            if series_number:
                series_number = f'Series {series_number}'
            
            card_number = str(record.get('card_number', ''))
            if int(card_number) < 10 and not re.findall(r'^00', card_number):
                card_number = f'00{card_number}'
            elif int(card_number) > 9 and int(card_number) < 100 and not re.findall(r'^0', card_number):
                card_number = f'0{card_number}'
            
            model = f'Animal Crossing Amiibo Card {model} {card_number}'

            compatible_games = ', '.join(record.get('compatible_games', ''))

            birthday = record.get('birthday', '')
            if birthday:
                birthday = re.findall(r'\d+\s+\w+', birthday, flags=re.IGNORECASE)[0]

            transformed_record = {
                'id': record['id'],
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate',''),
                'status_code': record.get('status_code',''),
                
                'category': record.get('category', ''),
                'brand': record.get('brand', ''),
                'title': record.get('title', ''),

                'model': model,
                'series': series_number,
                'compatible_product': compatible_games,
                'date_of_creation': birthday,
                'card_type': record.get('card_type', ''),
                'model_sku': card_number
            }
            return transformed_record
        else:
            return None
