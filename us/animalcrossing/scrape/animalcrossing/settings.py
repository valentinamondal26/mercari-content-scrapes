# Scrapy settings for animalcrossing project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'animalcrossing'

SPIDER_MODULES = ['animalcrossing.spiders']
NEWSPIDER_MODULE = 'animalcrossing.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
}