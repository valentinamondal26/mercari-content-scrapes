'''
https://mercari.atlassian.net/browse/USCC-448 - Nintendo Video gaming merchandise
'''
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import re
import os
import json

class AnimalcrossingSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from animal-crossing.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Video gaming merchandise

    brand : str
        brand to be crawled, Supports
        1) Nintendo

    Command e.g:
    scrapy crawl animalcrossing -a category='Video gaming merchandise' -a brand='Nintendo'
    """

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    name = 'animalcrossing'
    category = ''
    brand = ''
    source = 'animalcrossing.com'
    blob_name = 'animalcrossing.txt'
    base_url = 'http://animal-crossing.com'
    image_base_url = 'https://animal-crossing.com/amiibo/assets/img/cards/'

    url_dict = {
        'Video gaming merchandise': {
            'Nintendo': {
                'url': 'https://animal-crossing.com/amiibo/collections/series-1-4-amiibo-cards/#',
                'listing_page_api': 'https://animal-crossing.com/amiibo/assets/JSON/series14_en.json'
            }
        }
    }
    games_url = 'https://animal-crossing.com/amiibo/assets/JSON/games_en.json'


    ## NOTE: This source does not have separate landing page for the products. When we click on the product a card will appear on
    ## the landing page itself and it will display the product details. We need to form an url for each products since there is no landing page.
    ## LISTING_PAGE_API will have all the products available in the site and all the details for each product mentioned in the specs.


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        super(AnimalcrossingSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.listing_page_api = self.url_dict.get(category, {}).get(brand, {}).get('listing_page_api', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand},url:{url}".format(category=self.category,brand=self.brand,url=self.launch_url))


    def contracts_mock_function(self):
        self.launch_url = 'https://animal-crossing.com/amiibo/collections/series-1-4-amiibo-cards/#'
        import requests
        response = requests.get(self.games_url)
        self.form_games_dict(response)


    def start_requests(self):
        yield self.create_request(url=self.games_url, callback=self.form_games_dict)
        yield self.create_request(url=self.listing_page_api, callback=self.crawldetails)


    def crawldetails(self, response):
        """
        @url https://animal-crossing.com/amiibo/assets/JSON/series14_en.json
        @meta {"use_proxy": "True"}
        @scrape_values title card_number series_number compatible_games birthday card_type released
        """
        products_data = json.loads(response.text)
        for product in products_data.get('series14',[]):
            title = product.get('name', '')
            url = f'{self.launch_url}{title}'
            image_id = product.get('image', '')
            image = f'{self.image_base_url}{image_id}'
            card_number = product.get('id', '')
            compatible_games_list = []
            for game in product.get('games', ''):
                game_name = self.games_data_dict.get(game, '')
                compatible_games_list.append(game_name)

            yield {
                'id': url,
                'image': image,

                'status_code': str(response.status),
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),

                'category': self.category,
                'brand': self.brand,
                'title': title,
                'card_number': card_number,
                'series_number': product.get('release_set', ''),
                'compatible_games': compatible_games_list,
                'birthday': product.get('birthday', ''),
                'card_type': product.get('card_type', ''),
                'released': product.get('released', ''),
                'keywords': product.get('keywords', '')
            }


    def form_games_dict(self, response):
        games_data = json.loads(response.text)
        self.games_data_dict = {}
        for data in games_data.get('games', ''):
            self.games_data_dict.update({data.get('slug', ''): data.get('name', '')})


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)

        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({'use_cache': True})
        return request
