import hashlib
import re
import json

class Mapper(object):
    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawl_date','')
            status_code = record.get('status_code','')
            image = record.get('image','')
            title = record.get('title','')
            color = record.get('color', '')
            color = re.sub(re.compile(r'Shirt -'), '', record.get("color",'')).strip()
            price=str(record.get('price',''))
            # material = record.get('material', '').strip('\u2043').strip('-').strip('')
            material = re.sub(re.compile(r'\u2022|\u2043|-'), '', record.get("material",''))
            description_list = record.get('description', '')
            description=''
            if description_list:
                remove_list = ["Free Standard Delivery (Estimated 4-7 Working Days) from $75", "\nFree Express Delivery (Estimated 1-3 Working Days) from $150", "\nFree Returns on all orders"]
                description = ' '.join([i for i in description_list if i not in remove_list])
                description = re.sub(re.compile(r'\u2022|\u2043|-'),'',description)

            position=price.find("$")
            price=price[position+1:].strip('USD')
            pattern=re.compile(r"\d+\.*\d*\s*\%\s*[a-zA-Z]+",re.IGNORECASE)
            material=re.findall(pattern,material)
            pattern=r"[\d\.\%]*"
            material=[re.sub(pattern,'',m).strip() for m in material]
            material=list(set(material))
            material=', '.join(material)
        
  
            ner_query = title 

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": record.get('category',''),
                "brand": record.get('brand'),
                "image": image,
                "model": title.title(),
                "color": color,   
                "price":{
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "description": description,
                "material":material

            }
            return transformed_record
        else:
            return None

