'''
https://mercari.atlassian.net/browse/USCC-11 - Gymshark Pants, tights, leggings
https://mercari.atlassian.net/browse/USCC-239 - Gymshark Women’s Activewear Tops
'''

# -*- coding: utf-8 -*-
import scrapy
import datetime
import json
import requests
import random
import os
import re
from scrapy.exceptions import CloseSpider


class GymsharkSpider(scrapy.Spider):

    """
     spider to crawl items in a provided category and sub-category from gymshark.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Pants, tights, leggings
        2) Women’s Activewear Tops

     brand : str
      brand to be crawled, Supports
       1) Gymshark

     Command e.g:
     scrapy crawl gymshark -a category='Pants, tights, leggings' -a brand="Gymshark"
     scrapy crawl gymshark -a category="Women’s Activewear Tops" -a brand="Gymshark"

    """
    name = 'gymshark'

    category = None
    brand = None

    source = 'gymshark.com'
    blob_name = 'gymshark.txt'

    launch_url = None
    base_url = "https://www.gymshark.com"
    id_url = 'https://www.gymshark.com/collections/bottoms/products/'
    image_url = 'https://img.gymshark.com/image/fetch/q_auto,f_auto,w_525/https:'

    url_dict = {
        'Pants, tights, leggings': {
            'Gymshark': {
                'url': 'https://www.gymshark.com/collections/bottoms/womens',
            }
        },
        'Women’s Activewear Tops': {
            'Gymshark': {
                'url': 'https://www.gymshark.com/collections/t-shirts-tops/womens'
            }
        }
    }

    # The sku's from the listing page goes into param
    # eg: https://www.gymshark.com/search/?view=json&q=GLLG1931-*

    product_api='https://www.gymshark.com/search/?view=json&q={}'
    proxy_pool = [
        #    'http://shp-mercari-us-d00001.tp-ns.com/',
        #     'http://shp-mercari-us-d00002.tp-ns.com/',
        #     'http://shp-mercari-us-d00003.tp-ns.com/',
        #     # 'http://shp-mercari-us-d00004.tp-ns.com/',
        #     'http://shp-mercari-us-d00005.tp-ns.com/',
        #     'http://shp-mercari-us-d00006.tp-ns.com/',
        #     'http://shp-mercari-us-d00007.tp-ns.com/',
        #     'http://shp-mercari-us-d00008.tp-ns.com/',
        #     'http://shp-mercari-us-d00009.tp-ns.com/',
        #     'http://shp-mercari-us-d00010.tp-ns.com/',
        #     'http://shp-mercari-us-d00011.tp-ns.com/',
        #     'http://shp-mercari-us-d00012.tp-ns.com/',
        #     'http://shp-mercari-us-d00013.tp-ns.com/',
        #     'http://shp-mercari-us-d00014.tp-ns.com/',
        #     'http://shp-mercari-us-d00015.tp-ns.com/',
        #     'http://shp-mercari-us-d00016.tp-ns.com/',
        #     'http://shp-mercari-us-d00017.tp-ns.com/',
        #     'http://shp-mercari-us-d00018.tp-ns.com/',
        #     'http://shp-mercari-us-d00019.tp-ns.com/',
        #     'http://shp-mercari-us-d00020.tp-ns.com/',
    ]


    def __init__(self, category=None,launch_url=None,brand=None, *args, **kwargs):
        super(GymsharkSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand :
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand=brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category,brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category,brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
         if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://www.gymshark.com/collections/bottoms/womens
        @returns requests 1
        """

        all_links = response.xpath('//div[@class="prod-image-wrap"]/a/@href').getall()
        if all_links:
            for link in all_links:
                yield self.create_request(url=self.base_url+link,callback=self.parse_product)

        next_page_url = response.xpath('//div[@id="pagination"]/span[@class="next"]/a/@href').get()

        if next_page_url:
            yield self.create_request(url=next_page_url, callback=self.parse)


    def parse_product(self,response):
        """
        @url https://www.gymshark.com/products/gymshark-slounge-ribbon-bottoms-charcoal-marl
        @scrape_values title color id breadcrumb image price description material
        """

        items={}

        items['crawl_date'] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        items['status_code'] = str(response.status)
        items['brand'] = self.brand
        items['category'] = self.category
        items['title'] = response.xpath('//section[@class="pdp__content__info pinfo"]/h1/text()').get(default='')
        detail_list = response.xpath('//div[contains(@class, "pdp-accordion")]').xpath('./div[@class="pdp-accordion__body"]/span/text()').getall() + response.xpath('//div[contains(@class, "pdp-accordion")]').xpath('./div[@class="pdp-accordion__body"]//text()').getall()
        subs = '%'
        material = [i for i in detail_list if subs in i]
        description = ' '.join(detail_list)
        if detail_list:
            items['description']=detail_list[0]
        items['price'] = response.xpath('//div[@class="pinfo__price"]/span/text()').get(default='')
        if material:
            items['material'] = material[0]

        script=response.xpath('//script[@id="product-schema"]/text()').get()
        colors=[]
        ids=[]
        images=[]
        if script:
            script_json=json.loads(script)
            sku=script_json.get('sku','')
            position=sku.find('-')
            sku=sku[:position+1]+"*"
            url="https://www.gymshark.com/search/?view=json&q={}".format(sku)
            r = requests.get(url =url)
            js=json.loads(r.text)

            if isinstance(js,list):
                for item in js:
                    line=item.get('title','')
                    position=line.find('-')
                    colors.append(line[position+1:].strip())
                    ids.append(self.id_url+item.get('handle',''))
                    im=item.get('images',[])
                    images.append(im[0])

        for i in range(len(colors)):
            items['color']=colors[i]
            items['id']=ids[i]
            breadcrumbs=response.xpath('//nav[@class="pdp__breadcrumbs"]//text()').getall()
            breadcrumb=''
            pattern=r"\w+"
            for br in breadcrumbs:
                if re.findall(pattern,br)!=[]:
                    breadcrumb+=br+"|"
            breadcrumb=breadcrumb[:-1]
            position=breadcrumb.find('-')
            breadcrumb=breadcrumb[:position+1]+colors[i]
            items['breadcrumb']=breadcrumb
            items['image'] = self.image_url + images[i]
            item_colors={}
            item_colors.update(items)
            yield item_colors


    def create_request(self, url, callback,method=None,meta=None):
        if not method:
            request = scrapy.Request(url=url, callback=callback)
            if self.proxy_pool:
                request.meta['proxy'] = random.choice(self.proxy_pool)
        if method:
            request=scrapy.Request(url=url,callback=callback,method=method)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
