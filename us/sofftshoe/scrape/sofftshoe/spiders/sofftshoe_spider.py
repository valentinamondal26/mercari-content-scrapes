'''
https://mercari.atlassian.net/browse/USCC-689 - Söfft Women's Sandals
https://mercari.atlassian.net/browse/USCC-690 - Söfft Women's Flats
https://mercari.atlassian.net/browse/USCC-691 - Söfft Women's Heels
https://mercari.atlassian.net/browse/USCC-692 - Söfft Women's Shoes
https://mercari.atlassian.net/browse/USCC-694 - Söfft Women's Boots
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class SofftshoeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from sofftshoe.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Sandals
        2) Women's Flats
        3) Women's Heels
        4) Women's Shoes
        5) Women's Boots

    brand : str
        brand to be crawled, Supports
        1) Söfft

    Command e.g:
    scrapy crawl sofftshoe -a category="Women's Sandals" -a brand="Söfft"
    scrapy crawl sofftshoe -a category="Women's Flats" -a brand="Söfft"
    scrapy crawl sofftshoe -a category="Women's Heels" -a brand="Söfft"
    scrapy crawl sofftshoe -a category="Women's Shoes" -a brand="Söfft"
    scrapy crawl sofftshoe -a category="Women's Boots" -a brand="Söfft"
    """

    name = 'sofftshoe'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'sofftshoe.com'
    blob_name = 'sofftshoe.txt'

    base_url = 'https://www.sofftshoe.com'

    url_dict = {
        "Women's Sandals": {
            "Söfft": {
                'url': 'https://www.sofftshoe.com/Womens/Sandals/Newest/All-Sizes/Page/1',
            }
        },
        "Women's Flats": {
            "Söfft": {
                'url': 'https://www.sofftshoe.com/Womens/Flats/Newest/All-Sizes/Page/1',
            }
        },
        "Women's Heels": {
            "Söfft": {
                'url': [
                    'https://www.sofftshoe.com/Womens/Heels/Newest/All-Sizes/Page/1',
                    'https://www.sofftshoe.com/Womens/Pumps/Newest/All-Sizes/Page/1',
                ]
            }
        },
        "Women's Shoes": {
            "Söfft": {
                'url': 'https://www.sofftshoe.com/Womens/Clogs/Newest/All-Sizes/Page/1',
            }
        },
        "Women's Boots": {
            "Söfft": {
                'url': [
                    'https://www.sofftshoe.com/Womens/Boots/Newest/All-Sizes/Page/1',
                    'https://www.sofftshoe.com/Womens/Booties/Newest/All-Sizes/Page/1',
                ]
            }
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SofftshoeSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.sofftshoe.com/Womens/Sandals/Saydee/Grey-Suede/SF0041828
        @meta {"use_proxy":"True"}
        @scrape_values id image style title price color sku description features
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'style': response.xpath('//section/h1/span[@class="new-style"]/text()').get(default='').strip(),
            'title': response.xpath('//section/h1/text()[normalize-space(.)]').get(default='').strip(),
            'price': response.xpath('//section/h1/span[@id="price"]/text()').get(default='').strip(),
            'color': response.xpath('//section/p[@class="was-h2"]/span[@class="color-style"]/text()[normalize-space(.)]').get(default='').strip(),
            'sku': response.xpath('//meta[@name="cludo:StyleNumber"]/@content').get(),
            'sizes': response.xpath('//select[@id="select-size"]/option[@data-instock]').xpath('normalize-space(./text())').getall(),
            'description': response.xpath('//div[@id="desc"]/text()[normalize-space(.)]').get(default='').strip(),
            'features': response.xpath('//div[@id="desc"]/ul/li/text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.sofftshoe.com/Womens/Sandals/Newest/All-Sizes/Page/1
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//ul[@class="grid-wrp"]/li[@class="grid-item"]'):
            product_urls = product.xpath('.//figure/p[@class="swatch"]/a/@href').getall() \
                or [product.xpath('.//figure/a/@href').get()]
            for product_url in product_urls:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//nav[@class="paging"]/ul/li/a/span[contains(@class, "arrow arrow-right")]/../@href').get()
        if next_page_link:
            next_page_link = self.base_url + next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
