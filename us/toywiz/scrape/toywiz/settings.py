# -*- coding: utf-8 -*-

# Scrapy settings for toywiz project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'toywiz'

SPIDER_MODULES = ['toywiz.spiders']

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT='Chrome/61.0.3163.100'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

ROBOTSTXT_OBEY = False

COOKIES_ENABLED = False

AUTOTHROTTLE_ENABLED = True

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

FEED_FORMAT='jsonlines'

ITEM_PIPELINES = {
    'toywiz.pipelines.GCSPipeline': 300,
}

# GCS settings - actual value be 'raw/toywiz.com/action_figures/wwe/2019-02-06_00_00_00/json/realreal.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'
GCS_BLOB_PREFIX_WITH_SUB_CATEGORY = 'raw/{source}/{category}/{brand}/{sub_category}/{timestamp}/json/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}