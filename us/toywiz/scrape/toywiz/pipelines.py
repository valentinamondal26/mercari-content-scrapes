# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from io import BytesIO
from datetime import datetime
from google.cloud import storage
from scrapy.exporters import JsonLinesItemExporter
import pandas as pd
import json
import numpy as np


class GCSPipeline(object):

    def __init__(self, gcs_blob_prefix,gcs_blob_prefix_with_sub_category):
        self.gcs_blob_prefix = gcs_blob_prefix
        self.gcs_blob_prefix_with_sub_category= gcs_blob_prefix_with_sub_category
        self.bucket_name = 'content_us'
        self.client = None
        self.bucket = None
        self.items = []
        self.items_merged=[]

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            gcs_blob_prefix=crawler.settings.get('GCS_BLOB_PREFIX'),
            gcs_blob_prefix_with_sub_category=crawler.settings.get('GCS_BLOB_PREFIX_WITH_SUB_CATEGORY')
        )

    def open_spider(self, spider):
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(self.bucket_name)

    def close_spider(self, spider):
        category = spider.category
        brand = spider.brand
        sub_category=spider.sub_category

        if not category or not brand:
            spider.logger.error('category or brand is empty, so skip uploading result to gcs')
            return

        source = spider.source
        timestamp = datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
        filename = spider.blob_name
        if not sub_category:
            destination_blob_name = self.gcs_blob_prefix.format(category=category,brand=brand, source=source, timestamp=timestamp, filename=filename)
            destination_blob_name = destination_blob_name.replace(" ", "_").replace(",", "").replace("'",'').lower()
        else:
            destination_blob_name = self.gcs_blob_prefix_with_sub_category.format(category=category,brand=brand, source=source, timestamp=timestamp, filename=filename,sub_category=sub_category)
            destination_blob_name = destination_blob_name.replace(" ", "_").replace(",", "").replace("'",'').lower()


        blob = self.bucket.blob(destination_blob_name)
        df=pd.DataFrame(self.items)
        unique_ids=list(set(df['id']))
        for id in unique_ids:
            group_df=df[df['id']==id].replace({np.nan:''})
            group_list=group_df.to_dict(orient='records')
            item={}
            item=item.fromkeys(group_list[0].keys(),'')
            for d in (group_list):        
                for key, value in d.items():
                    if key!="id":
                        if item[key]=='':
                            item[key]+=str(value)
                    else:
                        item['id']=d['id']

            self.items_merged.append(item)
                

        f = self._make_fileobj()
        blob.upload_from_file(f)

        spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
            bucket=self.bucket_name, blob=destination_blob_name))

    def process_item(self, item, spider):

        # cleanse the item values - replace None values with empty string ""
        image = item.get('image', '')
        item['image'] = image if image is not None else ""
        description = item.get('description', '')
        item['description'] = description if description is not None else ""

        self.items.append(item)

        return item

    def _make_fileobj(self):
        """
        Build file object from items.
        """

        bio = BytesIO()
        f = bio

        # Build file object using ItemExporter
        exporter = JsonLinesItemExporter(f)
        exporter.start_exporting()
        for item in self.items_merged:
            exporter.export_item(item)
        exporter.finish_exporting()

        # Seek to the top of file to be read later
        bio.seek(0)

        return bio
