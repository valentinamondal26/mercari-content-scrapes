'''
https://mercari.atlassian.net/browse/USCC-392 - Marvel Action Figures
https://mercari.atlassian.net/browse/USCC-366 - Hot Wheels Die-cast Toy Cars
https://mercari.atlassian.net/browse/USCC-31 - Mattel Fashion Dolls
https://mercari.atlassian.net/browse/USCC-359 - MGA Entertainment LOL Surprise!
https://mercari.atlassian.net/browse/USCC-362 - MGA Entertainment Mini Dolls & Playsets
https://mercari.atlassian.net/browse/USCC-384, https://mercari.atlassian.net/browse/USCC-332, https://mercari.atlassian.net/browse/USCC-234, https://mercari.atlassian.net/browse/USCC-89 - Wizards of the Coast Magic: The Gathering Cards & Merchandise
https://mercari.atlassian.net/browse/USCC-364 - Littlest Pet Shop Play Animals
https://mercari.atlassian.net/browse/USDE-537 - Topps Sports
'''

# -*- coding: utf-8 -*-
import scrapy
import datetime
import json
import random
import os
import re
from scrapy.exceptions import CloseSpider


class ToywizSpider(scrapy.Spider):

    """
     spider to crawl items in a provided category and sub-category from myvideogamelist.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Video Game Consoles

     brand : str
       1) Mattel
       2) Wizards of the Coast
       3) Littlest Pet Shop
       4) Marvel
       5) MGA Entertainment
       6) Hasbro
       7) Bandai
       8) Hot Wheels
       9) Pokemon
       10) NECA
       11) WWE
       12) Topps


     sub_category : str
      1) Mattel
      2) Mattel-Cyber
      3) Accessories
      4) Hasbro
      5) single-cards
      6) booster-boxes
      7) booster-packs
      8) sealed-decks
      9) Die-cast Toy Cars

     launch_url : str
     brand : str

     Command e.g:
     scrapy crawl toywiz  -a category='Video Games' -a brand="Mattel" -a launch_url='https://toywiz.com/search.php?search_query=Mattel%2B-cyber&sort=relevance&in_stock=1&_bc_fsnf=1&Company=Mattel%20Toys&section=content&Product%20Type=Action%20Figure'
     scrapy crawl toywiz  -a category='Die-cast Toy Cars' -a brand="Hot Wheels"
     scrapy crawl toywiz  -a category='Mini Dolls & Playsets' -a brand="MGA Entertainment"
     scrapy crawl toywiz  -a category='Play Animals' -a brand="Littlest Pet Shop"
     scrapy crawl toywiz  -a category='Sports' -a brand="Topps"

    """

    category = None
    launch_url = None
    brand = None
    sub_category = None
    next_page=None
    source = 'toywiz.com'
    name = 'toywiz'
    blob_name = 'toywiz.txt'
    base_url = "https://toywiz.com"
    link_count=0
    start_urls = ['https://toywiz.com']

    url_dict = {
        'Magic: The Gathering Cards & Merchandise':{
            'Wizards of the Coast':{
                'single-cards':'https://toywiz.com/magic-the-gathering/single-cards/',
                'booster-boxes':'https://toywiz.com/booster-boxes/',
                'booster-packs':'https://toywiz.com/magic-the-gathering/booster-packs/',
                'sealed-decks':'https://toywiz.com/magic-the-gathering/sealed-decks/'
            }
        },
        'Play Animals':{
            'Littlest Pet Shop':'https://toywiz.com/littlest-pet-shop/?_bc_fsnf=1&brand=694&Company=Hasbro+Toys'
        },
        'LOL Surprise!':{
            'MGA Entertainment':'https://toywiz.com/lol-surprise/?_bc_fsnf=1&Company=MGA+Entertainment'
        },
        'Mini Dolls & Playsets':{
            'MGA Entertainment':'https://toywiz.com/lol-surprise/?_bc_fsnf=1&Company=MGA+Entertainment'
        },
        'Action Figures':{
            'Marvel':'https://toywiz.com/marvel/?_bc_fsnf=1&Product+Type=Action+Figure'
        },
        'Comic Books':{
            'Marvel':'https://toywiz.com/marvel/comic-books/?_bc_fsnf=1&brand=720'
        },
        'Action Figures':{
            'Bandai':'https://toywiz.com/search.php?search_query=bandai%2B-cyber&sort=relevance&in_stock=1&section=content&_bc_fsnf=1&Company=Bandai%20America&Product%20Type=Action%20Figure'
        },
        'Action Figures':{
            'Hasbro':{
                'Hasbro':'https://toywiz.com/search.php?search_query=hasbro%2Baction%2Bfigures%2B-search&sort=relevance&in_stock=1&_bc_fsnf=1&Company%5B%5D=Hasbro%20Toys&Company%5B%5D=Hasbro&section=content',
                'Accessories':'https://toywiz.com/search.php?search_query=hasbro%2Baction%2Bfigures%2B-search&sort=relevance&in_stock=1&_bc_fsnf=1&Company=Hasbro&section=content'                
            }
        },
        'Die-cast Toy Cars':{
            'Hot Wheels':'https://toywiz.com/search.php?search_query=hot%2Bwheels%2Bcars%2B-search&sort=relevance&in_stock=1&section=content&_bc_fsnf=1&Company%5B%5D=Mattel%20Toys&Company%5B%5D=Mattel'
        },
        'Fashion Dolls':{
            'Mattel':{
                'Mattel':'https://toywiz.com/search.php?search_query=mattel%2Bdoll%2B-cyber&sort=relevance&in_stock=1&section=content&_bc_fsnf=1&Company=Mattel%20Toys',
                'Mattel-Cyber':'https://toywiz.com/search.php?search_query=Mattel%2B-cyber&sort=relevance&in_stock=1&_bc_fsnf=1&Company=Mattel%20Toys&section=content&Product%20Type=Action%20Figure'
            }
        },
        'Action Figures':{
            'NECA':'https://toywiz.com/search.php?search_query=NECA%2Baction%2Bfigure%2B-search&sort=relevance&in_stock=1&_bc_fsnf=1&Company=NECA&section=content'
        },
        'Pokémon Trading Card Game Cards & Merchandise':{
            'Pokemon':'https://toywiz.com/search.php?search_query=pokemon%2Bcard%2B-search&sort=relevance&in_stock=1&_bc_fsnf=1&brand=899&section=content'
        },
        'Sports':{
            'Topps':'https://toywiz.com/search.php?search_query=topps%2B-search&sort=relevance&in_stock=1&section=content&_bc_fsnf=1&brand=772'
        },
        'Action Figures':{
            'WWE':'https://toywiz.com/wwe-wrestling/?_bc_fsnf=1&Product+Type=Action%20Figure&brand=1327'
        }
    }
    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com/',
        'http://shp-mercari-us-d00002.tp-ns.com/',
        'http://shp-mercari-us-d00003.tp-ns.com/',
        'http://shp-mercari-us-d00004.tp-ns.com/',
        'http://shp-mercari-us-d00005.tp-ns.com/',
        'http://shp-mercari-us-d00006.tp-ns.com/',
        'http://shp-mercari-us-d00007.tp-ns.com/',
        'http://shp-mercari-us-d00008.tp-ns.com/',
        'http://shp-mercari-us-d00009.tp-ns.com/',
        'http://shp-mercari-us-d00010.tp-ns.com/',
        'http://shp-mercari-us-d00011.tp-ns.com/',
        'http://shp-mercari-us-d00012.tp-ns.com/',
        'http://shp-mercari-us-d00013.tp-ns.com/',
        'http://shp-mercari-us-d00014.tp-ns.com/',
        'http://shp-mercari-us-d00015.tp-ns.com/',
        'http://shp-mercari-us-d00016.tp-ns.com/',
        'http://shp-mercari-us-d00017.tp-ns.com/',
        'http://shp-mercari-us-d00018.tp-ns.com/',
        'http://shp-mercari-us-d00019.tp-ns.com/',
        'http://shp-mercari-us-d00020.tp-ns.com/',
    ]

    def __init__(self, category=None, brand=None, sub_category=None, *args, **kwargs):
        super(ToywizSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.sub_category=sub_category
        if category and brand and not sub_category:
            self.launch_url = self.url_dict.get(category,{}).get(brand,'')
        if category and brand and sub_category:
            self.launch_url = self.url_dict.get(category,{}).get(brand,{}).get(sub_category,'')
        if not self.launch_url:
            self.logger.error(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider(
                'Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            for key, value in meta.items():
                request.meta[key] = value
        return request

    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)
    

    def parse(self, response):
        """
        @url https://toywiz.com/search.php?search_query=topps%2B-search&sort=relevance&in_stock=1&section=content&_bc_fsnf=1&brand=772
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        callback=''
        all_links = response.xpath(
            '//div[@class="productBlock itemBlock"]/a/@href').getall()
        all_links = all_links[1:]
        callback=self.parse_product
        if all_links==[]:
            all_links=response.xpath('//div[@class="productsRow section-blocks"]/a/@href').getall()
            all_links=all_links[1:]
            callback=self.parse_product_links
        
        
        next_page_url=response.xpath('//div[@class="productsPaginationInner"]//a/@href').getall()
        if next_page_url!=[]:
            next_page_url=next_page_url[-1:][0]
        else:
            next_page_url=''
        if not self.next_page:
            self.next_page=next_page_url
        elif next_page_url==self.next_page:
            next_page_url=''
        elif self.next_page!=next_page_url:
            self.next_page=next_page_url
        for link in all_links:
            yield self.create_request(url=link, callback=callback)
        if next_page_url!='':
           if "https" not in next_page_url:
               yield self.create_request(url=self.base_url+next_page_url,callback=self.parse)
           else:
               yield self.create_request(url=next_page_url,callback=self.parse)
    
    def parse_product(self,response):
        """
        @url https://toywiz.com/monster-high-shriekwrecked-clawdeen-wolf-doll/
        @scrape_values title image breadcrumb price description condition upc company series product_type scale character rarity sub_brand product_line event
        @meta {"use_proxy":"True"}
        """
        schema={} 
        schema["crawl_date"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        schema["status_code"]=str(200)
        schema['image']=response.xpath('//div[@class="productImage"]/img/@src').get(default='')
        schema['title']=response.xpath('//div[@class="productTitle mobileHide"]/h1/text()').get(default='')
        breadcrumb=response.xpath('//ul[@class="breadcrumbs"]/li/a/text()').getall()
        schema['breadcrumb']='|'.join(breadcrumb)
        schema['id']=response.xpath('//meta[@property="og:url"]/@content').get(default='')
        schema['brand']=self.brand
        schema['category']=self.category
        schema['price']=response.xpath('//div[@class="buyboxValue"]/text()').get(default='')
        schema['description']=response.xpath('normalize-space(//div[@class="productDescription"]/text())').get(default='')
        schema['sku']=response.xpath('//div[@id="ProductDetails"]/following-sibling::label[contains(text(),"SKU:")]/following-sibling::text()').get(default='').strip()
        schema['condition']=response.xpath('//div[@id="ProductDetails"]/following-sibling::label[contains(text(),"Condition:")]/following-sibling::text()').get(default='').strip()
        schema['upc']=response.xpath('//div[@id="ProductDetails"]/following-sibling::label[contains(text(),"UPC:")]/following-sibling::text()').get(default='').strip()
        company=''
        series=''
        product_type=''
        scale=''
        character=''
        rarity=''
        sub_brand=''
        product_line=''
        event=''
        spec=' '.join(response.xpath('//script[contains(text(),"/***** SET CUSTOM FIELDS *****/")]/text()').getall())
        spec=spec.split('/***** SET CUSTOM FIELDS *****/')[1].split('var url')[0]
        pattern_custom=r"custom.push\([\W\w\d]*"
        pattern_json=r"\{[\w\W\d]*\}"
        custom_spec=re.findall(pattern_custom,spec)[0].split(');')
        for spec in custom_spec:
            spec_json=re.findall(pattern_json,spec)
            if spec_json!=[]:

                js=json.loads(spec_json[0].replace("\'",'"'))
                if js.get('name','')=="Company":
                    company=js.get('value','')
                elif js.get('name','')=="Series":
                    series=js.get('value','')
                elif js.get('name','')=="Product Type":
                    product_type=js.get('value','')
                elif js.get('name','')=="Rarity":
                    rarity=js.get('value','')
                elif js.get('name','')=="Sub-Brand":
                    sub_brand=js.get('value','')
                elif js.get('name','')=="Brand":
                    product_line=js.get('value','')
                elif js.get('name','')=="Event":
                    event=js.get('value','')
                elif js.get('name','')=="Scale":
                    scale=js.get('value','')
                    if "&quot" in scale:
                        scale=re.findall(r"\d+\.*\d*",scale)[0]+'"'
                elif js.get('name')=="Filter":
                    character=js.get('value','')
                    if "Character=" in character:
                        character=character.split('Character=')[1]
                        
        schema['company']=company
        schema['series']=series
        schema['product_type']=product_type
        schema['scale']=scale
        schema['character']=character
        schema['rarity']=rarity
        schema['sub_brand']=sub_brand
        schema['product_line']=product_line
        schema['event']=event


        yield schema  
       

    def parse_product_links(self, response):
        """
        @url https://toywiz.com/magic-the-gathering/single-cards/
        @returns requests 1
        """
        next_page_url=response.xpath('//div[@class="productsPaginationInner"]//a/@href').getall()
        if next_page_url!=[]:
                next_page_url=next_page_url[-1:][0]
        else:
                next_page_url=''
        if not self.next_page:
                self.next_page=next_page_url
        elif next_page_url==self.next_page:
                next_page_url=''
        elif self.next_page!=next_page_url:
                self.next_page=next_page_url

        callback=''
        all_links=response.xpath('//div[@class="productsRow section-blocks"]/a/@href').getall()
        callback=self.parse_product_links
        if all_links!=[]:
            for link in all_links:
                yield self.create_request(url=link,callback=self.parse)
            if next_page_url!='':
                    yield self.create_request(url=self.base_url+next_page_url,callback=self.parse)

        else:
            callback=self.parse_product
            all_links = response.xpath('//div[@class="productBlock itemBlock"]/a/@href').getall() 
            next_page_url=response.xpath('//div[@class="productsPaginationInner"]//a/@href').getall()
           
            if all_links!=[]:
                for link in all_links:
                    yield self.create_request(url=link,callback=callback,meta=response.meta)
                    
                if next_page_url!='':
                    yield self.create_request(url=self.base_url+next_page_url,callback=self.parse)


      
