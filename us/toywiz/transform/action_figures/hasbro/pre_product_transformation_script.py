import hashlib
import re
import html


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', '')
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')
            scale = html.unescape(record.get('scale', ''))
            pattern = re.compile(
                r"\baction\s*\-*figures*\b|\({1}pre\s*\-\s*order\s*ships\s*[a-zA-Z\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|\bexclusives*\b|\bmtg\b|\brarity\b|\blot\b|\bonline\s*code\b|\bcounts*\b|\bct\b|pre\s*\-*order|\d+\.*\d*\s*\-*\s*inch|Mini+\s*Figures*|Micro\s*Figures*|Studios\s*\:|\{\s*Full\s*Color\s*\}|POTF\s*\d*", re.IGNORECASE)

            # category , brand , rarity removed from model
            model = re.sub(pattern, '', model)
            model = model.replace('[', '').replace(']', '')
            model = re.sub(re.compile(r"\b{}\b".format(
                scale), re.IGNORECASE), '', model)
            model = model.replace(record.get('category', ''), '')
            model = model.replace(record.get('brand', ''), '')
            model = model.replace(record.get('rarity', ''), '')

            # model string fixes
            model = re.sub(re.compile(r"\bvol\s*\.*\s*\b",
                                      re.IGNORECASE), "Volume ", model)
            model = re.sub(re.compile(
                r"\bv\s*\.*\s*s\s*\.*\s*\b", re.IGNORECASE), " VS ", model)
            model = re.sub(r"PACHYCEPHALOSAURUS", "Pachycephalosaurus", model)
            model = re.sub(r"THE INFINITY GAUNTLET",
                           "The Infinity Gauntlet", model)
            model = re.sub(re.compile(r"Hulk with BIke",
                                      re.IGNORECASE), "Hulk with Bike", model)
            model = re.sub(re.compile(r"\bD\.\s*Va\b",
                                      re.IGNORECASE), "D.Va", model)
            model = re.sub(re.compile(r"\bEpisode I\b",
                                      re.IGNORECASE), "Episode 1", model)
            model = re.sub(re.compile(r"\bStar\s*\-\s*Lord\b",
                                      re.IGNORECASE), "Star Lord", model)

            if re.findall(re.compile(r"G.I", re.IGNORECASE), model) != []:
                model = re.sub(re.compile(r"\bB\s*A\s*T\b",
                                          re.IGNORECASE), "B.A.T", model)
            model = re.sub(re.compile(
                r"\(\s*ARTOO\s*\-\s*DETOO\s*\)", re.IGNORECASE), '', model)
            if re.findall(re.compile(r"AT\s*\-\s*TE\s*\(\s*All Terrain Tactical Enforcer\s*\)", re.IGNORECASE), model) != []:
                model = re.sub(re.compile(
                    r"\(\s*All Terrain Tactical Enforcer\s*\)", re.IGNORECASE), '', model)
            model = re.sub(r"\d+\.*\d*\s*\"", '', model)
            model = re.sub(re.compile(r"\ss\s|\ss$", re.IGNORECASE), "", model)
            model = ' '.join(model.split())
            
            series = html.unescape(record.get('series', ''))
            rarity = record.get('rarity', '').replace('Online Code Card', '')
            if "custom" in series or "Custom" in series or "CUSTOM" in series:
                return None
            if re.findall(re.compile(r"set of|Shirt|Boxers", re.IGNORECASE), model) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": 'Action Figures',
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "model": model,
                "description": record.get('description', ''),
                "character": html.unescape(record.get('character', '')),
                "manufacturer": html.unescape(record.get('company', '')),
                "sku": record.get('sku', ''),
                "ner_query": ner_query,
                "series": html.unescape(record.get('series', '')),
                "condition": record.get('condition', ''),
                "product_type": html.unescape(record.get('product_type', '')),
                "product_line": record.get("sub_brand", '')+" "+record.get("category", ''),
                "scale": html.unescape(record.get('scale', '')),
                "rarity": rarity,
                "upc": record.get('upc', ''),
                "price": {
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$", "")
                },

            }

            return transformed_record
        else:
            return None

