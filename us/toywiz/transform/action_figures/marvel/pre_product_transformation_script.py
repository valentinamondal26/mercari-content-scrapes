import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            scale=html.unescape(record.get('scale',''))
            #pattern to remove (Pre-order ships < month>),[<anything>],Exclusive,new,rarity
            pattern=re.compile(r"\baction\s*\-*figures*\s*s{0,1}\b|\({1}pre\s*\-\s*order\s*ships\s*[\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|\bexclusive\b|mtg|rarity|figure\s*s\b",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            #pattern to remove lot, online code ,count/ct
            pattern=re.compile(r"\blot\b|online\s*code|count|\bct\b|pre\s*\-*order",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            model=model.replace("'s",'')

            model=model.replace('[','').replace(']','')
            #model=model.replace(record.get('category',''),'')
            #if model.find(record.get('brand',''))==0:
                #model=model.replace(record.get('brand',''),'',1)
            model=model.replace(record.get('rarity',''),'')
            model=model.replace(record.get('scale',''),'')
            model=re.sub(re.compile(r"\bVer\s*\.|\bVer\b",re.IGNORECASE),'Version',model)
            pattern=r"\(\s*\d+\s*\)"
            if re.findall(pattern,model)!=[]:
                to_be_replaced=re.findall(pattern,model)[0]
                replace_word=to_be_replaced.replace(" ",'')
                model=model.replace(to_be_replaced,replace_word)
            pattern=re.compile(r"\(\s*electronic\s*\)",re.IGNORECASE)
            if re.findall(pattern,model)!=[]:
                model=re.sub(pattern,re.findall(pattern,model)[0].strip('()'),model)


            pattern=r"\#\s*\d+"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')

            pattern=re.compile(r"marvels*\s*x|Captain Marvels*|Ms\s*\.\s*marvels*",re.IGNORECASE)
            if re.findall(pattern,model)==[]:
                model=re.sub(re.compile(r"marvels*",re.IGNORECASE),'',model)
            else:
                model_words=re.findall(r"\w+",model)
                if model_words[0]=="marvel" or model_words[0]=="Marvel":
                    model=model.replace("marvel",'',1)
                    model=model.replace("Marvel",'',1)

            
            series=html.unescape(record.get('series',''))

            if re.findall(re.compile(r"all new",re.IGNORECASE),series)==[]:
                model=re.sub(re.compile(r"\(*\s*all new\s*\)*",re.IGNORECASE),'',model)

           
            model=' '.join(model.split())
            
            rarity=record.get('rarity','').replace('Online Code Card','')
            if "custom" in series or "Custom"  in series or "CUSTOM" in series:
                return None 
        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Action Figures',
                "brand":record.get('brand',''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":html.unescape(record.get('character','')),
                "manufacturer":html.unescape(record.get('company','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "condition":record.get('condition',''),
                "product_type":html.unescape(record.get('product_type','')), 
                "product_line":record.get("sub_brand",'')+" "+record.get("category",'') ,
                "scale":html.unescape(record.get('scale','')),
                "rarity":rarity,
                "upc":record.get('upc',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None


