#Key Mapper for toywiz data - model  combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]
                    break

        return key_field
