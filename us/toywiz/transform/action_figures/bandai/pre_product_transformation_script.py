import hashlib
import re
import html


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', '')
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')
            # model transformations

            # remove action figures, pre order ships()
            pattern = re.compile(
                r"action\s*figures*|\(\s*pre\s*\-\s*order\s*ships*\s*\w+\s*\)|new|exclusive|\bExclusive Action Figure\b", re.IGNORECASE)
            model = re.sub(pattern, '', model)

            # remove inch details from model
            inch_pattern = r"\[{1}\s*\d+\"*\s*\]{1}"
            if re.findall(inch_pattern, model) != []:
                replace_text = re.findall(inch_pattern, model)[0].replace(
                    '[', '').replace(']', '').replace('"', ' Inch')
                model = model.replace(re.findall(
                    inch_pattern, model)[0], replace_text)

            # Version string transformation
            if "Version" not in model:
                model = model.replace('Ver', 'Version')
            model = model.replace('V.', 'Version ')

            # remove words
            words = ['loose', 'looses', 'package', 'packages',
                     'with charger', 'with chargers', 'with charger, black', 'pris']
            for word in words:
                pattern = re.compile(
                    r"\[+\s*[\w\W\d]*\s*"+word+"\s*[\w\W\d]*\s*\]+", re.IGNORECASE)
                model = re.sub(pattern, '', model)
            model = model.replace(record.get('category', ''), '')
            model = model.replace(record.get('brand', ''), '')

            # remove cycle , series from model
            model = re.sub(re.compile(
                r"Cylcle", re.IGNORECASE), "Cycle", model)
            series = html.unescape(record.get('series', ''))
            model = re.sub(re.compile(r"{}".format(
                series), re.IGNORECASE), '', model)

            # model transformation  [12] - 12 Inch
            model = re.sub(re.compile(
                r"\d+\.*\d*\s*\"|\d+\.*\d*\s*inch", re.IGNORECASE), "", model)
            if re.findall(r"\[\s*.*\s*\]", model) != []:
                words_inside_bracket = re.findall(
                    r"\[\s*.*\s*\]", model)[0].strip("[]")
                if len(re.findall(re.compile(r"{}".format(words_inside_bracket), re.IGNORECASE), model)) > 1:
                    model = re.sub(re.compile(
                        r"\[\s*.*\s*\]", re.IGNORECASE), "", model)

            # case-converting in model
            model = re.sub(re.compile(r"Metallic, MMPR",
                                      re.IGNORECASE), "Metallic", model)
            model = re.sub(re.compile(
                r"\[\s*MMPR\s*\]", re.IGNORECASE), "", model)

            # model - Ben 10 Omniverse Kickin Hawk to Ben 10 Omniverse Kickin' Hawk
            model = re.sub(re.compile(r"Ben 10 Omniverse Kickin Hawk",
                                      re.IGNORECASE), "Ben 10 Omniverse Kickin' Hawk", model)

            model = ' '.join(model.split())

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": 'Action Figures',
                "brand": record.get('brand', ''),
                "title": record.get('title', ''),
                "model": model,
                "description": record.get('description', ''),
                "character": record.get('character', ''),
                "company": record.get('company', ''),
                "sku": record.get('sku', ''),
                "ner_query": ner_query,
                "series": series,
                "condition": record.get('condition', ''),
                "product_type": record.get('product_type', ''),
                "scale": record.get('scale', ''),
                "upc": record.get('upc', ''),
                "price": {
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$", "")
                },

            }

            return transformed_record
        else:
            return None
