import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            scale=html.unescape(record.get('scale',''))
            #pattern to remove (Pre-order ships < month>),[<anything>],Exclusive,new,rarity
            pattern=r"[a|A][c|C][t|T][i|I][o|O][n|N]\s*\-*[F|f][i|I][g|G][u|U][r|R][e|E][s|S]{0,1}|\({1}[p|P]re\s*\-\s*[o|O]rder\s*[s|S]hips\s*[a-zA-Z\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|[N|n][e|E][w|W]|[e|E][x|X][c|C][l|L][u|U][s|S][i|I][v|V][e|E]|[M|m][t|T][G|g]|[r|R][a|A][r|R][i|I][t|T][y|Y]"
            matches=re.findall(pattern,model)
            model=model.replace(scale,'')
            for match in matches:
                model=model.replace(match,'')


            
            model=model.replace('[','').replace(']','')
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'')

            


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Action Figures',
                "brand":record.get('brand',''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":html.unescape(record.get('character','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "product_line":record.get("sub_brand",'')+" "+record.get("category",'') ,
                "scale":html.unescape(record.get('scale','')),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None

