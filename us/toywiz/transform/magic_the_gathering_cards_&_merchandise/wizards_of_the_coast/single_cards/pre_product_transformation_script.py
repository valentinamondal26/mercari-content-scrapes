import hashlib
import re
import html


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', '')
            id = record.get('id', '')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title', '')
            scale = html.unescape(record.get('scale', ''))

            # model transformation - remove brand , category
            pattern = re.compile(
                r"\({1}pre\s*\-\s*order\s*ships\s*[\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|exclusive|mtg|rarity|\bcard\b", re.IGNORECASE)
            model = re.sub(pattern, '', model)
            model = model.replace(scale, '')
            model = model.replace('[', '').replace(']', '')
            model = model.replace(record.get('category', ''), '')
            model = model.replace(record.get('brand', ''), '')

            # extract color
            color = record.get('character', '')
            if "Color=" in color:
                color = color.split('Color=')[1]

            # model transformation - ther to aether
            pattern = re.compile(
                r"\bther\b|\btherize\b|\bthersnipe\b|\btherspouts\b|\bthersnatch\b|\btherling\b", re.IGNORECASE)
            for word in model.split():
                if re.findall(pattern, word) != []:
                    model = re.sub(pattern, 'Ae'+word, model)
            model = re.sub(re.compile(
                r"Æther", re.IGNORECASE), "Aether", model)
            model = ' '.join(model.split())

            series = html.unescape(record.get('series', ''))
            product_type = html.unescape(record.get('product_type', ''))

            # model transformation retain one occurance of Stronghold from title when
            # has the series name aslo as Stronghold
            if re.findall(re.compile(r"\bStronghold\b", re.IGNORECASE), series) != []:
                if record.get('title').count('Stronghold') > 1 and model.count('Stronghold') > 1:
                    model = model.replace(series, '', 1)
                elif record.get('title').count('Stronghold') == 1 and model.count('Stronghold') == 1:
                    model = model.replace(series, '')
            else:
                model = model.replace(series, '')
            model = ' '.join(model.split())

            # model transformations
            rarity_removed = model.replace(record.get('rarity', ''), '')
            rarity_removed = ' '.join(rarity_removed.split())
            if re.findall(r"^[\#\d]+$", rarity_removed) == []:
                model = model.replace(record.get('rarity', ''), '')
            model = ' '.join(model.split())
            if model == "Volrath's":
                model += " "+series

            pattern = re.compile(
                r"bundle|Complete\s*Set|set\s*of|Draft\s*Booster\s*Pack|Deck\s*Builder\'*\s*s\s*Toolkit|Draft\s*Booster\s*Box|Theme\s*Booster", re.IGNORECASE)
            if re.findall(pattern, product_type) != []:
                return None
            pattern = re.compile(
                r"YuiGiOh\s*ZEXAL\s*Spectral\s*Procession|custom", re.IGNORECASE)
            if re.findall(pattern, model) != [] or re.findall(pattern, series) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": 'Magic: The Gathering Cards & Merchandise',
                "brand": "Wizards of the Coast",
                "title": record.get('title', ''),
                "model": model,
                "description": record.get('description', ''),
                # "character":html.unescape(record.get('character','')),
                "company": html.unescape(record.get('company', '')),
                "sku": record.get('sku', ''),
                "ner_query": ner_query,
                "series": html.unescape(record.get('series', '')),
                "condition": record.get('condition', ''),
                "product_type": html.unescape(record.get('product_type', '')),
                "product_line": html.unescape(record.get('product_line', '')),
                "sub_brand": record.get("sub_brand", ''),
                "scale": html.unescape(record.get('scale', '')),
                "rarity": record.get('rarity', ''),
                "card_color": color,
                "upc": record.get('upc', ''),
                "price": {
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$", "")
                },

            }

            return transformed_record
        else:
            return None
