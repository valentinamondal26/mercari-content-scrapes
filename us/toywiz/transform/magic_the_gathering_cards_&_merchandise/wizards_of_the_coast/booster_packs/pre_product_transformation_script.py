import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            scale=html.unescape(record.get('scale',''))
            pattern=re.compile(r"Trading Card Game|\({1}pre\s*\-\s*order\s*ships\s*[a-zA-Z\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|exclusive|mtg|rarity",re.IGNORECASE)
            #pattern=r"||[N|n][e|E][w|W]|[e|E][x|X][c|C][l|L][u|U][s|S][i|I][v|V][e|E]|[M|m][t|T][G|g]|[r|R][a|A][r|R][i|I][t|T][y|Y]"
            model=re.sub(pattern,'',model)
            model=model.replace(scale,'')            
            model=model.replace('[','').replace(']','')
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'')
            model=model.replace(record.get('rarity',''),'')
            pattern=r"\#\s*\d+"
            matches=re.findall(pattern,model)
            for match in matches:
               model=model.replace(match,'')
            
            product_type=html.unescape(record.get('product_type',''))
            if product_type=="Two-Player Clash Pack":
                product_type="Clash Pack"
            elif product_type=="Theme Decks":
                product_type="Theme Deck"
            elif product_type=="Theme Booster":
                product_type="Theme Booster Pack"
            elif product_type=="EDH Deck":
                product_type="Commander (EDH) Deck"
            elif product_type=="Deck":
                breadcrumb=record.get('breadcrumb','')
                if breadcrumb!='':
                    product_type=breadcrumb.split('|')[-1:][0]
                    if "Decks" in product_type:
                        product_type= product_type.replace("Decks","Deck")
                

            series=html.unescape(record.get('series',''))

            for word in product_type.split():
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)


            model=' '.join(model.split())
            
            if "custom" in series or "Custom"  in series or "CUSTOM" in series:
                return None 
            if product_type=="Deck Box" or product_type=="Complete Set":
                return None
            if product_type!="Duel Decks" and ("Decks" in product_type or "Theme Boosters" in product_type):
                return None
            if re.findall(re.compile(r"Lot of 5 Booster Packs",re.IGNORECASE),product_type)!=[]:
                return None
            if re.findall(re.compile(r"MtG Trading Card Game 2014 Core Set LOT of 5 Booster Packs",re.IGNORECASE),model)!=[]:
                return None
        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Magic: The Gathering Cards & Merchandise',
                "brand": "Wizards of the Coast",
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":html.unescape(record.get('character','')),
                "company":html.unescape(record.get('company','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "condition":record.get('condition',''),
                "product_type":product_type,
                "product_line":html.unescape(record.get('product_line','')),
                "sub_brand":record.get("sub_brand",''),
                "scale":html.unescape(record.get('scale','')),
                "upc":record.get('upc',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None


