import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            pattern=r"\({1}pre-order\s*ships\s*[a-zA-Z]+\s*\){1}|\[\s*[\w\d\W]+\s*\]|new"
            pattern=re.compile(pattern,re.IGNORECASE)
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'')

            if "Lil'" in model:
                model=model.replace("Lil’",'Li')

            model=model.replace('Secred','Secret')
            model=model.replace('Watch Watch','Watch')
            model=re.sub(re.compile(r"omg",re.IGNORECASE),'OMG',model)
            model=re.sub(re.compile(r"#\s*Ootd",re.IGNORECASE),'#OOTD',model)
            model=re.sub(re.compile(r"#\s*hairgoals",re.IGNORECASE),'#HairGoals',model)
            model=' '.join(model.split())
            pattern_remove=re.compile(r"bundle|lot|1x",re.IGNORECASE)
            #pattern_remove=r"[b|B][u|U][n|N][d|D][l|L][e|E]|[l|L][o|O][t|T]|[1][x|X]"
            matches=re.findall(pattern_remove,model)
            if matches!=[]:
                return None
           
            

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'LOL Surprise!',
                "brand": record.get('brand', ''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":html.unescape(record.get('character','')),
                "company":html.unescape(record.get('company','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "rarity":record.get('rarity',''),
                "series":html.unescape(record.get('series','')).title(),
                "condition":record.get('condition',''),
                "product_type":html.unescape(record.get('product_type','')),
                "sub_brand":record.get("sub_brand",''),
                "scale":html.unescape(record.get('scale','')),
                "upc":record.get('upc',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None


