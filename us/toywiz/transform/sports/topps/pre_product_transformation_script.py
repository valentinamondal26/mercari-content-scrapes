import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            #pattern to remove (Pre-order ships < month>),[<anything>],Exclusive,new
            pattern=r"\({1}[p|P]re\s*\-\s*[o|O]rder\s*[s|S]hips\s*[a-zA-Z\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|[N|n][e|E][w|W]|[e|E][x|X][c|C][l|L][u|U][s|S][i|I][v|V][e|E]"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')
            
            model=model.replace('[','').replace(']','')
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'')
        
            model=' '.join(model.split())

            pattern=re.compile(r"Vinyl\s*Figure|Mini\s*figure|Figurine|\[\s*Loose\s*\]",re.IGNORECASE)
            if re.findall(pattern,model)!=[]:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":"Sports",
                "brand":record.get('brand',''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "product_line":record.get("sub_brand",''),
                "rarity":record.get('rarity',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None


