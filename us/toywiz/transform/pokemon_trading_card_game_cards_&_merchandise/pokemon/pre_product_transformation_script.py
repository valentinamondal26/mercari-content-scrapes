import hashlib
import re
from colour import Color
import html
class Mapper(object):


    

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            #pattern to remove (Pre-order ships < month>),[<anything>],Exclusive,new,rarity
            def pattern_words(words):
                 return r'\b' + r'\b|\b'.join(words) + r'\b'
            pattern=re.compile(r"\({1}pre\s*\-\s*order\s*ships\s*[a-zA-Z\d\w\W]+\s*\){1}|new|exclusive|mtg|rarity",re.IGNORECASE)
            model=re.sub(pattern,'',model)
          
            
 
            #pattern to remove  online code ,count/ct
            pattern=re.compile(r"online\s*code|\bcounts*\b|\bct\b|pre\s*\-*order|",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            if re.findall(re.compile(r"\bTin Set\b",re.IGNORECASE),model)==[] and re.findall(re.compile(r"\bTin\b",re.IGNORECASE),model)!=[]:
                model=re.sub(re.compile(r"\bTin\b",re.IGNORECASE),'Tin Set',model)
                
            
                   
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'') 
            model=model.replace("!",'')

            product_type=html.unescape(record.get('product_type',''))
            product_line=html.unescape(record.get("sub_brand",''))

            if product_type=="Collector Tin Set" :
                product_type=product_type.replace('Set','')
            elif product_type=="Mini Tin" or product_type=="Mini Tin Box":
                product_type=product_type.replace('Box','')
                if "Set" not in product_type:
                    product_type+=" Set"
            elif product_type=="Mythical Collection Deluxe Box":
                product_type=product_type.replace("Deluxe",'') 
            elif product_type=="Pin Collection":
                product_type=product_type.replace('Collection','Box')
            elif product_type=="Tin":
                product_type+=" Set"
            elif product_type=="Trading Card Pack":
                product_type="Booster Pack"
            elif product_type=="Collection":
                product_type+=" Box"  
            product_type=' '.join(product_type.split())   

            
            #extract seires
            series=html.unescape(record.get('series',''))
            if series=='':
                meta_serires=['Secret Wonders', "2014 Collector's Chest", 'Sun & Moon Guardians Rising', 'Neo Revelation', 'Dark Rush', 'Diamond & Pearl Legends Awakened', '2005 World Championships', 'Hail Bilzzard', '2017 World Championships', 'Emerging Powers', 'Lost Link', '2014', '2016', 'Noble Victories', 'Knock Out', 'Red & Blue Collection', 'Neo Genesis', 'Phantom Forces', 'Black & White Next Destinies', 'Forbidden Light', 'Black & White Plasma Freeze', 'GX Tag Team Power Partnership', 'Promo', 'Sun & Moon Ultra Prism', 'Tag Team Powers', 'Undaunted', 'Black & White Legends', 'Fossil', 'HeartGold SoulSilver Unleashed', 'Shiny Collection', 'Mythical', 'XY Steam Siege', 'Platinum Arceus', 'Legends of Hoenn', 'X & Y Base Set', 'Dragon Majesty', 'XY Furious Fists', 'Power Keepers', 'Best of 2014', 'Galar Pals', '2016 World Championships', 'HeartGold SoulSilver Undaunted', 'Sword & Shield Rebel Clash', 'Platinum Rising Rivals', 'Organized Play Series 5', 'Black & White Noble Victories', 'BREAKpoint', '2013 Black & White Team Plasma', "2020 Collector's Chest", 'Organized Play Series 6', 'Sword and Shield', 'Black & White Legendary EX', 'Plasma Storm', 'Evolutions', 'Dragon Vault', 'Unseen Forces', 'Gym Heroes', 'Black & White Base Set', '2017 Legends of Alola', 'Hoenn Power', 'Celestial Storm', 'Island Guardians', 'Kanto Friends', 'Legendary Treasures', '2018', 'Organized Play Series 7', 'Sandstorm', 'Black & White Dark Explorers', 'Majestic Dawn', 'Organized Play Series 1', 'Sun & Moon Crimson Invasion', 'Black & White Boundaries Crossed', 'Pokemon League Promo', 'EX Deoxys', 'Black & White Plasma Storm', 'Diamond & Pearl Majestic Dawn', 'XY Evolutiones', 'Sun & Moon Lost Thunder', 'XY Primal Clash', 'XY Roaring Skies', 'Detective Pikachu', 'Expedition Base Set', 'Team Up', 'Unleashed', 'Jungle', "2018 Collector's Chest", 'GX Tag Team', 'HeartGold', 'Diamond & Pearl', 'HeartGold & Soulsilver', 'Platinum', "2015 Collector's Chest", 'Advanced Challenge', 'XY Ancient Origins', 'Aquapolis', 'Triumphant', 'Pokemon The Movie 2000', 'Black & White', 'Organized Play', 'Lost Thunder', "2017 Collector's Chest", 'Sun & Moon Shining Legends', 'Evolution Celebration', '2015 Legends of Hoenn', 'XY Mega Evolution', 'Mysterious Powers', 'Sun & Moon Unified Minds', 'Dragon', 'Black & White Legendary Treasures', 'Sun & Moon', 'Tag Team Generations', 'Skyridge', 'Southern Islands', 'Supreme Victors', 'Diamond & Pearl Secret Wonders', 'Generations Radiant Collection', '2019 World Championships', 'Furious Fists', 'Team Rocket Returns', 'Sun & Moon Hidden Fates', 'World Championships Deck 2013', 'Legends of Galar', 'Emerald', 'TV Animation Series 2', 'Tag Team', 'Steam Siege', 'Ruby & Sapphire', 'Delta Species', 'EX Dragon', 'Team Magma vs Team Aqua', 'Plasma Freeze', 'Unbroken Bonds', 'Battle Heart', 'BREAKthrough', '2018 World Championships', 'Dragons Exalted', 'Legends Awakened', 'Rising Rivals', 'Sword & Shield Base Set', '2017 Island Guardians', "2016 Collector's Chest", 'Ancient Origins', 'Crimson Invasion', 'Sun & Moon Team Up', 'Fates Collide', 'Sun & Moon Base Set', 'Sword & Shield', 'XY Flashfire', 'HeartGold SoulSilver', 'Legendary Collection', 'Organized Play Series 3', 'Fire Red & Leaf Green', "2019 Collector's Chest", 'Alcove', 'Crystal Guardians', 'Reviving Legends', '2009', 'Sun & Moon Forbidden Light', '2015', 'Burning Shadows', 'Play! Promo', 'Sun & Moon Unbroken Bonds', 'Legend Maker', 'Nintendo', 'Sun & Moon Celestial Storm', 'Basic', 'Dragon Frontiers', 'Organized Play Series 8', 'Flashfire', 'Stormfront', 'Sun & Moon Promo', 'Black & White Reissue', 'Legendary Treasures Radiant Collection', 'Dark Explorers', 'Gym Challenge', 'Diamond & Pearl Stormfront', 'Mysterious Treasures', 'Red Collection', 'Platinum Supreme Victors', 'Next Destinies', 'Double Crisis', 'XY Evolutions', 'Sun & Moon Dragon Majesty', 'Hidden Legends', 'World Championships Deck 2006', 'XY Fates Collide', 'Ultra Prism', 'Promo Cards', 'Power Beyond', 'Generations', 'Organized Play Series 9', 'Primal Clash', 'Legends of Johto', 'XY BREAKthrough', 'Unified Minds', 'Card Supplies', 'Sun & Moon Burning Shadows', 'Galar Partners', 'Guardians Rising', 'Organized Play Series 2', 'Trio Tin', 'Sun & Moon Cosmic Eclipse', 'Championship Promo', 'Detective Pikachu Promo', 'World Championships Deck 2011', 'XY BREAKpoint', 'Team Rocket', 'Shiny Kalos', 'Base Set', 'Psycho Drive', 'Roaring Skies', 'Promo Card', '2014 Black & White Legends of Kalos', 'Black & White Emerging Powers', 'Shining Legends', 'Chrome', 'Kanto Power', 'Call of Legends', 'Boundaries Crossed', 'Dragon Selection', 'Legendary Beasts', 'SoulSilver', 'Black & White Dragons Exalted', 'Plasma Blast', 'BreakPOINT', 'Premium Powers', 'Diamond & Pearl Great Encounters', 'Neo Destiny', 'Great Encounters', 'XY', 'XY Phantom Forces', 'Holon Phantoms', 'Cosmic Eclipse', 'Organized Play Series 4', 'Hidden Fates', 'Galar Collection', 'Neo Discovery', 'Black & White Plasma Blast', 'BreakTHROUGH']
                for word in  meta_serires:
                    pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                    if re.findall(pattern,record.get('title',''))!=[]:
                        series=word
                        break
            pattern=re.compile(r"\bHeartGold SoulSilver\b|\bHeartGold\b|\bSoulSilver\b",re.IGNORECASE)
            series=re.sub(pattern,"HeartGold & SoulSilver",series)
            
            if series.strip()=="X & Y":
                series="XY"
            if series.strip()=="XY X & Y":
                series="XY"

            pattern=re.compile(r"{}".format(product_line),re.IGNORECASE)
            if re.findall(pattern,series)==[]:
                series=product_line+" "+series

            
            if series.strip()=="X & Y":
                series="XY"

            
           
            
            #charaters with comma separated inside the model
            model=re.sub(re.compile(r"\-\s*GX",re.IGNORECASE),'',model)
            characters=['Absol', 'Aerodactyl', 'Aipom', 'Alakazam', 'Alolan Ninetales', 'Alolan Sandslash', 'Altaria', 'Ambipom', 'Ampharos', 'Anorith', 'Arcanine', 'Arceus', 'Armaldo', 'Articuno', 'Ash', 'Axew', 'Azelf', 'Azumarill', 'Bayleef', 'Beautifly', 'Bewear', 'Black Bolt', 'Black Kyurem', 'Blastoise', 'Blaziken', 'Blitzle', 'Braixen', 'Brock', 'Buizel', 'Bulbasaur', 'Buneary', 'Bunnelby', 'Burmy', 'Butterfree', 'Cacturn', 'Carbink', 'Celebi', 'Charizard', 'Charmander', 'Charmeleon', 'Chatot', 'Cherrim', 'Chespin', 'Chikorita', 'Chimchar', 'Chimecho', 'Chinchou', 'Clefable', 'Clefairy', 'Cleffa', 'Cobalion', 'Combee', 'Combusken', 'Corphish', 'Cresselia', 'Croagunk', 'Crobat', 'Cubone', 'Cyndaquil', 'Darkrai', 'Decidueye', 'Deoxys', 'Detective Pikachu', 'Dewott', 'Dialga', 'Diancie', 'Ditto', 'Dragonair', 'Drapion', 'Drifblim', 'Drifloon', 'Drilbur', 'Dusclops', 'Duskull', 'Eevee', 'Electabuzz', 'Electivire', 'Electrike', 'Emolga', 'Empoleon', 'Entei', 'Espeon', 'Feebas', 'Fennekin', 'Feraligatr', 'Flareon', 'Fletchinder', 'Fletchling', 'Froakie', 'Gallade', 'Garchomp', 'Gardevoir', 'Gastly', 'Genesect', 'Gengar', 'Geodude', 'Gible', 'Girafarig', 'Giratina', 'Glaceon', 'Glalie', 'Glameow', 'Gligar', 'Gloom', 'Golbat', 'Golduck', 'Golem', 'Gothita', 'Graveler', 'Grookey', 'Groudon', 'Grovyle', 'Growlith', 'Growlithe', 'Gulpin', 'Gyarados', 'Happiny', 'Haunter', 'Hawlucha', 'Hippopotas', 'Ho-Oh', 'Hoopa', 'HootHoot', 'Horsea', 'Houndoom', 'Houndour', 'Huntail', 'Igglybuff', 'Infernape', 'Inkay', 'Jigglypuff', 'Jirachi', 'Jolteon', 'Kabuto', 'Keldeo', 'Kingdra', 'Kirlia', 'Klefki', 'Koffing', 'Komala', 'Kricketune', 'Kyogre', 'Landorus', 'Lanturn', 'Lapras', 'Latias', 'Latios', 'Leafeon', 'Lickitung', 'Litleo', 'Litten', 'Litwick', 'Lucario', 'Ludicolo', 'Lugia', 'Luvdisc', 'Lycanroc', 'Machamp', 'Machop', 'Magby', 'Magearna', 'Magikarp', 'Magmar', 'Magmortar', 'Magnemite', 'Magneton', 'Makuhita', 'Mamoswine', 'Manaphy', 'Manectric', 'Mantine', 'Mareanie', 'Marill', 'Marshadow', 'Marshtomp', 'Mawile', 'Medicham', 'Meditite', 'Meganium', 'Meloetta', 'Meowth', 'Mesprit', 'Metagross', 'Mew', 'Mewtwo', 'Milotic', 'Mimikyu', 'Minccino', 'Minun', 'Misdreavus', 'Moltres', 'Monferno', 'Mothim', 'Mr. Mime Jr.', 'Mudkip', 'Munchlax', 'Munna', 'Murkrow', 'Natu', 'Necrozma', 'Ninetales', 'Noctowl', 'Noibat', 'Noivern', 'Numel', 'Omanyte', 'Oshawott', 'Pachirisu', 'Palkia', 'Pancham', 'Pangoro', 'Pansage', 'Passimian', 'Pelipper', 'Petilil', 'Phanpy', 'Pidove', 'Pignite', 'Pikachu', 'Pikipek', 'Plusle', 'Ponyta', 'Porygon', 'Porygon-Z', 'Primarina', 'Prinplup', 'Psyduck', 'Purrloin', 'Purugly', 'Pyroar', 'Quagsire', 'Quilladin', 'Raichu', 'Raikou', 'Rayquaza', 'Regice', 'Regigigas', 'Regirock', 'Registeel', 'Reshiram', 'Rhydon', 'Rhyperior', 'Riolu', 'Roselia', 'Roserade', 'Rotom', 'Sableye', 'Salamence', 'Samurott', 'Sandshrew', 'Sceptile', 'Scizor', 'Scorbunny', 'Scraggy', 'Seadra', 'Sharpedo', 'Shaymin', 'Shellos', 'Shieldon', 'Shiftry', 'Shinx', 'Silvally', 'Skitty', 'Skorupi', 'Slowking', 'Sneasel', 'Snivy', 'Snorlax', 'Sobble', 'Solgaleo', 'Spheal', 'Spinda', 'Squirtle', 'Staraptor', 'Steelix', 'Stunfisk', 'Stunky', 'Sudowoodo', 'Suicune', 'Swampert', 'Swellow', 'Swinub', 'Tapu Fini', 'Tapu Koko', 'Tapu Lele', 'Tepig', 'Terrakion', 'Togedemaru', 'Togekiss', 'Torchic', 'Torterra', 'Treecko', 'Tsareena', 'Turtwig', 'Typhlosion', 'Tyranitar', 'Ultra Necrozma', 'Umbreon', 'Unova', 'Unown', 'Vaporeon', 'Venusaur', 'Victini', 'Virizion', 'Volcanion', 'Vulpix', 'Wailmer', 'Wailord', 'Whiscash', 'White Kyurem', 'Wingull', 'Wobbuffet', 'Wooper', 'Wormadam', 'Wurmple', 'Xatu', 'Xerneas', 'Yanma', 'Yanmega', 'Yveltal', 'Zapdos', 'Zekrom', 'Zeraora', 'Zoroark', 'Zorua', 'Zubat', 'Zygarde']
            pattern=re.compile(r''+f'{pattern_words(characters)}',re.IGNORECASE)
            replace_word=', '.join(re.findall(pattern,model))
            character=html.unescape(record.get('character',''))
            if character=='':
                character=replace_word
            elif character!='' and replace_word!='':
                character+=", "+replace_word
                character=', '.join(list(set(character.split(", "))))


            if len(re.findall(pattern,model))>1:
                for word in character.split(", "):
                    model=re.sub(re.compile(r"{}".format(word),re.IGNORECASE),"$",model)
                start=model.find("$")
                end=model.rfind("$")
                if start!=-1 and end!=-1:
                    model=model[:start]+replace_word+model[end+1:]

            rarity=record.get('rarity','').replace('Online Code Card','')
            for word in rarity.split():
                model=re.sub(re.compile(r"\b{}\b".format(word),re.IGNORECASE),'',model)
            
           
            

            #movinf color to rear end if  'Elite trainder box' in model
            if re.findall(re.compile(r"Elite trainer box",re.IGNORECASE),model)!=[]:
                color=''
                _color = [i for i in re.findall(r"\w+",model) if self.check_color(i)]
                if _color!=[]:
                    for cl in _color:
                        if cl!='':
                            color=cl
                            break
                pattern=re.compile(r"\(*\s*{}\s*\)*".format(color),re.IGNORECASE)
                if re.findall(pattern,model)!=[]:
                        model=re.sub(r"\[|\]",'',model)
                        start=model.index(re.findall(pattern,model)[0])
                        end=start+len(re.findall(pattern,model)[0])
                        model=model[0:start]+model[end:]+ " "+model[start:end]
            pattern=re.compile(r"\[{1}\s*[\w\d\W]+\s*\]{1}",re.IGNORECASE)
            model=re.sub(pattern,'',model)



            
            pattern=re.compile(r"\b\d+\b")
            if re.findall(pattern,model)!=[] and "#" not in model:
                if len(model)==len(re.findall(pattern,model)[0])+model.index(re.findall(pattern,model)[0]):
                    model=re.sub(pattern,"#"+re.findall(pattern,model)[0],model)


            model=' '.join(model.split())
            model_with_series=model
            for word in series.split():
                model=re.sub(re.compile(r"\b{}\b".format(word),re.IGNORECASE),'',model)

            model=' '.join(model.split())           
            if re.findall(r"^[\#\d]+$",model)!=[]:
                model=model_with_series


           
            
            
            
            model=' '.join(model.split())
            model=re.sub(re.compile(r"\bX\s*\&\s*Y\b",re.IGNORECASE),'',model)
            model=re.sub(r"^[\&\s*\&*\s*]+|^[\,\s*\,*\s*]+",'',model)
            model=re.sub(re.compile(r"\bex\b",re.IGNORECASE),"EX",model)
            model=re.sub(re.compile(r"\b\'\s*s\b",re.IGNORECASE),'',model)
            model=' '.join(model.split())


            product_type_donot_load=['binder','Miscellaneous','Sleeves','Lot','Album','2-Pack','3-Pack','Portfolio','Box Display','case','Cards','Counter','display','Figure','Shield','Flip Box','Plastic Coin','Play Mat','Playmat','Set of ','Wallet','Storage Box','Sheet','Deck Box']
            for word in product_type_donot_load:
                if word.lower() in product_type or word.upper() in product_type or word.title() in product_type:
                    return None        
            if product_type=="Set" or product_type=="set" or product_type=="SET":
                return None
            
           
            

            pattern=re.compile(r"\blot\s*of\b|\bonline code card\b|\bpromo code\b",re.IGNORECASE)
            if re.findall(pattern,model)!=[] or re.findall(pattern,record.get('title',''))!=[]:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Pokémon Trading Card Game Cards & Merchandise',
                "brand":record.get('brand',''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":character,
                "manufacturer":html.unescape(record.get('company','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":series,
                "condition":record.get('condition',''),
                "product_type":product_type,
                "product_line":html.unescape(record.get("sub_brand",'')),
                "scale":html.unescape(record.get('scale','')),
                "rarity":rarity,
                "upc":record.get('upc',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

