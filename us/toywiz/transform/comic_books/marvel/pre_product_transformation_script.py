import hashlib
import re
import json
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            rating=re.findall(r"Rated\s*\d*\w*\s*\+{1}",record.get('description',''))
            rating=[rating[0] if rating!=[] else ''][0]
            rarity=record.get('rarity','').replace('Limted to 4000','')


            character=html.unescape(record.get('character',''))
            event=''
     
            if "Event=" in character:
                event=character.split('Event=')[1]
                character=''
            elif "=" in character:
                character=''
            model=record.get('title','')
            #pattern to remove (Pre-order ships < month>),[<anything>],Exclusive,new,marvel,Comic Book
            pattern=re.compile(r"\({1}pre\s*\-\s*order\s*ships\s*[a-zA-Z\d\w\W]+\s*\){1}|exclusive|comics*|book|Limited\s*to\s*\d+|\bPolybagged\b",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            
            
            model=re.sub(re.compile(r"\[Christopher Trading Card Cover Variant\]",re.IGNORECASE),'Variant Trading Card Cover',model)

            #extarct artist and add varaint covers
            if re.findall(re.compile(r"\[\s*[\w\W\D]*\s*\]",re.IGNORECASE),model)!=[]:
                words=[character,html.unescape(record.get('series','')),"Marvels* X","marvels*s","Black Cat","\d+","\d+\s*\w{1,3}"]
                if re.findall(re.compile(r"Vairant cover|Variant cover|\bart\b|\bcover\b",re.IGNORECASE),re.findall(re.compile(r"\[\s*[\w\W\D]*\s*\]",re.IGNORECASE),model)[0])!=[]:
                    variant=re.findall(r"\[\s*[\w\W\d]+\s*[Variant|Vairant]*\s*C*o*v*e*r*\s*\]",model)
                    if variant!=[]:
                        artist=re.sub(re.compile(r"variant|cover|\[|\]",re.IGNORECASE),'',variant[0]).strip()
                        variant_words=[]
                        for word in words:
                            pattern=re.compile(r"\b{}\b".format(word),re.IGNORECASE)
                            if re.findall(pattern,artist)!=[]:
                                variant_words.append(re.findall(pattern,artist)[0])
                                artist=re.sub(pattern,'',artist)

                        artist=' '.join(artist.split())
                        
                        if artist!='':
                            model=re.sub(re.compile(r"\[{1}\s*[\w\d\W]+\s*\]{1}",re.IGNORECASE),'',model)
                            model+=" "+' '.join(variant_words)+" Variant Cover by {artist}".format(artist=artist)
                        else:
                            model=re.sub(r"\[|\]",'',model)

                elif re.findall(re.compile(r"Variant|Vairant",re.IGNORECASE),re.findall(re.compile(r"\[\s*[\w\W\D]*\s*\]",re.IGNORECASE),model)[0])!=[]:
                    model=re.sub(r"\[|\]",'',model)
                else:
                    model=re.sub(r"\[|\]",'',model)
                    #model=re.sub(re.compile(r"\[{1}\s*[\w\d\W]+\s*\]{1}",re.IGNORECASE),'',model)
            
            pattern=re.compile(r"Avengers\s*Vs\s*X\s*\-*\s*Men\s*Avengers\s*Vs\s*\.*\s*X\s*\-*\s*Men",re.IGNORECASE)
            model=re.sub(pattern,"Avengers Vs X-Men",model)
            model=re.sub(re.compile(r"Dellotto|Dell Otto",re.IGNORECASE),"Dell'Otto",model)
            
            
            model=model.replace('[','').replace(']','')
            model=model.replace(record.get('category',''),'')
            #vol. to Volume
            model=re.sub(re.compile(r"\bvol\s*\.*\s*\b",re.IGNORECASE),"Volume ",model)
            model=re.sub(re.compile(r"\bv\s*\.*\s*s\s*\.*\s*\b",re.IGNORECASE)," VS ",model)
            #Do dont remove marvels x, captain marvel
            if re.findall(re.compile(r"marvels*\s*x|Captain Marvel",re.IGNORECASE),model)==[]:
                model=re.sub(re.compile(r"marvels*",re.IGNORECASE),'',model)
            else:
                model_words=re.findall(r"\w+",model)
                if model_words[0]=="marvel" or model_words[0]=="Marvel":
                    model=model.replace("marvel",'',1)
                    model=model.replace("Marvel",'',1)
            
            model=re.sub(re.compile(r"\.HU",re.IGNORECASE)," Hunted ",model)
            model=re.sub(re.compile(r"Gerald Peral",re.IGNORECASE),"Gerald Parel",model)
            model=model.replace('Vairant','')
            model=' '.join(model.split())
            


            


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Comic Books',
                "brand":record.get('brand',''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },
                "rating":rating,
                "rarity":rarity,
                "event":event,
                "character":character,
                "upc":record.get('upc',''),
                "item_condition":record.get('condition','')

            }

            return transformed_record
        else:
            return None


