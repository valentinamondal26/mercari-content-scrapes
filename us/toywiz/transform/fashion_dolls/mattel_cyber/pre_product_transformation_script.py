import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            scale=html.unescape(record.get('scale',''))
            #pattern to remove (pre-order ships <month>), new ,exclusive,[<anything>]
            pattern=r"\({1}[p|P]re\s*\-\s*[o|O]rder\s*[s|S]hips\s*[a-zA-Z]+\s*\){1}|\[\s*[\w\d\W]+\s*\]|[N|n][e|E][w|W]|[e|E][x|X][c|C][l|L][u|U][s|S][i|I][v|V][e|E]"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')
            
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')

            scale_words=re.findall(r"\w+",scale)
            for word in scale_words:
                model=model.replace(word,'')
                
            pattern=r"\#\s*\d+"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')

            
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'')

            model=' '.join(model.split())
        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Fashion Dolls',
                "brand": record.get('brand', ''),
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":html.unescape(record.get('character','')),
                "manufacturer":html.unescape(record.get('company','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "condition":record.get('condition',''),
                "product_type":html.unescape(record.get('product_type','')),
                "product_line":html.unescape(record.get('sub_brand','')),
                "scale":html.unescape(record.get('scale','')),
                "upc":record.get('upc',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None

