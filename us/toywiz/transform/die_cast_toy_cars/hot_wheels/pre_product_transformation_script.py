import hashlib
import re
import html
class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description','')
            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title','')
            scale=html.unescape(record.get('scale',''))
            pattern=r"\({1}[p|P]re\s*\-\s*[o|O]rder\s*[s|S]hips\s*[a-zA-Z\d\w\W]+\s*\){1}|\[{1}\s*[\w\d\W]+\s*\]{1}|[N|n][e|E][w|W]|[e|E][x|X][c|C][l|L][u|U][s|S][i|I][v|V][e|E]|[M|m][t|T][G|g]|[r|R][a|A][r|R][i|I][t|T][y|Y]"
            matches=re.findall(pattern,model)
            model=model.replace(scale,'')
            for match in matches:
                model=model.replace(match,'')
            model=model.replace('[','').replace(']','')
            model=model.replace(record.get('category',''),'')
            model=model.replace(record.get('brand',''),'')
            model=model.replace(record.get('rarity',''),'')
            model=model.replace(record.get('scale',''),'')
            character=record.get('character','')
            color=''
            vehicle_make=''
            wheel_type=''
            if "Color=" in character:
                color=character.split('Color=')[1]
                character=''
            elif "Wheel Type=" in character:
                wheel_type=character.split('Wheel Type=')[1]
                character=''
            elif "Vehicle=" in character:
                vehicle_make=character.split('Vehicle=')[1]
                character=''

            pattern=r"\#\s*\d+"
            matches=re.findall(pattern,model)
            for match in matches:
                model=model.replace(match,'')

            position=model.find(html.unescape(record.get('product_type','')))
            model=model[:position]

            product_type=html.unescape(record.get('product_type',''))
            product_type=product_type.replace('Die-Cast','Diecast')
            product_type=product_type.replace('Die Cast','Diecast')

            #fish’d & chip'd - capitalize to - Fish'D & Chip'D
            model=re.sub(r"[f|F][i|I][s|S][h|H]\s*\'*[d|D]*[n|N]*\s*\&*\s*[c|C][h|H][i|I][p|P]\s*\'*[d|D]*[n|N]*","Fish'D & Chip'D",model)
            model=' '.join(model.split())
            series=html.unescape(record.get('series',''))
            #set of ,Die Cast Car Set
            product_type_pattern=r"[s|S][e|E][t|T]\s*[o|O][f|F]|[d|D][i|I][e|E]\s*[C|c][a|A][s|S][t|T]\s*[c|C][a|A][r|R]\s*[s|S][e|E][t|T]"
            matches =re.findall(product_type_pattern,product_type)
            if matches!=[] or model=="20" or "DVD" in model:
                return None
                
            if "custom" in series or "Custom"  in series or "CUSTOM" in series:
                return None 
        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category":'Die-cast Toy Cars',
                "brand":"Hot Wheels",
                "title":record.get('title',''),
                "model":model,
                "description": record.get('description', ''),
                "character":character,
                "manufacturer":html.unescape(record.get('company','')),
                "sku":record.get('sku',''),
                "ner_query":ner_query,
                "series":html.unescape(record.get('series','')),
                "condition":record.get('condition',''),
                "product_type":product_type,
                "product_line":record.get("sub_brand",''),
                "Category":record.get('product_line',''),
                "scale":html.unescape(record.get('scale','')),
                "rarity":record.get('rarity',''),
                "color":color,
                "vehicle_make":vehicle_make,
                "wheel_type":wheel_type,
                "upc":record.get('upc',''),
                "price":{
                    "currency_code": "USD",
                    "amount": record.get('price').replace("$","")
                },

            }

            
            return transformed_record
        else:
            return None


