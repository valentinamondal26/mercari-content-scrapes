'''
https://mercari.atlassian.net/browse/USCC-0 - Two-pieces Cabana\ Life
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class CabanalifeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from cabanalife.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Cabana\ Life

    brand : str
        brand to be crawled, Supports
        1) Two-pieces

    Command e.g:
    scrapy crawl cabanalife -a category="Cabana\ Life" -a brand="Two-pieces"
    """

    name = 'cabanalife'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = 'Cabana Life'
    brand = 'Two-pieces'
    source = 'cabanalife.com'
    blob_name = 'cabanalife.txt'

    url_dict = {
        "Cabana\ Life": {
            "Two-pieces": {
                'url': ['https://www.cabanalife.com/collections/bikini-bottoms',
                        'https://www.cabanalife.com/collections/bikini-tops',
                        'https://www.cabanalife.com/collections/tankini-tops'],
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(CabanalifeSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK', ''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")

    def contracts_mock_function(self):
        pass

    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)

    def crawlDetail(self, response):
        """
        @url
        @meta {"use_proxy":"True"}
        @scrape_values id
        """
<<<<<<< HEAD
<<<<<<< HEAD
=======
        shipping = response.xpath('//div[@itemprop="description"]/ul[@class="ul1"]/li/span/text()').getall()
        general_info = response.xpath('//div[@itemprop="description"]/ul/li/span/text()').getall()
        fabric_care = [bullet_point for bullet_point in general_info if bullet_point not in shipping]
>>>>>>> parent of 4e0de78f (one-piece crawling update)
=======
>>>>>>> parent of 680df417 (two-pieces crawling update)

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,
<<<<<<< HEAD
<<<<<<< HEAD
=======

            'title': response.xpath('//h1[@itemprop="name"]/text()').get(),
            'tags': re.findall(r'title : \"(.*?)\"', response.xpath('//script[contains(text(), "evmCollectionsDetails.push")]/text()').get()),
            'price': response.xpath('//span[@itemprop="price"]/@content').get(),
            'description': response.xpath('//div[@itemprop="description"]/p/span/text()').getall(),
            'sku': response.xpath("//span[@itemprop='sku']/text()").get(),
            'shipping_options': shipping,
            'fabric_care': fabric_care,

            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'sizes': response.xpath('//div[@class="select"]/select[@name="id"]/option/text()').getall(),

            # NOTE: color variant information is loaded dynamically, and so not availble through static crawling
            # 'colors': response.xpath('//div[@option-name="Color"]/div/ul/li/@orig-value').getall(),
            # 'color': response.xpath('//span[@class="swatch-variant-name"]/text()').get(),
>>>>>>> parent of 4e0de78f (one-piece crawling update)
=======
>>>>>>> parent of 680df417 (two-pieces crawling update)
        }

    def parse(self, response):
        """
        @url
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//'):
            product_url = product.xpath('./@href').extract_first()
            if product_url:
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//@data-grid-url').extract_first()
        if next_page_link:
            yield self.create_request(url=next_page_link, callback=self.parse)

    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache": True})
        return request
