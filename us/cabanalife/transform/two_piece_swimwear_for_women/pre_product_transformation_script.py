import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            description = '. '.join(record.get('description', []))
            title = record.get('title', '')
            ner_query = description or title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            pattern = ''
            tags = record.get('tags', [])
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', tags))), title)
            if match:
                pattern = match[0] if isinstance(match[0], str) and match[0] != title else ''
            pattern = re.sub(r'\s+', ' ', pattern).strip()

            model = title
            if pattern:
                model = re.sub(r'\b'+pattern+r'\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            material = ''
            materials_meta = ['Nylon', 'Spandex']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))),
                               description, flags=re.IGNORECASE)
            if not match:
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))),
                                   ' '.join(record.get('fabric_care', [])), flags=re.IGNORECASE)
            if match:
                material = '/'.join(match)

            product_type = ''
            if re.findall(r'\bTop\b', title, flags=re.IGNORECASE):
                product_type = 'Top'
            elif re.findall(r'\bBottoms\b', title, flags=re.IGNORECASE):
                product_type = 'Bottoms'

            model_sku = record.get('sku', '')
            if not model_sku:
                match = re.findall(r'Style #:(.*?).', description, flags=re.IGNORECASE)
                if match:
                    model_sku = match[0]
            model_sku = re.sub(r'Style #:|\u00a0', '', model_sku).strip()
            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "pattern": pattern,
                "material": material,
                'product_type': product_type,
                'model_sku': model_sku,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
