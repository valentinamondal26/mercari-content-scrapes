'''
https://mercari.atlassian.net/browse/USDE-1673 - Kiss Nail Art Designs
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider

from urllib.parse import urlparse, parse_qs, urlencode


class KissusaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from kissusa.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Nail Art Designs

    brand : str
        brand to be crawled, Supports
        1) Kiss

    Command e.g:
    scrapy crawl kissusa -a category='Nail Art Designs' -a brand='Kiss'
    """

    name = 'kissusa'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'kissusa.com'
    blob_name = 'kissusa.txt'

    url_dict = {
        'Nail Art Designs': {
            'Kiss': {
                'url': 'https://www.kissusa.com/nails/all-kiss-nails',
                'filters': ['Nail Base Color', 'Nail Bed Length', 'KISS NAIL BRANDS'],
            }
        },
    }

    extra_item_infos = {}

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(KissusaSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse, dont_filter=True)
            yield self.create_request(url=self.launch_url, callback=self.parse_filter, dont_filter=True)


    def crawlDetail(self, response):
        specs = {}
        for desc in response.xpath('//section[@id="product-detail-accordian__container"]/div[@class="panel description"]'):
            key = desc.xpath(u'normalize-space(./h2/button/text())').extract_first()
            value = desc.xpath('.//div[@class="panel-body"]//p/text()').extract_first()
            specs[key] =  value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
            'title': response.xpath('//meta[@property="og:title"]/@content').extract_first(),
            'title_0': response.xpath('//h1[@class="page-title"]/span[@id="product_title_01"]/text()').extract_first(),
            'title_1': response.xpath('//h1[@class="page-title"]/span[@id="product_title_02"]/text()').extract_first(),
            'title_2': response.xpath('//h1[@class="page-title"]/span[@id="product_title_03"]/text()').extract_first(),
            'price': response.xpath('//div[@class="product-info-price"]//span[@class="price"]/text()').extract_first(),
            'specs': specs,
        }


    def parse_filter(self, response):
        for filter in response.xpath('//div[@class="filter-options"]/div[contains(@class, "filter-options-item")]'):
            filter_name = filter.xpath('./div[@data-role="title"]/text()').extract_first()
            if filter_name in self.filters:
                for filter_item in filter.xpath('.//ol[@class="items"]/li[@class="item"]'):
                    filter_value = filter_item.xpath(u'normalize-space(./a/text())').extract_first()
                    url = filter_item.xpath('./a/@href').extract_first()
                    if url:
                        request = self.create_request(url=url, callback=self.parse)
                        request.meta['extra_item_info'] = {
                            filter_name: filter_value
                        }
                        yield request


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        self.extra_item_infos[id] = info


    def parse(self, response):
        for product in response.xpath('//ol[@class="products list items product-items"]/li[@class="item product product-item"]'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.update_extra_item_infos(id, response.meta.get('extra_item_info', {}))

                yield self.create_request(url=id, callback=self.crawlDetail)

        page_numbers = response.xpath('//div[@class="toolbar toolbar-products"][1]/p[@id="toolbar-amount"]/span[@class="toolbar-number"]/text()').extract()
        self.logger.info(f'==> page_numbers: {page_numbers}')

        if page_numbers and len(page_numbers) > 2 and int(page_numbers[1]) < int(page_numbers[2]):

            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            current_page = int(query_dict.get('p', ['0'])[0])
            query_dict.update({'p':[current_page + 1]})
            url = parts._replace(query=urlencode(query_dict, True)).geturl()
            self.logger.info(f'==> next_page_url: {url}')
            yield self.create_request(url=url, callback=self.parse)


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

