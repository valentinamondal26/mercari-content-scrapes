import hashlib
import re
from colour import Color

class Mapper(object):

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

    counter = 0
    def remove_duplicate_occurence(self, x, max):
        self.counter += 1
        return '' if self.counter > max else x.group()

    def map(self, record):
        if record:
            description = record.get('specs', {}).get('Details', '')
            description = description.replace('\r', '').replace('\n', '')
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            title = record.get('title', '')

            # product_line = ', '.join(record.get('KISS NAIL BRANDS', []))
            # if not product_line:
            #     product_line = record.get('title_0', '')
            # product_line = re.sub(re.compile(r'\bNails\b|\bKiss\b', re.IGNORECASE), '', product_line)
            # product_line = re.sub(r'\s+', ' ', product_line).strip()

            length = ', '.join(record.get('Nail Bed Length', []))
            if not length:
                length_info = record.get('title_2', '')
                if length_info and re.findall(r'\blength\b', length_info, flags=re.IGNORECASE):
                    length = length_info
            elif not re.findall(r'\blength\b', length, flags=re.IGNORECASE):
                length = length + ' Length'
            colors = record.get('Nail Base Color', [])
            if not colors:
                colors = [i.strip('') for i in title.split(' ') if self.check_color(i.strip(''))]
                colors = list(filter(None, colors))                
            if re.findall(r'\bNude\b', title, flags=re.IGNORECASE):
                colors.append('Nude')
            color = ', '.join(dict.fromkeys(colors))            

            model = f"{record.get('title_0', '')} {record.get('title_1', '')}"
            if length:
                model = f'{model} {length} Nails'
            model = re.sub(r'\bNails\b|\bNail\b', lambda x: self.remove_duplicate_occurence(x, 1), model, flags=re.IGNORECASE); self.counter = 0
            model = re.sub(r'\bKits\b|\bKit\b', lambda x: self.remove_duplicate_occurence(x, 1), model, flags=re.IGNORECASE); self.counter = 0
            if not re.findall(r'\bKISS\b', model, flags=re.IGNORECASE):
                model = f'KISS {model}'
            
            if not re.findall(r'\bKit\b|\bArt\b', model, flags=re.IGNORECASE):
                model = re.sub(r'(.* )(\w+ Nails)(.*)('+length+')', r'\1 \3 \4 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\b\d+\s*Pcs\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b' + x + r'\b', colors))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": title,
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                "color": color,
                # "product_line": product_line,
                'length': length,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
