'''
https://mercari.atlassian.net/browse/USCC-667 - BlackCraft Cult Men's T-Shirts
https://mercari.atlassian.net/browse/USCC-668 - BlackCraft Cult Men's Hoodies & Sweatshirts
https://mercari.atlassian.net/browse/USCC-669 - BlackCraft Cult Men's Sweaters
https://mercari.atlassian.net/browse/USCC-670 - BlackCraft Cult Men's Shorts
https://mercari.atlassian.net/browse/USCC-671 - BlackCraft Cult Men's Pants
https://mercari.atlassian.net/browse/USCC-672 - BlackCraft Cult Men's Shirts
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class BlackcraftcultSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from blackcraftcult.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's T-Shirts
        2) Men's Hoodies & Sweatshirts
        3) Men's Sweaters
        4) Men's Shorts
        5) Men's Pants
        6) Men's Shirts
    
    brand : str
        brand to be crawled, Supports
        1) BlackCraft Cult

    Command e.g:
    scrapy crawl blackcraftcult -a category="Men's T-Shirts" -a brand='BlackCraft Cult'
    scrapy crawl blackcraftcult -a category="Men's Hoodies & Sweatshirts" -a brand='BlackCraft Cult'
    scrapy crawl blackcraftcult -a category="Men's Sweaters" -a brand='BlackCraft Cult'
    scrapy crawl blackcraftcult -a category="Men's Shorts" -a brand='BlackCraft Cult'
    scrapy crawl blackcraftcult -a category="Men's Pants" -a brand='BlackCraft Cult'
    scrapy crawl blackcraftcult -a category="Men's Shirts" -a brand='BlackCraft Cult'
    """

    name = 'blackcraftcult'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'blackcraftcult.com'
    blob_name = 'blackcraftcult.txt'

    base_url = 'https://www.blackcraftcult.com'

    url_dict = {
        "Men's T-Shirts": {
            "BlackCraft Cult": {
                'url': 'https://www.blackcraftcult.com/collections/mens/Men%27s-Tees',
            }
        },
        "Men's Hoodies & Sweatshirts": {
            "BlackCraft Cult": {
                'url': [
                    'https://www.blackcraftcult.com/collections/mens/Zip-Up',
                    'https://www.blackcraftcult.com/collections/mens/Pullover',
                ],
            }
        },
        "Men's Sweaters": {
            "BlackCraft Cult": {
                'url': 'https://www.blackcraftcult.com/collections/sweaters',
            }
        },
        "Men's Shorts": {
            "BlackCraft Cult": {
                'url': 'https://www.blackcraftcult.com/collections/mens/BottomsM',
            }
        },
        "Men's Pants": {
            "BlackCraft Cult": {
                'url': 'https://www.blackcraftcult.com/collections/mens/BottomsM',
            }
        },
        "Men's Shirts": {
            "BlackCraft Cult": {
                'url': 'https://www.blackcraftcult.com/collections/mens/Tank',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BlackcraftcultSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.blackcraftcult.com/collections/mens/products/in-blackcraft-we-trust-blue-lunar-dye-tee
        @meta {"use_proxy":"True"}
        @scrape_values id title price image description
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//h1[@itemprop="name"]/text()').get(),
            'price': response.xpath('//p[@class="product-prices"]/span[@itemprop="price"]/text()').get(),
            'was_price': response.xpath('//p[@class="product-prices"]/span[@class="was hideWsg"]/text()').get(),
            'select_options': response.xpath('//div[@class="select"]//text()[normalize-space(.)]').getall(),
            'description': response.xpath('//div[@id="product-description"]/p//text()[normalize-space(.)]').getall() or \
                response.xpath('//div[@id="product-description"]/text()[normalize-space(.)]').getall() or \
                response.xpath('//div[@id="product-description"]/span/text()[normalize-space(.)]').getall() or \
                response.xpath('//div[@id="product-description"]/ul/li/text()').getall(),
            'breadcrumb': response.xpath('//div[@id="breadcrumb"]/*/text()').getall(),
            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'measurements': response.xpath('//div[@id="product-description"]/blockquote/p/text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.blackcraftcult.com/collections/mens/Men%27s-Tees
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.xpath('//div[@id="product-loop"]/div[contains(@class, "product-index")]'):
            product_url = product.xpath('.//a/@href').get()
            if product_url:
                if not product_url.startswith(self.base_url):
                    product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//link[@rel="next"]/@href').get()
        if next_page_link:
            if not next_page_link.startswith(self.base_url):
                next_page_link = self.base_url + next_page_link
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
