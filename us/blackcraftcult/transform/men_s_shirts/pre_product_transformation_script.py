import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ' '.join(record.get('description', []))
            description = description.replace('\n', ' ').strip()

            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            price = re.sub(r'Sold Out', '', price, flags=re.IGNORECASE)

            model = record.get('title', '')
            model = re.sub(r'\bUnisex\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'Muscle Tee', 'Muscle Tank', model, flags=re.IGNORECASE)
            model = re.sub(r'\s-\s|-\s', ' ', model)
            model = re.sub(r'(.*)(Muscle Tank)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Tank Top)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            if re.findall(r'\bMuscle\b', model, flags=re.IGNORECASE) and \
                not re.findall(r'\bMuscle Tank\b', model, flags=re.IGNORECASE):
                model = re.sub(r'\bMuscle\b', 'Muscle Tank', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            materials_meta = [
                'Cotton', 'Polyester', 'Rayon', 'Spandex', 'combed and ring spun cotton',\
                'combed / ring-spun cotton',
            ]
            material = ''
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(match)
            material = material.title()


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
