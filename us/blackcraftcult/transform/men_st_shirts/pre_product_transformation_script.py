import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ' '.join(record.get('description', []))
            description = description.replace('\n', ' ').strip()

            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            price = re.sub(r'Sold Out', '', price, flags=re.IGNORECASE)

            model = record.get('title', '')
            model = re.sub(r'\s-\s|\s-|-\s', ' ', model)
            words_to_remove = ['BCC', 'BC', 'Blackcraft', 'Unisex']
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', words_to_remove))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\b970\b', 'Create Your Own Future', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Tee)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(T-Shirt)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            d = {
                'x Villain': 'Villain Front Mask Collaboration T-Shirt',
                'x Villain 2': 'Villain Side Mask Collaboration T-Shirt',
                'Oddities': 'Strange & Unusual Oddities Trick Or Treat T-Shirt',
                'Oddities 2': 'Strange & Unusual Oddities Horror T-Shirt',
                'Oddities 3': 'Strange & Unusual Oddities Magic T-Shirt',
            }
            model = re.sub(r'|'.join(list(map(lambda x: r'^'+x+'$', d.keys()))), lambda x: d.get(x.group()), model.strip(), flags=re.IGNORECASE)
            model = re.sub(r'^x Villain 2$', 'Villain Side Mask Collaboration T-Shirt', model.strip(), flags=re.IGNORECASE)
            model = re.sub(r'^Tee$', 'Logo Tee', model.strip(), flags=re.IGNORECASE)
            model = re.sub(r'\sx\s|^x\s|\sx$', ' Collaboration ', model.strip(), flags=re.IGNORECASE)
            if not re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Tank', 'Muscle Tee', 'Tee', 'T-Shirt', 'Shirt']))), model, flags=re.IGNORECASE):
                model = f'{model} Shirt'
            model = re.sub(r'\s+', ' ', model).strip()

            materials_meta = [
                'Cotton', 'Polyester', 'Rayon', 'Spandex', 'combed and ring spun cotton',\
                'combed / ring-spun cotton',
            ]
            material = ''
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(match)
            material = material.title()

            if re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Youth']))), model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
