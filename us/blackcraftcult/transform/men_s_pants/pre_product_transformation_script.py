import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ' '.join(record.get('description', []))
            description = description.replace('\n', ' ').strip()

            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()
            price = re.sub(r'Sold Out', '', price, flags=re.IGNORECASE)

            model = record.get('title', '')
            model = re.sub(r'Joggers', 'Jogger Pants', model, flags=re.IGNORECASE)
            model = re.sub(r'\s-\s|\sx\s', ' ', model)
            words_to_remove = ['BC', 'Blackcraft']
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', words_to_remove))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Jogger Pants)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Pants)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            if not re.findall(r'\bPants\b', model, flags=re.IGNORECASE):
                model = model + ' Pants'
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'^Jogger Pants$', 'Logo Jogger Pants', model, flags=re.IGNORECASE)


            materials_meta = [
                'Cotton', 'Polyester', 'Rayon', 'Spandex', 'combed and ring spun cotton',\
                'combed / ring-spun cotton',
            ]
            material = ''
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), description, flags=re.IGNORECASE)
            if match:
                material = '/'.join(match)
            material = material.title()

            if not re.findall(r'\bJoggers\b|\bPants\b', record.get('title', ''), flags=re.IGNORECASE):
                return None


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "material": material,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
