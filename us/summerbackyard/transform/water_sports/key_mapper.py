#Key Mapper for summmerbackyard data - model  combination is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]!=[]:
                    key_field += entity["attribute"][0]["id"]
        return key_field
