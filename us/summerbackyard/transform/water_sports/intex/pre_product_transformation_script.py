import re
import hashlib
from colour import Color


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            description = re.sub(r"\n|\t", "", description)
            #ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            brand = record.get("brand", "")

            model = record.get("title", "")
            model = re.sub(re.compile(r"\b{}\b|New Model Set Of".format(brand),
                                      re.IGNORECASE), "", model)

            dimention_instances = re.findall(re.compile(
                r"\d+\s*[\'|\"]*\s*x\s*\d+\s*[\'|\"]*", re.IGNORECASE), model)
            if len(dimention_instances) > 1:
                model = re.sub(re.compile(
                    r"(\d+\s*[\'|\"|\’]*)\s*x\s*\d+\s*[\'|\"|\’]*", re.IGNORECASE), r"\1", model)
                words = re.findall(r"\d+\.*\d*\s*\'", model)
                model = re.sub(re.compile(r"\d+\.*\d*\s*\'\s*\,.*\'"),
                               ", ".join(sorted(set(words), key=words.index)), model)
            model = re.sub(re.compile(
                r"(\d+)\s*GPH", re.IGNORECASE), r"\1 GPH", model)
            model = re.sub(re.compile(r"\&", re.IGNORECASE), "and", model)
            model = re.sub(r"\(.*\)|\-\s*Type", "", model)
            model = re.sub(re.compile(r"(\d+) Pack of",
                                      re.IGNORECASE), r"\1-Pack", model)
            model = re.sub(re.compile(r"(\d+\.*\d*)\s*\-*(ft|feet)",
                                      re.IGNORECASE), r"\1'", model)
            model = re.sub(re.compile(
                r"\b(\d+\.*\d*)*\-*in\b|(\d+\.*\d*)\s*\-*inch\b", re.IGNORECASE), r'\1\2"', model)
            model = re.sub(re.compile(r"w/", re.IGNORECASE), "with", model)
            model = re.sub(r"(\d+)\s*(\d+\/\d+)", r"\1-\2", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(re.compile(r"\bfor\b", re.IGNORECASE), "for", model)
            model = re.sub(re.compile(r"\band\b", re.IGNORECASE), "and", model)
            def sub_s(match):
                data = match.groups()[0]
                return re.sub(r"(\d+)(\s|\,)", r"\1',", data)
            model = re.sub(r"(\d+\s*\'*\,*\s*.*and\s*\d+\s*\')", sub_s, model)
            model = re.sub(r"\,\s*and", "  and", model)
            model = re.sub(r"\s\'\s", "", model)

            model = " ".join(model.split())

            transformed_record = {

                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": record.get('category', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": description,
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": description,
                "price": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "product_type": record.get("product_type"),
                "model_sku": record.get("sku")

            }

            return transformed_record
        else:
            return None
