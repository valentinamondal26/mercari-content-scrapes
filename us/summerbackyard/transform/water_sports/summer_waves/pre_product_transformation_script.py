import re
import hashlib
from colour import Color


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            description = re.sub(r"\n|\t", "", description)
            #ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            brand = record.get("brand", "")

            model = record.get("title", "")
            model = re.sub(re.compile(r"\b{}\b|POOL PART ONLY\-*|By Polygroup".format(brand),
                                      re.IGNORECASE), "", model)
            model = re.sub(r"(\d+)\s*(\d+\/\d+)", r"\1-\2", model)
            model = re.sub(re.compile(r"(\d+\.*\d*)\s*\-*(ft|feet)",
                                      re.IGNORECASE), r"\1'", model)
            model = re.sub(re.compile(
                r"\b(\d+\.*\d*)\-*in\b|(\d+\.*\d*)\s*\-*inch\b", re.IGNORECASE), r'\1\2"', model)
            model = re.sub(re.compile(r"\&", re.IGNORECASE), "and", model)
            model = re.sub(
                r"([A-Z]+\s*\d+)\s[\/|and]*\s*([A-Z]+\s*\d+)", r"\1/\2", model)
            model = re.sub(re.compile(
                r"for\s*Intex\s*\,*\s*Summer\s*Escapes\s*And\s*Pools|Summer Escapes|Intex|for Summer Escapes Pools|for Summer Escapes or|\bColeman\b", re.IGNORECASE), "", model)
            model = re.sub(re.compile(
                r"2 Pack", re.IGNORECASE), "2-Pack", model)
            model = re.sub(re.compile(r"w/", re.IGNORECASE), "with", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])

            d = {
                'For': 'for',
                'Or': 'or',
                'And': 'and'
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'
            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)

            model = re.sub(re.compile(r"\bBy$", re.IGNORECASE), "", model)
            model = " ".join(model.split())
            model = re.sub(re.compile(r"18\' Solar Cover \(Fits Quick Set Ring Pools And Ring\)",re.IGNORECASE), "18' Solar Cover for Quick Set Pools and Ring", model)

            transformed_record = {

                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": record.get('category', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": description,
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "ner_query": description,
                "price": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "product_type": record.get("product_type"),
                "model_sku": record.get("sku")

            }

            return transformed_record
        else:
            return None
