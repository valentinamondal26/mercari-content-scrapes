'''
https://mercari.atlassian.net/browse/USCC-73 - Summer Waves Water Sports
https://mercari.atlassian.net/browse/USCC-1 - Intex Water Sports
'''

import scrapy
import datetime
import json
import random
import os
import re
from scrapy.exceptions import CloseSpider


class Summerbackyard(scrapy.Spider):
    """
        spider to crawl items in a provided category from summerbackyard.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Water sports

    brand : str
        category to be crawled, Supports
        1) Intex
        2) Summer Waves

    Command e.g:
    scrapy crawl summerbackyard -a category='Water sports' -a brand='Intex'
    scrapy crawl summerbackyard -a category='Water sports' -a brand='Summer Waves'

    """

    name = 'summerbackyard'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    source_url = None
    launch_url = None
    filters = None

    source = 'summerbackyard.com'
    blob_name = 'summerbackyard.txt'

    url_dict = {
        'Water sports': {
            'Intex': {
                'url': [
                    'https://summerbackyard.com/collections/intex-complete-pools',
                    'https://summerbackyard.com/collections/intex-pump-replacements-for-above-ground-pools',
                    'https://summerbackyard.com/collections/intex-pump-parts',
                    'https://summerbackyard.com/collections/intex-liners',
                    'https://summerbackyard.com/collections/intex-hoses',
                    'https://summerbackyard.com/collections/intex-pool-accessories',
                    'https://summerbackyard.com/collections/intex-pool-parts',
                ],
            },
            'Summer Waves': {
                'url': [
                    'https://summerbackyard.com/collections/summer-waves-pool-pumps',
                    'https://summerbackyard.com/collections/summer-waves-pump-motors',
                    'https://summerbackyard.com/collections/summer-waves-filters',
                    'https://summerbackyard.com/collections/summer-waves-hoses',
                    'https://summerbackyard.com/collections/summer-waves-accessories',
                    'https://summerbackyard.com/collections/summer-waves-other-parts',
                    'https://summerbackyard.com/collections/summer-waves-complete-pools',
                ],
            },
        }
    }

    base_url = 'https://summerbackyard.com'

    number_of_products = None


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(Summerbackyard, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, "").get('url', '')
        if not self.launch_url:
            self.logger.error(f'Spider is not supported for  category:{category} and brand:{brand}')
            raise(CloseSpider(f'Spider is not supported for  category:{category} and brand:{brand}'))
        self.logger.info(f'Crawling for  category: {category}, url: {str(self.launch_url)}')


    def contracts_mock_function(self):
        self.brand = 'Summer Waves'


    def start_requests(self):
        if self.launch_url and isinstance(self.launch_url, list):
            for url in self.launch_url:
                yield self.createRequest(url=url, callback=self.parse, dont_filter=True)
        elif self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse, dont_filter=True)


    def parse(self, response):
        """
        @url https://summerbackyard.com/collections/summer-waves-other-parts
        @meta {"use_proxy": "True"}
        @returns requests 1
        """

        all_links = response.xpath('//a[@class="grid-link"]/@href').getall()
        all_links = [self.base_url + link if "https" not in link else link for link in all_links]
        for link in all_links:
            yield self.createRequest(url=link, callback=self.parse_products, dont_filter=True, meta=response.meta)

        next_page_url = response.xpath('//ul[@class="pagination-custom"]//li//a[contains(@title,"Next")]/@href').get(default=None)
        if next_page_url:
            if "https" not in next_page_url:
                next_page_url = self.base_url+next_page_url
            yield self.createRequest(url=next_page_url, callback=self.parse, dont_filter=True)


    def parse_products(self, response):
        """
        @url https://summerbackyard.com/collections/summer-waves-other-parts/products/1-1-4-hose-fittings-kit-for-above-ground-pools
        @scrape_values id title image price product_type sku description breadcrumb
        @meta {"use_proxy":"True"}
        """

        item = {}
        item["crawl_date"] = str(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"] = str(response.status)
        item['id'] = response.url
        item['category'] = self.category
        item['brand'] = self.brand
        breadcrumb = response.xpath('//nav[@class="breadcrumb"]//a/text()').getall()+response.xpath('//nav[@class="breadcrumb"]//span//text()').getall()
        for crumb in breadcrumb:
            if re.compile(r"^\W$").match(crumb):
                breadcrumb.remove(crumb)
        item["breadcrumb"] = "|".join(breadcrumb)
        json_data = response.xpath("//script[contains(text(),'\"@type\": \"Product\"')]//text()").get(default="{}")
        json_data = json.loads(json_data)
        item["title"] = json_data.get("name", "")
        item["price"] = json_data.get("offers", {}).get("price", "")
        item["image"] = json_data.get("image", "")
        item["description"] = json_data.get("description", "")
        item["sku"] = json_data.get("sku", "")
        item["product_type"] = response.xpath('//div[a[contains(text(),"{}")]]//following-sibling::ul//li//a[contains(@class,"active")]/text()'.format(self.brand)).get(default="")

        meta = {}
        filter_name = response.meta.get('filter_name', '')
        if filter_name:
            meta = meta.fromkeys([filter_name], response.meta[filter_name])
            item.update(meta)

        yield item


    def createRequest(self, url, callback, meta=None, headers=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key, value in headers.items():
                request.headers[key] = value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request
