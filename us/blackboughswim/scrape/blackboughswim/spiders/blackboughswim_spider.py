'''
https://mercari.atlassian.net/browse/USCC-666 - Blackbough Women's Swimwear
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import time
import json
import re
from parsel import Selector

class BlackboughswimSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from blackboughswim.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Swimwear
    
    brand : str
        brand to be crawled, Supports
        1) Blackbough

    Command e.g:
    scrapy crawl blackboughswim -a category="Women's Swimwear" -a brand="Blackbough"
    """

    name = 'blackboughswim'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'blackboughswim.com'
    blob_name = 'blackboughswim.txt'

    # query_params = '?view=ajax&limit=20&page={page}&sort_by=manual&_={timestamp}'
    query_params = '?view=ajax&limit=20&page={page}&sort_by=manual'
    url_dict = {
        "Women's Swimwear": {
            "Blackbough": {
                'url': 'https://blackboughswim.com/collections/all/filter_bikinis?sort=title-ascending',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BlackboughswimSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                url = url.split('?')[0] + self.query_params.format(page=1, timestamp=self.get_currenttimestamp())
                yield self.create_request(url=url, callback=self.parse)


    def parse(self, response):
        """
        @url https://blackboughswim.com/collections/all/filter_bikinis?view=ajax&limit=20&page=1&sort_by=manual&_=1620671666349
        @meta {"use_proxy":"True"}
        @scrape_values id title description image price details sizes
        @returns requests 1
        """

        res = json.loads(response.text)
        products = res.get('products', [])
        for product in products:

            description_selector = Selector(product.get('description', '') or '')
            image = product.get('featured_image', '')
            if not image.startswith('https:'):
                image = 'https:' + image

            yield {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': self.brand,
                'id': 'https://blackboughswim.com/products/' + product.get('handle', ''),

                'title': product.get('title', ''),
                'price': product.get('price', ''),
                'description': Selector(description_selector.xpath('//div[@class="container"]/div[@class="one-half columns"]').get(default='')).xpath('//p/text()').getall(),
                'details': description_selector.xpath('//div[@class="container"]/div[@class="one-half columns"]/ul/li/text()').getall(),
                'sizes': list(map(lambda x: x.get('title', ''), product.get('variants', []))),
                'image': image,
                'response_url': response.url,
            }

        current_page = 0
        match = re.findall(r'page=(\d+)', response.url)
        if match:
            current_page = int(match[0])
        next_page_link = response.url.split('?')[0] + self.query_params.format(page=current_page + 1, timestamp=self.get_currenttimestamp())
        if products and current_page and next_page_link:
            yield self.create_request(url=next_page_link, callback=self.parse)


    def get_currenttimestamp(self):
        return int(time.time()*1000)

    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
