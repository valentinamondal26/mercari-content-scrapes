import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ' '.join(record.get('description')).replace('\n', '')
            ner_query = description + ' ' + ' '.join(record.get('details', [])).replace('\n', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = str(round(int(price)/100))

            title = record.get('title', '')
            model = title.split('/')[0].strip()
            if not re.findall(r'\bBikini\b', model, flags=re.IGNORECASE):
                model = re.sub(r'(\bTop\b|\bBottoms\b)', r'Bikini \1', model, flags=re.IGNORECASE)

            color = title.split('/')[-1].strip()

            product_type = ''
            if re.findall(r'\bTop\b', model, flags=re.IGNORECASE):
                product_type = 'Top'
            elif re.findall(r'\bBottoms\b', model, flags=re.IGNORECASE):
                product_type = 'Bottom'

            material = ''
            for detail in record.get('details', []):
                match = re.findall(r'\d+%\s*(.*?)[/\n]', detail+'\n')
                if match:
                    material = '/'.join(list(map(lambda x: x.strip().title(), match)))
                    break

            if not material:
                match = re.findall(r'\d+%\s*(\w+)', description)
                if match:
                    material = '/'.join(list(map(lambda x: x.strip().title(), match)))


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": title,
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                'size': ', '.join(record.get('sizes', [])),
                'product_type': product_type,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
