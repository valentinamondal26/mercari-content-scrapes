'''
https://mercari.atlassian.net/browse/USCC-767 - Kanu Surf Men's Board Shorts
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import copy


class KanusurfSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from kanusurf.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Board Shorts
    
    brand : str
        brand to be crawled, Supports
        1) Kanu Surf

    Command e.g:
    scrapy crawl kanusurf -a category="Men's Board Shorts" -a brand="Kanu Surf"
    """

    name = 'kanusurf'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'kanusurf.com'
    blob_name = 'kanusurf.txt'

    base_url = 'http://www.kanusurf.com'

    url_dict = {
        "Men's Board Shorts": {
            "Kanu Surf": {
                'url': [
                    'http://www.kanusurf.com/product_center.asp?dept_id=1029',
                    'http://www.kanusurf.com/product_center.asp?dept_id=1025',
                    'http://www.kanusurf.com/product_center.asp?dept_id=1073',
                ]
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(KanusurfSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url http://www.kanusurf.com/product_detail.asp?product_id=3163&dept_id=1029
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title sku price description color sizes
        """

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//div[@class="breadcrumbs"]//text()[normalize-space(.)]').getall(),
            'image': response.xpath('//div[@id="main-image"]//img/@src').get(),
            'title': response.xpath('//h3[@class="blue"]/text()').get(),
            'sku': response.xpath('//form[@name="add-to-cart"]/div/text()').get(),
            'price': response.xpath('//form[@name="add-to-cart"]/span[@class="blue" and @style="font-weight:bold;"]/text()').get(),
            'description': response.xpath('//div[@class="description"]/ul/li/span/text()[normalize-space(.)]').getall(),
            'color': response.xpath('//span[@id="selected_color_label"]/text()').get(),
            'sizes': response.xpath('//div[@class="variants"]//select[@id="variant_dropdown_size"]/option').xpath('normalize-space(./text())').getall(),
        }

        variants = response.xpath('//div[@class="variants"]//select[@id="variant_dropdown_color"]/option')

        if variants:
            for variant_info in variants:
                variant_id = variant_info.xpath('./@value').get()
                color = variant_info.xpath('normalize-space(./text())').get().strip()
                variant = copy.deepcopy(item)
                variant.update({
                    'id': response.url + f'&color_id={variant_id}',
                    'color': color,
                    'image': response.xpath(f'//div[@title="{color}"]/span/text()').get(),
                })
                yield variant
        else:
            yield item


    def parse(self, response):
        """
        @url http://www.kanusurf.com/product_center.asp?dept_id=1029
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="product"]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + '/' + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
