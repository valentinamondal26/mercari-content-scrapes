import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = '. '.join(record.get('description', ''))
            title = record.get('title', '')
            ner_query = description or title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = title
            model = re.sub(r'Men\'s|Swim Trunks|Shorts|Boardshorts|Board Shorts', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()

            product_type = ''
            if re.findall(r'Trunks|Elastic Shorts', title, flags=re.IGNORECASE):
                product_type = 'Swim Trunks'
            elif re.findall(r'Board Shorts|Boardshorts', title, flags=re.IGNORECASE):
                product_type = 'Boardshorts'

            material = ''
            match = re.findall(r'\d+%\s(\w+)', description)
            if match:
                material = '/'.join(list(dict.fromkeys(list(map(lambda x: x.strip(), match)))))
                material = material.title()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                'product_type': product_type,
                'model_sku': record.get('sku', ''),
                'material': material,
                'color': record.get('color', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
