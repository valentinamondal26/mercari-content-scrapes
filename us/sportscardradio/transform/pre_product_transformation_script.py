import re
import hashlib


class Mapper:
    
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '')
            model = record.get('title', '')

            model = re.sub(r'(#\w+)(.*)(#/\w+)', r'\2 \1\3', model)
            model = re.sub(r'\#\/', '-', model)
            if not re.findall(r'(#\w+)(.*)(#/\w+)', title, flags=re.IGNORECASE):
                model = re.sub(r'(^#[A-Z]+[0-9\-*[A-Z]+|^#[0-9]+[0-9\-*[A-Z]+|^#[0-9])(.*)(-\s+.*)*', r'\2 \1 \3', model)
            model = re.sub(r'\s+\-\s+|^\-|\-$|–', ' ', model)
            model = re.sub(r'\bVrs\b', 'vs', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            sports_name = record.get('sports_name', '')
            sports_name = re.sub(r'^\bTopps UFC\b$', 'UFC', sports_name, flags=re.IGNORECASE)

            player_name = re.sub(r'^[A-Z]+[0-9\-[A-Z]+|[0-9]+[0-9\-[A-Z]+|^\#\w+|^\#\w+\-\w+', '', title).strip() ##remove alphanumeric string in title 
            player_name = re.sub(r'\b^\d+\b', ' ', player_name)
            player_name = re.sub(r'-\s+(.*)$|–\s+(.*)$', ' ', player_name).strip()
            player_name = re.sub(r'^-|\#|\/', ' ', player_name).strip()
            player_name = re.sub(r'\s+', ' ', player_name).strip()
       
            match = re.findall(r'http://www\.sportscardradio\.com/(\d{4})\-', record['id'])
            year = ''
            if match:
                year = match[0]
       
            product_line = record.get('complete_set_name', '')
            if year:
                product_line = re.sub(r'^0\d\b', year, product_line)

            if re.findall(r'Topps UFC', product_line, flags=re.IGNORECASE):
                sports = 'UFC'
            
            if re.findall(r'#CL\d+ Checklist \d+|#\d*\/\d+', title, flags=re.IGNORECASE):
                return None

            transformed_record = {
                'id': record['id'], 
                'crawl_date': record.get('crawl_date', ''),
                'category': record.get('category', ''),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'model': model,

                'price': '',
                'product_line': product_line,
                'sport': sports,
                'player': player_name,
            }
            return transformed_record
        else:
            return None
