'''
https://mercari.atlassian.net/browse/USCC-640 - Sports Trading Cards & Accessories & Topps - UFC cards
'''

import scrapy
import datetime
from datetime import datetime

import random
import os
import re
from scrapy.exceptions import CloseSpider


class SportdcardradioSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and brand from sportscardio.com

    Attributes

    category : str
        category to be crawled, Supports
        1) Sports Trading Cards & Accessories

    brand : str
        brand to be crawled, Supports
        1) Topps

    Command e.g:
    scrapy crawl sportscardradio  -a category='Sports Trading Cards & Accessories' -a brand='Topps'

    """
    name = 'sportscardradio'

    source = 'sportscardradio.com'
    blob_name = 'sportscardradio.txt'

    category = None
    brand = None

    launch_url = None
    filters = None

    base_url='https://www.sportscardradio.com/'

    url_dict = {
        'Sports Trading Cards & Accessories':
            {
                'Topps': {
                    'url': [
                        'http://www.sportscardradio.com/2009-topps-ufc-round-2-hobby-box-checklist/',
                        'http://www.sportscardradio.com/2009-topps-ufc-round-one-1-hobby-box-checklist/'
                    ],
                }
            }
        }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com/',
        'http://shp-mercari-us-d00002.tp-ns.com/',
        'http://shp-mercari-us-d00003.tp-ns.com/',
        'http://shp-mercari-us-d00004.tp-ns.com/',
        'http://shp-mercari-us-d00005.tp-ns.com/',
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SportdcardradioSpider, self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        for url in self.launch_url:
            yield self.create_request(url=url, callback=self.get_listing_page_items)


    def get_listing_page_items(self, response):
        """
        @url http://www.sportscardradio.com/2009-topps-ufc-round-2-hobby-box-checklist/
        @meta {"use_proxy": "True"}
        @scrape_values  title parallel_cards set_name complete_set_name
        """

        def get_parallel_cards_value(data):
            values = []
            if re.findall(r'\bParallel Cards\b', ' '.join(data), flags=re.IGNORECASE):
                extract_values_flag = False
                for value in data:
                    if re.findall(r'\bParallel Cards\b', value, flags=re.IGNORECASE):
                        extract_values_flag = True
                    if extract_values_flag and not re.findall(r'^\#', value.strip('\n')):
                        values.append(value)
                    elif re.findall(r'^#', value.strip()):
                        break
            return values

        def get_cards(cards_data):
            titles = []
            for data in cards_data:
                if re.findall(r'^#', data.strip()):
                    titles.append(data.strip())
                    print(data.strip())
            if not titles:
                return cards_data
            return titles

        all_model_texts = response.xpath('//div[@class="awr "]//p//text()').getall()
        set_data = response.xpath("//em")
        set_names = []
        complete_set_names = []
        for res in set_data:
            set_name = res.xpath("./../span/strong/span/text()").get(default='') or res.xpath("./../strong/text()").get(default='')
            base_set_type = res.xpath(".//text()").get(default='')
            complete_set_names.append(f'{base_set_type} {set_name}')
            set_names.append(set_name)

        set_data_dict = {}
        for i in range (0, len(set_names)):
            set_first_item = all_model_texts.index(set_names[i])
            if i==len(set_names)-1:
                set_item_values = all_model_texts[set_first_item+1: ]
            else:
                next_set_item = all_model_texts.index(set_names[i+1])
                set_item_values =  all_model_texts[set_first_item+1: next_set_item]
            cards  = get_cards(set_item_values)
            parallel_cards = get_parallel_cards_value(set_item_values)
            set_data_dict.update({set_names[i]: {}})
            set_data_dict[set_names[i]]['cards'] = cards
            set_data_dict[set_names[i]]['parallel_cards'] = parallel_cards

        for set_name, complete_set_name in zip(set_names, complete_set_names):
            set_data = set_data_dict[set_name]['cards']
            for title in set_data:
                title = title.strip()
                parallel_cards = set_data_dict[set_name]['parallel_cards']
                yield {
                    'id': f'{response.url}#{title}',
                    'status_code': str(response.status),
                    'crawl_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'category': self.category,
                    'brand': self.brand,

                    'title': title,
                    'set_name': set_name,
                    'parallel_cards': parallel_cards,
                    'complete_set_name': complete_set_name
                }


    def create_request(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta, headers=headers)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({'use_cache': True})
        return request
