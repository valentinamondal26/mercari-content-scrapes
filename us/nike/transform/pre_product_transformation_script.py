import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "description": description,
                "title": title,
                "model": title,
                "color": record['color'],
                "rating": record['rating'],
                "style": record['style'],
                "product_details": product_details,
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": str(price)
                }
            }

            return transformed_record
        else:
            return None
