import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = re.sub(re.compile(r'nike', re.IGNORECASE), '', title)

            material = ''
            match = re.findall(re.compile(
                r'leather|Synthetic leather|Leather', re.IGNORECASE), product_details)
            if match:
                material = match[0]
            else:
                match = re.findall(re.compile(
                    r'leather|Synthetic leather|Leather', re.IGNORECASE), model)
                if match:
                    material = match[0]

            model = re.sub(re.compile(r'\s+'), ' ', model).strip()
            model = re.sub(r"\"", "", model)
            model = re.sub(re.compile(
                r"leather|Synthetic leather|Leather", re.IGNORECASE), "", model)
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = " ".join(model.split())
            sub_brand = ''
            match = re.findall(re.compile(r'Air Jordan|Jordan'), title)
            if match:
                sub_brand = match[0]

            model_sku = record.get('style', '')

            if re.findall(re.compile(r'Gift Card', re.IGNORECASE), model) or \
                    re.findall(re.compile(r'Gift\s*Card', re.IGNORECASE), model_sku):
                return None
            if model == "1" or model == "11":
                return None
            if re.findall(re.compile(r"converse", re.IGNORECASE), title) != []:
                return None
            if model == "1" or model == "11":
                return None

            color = record.get("color", '')
            color = '/'.join(list(dict.fromkeys(color.split('/'))))
            color = "/".join(sorted(set(color.split("/")), key=color.index))
            color = re.sub(re.compile(r"Multi\-color",
                                      re.IGNORECASE), "Multicolor", color)
            if len(color.split("/")) > 3:
                color = color.split("/")[0]+"/Multicolor"
            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "rating": record['rating'],
                "ner_query": ner_query,
                "product_details": product_details,

                "title": title,
                "model": model,
                "color": color,
                "product_type": record.get('Shoes', '') or sub_brand,
                "model_sku": model_sku,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "technology": record.get('Technology', ''),
                "shoe_height": record.get('Shoe Height', ''),
                "shoe_width": record.get('Width', ''),
                "recommended_surfrace": record.get('Surface', ''),
                "closure_type": record.get('Closure Type', ''),
                "description": description + product_details,
                "athlete": record.get('Athletes', ''),
                "age": record.get('Kids Age', ''),
                "product_line": record.get('Brand', ''),
                "product_family": record.get('Icon', ''),
                "material": record.get('Material', '') or material.title(),
                "features": record.get('Features', ''),
            }

            return transformed_record
        else:
            return None
