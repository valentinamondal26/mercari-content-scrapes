import hashlib
import re
import itertools
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title','')

            color = record.get('color','')
            if not color:
                color = record.get('Color','')
            colors = list(dict.fromkeys(color.split('/')))
            if len(colors) > 2:
                primary_color = None
                for c in colors:
                    try:
                        Color(c)
                        primary_color = c
                        break
                    except:
                        pass
                colors = [primary_color if primary_color else colors[0], 'Multicolor']
            color = '/'.join(colors)

            sub_title = record.get('subtitle','')
            sub_title = re.sub(re.compile(r"Women's|\(Plus Size\)",re.IGNORECASE),'',sub_title)
            model = title +' '+ sub_title
            model = re.sub(r'\(M\)|\(|\)|1-Piece Pad|Color-Block', '', model, flags=re.IGNORECASE)
            model = re.sub(r'Sports Bra\s+Maternity', 'Maternity Sports Bra', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+',' ',model).strip()

            transformed_record = {

                'id': record.get('id',''),
                'item_id': hex_dig,

                'title': record.get('title',''),
                'model': model,
                'brand': record.get('brand',''),
                'category': record.get('category',''),
                'image': record.get('image',''),

                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statuscode',''),

                'support_type': record.get('subtitle',''),
                'color': color,
                'description': record.get('description',''),
                'model_sku': record.get('style',''),
                'bra_cup_type': record.get('Cup Type',''),
                'back_style': record.get('Back Type',''),
                'closure_type': record.get('Closure Type',''),
                'neckline': record.get('Neck Style',''),

                'price':{
                    'currency_code':'USD',
                    'amount':record.get('price','').replace('$','')
                },
            }
            return transformed_record
        else:
            return None
