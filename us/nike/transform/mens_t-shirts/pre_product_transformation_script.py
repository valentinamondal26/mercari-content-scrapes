import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            color = record.get('color', '')
            athlete = record.get('Athletes', '')

            model = re.sub(re.compile(r'\(|\)|\"|' +
                                      athlete, re.IGNORECASE), '', title)
            if re.findall(re.compile(r"City Edition", re.IGNORECASE), model):
                model = re.sub(r"\–", "", model)
            model = re.sub('Sportswear Club Fleece x Nike LA Editions', 'x LA Editions Sportswear Club Fleece', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(\bNike Dri-FIT\b|\bNike\b)(.*)',r'\2 \1 \3',model) 
            if not re.findall(r'^Nike',model,flags=re.IGNORECASE):
                model = f'Nike {model}' 
            model = re.sub(r'®', '', model)
            model = re.sub(re.compile(r'\s+'), ' ', model).strip()
            model = ' '.join([mod[0].upper()+mod[1:] for mod in model.split()]) ###titlecase the model and retain source capitalisation
            
            material = ''
            match = re.findall(re.compile(
                r'Fabric: Body:(.*?\.)', re.IGNORECASE), product_details)
            if match:
                material = match[0].strip('.').strip()

            if not material:
                match = re.findall(re.compile(
                    r'100% recycled polyester|100% polyester', re.IGNORECASE), product_details)
                if match:
                    material = match[0]

            color = "/".join(sorted(set(color.split("/")), key = color.split("/").index))
            if len(color.split("/"))>2:
                color = color.split("/")[0]+"/Multicolor"

            if not color \
                    or re.findall(r'gift|hurley|^Nike$|uni\s*\-*sex', title, flags=re.IGNORECASE) \
                    or re.findall(r'uni\s*\-*sex', record.get('id'), flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "image": record.get('image', ''),
                "rating": record.get('rating', ''),
                "ner_query": ner_query,
                "product_details": product_details,
                "description": description + product_details,
                "title": title,

                "model": model,
                "color": color,
                "model_sku": record.get('style', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('Brand', ''),
                "activity": record.get('Sports', ''),
                "best_use": record.get('Best For', ''),
                "technology": record.get('Technology', ''),
                "subtype": record.get('Collaborator', ''),
                "sleeve_length": record.get('Sleeve Length', ''),
                "closure_type": record.get('Closure Type', ''),
                "fit": record.get('Fit', ''),
                "athlete": athlete,
                "features": record.get('Features', ''),
                "material": material,
            }

            return transformed_record
        else:
            return None
