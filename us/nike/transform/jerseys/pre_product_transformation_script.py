import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            color = record.get('color', '')
            athlete = record.get('Athletes', '')

            model = re.sub(re.compile(athlete, re.IGNORECASE), '', title)
            model = re.sub(re.compile('Bagley Iii', re.IGNORECASE), 'Bagley III', model)
            model = re.sub(re.compile('–', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\s+'), ' ', model).strip()

            material = ''
            match = re.findall(re.compile(r'Fabric: Body:(.*?\.)', re.IGNORECASE), product_details)
            if match:
                material = match[0].strip('.').strip()

            if not material:
                match = re.findall(re.compile(r'100% recycled polyester|100% polyester', re.IGNORECASE), product_details)
                if match:
                    material = match[0]

            material = material.lower()

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "rating": record['rating'],
                "ner_query": ner_query,
                "product_details": product_details,
                "title": title,

                "model": model,
                "color": color,
                "model_sku": record.get('style', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('Brand', ''),
                "sleeve_length": record.get('Sleeve Length', ''),
                "fit": record.get('Fit', ''),
                "athlete": athlete,
                "jersey_size": record.get('Size', ''),
                "material": material,
                "description": description + product_details,
            }

            return transformed_record
        else:
            return None
