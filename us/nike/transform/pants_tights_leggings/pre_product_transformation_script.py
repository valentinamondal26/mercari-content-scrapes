import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')

            product_details = record.get('product_details', '')

            product_line = record.get('subtitle', '')

            brand = record.get('brand', '')

            title = record['title']
            model = f'{title} {product_line}'
            keywords = [
                "Women's", "Women’s", '™', '®',
                # brand,
            ]
            model = re.sub(r'\b'+brand+r'\b', '', model, re.IGNORECASE)
            model = re.sub(re.compile(r'|'.join(keywords), re.IGNORECASE), '', model)
            model = re.sub(r'\(.*\)', '', model)
            model = re.sub(r'^\s*x\s', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            color_filter = record.get('Color', '')
            if isinstance(color_filter, list):
                color_filter = ', '.join(color_filter)
            color_filter = re.sub(re.compile(r'Multi-color', re.IGNORECASE), 'Multicolor', color_filter)

            color = record.get('color', '')
            color = '/'.join(list(dict.fromkeys(color.split('/'))))
            if len(color.split('/')) > 3:
                color = color.split('/', 1)[0] + '/Multicolor'
            color = re.sub(r'\s+', ' ', color).strip()

            if re.findall(re.compile(r'gift card|Underwear', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": brand,

                "title": title,
                "image": record.get('image', ''),
                "description": description,
                "ner_query": description + ' ' + product_details + ' ' + title,
                "rating": record.get('rating', ''),
                "product_details": product_details,

                "product_line": product_line,
                "model": model,
                "color": color_filter or color,
                "style": record.get('style', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                }
            }

            return transformed_record
        else:
            return None
