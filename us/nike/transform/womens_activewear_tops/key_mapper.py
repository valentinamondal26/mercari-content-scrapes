class Mapper:
    def map(self, record):
        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity.get("attribute", []):
                    model = entity.get("attribute", [{}])[0].get("id", '')
            elif "color" == entity['name']:
                if entity.get("attribute", []):
                    color = entity.get("attribute", [{}])[0].get("id", '')

        key_field = model + color
        return key_field
