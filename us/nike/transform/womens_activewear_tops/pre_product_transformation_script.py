import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            color = record.get('color', '')
            color = '/'.join(list(dict.fromkeys(color.split('/'))))
            colors = color.split('/')
            colors = [c for c in colors if not len(re.findall(
                re.compile(r'\b'+c+r'\b', re.IGNORECASE), color)) > 1]
            if len(colors) > 1:
                colors = [colors[0], 'Multicolor']
            color = '/'.join(colors)

            # model = re.sub(re.compile(r'\bNike x\b|\bNike\b|\(|\)|®', re.IGNORECASE), '', title)
            model = re.sub(re.compile(r'\(|\)|®', re.IGNORECASE), '', title)
            model = re.sub(r'(.*)(\bNike Dri-FIT\b|\bNike\b)(.*)', r'\2 \1 \3', model, flags=re.IGNORECASE)
            if not re.findall(r'^Nike', model, flags=re.IGNORECASE):
                model = f'Nike {model}'
            model = re.sub(re.compile(r'\s+'), ' ', model).strip()

            if not model \
                or re.findall(re.compile(r"^Nike$|\bDigital\b|\bGift Card\b", re.IGNORECASE), model):
                return None

            # ignore the overlapping items, prioritze to Men's T-Shirts
            # refer https://mercari.atlassian.net/browse/USCC-233 for more information
            if hex_dig in [
                    '4afe0516f438a9d2dbd2176b2faf077061359726', '69e4c1f666b6388a87d46fa70a2461cdb269c58d',
                    '0f15801859437df83ece9048613d17dc8cfbb760', '8ae70dd5b46eafd434cba3eddd7e944d9d65f036',
                    '15a44fb323cf61ec59157b99e635b55ad104f11e', '7f75cb12591ee08b88f8714c309ca54cfb3b187f',
                    '58bd0b1bd8090b21e826a8b3ef4630842656bd80', '596b4012cf73b8d7ebf63a836ffc24cd610b61dc',
                    'b8f9e1c4763b150c9a6c2c42832bc78fdda4fe09', '865af745386e8ab8ef4ddd10d405aed6446f1b0a',
                    'b6394291931d1eb3b76f5c73eeb28a9946b30b9e', '2244c9a4d4bf960c823b5d3e1b397a23fcb16f71',
                    'dc8766c339a0fbc622bbaafc96c49a6524a5acbb', 'ddae467fd4383f6437f320e0f47abb5c48256c2a',
                    '78667316f10afd382cc5c2d7fbc880c149077bc1', '7627e4d2ad35e5da7a87dc4467445df1a7d05ae5',
                    'c09da3b2a86db30713ba95cbccb659abe6af3f9e', '065c0f4c5d82f5e95a7662c0a3aa0d40d7318070',
                    '0a3af709be293e5df6c3570fb765671ac4894503', '2a50c33b295091ed39ee44e8f096f462134a4ab5',
                    '467b754d1653fad45a9ca0b8cb9e2b249e6978a8', '596c51ad47faa18ea92bc40885a16ad327db9019',
                    'd33a22a4c0e0fdce27ef8d6e447bbb66de570a9e', '471b698ea7704086eb8b54bce04227a66e804835',
                ]:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "brand": record.get('brand', ''),

                "image": record.get('image', ''),
                "rating": record.get('rating', ''),
                "ner_query": ner_query,
                "product_details": product_details,
                "title": title,

                "model": model,
                "color": color,
                "model_sku": record.get('style', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('Brand', ''),
                "technology": record.get('Technology', ''),
                "fitting_type": record.get('Fit', ''),
                "series": record.get('Collaborator', ''),
                "activity": record.get('Sports', ''),
                "bottoms_size_womens": ', '.join(record.get('sizes', '')),
                "material": record.get('Material', ''),
                "best_use": record.get('Best For', ''),
                "sleeve_length": record.get('Sleeve Length', ''),
                "closure_type": record.get('Closure Type', ''),
                "features": record.get('Features', ''),
                "description": description + product_details,
            }

            return transformed_record
        else:
            return None
