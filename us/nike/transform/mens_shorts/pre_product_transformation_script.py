import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            size_fit_description = record.get('size_fit_description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            color = record.get('color', '')
            color = re.sub('Multi-Color', 'Multicolor', color, flags=re.IGNORECASE)
            colors = list(dict.fromkeys(color.split('/')))
            if len(colors) > 2:
                colors = [colors[1] if colors[0].lower() == 'multicolor' else colors[0], 'Multicolor']
            color = '/'.join(colors)

            if not color:
                if hex_dig == 'b7507cf5656312f6019f6ffefbd891e049b2357a':
                    color = 'Dark Blue/Multicolor'
                elif hex_dig == '69ea43ad8a2d7502cf5593133d0255ec9f0a0ddd':
                    color = 'Dark Blue'

            model = re.sub(r'™', '', title)
            model = re.sub(r'(.*)(\bNike\b)(.*)', r'\2 \1 \3', model, flags=re.IGNORECASE)
            if not re.findall(r'\bShorts\b', model):
                model = f'{model} Shorts'
            if not re.findall(r'\bNike\b', model):
                model = f'Nike {model}'
            model = re.sub(r'\(|\)', '', model)
            model = re.sub(re.compile(r'\s+'), ' ', model).strip()

            material = ''
            match = re.findall(re.compile(r'Fabric: Body:(.*?\.)', re.IGNORECASE), product_details)
            if match:
                material = match[0].strip('.').strip()

            if not material:
                match = re.findall(re.compile(r'100% recycled polyester|100% polyester', re.IGNORECASE), product_details)
                if match:
                    material = match[0]
            material = re.sub(r'\s*\d+%\s*', '', material).strip()
            material = material.title()

            fit = record.get('Fit', '')
            if not fit:
                match = re.findall(r'(\w+)\xa0*\s*fit', size_fit_description, flags=re.IGNORECASE)
                if match:
                    fit = match[0].strip()
            if not fit:
                fit = 'Standard'

            model_sku = record.get('style', '')

            if re.findall(re.compile(r'Gift\s*Card', re.IGNORECASE), model) or \
                re.findall(re.compile(r'Gift\s*Card', re.IGNORECASE), model_sku):
                return None

            # ignore the overlapping items
            if hex_dig in [
                'dff04bd19e0140160421a309018abbdce98ae8f9', '5d108ffaf634e13006eb713f2fa59e6d8ce5e320',
                '9784e3800b6f76bd63eaa91285677d809b288d53', '204f3c7eb5025f8b84f39e1ebbcff8ac6a22e862',
                'd88fbacac892988487edb9229452d9639771c96d', '8f9b8eabc84b2902617d4caf2ce3e4f379c960b7',
                '98c191280f577ad5f0c9b7bd9b1cbba8af75aace', '4914edd8a508a25152cf86a4ee777885b86d6d98',
                '5eb74ecfb29a1fdba07de3701b70737c658ccd2a', '7cc6bf41e5f4c1d39c3cfe622fa434a35461ca05',
                '17d516bc1bd3e9eb488ccd4607ebb3da8a0fc3ef', 'ce585f70489cdfcf0dfa10e8b8923f9ac20d6a5f',
                '97e49e12031d0ee707d7edbac9f7aca655684a40', '93817d1f3d69bb0406099361af5be924921d5d6d',
                'f4fbd53d45f74feaff29a1dbb095e3732b68b9ff', 'f2c836042d3b14943a2213eeb5096fd450cd8feb',
                '4911f9769dca5cdb0672cc6f2490eae6c9062f81', '16746ac9d2243972e4664618b4e47789fc2ae2b1',
                '64ee3fb0846a820ec42b189e4955d492870ed532', 'd3764d28ef009aab94cb01ff54e7262f356dca1b',
                'b4ad2b423243f17ff7306ca63f1026bdf077e87f', '25bea7fd88755787755a4aeaeb9e34fb885d8d01',
                ]:
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],

                "image": record['image'],
                "rating": record['rating'],
                "ner_query": ner_query,
                "product_details": product_details,
                "title": title,

                "model": model,
                "color": color,
                "model_sku": model_sku,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('Brand', ''),
                "type_of_sport": record.get('Sports & Activities', ''),
                "best_use": record.get('Best For', ''),
                "fit": fit,
                "material": material,
                "description": description + product_details,

                # Note: As the site updated, series and athlete information
                # is no more availble on the source
                "series": record.get('Collaborator', ''),
                "athlete": record.get('Athletes', ''),
            }

            return transformed_record
        else:
            return None
