import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            size_fit_description = record.get('size_fit_description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + size_fit_description + ' ' + title
            ner_query = ner_query.replace('\r', '').replace('\n', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            color = record.get('color', '')
            colors = list(dict.fromkeys(color.split('/')))
            if len(colors) > 2:
                colors = [colors[0], 'Multicolor']
            color = '/'.join(colors)
            sizes = record.get('sizes', [])

            model = re.sub(r'™', '', title)
            model = re.sub(r'(.*)(\bNike\b)(.*)', r'\2 \1 \3', model, flags=re.IGNORECASE)
            if not re.findall(r'\bShorts\b', model):
                model = f'{model} Shorts'
            if not re.findall(r'\bNike\b|\bNikeCourt\b', model):
                model = f'Nike {model}'
            model = re.sub(r'\(|\)', '', model)
            model = re.sub(re.compile(r'\s+'), ' ', model).strip()

            material = ''
            match = re.findall(re.compile(r'Fabric: Body:(.*?\.)', re.IGNORECASE), product_details)
            if match:
                material = match[0].strip('.').strip()

            if not material:
                match = re.findall(re.compile(r'100% recycled polyester|100% polyester', re.IGNORECASE), product_details)
                if match:
                    material = match[0]
            material = re.sub(r'\s*\d+%\s*', '', material).strip()
            material = material.title()

            inseam = ''
            match = re.findall(re.compile(r'Inseam length:\s+(.*?in/)', re.IGNORECASE), size_fit_description)
            if match:
                inseam = match[0].replace('in/', '"').strip()

            if not inseam:
                match = re.findall(re.compile(r'(\d+")\s+Inseam', re.IGNORECASE), product_details)
                if match:
                    inseam = match[0].strip()

            model_sku = record.get('style', '')

            fit = record.get('Fit', '')
            if not fit:
                match = re.findall(r'(\w+)\xa0*\s*fit', size_fit_description, flags=re.IGNORECASE)
                if match:
                    fit = match[0].strip()
            if not fit:
                fit = 'Standard'

            if re.findall(re.compile(r'Gift\s*Card', re.IGNORECASE), model) or \
                re.findall(re.compile(r'Gift\s*Card', re.IGNORECASE), model_sku) or \
                re.findall(re.compile(r'Hurley', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],

                "category": record['category'],
                "brand": record['brand'],

                "image": record['image'],
                "rating": record['rating'],
                "ner_query": ner_query,
                "product_details": product_details,
                "title": title,

                "model": model,
                "color": color,
                "model_sku": model_sku,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_line": record.get('Brand', ''),
                "technology": record.get('Technology', ''),
                "fit": fit,
                "activity": record.get('Sports & Activities', ''),
                "material": material,
                "inseam": inseam,
                "description": description + product_details,
            }

            return transformed_record
        else:
            return None
