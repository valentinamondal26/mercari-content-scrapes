class Mapper:
    def map(self, record):
        key_field = ""
        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity.get("attribute", []):
                    model = entity.get("attribute", [{}])[0].get("id", '')
            if "color" == entity['name']:
                if entity.get("attribute", []):
                    color = entity.get("attribute", [{}])[0].get("id", '')

        key_field = color + model
        return key_field
