import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            price = record.get('price', '')
            price = price.replace('$', '').strip()

            description = record.get('description', '')
            product_details = record.get('product_details', '')
            title = record['title']
            ner_query = description + ' ' + product_details + ' ' + title
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            color = record.get('color', '')
            sizes = record.get('sizes', [])

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            model = re.sub(re.compile(r'Air Jordan|Jordan|', re.IGNORECASE), '', title)
            model = re.sub(re.compile(color, re.IGNORECASE), '', model)
            model = re.sub(re.compile(pattern_words(sizes), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\s+'), ' ', model).strip()


            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "rating": record['rating'],
                "ner_query": ner_query,
                "product_details": product_details,
                "description": description + product_details,
                "title": title,

                "model": model,
                "color": color,
                "model_sku": record.get('style', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "technology": record.get('Technology', ''),
                "shoe_height": record.get('Shoe Height', ''),
                "shoe_width": record.get('Width', ''),
                "closure_type": record.get('Closure Type', ''),
                "use": record.get('Surface', ''),
                "product_line": record.get('Shoes', ''),
                "us_shoe_size_mens": ','.join(sizes),
                "features": record.get('Features', ''),
                "series": record.get('Collaborator', ''),
            }

            return transformed_record
        else:
            return None
