'''
https://mercari.atlassian.net/browse/USDE-1280 - Fix: Nike Pants, tights, leggings
https://mercari.atlassian.net/browse/USDE-437 - Nike Pants, tights, leggings
https://mercari.atlassian.net/browse/USDE-1681 - Nike Women's Sports Bras
https://mercari.atlassian.net/browse/USCC-227 - Nike Women's Exercise Shorts
https://mercari.atlassian.net/browse/USCC-297 - Nike Men's Shorts
https://mercari.atlassian.net/browse/USCC-278 - Nike Jerseys
https://mercari.atlassian.net/browse/USDE-658 - (dropped) Nike Men's Athletic Shoes
https://mercari.atlassian.net/browse/USCC-76 - Nike Men's T-Shirts
https://mercari.atlassian.net/browse/USCC-233 - Nike Women's Activewear Tops
'''

import scrapy
import json
import os
from datetime import datetime
from scrapy.exceptions import CloseSpider
import random
import re
import uuid
from parsel import Selector

class NikeSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from nike.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Womens Sneakers
        2) Womens Athletic Shoes
        3) Men's Athletic Shoes
        4) Pants, tights, leggings
        5) Boys' Shoes
        6) Jerseys
        7) Women's Exercise Shorts
        8) Women's Activewear Tops
        9) Men's T-Shirts
        10) Men's Shorts
        11) Women's Sports Bras

    brand : str
        brand to be crawled, Supports
        1) Nike
        2) Air Jordan

    Command e.g:
    scrapy crawl nike -a category="Men's Shorts" -a brand='Nike'
    scrapy crawl nike -a category="Jerseys" -a brand='Nike'
    scrapy crawl nike -a category="Men's Athletic Shoes" -a brand='Nike'
    scrapy crawl nike -a category="Men's T-Shirts" -a brand='Nike'
    scrapy crawl nike -a category="Women's Activewear Tops" -a brand='Nike'

    scrapy crawl nike -a category="Pants, tights, leggings" -a brand='Nike'
    scrapy crawl nike -a category="Women's Sports Bras" -a brand='Nike'
    scrapy crawl nike -a category="Women's Exercise Shorts" -a brand='Nike'
    """

    name = "nike"

    api_base_graphql = 'https://api.nike.com/cic/browse/v1?queryid=' # 'https://www.nike.com/w/graphql?queryid='
    base_api_url = api_base_graphql + 'products&anonymousId={anonymous_id}&endpoint='.format(anonymous_id=uuid.uuid4().hex)
    api_url = base_api_url+'/product_feed/rollup_threads/v2?filter=marketplace(US)%26filter=language(en)%26filter=employeePrice(true)%26filter=attributeIds({attribute_ids})%26anchor={offset}%26count={size}%26consumerChannelId={channel_id}'

    detail_base_url = 'https://www.nike.com'

    category = None
    brand = None

    merge_key = 'style'

    source = 'nike.com'
    blob_name = 'nike.txt'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    url_dict = {
        'Womens Sneakers': {
            'Nike': {
                'url': 'https://store.nike.com/us/en_us/pw/womens-shoes/7ptZoi3',
                'filters': [],
            }
        },
        'Womens Athletic Shoes': {
            'Nike': {
                'url': 'https://store.nike.com/us/en_us/pw/womens-running-shoes/7ptZ8yzZoi3',
                'filters': [],
            }
        },
        "Men's Athletic Shoes": {
            'Air Jordan': {
                'url': 'https://www.nike.com/w/mens-jordan-shoes-37eefznik1zy7ok',
                'filters': ["Collaborator", "Shoes", "Technology", "Shoe Height", "Width", "Surface", "Closure Type", "Features", ],
            }
        },
        "Pants, tights, leggings": {
            'Nike': {
                'url': "https://www.nike.com/w/womens-pants-tights-2kq19z5e1x6",
                'filters': ['Color'],
            }
        },
        "Boys' Shoes": {
            # 'Nike': {
            #     'url': 'https://www.nike.com/w/boys-jordan-shoes-1onraz37eefz6dacezagibjzy7ok',
            #     'filters': ["Technology", "Shoe Height", "Width", "Surface", "Closure Type", "Athletes", "Kids Age"],
            # }
            # 'Nike': {
            #     'url': 'https://www.nike.com/w/boys-shoes-1onraz6dacezagibjzy7ok',
            #     'filters': ["Brand", "Icon", "Technology", "Shoe Height", "Width", "Surface", "Closure Type", "Athletes", "Kids Age", "Features"],
            # }
            # 'Nike': {
            #     'url': 'https://www.nike.com/w/baby-toddler-boys-shoes-1onraz2j488zy7ok',
            #     'filters': ["Brand", "Icon", "Technology", "Shoe Height", "Width", "Surface", "Closure Type", "Athletes", "Kids Age", "Material", "Features"],
            # }
            'Nike': {
                'url': [
                    'https://www.nike.com/w/baby-toddler-boys-shoes-1onraz2j488zy7ok',
                    'https://www.nike.com/w/boys-shoes-1onraz6dacezagibjzy7ok',
                    'https://www.nike.com/w/baby-toddler-boys-shoes-1onraz2j488zy7ok',
                ],
                'filters': ["Brand", "Icon", "Technology", "Shoe Height", "Width", "Surface", "Closure Type", "Athletes", "Kids Age", "Material", "Features"],
            }
        },
        "Jerseys": {
            'Nike': {
                'url': 'https://www.nike.com/w/mens-jerseys-3a41eznik1',
                'filters': ["Brand", "Sleeve Length", "Fit", "Athletes", "Size", ],
            }
        },
        "Women's Exercise Shorts": {
            'Nike': {
                'url': 'https://www.nike.com/w/womens-shorts-38fphz5e1x6',
                'filters': ["Brand", "Color", "Fit", "Technology", "Sports & Activities", "Rise"],
            }
        },
        "Women's Activewear Tops": {
            'Nike': {
                'url': 'https://www.nike.com/w/womens-tops-t-shirts-5e1x6z9om13',
                'filters': [
                    "Brand", "Color", "Fit", "Technology",
                    # "Size",
                    "Material", "Collaborator", "Sports", "Best For", "Sleeve Length", "Closure Type", "Features",
                ],
            }
        },
        "Men's T-Shirts": {
            'Nike': {
                'url': 'https://www.nike.com/w/mens-tops-t-shirts-9om13znik1',
                'filters': [
                    "Brand", "Color", "Fit", "Technology", "Collaborator", "Sports",
                    "Best For",
                    "Sleeve Length", "Closure Type", "Features", "Athletes"
                ],
            }
        },
        "Men's Shorts": {
            'Nike': {
                'url': 'https://www.nike.com/w/mens-shorts-38fphznik1',
                'filters': [
                    "Brand", "Color", "Sports & Activities", "Best For",
                    # "Technology",
                    # "Fit", "Collaborator", # "Athletes",
                ],
            }
        },
        "Women's Sports Bras": {
            'Nike': {
                'url': 'https://www.nike.com/w/womens-sports-bras-40qgmz5e1x6',
                'filters': [
                    "Color", "Cup Type", "Back Type", "Closure Type", "Neck Style",
                ],
            }
        },
    }

    filter_list = []

    extra_item_infos = {}


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(NikeSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}, filters:{filter}".format(category = self.category,brand=self.brand, url=self.launch_url,filter =self.filter_list))


    def contracts_mock_function(self):
        self.filter_list = [
                    "Brand", "Color", "Fit", "Technology", "Collaborator", "Sports",
                    "Best For", "Sleeve Length", "Closure Type", "Features", "Athletes"
                ]


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            for url in [self.launch_url] if isinstance(self.launch_url, str) else self.launch_url:
                yield self.create_request(url=url, callback=self.parse, dont_filter=True)
                yield self.create_request(url=url, callback=self.parse_filters if self.filter_list else self.parse, dont_filter=True)


    def parse_filters(self, response):
        """
        @url https://www.nike.com/w/womens-tops-t-shirts-5e1x6z9om13
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        script = response.xpath('//script[contains(text(), "window.INITIAL_REDUX_STATE=")]/text()').extract_first()

        script=script.replace('window.INITIAL_REDUX_STATE=', '')
        s=script.rsplit(';', 1)[0]
        j=json.loads(s)
        channel_id = j.get('Config', {}).get('channelId', '')

        # NOTE: This category filter, is no more needed as this info is available in the detail page
        # for category in j.get('Wall', {}).get('facetNav', {}).get('categories', []):
        #     attribute_ids = ','.join(category.get('navigationAttributeIds', []))
        #     url = self.api_url.format(channel_id=channel_id, attribute_ids=attribute_ids, size=24, offset=0)
        #     request = self.create_request(url=url, callback=self.parse_listing_page)
        #     request.meta['extra_item_info'] = {
        #         'filter_category': category.get('displayText', '')
        #     }
        #     yield request

        for filter in j.get('Wall', {}).get('facetNav', {}).get('filters', []):
            if filter.get('displayText', '') in self.filter_list:
                a = [d.get('selected', False) for d in filter.get('options', [])]
                from functools import reduce
                import operator
                has_already_selected_options = reduce(operator.or_, a, False)
                for option in filter.get('options', []):
                    if (has_already_selected_options and option.get('selected', False)) \
                        or not has_already_selected_options:
                        attribute_ids = ','.join(option.get('navigationAttributeIds', []))
                        url = self.api_url.format(channel_id=channel_id, attribute_ids=attribute_ids, size=24, offset=0)
                        request = self.create_request(url=url, callback=self.parse_listing_page)
                        request.meta['extra_item_info'] = {
                            filter.get('displayText', ''): option.get('displayText', '')
                        }
                        yield request


    def parse(self, response):
        """
        @url https://www.nike.com/w/womens-tops-t-shirts-5e1x6z9om13
        @returns requests 1
        """
        print(response.url)
        channel_id = ''
        attribute_ids = ''

        script = response.xpath('//script[contains(text(), "window.INITIAL_REDUX_STATE=")]/text()').extract_first()
        match = re.findall(re.compile(r'consumerChannelId=(.*?)",', re.IGNORECASE), script)
        if match:
            channel_id=match[0]

        match = re.findall(re.compile(r'&filter=attributeIds%28(.*?)%29&anchor=', re.IGNORECASE), script)
        if match:
            attribute_ids = re.sub(re.compile('%2C', re.IGNORECASE), ',', match[0])

        print(f'channel_id:{channel_id} and attribute_ids:{attribute_ids}')
        if channel_id and attribute_ids:
            url = self.api_url.format(channel_id=channel_id, attribute_ids=attribute_ids, size=24, offset=0)
            yield self.create_request(url=url, callback=self.parse_listing_page)


    def parse_listing_page(self, response):
        """
        @url https://api.nike.com/cic/browse/v1?queryid=products&anonymousId=e31fd393b8a545beb2b467dc209f4c8f&endpoint=/product_feed/rollup_threads/v2?filter=marketplace(US)%26filter=language(en)%26filter=employeePrice(true)%26filter=attributeIds(de314d73-b9c5-4b15-93dd-0bef5257d3b4,7baf216c-acc6-4452-9e07-39c2ca77ba32)%26anchor=0%26count=24%26consumerChannelId=d9a5bc42-4b9c-4976-858a-f159cf99c647&count=24
        @returns requests 1
        """
        res = json.loads(response.text)

        products = res.get('data', {}).get('products', {}).get('products', []) or []

        for product in products:
            style_colors = []

            url = product.get('url','').replace('{countryLang}','')
            style_id = url.split('/')[-1]
            url = url.replace(style_id,'')
            if url:
                variants = product.get('colorways',[])
                for variant in variants:
                    product_url = variant.get('pdpUrl','') ##e.g "pdpUrl": "{countryLang}/t/fast-womens-running-tank-plus-size-1f9BwS/CU8422-529"
                    style_color = product_url.split('/')[-1]
                    style_colors.append(style_color)
                style_colors = list(set(style_colors))

                for style_color in style_colors:
                    if not self.extra_item_infos.get(style_color, None):
                        self.extra_item_infos[style_color] = {}

                    self.extra_item_infos.get(style_color).update(response.meta.get('extra_item_info', {}))
                    # self.update_extra_item_infos(style_color, response.meta.get('extra_item_info', {}))

                    detail_page_url = self.detail_base_url + url +  style_color
                    print(detail_page_url)
                    request = self.create_request(url=detail_page_url,
                        callback=self.crawl_detail_page)
                    # request.meta['extra_item_info'] = response.meta.get('extra_item_info', {})
                    yield request

        next_page_url = res.get('data', {}).get('products', {}).get('pages', {}).get('next', '')
        if next_page_url:
            request = self.create_request(url=self.base_api_url+next_page_url.replace('&', '%26'), callback=self.parse_listing_page)
            request.meta['extra_item_info'] = response.meta.get('extra_item_info', {})
            yield request


    def crawl_detail_page(self, response):
        """
        @url https://www.nike.com/in/t/swoosh-run-1-2-zip-running-top-KPxhnp/CW0293-010
        @scrape_values title subtitle price image description style color rating product_details sizes item_category size_fit_description
        """
        style = response.css('ul.description-preview__features > li.description-preview__style-color ::text').extract_first(default='') or ''
        style = style.replace('Style: ', '') if style else ''
        style = style.strip()

        script = response.xpath('//script[contains(text(), "window.INITIAL_REDUX_STATE=")]/text()').extract_first(default='')
        script=script.replace('window.INITIAL_REDUX_STATE=', '')
        s=script.rsplit(';', 1)[0]
        j=json.loads(s)
        item = j.get('Threads', {}).get('products', {}).get(style, {})
        if not item:
            for product in j.get('Threads', {}).get('products', {}).values():
                if product and product.get('styleColor', '') == style:
                    item = product
                    break
        skus = [(sku.get('localizedSize', ''), sku.get('skuId', '')) for sku in item.get('skus', [])]
        available_skus = [sku.get('skuId', '') for sku in item.get('availableSkus', [])]
        available_sizes = map(lambda x: x[0] if x[1] in available_skus else None, skus)
        available_sizes = list(filter(None, available_sizes))
        item_category = item.get('category', '')
        size_fit_description = item.get('sizeAndFitDescription', '')
        if size_fit_description:
            sel = Selector(size_fit_description)
            size_fit_description = ', '.join(sel.xpath('//text()').extract())

        color = response.css('ul.description-preview__features > li.description-preview__color-description ::text').extract_first(default='') or ''
        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,
            'subtitle': response.xpath('//h2[@data-test="product-sub-title"]/text()').extract_first(default='') or '',
            'title': response.xpath('//h1[@id="pdp_product_title"]/text()').extract_first(default='') or '',
            'price': response.xpath('//div[@data-test="product-price"]/text()').extract_first(default='') or '',
            'image': response.xpath('//img[@class="css-viwop1 u-full-width u-full-height"]/@src').extract_first(default='') or '',
            'description': response.css('div.description-preview > p ::text').extract_first(default='') or '',
            'style': style,
            'color': color.replace('Shown: ', '') if color else '',
            'rating': response.css('div.product-review > p.d-sm-ib ::text').extract_first(default='') or '',
            'product_details': ', '.join(response.xpath('//div[@class="pi-pdpmainbody"]/ul/li/text()').extract()) or '',
            'sizes': available_sizes,
            'item_category': item_category,
            'size_fit_description': size_fit_description,
        }


    # def update_extra_item_infos(self, id, extra_item_info):
    #     info = self.extra_item_infos.get(id, {})
    #     for key, value in extra_item_info.items():
    #         l = info.get(key, [])
    #         if value not in l:
    #             l.append(value)
    #             info[key] = l
    #     self.extra_item_infos[id] = info


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
