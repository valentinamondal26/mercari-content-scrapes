'''
https://mercari.atlassian.net/browse/USCC-357 - SeneGence Lip Makeup
'''

import scrapy
import random
import os
from datetime import datetime
from scrapy.exceptions import CloseSpider

class SenegenceSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from senegence.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Lip Makeup

    brand : str
        brand to be crawled, Supports
        1) SeneGence

    Command e.g:
    scrapy crawl senegence -a category='Lip Makeup' -a brand='SeneGence'
    """

    name = 'senegence'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'senegence.com'
    blob_name = 'senegence.txt'

    url_dict = {
        'Lip Makeup': {
            'SeneGence': {
                'url': 'https://seneweb.senegence.com/us/products/lips/',
            }
        }
    }

    base_url = "https://seneweb.senegence.com"


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SenegenceSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://seneweb.senegence.com/us/products/lips/lipsense/lipsense/
        @scrape_values title price image volume model_sku description color
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        products = response.selector.xpath('//div[@class="categoryChildDiv productListChild"]')
        if products:
            for product in products:

                id = product.xpath('.//a/@href').extract_first()

                if id:
                    request = self.create_request(url=self.base_url + id, callback=self.crawlDetail)
                    yield request
            return

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//div[@id="p-font-subhead"]/p/text()').extract_first(default=''),
            'price': response.xpath('//span[@class="p-font-price"]/text()').extract_first(default='').strip(),
            'image': self.base_url + response.xpath('//img[@id="productMainImage"]/@src').extract_first(default=''),
            'volume': response.xpath('//div[@class="p-font-itemsize"]/span/text()').extract_first(default=''),
            'model_sku': response.xpath('//span[@id="dispSku"]/text()').extract_first(default='').strip()
                or response.xpath('//span[@id="ItemSpan"]/following-sibling::span/text()').extract_first(default='').strip(),
            'description': ''.join(response.xpath('//div[@id="p-font-text"]//text()[normalize-space(.)]').extract()),
            'color': response.xpath('//select[@id="ProductNameDropDown"]/option').xpath(u'normalize-space(./text())').extract(),
        }


    def parse(self, response):
        """
        @url https://seneweb.senegence.com/us/products/lips/
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for product in response.selector.xpath('//div[@class="categoryChildDiv productListChild"]'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                request = self.create_request(url=self.base_url + id, callback=self.crawlDetail)
                yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

