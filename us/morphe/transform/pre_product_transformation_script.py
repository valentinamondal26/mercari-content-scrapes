import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '') or ''
            if price:
                price = str(price/100).replace("$", "").strip()

            brand = record.get('brand', '')
            

            model = record.get('title', '')
            model = re.sub(re.compile(r'Palette|\'TM\'9|'+brand, re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'^\s*X\b', re.IGNORECASE), '', model)
            model = model.title()
            model = re.sub(re.compile(r"\bIi\b", re.IGNORECASE), "II", model)
            model = re.sub(r'\s+', ' ', model).strip()

            length = ''
            width = ''
            thickness = ''
            weight = ''
            number_of_pans = ''

            content = record.get('content', '')

            match = re.findall(re.compile(r'Length [–-] (.*?")', re.IGNORECASE), content)
            if match:
                length = match[0]

            match = re.findall(re.compile(r'Width [–-] (.*?")', re.IGNORECASE), content)
            if match:
                width = match[0]

            match = re.findall(re.compile(r'Thickness [–-] (.*?")', re.IGNORECASE), content)
            if match:
                thickness = match[0]

            match = re.findall(re.compile(r'Net Wt\.(.*?oz)', re.IGNORECASE), content)
            if match:
                weight = match[0]

            match = re.findall(re.compile(r'Number of Pans:\s*(\d+)', re.IGNORECASE), content)
            if match:
                number_of_pans = match[0]

            if not re.findall(re.compile(brand, re.IGNORECASE), record.get('vendor', '')):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),

                "product_type": record.get('collection', ''),
                "length": length,
                "width": width,
                "thickness": thickness,
                "weight": weight,
                "description": record.get('description', ''),
                "color": ','.join([color_name.strip('/').strip() for color_name in record.get('colors_name', '')]),
                "number_of_pans": number_of_pans,

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
