'''
https://mercari.atlassian.net/browse/USCC-363 - Morphe Makeup palettes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import json
from parsel import Selector
from urllib.parse import urlparse, parse_qs, urlencode

class MorpheSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from morphe.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Makeup palettes

    brand : str
        brand to be crawled, Supports
        1) Morphe

    Command e.g:
    scrapy crawl morphe -a category='Makeup palettes' -a brand='Morphe'
    """

    name = 'morphe'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'morphe.com'
    blob_name = 'morphe.txt'

    url_dict = {
        'Makeup palettes': {
            'Morphe': {
                'url': 'https://www.morphe.com/collections/eyeshadow-palettes'
            }
        }
    }

    base_url = "https://www.morphe.com"


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(MorpheSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.morphe.com/collections/eyeshadow-palettes/products/jaclyn-hill-eyeshadow-palette-collection
        @scrape_values title vendor price colors_name raw_content color_synonyms content collection description benefits colors
        """

        j = json.loads(response.xpath('//form[@class="product_form"]/@data-product').extract_first())

        sel = Selector(j['description'])
        desc = sel.xpath('//div[@class="resp-tabs-container"]/div[1]')
        # description = desc.xpath('./child::p/text()').extract_first()

        i = 0
        for d in desc.xpath('./child::p'):
            i = i+1
            dd= d.xpath('./text()')
            description = dd.extract_first()
            if description:
                while i > 0:
                    i = i - 1
                    collection = desc.xpath('./child::p[$index]/strong/text()', index=i).extract_first()
                    if collection:
                        break
                break

        # print(description, collection, sep='\n')

        benefits = desc.xpath('./p/strong[contains(., "BENEFITS")]/../following-sibling::ul[1]/li/text()').extract()
        colors = desc.xpath('./p/strong[contains(., "SHADE NAMES")]/../following-sibling::ul/li//text()[normalize-space(.)]').extract()

        color_name = desc.xpath('./p/strong[contains(., "SHADE NAMES")]/../following-sibling::ul/li/strong/text()[normalize-space(.)]').extract()
        color_synonyms = desc.xpath('./p/strong[contains(., "SHADE NAMES")]/../following-sibling::ul/li/text()[normalize-space(.)]').extract()

        raw_content =j['description']
        content = ''.join(desc.xpath('.//text()[normalize-space(.)]').extract())

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            # 'title': response.xpath('//h1[@itemprop="name"]/text()').extract_first(),

            'title': j.get('title', ''),
            'vendor': j.get('vendor', ''),
            'price': j.get('price', ''),

            'raw_content': raw_content,
            'colors_name': color_name,
            'color_synonyms': color_synonyms,

            'content': content,

            'collection': collection,
            'description': description,
            'benefits': benefits,
            'colors': colors,
        }


    def parse(self, response):
        """
        @url https://www.morphe.com/collections/eyeshadow-palettes
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        products = response.selector.xpath('//div[@class="product item"]')
        if products:
            for product in products:

                id = product.xpath('.//a/@href').extract_first()

                if id:
                    request = self.create_request(url=self.base_url + id, callback=self.crawlDetail)
                    yield request

            parts = urlparse(response.url)
            query_dict = parse_qs(parts.query)
            current_page = int(query_dict.get('page', ['0'])[0])
            query_dict.update({'page':[current_page + 1]})
            next_link = parts._replace(query=urlencode(query_dict, True)).geturl()
            yield self.create_request(url=next_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request