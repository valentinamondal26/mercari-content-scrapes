CREATE RULE "pending_synonym_duplicate_ignore" AS ON INSERT TO public.pending_synonym
  WHERE EXISTS(SELECT 1 FROM public.pending_synonym
                WHERE "pendingSynonymId"=NEW."pendingSynonymId")
  DO INSTEAD NOTHING;