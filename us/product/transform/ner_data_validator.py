# Checks for attributes with empty IDs in NER resolved data and remove them
class NERDataValidator(object):

    def map(self, item):
        entities = item['entities']
        valid_resolved_entities = []
        for entity in entities:
            if 'attribute' in entity and entity['attribute']:
                entity['attribute'] = self.ignoreInvalidIds(entity['attribute'])
                valid_resolved_entities.append(entity)

        item['entities'] = valid_resolved_entities
        return item

    # Ignoring attributes that have 'entity resolved id' empty (500 from NER API)
    def ignoreInvalidIds(self, array):
        data = []
        for val in array:
            if 'id' in val and val['id']:
                data.append(val)

        return data
