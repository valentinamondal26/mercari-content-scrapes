import json

from pyspark.sql.functions import udf, explode, split, length
import uuid
from us.product.transform.category_name_id_map import get_category_id

# Class to extract attributes that are not resovled by NER to be passed to turks
class ExtractEntityAttributes(object):

    def process(self, context, config, datadict):

        df = datadict['transformed']
        attributes_extract_udf = udf(lambda x: self.extract_entity_attributes(x))
        attributes_df = df.withColumn(df.columns[0], attributes_extract_udf(df.columns[0]))

        report_item = {
            "attributes_count_before_exploding": attributes_df.count()
        }

        attributes_df.show(truncate=False)
        attributes_df = attributes_df.select(explode(split(attributes_df.columns[0], "\n")))

        report_item['attributes_count_after_exploding'] = attributes_df.count()

        attributes_df = attributes_df.filter(length(attributes_df.columns[0])>0)
        attributes_df = attributes_df.dropDuplicates()
        report_item['attributes_count_after_dropping_duplicates'] = attributes_df.count()

        datadict['attributes_for_turks'] = attributes_df

        context.report_writer.add_line_item("invalid-attributes-extraction", report_item)

    def extract_entity_attributes(self, item):
        item = json.loads(item)
        print("Item : {}".format(item))
        attributes = ""
        source = item['source']

        if 'entities' in item:
            entities = item['entities']
            length = len(entities)
            categoryId = get_category_id(item['system_category'].lower())

            for idx, entity in enumerate(entities):
                if 'attribute' in entity:
                    entity_attributes = entity['attribute']
                    unresolved_attributes = self.extract_unresolved(entity_attributes)
                    attributes += self.flatten_and_clean(unresolved_attributes, source, entity['entity'], categoryId)
                    if attributes and idx < (length - 1):
                        attributes += "\n"

        return attributes

    #Returns unresolved attributes
    def extract_unresolved(self, attributes):
        unresolved_attributes = []
        for attribute in attributes:
            if 'id' in attribute and attribute['id'].startswith('000_'):
                unresolved_attributes.append(attribute)

        return unresolved_attributes

    # Flattening to output JSON lines instead of array. Clean data as required by Postgres entitydb.pending_synonym table
    #Format: {
    #     "active": true,
    #     "attributeId": null,
    #     "categoryId": null,
    #     "entityId": null,
    #     "name": "Josh",
    #     "pending": true,
    #     "pendingSynonymId": "rrrrrrr1"
    # }
    def flatten_and_clean(self, array, app, entityId, categoryId):
        data = ""
        length = len(array)
        for idx, val in enumerate(array):
            val['active'] =  True
            val['attributeId'] = val['id']
            val['categoryId'] = categoryId
            val['entityId'] = entityId
            val['pending'] = True
            val['name'] = val['value']
            val['app'] = app
            val['pendingSynonymId'] = self.getUuid(val['id'] + val['entityId'])
            val['normalizationStatus'] = "NotProcessed"
            del val['value']
            del val['id']
            data = data + json.dumps(val)
            if idx < (length - 1):
                data += "\n"

        return data

    def getUuid(self, value):
        return str(uuid.uuid5(uuid.NAMESPACE_OID, value))
