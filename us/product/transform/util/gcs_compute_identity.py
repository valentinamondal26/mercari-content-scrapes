from google.auth.transport.requests import Request

metadata_identity_doc_url = "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity"
project_id_url = "http://metadata/computeMetadata/v1/project/numeric-project-id"


def get_id_token_from_compute_engine():
    request = Request()
    headers = {"Metadata-Flavor": "Google"}
    project_id = request(project_id_url, method='GET', headers=headers)
    target_audience = project_id.data.decode('utf-8') + '-compute@developer.gserviceaccount.com'
    url = metadata_identity_doc_url + "?audience=" + target_audience
    print(url)

    resp = request(url, method='GET', headers=headers)
    return resp.data.decode('utf-8')


class GoogleIDToken:
    def headers(self):
        return {"Authorization": "Bearer {}".format(get_id_token_from_compute_engine())}
