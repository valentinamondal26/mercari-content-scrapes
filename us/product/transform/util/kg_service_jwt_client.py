import re
import time

import google.auth.crypt
import google.auth.jwt

def generate_jwt(sa_keyfile,
                 sa_email='account@project-id.iam.gserviceaccount.com',
                 audience='your-service-name',
                 expiry_length=3600):
    """
    Generate the jwt token for the given service account
    :param sa_keyfile: service account key file path
    :param sa_email: service account mail id
    :param audience: endpoint
    :param expiry_length: in seconds
    :return:
    """
    now = int(time.time())
    payload = {
        'iat': now,
        "exp": now + expiry_length,
        'iss': sa_email,
        'aud':  audience,
        'sub': sa_email,
        'email': sa_email
    }
    signer = google.auth.crypt.RSASigner.from_service_account_file(sa_keyfile)
    jwt = google.auth.jwt.encode(signer, payload)

    return jwt.decode("utf_8")

class JwtClient:
    def headers(self, uri):
        domain_name = self.extract_domain(uri)
        return {"Authorization": "Bearer {}".format(generate_jwt(
            sa_keyfile="/opt/spark/mario/data_bags/sa_key.json",
            sa_email="content-pipeline@mercari-us-de.iam.gserviceaccount.com",
            audience=domain_name
        ))}

    def extract_domain(self, url):
        domain_name = re.sub(r'(.*://)?([^/?]+).*', '\g<1>\g<2>', url)
        return domain_name
