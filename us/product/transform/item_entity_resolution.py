import traceback
import urllib
from functools import lru_cache

import requests
from requests import Timeout

from us.product.transform.util.gcs_compute_identity import get_id_token_from_compute_engine


class Mapper(object):
    def __init__(self, **kwargs):
        kwargs = kwargs['kwargs']
        self.url = kwargs['entity_normalizer_url']
        print("Entity Normalizer URL : {}".format(self.url))
        if not hasattr(self, 'token'):
            print("Initializing Identity Token in constructor")
            self.token = get_id_token_from_compute_engine()
        return

    def map(self, record):
        try:
            if 'id' in record:
                url = record['id']
                parsed_uri = urllib.parse.urlparse(url)
                source = parsed_uri.netloc
            else:
                source = "manual"

            record['entities'].append({
                'source': [source],
            })

            resolved_entities = self.resolve_entities(record['entities'], freeText=False)
            # auto_entities = self.resolve_entities(record.get('auto_entities', None), freeText=True)
            # if auto_entities is not None:
            #     resolved_entities = resolved_entities + auto_entities

            entity_resolved_item = {
                                      "item_id": record['item_id'],
                                      "system_category":record.get('system_category') or record.get('category'),
                                      "entities": resolved_entities,
                                      "source": source
            }
        except Exception as e:
            print("Exception Raised for {} with exception {}".format(record, str(e)))
            print(traceback.format_exc())
            return None
        return entity_resolved_item

    def resolve_entities(self, raw_entities, freeText):

        resolved_entities = []

        for entity in raw_entities:
            key = next(iter(entity))
            attr = entity[key]
            if len(attr) == 0:
                continue
            resolved_entities.append(self.resolve(entity, freeText))
        return resolved_entities

    def resolve(self, entity, freeText):
        key = next(iter(entity))
        attributes = entity[key]

        entity_map = dict()
        for attr in attributes:
            print (attr)
            print (key)
            attr = urllib.parse.quote(str(attr))
            url = self.url.format(attr, key)
            try:
                self.call_entity_normalizer(url, key, entity_map)
            except Timeout:
                print("FAILED: Request Timed out - {}".format(url))
                print(traceback.format_exc())

        print("Entity Map : {}".format(entity_map))
        return entity_map

    @lru_cache(maxsize=1024)
    def get_request(self, url, token):
        header = {"Authorization": "Bearer {}".format(token)}
        return requests.get(url, timeout=60, headers=header)

    def call_entity_normalizer(self, url, key, entity_map):
        resolved_attributes = []
        # loop for a max of 3 retries in case of token expiry
        for i in range(3):
            with self.get_request(url, self.token) as r:
                if r.status_code == requests.codes.ok:
                    res = r.json()
                    resolved_entity_id = res['entity_id']
                    attr_map = dict()
                    attr_map['id'] = res['attribute_id'] if res['attribute_id'] else ""
                    attr_map['name'] = res['attribute_name'] if res['attribute_name'] else ""
                    attr_map['value'] = res['query']

                    if 'attribute' in entity_map and entity_map['attribute']:
                        resolved_attributes = entity_map['attribute']
                    resolved_attributes.append(attr_map)
                    entity_map['entity'] = resolved_entity_id
                    entity_map['name'] = key
                    entity_map['attribute'] = resolved_attributes
                    # entity_map['freeText'] = freeText
                    break
                elif r.status_code == 403 or r.status_code == 401:
                    print("FAILED: Status {}".format(r.status_code))
                    print("Refreshing token ... ")
                    self.token = get_id_token_from_compute_engine()
                else:
                    print(r.text)
                    print("FAILED: Status_code {0} - {1} - {2}".format(r.status_code, url, r.text))
