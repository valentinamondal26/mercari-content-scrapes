import traceback
import urllib
from collections import defaultdict

import requests
from requests import Timeout
from us.product.transform.category_name_id_map import get_category_id
from us.product.transform.util.gcs_iap_request_util import get_google_open_id_connect_token


class Mapper(object):

    def __init__(self):
        self.url = "https://us-central1-mercari-us-de.cloudfunctions.net/category_ner?cat_id={}&query={}"
        self.client_id = '573445696111-q1h7qbjk4gl7frjdlmho4kfd7d3tuod3.apps.googleusercontent.com'
        if not hasattr(self, 'token'):
            print("Initializing Identity Token in constructor")
            self.token = get_google_open_id_connect_token(self.client_id)
        self.header = {"Authorization": "Bearer {}".format(self.token)}
        return

    def map(self, record):

        try:
            cat_id=get_category_id(record['category'].lower())
            description = urllib.parse.quote(record['ner_query'])
            url = self.url.format(cat_id, description)
            print("Url : {}".format(url))
            try:
                data_dict = defaultdict(list)
                with requests.get(url, timeout=60, headers=self.header) as r:
                    if r.status_code == requests.codes.ok:
                        res = r.json()
                        entities = res['entities']
                        entities_list = list()
                        for label in entities:
                            data_dict[(label['label']).lower()].append(label['normalized'])
                        for k, v in data_dict.items():
                            entities_list.append({k: v})
                        record['auto_entities'] = entities_list
                        record.pop("ner_query", None)
                    elif r.status_code == 403:
                        print("FAILED: Status 403. Refreshing Identity Token")
                        self.token = get_google_open_id_connect_token(self.client_id)
                        self.header = {"Authorization": "Bearer {}".format(self.token)}
                    else:
                        print("FAILED: Status_code {0} - {1} ".format(r.status_code, url))

            except Timeout:
                print("FAILED: Request Timed out - {}".format(url))
                print(traceback.format_exc())

        except Exception as e:
            print(" Exception Raised for {} with exception {}".format(record, str(e)))
            print(traceback.format_exc())
            return record
        return record
