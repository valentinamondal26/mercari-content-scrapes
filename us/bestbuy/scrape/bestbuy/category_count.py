from bs4 import BeautifulSoup
import os

category_count = {}
with open(os.path.join('output/breadcrum_list.csv'), 'a') as breadcrum:
    for folder in os.listdir('output/html/'):
        for file in os.listdir(os.path.join('output/html/',folder)):
            soup = None
            with open(os.path.join('output/html/',folder,file), 'r') as html_file:
                soup = BeautifulSoup(html_file.read(), 'lxml')
            ol_list = soup.find_all('ol')
            for ol in ol_list:
                li_list = ol.find_all("li")
                list = [li.text for li in li_list]
                breadcrum.write(",".join(list))
                breadcrum.write("\n")
                if len(list) > 2:
                    category_count[list[1]] = category_count.get(list[1],0) + 1
                    print(",".join(list))
print(category_count)