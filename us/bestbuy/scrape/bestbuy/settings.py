
# -*- coding: utf-8 -*-

# Scrapy settings for bestbuy project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'bestbuy'

SPIDER_MODULES = ['bestbuy.spiders']
NEWSPIDER_MODULE = 'bestbuy.spiders'

USER_AGENT='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'

COOKIES_ENABLED = True

USER_AGENT = 'httpie/1.0'

ROBOTSTXT_OBEY = True

CRAWL_ID=''
GCS_BLOB_TIMESTAMP=''

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 64

CONCURRENT_REQUESTS_PER_DOMAIN = 64
CONCURRENT_REQUESTS_PER_IP = 64

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

FEED_FORMAT = 'jsonlines'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/live_crawl/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.selenium_middleware.middlewares.SeleniumMiddleware': 1000,
    'common.httpproxy.HttpProxyMiddleware': 751,
}

SPIDER_CONTRACTS = {
    'common.scrapy_contracts.contracts.AddRequestArgs':10,
    'common.scrapy_contracts.contracts.ScrapesValues':10,
    'common.scrapy_contracts.contracts.CustomReturnsContract':10
}