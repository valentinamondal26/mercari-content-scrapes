import os
import scrapy
import random

class BestbuySpider(scrapy.Spider):

    name = "bestbuy"
    # with open("input/bestbuy_urls_superset_10.txt", "rt") as f:
    #     start_urls = [url.strip() for url in f.readlines()]
    # output_folder = 'output/html/{0}'
    # output_path = output_folder + '/{1}.html'

    def start_requests(self):
        for url in self.start_urls:
            request = scrapy.Request(url=url, callback=self.parse, meta={'save_html': True,})
            request = self.set_proxy(request)
            yield request

    def parse(self, response):
        req_url = response.request.url
        sku_id = req_url[req_url.rfind('=')+1:]

        yield {'url': req_url, 'sku_id' : sku_id}

    def set_proxy(self, request):

        proxy_pool = [
            'http://shp-mercari-us-d00001.tp-ns.com:80',
            'http://shp-mercari-us-d00003.tp-ns.com:80',
            'http://shp-mercari-us-d00004.tp-ns.com:80',
            'http://shp-mercari-us-d00005.tp-ns.com:80',
            'http://shp-mercari-us-d00006.tp-ns.com:80',
            'http://shp-mercari-us-d00007.tp-ns.com:80',
            'http://shp-mercari-us-d00008.tp-ns.com:80',
            'http://shp-mercari-us-d00009.tp-ns.com:80',
            'http://shp-mercari-us-d00010.tp-ns.com:80',
            'http://shp-mercari-us-d00011.tp-ns.com:80',
            'http://shp-mercari-us-d00015.tp-ns.com:80',
            'http://shp-mercari-us-d00016.tp-ns.com:80',
            'http://shp-mercari-us-d00018.tp-ns.com:80',
            'http://shp-mercari-us-d00019.tp-ns.com:80',
            'http://shp-mercari-us-d00020.tp-ns.com:80',
        ]
        request.meta['proxy'] = random.choice(proxy_pool)
        return request

    def response_html_path(self, request, response):
        """
        Args:
            request (scrapy.http.request.Request): request that produced the
                response.
        """
        #self.output_path.format(category,sku_id)
        sku = request.url.split("=")[-1]
        try:
            category = response.selector.xpath(('//*[@data-track="Breadcrumb"]/text()'))[-1].get().replace(" ", "_")
        except Exception as e:
            category = "others"

        return self.output_path.format(category, sku)
