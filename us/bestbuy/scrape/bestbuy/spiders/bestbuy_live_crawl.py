'''
https://mercari.atlassian.net/browse/USCC-10 - Texas Instruments Calculators

https://mercari.atlassian.net/browse/USCC-27 - Cellphone with Plan Cell Phones & Smartphones
https://mercari.atlassian.net/browse/USCC-37 - Unlocked Cellphone Cell Phones & Smartphones
https://mercari.atlassian.net/browse/USCC-160 - Samsung Cell Phones & Smartphones

https://mercari.atlassian.net/browse/USCC-294 - Canon Camera Lenses

https://mercari.atlassian.net/browse/USCC-267 - Beats by Dr. Dre Headphones

https://mercari.atlassian.net/browse/USDE-397 - Beats by Dr.Dre Bluetooth Headphones
https://mercari.atlassian.net/browse/USDE-799 - Generic Bluetooth Headphones
https://mercari.atlassian.net/browse/USDE-807 - Sony Bluetooth Headphones

https://mercari.atlassian.net/browse/USCC-232 - JBL Bluetooth Speakers

https://mercari.atlassian.net/browse/USDE-774 - Otterbox Cell Phone Cases, Covers & Skins
https://mercari.atlassian.net/browse/USDE-796 - Apple Cell Phone Cases, Covers & Skins

https://mercari.atlassian.net/browse/USDE-782 - Apple iPad/tablet/ebook readers (Tablets & eReaders)
https://mercari.atlassian.net/browse/USDE-687 - Samsung iPad/tablet/ebook readers (Tablets & eReaders)
https://mercari.atlassian.net/browse/USCC-129 - Amazon iPad/tablet/ebook readers (Tablets & eReaders)

https://mercari.atlassian.net/browse/USDE-783 - Fujifilm Polaroid Film Cameras

https://mercari.atlassian.net/browse/USDE-555 - Samsung Smart Watches
https://mercari.atlassian.net/browse/USDE-789 - Fitbit Smart Watches
https://mercari.atlassian.net/browse/USCC-231 - Apple Smart Watches

https://mercari.atlassian.net/browse/USDE-1219 - Apple Laptops & Netbooks
https://mercari.atlassian.net/browse/USDE-1220 - Dell Laptops & Netbooks
https://mercari.atlassian.net/browse/USDE-1221 - HP Laptops & Netbooks
https://mercari.atlassian.net/browse/USDE-1222 - Acer Laptops & Netbooks
https://mercari.atlassian.net/browse/USDE-1223 - Lenovo Laptops & Netbooks
https://mercari.atlassian.net/browse/USDE-1224 - ASUS Laptops & Netbooks
https://mercari.atlassian.net/browse/USDE-1225 - Microsoft Laptops & Netbooks

https://mercari.atlassian.net/browse/USDE-800 - Apple Smart Watch Accessories

https://mercari.atlassian.net/browse/USDE-806 - Apple Computer Accessories
https://mercari.atlassian.net/browse/USDE-1608 - Logitech Computer Accessories

https://mercari.atlassian.net/browse/USCC-242 - Nintendo  Video Game Accessories
https://mercari.atlassian.net/browse/USDE-644 - Xbox Video Game Accessories

https://mercari.atlassian.net/browse/USDE-1666 - KitchenAid Kitchen Appliances

https://mercari.atlassian.net/browse/USDE-1679 - NetGear Home Network & Connectivity Equipment

https://mercari.atlassian.net/browse/USCC-323 - Oculus VR Headsets

https://mercari.atlassian.net/browse/USCC-309 - DJI Drones
https://mercari.atlassian.net/browse/USCC-301 - Marvel DVDs & Blu-ray Discs
https://mercari.atlassian.net/browse/USCC-245 - Fitbit Fitness Activity Trackers
https://mercari.atlassian.net/browse/USCC-43 - Ring Home Surveillance Systems
https://mercari.atlassian.net/browse/USCC-114 - GoPro Sport & Waterproof Cameras
https://mercari.atlassian.net/browse/USCC-74 - Amazon Streaming Devices
https://mercari.atlassian.net/browse/USCC-155, https://mercari.atlassian.net/browse/USCC-157, https://mercari.atlassian.net/browse/USCC-161 - Dyson Vacuums & floor care
'''

import scrapy
from scrapy.exceptions import CloseSpider

import requests
import random
import re
from parsel import Selector
import datetime
import json
from urllib.parse import urlparse, parse_qs, urlencode
# from dateutil import date_parser
from .bestbuy import get_dict

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import os
from common.selenium_middleware.http import SeleniumRequest


class BestbuyLiveCrawlSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bestbuy.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Cell Phones & Smartphones
        2) Digital Cameras
        3) Video Game Consoles
        4) Bluetooth Headphones
        5) Laptops & Netbooks
        6) Calculators
        7) Home Surveillance Systems
        8) iPad/tablet/ebook readers ((Tablets & eReaders))
        9) Headphones
        10) Vacuums & floor care
        11) Camera Lenses
        12) Fitness Activity Trackers
        13) Smart Watches
        14) Smart Watch Accessories
        15) Computer Accessories
        16) Kitchen Appliances
        17) Home Network & Connectivity Equipment
        18) Bluetooth Speakers
        19) Streaming Devices

    brand : str
        brand to be crawled, Supports
        1) Cellphone with Plan
        2) Unlocked Cellphone
        3) Point & Shoot Cameras
        4) Mirrorless Camera
        5) DSLR Camera
        6) Xbox One
        7) PlayStation 4
        8) Nintendo Switch
        9) Beats by Dr. Dre
        10) Apple
        11) Texas Instruments
        12) Amazon
        13) Bose
        14) Dyson
        15) Canon
        16) Fitbit
        17) Apple
        18) Logitech
        19) KitchenAid
        20) NetGear
        21) JBL
        22) Samsung

    Command e.g:
    scrapy crawl bestbuy_live_crawl -a category='Calculators' -a brand='Texas Instruments'

    scrapy crawl bestbuy_live_crawl -a category='Cell Phones & Smartphones' -a brand='Cellphone with Plan'
    scrapy crawl bestbuy_live_crawl -a category='Cell Phones & Smartphones' -a brand='Unlocked Cellphone'
    scrapy crawl bestbuy_live_crawl -a category='Cell Phones & Smartphones' -a brand='Samsung'

    scrapy crawl bestbuy_live_crawl -a category='Camera Lenses' -a brand='Canon'

    scrapy crawl bestbuy_live_crawl -a category='Headphones' -a brand='Beats by Dr. Dre'

    scrapy crawl bestbuy_live_crawl -a category='Bluetooth Headphones' -a brand='Beats by Dr. Dre'
    scrapy crawl bestbuy_live_crawl -a category='Bluetooth Headphones' -a brand='Sony'
    scrapy crawl bestbuy_live_crawl -a category='Bluetooth Headphones' -a brand='Generic'

    scrapy crawl bestbuy_live_crawl -a category='Bluetooth Speakers' -a brand='JBL'

    scrapy crawl bestbuy_live_crawl -a category='Tablets & eReaders' -a brand='Samsung'
    scrapy crawl bestbuy_live_crawl -a category='Tablets & eReaders' -a brand='Apple'
    scrapy crawl bestbuy_live_crawl -a category='Tablets & eReaders' -a brand='Amazon'

    scrapy crawl bestbuy_live_crawl -a category='Smart Watches' -a brand='Fitbit'
    scrapy crawl bestbuy_live_crawl -a category='Smart Watches' -a brand='Samsung'
    scrapy crawl bestbuy_live_crawl -a category='Smart Watches' -a brand='Apple'

    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='Apple'
    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='Dell'
    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='HP'
    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='Acer'
    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='Lenovo'
    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='ASUS'
    scrapy crawl bestbuy_live_crawl -a category='Laptops & Netbooks' -a brand='Microsoft'

    scrapy crawl bestbuy_live_crawl -a category='Polaroid Film Cameras' -a brand='Fujifilm'

    scrapy crawl bestbuy_live_crawl -a category='Cell Phone Cases, Covers & Skins' -a brand='Apple'
    scrapy crawl bestbuy_live_crawl -a category='Cell Phone Cases, Covers & Skins' -a brand='OtterBox'

    scrapy crawl bestbuy_live_crawl -a category='Smart Watch Accessories' -a brand='Apple'

    scrapy crawl bestbuy_live_crawl -a category='Computer Accessories' -a brand='Apple'
    scrapy crawl bestbuy_live_crawl -a category='Computer Accessories' -a brand='Logitech'

    scrapy crawl bestbuy_live_crawl -a category='Video Game Accessories' -a brand='Nintendo'
    scrapy crawl bestbuy_live_crawl -a category='Video Game Accessories' -a brand='Xbox'

    scrapy crawl bestbuy_live_crawl -a category='Kitchen Appliances' -a brand='KitchenAid'

    scrapy crawl bestbuy_live_crawl -a category='Home Network & Connectivity Equipment' -a brand='NetGear'

    scrapy crawl bestbuy_live_crawl -a category='VR Headsets' -a brand='Oculus'

    scrapy crawl bestbuy_live_crawl -a category='Digital Cameras' -a brand='Polaroid'
    scrapy crawl bestbuy_live_crawl -a category='Fitness Activity Trackers' -a brand='Fitbit'
    scrapy crawl bestbuy_live_crawl -a category='Home Surveillance Systems' -a brand='Ring'
    scrapy crawl bestbuy_live_crawl -a category='Sport & Waterproof Cameras' -a brand='GoPro'
    scrapy crawl bestbuy_live_crawl -a category='Streaming Devices' -a brand='Amazon'
    scrapy crawl bestbuy_live_crawl -a category='Vacuums & floor care' -a brand='Dyson'
    """

    name = 'bestbuy_live_crawl'
    category = None
    brand = None
    source = 'bestbuy.com'
    blob_name = 'bestbuy.txt'
    schema_keys = None

    merge_key = 'id'
    extra_item_infos = {}

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'Cell Phones & Smartphones' : {
            'Cellphone with Plan': {
                'url': 'https://www.bestbuy.com/site/mobile-cell-phones/mobile-phones-with-plans/pcmcat209400050001.c?id=pcmcat209400050001',
            },
            'Unlocked Cellphone': {
                'url': 'https://www.bestbuy.com/site/unlocked-mobile-phones/all-unlocked-cell-phones/pcmcat311200050005.c?id=pcmcat311200050005',
            },
            'Samsung': {
                'url': 'https://www.bestbuy.com/site/unlocked-mobile-phones/all-unlocked-cell-phones/pcmcat311200050005.c?id=pcmcat311200050005&qp=brand_facet%3DBrand~Samsung',
            },
        }, 'Digital Cameras': {
            'Point & Shoot Cameras': {
                'url': 'https://www.bestbuy.com/site/digital-cameras/point-shoot-cameras/abcat0401001.c?id=abcat0401001',
            },
            'Mirrorless Camera': {
                'url': 'https://www.bestbuy.com/site/digital-cameras/mirrorless-cameras/pcmcat214000050005.c?id=pcmcat214000050005',
            },
            'DSLR Camera': {
                'url': 'https://www.bestbuy.com/site/digital-slr-cameras/all-dslrs/pcmcat186400050004.c?id=pcmcat186400050004',
            },
            'Polaroid': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?af=false&id=pcat17071&st=polaroid%20camera'
            }
        }, 'Video Game Consoles' : {
            'Xbox One': {
                'url': 'https://www.bestbuy.com/site/xbox-one/xbox-one-consoles/pcmcat303600050004.c?id=pcmcat303600050004',
            },
            'PlayStation 4': {
                'url': 'https://www.bestbuy.com/site/playstation-4-ps4/playstation-4-ps4-hardware-console/pcmcat296300050017.c?id=pcmcat296300050017',
            },
            'Nintendo Switch': {
                'url': 'https://www.bestbuy.com/site/nintendo-switch/nintendo-switch-consoles/pcmcat1484077694025.c?id=pcmcat1484077694025',
            },
        }, 'Bluetooth Speakers' : {
            'JBL': {
                'url': 'https://www.bestbuy.com/site/portable-speakers-docks-radios/all-bluetooth-and-wireless-speakers/pcmcat748301665095.c?id=pcmcat748301665095&qp=brand_facet%3DBrand~JBL',
            },
        }, 'Bluetooth Headphones' : {
            'Beats by Dr. Dre': {
                'url': 'https://www.bestbuy.com/site/beats-by-dre-headphones-speakers/all-beats-products/pcmcat1554393731715.c?id=pcmcat1554393731715&qp=headphoneconnectiontype_facet%3DConnection%20Type~True%20Wireless%5Eheadphoneconnectiontype_facet%3DConnection%20Type~Wireless',
            },
            'Apple': {
                'url': 'https://www.bestbuy.com/site/headphones/all-headphones/pcmcat144700050004.c?id=pcmcat144700050004&qp=brand_facet%3DBrand~Apple',
            },
            'Bose': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&browsedCategory=pcmcat331200050015&id=pcat17071&iht=n&ks=960&list=y&qp=brand_facet%3DBrand~Bose%C2%AE&sc=Global&st=pcmcat331200050015_categoryid%24pcmcat144700050004&type=page&usc=All%20Categories',
            },
            'Sony': {
                'url': 'https://www.bestbuy.com/site/sony-portable-audio/sony-headphones/pcmcat302400050012.c?id=pcmcat302400050012&qp=wirelessconnectivity_facet%3DWireless%20Connectivity~Bluetooth',
            },
            'Generic': {
                'url': 'https://www.bestbuy.com/site/headphones/over-the-ear-headphones/pcmcat143000050011.c?id=pcmcat143000050011&qp=features_facet%3DFeatures~Bluetooth%20Built-In',
            },
        }, 'Calculators': {
            'Texas Instruments': {
                'url': 'https://www.bestbuy.com/site/office-electronics/calculators/abcat0805001.c?id=abcat0805001&qp=brand_facet%3DBrand~Texas%20Instruments',
            },
        }, 'Laptops & Netbooks': {
            'All': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001',
            },
            'Apple': {
                'url': 'https://www.bestbuy.com/site/all-laptops/macbooks/pcmcat247400050001.c?id=pcmcat247400050001',
            },
            'Dell': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001&qp=brand_facet%3DBrand~Dell',
            },
            'HP': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001&qp=brand_facet%3DBrand~HP',
            },
            'Acer': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001&qp=brand_facet%3DBrand~Acer',
            },
            'Lenovo': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001&qp=brand_facet%3DBrand~Lenovo',
            },
            'ASUS': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001&qp=brand_facet%3DBrand~ASUS',
            },
            'Microsoft': {
                'url': 'https://www.bestbuy.com/site/laptop-computers/all-laptops/pcmcat138500050001.c?id=pcmcat138500050001&qp=brand_facet%3DBrand~Microsoft',
            },
            'schema_keys': ['features']
        }, 'Home Surveillance Systems': {
            'Ring': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&id=pcat17071&iht=y&keys=keys&ks=960&list=n&qp=brand_facet%3DBrand~Ring&sc=Global&st=home%20survelliance%20ring&type=page&usc=All%20Categories'
            },
        }, 'Tablets & eReaders': {
            'Amazon': {
                'url': 'https://www.bestbuy.com/site/ipad-tablets-ereaders/tablets/pcmcat209000050008.c?id=pcmcat209000050008&qp=brand_facet%3DBrand~Amazon',
            },
            'Samsung': {
                'url': 'https://www.bestbuy.com/site/ipad-tablets-ereaders/tablets/pcmcat209000050008.c?id=pcmcat209000050008&qp=brand_facet%3DBrand~Samsung%5Econdition_facet%3DCondition~New',
            },
            'Apple': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&browsedCategory=pcmcat209000050008&id=pcat17071&iht=n&ks=960&list=y&qp=brand_facet%3DBrand~Apple%5Econdition_facet%3DCondition~New&sc=Global&st=categoryid%24pcmcat209000050008&type=page&usc=All%20Categories',
                'detail_variants': ['Carrier'],
            },
        }, 'Headphones': {
            'Beats by Dr. Dre': {
                'url': 'https://www.bestbuy.com/site/headphones/all-headphones/pcmcat144700050004.c?id=pcmcat144700050004&qp=brand_facet%3DBrand~Beats%20by%20Dr.%20Dre%5Eheadphoneconnectiontype_facet%3DConnection%20Type~Wired',
            },
        }, 'Vacuums & floor care': {
            'Dyson': {
                'url': [
                    'https://www.bestbuy.com/site/dyson/dyson-canister-vacuums/pcmcat162500050030.c?id=pcmcat162500050030',
                    'https://www.bestbuy.com/site/dyson/dyson-cordless-vacuums/pcmcat162500050029.c?id=pcmcat162500050029',
                    'https://www.bestbuy.com/site/dyson/dyson-upright-vacuums/pcmcat162500050028.c?id=pcmcat162500050028',
                ],
            },
        }, 'Camera Lenses': {
            'Canon': {
                'url': 'https://www.bestbuy.com/site/cameras-camcorders/digital-camera-accessories/abcat0410000.c?id=abcat0410000&qp=brand_facet%3DBrand~Canon%5Ecategory_facet%3DCamera%20Lenses~abcat0410018',
            },
        },
        'Streaming Devices': {
           'Amazon': {
                'url': 'https://www.bestbuy.com/site/tv-home-theater/streaming-media-devices/pcmcat161100050040.c?id=pcmcat161100050040&qp=brand_facet%3DBrand~Amazon%5Efeatures_facet%3DFeatures~Smart%20Capable&intl=nosplash',
                'schema_keys': ['category','title','specifications']
           },
        },
        'Drones': {
           'DJI': {
                'url': 'https://www.bestbuy.com/site/drones-toys-collectibles/aerial-drones-accessories/pcmcat369900050001.c?id=pcmcat369900050001&qp=brand_facet%3DBrand~DJI%5Ecategory_facet%3DCamera%20Drones~pcmcat369900050002'
           },
        },
        'Cell Phone Cases, Covers & Skins': {
            'Apple': {
                "url": 'https://www.bestbuy.com/site/mobile-phone-accessories/cases-covers-clips/abcat0811006.c?id=abcat0811006&qp=brand_facet%3DBrand~Apple',
               "filters": ['Material']
            },
            "OtterBox": {
               "url": "https://www.bestbuy.com/site/mobile-phone-accessories/cases-covers-clips/abcat0811006.c?id=abcat0811006&qp=brand_facet%3DBrand~OtterBox&intl=nosplash",
               "filters": ['Material']
            },
        },
        'Sport & Waterproof Cameras': {
            'GoPro': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?id=pcat17071&qp=brand_facet%3DBrand~GoPro%5Ecategory_facet%3DCamcorders~abcat0403000&st=gopro'
            },
        },
        'Desktops & All-In-One Computers': {
            'Apple': {
                'url': 'https://www.bestbuy.com/site/desktop-computers/all-desktops/pcmcat143400050013.c?id=pcmcat143400050013&qp=brand_facet%3DBrand~Apple',
            },
        },
        'Fitness Activity Trackers' : {
            'Fitbit': {
                'url': 'https://www.bestbuy.com/site/health-fitness-sports/gps-watches-fitness/abcat0301006.c?id=abcat0301006&qp=brand_facet%3DBrand~Fitbit'
            },
        },
        'Smart Watches': {
            'Apple': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&browsedCategory=pcmcat321000050004&id=pcat17071&iht=n&ks=960&list=y&qp=brand_facet%3DBrand~Apple&sc=Global&st=categoryid%24pcmcat321000050004&type=page&usc=All%20Categories',
            },
            'Fitbit': {
                'url': 'https://www.bestbuy.com/site/smart-watches-accessories/smart-watches/pcmcat321000050004.c?id=pcmcat321000050004&qp=brand_facet%3DBrand~Fitbit%5Eformfactormv_facet%3DForm%20Factor~Wristwatch',
            },
            'Samsung': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&id=pcat17071&iht=y&keys=keys&ks=960&list=n&qp=brand_facet%3DBrand~Samsung&sc=Global&st=samsung%20smartwatch&type=page&usc=All%20Categories',
                'filters': ['Features'],
            },
        },
        'Video Game Accessories':{
            'Nintendo': {
                'url': 'https://www.bestbuy.com/site/nintendo-switch/nintendo-switch-accessories/pcmcat1484080326504.c?id=pcmcat1484080326504&intl=nosplash'
            },
            'Xbox': {
                'url': 'https://www.bestbuy.com/site/xbox-one/xbox-one-accessories/pcmcat303700050011.c?id=pcmcat303700050011',
                'filters': ['Category'],
            },
        },
       'DVDs & Blu-ray Discs':{
          'Marvel': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&browsedCategory=pcmcat748302046244&id=pcat17071&iht=n&ks=960&list=y&qp=format_facet%3DFormat~Blu-ray&sc=Global&st=categoryid%24pcmcat748302046244&type=page&usc=All%20Categories'
          },
        },
        'Polaroid Film Cameras': {
            'Fujifilm': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&id=pcat17071&iht=y&keys=keys&ks=960&list=n&qp=category_facet%3DPoint%20%26%20Shoot%20Cameras~abcat0401001&sc=Global&st=fuji%20film&type=page&usc=All%20Categories',
            },
        },
        'Smart Watch Accessories': {
            'Apple': {
                'url': 'https://www.bestbuy.com/site/apple-watch-device-accessories/apple-watch-accessories/pcmcat377500050002.c?id=pcmcat377500050002&qp=brand_facet%3DBrand~Apple',
            },
        },
        'Computer Accessories': {
            'Apple': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&id=pcat17071&iht=y&keys=keys&ks=960&list=n&qp=brand_facet%3DBrand~Apple&sc=Global&st=Apple%20computer%20accessories&type=page&usc=All%20Categories',
            },
            'Logitech': {
                'url': 'https://www.bestbuy.com/site/computers-pcs/computer-accessories/abcat0515000.c?id=abcat0515000&qp=brand_facet%3DBrand~Logitech',
                'filters': ['Category', ],
            },
        },
        'Kitchen Appliances': {
            'KitchenAid': {
                'url': 'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&browsedCategory=abcat0900000&id=pcat17071&iht=n&ks=960&list=y&qp=brand_facet%3DBrand~KitchenAid&sc=Global&st=categoryid%24abcat0900000&type=page&usc=All%20Categories',
                'filters': ['Category', ],
            },
        },
        'Home Network & Connectivity Equipment': {
            'NetGear': {
                'url': [
                    'https://www.bestbuy.com/site/routers/mesh-wi-fi/pcmcat748302046257.c?id=pcmcat748302046257&qp=brand_facet%3DBrand~NETGEAR%5Efeatures_facet%3DFeatures~Wi-Fi%20Mesh%20System',
                    'https://www.bestbuy.com/site/networking/routers/pcmcat211400050001.c?id=pcmcat211400050001&qp=brand_facet%3DBrand~NETGEAR&sp=-displaydate%20skuidsaas',
                    'https://www.bestbuy.com/site/networking/cable-dsl-modem-voip/abcat0503013.c?id=abcat0503013&qp=brand_facet%3DBrand~NETGEAR',
                    'https://www.bestbuy.com/site/searchpage.jsp?_dyncharset=UTF-8&browsedCategory=pcmcat161100050044&id=pcat17071&iht=n&ks=960&list=y&qp=brand_facet%3DBrand~NETGEAR&sc=Global&st=categoryid%24pcmcat161100050044&type=page&usc=All%20Categories',
                    'https://www.bestbuy.com/site/networking/hubs-switches/abcat0503012.c?id=abcat0503012&qp=brand_facet%3DBrand~NETGEAR',
                    'https://www.bestbuy.com/site/promo/netgear-other-networking-devices',
                ]
            },
        },
        'VR Headsets': {
            'Oculus': {
                'url': [
                    'https://www.bestbuy.com/site/virtual-reality/stand-alone-virtual-reality/pcmcat1520524211090.c?id=pcmcat1520524211090&qp=brand_facet%3DBrand~Oculus%5Ebrand_facet%3DBrand~Oculus%20Go',
                    'https://www.bestbuy.com/site/virtual-reality-devices-and-games/virtual-reality-headsets-for-pc/pcmcat1476726957734.c?id=pcmcat1476726957734&qp=brand_facet%3DBrand~Oculus',
                ],
            },
        },
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return
        super(BestbuyLiveCrawlSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        if isinstance(self.url_dict.get(category, {}).get(brand, ''), dict):
            self.launch_url =self.url_dict.get(category,{}).get(brand,{}).get("url", '')
            self.filters = self.url_dict.get(category,{}).get(brand, {}).get('filters', None)
        else:
            self.launch_url = self.url_dict.get(category,{}).get(brand, '')

        self.schema_keys = self.url_dict.get(category, {}).get(brand, {}).get('schema_keys', []) \
            or self.url_dict.get(category, {}).get('schema_keys', []) \
            or self.schema_keys \
            or []
        # if not self.schema_keys:
        #     self.schema_keys = self.url_dict.get(category, {}).get('schema_keys', [])
        #     if not self.schema_keys:
        #         self.schema_keys = self.url_dict.get(category, {}).get(brand, {}).get('schema_keys', [])

        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info(f"Crawling for category {category} and brand {brand},url:{self.launch_url}")

    def contracts_mock_function(self):
        self.filters = ['Material']

    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        
        if self.launch_url:
            urls = self.launch_url if isinstance(self.launch_url, list) else [self.launch_url]
            for url in urls:
                yield self.createRequest(url=url, callback=self.parse)
                if self.filters:
                    yield self.createRequest(url=url, callback=self.parse_filters, dont_filter=True)


    def update_extra_item_infos(self, id, extra_item_info): 
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse_filters(self, response):
        """
        @url https://www.bestbuy.com/site/mobile-phone-accessories/cases-covers-clips/abcat0811006.c?id=abcat0811006&qp=brand_facet%3DBrand~Apple
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        for filter in self.filters:
            for facet in response.xpath(f'//div[@class="facet-section"]/section[@class="lv facet"]/fieldset[contains(@name, "{filter}")]/ul/li'):
                filter_name = facet.xpath('.//span/a//text()').extract_first() \
                    or facet.xpath('.//a//text()').extract_first()
                filter_url = facet.xpath('.//span/a/@href').extract_first() \
                    or facet.xpath('.//a/@href').extract_first()
                if filter_name and filter_url:
                    request = self.createRequest(url=filter_url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                        filter: filter_name,
                    }
                    yield request


    def parse(self, response):
        """
        @url https://www.bestbuy.com/site/mobile-phone-accessories/cases-covers-clips/abcat0811006.c?id=abcat0811006&qp=brand_facet%3DBrand~Apple
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        extra_item_info = response.meta.get('extra_item_info', {})
        for product in response.xpath('//div[@class="shop-sku-list-item"]'):
            url_key = product.xpath('.//a/@href').extract_first(default='') or ''
            if url_key:
                url = 'https://www.bestbuy.com' + url_key
                variant_urls = product.xpath('.//div[@data-cy="product-variation-item"]//a/@href').extract()
                variant_urls.append(url)
                variant_urls = list(set(variant_urls))
                for variant_url in variant_urls:
                    if not self.extra_item_infos.get(variant_url, None):
                        self.extra_item_infos[variant_url] = {}

                    # self.extra_item_infos.get(id).update(extra_item_info)
                    self.update_extra_item_infos(variant_url, extra_item_info)
                    yield self.createRequest(url=variant_url, callback=self.crawlDetail, meta={'extra_item_info': response.meta.get('extra_item_info', {})})

        next_page = response.xpath('//a[contains(@class, "ficon-caret-right trans-button ")]/@href').extract_first(default='')\
            or response.xpath('//a[contains(@class, "sku-list-page-next")]/@href').extract_first(default='')

        if next_page:
            request = self.createRequest(url=next_page, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request
        
        
        # changes made for USDE-1608 [ computer cccessories - logitech]
        # if next_page:
        #     current_page = response.xpath('//ol[@class="paging-list"]//li//span[contains(@class, "current-page-number")]/text()').get(default = '')
        #     if re.findall(r"cp=\s*\%*2*0*\d+", response.url):
        #         next_url = re.sub(r"(cp=)\%*2*0*\s*\d+", r"\1 {}".format(str(int(current_page)+1)), response.url)
        #     else:
        #         next_url += "&cp={}".format(str(int(current_page)+1))
        #     request = self.createRequest(url=next_url, callback=self.parse)
        #     request.meta['extra_item_info'] = extra_item_info
        #     yield request


    def crawlDetailDynamic(self, response):
        """
        @url https://www.bestbuy.com/site/otterbox-symmetry-series-case-for-apple-iphone-11-clear/6361908.p?skuId=6361908&intl=nosplash
        @scrape_values title price was_price model sku id image breadcrumb item_condition description features bundle_includes specifications.Key\sSpecs.Brand\sCompatibility specifications.Key\sSpecs.Model\sCompatibility specifications.Key\sSpecs.Scratch\sResistant specifications.Key\sSpecs.Case\sStyle specifications.Key\sSpecs.Case\sType specifications.Key\sSpecs.Series specifications.General.Product\sName specifications.General.Brand specifications.General.Case\sStyle specifications.General.Case\sType specifications.General.Model\sNumber specifications.General.Series specifications.General.Color specifications.General.Color\sCategory specifications.Compatibility.Brand\sCompatibility specifications.Compatibility.Model\sCompatibility specifications.Performance.Scratch\sResistant specifications.Dimension.Product\sHeight specifications.Dimension.Product\sWidth specifications.Dimension.Product\sWeight specifications.Dimension.Product\sDepth specifications.Other.UPC

        @meta {"use_selenium":"True","perform_action":[{"element_type": "By.XPATH","element_path": "//button[@data-track='Specifications: Accordion Open']","type": "click",},{"element_type": "By.XPATH","element_path": "//button[@data-track='Overview: Accordion Open']","type": "click"}]}         
        """
        

        yield get_dict(response.text, self.category, self.brand)


    def crawlDetail(self, response):
        """
        @url https://www.bestbuy.com/site/hp-2-in-1-14-touch-screen-chromebook-intel-celeron-4gb-memory-32gb-emmc-flash-memory-ceramic-white/6367729.p?skuId=6367729
        @meta {"use_proxy":"True"}
        @returns requests 1
        """
        item = get_dict(response.text, self.category, self.brand)

        variants = self.url_dict.get(self.category, {}).get(self.brand, {}).get('detail_variants', [])
        if variants:
            script = response.xpath('//div[contains(@id, "shop-product-variations")]/script[contains(text(), "initializer.initializeComponent(")]/text()').extract_first()
            j = json.loads(', '.join([x.strip('"') for x in re.findall(r"initializer.initializeComponent\((.*?)\);", script)[0].split(", ")][2:-1]).replace('\\"', '"').replace('\\\\"', 'inches'))

            for categories in j.get('categories', []):
                if categories.get('displayName', '') in variants:
                    for variation in categories.get('variations', []):
                        variant_url = j.get('skuDetails', {}).get(variation.get('selectionSku', ''), {}).get('seoPdpUrl', '').replace('\\u002F', '/')
                        if variant_url:
                            print(variation.get('name', ''), variation.get('selectionSku', ''), variant_url)

                            if not self.extra_item_infos.get(variant_url, None):
                                self.extra_item_infos[variant_url] = {}

                            # self.extra_item_infos.get(id).update(extra_item_info)
                            self.update_extra_item_infos(variant_url, response.meta.get('extra_item_info', {}))
                            yield self.createRequest(url=variant_url, callback=self.crawlDetail)

        # should check for category-wise mandatory fields before instantiating dynamic crawl,
        # as Laptops & Netbooks needed 'features' information but many other categories may not needed
        is_item_has_all_needed_information = item.get('specifications', {}) and \
            not (self.schema_keys and 'features' in self.schema_keys and not item.get('features', ''))
        if is_item_has_all_needed_information:
            yield item
        else:
            yield self.createDynamicRequest(url=response.url+"&intl=nosplash",
                callback=self.crawlDetailDynamic, meta={"use_cache":True})


    def createDynamicRequest(self, url, callback, errback=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, 
            wait_time = 20,
            wait_until = EC.visibility_of_element_located((By.XPATH, '//button[contains(@class,"c-show-hide-trigger c-accordion-trigger")]')),
            perform_action = [
                {
                    'element_type': By.XPATH,
                    'element_path': '//button[@data-track="Specifications: Accordion Open"]',
                    'type': 'click',
                },
                {
                    'element_type': By.XPATH,
                    'element_path': '//button[@data-track="Overview: Accordion Open"]',
                    'type': 'click',
                }
            ]
        )
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]
        request.meta.update({"use_cache":True})
        return request


    def createRequest(self, url, callback, errback=None, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, errback=errback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({"use_cache":True})
        return request


