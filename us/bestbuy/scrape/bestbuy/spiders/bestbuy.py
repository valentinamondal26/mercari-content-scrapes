from google.cloud import storage
from parsel import Selector
import json
from collections import OrderedDict 
import datetime
import time
import re
import argparse
import sys
import os
import errno
import json
import gcsfs
import logging

from multiprocessing import Process
from multiprocessing import Pool
import multiprocessing


category=''
sub_category=''
output_path=''
start_time_str=''

url_dict = {
    'Video Game Consoles' : {
        'Xbox One':'Xbox_One_Consoles',
        'PlayStation 4':'PlayStation_4',
        'Nintendo Switch':'Nintendo_Switch_Consoles',
    },
    'Laptops & Netbooks' : {
        "All": '2-in-1s,PC_Laptops,MacBooks,Chromebooks,Gaming_Laptops',
    },
    'Cell Phones & Smartphones':{
        'Unlocked Cellphone':'All_Unlocked_Cell_Phones',
        'Cellphone with Plan':'All_Cell_Phones_with_Plans',
    },
    'Digital Cameras': {
        'Point & Shoot Cameras': "Point_&_Shoot_Cameras,Kids'Cameras,Instant_Print_Cameras,Premium_Point_&_Shoot",
        'Mirrorless Camera': 'Mirrorless_Cameras',
        'DSLR Camera': 'All_DSLRs,DSLR_Body_&_Lens,DSLR_Body_Only'
    },
    'Bluetooth Headphones' : {
        'All': 'All_Headphones',
    },
    'Calculators': {
        'All': 'Calculators',
    },
        'Home Surveillance Systems':{
        'Ring':'Security_Lighting,Home_Security_Accessories,Smart_Doorbells,Security_Cameras_&_Surveillance,Security_Camera_&_System_Accessories,Security_Cameras'
    },
    'Streaming Devices':{
        'Amazon':'Streaming_Media_Players,TV_Antennas'
    },
    'Video Game Accessories':{
        'Nintendo':'Nintendo_Switch_Batteries_&_Chargers,Nintendo_Switch_Cases_&_Skins,Nintendo_Switch_Controllers,Nintendo_Switch_Headsets'
    }
}


def remove_html_tags(text):
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)
        

def get_dict(response, category, sub_category):

    schema={}
    select=Selector(response)
    #Common attributes
    schema["crawlDate"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    schema["statusCode"]=str(200)
    schema['title']=select.xpath('//div[@class="sku-title"]/h1//text()').get(default='')
    price=select.xpath('//div[@class="priceView-hero-price priceView-customer-price"]/span[1]/text()').getall()
    schema["price"]=''.join(price)
    schema['was_price'] = select.xpath('//div[@class="pricing-price__regular-price"]/text()').extract_first(default='').replace('Was ', '')
    schema['model']=select.xpath('//div[@class="model product-data"]/span[2]/text()').get(default='')
    schema['sku']=select.xpath('//div[@class="sku product-data"]/span[2]/text()').get(default='')
    schema['id']=select.xpath('//head//meta[@property="og:url"]/@content').get(default="")
    schema['image']=select.css('img.primary-image::attr(src)').get(default='')
    breadcrumb=select.xpath('//div[@class="shop-breadcrumb"]//ol//text()').getall()
    schema['breadcrumb']='|'.join(breadcrumb)
    schema['category']=category
    schema['sub_category']=sub_category
    condition=select.xpath('//div[@class="open-box-option"]//span/text()').get(default='')
    if condition=='':
        if "Refurbished" in schema['title']:
            condition='Refurbished'
    else:
        condition='Open-Box'
    if condition=='':
        condition='New'
    schema['item_condition']=condition

    description=select.xpath('//div[contains(@class,"long-description-container")]//text()').getall()
    schema['description']=''.join(description)
    if schema['description']=='':
        dic=select.xpath('//div[@class="shop-product-movie-schema"]/script/text()').get(default='')
        if dic!='':
            dic=json.loads(dic)
            description=dic.get('description','')
            schema['description']=description
    #print(schema['description'],"\n\n\nSCHEMADESC\n\n\n")
       
    
    # features=select.xpath('//div[@class="features-list-container"]//text()').getall()
    # schema['features']=''.join(features)
    features = {}
    for feature in select.xpath('//div[@class="features-list-container"]//div[@class="list-row"]'):
        name = feature.xpath('./h4/text()').extract_first()
        value = feature.xpath('./p/text()').extract_first()
        features[name] = value
    schema['features'] = features

    if category!="Laptops":
        bundle_includes=select.css('div.list-container.list-lv li::text').getall()
        bundle_includes=','.join(bundle_includes)
        schema['bundle_includes']=bundle_includes


    schema['specifications']=''
    specifications=OrderedDict()

    spec_categories=select.xpath('//div[@class="spec-categories "]//h3/text()').getall()
    specifications=specifications.fromkeys(spec_categories,{})
    ctr=0
    uls=select.xpath('//div[@class="spec-categories "]//ul')
    for ul in uls:
        categories=spec_categories[ctr]
        lis=ul.css('li')
        keys=[]
        values=[]
        for l in lis:
            key=''.join(l.xpath('.//span/span/text()').getall())
            key=remove_html_tags(key)
            if key=='':
                key=''.join(l.xpath('div/div/text()').getall() or l.xpath('div/div/div/span/text()').getall())
                key=key.strip()
                key=remove_html_tags(key)
            value=l.xpath('div[2]/text()').get()
            value=remove_html_tags(value)
            keys.append(key)
            values.append(value)
        specifications[categories]=specifications[categories].fromkeys(keys,'')
        for i in range(len(keys)):
            if specifications[categories][keys[i]]=='':
                specifications[categories][keys[i]]=values[i]
        ctr+=1
    schema["specifications"]=specifications
    return schema


def get_json(response, category, sub_category):
    return json.dumps(get_dict(response, category, sub_category))


gcs_paths = [
    # 'raw/best_buy/1/html/{folder_name}',
    # 'raw/best_buy/2/html/{folder_name}',
    'raw/best_buy/2019-10-08_04-48-03/html/{folder_name}'
    ]

def yield_json(foldername, category, sub_category):
    client = storage.Client()
    bucket = client.get_bucket('imaginea_content_us')

    global gcs_paths

    for gcs_path in gcs_paths:
        # gcs_path = 'raw/best_buy/{}/html/{}'.format(num_folder,foldername)
        gcs_folderpath = gcs_path.format(folder_name=foldername)
        for blob in bucket.list_blobs(prefix=gcs_folderpath):
            text=blob.download_as_string().decode("utf-8")
            if text:
                yield json.loads(get_json(text, category, sub_category))

        # num_folder=num_folder+1
        logging.info('{} Completed'.format(gcs_folderpath))


def main(foldername):
    global output_path
    global category
    global sub_category
    global start_time_str

    output_file = output_path + foldername + '.jl'
    with open(output_file,'w') as f:
        for js in yield_json(foldername, category, sub_category):
            json.dump(js, f)
            f.write("\n")
        f.flush()
        f.close()

        # upload output file to gcs
        client = storage.Client()
        bucket_name = 'imaginea_content_us'
        bucket = client.get_bucket(bucket_name)
        destination_blob_name= 'raw/bestbuy.com/{category}/{sub_category}/sitemap_crawl/{timestamp}/json/{filename}.txt'.format(
            category=category, sub_category=(sub_category+'/') if sub_category else '', timestamp=start_time_str, filename=foldername
        ).replace(" ", "_").lower()
        blob = bucket.blob(destination_blob_name)
        blob.upload_from_filename(output_file)
        logging.info('Output Uploaded to: gs://{bucket}/{path}'.format(bucket=bucket_name, path=destination_blob_name))

        # remove the output file in local
        os.remove(output_file)



def get_folder_name_list(category, sub_category):
    folder_list = set()
    if category == 'All':
        for folder_num in [1, 2]:
            gcs = gcsfs.GCSFileSystem(project='mercari-us-imaginea-dev')
            files = gcs.ls('gs://imaginea_content_us/raw/best_buy/{}/html'.format(folder_num))
            folder_list.update([re.sub(re.compile(r'imaginea_content_us/raw/best_buy/\d+/html/'), '', folder_name)
                .replace('/', '') for folder_name in files])
    else:
        folder_names = url_dict.get(category, {}).get(sub_category, '')
        folder_list.update(folder_names.split(','))

    return folder_list


def execute(folder_list):
    process_count = 20
    with Pool(processes=process_count) as pool:
        pool.map(main, folder_list)


def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


# logging intializer
def log_setup(filename):
    log_handler = logging.FileHandler(filename)
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)03d [pid-%(process)d] [%(levelname)-5.5s]: %(message)s'.format(),
        '%Y-%m-%d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(log_handler)
    root_logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--category', required=True, help='provide category')
    parser.add_argument('--sub_category', required=False, help='provide  sub_category', default='')
    # parser.add_argument('--output', required=True, help='provide  JsonLine filename')
    args = parser.parse_args()
    category=args.category
    sub_category=args.sub_category

    start_time = datetime.datetime.now()
    start_time_str = start_time.strftime("%Y-%m-%d-%H_%M_%S")
    output_path = 'out/{timestamp}/'.format(timestamp=start_time_str)
    make_sure_path_exists(output_path)

    log_setup(output_path + 'log.txt')

    logging.info('Output Path: {}'.format(output_path))


    folder_list = get_folder_name_list(category, sub_category)
    execute(folder_list)

    logging.info('Output Path: {}'.format(output_path))
    logging.info('Total Time taken for scraping: {}'.format((datetime.datetime.now() - start_time)))

