import requests
import traceback
from collections import defaultdict
from requests import Timeout
import urllib


class Mapper(object):

    def __init__(self):
        self.url = "https://us-central1-mercari-us-de.cloudfunctions.net/category_ner?cat_id=771&query={}"
        return

    def map(self, record):
        try:
            description = record['ner_query']
            description = urllib.parse.quote(description)
            url = self.url.format(description)
            try:

                data_dict = defaultdict(list)
                entities_list = list()
                with requests.get(url, timeout=60) as r:
                    if r.status_code == requests.codes.ok:
                        res = r.json()
                        entities = res['entities']
                        for label in entities:
                            data_dict[(label['label']).lower()].append(label['normalized'])
                        for k, v in data_dict.items():
                            entities_list.append({k: v})

                    else:
                        print("FAILED: Status_code {0} - {1} ".format(r.status_code, url))
                    record['auto_entities'] = entities_list
                    record.pop("ner_query", None)
            except Timeout:
                print("FAILED: Request Timed out - {}".format(url))
                print(traceback.format_exc())

        except Exception as e:
            print(" Exception Raised for {} with exception {}".format(record, str(e)))
            print(traceback.format_exc())
            return None
        return record
