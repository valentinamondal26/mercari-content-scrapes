import hashlib
from colour import Color
import re
import difflib

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()


            specifications=record.get('specifications',{})
            pattern=""
            price=record.get('price','')
            price=price.split('$')[1]
            price=price.replace(",","")
            
            model=specifications.get('General',{}).get('Product Name','')
            
            pattern=re.compile(r"Bluetooth|Speakers*|®|\+|\-|portable",re.IGNORECASE)
            model=re.sub(pattern,'',model)
                
                
            wordList = re.sub("[^\w]", " ",  model).split()
            _color = [i.strip(' ,') for i in wordList if self.check_color(i.strip(' ,'))]
            color=''
            for color in _color:
                if color:
                    model = model.replace(color, '')
            model=re.sub(re.compile(r"a*n*d*\s*with\s*Google\s*Assistant|\(\s*each\s*\)",re.IGNORECASE),'',model)
                    
            model=' '.join(model.split())


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "Bluetooth Speakers",
                "image": record.get('image', ''),
                'brand':specifications.get('General',{}).get('Brand','Oculus'),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),                
                "features": record.get('features',''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "model": model,
                "title":specifications.get('General',{}).get('Product Name',''),
                "charger_type":specifications.get('Key Specs',{}).get('Rechargeable',specifications.get('Power',{}).get('Rechargeable','')),
                "battery_life":specifications.get('Key Specs',{}).get('Battery Life',specifications.get('Power',{}).get('Battery Life','')),
                "usb_specification":specifications.get('Key Specs',{}).get('USB Device Charging',specifications.get('Power',{}).get('USB Device Charging','')),
                "compatibility":specifications.get('General',{}).get('Compatibility',''),
                "model_sku":specifications.get('General',{}).get('Model Number',''),
                "upc":specifications.get('Other',{}).get('UPC',''),
                "color":specifications.get('General',{}).get('Color',''), 
                "speakers_included":specifications.get('Audio',{}).get('Number Of Speakers',''),
                "snr":specifications.get('Audio',{}).get('Signal-to-Noise Ratio (SNR)',''),
                "minimum_frequency":specifications.get('Audio',{}).get('Minimum Frequency Response',''), 
                "maximum_frequency":specifications.get('Audio',{}).get('Maximum Frequency Response',''),
                "water_resistant":specifications.get('Features',{}).get('Water Resistant',''),
                "display":specifications.get('Features',{}).get('Digital Display',''),
                "charge_time":specifications.get('Power',{}).get('Battery Charge Time',''),
                "subwoofer_woofer_size":record.get('Subwoofer Size',''),
                "height":specifications.get('Dimension',{}).get('Product Height',''),
                "width":specifications.get('Dimension',{}).get('Product Width',''),
                "depth":specifications.get('Dimension',{}).get('Product Depth',''),
                "weight":specifications.get('Dimension',{}).get('Product Weight',''),
                "item_condition":record.get('item_condition','')
                
            }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

