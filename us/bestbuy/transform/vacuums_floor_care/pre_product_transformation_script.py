import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            brand = specifications.get('General',{}).get('Brand', '')

            model = specifications.get('General',{}).get('Product Name','')

            vacuum_cleaner_type = ''
            corded_cordless = specifications.get('Key Specs',{}).get('Corded/Cordless', '') or ''

            upright_string="upright"
            if(upright_string in model.lower()):
                model = re.sub(re.compile(r'Upright Vacuum|Bagged|Bagless', re.IGNORECASE), '', model)
                model = re.sub(re.compile(color, re.IGNORECASE), '', model)
                model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
                model = re.sub('-', ' ', model).strip()

                vacuum_cleaner_type ='Upright Vacuums'
            elif corded_cordless.lower() == 'cordless':
                model = re.sub(re.compile(r'Cord-Free|Stick|Cordless Hand Vac|Bagged|Bagless|Cordless|Vacuum|vac', re.IGNORECASE), '', model)
                model = re.sub(re.compile(color, re.IGNORECASE), '', model)
                model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
                model = re.sub(' +', ' ', model).strip()

                vacuum_cleaner_type = 'Cordless Vacuums'
            elif corded_cordless.lower() == 'corded':
                model = re.sub(re.compile(r'Canister Vacuum|Bagged|Bagless', re.IGNORECASE), '', model)
                model = re.sub(re.compile(color, re.IGNORECASE), '', model)
                model = re.sub(re.compile(brand, re.IGNORECASE), '', model)
                model = re.sub(' +', ' ', model).strip()

                vacuum_cleaner_type = 'Canister Vacuums'

            bagged_bagless = specifications.get('Key Specs',{}).get('Bagless', '')
            if bagged_bagless.lower() == 'Yes'.lower():
                bagged_bagless = 'Bagless'
            elif bagged_bagless.lower() == 'No'.lower():
                bagged_bagless = 'Bagged'
            else:
                bagged_bagless = ''

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "color": color,
                "color_category": color_category,
                "brand": brand,
                "model_sku": specifications.get('General',{}).get('Model Number',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width',''),
                "length": specifications.get('Dimension',{}).get('Product Length',''),
                "weight": specifications.get('Key Specs',{}).get('Product Weight',''),
                'suction_hose_length': specifications.get('Dimension',{}).get('Hose Length',''),
                "upc": specifications.get('Other',{}).get('UPC',''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                'product_line': specifications.get('General',{}).get('Series', ''),
                'pet': specifications.get('Key Specs',{}).get('Pet', ''),
                # 'vacuum_cleaner_type': specifications.get('Key Specs',{}).get('Vacuum Type', ''),
                'vacuum_cleaner_type': vacuum_cleaner_type,
                'vacuum_capacity': specifications.get('Key Specs',{}).get('Bin Capacity', ''),
                'runtime': specifications.get('Key Specs',{}).get('Maximum Runtime', ''),
                'bagged_bagless': bagged_bagless,
                'corded_cordless': corded_cordless,
                'flooring_type': specifications.get('Key Specs',{}).get('Compatible Floor Type', ''),
                'cord_length': specifications.get('Key Specs',{}).get('Cord Length', ''),
                'filter_type': specifications.get('Key Specs',{}).get('Filter Type', ''),
                'attachments_included': specifications.get('Key Specs',{}).get('Attachments Included', ''),
            }

            return transformed_record
        else:
            return None
