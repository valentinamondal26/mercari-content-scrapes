import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            title = record.get('title', '')
            ner_query = record.get('description', title)
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications = record.get('specifications', {})

            # color transformations
            color = specifications.get('Other', {}).get('Color', '') or ''
            if not color:
                color = specifications.get(
                        'General', {}).get('Color', '') or ''

            if not color:
                color = specifications.get(
                        'Other', {}).get('Color Category', '') or ''
            if not color:
                color = specifications.get(
                        'General', {}).get('Color Category', '') or ''
            to_remove = ['Printshield Stainless',
                         r'Monochromatic Stainless\-*\s*Steel',
                         r'Polished Stainless\s*\-*Steel',
                         r'\/Stainless\-*\s*Steel',
                         r'stainless\-*\s*steel',
                         r'Custom Panel\s*\-*Ready',
                         r'Panel\-*\s*Ready',
                         'With PrintShield Finish',
                         'Stainless']
            for words in to_remove:
                color = re.sub(re.compile(r"{}".format(words), re.IGNORECASE),
                               "", color).strip()

            if not color:
                color = specifications.get(
                        'Other', {}).get('Color Category', '') \
                        or specifications.get('General', {}).get(
                        'Color Category', '')
            color = re.sub(re.compile(r"Multi\-*\s*color|\bmulti\b",
                           re.IGNORECASE), "Multicolor", color).strip()
            # brand , model sku, volume extraction
            brand = specifications.get('General', {}).get('Brand', '') \
                or specifications.get('Other', {}).get('Brand', '') \
                or record.get('sub_category', '')

            model_sku = specifications.get('General', {}).get('Model Number',
                                                              '') \
                or specifications.get('Other', {}).get('Model Number', '')

            volume = specifications.get('Key Specs', {}).get('Capacity', '') \
                or specifications.get('Capacity', {}).get('Capacity', '') \
                or specifications.get('Other', {}).get('Capacity', '') \
                or specifications.get('Key Specs', {}).get('Bowl Capacity', '')\
                or specifications.get('Capacity', {}).get('Oven Capacity', '')

            # model transformations
            model = specifications.get('General', {}).get(
                    'Product Name', '') or specifications.get('Other', {}).get(
                    'Product Name', '')
            keywords = [brand]
            if re.findall(re.compile(r"Stainless\-*\s*steel", re.IGNORECASE),
                          title):
                if re.findall(
                        re.compile(r"Stainless\-*\s*steel", re.IGNORECASE),
                        model):
                    model = re.sub(re.compile(
                            r"(.*)(\s*Stainless\-*\s*steel)(.*)",
                            re.IGNORECASE), r"\2 \1 \3", model)
                else:
                    model = re.findall(re.compile(
                            r"Stainless\-*\s*steel", re.IGNORECASE),
                            title)[0] + " " + model
            elif not (
                    re.findall(re.compile(r"Stainless Steel With PrintShield "
                                          r"Finish|Stainless With PrintShield "
                                          r"Finish", re.IGNORECASE), color)
                    and re.findall(re.compile(r"Stainless Steel With "
                                              r"PrintShield Finish|Stainless "
                                              r"With PrintShield Finish",
                                   re.IGNORECASE), model)) \
                    and not re.findall(re.compile(r"Stainless\-*\s*steel",
                                       re.IGNORECASE), color):
                keywords.append(color)
            if re.findall(re.compile(r"Stainless Steel With PrintShield Finish|"
                                     r"Stainless With PrintShield Finish",
                          re.IGNORECASE), title) \
                    and not re.findall(re.compile(r"Stainless Steel With "
                                                  r"PrintShield Finish|"
                                                  r"Stainless With PrintShield "
                                                  r"Finish", re.IGNORECASE),
                                       model):
                model = model + " - " + re.findall(
                        re.compile(r"Stainless Steel With PrintShield Finish|"
                                   r"Stainless With PrintShield Finish",
                                   re.IGNORECASE), title)[0]

            keywords.append(model_sku)
            model = re.sub(re.compile(r'|'.join(
                    list(map(lambda x: r'\b' + x + r'\b', keywords)))),
                           '', model)
            model = re.sub(r'-\s*$|®|\(|\)|\s\-\s|\s-$|™', ' ', model)
            model = re.sub(
                    r'\d+\.*\d*-*\s*(Cup|quart|Qt|Cu\.\s*Ft\.)\s*\,*', '',
                    model, flags=re.IGNORECASE)
            model = " ".join([m[0].upper() + m[1:] for m in model.split()])
            model = re.sub(re.compile(r"(\d+\.*\d*)\-*Lb\.",
                           re.IGNORECASE), r"\1 lb.", model)
            model = re.sub(re.compile(r"\-Oz.", re.IGNORECASE), " oz.", model)
            model = re.sub(re.compile(
                    r"\d+\-\s*\d+\/*\d*", re.IGNORECASE), "", model)
            model = re.sub(re.compile(r"Convection Toaster/Pizza Oven",
                           re.IGNORECASE), "Convection Toaster Pizza Oven",
                           model)
            model = re.sub(re.compile(
                    r"Professional 500 450 Watt Bowl Lift Stand Mixer",
                    re.IGNORECASE),
                    "Professional 500 Series 450 Watt Bowl Lift Stand Mixer",
                    model)
            model = re.sub(r'\s+', ' ', model).strip()
            to_remove = [r'Dual\-*\s*Fuel True Convection Range With '
                         r'Self\-*\s*Cleaning',
                         r'With Self\-*\s*Cleaning And Griddle',
                         r'With Self\-*\s*Cleaning',
                         'KP26M1XNP',
                         'Freestanding'
                         ]
            for words in to_remove:
                model = re.sub(re.compile(r"{}".format(words), re.IGNORECASE),
                               "", model)

            model = re.sub(re.compile(r"\bWith Tub.*", re.IGNORECASE), "",
                           model)
            model = re.sub(re.compile(r"\bStainless\-Steel\b", re.IGNORECASE),
                           "Stainless Steel", model)
            if re.findall(re.compile(r"\bArtisan\b", re.IGNORECASE), model) \
               and not re.compile(r"^Artisan.*$", re.IGNORECASE).match(model):
                model = re.sub(re.compile(r"(.*)(\bArtisan\b)(.*)",
                               re.IGNORECASE), r"\2 \1 \3", model)

            model = re.sub(re.compile(r"(.*)(Stainless Steel)(.*)"
                                      r"(Stainless Steel)(.*)",
                                      re.IGNORECASE), r"\1\2\3 \5", model)
            model = re.sub(r'\s+', ' ', model).strip()
            if not model:
                d = {
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Imperial Black': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Passion Red': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Yellow Pepper': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Ink Blue': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Avocado Cream': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Scorched Orange': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 5.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Milkshake': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 4.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Imperial Black': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 4.1 Cu. Ft. Freestanding Dual-Fuel True Convection Range with Self-Cleaning - Misty Blue': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 4.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Passion Red': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 4.1 Cu. Ft. Freestanding Dual-Fuel True Convection Range with Self-Cleaning - Yellow Pepper': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 4.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Ink Blue': 'Freestanding Dual Fuel True Convection Range',
                    'KitchenAid - 4.1 Cu. Ft. Freestanding Dual Fuel True Convection Range with Self-Cleaning - Avocado Cream': 'Freestanding Dual Fuel True Convection Range',
                }
                for key, value in d.items():
                    if re.findall(key, title, flags=re.IGNORECASE):
                        model = value
                        break

            model = re.sub('Flex-Edge', 'Flex Edge', model, flags=re.IGNORECASE)


            # color string fox after model transformations
            if re.findall(re.compile(
                    r"Stainless Steel With PrintShield Finish|"
                    r"Stainless With PrintShield Finish",
                    re.IGNORECASE), model):
                color = re.sub(re.compile(
                        r"Stainless Steel With PrintShield Finish|"
                        r"Stainless With PrintShield Finish",
                        re.IGNORECASE), "", color)
                color = " ".join(color.split())

            # product type extraction from the breadcrumb
            product_types = record.get('Category', '')
            product_type = ', '.join(product_types)
            product_type_meta = ['Dishwashers', 'Refrigerators', 'Microwaves',
                                 r'Ranges\, Cooktops \& Ovens',
                                 r'Freezers \& Ice Makers',
                                 r'Appliance Parts \& Accessories',
                                 'Small Kitchen Appliances']
            if not product_type:
                if re.findall(
                        re.compile(r"|".join(product_type_meta), re.IGNORECASE),
                        record.get("breadcrumb", "")):
                    product_type = re.findall(re.compile(
                            r"|".join(product_type_meta), re.IGNORECASE),
                            record.get("breadcrumb", ""))[0]

            old_product_type = product_type
            product_types = product_type.split(", ")
            found = False
            for prod_type in product_types:
                pattern = prod_type
                if re.compile(r"^.*s$").match(pattern):
                    pattern += '*'
                if re.findall(re.compile(r"{}".format(pattern),
                              re.IGNORECASE), title):
                    found = True
                    product_type = prod_type
                    break
            if not found:
                product_type = product_types[0]

            if re.findall(re.compile(r'|'.join(list(
                    map(lambda x: r'\b' + x + r'\b', ['Bundle', 'Package']))),
                    re.IGNORECASE), model):
                return None

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),

                    "category": record.get('category', ''),
                    "brand": record.get('brand', '') or brand,

                    "image": record.get('image', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "description": record.get('description', ''),

                    "ner_query": ner_query,
                    "title": title,
                    "model": model,
                    'product_type': product_type,
                    # 'old_product_type': old_product_type,
                    "model_sku": model_sku,
                    "color": color,

                    'wattage': specifications.get('Key Specs', {}).get(
                            'Wattage', '') or specifications.get(
                            'Electrical Specification', {}).get('Wattage', ''),
                    'maximum_voltage_output': specifications.get(
                            'Power', {}).get('Voltage', '')
                    or specifications.get('Electrical Specification',
                                          {}).get('Voltage', '')
                    or specifications.get('Feature', {}).get('Voltage', ''),
                    'volume': volume,

                    "height": specifications.get('Dimension', {}).get(
                            'Product Height', ''),
                    "width": specifications.get('Dimension', {}).get(
                            'Product Width', ''),
                    "length": specifications.get('Dimension', {}).get(
                            'Product Length', ''),
                    "weight": specifications.get('Dimension', {}).get(
                            'Product Weight', ''),

                    "price": {
                            "currency_code": "USD",
                            "amount": price
                    },
                    "upc": specifications.get('Other', {}).get('UPC', ''),
            }

            return transformed_record
        else:
            return None
