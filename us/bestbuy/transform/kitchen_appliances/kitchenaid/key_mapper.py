#Key Mapper for Bestbuy data - model + color is used to de-dup items
class Mapper:
    def map(self, record):

        model = ''
        model_sku = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            elif "model_sku" == entity['name'] and entity["attribute"]:
                model_sku = entity["attribute"][0]["id"]
            elif "color" == entity['name'] and entity["attribute"]:
                color = entity["attribute"][0]["id"]

        key_field = model + model_sku + color
        return key_field
