import hashlib


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color Category', '') or ''

            camera_type = specifications.get("Feature",{}).get('Digital Camera Type', '') or ''
            if not camera_type:
                camera_type = record.get('sub_category', '') or ''

            if camera_type == 'Instant Print Camera' or camera_type == 'Point & Shoot Cameras':
                camera_type = 'Point and Shoot Cameras'

            model = specifications.get('Other',{}).get('Product Name','')
            if 'Pixpro' in model:
                model = model.replace('Pixpro', 'PIXPRO') #https://mercari.atlassian.net/browse/USDE-1275

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category":'Digital Cameras',
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model":model,
                "color": color,
                "brand":specifications.get('Other',{}).get('Brand',''),
                "upc":specifications.get('Other',{}).get('UPC',''),
                "width":specifications.get('Dimension',{}).get('Product Width',''),
                "height":specifications.get('Dimension',{}).get('Product Height',''),
                "depth":specifications.get('Dimension',{}).get('Product Depth',''),
                "weight":specifications.get('Dimension',{}).get('Product Weight',''),
                "screen_size":specifications.get('Display',{}).get('Screen Size',''),
                "megapixels":specifications.get('Key Specs',{}).get('Effective Megapixels',''),
                "sensor_type":specifications.get('Key Specs',{}).get('Image Sensor Type',''),
                "camera_iso_settings":specifications.get('Key Specs',{}).get('ISO Settings',''),
                "wifi":specifications.get('Key Specs',{}).get('Wi-Fi Enabled',''),
                "water_proof":specifications.get('Performance',{}).get('Water Resistant',''),
                "video_resolution":specifications.get("Key Specs",{}).get('Video Resolution',''),
                "display_type":specifications.get("Display",{}).get('Display Type',''),
                "touch_screen":specifications.get('Display',{}).get('Touch Screen',''),
                "display_resolution":specifications.get('Display',{}).get('Screen Resolution',''),
                "gps":specifications.get("Connectivity",{}).get('Integrated GPS',''),
                "tripod_monopod":specifications.get("Connectivity",{}).get('Tripod/Monopod Mountable',''),
                "camera_type": camera_type,
                "integrated_flash":specifications.get('Feature',{}).get('Integrated Flash',''),
                "slow_motion_recording":specifications.get('Feature',{}).get('Slow Motion Recording',''),
                "digital_zoom":specifications.get('Imaging',{}).get('Digital Zoom',''),
                "optical_zoom":specifications.get('Imaging',{}).get('Optical Zoom',''),
                "image_file_formats":specifications.get('Imaging',{}).get('Image File Format(s)',''),
                "camera_image_per_charge":specifications.get('Imaging',{}).get('Number Of Images Per Charge',''),
                "in_lens_image_stabilization":specifications.get('Imaging',{}).get('In-Lens Image Stabilization',''),
                "image_resolution":specifications.get('Imaging',{}).get('Image Resolution (Display)',''),
                "compatible_mount":specifications.get('Key Specs',{}).get('Lens Mount Compatibility',''),
                "model_sku":specifications.get('Other',{}).get('Model Number',''),
                "nfc":specifications.get('Connectivity',{}).get('NFC Enabled',''),
                "product_family":specifications.get('Compatibility',{}).get('Camera Model Family',''),
            }

            return transformed_record
        else:
            return None
