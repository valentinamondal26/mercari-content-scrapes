class Mapper:
    def map(self, record):
        model = ''
        color = ''
        storage_capacity = ''
        network = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "storage_capacity" == entity['name']:
                if entity["attribute"]:
                    storage_capacity = entity["attribute"][0]["id"]
            elif "network" == entity['name']:
                if entity["attribute"]:
                    network = entity["attribute"][0]["id"]


        key_field = model + color + storage_capacity + network
        return key_field
