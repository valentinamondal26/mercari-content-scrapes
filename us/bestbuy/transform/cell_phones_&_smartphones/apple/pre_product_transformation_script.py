import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            title = record.get('title', '')
            model = specifications.get('Compatibility',{}).get('Model Family','')
            brand = specifications.get('Other', {}).get('Brand', '') \
                or specifications.get('General', {}).get('Brand', '')
            storage_size = specifications.get('Key Specs',{}).get('Internal Memory','')
            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color Category', '') or ''

            color = re.sub(re.compile(r"™"), '', color)
            color = color.title()

            if not model:
                if brand.lower() not in ['razer']:
                    model = re.sub(re.compile(r"^{0}".format(brand)), '', title)
                else:
                    model = title
                pattern = re.compile(r"((with\s*){0,1}\d+(GB|MB)\s*(Memory){0,1}\s*){0,1}"
                    r"(Prepaid\s*){0,1}(Cell Phone\s*){0,1}((\(){0,1}Unlocked(\)){0,1}){0,1}"
                    r"(\s*Mobile Phone){0,1}(Smartphone){0,1}", re.IGNORECASE)
                model = re.sub(pattern, '', model)
                model = model.replace(color, '')

                model = re.sub(re.compile(r"LTE|4G|GSM|®|™|Apple|Google|Verizon(\s*Wireless){0,1}"
                    r"|T-Mobile|AT&T|Sprint|(Geek Squad Certified\s*){0,1}Refurbished"
                    r"|International Version|Branded|No-Contract|Pre-Owned|\(\s*\)"
                    r"|w\/ \$40 Net 10 bill credit upon activation", re.IGNORECASE), '', model)
                model = re.sub(re.compile(r'\(\W*\)'), '', model).strip()
                model = re.sub(r"^(-\s*)*-|(-\s*){1,}-|(-\s*)*-$", '', model.strip())
                model = re.sub(' +', ' ', model).strip()
            else:
                if brand.lower() not in ['razer']:
                    model = re.sub(re.compile(r"^{0}".format(brand)), '', model)
                model = re.sub(re.compile('Google', re.IGNORECASE), '', model)
                model = re.sub(r"^[-]|[-]$", '', model.strip())
                model = re.sub(' +', ' ', model).strip()

            if brand.lower() not in ['razer']:
                model = re.sub(' - ', ' ', model).strip()

            if brand.lower() != 'apple':
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category":'cell phones & smartphones',
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": title,
                "features": record.get('features',''),
                "ner_query": ner_query,
                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model or '',
                "storage_size": storage_size,
                "color": color,
                "network": specifications.get('Key Specs',{}).get('Carrier',''),
                "brand": brand,
                "upc": specifications.get('Other',{}).get('UPC',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "depth": specifications.get('Dimension',{}).get('Product Depth',''),
                "weight": specifications.get('Dimension',{}).get('Product Weight',''),
                "screen_size": specifications.get('Key Specs',{}).get('Screen Size',''),
                "front_camera_resolution": specifications.get('Key Specs',{}).get('Front-Facing Camera',''),
                "rear_camera_resolution": specifications.get('Key Specs',{}).get('Rear-Facing Camera',''),
                "display_type": specifications.get("Display",{}).get('Display Type',''),
                "display_resolution": specifications.get('Display',{}).get('Screen Resolution',''),
                "sim_card_size": specifications.get('Compatibility',{}).get('SIM Card Size',''),
                "charger_type": specifications.get('Compatibility',{}).get('Charging Interface(s)',''),
                "water_resistant": specifications.get('Key Specs',{}).get('Water Resistant',''),
                "ip_rating": specifications.get('Features',{}).get('Ingress Protection (IP) Rating',''),
                "touch_screen": specifications.get('Display',{}).get('Touch Screen',''),
                "wireless_charging": specifications.get('Features',{}).get('Wireless Charging',''),
                "headphone_jack": specifications.get('Features',{}).get('Headphone Jack','')
            }

            return transformed_record
        else:
            return None
