class Mapper:
    def map(self, record):
        model = ''
        color = ''
        storage_capacity = ''
        carrier = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "storage_capacity" == entity['name']:
                if entity["attribute"]:
                    storage_capacity = entity["attribute"][0]["id"]
            elif "carrier" == entity['name']:
                for attribute in entity["attribute"]:
                    carrier += attribute["id"]


        key_field = model + color + storage_capacity + carrier
        return key_field
