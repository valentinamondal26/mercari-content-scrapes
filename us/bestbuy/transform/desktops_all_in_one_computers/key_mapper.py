#Key Mapper for Bestbuy data - model +  storage_capacity is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""
        model = ''
        storage_capacity = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity.get("attribute", []):
                    model = entity.get("attribute", [{}])[0].get("id", '')
            if "storage_capacity" == entity['name']:
                if entity.get("attribute", []):
                    storage_capacity = entity.get("attribute", [{}])[0].get("id", '')

        key_field = model + storage_capacity
        return key_field
