import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            specifications=record.get('specifications',{})
            model = specifications.get('General',{}).get('Product Name','')
            x = re.sub("^([0-9]+.[0-9]*.)", "", model)
            x = re.sub("\\u00ae", "", x)
            x = re.sub("\(Latest Model\)", "", x)
            x = x.split(" -")
            x = x[0]
            model = re.sub("-", " ", x).strip()


            price = record['price']
            price = price.replace("$", "")

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "model": model,
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "description": record.get('description',''),
                "screen_size": specifications.get('Key Specs',{}).get('Screen Size','') ,
                "screen_resolution": specifications.get('Key Specs',{}).get('Screen Resolution','') ,
                "touch_screen": specifications.get('Key Specs',{}).get('Touch Screen','') ,
                "storage_type": specifications.get('Key Specs',{}).get('Storage Type','') ,
                "storage_capacity": specifications.get('Key Specs',{}).get('Total Storage Capacity',''),
                "ram": specifications.get('Key Specs',{}).get('System Memory (RAM)',''),
                "featured_graphics": specifications.get('Key Specs',{}).get('Graphics',''),
                "processor_model": specifications.get('Key Specs',{}).get('Processor Model',''),
                "brand": specifications.get('General',{}).get('Brand',''),
                "model_sku": specifications.get('General',{}).get('Model Number',''),
                "year_of_manufacture": specifications.get('General',{}).get('Year Introduced',''),
                "color":specifications.get('General',{}).get('Color',''),
                "size": specifications.get('General',{}).get('Size',''),
                "cooling_type": specifications.get('Feature',{}).get('Cooling System',''),
                "number_of_ports": specifications.get('Ports',{}).get('Number of USB ports (Total)','') ,
                "hdmi_input": specifications.get('Ports',{}).get('HDMI Input','') ,
                "graphics_processing_type": specifications.get('Graphics',{}).get('Graphics Type',''),
                "gpu_video_card": specifications.get('Graphics', {}).get('GPU Brand', ''),
                "gpu_video_memory": specifications.get('Graphics', {}).get('Video Memory', ''),
                "memory_type": specifications.get('Graphics', {}).get('Video Memory Type', ''),
                "camera_integration": specifications.get('Included',{}).get('Built-in Webcam','') ,
                "optical_drive_type": specifications.get('Included', {}).get('Optical Drive Type', ''),
                "height": specifications.get('Dimension', {}).get('Product Height', ''),
                "width": specifications.get('Dimension', {}).get('Product Width', ''),
                "depth": specifications.get('Dimension', {}).get('Product Depth', ''),
                "weight": specifications.get('Dimension', {}).get('Product Weight', ''),
                "upc": specifications.get('Other', {}).get('UPC', ''),
            }

            return transformed_record
        else:
            return None
