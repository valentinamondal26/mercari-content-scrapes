import hashlib
from colour import Color
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()


            specifications=record.get('specifications',{})
            price=record.get('price','')
            price = price.replace("$", "")
            price=''.join(price.split())
            price=price.replace(",","")
            
            connectivity=specifications.get('Connectivity',{}).get('Network Connectivity','')
            if connectivity=='':
                connectivity=specifications.get('Features',{}).get('Network Connectivity','')

            indoor_outdoor=specifications.get('Features',{}).get('Indoor Or Outdoor Use','')
            if indoor_outdoor=='':
                indoor_outdoor=specifications.get('Key Specs',{}).get('Indoor Or Outdoor Use','')
            power_source=specifications.get('Key Specs',{}).get('Power Source','')
            if power_source=='':
                power_source=specifications.get('Power',{}).get('Power Source','')

            model=specifications.get('General',{}).get('Product Name','')
            if model=='':
                model=specifications.get('Other',{}).get('Product Name','')




            # title=record.get('title','')
            # wordList = re.sub("[^\w]", " ",  title).split()

            # colour=''
            # _color = [i.strip(' ,') for i in wordList if self.check_color(i.strip(' ,'))]
            # color=''
            # for color in _color:
            #     if color:
            #         title = title.replace(color, '')


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "Home Surveillance Systems",
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),                
                "features": record.get('features',''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "color":specifications.get('General',{}).get('Color',''),
                "connectivity":connectivity,
                "mount_type":specifications.get('Features',{}).get('Mount Type',''),
                'indoor_outdoor':indoor_outdoor,
                'power_source':power_source,
                'number_of_cameras':specifications.get('Key Specs',{}).get('Number Of Cameras Included',''),
                'number_of_channels':specifications.get('Key Specs',{}).get('Number Of Channels',''),
                'wireless_standard':specifications.get('Key Specs',{}).get('Wireless Standard',''),
                'model_number':specifications.get('General',{}).get('Model Number',''),
                'storage_type':specifications.get('Key Specs',{}).get('Storage Type',''),
                'radio_type':specifications.get('Key Specs',{}).get('Audio Communication',''),
                'height':specifications.get('Dimension',{}).get('Product Height',''),
                'width':specifications.get('Dimension',{}).get('Product Width',''),
                'depth':specifications.get('Dimension',{}).get('Product Depth',''),
                'weight':specifications.get('Dimension',{}).get('Product Weight',''),
                "model": model,
                "itemIds": {
                    "UPC": [specifications.get('Other',{}).get('UPC','')],
                },
                'brand':specifications.get('General',{}).get('Brand','')
            }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

