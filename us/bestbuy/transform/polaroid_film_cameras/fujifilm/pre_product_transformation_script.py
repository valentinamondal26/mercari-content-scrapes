import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('was_price', '')
            if not price:
                price = record.get('price', '')
            price = price.replace("$", "")

            specifications = record.get('specifications', {})

            color_category = specifications.get('General', {}).get('Color Category', '')
            color = specifications.get('General', {}).get('Color', '') or color_category

            model = specifications.get('General', {}).get('Product Name', '')
            model = " ".join([m[0].upper()+m[1:] for m in model.split()])
            model = re.sub(r'\s+', ' ', model).strip()
            if re.findall(re.compile(r"\bbundle\b", re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": specifications.get('General', {}).get('Brand', ''),

                "description": record.get('description', ''),

                "model": model,
                "model_sku": specifications.get('General', {}).get('Model Number', ''),
                "color": color,

                "width": specifications.get('Dimension', {}).get('Product Width', ''),
                "height": specifications.get('Dimension', {}).get('Product Height', ''),
                "depth": specifications.get('Dimension', {}).get('Product Depth', ''),
                "weight":  specifications.get('Dimension', {}).get('Product Weight', ''),
                "battery_size":  specifications.get('Power', {}).get('Battery Size', ''),

                "photo_length": specifications.get('Feature', {}).get('Picture Length', ''),
                "photo_width": specifications.get('Feature', {}).get('Picture Width', ''),
                "water_resistant": specifications.get('Performance', {}).get('Water Resistant', ''),
                "film_format": specifications.get('Feature', {}).get('Film Format', ''),

                "minimum_aperture": specifications.get('Exposure Control', {}).get('Minimum Aperture', '').strip('/'),
                "maximum_aperture": specifications.get('Exposure Control', {}).get('Maximum Aperture', '').strip('/'),
                "focal_length": specifications.get('Exposure Control', {}).get('Lens Focal Length', ''),
                "upc": specifications.get('Other', {}).get('UPC', ''),

                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },
            }

            return transformed_record
        else:
            return None
