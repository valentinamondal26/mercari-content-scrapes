class Mapper:
    def map(self, record):
        model = ''
        storage_capacity = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "storage_capacity" == entity['name']:
                if entity["attribute"]:
                    storage_capacity = entity["attribute"][0]["id"]

        key_field = model + storage_capacity
        return key_field
