import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            storage_capacity = specifications.get('Key Specs', {}).get('Total Storage Capacity', '') \
                or specifications.get('Storage', {}).get('Total Storage Capacity', '') \
                or specifications.get('Other', {}).get('Storage Capacity', '') \
                or specifications.get('Feature', {}).get('Storage Capacity', '')

            storage_capacity = re.sub(re.compile('gigabytes', re.IGNORECASE), 'GB', storage_capacity)

            screen_size_inch = specifications.get('Key Specs', {}).get('Screen Size', '') or specifications.get('Display', {}).get('Screen Size', '')

            brand = specifications.get('General', {}).get('Brand', '') or specifications.get('Other', {}).get('Brand', '')
            model_sku = specifications.get('General', {}).get('Model Number', '') or specifications.get('Other', {}).get('Model Number', '')

            carriers_meta = ['AT&T', 'Sprint', 'T-Mobile', 'Verizon', 'Unlocked']

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')

            carrier = ''
            match = re.findall('|'.join(list(map(lambda x: r'\b'+x+r'\b', carriers_meta))), model)
            if match:
                carrier = match[0]
            model = model.split('-', 1)[0]
            if carrier:
                model = f'{model} {carrier}'

            model = re.sub(re.compile(r'\(Latest Model\)|' \
                +storage_capacity.replace(' ', r'\s*') \
                +r'|'+screen_size_inch.replace(' ', r'\s*').replace('inches', '"') \
                +r'|\(|\)', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r'iPad', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,

                "model": model,
                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model_number": model_sku,
                "product_family": specifications.get('General', {}).get('Model Family', ''),
                "color": color,
                "storage_capacity": storage_capacity,
                'screen_size_inch': screen_size_inch,
                'screen_resolution': specifications.get('Key Specs', {}).get('Screen Resolution', '') or specifications.get('Display', {}).get('Screen Resolution', ''),
                'gps': specifications.get('Feature', {}).get('GPS Enabled', ''),
                'front_camera_resolution': specifications.get('Camera', {}).get('Front Facing Camera Megapixels', ''), # Front Facing Camera Video Resolution
                'rear_camera_resolution': specifications.get('Camera', {}).get('Rear Facing Camera Megapixels', ''), # Rear Facing Camera Video Resolution
                'ram': specifications.get('Performance', {}).get('System Memory (RAM)', ''),
                'charger_type': specifications.get('Power', {}).get('Charging Interface(s)', ''),
                'ports_interfaces': specifications.get('Connectivity', {}).get('Interface(s)', ''),
                "height": specifications.get('Dimension',{}).get('Product Height', ''),
                "width": specifications.get('Dimension',{}).get('Product Width', ''),
                "depth": specifications.get('Dimension',{}).get('Product Depth', ''),
                "weight": specifications.get('Dimension',{}).get('Product Weight', ''),

                "upc": specifications.get('Other', {}).get('UPC', ''),
            }

            return transformed_record
        else:
            return None
