# TODO: Get the latest model's generation from :
#  https://support.apple.com/en-us/HT201471.
#  i.e Current Latest generation of iPad Pro is 4th generation
#  Can add this latest generation fetching as part of the bestbuy crawl,in-order
#  to update the generation transformation dynamically.
import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get('title', '')
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications = record.get('specifications', {})

            def flatten_json(y):
                out = {}

                def flatten(x, name=''):
                    if type(x) is dict:
                        for a in x:
                            flatten(x[a], name + a + '_')
                    elif type(x) is list:
                        i = 0
                        for a in x:
                            flatten(a, name + str(i) + '_')
                            i += 1
                    else:
                        out[name[:-1]] = x

                flatten(y)
                return out

            features = ''
            features_json = record.get('features', {})
            if features_json:
                features = ', '.join([f'{key}: {value}' for key, value in flatten_json(features).items()])


            color = specifications.get('Other', {}).get('Color', '') or ''
            if not color:
                color = specifications.get('General', {}).get('Color', '') or ''

            color_category = specifications.get('Other', {}).get(
                    'Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General', {}).get(
                        'Color Category', '') or ''

            storage_capacity = specifications.get('Key Specs', {}).get(
                    'Total Storage Capacity', '') \
                               or specifications.get('Storage', {}).get(
                    'Total Storage Capacity', '') \
                               or specifications.get('Other', {}).get(
                    'Storage Capacity', '') \
                               or specifications.get('Feature', {}).get(
                    'Storage Capacity', '')
            storage_capacity = storage_capacity.replace('gigabytes', 'GB')

            brand = specifications.get('General', {}).get('Brand',
                    '') or specifications.get('Other', {}).get('Brand', '')
            model_sku = specifications.get('General', {}).get('Model Number',
                    '') or specifications.get('Other', {}).get('Model Number',
                    '')
            carrier = specifications.get('General', {}).get('Carrier', '')
            carriers = []
            if carrier:
                carriers.append(carrier)
            else:
                carriers.extend(['AT&T', 'Sprint', 'T-Mobile', 'Verizon', ])
            carriers.extend(['Unlocked'])
            carrier = ', '.join(carriers)

            product_type = specifications.get('General', {}).get('Model Family',
                    '')
            product_type = product_type.strip('Apple ')

            model = specifications.get('General', {}).get('Product Name',
                    '') or specifications.get('Other', {}).get('Product Name',
                    '')

            if re.findall(re.compile(r"\bAir\b", re.IGNORECASE), model):
                product_type = "iPad Air"
            elif re.findall(re.compile(r"\bMini\b", re.IGNORECASE), model):
                product_type = "iPad Mini"
            elif re.findall(re.compile(r"\bPro\b", re.IGNORECASE), model):
                product_type = "iPad Pro"
            else:
                product_type = "iPad"

            model = model.rsplit('-', 1)[0]
            keywords = ['Wireless', 'Latest Model', 'Unlocked']
            keywords.append(brand)
            keywords.append(storage_capacity)
            keywords.extend(carriers)
            model = re.sub(re.compile(
                    r'|'.join(list(map(lambda x: r'\b' + x + r'\b', keywords))),
                    re.IGNORECASE), '', model)
            model = re.sub(re.compile(
                    r"with Wi\-*Fi\s*\+*\s*Cellular|With Wi\-*Fi|\(|\)|\d+\.*\d*\s*\-*\s*(Inch|inches)",
                    re.IGNORECASE), "", model)
            model = re.sub(re.compile(r'\bgen\b', re.IGNORECASE), 'Generation',
                    model)
            model = re.sub(r'\(\s*\)', '', model)
            model = re.sub(r'-\s*$', '', model)
            if re.findall(re.compile(r"Latest Model", re.IGNORECASE), title):
                if product_type == "iPad Pro":
                    model = model + " 4th Generation"
                if product_type == "iPad Air":
                    model = model + " 4th Generation"
                if product_type == "iPad Mini":
                    model = model + " 5th Generation"
                if product_type == "iPad":
                    model = model + " 8th Generation"
            model = model.title()
            model = re.sub(r"Ipad", "iPad", model)

            def lower_repl(match):
                return match.group(1).lower()

            model = re.sub(re.compile(r"(\d+\s*\w{2}\s*)", re.IGNORECASE),
                    lower_repl, model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),

                    "category": record.get('category', ''),
                    "brand": brand,

                    "image": record.get('image', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "description": record.get('description', ''),
                    "features": features,
                    "ner_query": ner_query,
                    "title": title,
                    "model": model,
                    "product_type": product_type,

                    "price": {
                            "currency_code": "USD",
                            "amount": price
                    },
                    'carrier': carrier,
                    "model_sku": model_sku,
                    "color": color,
                    "max_storage_capacity": storage_capacity,
                    'screen_size_inch': specifications.get('Key Specs', {}).get(
                            'Screen Size', '') or specifications.get('Display',
                            {}).get('Screen Size', ''),
                    'screen_resolution': specifications.get('Key Specs',
                            {}).get('Screen Resolution',
                            '') or specifications.get('Display', {}).get(
                            'Screen Resolution', ''),
                    'front_camera_resolution': specifications.get('Camera',
                            {}).get('Front Facing Camera Megapixels', ''),
                    # Front Facing Camera Video Resolution
                    'rear_camera_resolution': specifications.get('Camera',
                            {}).get('Rear Facing Camera Megapixels', ''),
                    # Rear Facing Camera Video Resolution
                    # 'memory_ram_capacity': specifications.get('Performance', {}).get('System Memory (RAM)', ''),
                    "height": specifications.get('Dimension', {}).get(
                            'Product Height', ''),
                    "width": specifications.get('Dimension', {}).get(
                            'Product Width', ''),
                    "depth": specifications.get('Dimension', {}).get(
                            'Product Depth', ''),
                    "weight": specifications.get('Dimension', {}).get(
                            'Product Weight', ''),

                    "upc": specifications.get('Other', {}).get('UPC', ''),
            }

            return transformed_record
        else:
            return None
