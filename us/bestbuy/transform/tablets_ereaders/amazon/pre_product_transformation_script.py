import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            storage_capacity = specifications.get('Key Specs', {}).get('Total Storage Capacity', '') or specifications.get('Storage', {}).get('Total Storage Capacity', '') \
                or specifications.get('Other', {}).get('Storage Capacity', '')
            storage_capacity = re.sub(re.compile(r"\bgigabytes*", re.IGNORECASE), "GB", storage_capacity)
            storage_capacity = re.sub(re.compile(r"(\d+)\s*(\w+)", re.IGNORECASE), r"\1 \2", storage_capacity)

            brand = specifications.get('General', {}).get('Brand', '') or specifications.get('Other', {}).get('Brand', '')
            if not brand:
                brand = record.get('sub_category', '')
            model_sku = specifications.get('General', {}).get('Model Number', '') or specifications.get('Other', {}).get('Model Number', '')
            if not model_sku:
                model_sku = record.get('model', '')

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            if not model:
                model = record.get('title', '').title()
                if not color:
                    color = 'Black'

            model = re.sub(re.compile(r'Amazon|6"|7"|8"|10.1"|Tablet|4GB|8GB|16GB|32GB|64GB|128GB|\(with special offers\)|Release|'+color, re.IGNORECASE), '', model)
            model = re.sub(r'\(2019\)', '2019', model)
            model = re.sub(r'\(10th Generation\)', '10th Generation', model)
            match = re.findall(re.compile(r'\+ Cellular', re.IGNORECASE), model)
            if match:
                model = re.sub(re.compile(r'\+ Cellular', re.IGNORECASE), '', model) + match[0]
            model = re.sub(r'(-\s)+', ' ', model)
            model = re.sub(r'(,\s)+', ' ', model)
            model = re.sub(re.compile(r'High-Resolution Display \(300 ppi\) Waterproof Built-In Audible', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            gen_match = re.findall(re.compile(r'(\b([0-9]|[0-9]{2}) ([0-9]|[0-9]{2})th Generation\b)', re.IGNORECASE), \
                                   model)
            if gen_match:
                model = re.sub(r'(\b([0-9]|[0-9]{2})th Generation\b)', '', model)
            model = re.sub(' +', ' ', model).strip()

            screen_size_inch = specifications.get('Key Specs', {}).get('Screen Size', '') or specifications.get('Display', {}).get('Screen Size', '')
            if not screen_size_inch:
                screen_size_inch = specifications.get('Other', {}).get('Screen Size (Measured Diagonally)', '')
                screen_size_inch = re.sub('"', ' inches', screen_size_inch)
                screen_size_inch = re.sub(' +', ' ', screen_size_inch).strip()
            
            color = re.sub(re.compile(r"Black and silver", re.IGNORECASE), "Black/Silver", color)
            if re.compile(r"^Paperwhite E\-Reader \+ Cellular$", re.IGNORECASE).match(model):
                model = "Kindle Paperwhite E-Reader + Cellular"


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,

                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model_sku": model_sku,
                "width": specifications.get('Dimension',{}).get('Product Width', ''),
                "height": specifications.get('Dimension',{}).get('Product Height', ''),
                "depth": specifications.get('Dimension',{}).get('Product Depth', ''),
                "weight": specifications.get('Dimension',{}).get('Product Weight', ''),
                "upc": specifications.get('Other', {}).get('UPC', ''),
                'operating_system': specifications.get('Key Specs', {}).get('Operating System', '') or specifications.get('Compatibility', {}).get('Operating System', '') \
                    or specifications.get('Other', {}).get('Operating System', ''),

                "model": model,
                "color": color,
                "max_storage_capacity": storage_capacity,
                "display_type": specifications.get('Display', {}).get('Display Type', ''),
                "battery_life": specifications.get('Performance', {}).get('Battery Life', ''),
                "product_line": specifications.get('General', {}).get('Model Family', ''),
                "year": specifications.get('General', {}).get('Year of Release', ''),
                'screen_size_inch': screen_size_inch,
                'front_camera_resolution': specifications.get('Camera', {}).get('Front Facing Camera Megapixels', ''), # Front Facing Camera Video Resolution
                'rear_camera_resolution': specifications.get('Camera', {}).get('Rear Facing Camera Megapixels', ''), # Rear Facing Camera Video Resolution
                'screen_resolution': specifications.get('Key Specs', {}).get('Screen Resolution', '') or specifications.get('Display', {}).get('Screen Resolution', ''),
                'headphone_jack': specifications.get('Connectivity', {}).get('Headphone Jack', '') or specifications.get('Other', {}).get('Headphone Jack', ''),
                'ports_interfaces': specifications.get('Connectivity', {}).get('Interface(s)', ''),
                'memory_cards_supported': specifications.get('Compatibility', {}).get('Expandable Memory Compatibility', ''),
                'charger_type': specifications.get('Power', {}).get('Charging Interface(s)', ''),
            }

            return transformed_record
        else:
            return None
