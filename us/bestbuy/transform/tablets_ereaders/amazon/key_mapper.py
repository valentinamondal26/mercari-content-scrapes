class Mapper:
    def map(self, record):
        model = ''
        color = ''
        max_storage_capacity = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "max_storage_capacity" == entity['name']:
                if entity["attribute"]:
                    max_storage_capacity = entity["attribute"][0]["id"]

        key_field = model + color + max_storage_capacity
        return key_field
