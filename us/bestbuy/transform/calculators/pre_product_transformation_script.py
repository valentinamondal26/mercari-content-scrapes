import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')

            model_sku = specifications.get('General',{}).get('Model Number','') or specifications.get('Other',{}).get('Model Number','')

            if id == 'https://www.bestbuy.com/site/texas-instruments-scientific-calculator-blue/8837878.p?skuId=8837878':
                model = "TI-30XS Multiview Scientific Calculator"
            else:
                model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            model = re.sub(re.compile(r'Graphing Calculator|, Pixel Display|Scientific Calculator|Handheld|Adv. Financial Calculator|Teachers Pack|Teacher Kit Portable (10-Pack)|Multiview', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()
            if not model:
                model = model_sku

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,

                "color": color,
                "brand": brand,
                "model_sku": model_sku,
                "height": specifications.get('Other',{}).get('Product Height','') or specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Other',{}).get('Product Width','') or specifications.get('Dimension',{}).get('Product Width',''),
                "length": specifications.get('Other',{}).get('Product Length','') or specifications.get('Dimension',{}).get('Product Length',''),
                "weight": specifications.get('Other',{}).get('Product Weight','') or specifications.get('Dimension',{}).get('Product Weight',''),
                "upc": specifications.get('Other',{}).get('UPC',''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "battery_life": specifications.get('Other',{}).get('Battery Life',''),
                "display_type": specifications.get('Other',{}).get('Display Type',''),
                "display_size": specifications.get('Other',{}).get('Display Size',''),
                "battery_type": specifications.get('Other',{}).get('Battery Type',''),
                "color_category": color_category,
            }

            return transformed_record
        else:
            return None
