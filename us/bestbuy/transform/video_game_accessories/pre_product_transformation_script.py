import hashlib
from colour import Color
import re


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            specifications = record.get('specifications', {})
            price = record.get('price', '')
            price = price.replace("$", "")
            price = ''.join(price.split())
            price = price.replace(",", "")

            product_type = record.get('breadcrumb', '')
            product_type = product_type.split('|')
            product_type = product_type[-1:][0]

            color = specifications.get('General', {}).get('Color', '')
            if color == '':
                color = specifications.get(
                    'General', {}).get('Color Category', '')

            material = ''
            materials = ['PVC', 'Tempered glass', 'Nylon', 'EVA', 'Plastic', 'Polyester',
                         'Other', 'Fabric', 'Acetate', 'Leather', 'Polyethylene', 'Textile']
            for key, value in specifications.items():
                for k, v in value.items():
                    if v in materials:
                        material = v
                        break

            connection_type = specifications.get(
                'Key Specs', {}).get('Connection Type', '')
            if connection_type == '':
                connection_type = specifications.get(
                    'Connectivity', {}).get('Connection Type', '')

            fit = specifications.get('Key Specs', {}).get('Headphone Fit', '')
            if fit == '':
                fit = specifications.get('Design', {}).get('Headphone Fit', '')

            brand = specifications.get('General', {}).get('Brand', '')
            if brand == '':
                brand = specifications.get('Other', {}).get('Brand', '')

            storage_capacity = specifications.get(
                'Key Specs', {}).get('Storage Capacity', '')
            pattern = re.compile(r"\s*giga\s*bytes", re.IGNORECASE)
            storage_capacity = re.sub(pattern, ' GB', storage_capacity)
            pattern = re.compile(r"\s*tera\s*bytes", re.IGNORECASE)
            storage_capacity = re.sub(pattern, ' TB', storage_capacity)
            pattern = re.compile(r"\s*mega\s*bytes", re.IGNORECASE)
            storage_capacity = re.sub(pattern, ' MB', storage_capacity)

            model = specifications.get('General', {}).get('Product Name', '')
            if model == '':
                model = specifications.get('Other', {}).get('Product Name', '')
            if re.findall(re.compile(r"\b{}\b".format(color), re.IGNORECASE), model) != []:
                color = ''

            product_name = specifications.get(
                'General', {}).get('Product Name', '')
            if product_name == '':
                product_name = specifications.get(
                    'Other', {}).get('Product Name', '')

            platform = specifications.get('Key Specs', {}).get(
                'Compatible Platform(s)', '')
            if platform == '':
                platform = specifications.get('Compatibility', {}).get(
                    'Compatible Platform(s)', '')

            wordList = re.sub("[^\w]", " ",  model).split()

            _color = [i.strip(' ,')
                      for i in wordList if self.check_color(i.strip(' ,'))]
            for cl in _color:
                model = model.replace(cl, '')

            model = model.replace(color, '')
            pattern = re.compile(
                r"®|™|Nintendo|Nintendo\s*Switch|Refurbished|\s+for\s*[\w\W\d]*", re.IGNORECASE)
            model = re.sub(pattern, '', model)
            model = model.replace(storage_capacity, '')

            model = ' '.join(model.split())
            model = re.sub(re.compile(r"DAEMON X MACHINA",
                                      re.IGNORECASE), "Daemon X Machina", model)
            if re.findall(re.compile(r"Controller", re.IGNORECASE), model) != []:
                model = brand+" "+model
                model = " ".join(model.split())

            product_type_meta = ["Nintendo Switch Batteries & Chargers",
                                 "Nintendo Switch Cases & Skins", "Nintendo Switch Controllers", "Nintendo Switch Headsets"]

            if product_type not in product_type_meta:
                return None
            ignore_pattern = re.compile(
                r"Geek Squad|Certified|S1 Mini Nintendo Switch Dock 720p DLP Projector - Black", re.IGNORECASE)
            if re.findall(ignore_pattern, model) != []:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "Video Game Accessories",
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                # "description": record.get('description', ''),
                # "features": record.get('features', ''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_name": product_name,
                "model": model,
                "brand": "Nintendo",
                "product_type": product_type,
                "platform": platform,
                "case_type": specifications.get('Feature', {}).get('Bag/Case Type', ''),
                "color": color.title(),
                "storage_capacity": storage_capacity,
                "material": material,
                "connection_type": connection_type,
                "fit": fit,
                "cord_length": specifications.get('Dimension', {}).get('Cord Length', ''),
                "model_sku": specifications.get('General', {}).get('Model Number', ''),
                "item_condition": record.get('item_condition', '')



            }

            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
