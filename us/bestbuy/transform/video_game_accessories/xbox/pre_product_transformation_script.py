import hashlib
from colour import Color
import re


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            specifications = record.get('specifications', {})
            price = record.get('price', '')
            price = price.replace("$", "")
            price = ''.join(price.split())
            price = price.replace(",", "")

            color = specifications.get('General', {}).get('Color', '')
            if not color:
                color = specifications.get('General', {}).get('Color Category', '')
            color = re.sub(re.compile(r'\s*(,|&|and)\s*', re.IGNORECASE), '/', color)
            color = re.sub(r'\s+', ' ', color).strip()
            color = color.title()

            connection_type = specifications.get(
                'Key Specs', {}).get('Connection Type', '')
            if connection_type == '':
                connection_type = specifications.get(
                    'Connectivity', {}).get('Connection Type', '')

            fit = specifications.get('Key Specs', {}).get('Headphone Fit', '')
            if fit == '':
                fit = specifications.get('Design', {}).get('Headphone Fit', '')

            brand = specifications.get('General', {}).get('Brand', '')
            if brand == '':
                brand = specifications.get('Other', {}).get('Brand', '')

            storage_capacity = specifications.get('Key Specs', {}).get('Hard Drive Capacity', '') \
                or specifications.get('Capacity', {}).get('Hard Drive Capacity', '')
            pattern = re.compile(r"\s*giga\s*bytes", re.IGNORECASE)
            storage_capacity = re.sub(pattern, ' GB', storage_capacity)
            pattern = re.compile(r"\s*tera\s*bytes", re.IGNORECASE)
            storage_capacity = re.sub(pattern, ' TB', storage_capacity)
            pattern = re.compile(r"\s*mega\s*bytes", re.IGNORECASE)
            storage_capacity = re.sub(pattern, ' MB', storage_capacity)

            product_name = specifications.get('General', {}).get('Product Name', '') or \
                specifications.get('Other', {}).get('Product Name', '')

            keywords = [
                'PC', 'PCs', 'PS4', 'PS3', 'Nintendo Switch', 'iOS', 'Android', 'Windows 10', 'Windows',
                'switch',
                'Mobile Devices', 'Nintendo Wii U',
                'PlayStation 4 Pro', 'PlayStation 4', 'PlayStation 3', 'PlayStation',
                'VR',
                'Mac',
                'Officially Licensed',
            ]
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', product_name)
            model = re.sub(r'\s,', ' ', model)
            model = re.sub(r'/|\*', '', model)
            model = re.sub(r',\s*(and|with)\b', lambda x: f' {x.group(1)}', model)
            model = re.sub(r'for\s*and\b', lambda x: 'for', model)
            model = re.sub(r'\band\s*(most)*$', '', model)
            model = re.sub(r',\s*$', '', model)
            model = re.sub(r'for\s*$', '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            pattern = re.compile(r'(for (xbox 360|xbox one s|xbox one x|xbox series x|xbox one|xbox|one s|\s*Microsoft\s*|\s*and\s*|,\s*)*)', re.IGNORECASE)
            match = re.findall(pattern, model)
            if match and match[0][1]:
                model = re.sub(pattern, ' ', model)
                model = f'{model} {match[0][0]}'
                model = re.sub(r'\band\s*(most)*$', '', model)
                model = re.sub(r'\s+', ' ', model).strip()

            if product_name in ['Xbox Wireless Controller + Wireless Adapter - Windows 10', 'Xbox Wireless Adapter for Windows 10']:
                model = product_name
                model = re.sub(r'\+', 'with', model)
                model = re.sub(r'-', 'for', model)

            platform = specifications.get('Key Specs', {}).get('Compatible Platform(s)', '') or \
                specifications.get('Compatibility', {}).get('Compatible Platform(s)', '')

            product_type = ', '.join(record.get('Category', ''))
            product_type = re.sub(re.compile(r'\bXbox One\b', re.IGNORECASE), '', product_type)
            product_type = re.sub(re.compile(r'Controllers', re.IGNORECASE), 'Controllers and Controller Accessories', product_type)
            product_type = re.sub(r'\s+', ' ', product_type).strip()
            if not product_type:
                if re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Cable', 'Adapter']))), re.IGNORECASE), model):
                    product_type = 'Cables & Networking'
                elif re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Charger', 'Charging', 'Charge']))), re.IGNORECASE), model):
                    product_type = 'Batteries & Chargers'
                elif re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Controller']))), re.IGNORECASE), model):
                    product_type = 'Controllers and Controller Accessories'

            ignore_pattern = re.compile(
                r"Geek Squad|Certified|S1 Mini Nintendo Switch Dock 720p DLP Projector - Black", re.IGNORECASE)
            if re.findall(ignore_pattern, model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": "Video Game Accessories",
                "brand": record.get('sub_category', ''), #brand,

                'title': specifications.get('General', {}).get('Product Name', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                # "description": record.get('description', ''),
                # "features": record.get('features', ''),
                "ner_query": ner_query,

                "msrp": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "model": model,
                "color": color,
                "product_type": product_type,
                "item_condition": record.get('item_condition', ''),
                "wired_wireless": specifications.get('Key Specs', {}).get('Wireless', '') \
                    or specifications.get('Connectivity', {}).get('Wireless', ''),
                "connection_type": connection_type,
                "cord_length": specifications.get('Dimension', {}).get('Cord Length', ''),
                'cable_length': specifications.get('Key Specs', {}).get('Cable Length', '') \
                    or specifications.get('Connectivity', {}).get('Cable Length', ''),
                'cable_type': specifications.get('Connectivity', {}).get('Cable Type', ''),
                "fit_design": fit,
                'connector_type': specifications.get('Power', {}).get('Charging Interface(s)', ''),
                'connector_size': specifications.get('Connectivity', {}).get('Connector Size', ''),
                "platform": platform,
                'hard_drive_capacity': storage_capacity,
                "model_sku": specifications.get('General', {}).get('Model Number', ''),
                'internal_external': specifications.get('Power', {}).get('Internal Or External', ''),
                'form_factor': specifications.get('Compatibility', {}).get('Form Factor', ''),
                'power_type': specifications.get('Power', {}).get('Power Source', ''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width',''),
                "weight": specifications.get('Dimension',{}).get('Product Weight',''),
                'sound_quality': specifications.get('Key Specs',{}).get('Sound Mode','') \
                    or specifications.get('Audio',{}).get('Sound Mode',''),
                'battery_life': specifications.get('Power', {}).get('Battery Life', ''),
            }

            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
