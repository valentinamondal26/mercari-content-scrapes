import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color_category = specifications.get('General',{}).get('Color Category', '') or specifications.get('Other',{}).get('Color Category', '')
            color = specifications.get('General',{}).get('Color', '') or specifications.get('Other',{}).get('Color', '')

            color = color_category or color
            color = re.sub(re.compile(r'\bMulti\b', re.IGNORECASE), 'Multicolor', color)

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')

            compatible_phone_model_raw = specifications.get('Compatibility',{}).get('Model Compatibility', '') or specifications.get('Key Specs',{}).get('Model Compatibility', '')
            compatible_phone_model = re.sub(re.compile(r'\bApple\b|\(|\)|\b5G\b', re.IGNORECASE), '', compatible_phone_model_raw)
            models_meta = [
                'iPhone', 'Samsung Galaxy', 'Galaxy', 'Google Pixel',
            ]
            pattern = re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', models_meta))), re.IGNORECASE)
            match = re.findall(pattern, compatible_phone_model)
            if match:
                compatible_phone_model = f"{match[0]} {re.sub(pattern, '', compatible_phone_model)}"
            compatible_phone_model = re.sub(r'\s*,\s*', '/', compatible_phone_model)
            compatible_phone_model = re.sub(re.compile(r'Gen\.', re.IGNORECASE), 'Generation', compatible_phone_model)
            compatible_phone_model = re.sub(r'\s+', ' ', compatible_phone_model).strip()

            def word_regex(word):
                return r'\b'+word+r'\b'

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            model = re.sub(re.compile(r'for.*', re.IGNORECASE), '', model)
            keywords = [
                # word_regex(r'Otter \+'),
                word_regex(brand), word_regex(color),
                # word_regex(compatible_phone_model),
                '®', word_regex('Apple'),
                # word_regex('For'),
                word_regex('Case'), word_regex('Motorola MOTO E5 Play'),
                word_regex('clear'),
            ]
            model = re.sub(re.compile(r'|'.join(keywords), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'Otter \+|\(|\)', re.IGNORECASE), '', model)
            # keywords = []
            # keywords.extend([word_regex(m.replace('+', '\+').strip()) for m in compatible_phone_model_raw.split(',')])
            # keywords.extend([word_regex(m.replace('+', '\+').strip()) for m in compatible_phone_model.split('/')])
            # model = re.sub(re.compile(r'|'.join(keywords), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\b'+compatible_phone_model+r'\b', re.IGNORECASE), '', model)
            model = re.sub(r'^\s*\+|\s+\+\s*$', '', model)
            # model = re.sub(r'\s+and\s*$', '', model)
            # model = re.sub(r',\s*$', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,

                'compatible_brand': specifications.get('Compatibility',{}).get('Brand Compatibility', '') or specifications.get('Key Specs',{}).get('Brand Compatibility', ''),
                'compatible_phone_model': compatible_phone_model,
                'series': specifications.get('General',{}).get('Series', '') or specifications.get('Key Specs',{}).get('Series', ''),
                'case_type': specifications.get('General',{}).get('Case Type', '') or specifications.get('Key Specs',{}).get('Case Type', ''),
                'style': specifications.get('General',{}).get('Case Style', '') or specifications.get('Key Specs',{}).get('Case Style', ''),
                'camera_integration': specifications.get('Features',{}).get('Camera Cut-Out', ''),
                'resistance_type': specifications.get('Performance',{}).get('Scratch Resistant', '') or specifications.get('Key Specs',{}).get('Scratch Resistant', ''),
                # 'material': record.get('Material', ''),
                'material': specifications.get('Material',{}).get('Material', ''),
                'item_condition': record.get('item_condition', ''),

                "color": color,
                "model_sku": specifications.get('General',{}).get('Model Number',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width',''),
                'depth': specifications.get('Dimension',{}).get('Product Depth',''),
                "weight": specifications.get('Dimension',{}).get('Product Weight',''),
                "upc": specifications.get('Other',{}).get('UPC',''),

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,

            }

            return transformed_record
        else:
            return None
