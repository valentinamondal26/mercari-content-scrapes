import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('General',{}).get('Color', '') or specifications.get('Other',{}).get('Color', '')

            if not color and re.findall(re.compile(r'clear case', re.IGNORECASE), record.get('title', '')):
                color = 'Clear'

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')

            compatible_phone_model = specifications.get('Compatibility',{}).get('Model Compatibility', '') or specifications.get('Key Specs',{}).get('Model Compatibility', '')

            material = specifications.get('Material',{}).get('Material', '')
            material = re.sub(re.compile(r'Polycarbonate, Polyurethane|Polycarbonate|Polyurethane', re.IGNORECASE), 'Platstic', material)

            def word_regex(word):
                return r'\b'+word+r'\b'

            keywords = [
                word_regex(brand), word_regex(color), '®',
                # word_regex('case'),
                'Geek Squad Certified Refurbished', word_regex('Apple')]
            model = re.sub(re.compile(r'|'.join(keywords), re.IGNORECASE), '', model)
            pattern = re.compile(r'^('+material+r'\s*case) for', re.IGNORECASE)
            match = re.findall(pattern, model)
            if match:
                model = re.sub(pattern, '', model) + f' {match[0]}'
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,


                'compatible_brand': specifications.get('Compatibility',{}).get('Brand Compatibility', '') or specifications.get('Key Specs',{}).get('Brand Compatibility', ''),
                'compatible_phone_model': compatible_phone_model,
                'series': specifications.get('General',{}).get('Series', '') or specifications.get('Key Specs',{}).get('Series', ''),
                'case_type': specifications.get('General',{}).get('Case Type', '') or specifications.get('Key Specs',{}).get('Case Type', ''),
                'style': specifications.get('General',{}).get('Case Style', '') or specifications.get('Key Specs',{}).get('Case Style', ''),
                'camera_integration': specifications.get('Features',{}).get('Camera Cut-Out', ''),
                'resistance_type': specifications.get('Performance',{}).get('Scratch Resistant', '') or specifications.get('Key Specs',{}).get('Scratch Resistant', ''),
                'material': material,
                'item_condition': record.get('item_condition', ''),

                "color": color,
                "brand": brand,
                "model_sku": specifications.get('General',{}).get('Model Number',''),
                "upc": specifications.get('Other',{}).get('UPC',''),

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
            }

            return transformed_record
        else:
            return None
