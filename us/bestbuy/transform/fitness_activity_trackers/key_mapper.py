
class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            if "color" == entity['name']:
                if entity.get("attribute", []):
                    color = entity.get("attribute", [{}])[0].get("id", '')

        key_field = model + color
        return key_field
