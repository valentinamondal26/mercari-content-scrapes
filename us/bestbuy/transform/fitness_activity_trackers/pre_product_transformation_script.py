import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")

            title = record['title']
            title = title.split("-")[1].strip()

            specifications=record.get('specifications',{})

            color = specifications.get('General',{}).get('Color', '') or ''
            color = color.split("with")[0]
            if not color:
                color = specifications.get('Other',{}).get('Color', '') or ''

            color_category = specifications.get('General',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('Other',{}).get('Color Category', '') or ''


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "model":title,
                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },
                "color": color,
                "color_category":color_category,
                "product_family":specifications.get('General',{}).get('Model Family',''),
                #"item_condition":,
                "form_factor":specifications.get('Features',{}).get('Form Factor',''),
                "charger_type":specifications.get('Power',{}).get('Charging Interface(s)',''),
                "handle_strap_material" :specifications.get('Key Specs',{}).get('Band Material',''),
                "upc":specifications.get('Other',{}).get('UPC',''),
                "model_sku":specifications.get('General',{}).get('Model Number',''),
                "touch_screen":specifications.get('Key Specs',{}).get('Touch Screen',''),
                "case_size":specifications.get('Key Specs',{}).get('Screen Size',''),
                "series":specifications.get('General',{}).get('Series',''),
                "sensors":specifications.get('Activity',{}).get('Sensors',''),
                "water_resistant":specifications.get('Key Specs',{}).get('Water Resistant',''),
                "maximum_depth":specifications.get('Key Specs',{}).get('Maximum Depth of Water Resistance',''),
                "gps":specifications.get('Key Specs',{}).get('Global Positioning',''),
                "height":specifications.get('Dimension',{}).get('Product Height',''),
                "width":specifications.get('Dimension',{}).get('Product Width',''),
                "weight":specifications.get('Dimension',{}).get('Product Weight',''),
                "depth":specifications.get('Dimension',{}).get('Product Depth',''),
                "band_color":specifications.get('Watch',{}).get('Band Color',''),
                "band_type":specifications.get('Watch',{}).get('Band Type',''),
                "brand": specifications.get('General', {}).get('Brand', '')
            }

            return transformed_record
        else:
            return None
