import hashlib
from colour import Color
import re


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            specifications = record.get('specifications', {})
            price = record.get('price', '')
            price = price.replace("$", "")
            price = ''.join(price.split())
            price = price.replace(",", "")

            model = specifications.get("Details", {}).get("Title", "")
            if model == "":
                model = specifications.get("Other", {}).get("Product Name", "")
            prod = model

            # remove color details from model
            wordList = re.sub(r"[^\w]", " ",  model).split()
            _color = [i.strip(' ,')
                      for i in wordList if self.check_color(i.strip(' ,'))]
            for cl in _color:
                model = model.replace(cl, '')

            tags = specifications.get("Details", {}).get("Product Tags", "")
            for word in tags.split(","):
                model = re.sub(re.compile(r"\b{}\b".format(
                    word.strip()), re.IGNORECASE), "", model)
            model = re.sub(r"\[\s*\]", "", model)
            model = " ".join(model.split())

            # extract edition form title
            editions_meta = ["SteelBook", "Includes Digital Copy",
                             "Only @ Best Buy", "Collector's Edition", "Limited Edition"]
            edition = []
            for edit in editions_meta:
                pattern = re.compile(r"{}".format(edit), re.IGNORECASE)
                print(re.findall(pattern, record.get("title", "")))
                if re.findall(pattern, record.get("title", '')) != []:
                    edition.append(edit)
            edition = ", ".join(edition)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "DVDs & Blu-ray Discs",
                "brand": record.get("brand", ""),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', ''),
                "features": record.get('features', ''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": str(price)
                },
                "product_name": prod,
                "model": model,
                # "title":record.get("title",''),
                "genre": specifications.get('Details', {}).get('Genre', ''),
                "sub_genre": specifications.get('Details', {}).get('Subgenre', ''),
                "duration": specifications.get('Details', {}).get('Duration', ''),
                "release_year": specifications.get('Details', {}).get('Year of Release', ''),
                "media_format": specifications.get('Details', {}).get('Format', ''),
                "edition": edition,
                "aspect_ratio": specifications.get('Details', {}).get('Aspect Ratio', ''),
                "product_line": specifications.get('Details', {}).get('Studio', ''),
                "decade": record.get("Decade", ""),
                "rating": record.get("rating", ""),
                "award": record.get("Award Winner", ""),
                "upc": specifications.get('Other', {}).get('UPC', ''),




            }

            return transformed_record
        else:
            return None

    def check_color(self, color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
