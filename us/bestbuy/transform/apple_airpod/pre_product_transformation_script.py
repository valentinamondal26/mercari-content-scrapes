import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')

            if re.findall(re.compile('Geek Squad Certified Refurbished', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "color": color,
                "brand": brand,
                "model_sku": specifications.get('Other',{}).get('Model Number',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width',''),
                "weight": specifications.get('Dimension',{}).get('Product Weight',''),
                "upc": specifications.get('Other',{}).get('UPC',''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "battery_life": specifications.get('Power',{}).get('Battery Life',''),
                "charging_case_battery_life": specifications.get('Power',{}).get('Battery Life Provided by Charging Case',''),
                "connection_type": specifications.get('Connectivity',{}).get('Connection Type','') or specifications.get('Key Specs',{}).get('Connection Type',''),
                "charger_type": specifications.get('Power',{}).get('Charging Interface(s)',''),
                "fit": specifications.get('Design',{}).get('Headphone Fit','') or specifications.get('Key Specs',{}).get('Headphone Fit',''),
                "active_noise_canceling": specifications.get('Audio',{}).get('Noise Canceling (Active)','') or specifications.get('Key Specs',{}).get('Noise Canceling (Active)',''),
                "water_resistance": specifications.get('Features',{}).get('Water Resistant','') or specifications.get('Key Specs',{}).get('Water Resistant',''),
                "wireless_technology": specifications.get('Connectivity',{}).get('Wireless Connectivity','') or specifications.get('Key Specs',{}).get('Wireless Connectivity',''),
            }

            return transformed_record
        else:
            return None
