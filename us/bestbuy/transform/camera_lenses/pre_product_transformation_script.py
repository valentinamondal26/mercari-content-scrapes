import hashlib


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('General',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('Other',{}).get('Color', '') or ''

            color_category = specifications.get('General',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('Other',{}).get('Color Category', '') or ''


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model":specifications.get('General',{}).get('Product Name',''),
                "color": color,
                "brand":specifications.get('General',{}).get('Brand',''),
                "upc":specifications.get('Other',{}).get('UPC',''),
                "diameter":specifications.get('Additional Dimension',{}).get('Product Diameter',''),
                "length":specifications.get('Additional Dimension',{}).get('Product Length with Lens Closed',''),
                "weight":specifications.get('Dimension',{}).get('Product Weight',''),

                "lens_type": specifications.get('Key Specs',{}).get('Lens Type',''),
                "focal_length_min": specifications.get('Key Specs',{}).get('Minimum Focal Length',''),
                "maximum_aperture": specifications.get('Key Specs',{}).get('Maximum Aperture',''),
                "minimum_aperture": specifications.get('Exposure Control',{}).get('Minimum Aperture',''),
                "focal_length_max": specifications.get('Key Specs',{}).get('Maximum Focal Length',''),
                "water_resistance": specifications.get('Performance',{}).get('Water Resistant',''),
                "model_sku": specifications.get('General',{}).get('Model Number',''),
                "in_lens_image_stabilization": specifications.get('Imaging',{}).get('In-Lens Image Stabilization',''),
                "tripod_monopod": specifications.get('Other',{}).get('Tripod/Monopod Mountable',''),
            }

            return transformed_record
        else:
            return None
