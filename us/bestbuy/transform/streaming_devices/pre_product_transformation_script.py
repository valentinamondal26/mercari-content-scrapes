import hashlib
from colour import Color
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()


            specifications=record.get('specifications',{})
            price=record.get('price','')
            price = price.replace("$", "")
            price=''.join(price.split())
            price=price.replace(",","")
            
            

            model=specifications.get('General',{}).get('Product Name','')
            if model=='':
                model=specifications.get('Other',{}).get('Product Name','')
            product_name=model
            
            wordList = re.sub("[^\w]", " ",  model).split()

            _color = [i.strip(' ,') for i in wordList if self.check_color(i.strip(' ,'))]
            for color in _color:
                model = model.replace(color, '')
            model=model.replace(specifications.get('General',{}).get('Color',''),'')
            model=model.replace(specifications.get('General',{}).get('Brand',''),'')
            
            #all-new
            pattern=re.compile(r"all\s*\-*\+*new|\,{0,1}\s{0,1}Streaming\s*Media\s*Player",re.IGNORECASE)
            #model=re.sub(r"[a|A][l|L][l|L]\s*\-*\+*[n|N][e|E][w|W]|\d+\s*[A-Z]{2}|\-*",'',model)
            model=re.sub(pattern,'',model)
            model=re.sub(r"\d+\s*[A-Z]{2}",'',model)
            # matches=re.findall(pattern,model)
            # for match in matches:
            #     model=model.replace(match,'')
            model=model.replace('-','')
            model=' '.join(model.split())
            


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "Streaming Devices",
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),                
                "features": record.get('features',''),
                "ner_query": ner_query,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "color":specifications.get('General',{}).get('Color',''),
                'model_sku':specifications.get('General',{}).get('Model Number',''),
                "product_name":product_name,
                "model": model,
                "itemIds": {
                    "UPC": [specifications.get('Other',{}).get('UPC','')],
                },
                'brand':specifications.get('General',{}).get('Brand',''),
                'max_resolution':specifications.get('Key Specs',{}).get('Maximum Supported Resolution',''),
                'ports_interfaces':specifications.get('Compatibility',{}).get('Interface(s)',''),
                'upc':specifications.get('Other',{}).get('UPC',''),
                'hdr':specifications.get('Key Specs',{}).get('High Dynamic Range (HDR)',''),
                'playable_file_formats':specifications.get('Compatibility',{}).get('Playable Formats','')

            }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

