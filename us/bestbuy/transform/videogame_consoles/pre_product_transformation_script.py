import hashlib
from colour import Color
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            for key in record:
                if key == 'category':
                    transformed_record['category'] = 'Video Game Consoles'
                else:
                    transformed_record[key] = record[key]
            if record.get('category','')!='Handheld Consoles':
                category='Video Game Consoles'
            else:
                category=record.get('category','')

            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            specifications=record.get('specifications',{})
            price=record.get('price','')
            price = price.replace("$", "")
            price=''.join(price.split())
            price=price.replace(",","")
            title=record.get('title','')
            wordList = re.sub("[^\w]", " ",  title).split()
            colour=''
            _color = [i.strip(' ,') for i in wordList if self.check_color(i.strip(' ,'))]
            color=''
            for color in _color:
                if color:
                    colour+=color+","
                    title = title.replace(color, '')

            colour=colour[:-1]
            pattern=r"[n|N]{1}eon|[e|E]{1}lectric"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            pattern=r"[\+\/\-]*"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            storage_capacity=''
            if re.findall(r"\d+\s*[A-Z]{2}",title):
                storage_capacity=re.findall(r"\d+\s*[A-Z]{2}",title)[0]
                title=title.replace(re.findall(r"\d+\s*[A-Z]{2}",title)[0],'')
            pattern=r"[c|C]{1}onsole[s]*"
            matches=re.findall(pattern,title)
            for match in matches:
                title=title.replace(match,'')
            
            platform=specifications.get('Compatibility',{}).get('Compatible Platform(s)','')
            if platform=='':
                platform=specifications.get('General',{}).get('Compatible Platform(s)','')
            if title.find(record.get('brand',''))==0 and record.get('brand','') !='Nintendo':
                title=title.replace(record.get('brand',''),'',1)
            pattern = r"([p|P]{1}remium){0,1}\s*[r|R]{1}efurbished\){0,1}"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            pattern=r"\({0,1}[p|P]{1}re\-{0,1}[o|O]wned\){0,1}"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            pattern=r"\[{0,1}[d|D]{1}igital\s{0,1}[d|D]ownload\s{0,1}[a|A]dd\-{0,1}[o|O]n\]"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            pattern=r"(\(\w{0}\s*\))"
            title = re.sub(re.compile(pattern, re.IGNORECASE), '', title)
            title=' '.join(title.split())


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": category,
                "brand":record.get('brand',''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "model": title,
                "storage_capacity": storage_capacity,
                "items_included": record.get('bundle_includes',''),
                "platform":platform ,
                "model_sku": record.get('model', ''),
                "itemIds": {
                    "UPC": [specifications.get('Other',{}).get('upc','')],
                },
                "release_date":specifications.get('Details',{}).get('Original Release Date',''),
                "esrb_rating":specifications.get('Certifications & Listings',{}).get('ESRB Rating',''),
                "upc":specifications.get('Other',{}).get('upc',''),
                "color":colour,
                "console_type":specifications.get("General",{}).get('Console Type',''),
                "length":specifications.get('Dimension',{}).get('Product Length',''),
                "width":specifications.get('Dimension',{}).get('Product Width',''),
                "weight":specifications.get('Dimension',{}).get('Product Weight',''),
                "height":specifications.get('Dimension',{}).get('Product Height',''),

            }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

