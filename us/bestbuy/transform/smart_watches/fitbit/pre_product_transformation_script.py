import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('was_price', '')
            if not price:
                price = record.get('price', '')
            price = price.replace("$", "")

            specifications=record.get('specifications',{})

            color_category = specifications.get('General', {}).get('Color Category', '')
            color = specifications.get('General', {}).get('Color', '') or color_category

            model = specifications.get('General', {}).get('Product Name', '')
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": specifications.get('General', {}).get('Brand', ''),

                "product_name": model,
                "model": model,
                "color": color,
                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },
                "model_number": specifications.get('General', {}).get('Model Number', ''),
                "product_line": specifications.get('General', {}).get('Model Family', ''),
                "series": specifications.get('General', {}).get('Series', ''),
                "water_resistant": specifications.get('Key Specs', {}).get('Water Resistant', ''),
                "maximum_depth": specifications.get('Key Specs', {}).get('Maximum Depth of Water Resistance', '') \
                    or specifications.get('Performance', {}).get('Maximum Depth of Water Resistance', ''),
                "band_color": specifications.get('Watch', {}).get('Band Color', ''),
                "band_type": specifications.get('Watch', {}).get('Band Type', ''),

                "length": specifications.get('Dimension', {}).get('Product Length', ''),
                "height": specifications.get('Dimension', {}).get('Product Height', ''),
                "width": specifications.get('Dimension', {}).get('Product Width', '') \
                    or specifications.get('Dimension', {}).get('Case Width', ''),
                "depth": specifications.get('Dimension', {}).get('Product Depth', ''),
                "weight": specifications.get('Dimension', {}).get('Product Weight', ''),
            }

            return transformed_record
        else:
            return None
