import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('was_price', '')
            if not price:
                price = record.get('price', '')
            price = price.replace("$", "")

            specifications=record.get('specifications',{})

            color_category = specifications.get('General', {}).get('Color Category', '')
            color = specifications.get('General', {}).get('Color', '') or color_category

            model = specifications.get('General', {}).get('Product Name', '')
            title = model

            materials_meta = ['Stainless steel', 'Aluminum']
            material = specifications.get('Material', {}).get('Case Material', '')
            if not material:
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', materials_meta))), re.IGNORECASE), model)
                if match:
                    material = ', '.join(match)
            material = material.title()

            keywords = ['BT', 'AT&T', 'Sprint', 'T-Mobile', 'Verizon', 'Unlocked']
            keywords.append(material)
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = re.sub(r'\(.*\)', '', model)
            model = re.sub(r'\+', 'plus', model)
            model = re.sub(r'\s+', ' ', model).strip()

            screen_size = specifications.get('Key Specs', {}).get('Screen Size', '')
            screen_size = re.sub(re.compile('millimeters', re.IGNORECASE), 'mm', screen_size)
            screen_size = re.sub(r'\s+', ' ', screen_size).strip()

            exclude_keywords = [
                'Watch Band', 'Charging Pad', 'Charging Dock', 'Geek Squad Certified Refurbished'
            ]
            if re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', exclude_keywords))), re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": specifications.get('General', {}).get('Brand', ''),

                "model": model,
                "color": color,
                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },

                'title': title,
                "image": record.get('image', ''),
                "features": ', '.join(record.get('Features', '')),
                "screen_resolution": specifications.get('Display', {}).get('Screen Resolution', ''),
                "screen_size": screen_size,
                "item_condition": record.get('item_condition', ''),

                "model_number": specifications.get('General', {}).get('Model Number', ''),
                "product_line": specifications.get('General', {}).get('Model Family', ''),
                "compatibility": specifications.get('Key Specs', {}).get('Operating System Compatibility', ''),
                "carrier": specifications.get('Key Specs', {}).get('Carrier', ''),
                "material": material,
                "wireless_charging": specifications.get('Power', {}).get('Charging Interface(s)', ''),
                "battery_life": specifications.get('Key Specs', {}).get('Usage Time', ''),
                "operating_systems": specifications.get('Key Specs', {}).get('Operating System Compatibility', ''),

                "thickness": specifications.get('Dimension', {}).get('Case Thickness', ''),
                "width": specifications.get('Dimension', {}).get('Product Width', '') \
                    or specifications.get('Dimension', {}).get('Case Width', ''),
                "depth": specifications.get('Dimension', {}).get('Product Depth', ''),
                "weight": specifications.get('Dimension', {}).get('Product Weight', ''),
            }

            return transformed_record
        else:
            return None
