class Mapper:
    def map(self, record):
        model = ""
        model_sku=""

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity.get("attribute", []):
                    model = entity["attribute"][0]["id"]
            elif "model_sku" == entity['name']:
                if entity.get("attribute", []):
                    model_sku = entity.get("attribute", [{}])[0].get("id", '')

        key_field = model + model_sku
        return key_field
