import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('was_price', '')
            if not price:
                price = record.get('price', '')
            price = price.replace("$", "")

            specifications=record.get('specifications',{})

            # model_value = ' '+specifications.get('General', {}).get('Product Name', '')
            # model = re.findall(".Apple.*mm",model_value)[0]
            # model = model[:-4].strip()
            model = specifications.get('General', {}).get('Product Name', '')
            model = re.sub(re.compile(r'Apple -',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'\d+mm',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'Geek Squad Certified Refurbished',re.IGNORECASE),'',model)

            screen_size_value = specifications.get('Key Specs',{}).get('Screen Size','')
            screen_size = re.findall("[0-9]+",screen_size_value)[0]
            screen_size = screen_size + 'mm'

            model_sku_value = specifications.get('General',{}).get('Model Number','')
            if (re.match("GSRF", model_sku_value) is not None):
                model_sku_value = model_sku_value[5:]

            color_category = specifications.get('General', {}).get('Color Category', '')
            color = specifications.get('General', {}).get('Color', '')
            if (re.search("Aluminum", color) is not None):
                color = re.sub('Aluminum', '', color).strip()
            elif (re.search("Ceramic", color) is not None):
                color = re.sub('Ceramic', '', color).strip()
            elif (re.search("Stainless Steel", color) is not None):
                color = re.sub('Stainless Steel', '', color).strip()

            if (color ==''):
                color = color_category

            if re.findall(re.compile(r'\(GPS \+ Cellular\)|\(GPS\+Cellular\)',re.IGNORECASE), specifications.get('General', {}).get('Product Name', '')):
                connectivity = "GPS + Cellular"
            else:
                connectivity = "GPS"
            
            model = re.sub(re.compile(r'\(GPS \+ Cellular\)|\(GPS\+Cellular\)|\(GPS\)',re.IGNORECASE),'',model)
            model = model.strip(',')
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": specifications.get('General', {}).get('Brand', ''),

                "model": model,
                "case_size": screen_size,
                "color": color,
                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },
                "case_material": specifications.get('Material', {}).get('Case Material', ''),
                "gps": specifications.get('Key Specs',{}).get('Global Positioning',''),
                "water_resistant": specifications.get('Key Specs',{}).get('Water Resistant',''),
                "maximum_depth": specifications.get('Key Specs',{}).get('Maximum Depth of Water Resistance','') or specifications.get('Performance',{}).get('Maximum Depth of Water Resistance',''),
                "band_type":specifications.get('Watch',{}).get('Band Type',''),
                "model_sku": model_sku_value,
                "product_family": specifications.get('General',{}).get('Model Family',''),
                "series": specifications.get('General',{}).get('Series',''),
                "color_category":color_category ,
                "product_type": specifications.get('Features',{}).get('Product Type',''),
                "sensors": specifications.get('Activity',{}).get('Sensors',''),
                "band_color": specifications.get('Watch',{}).get('Band Color',''),
                "upc": specifications.get('Other',{}).get('UPC',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width','') or specifications.get('Dimension',{}).get('Case Width',''),
                "weight":  specifications.get('Dimension',{}).get('Product Weight',''),
                "depth": specifications.get('Dimension', {}).get('Product Depth', ''),
                "connectivity": connectivity,
                # "Product Name": specifications.get('General', {}).get('Product Name', ''),
            }

            return transformed_record
        else:
            return None
