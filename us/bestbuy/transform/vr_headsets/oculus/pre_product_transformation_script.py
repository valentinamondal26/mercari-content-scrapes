import hashlib
from colour import Color
import re
import difflib

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            ner_query = record.get('description', record.get('features', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()


            specifications=record.get('specifications',{})
            pattern=""
            price=record.get('price','')
            price=price.split('$')[1]
            price=price.replace(",","")
            title=record.get('title','')
            
            model=specifications.get('General',{}).get('Product Name','')
            product_types=['Go','Rift S','Quest']
            product_type=''
            pattern=re.compile(r"Oculus\s*\-\s*\w+\s*",re.IGNORECASE)
            if re.findall(pattern,title)!=[]:
                product_type=re.findall(pattern,title)[0].strip('Oculus - ').strip()
            if product_type not in product_types:
                for word in title.split():
                    best_match = difflib.get_close_matches(word,product_types)
                    if best_match!=[]:
                        product_type=best_match[0]
                        
            pattern=re.compile(r"\d+\s*GB|MB|TB|gaming|headsetS*|Oculus\s*\-|Vr|",re.IGNORECASE)
            model=re.sub(pattern,'',model)
        

            wordList = re.sub("[^\w]", " ",  model).split()
            _color = [i.strip(' ,') for i in wordList if self.check_color(i.strip(' ,'))]
            color=''
            for color in _color:
                if color:
                    model = model.replace(color, '')
            model=model.replace('-',' ')
            model=' '.join(model.split())
            model = model.title()
            d = {
                'Pc': 'PC',
                'For': 'for',
            }

            def pattern_word(word):
                return r'\b' + word + r'\b'

            model = re.sub(re.compile(r'|'.join([pattern_word(
                k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "VR Headsets",
                "image": record.get('image', ''),
                'brand':specifications.get('General',{}).get('Brand','Oculus'),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),                
                "features": record.get('features',''),
                "ner_query": ner_query,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "model": model,
                "title":specifications.get('General',{}).get('Product Name',''),
                "model_sku":specifications.get('General',{}).get('Model Number',''),
                "upc":specifications.get('Other',{}).get('UPC',''),
                "internal_memory_capacity":specifications.get('Features',{}).get('Internal Memory',''),
                "device_type":specifications.get('Key Specs',{}).get('Device Type',''),
                "sensor_type":specifications.get('Key Specs',{}).get('Sensors',''),
                "color":specifications.get('General',{}).get('Color',''),
                "control_type":specifications.get('Key Specs',{}).get('Control Type',''),
                "display_type":specifications.get('Display',{}).get('Display Type',''),
                "platform":specifications.get('Compatibility',{}).get('Compatible Devices',''),
                "item_condition":record.get('item_condition',''),
                "wired_wireless":specifications.get('Key Specs',{}).get('Wireless',''),
                "field_of_view":specifications.get('Display',{}).get('Field of View',''),
                "product_type":product_type
            }

            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False

