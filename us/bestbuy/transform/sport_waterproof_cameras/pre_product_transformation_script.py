import hashlib
import re

class Mapper(object):

    def transform_product_name_to_model(self, product_name, color):
        model = re.sub(re.compile(r'Waterproof|Action Camera|Digital Camera|Special Bundle|HD|4K|5.6K', re.IGNORECASE), '', product_name)
        # model = model.replace(color, '').strip()
        model = re.sub(r'\s+', ' ', model).strip()
        return model
         
    def map(self, record):
        if record:
            transformed_record = dict()
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
        
            specifications = record.get('specifications', {})
            key_specs = specifications.get('Key Specs', {})
            features_specs = specifications.get('Feature', {})
            included_specs = specifications.get('Included', {})
            other_specs = specifications.get('Other',{})
            compatibility_specs = specifications.get('Compatibility', {})
            camera_specs = specifications.get('Camera', {})
            flight_specs = specifications.get('Flight', {})
            power_specs = specifications.get('Power', {})
            general_specs = specifications.get('General', {})
            performance_specs = specifications.get('Performance', {})
            display_specs = specifications.get('Display', {}) 
            color = general_specs.get('Color', '') or ''
            color_category = general_specs.get('Color Category', '') or ''
            price = record['price']
            price = price.replace("$", "")
            megapixels = key_specs.get("Effective Megapixels", '')
            video_resolution = key_specs.get("Video Resolution (Recording)", '')
            memory_card_supported = compatibility_specs.get('Memory Card Compatibility','')
            wifi = key_specs.get('Wi-Fi Enabled','')
            water_resistant = performance_specs.get('Water Resistant','')
            charger_type = power_specs.get('Charging Interface(s)','')
            hdr =features_specs.get('HDR Mode','')
            upc =other_specs.get('UPC','')
            model_sku =general_specs.get('Model Number','')
            touch_screen =display_specs.get('Touch Screen','')
            brand = general_specs.get('Brand','')
            product_name = general_specs.get('Product Name', '')   
            model = self.transform_product_name_to_model(product_name, color)
              
            
            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "color": color,
                "model":model,
                "color_category": color_category,
                 "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "video_resolution": video_resolution,
                "megapixels":megapixels, 
                "memory_card_supported" : memory_card_supported,
                "wifi" : wifi,
                "water_resistant" : water_resistant,
                "charger_type":charger_type,
                "upc" : upc,
                "model_sku" : model_sku,
                "hdr" : hdr,
                "touch_screen" :touch_screen, 
                "brand" : brand,
            }

            return transformed_record
        else:
            return None

if __name__ == "__main__":
    import sys
    scrape_proj_path = '/home/salman/github/mercari/mercari-content-scrapes/'
    if scrape_proj_path not in sys.path:
        sys.path.append(scrape_proj_path)
    from utils.transformation_util import sample_pre_product_script
    path='/home/salman/Desktop/debug/bestbuy.com/sport_waterproof_cameras/'
    schema = None#'/home/salman/Desktop/schema.json'
    sample_pre_product_script('gs://content_us/raw/bestbuy.com/sport_waterproof_cameras/gopro/2019-12-09_16_53_46/json/bestbuy.txt',
        path+'go_pro_pre.jl', Mapper, schema)
