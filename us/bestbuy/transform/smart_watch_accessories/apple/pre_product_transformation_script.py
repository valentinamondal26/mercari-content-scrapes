import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('was_price', '')
            if not price:
                price = record.get('price', '')
            price = price.replace("$", "")

            specifications = record.get('specifications', {})

            color_category = specifications.get('General', {}).get('Color Category', '')
            color = specifications.get('General', {}).get('Color', '') or color_category

            model = specifications.get('General', {}).get('Product Name', '')
            model = re.sub(r'™|\s(-)\s', ' ', model)
            pattern_measurements = r"(\d+.*(mm|'))"
            match = re.findall(pattern_measurements, model)
            if match:
                model = re.sub(pattern_measurements, '', model) + ' ' + match[0][0]
            model = re.sub(r'\(|\)', '', model)
            model = re.sub(r'(.*)(for Apple Watch)(.*)', r'\1\3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'(^Apple Watch)(.*)', r'\2 for \1', model, flags=re.IGNORECASE)
            if not re.findall(r'for Apple Watch', model, flags=re.IGNORECASE):
                model += ' for Apple Watch'
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": specifications.get('General', {}).get('Brand', ''),

                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "model_number": specifications.get('General', {}).get('Model Number', ''),
                "color": color,

                "upc": specifications.get('Other', {}).get('UPC', ''),

                'price': {
                    'currencyCode': 'USD',
                    'amount': price,
                },
            }

            return transformed_record
        else:
            return None
