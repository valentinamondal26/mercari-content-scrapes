#Key Mapper for Bestbuy data - upc is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        entities = record['entities']
        for entity in entities:
            if "upc" == entity['name']:
                key_field += entity["attribute"][0]["id"]
                break

        return key_field
