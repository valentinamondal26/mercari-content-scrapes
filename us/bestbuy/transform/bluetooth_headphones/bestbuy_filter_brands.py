
if __name__ == "__main__":

    import json
    brand_list = ['Beats by Dr. Dre']
    count = 0
    path = '/home/salman/Desktop/debug/bestbuy.com/bluetooth_headphones/'
    with open(path+'bluetooth_headphones.txt', 'r') as f:
        content = f.readlines()
    for line in content:
        product = json.loads(line)
        brand = product.get("specifications", {}).get('General', {}).get('Brand', '')
        # print(brand)
        if brand in brand_list:
            with open(path+'bluetooth_headphones_brand_filtered.txt', "a") as f:
                f.write(line)
                count = count + 1
        # else:
        #     count += 1
        #     print(line.get("brand"), count)
    print('count:', count)
