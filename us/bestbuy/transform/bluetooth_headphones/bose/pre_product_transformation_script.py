import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color = re.sub(re.compile(r'\(The Beats Decade Collection\)|\(The Beats Icon Collection\)', re.IGNORECASE), '', color)
            color = re.sub(' +', ' ', color).strip()

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')
            brand = re.sub(re.compile(r'®', re.IGNORECASE), '', brand)
            brand = re.sub(' +', ' ', brand).strip()

            connection_type = specifications.get('Connectivity', {}).get('Connection Type', '') or specifications.get('Key Specs', {}).get('Connection Type', '')
            
            

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            model = model.replace('UrBeats', 'urBeats')\
                .replace('Solo 2', 'Solo2')\
                .replace('Powerbeats3', 'Powerbeats³')\
                .replace('Studio3', 'Studio³')\
                .replace('Solo3', 'Solo³')\
                .replace('BeatsX Wireless', 'BeatsX')
            model = re.sub(re.compile(r'\bEarphones\b|Earbud|Over-Ear|Over-the-Ear|On-Ear|In-Ear|'
                r'\bHeadphones\b|Noise Canceling|Noise Cancelling|by Dr. Dre|Clip-On|Geek Squad Certified Refurbished|'
                r'™|®|-Enhancing|with Bluetooth Connectivity|wireless',
                re.IGNORECASE), '', model)
            model = re.sub(re.compile(color, re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r'Open Box Excellent Condition|Geek Squad Certified Refurbished',
                re.IGNORECASE), model):
                return None
            if re.findall(re.compile(r'Wired|wired', re.IGNORECASE), connection_type):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "color": color,
                "brand": brand,
                "model_sku": specifications.get('General',{}).get('Model Number','') or \
                    specifications.get('Other',{}).get('Model Number',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width',''),
                "weight": specifications.get('Dimension',{}).get('Product Weight',''),
                "upc": specifications.get('Other',{}).get('UPC',''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "battery_life": specifications.get('Power',{}).get('Battery Life',''),
                "charging_case_battery_life": specifications.get('Power',{}).get('Battery Life Provided by Charging Case',''),
                "connection_type": specifications.get('Connectivity',{}).get('Connection Type','') or specifications.get('Key Specs',{}).get('Connection Type',''),
                "charger_type": specifications.get('Power',{}).get('Charging Interface(s)',''),
                "fit": specifications.get('Design',{}).get('Headphone Fit','') or specifications.get('Key Specs',{}).get('Headphone Fit',''),
                "foldable_design": specifications.get('Design',{}).get('Foldable Design',''),
                "active_noise_canceling": specifications.get('Audio',{}).get('Noise Canceling (Active)','') or specifications.get('Key Specs',{}).get('Noise Canceling (Active)',''),
                "water_resistance": specifications.get('Features',{}).get('Water Resistant','') or specifications.get('Key Specs',{}).get('Water Resistant',''),
                "wireless_technology": specifications.get('Connectivity',{}).get('Wireless Connectivity','') or specifications.get('Key Specs',{}).get('Wireless Connectivity',''),
                "color_category": color_category,
            }

            return transformed_record
        else:
            return None
