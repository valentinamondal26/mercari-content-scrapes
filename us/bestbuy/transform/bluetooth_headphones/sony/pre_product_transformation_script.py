import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')
            brand = re.sub(re.compile(r'®', re.IGNORECASE), '', brand)
            brand = re.sub(' +', ' ', brand).strip()

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            model = re.sub(re.compile(r'\bwith Google Assistant\b', re.IGNORECASE), '', model)
            d = {
                'In-Ear': 'In Ear',
                'Behind-the-Ear': 'Behind the Ear',
                'Over-the-Ear': 'Over the Ear',
                'On-Ear': 'On Ear',
            }
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', d.keys()))), re.IGNORECASE), lambda x: d.get(x.group(), x.group()), model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": record.get('brand', '') or brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,

                "model": model,
                "model_sku": specifications.get('General', {}).get('Model Number', '') or \
                    specifications.get('Other', {}).get('Model Number', ''),
                "series": specifications.get('General', {}).get('Series', ''),
                "color": color,
                'microphone': specifications.get('Features', {}).get('Built-In Microphone', ''),
                "battery_life": specifications.get('Power', {}).get('Battery Life', ''),
                "height": specifications.get('Dimension', {}).get('Product Height', ''),
                "width": specifications.get('Dimension', {}).get('Product Width', ''),
                "depth": specifications.get('Dimension', {}).get('Product Depth', ''),
                "weight": specifications.get('Dimension', {}).get('Product Weight', ''),

                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "upc": specifications.get('Other',{}).get('UPC',''),
                "color_category": color_category,
            }

            return transformed_record
        else:
            return None
