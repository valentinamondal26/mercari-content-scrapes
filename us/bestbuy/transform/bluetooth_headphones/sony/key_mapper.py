#Key Mapper for Bestbuy data - upc is used to de-dup items
class Mapper:
    def map(self, record):

        model = ''
        model_sku = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "model_sku" == entity['name']:
                if entity['attribute']:
                    model_sku = entity["attribute"][0]["id"]

        key_field = model + model_sku
        return key_field
