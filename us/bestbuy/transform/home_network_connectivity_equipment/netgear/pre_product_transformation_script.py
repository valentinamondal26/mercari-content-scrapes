import hashlib
import re

from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications = record.get('specifications', {})

            # color transformations
            color = specifications.get('Other', {}).get('Color', '') or ''
            if not color:
                color = specifications.get('General', {}).get('Color', '') or ''

            color_category = specifications.get('Other', {}).get(
                    'Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General', {}).get(
                        'Color Category', '') or ''

            color = color or color_category
            color = re.sub(r'\bMulti\b', 'Multicolor', color,
                           flags=re.IGNORECASE)

            brand = specifications.get('General', {}).get(
                    'Brand', '') or specifications.get('Other', {}).get(
                    'Brand', '') or record.get('sub_category', '')

            # model transformations
            model = specifications.get('General', {}).get(
                    'Product Name', '') or specifications.get(
                    'Other', {}).get('Product Name', '')
            keywords = [brand, color]
            model = re.sub(re.compile(r'|'.join(
                    list(map(lambda x: r'\b' + x + r'\b', keywords)))), '',
                    model)
            model = re.sub(r'-\s*$|®|\(|\)|\s\-\s|\s-$', ' ', model,
                           re.IGNORECASE)
            model = re.sub(r'\,\s*Seamless Roaming\s*\,\s*One WiFi Name\s*\,\s*'
                           r'Works with any WiFi Router\s*\(*\s*EX8000\s*\)*|'
                           r'\s*,\s*Voice support|'
                           r'\d*\/\d+\s*(mbps)*|\d+\/\d*\s*(mbps)*',
                           # r'10/100/1000/2500/5000\s*(Mbps)*|'
                           # r'10/100/1000\s*(Mbps)*',
                           '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bWi-Fi\b', 'WiFi', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(r'Mesh Wi-*Fi \d*\s*system', model,
                          flags=re.IGNORECASE):
                product_type = 'Mesh Wi-Fi systems'
            elif re.findall(r'Wi-*Fi \d*\s*router', model, flags=re.IGNORECASE):
                product_type = 'Wi-Fi routers'
            elif re.findall(r'Cable modem', model, flags=re.IGNORECASE):
                product_type = 'Cable modems'
            elif re.findall(
                    r'Wi-*Fi \d*\s*(range|mesh)*\s*extender|Network extender',
                    model, flags=re.IGNORECASE):
                product_type = 'Wi-Fi extenders'
            elif re.findall(r'switch', model, flags=re.IGNORECASE):
                product_type = 'Network switches'
            else:
                product_type = 'Networking Accessories'

            product_type = titlecase(product_type)

            transformed_record = {
                    "id": record.get('id', ''),
                    "item_id": hex_dig,
                    "crawl_date": record.get('crawlDate', ''),
                    "status_code": record.get('statusCode', ''),

                    "category": record.get('category', ''),
                    "brand": record.get('brand', '') or brand,

                    "image": record.get('image', ''),
                    "breadcrumb": record.get('breadcrumb', ''),
                    "description": record.get('description', ''),
                    "title": record.get('title', ''),
                    "ner_query": ner_query,

                    "model": model,
                    "model_sku": specifications.get('General', {}).get(
                            'Model Number', '') or specifications.get(
                            'Other', {}).get('Model Number', ''),
                    "color": color,

                    'product_type': product_type,
                    'wifi_speed': specifications.get('Key Specs', {}).get(
                            'Wi-Fi Speed', ''),
                    'band_type': specifications.get('Key Specs', {}).get(
                            'Band Technology', ''),

                    "height": specifications.get('Dimension', {}).get(
                            'Product Height', ''),
                    "length": specifications.get('Dimension', {}).get(
                            'Product Length', ''),
                    "width": specifications.get('Dimension', {}).get(
                            'Product Width', ''),
                    "weight": specifications.get('Dimension', {}).get(
                            'Product Weight', ''),

                    "price": {
                            "currency_code": "USD",
                            "amount": price
                    },
                    "upc": specifications.get('Other', {}).get('UPC', ''),
            }

            return transformed_record
        else:
            return None
