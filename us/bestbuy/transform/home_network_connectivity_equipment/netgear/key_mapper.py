#Key Mapper for Bestbuy data - model + color is used to de-dup items
class Mapper:
    def map(self, record):

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name'] and entity["attribute"]:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name'] and entity["attribute"]:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = model +  color
        return key_field
