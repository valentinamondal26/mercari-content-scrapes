import hashlib
import re
from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            if re.findall(r"\d+\.*\d*", price):
                price = re.findall(r"\d+\.*\d*", price)[0]
            specifications = record.get('specifications', {})

            color = specifications.get('Other', {}).get('Color', '') or ''
            if not color:
                color = specifications.get(
                    'General', {}).get('Color', '') or ''

            color_category = specifications.get(
                'Other', {}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get(
                    'General', {}).get('Color Category', '') or ''

            brand = specifications.get('General', {}).get('Brand', '') \
                or specifications.get('Other', {}).get('Brand', '') \
                or record.get('sub_category', '')

            model = specifications.get('General', {}).get(
                'Product Name', '') or specifications.get('Other', {}).get('Product Name', '')
            keywords = []
            keywords.append(brand)
            keywords.append(color)
            model = re.sub(re.compile(
                r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords)))), '', model)
            model = re.sub(
                r'-\s*$|®|\(\s*Latest Model\s*\)|\(|\)|\s\-\s|\s-$|Works with Chromebook|\bfor (PC\b|PlayStation 4|XBOX ONE|Windows|\sand\s|,\s)*', ' ', model, re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)
            model = re.sub(re.compile(r"\bAnd\b", re.IGNORECASE), "and", model)
            model = re.sub(re.compile(r"\bOr\b", re.IGNORECASE), "or", model)
            model = re.sub(re.compile(
                r"\bWith\b", re.IGNORECASE), "with", model)
            # d = {
            #     'And': 'and',
            #     'With': 'with',
            #     'Or': 'or'
            # }

            # def pattern_word(word):
            #     return r'\b' + word + r'\b'
            # model = re.sub(re.compile(r'|'.join([pattern_word(
            #     k) for k in d.keys()]), re.IGNORECASE), lambda x: d.get(x.group()), model)
            model = re.sub(re.compile(r'Keyboard with RGB Backlighting',re.IGNORECASE),'RGB Keyboard',model)
            model = re.sub(re.compile(r'Keyboard with Backlighting',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'Mac and PlayStation 4',re.IGNORECASE),'',model)
            if 'Cherry' in model and 'Red' in model:
                color = color+'/Cherry Red'
            model = re.sub(re.compile(r'\bCherry\b|\bRed\b',re.IGNORECASE),'',model)
            model = re.sub(r'\s+',' ',model).strip()

            if 'G915 TKL Tenkeyless LIGHTSPEED Wireless RGB Mechanical Gaming GL Clicky Switch Keyboard with Back Lighting' in record.get('title',''):
                model = 'G915 TKL Tenkeyless LIGHTSPEED Wireless  Mechanical Gaming RGB Keyboard'

            product_types = record.get('Category', '')
            product_type = ', '.join(product_types)
            product_type_meta = [r'Mice \& Keyboards', 'Computer Speakers',
                                 r'Webcams*', r'Headset \& Microphones',
                                 r'Mousepad \& Charging Pad',
                                 r'Gaming Accessories \& Peripherals']
            if not product_type:
                breadcumb = ", ".join(record.get("breadcrumb", "").split("|"))
                for pr_type in product_type_meta:
                    if re.findall(re.compile(r"^{}\s*\,\s|\,\s{}\s*\,\s|\,\s{}\s*$".format(pr_type, pr_type, pr_type), re.IGNORECASE), breadcumb):
                        product_type = pr_type.replace(r"\&", "&")

            if 'Headset' in model:
                product_type = 'Headsets & Microphones'
            elif 'Webcam' in model:
                product_type = 'Webcams'
            elif 'Speaker' in model:
                product_type = 'Computer Speakers'
            if "Mouse" in model or "Keyboard" in model or "Mice" in model: # re.findall(re.compile(r"Gaming Accessories & Peripherals", re.IGNORECASE), product_type)
                product_type = "Mice & Keyboards"

            if re.findall(re.compile(r"Mouse\s*pad|Charging\s*Pad", re.IGNORECASE), record.get("title", "")):
                product_type = "Mousepad & Charging Pad"
            if re.findall(re.compile(r"Joystick|shifter|\bwheel\b", re.IGNORECASE), record.get("title", "")):
                product_type = "Gaming Accessories & Peripherals"
            product_type = re.sub(re.compile(
                r"Gaming Accessories & Peripherals", re.IGNORECASE), "Gaming Peripherals", product_type)

            if product_type == "Mice & Keyboards":
                if re.findall(re.compile(r"Mice|Mouse", re.IGNORECASE), model) and re.findall(re.compile(r"keyboard", re.IGNORECASE), model):
                    product_type = "Keyboards"
                elif re.findall(re.compile(r"Mice|Mouse", re.IGNORECASE), model) and not re.findall(re.compile(r"keyboard", re.IGNORECASE), model):
                    product_type = "Mice"
                elif (not re.findall(re.compile(r"Mice|Mouse", re.IGNORECASE), model) and re.findall(re.compile(r"keyboard", re.IGNORECASE), model)) or (re.findall(re.compile(r"\bPalm\b", re.IGNORECASE), model)):
                        product_type = "Keyboards"
                else:
                    product_type = 'Mice'

            d = {
                'G PRO X Wireless DTS Headphone:X 2.0 Gaming Headset with Blue VO!CE Mic Filter Tech and LIGHTSPEED Wireless': 'G PRO X Wireless DTS LIGHTSPEED Gaming Headset',
                '910-001354 R400 Presenter Remote Control': 'R400 Presenter Remote Control',
                'ERGO K860 Ergonomic Split Bluetooth or USB Keyboard': 'ERGO K860 Ergonomic Split Bluetooth/USB Keyboard',
                'G Pro Hero Wired Optical Gaming Mouse with LIGHTSYNC RGB Lighting': 'G Pro Hero Wired LIGHTSYNC RGB Gaming Mouse',
                'G Pro Mechanical Wired Gaming GX Blue Clicky Switch Keyboard with RGB Back Lighting': 'G Pro Wired Gaming RGB Mechanical Keyboard',
                'G560 LIGHTSYNC 2.1 Bluetooth Gaming Speakers with Game Driven RGB Lighting 3-Piece': 'G560 LIGHTSYNC 2.1 Bluetooth RGB Gaming Speakers 3-Piece',
                'G635 Wired 7.1 Surround Sound Gaming Headset with LIGHTSYNC RGB Lighting': 'G635 Wired 7.1 Surround Sound RBG Gaming Headset',
                'G915 LIGHTSPEED Wireless RGB Mechanical Clicky Switch Gaming Keyboard': 'G915 LIGHTSPEED Wireless RGB Mechanical Linear Switch Gaming Keyboard',
                'G915 LIGHTSPEED Wireless RGB Mechanical Gaming Keyboard with GL Linear Switch': 'G915 LIGHTSPEED Wireless RGB Mechanical Linear Switch Gaming Keyboard',
                'G915 LIGHTSPEED Wireless RGB Mechanical Gaming Keyboard with GL Tactile Switch': 'G915 LIGHTSPEED Wireless RGB Mechanical Tactile Switch Gaming Keyboard',
                'MX Keys Wireless Membrane Keyboard Mac with Smart Illumination': 'MX Keys Wireless Membrane Smart Illumination Mac Keyboard',
                'Keyboard Mac': 'Mac Keyboard',
                'G Pro': 'G PRO',
            }

            for key, value in d.items():
                model = re.sub(key, value, model, flags=re.IGNORECASE)
            model = re.sub(r'(.*) (Keyboard|Gaming Keyboard) (with) (.*)', r'\1 \4 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Bundle', 'Package']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": record.get('brand', '') or brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', ''),
                "ner_query": ner_query,
                "model_sku": specifications.get('General', {}).get('Model Number', '') \
                    or specifications.get('Other', {}).get('Model Number', ''),
                "title": record.get('title', ''),
                "model": model,
                "color": color.title() or color_category.title(),
                'product_type': product_type,
                "battery_size": specifications.get('Power', {}).get('Battery Size', ''),

                "height": specifications.get('Dimension', {}).get('Product Height', ''),
                "width": specifications.get('Dimension', {}).get('Product Width', ''),
                "weight": specifications.get('Dimension', {}).get('Product Weight', ''),

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "upc": specifications.get('Other', {}).get('UPC', ''),
            }

            return transformed_record
        else:
            return None
