import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications = record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            brand = specifications.get('General',{}).get('Brand','') \
                or specifications.get('Other',{}).get('Brand','') \
                or record.get('sub_category', '')

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            keywords = []
            keywords.append(brand)
            keywords.append(color)
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords)))), '', model)
            model = re.sub(r'-\s*$|®|\(\s*Latest Model\s*\)|\(|\)|\s\-\s|\s-$', ' ', model)
            compatability = ["Macbook","iPad", "iMac"]
            for comp in compatability:
                model = re.sub(re.compile(r"{}\s*([pro|air]+)\s*and\s*{}\s*([pro|air]+)\s*and*\s*([\d\.\"]*)".format(comp,comp), re.IGNORECASE), r"{} \1/\2/\3".format(comp), model)
                model = re.sub(re.compile(r"{}\s*([pro|air|\d\"]+)\s*and\s*{}\s*([pro|air|\d\"]+)".format(comp,comp), re.IGNORECASE),r"{} \1/\2".format(comp), model)
            model = re.sub(re.compile(r"(\d+\w+)\s*[Generation]*\s*(\d*)\s*[and]*\s*(\d+\w+)\s*Generation\s*(\d*)", re.IGNORECASE),r"\1 \2/\3 \4 Generation", model)
            model = re.sub(r"([\d\w]+)\s*\/\s*(.*)",r"\1/\2",model)
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(re.compile(r"Geek Squad|Refurbished" , re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": record.get('brand', '') or brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),

                "model": model,
                "model_sku": specifications.get('General', {}).get('Model Number', '') or \
                    specifications.get('Other', {}).get('Model Number', ''),
                "color": color or color_category,

                'product_type': specifications.get('Connectivity', {}).get('Cable Type', ''),
                "connector_adapter_type": specifications.get('Connectivity', {}).get('Connector Type', ''),
                "item_condition": record.get('item_condition', ''),
                "cable_length": specifications.get('Key Specs', {}).get('Cable Length', '') \
                    or specifications.get('Connectivity', {}).get('Cable Length', ''),

                "height": specifications.get('Dimension', {}).get('Product Height', ''),
                "width": specifications.get('Dimension', {}).get('Product Width', ''),
                "weight": specifications.get('Dimension', {}).get('Product Weight', ''),

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "upc": specifications.get('Other',{}).get('UPC',''),
            }

            return transformed_record
        else:
            return None
