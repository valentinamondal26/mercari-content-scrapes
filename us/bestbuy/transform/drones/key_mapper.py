#Key Mapper for Bestbuy data - model is used to de-dup items
class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]
            if "color" == entity['name']:
                if entity['attribute']!=[]:
                    key_field += entity["attribute"][0]["id"]

        return key_field
