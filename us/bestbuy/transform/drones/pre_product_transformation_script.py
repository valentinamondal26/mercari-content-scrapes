import hashlib
import re

class Mapper(object):
    def post_process(self, record):
        def trim(value):
            return re.sub(re.compile(r'\s+'), ' ', value).strip() if isinstance(value, str) else value
        record = {k: trim(v) for k, v in record.items()}
        return record

    def transform_product_name_to_model(self, product_name, color):
        model = re.sub(re.compile(r'with(.*)|Fly More Combo', re.IGNORECASE),'', product_name)
        model = model.replace(color, '').strip()
        return model
         
    def map(self, record):
        if record:
            transformed_record = dict()
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
        
            specifications = record.get('specifications', {})
            key_specs = specifications.get('Key Specs', {})
            features_specs = specifications.get('Features', {})
            included_specs = specifications.get('Included', {})
            other_specs = specifications.get('Other',{})
            compatibility_specs = specifications.get('Compatibility', {})
            camera_specs = specifications.get('Camera', {})
            flight_specs = specifications.get('Flight', {})
            power_specs = specifications.get('Power', {})
            general_specs = specifications.get('General', {})
            price = record['price']
            price = price.replace("$", "")
            color = general_specs.get('Color', '') or ''
            color_category = general_specs.get('Color Category', '') or ''

            product_name = general_specs.get('Product Name', '')   
            model = self.transform_product_name_to_model(product_name, color)
            model = re.sub(r'\bDrone\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()

            indoor_outdoor_use = features_specs.get("Indoor Or Outdoor Use", '')
            memory_cards_supported = included_specs.get("Memory Card Compatibility", '')
            weight = key_specs.get("Product Weight", '')
            video_resolution = key_specs.get("Video Resolution", '')
            megapixels = key_specs.get("Total Megapixels", '')
            product_line = general_specs.get('Series', '')
            field_of_view = features_specs.get('Field of View', '')
            camera_integration = compatibility_specs.get('Camera Compatibility','')
            rotor_number = flight_specs.get('Number of Rotors','')
            battery_life = power_specs.get('Battery Capacity','')
            battery_voltage =power_specs.get('Battery Voltage','')
            product_type = other_specs.get('Product Set', '') 
            model_sku = general_specs.get('Model Number', '')
            upc = other_specs.get('UPC', '')
            interface = power_specs.get('Interface(s)','')
            brand = general_specs.get('Brand', '')

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "color": color,
                "model":model,
                "color_category": color_category,
                 "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "indoor_outdoor_use": indoor_outdoor_use,
                "memory_cards_supported": memory_cards_supported,
                "weight": weight,
                "video_resolution": video_resolution,
                 "megapixels":megapixels, 
                "product_line" : product_line,
                "field_of_view" : field_of_view,
                "camera_integration" : camera_integration,
                "rotor_number" : rotor_number,
                "battery_life" : battery_life,
                "battery_voltage" : battery_voltage,
                "product_type" :product_type, 
                "model_sku" : model_sku,
                "upc" :upc,
                "interface" : interface,
                "brand" : brand
            }

            return self.post_process(transformed_record)
        else:
            return None