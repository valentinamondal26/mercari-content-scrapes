import hashlib
import re

def to_bytes(a, unit):
    B = 1
    KB = float(1000)
    MB = float(KB ** 2)
    GB = float(KB ** 3)
    TB = float(KB ** 4)
    d = {
        'B': B,
        'K': KB,
        'M': MB,
        "G": GB,
        "T": TB,
    }
    x = a * d.get(unit.upper())
    return int(x)

def humanbytes(B):
    'Return the given bytes as a human friendly KB, MB, GB, or TB string'
    B = float(B)
    KB = float(1000)
    MB = float(KB ** 2)
    GB = float(KB ** 3)
    TB = float(KB ** 4)

    if B < KB:
        return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
    elif KB <= B < MB:
        x = B/KB
        return '{0} KB'.format(round(x) if x.is_integer() else round(x, 2))
    elif MB <= B < GB:
        x = B/MB
        return '{0} MB'.format(round(x) if x.is_integer() else round(x, 2))
    elif GB <= B < TB:
        x = B/GB
        return '{0} GB'.format(round(x) if x.is_integer() else round(x, 2))
    elif TB <= B:
        x = B/TB
        return '{0} TB'.format(round(x) if x.is_integer() else round(x, 2))

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications', {})
            features = ''
            features_dict = record.get('features', {})
            features_dict = dict((k, v) for k, v in features_dict.items() if k and v)
            features = ' '.join(map(': '.join, features_dict.items()))

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            if not color:
                color = color_category

            color_remove_keywords = [
                'Mgcote', 'With Black Carbon Fiber', 'With Black Interior', 'HP Finish In'
            ]
            color = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color_remove_keywords))), re.IGNORECASE), '', color)
            color = re.sub(r'\s*\+\s*', '/', color)
            color = re.sub(r'\s+', ' ', color).strip()

            processor_model = specifications.get('Key Specs', {}).get('Processor Model', '') or specifications.get('Processor', {}).get('Processor Model', '')
            processor_model = re.sub(re.compile(r'Non-AMD/Intel processor', re.IGNORECASE), '', processor_model)
            if not processor_model:
                processor_model_meta = [
                    'Intel Celeron',
                ]
                match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', processor_model_meta))), re.IGNORECASE), record.get('title', ''))
                if match:
                    processor_model = match[0].strip()

            storage_capacity = specifications.get('Key Specs', {}).get('Total Storage Capacity', '') or specifications.get('Storage', {}).get('Total Storage Capacity', '')
            match = re.findall(re.compile(r"(\d+\.*\d*)\s*(Terabytes|Gigabytes|Megabytes|Kilobytes)", re.IGNORECASE), storage_capacity)
            if match:
                storage_capacity = humanbytes(to_bytes(float(match[0][0]), match[0][1][0]))

            hdd_storage_capacity = specifications.get('Key Specs', {}).get('Hard Drive Capacity', '') or specifications.get('Storage', {}).get('Hard Drive Capacity', '')
            match = re.findall(re.compile(r"(\d+\.*\d*)\s*(Terabytes|Gigabytes|Megabytes|Kilobytes)", re.IGNORECASE), hdd_storage_capacity)
            if match:
                hdd_storage_capacity = humanbytes(to_bytes(float(match[0][0]), match[0][1][0]))

            ssd_storage_capacity = specifications.get('Key Specs', {}).get('Solid State Drive Capacity', '') or specifications.get('Storage', {}).get('Solid State Drive Capacity', '')
            match = re.findall(re.compile(r"(\d+\.*\d*)\s*(Terabytes|Gigabytes|Megabytes|Kilobytes)", re.IGNORECASE), ssd_storage_capacity)
            if match:
                ssd_storage_capacity = humanbytes(to_bytes(float(match[0][0]), match[0][1][0]))
            if hdd_storage_capacity and ssd_storage_capacity:
                storage_capacity = f'{hdd_storage_capacity} HDD + {ssd_storage_capacity} SSD'

            if not storage_capacity:
                storage_capacity = ssd_storage_capacity

            memory_ram_capacity = specifications.get('Key Specs', {}).get('System Memory (RAM)', '') or specifications.get('Memory', {}).get('System Memory (RAM)', '')
            match = re.findall(re.compile(r"(\d+\.*\d*)\s*(Terabytes|Gigabytes|Megabytes|Kilobytes)", re.IGNORECASE), memory_ram_capacity)
            if match:
                memory_ram_capacity = humanbytes(to_bytes(float(match[0][0]), match[0][1][0]))
            # memory_ram_capacity =  re.sub(re.compile(r"(\d+\.*\d*)\s*((T)erabytes*|(G)igabytes*|(M)egabytes*|(K)ilobytes*)+", re.IGNORECASE), r"\1\4B", memory_ram_capacity.title())
            # storage_capacity =  re.sub(re.compile(r"(\d+\.*\d*)\s*(Terabytes|Gigabytes|Megabytes|Kilobytes)", re.IGNORECASE), r"\1\4B", storage_capacity.title())

            # if memory_ram_capacity !='' and float(re.findall(r"\d+\.*\d*", memory_ram_capacity)[0]) >= 1000.0:
            #     memory_ram_capacity =  str(int(float(re.findall(r"\d+\.*\d*", memory_ram_capacity)[0]) / 1000.0))+"TB"
            # if storage_capacity!='' and float(re.findall(r"\d+\.*\d*", storage_capacity)[0]) >= 1000.0:
            #     storage_capacity =  str(int(float(re.findall(r"\d+\.*\d*", storage_capacity)[0]) / 1000.0))+"TB"

            brand = specifications.get('General', {}).get('Brand', '') or specifications.get('Other', {}).get('Brand', '')
            model_sku = specifications.get('General', {}).get('Model Number', '') or specifications.get('Other', {}).get('Model Number', '')

            processor_model_number = specifications.get('Key Specs',{}).get('Processor Model Number', '') or specifications.get('Processor',{}).get('Processor Model Number', '')

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            model = model.split(' - ')[0]
            keywords = ['with Touch ID', 'Latest Model', 'Intel Core i7',
                # 'netbook', 'notebook', 'Laptop',
                brand, color, processor_model, storage_capacity]
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\d+\.*\,*\d*\s*"\s*(Display)*', re.IGNORECASE), '', model)
            model = re.sub(re.compile(r"Macbook", re.IGNORECASE), "MacBook", model)
            model = re.sub(re.compile(r'OMEN by Gaming', re.IGNORECASE), 'OMEN Gaming', model)
            model = re.sub(r'\(\s*\)|®|™|-\s*$', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            gpu_video_card = specifications.get('Key Specs', {}).get('Graphics', '') or specifications.get('Graphics', {}).get('Graphics', '')
            if not gpu_video_card or re.findall(re.compile(r'\bother\b', re.IGNORECASE), gpu_video_card):
                for name, value in features_dict.items():
                    if re.findall(re.compile(r'\bvideo memory\b', re.IGNORECASE), value) \
                        or re.findall(re.compile(r'\bGraphics\b', re.IGNORECASE), name):
                        gpu_video_card = name
                        gpu_video_card = re.sub(r'®|™', '', gpu_video_card)
                        gpu_video_card = re.sub(r'\s+', ' ', gpu_video_card).strip()
            gpu_video_card = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['none', 'other']))), re.IGNORECASE), '', gpu_video_card).strip()
            gpu_video_card = re.sub(r'\bgraphics\b', lambda x: x.group().title(), gpu_video_card)

            if re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['bundle', 'included', 'Refurbished', 'Pre-Owned']))), re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category', ''),
                "brand": brand,

                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "title": record.get('title', ''),
                "features": features,
                "ner_query": ner_query,
                "description": record.get('description',''),

                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "color": color,
                "processor_model": processor_model,
                "max_storage_capacity": storage_capacity,
                "screen_size": specifications.get('Key Specs', {}).get('Screen Size', '') or specifications.get('Display', {}).get('Screen Size', ''),
                "memory_ram_capacity": memory_ram_capacity,
                "operating_system": specifications.get('Key Specs', {}).get('Operating System', '') or specifications.get('Compatibility', {}).get('Operating System', ''),
                "battery_life": specifications.get('Key Specs', {}).get('Battery Life', '') or specifications.get('Power', {}).get('Battery Life', ''),
                "model_sku": model_sku,
                "width": specifications.get('Dimension',{}).get('Product Width', ''),
                "height": specifications.get('Dimension',{}).get('Product Height', ''),
                "depth": specifications.get('Dimension',{}).get('Product Depth', ''),
                "weight": specifications.get('Dimension',{}).get('Product Weight', ''),

                "gpu_video_card": gpu_video_card,

                # not requested in the latest requirement, but keep it here so that raw_items has this info
                "color_category": color_category,
                "body_material": specifications.get('General', {}).get('Casing Material', ''),

                "touch_screen": specifications.get('Key Specs', {}).get('Touch Screen', '') or specifications.get('Display', {}).get('Touch Screen', ''),
                "display_resolution": specifications.get('Display', {}).get('Screen Resolution', ''),
                "upc": specifications.get('Other', {}).get('UPC', ''),
                "display_type": specifications.get('Display', {}).get('Display Type', ''),
                "battery_type": specifications.get('Key Specs', {}).get('Battery Type', '') or specifications.get('Power', {}).get('Battery Type', ''),
                "optical_drive_type": specifications.get('Included', {}).get('Optical Drive Type', ''),
                "processor_manufacturer": specifications.get('Processor', {}).get('Processor Brand', ''),
                "processor_speed": specifications.get('Key Specs', {}).get('Processor Speed (Base)', '') or specifications.get('Processor', {}).get('Processor Speed (Base)', ''),
                "processor_model_number": '' if processor_model_number.lower() == 'not available' else processor_model_number,

                "chipset_gpu_manufacturer": specifications.get('Graphics', {}).get('GPU Brand', ''),
                "gpu_video_memory": specifications.get('Graphics', {}).get('Video Memory', ''),
                "memory_type_ram": specifications.get('Memory', {}).get('Type of Memory (RAM)', ''),
                "memory_speed_ram": specifications.get('Memory', {}).get('System Memory RAM Speed', ''),
                "storage_type": specifications.get('Key Specs', {}).get('Storage Type', '') or specifications.get('Storage', {}).get('Storage Type', ''),
            }

            return transformed_record
        else:
            return None
