#Key Mapper for Bestbuy data - [model + screen_size + color + processor_model + installed_memory_ram + storage_capacity] is used to de-dup items
class Mapper:
    def map(self, record):

        model = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]

        key_field = model + color
        return key_field
