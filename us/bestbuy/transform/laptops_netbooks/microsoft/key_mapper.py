#Key Mapper for Bestbuy data - [model + screen_size + color + processor_model + installed_memory_ram + storage_capacity] is used to de-dup items
class Mapper:
    def map(self, record):

        model = ''
        color = ''
        max_storage_capacity = ''
        screen_size = ''
        operating_system = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "max_storage_capacity" == entity['name']:
                if entity["attribute"]:
                    max_storage_capacity = entity["attribute"][0]["id"]
            elif "screen_size" == entity['name']:
                if entity["attribute"]:
                    screen_size = entity["attribute"][0]["id"]
            elif "operating_system" == entity['name']:
                if entity["attribute"]:
                    operating_system = entity["attribute"][0]["id"]

        key_field = model + color + max_storage_capacity + screen_size + operating_system
        return key_field
