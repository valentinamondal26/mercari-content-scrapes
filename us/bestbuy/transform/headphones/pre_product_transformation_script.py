import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['was_price']
            if not price:
                price = record['price']
            price = price.replace("$", "")
            specifications=record.get('specifications',{})

            color = specifications.get('Other',{}).get('Color', '') or ''
            if not color:
                color = specifications.get('General',{}).get('Color', '') or ''

            color = re.sub(re.compile(r'\(The Beats Decade Collection\)|\(The Beats Icon Collection\)', re.IGNORECASE), '', color).strip()

            color_category = specifications.get('Other',{}).get('Color Category', '') or ''
            if not color_category:
                color_category = specifications.get('General',{}).get('Color Category', '') or ''

            brand = specifications.get('General',{}).get('Brand','') or specifications.get('Other',{}).get('Brand','') or record.get('sub_category', '')

            model = specifications.get('General',{}).get('Product Name','') or specifications.get('Other',{}).get('Product Name','')
            model = model.replace('UrBeats', 'urBeats')\
                    .replace('Solo 2', 'Solo2')\
                    .replace('Powerbeats3', u'Powerbeats³')\
                    .replace('Studio3', u'Studio³')\
                    .replace('Solo3', u'Solo³')\
                    .replace('BeatsX Wireless', 'BeatsX')
            model = re.sub(re.compile(r'Earphones|Earbud|Over-Ear|Over-the-Ear|On-Ear|In-Ear|'
                r'Headphones|Noise Canceling|by Dr. Dre|Clip-On|Geek Squad Certified Refurbished',
                re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            if re.findall(re.compile('wireless', re.IGNORECASE), model):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category', ''),
                "image": record.get('image', ''),
                "breadcrumb": record.get('breadcrumb',''),
                "description": record.get('description',''),
                "title": record.get('title', ''),
                "features": record.get('features',''),
                "ner_query": ner_query,
                "color": color,
                "brand": brand,
                "model_sku": specifications.get('Other',{}).get('Model Number',''),
                "height": specifications.get('Dimension',{}).get('Product Height',''),
                "width": specifications.get('Dimension',{}).get('Product Width',''),
                "weight": specifications.get('Dimension',{}).get('Product Weight',''),
                "upc": specifications.get('Other',{}).get('UPC',''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": price
                },
                "model": model,
                "battery_life": specifications.get('Power',{}).get('Battery Life',''),
                "wired_wireless": specifications.get('Connectivity',{}).get('Connection Type','') or specifications.get('Key Specs',{}).get('Connection Type',''),
                "charger_type": specifications.get('Power',{}).get('Charging Interface(s)',''),
                "active_noise_canceling": specifications.get('Audio',{}).get('Noise Canceling (Active)','') or specifications.get('Key Specs',{}).get('Noise Canceling (Active)',''),
                "connector_type": specifications.get('Connectivity',{}).get('Connection Type','') or specifications.get('Key Specs',{}).get('Connection Type',''),
                "color_category": color_category,
            }

            return transformed_record
        else:
            return None
