# -*- coding: utf-8 -*-
import scrapy
import json
import datetime
import re
from scrapy import Request
import random
import os
from scrapy.exceptions import CloseSpider

class ZumiezSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category and sub-category from zumiez.com for the specified brand

    Attributes

    category : str
        category to be crawled, Supports
        1) Women_Shoes_Fashion_Sneakers
    brand : str
        brand to crawled , Supports
        1) converse
        2) vans

    Command e.g:
    scrapy crawl zumiez_spider -a category='Women_Shoes_Fashion_Sneakers' -a brand='converse'
    scrapy crawl zumiez_spider -a category='Women_Shoes_Fashion_Sneakers' -a brand='vans'
    """


    name = 'zumiez_spider'

    category = None
    brand = None
    launch_url = None
    breadcrumb = None
    base_url = "https://www.zumiez.com"

    url_dict = {
        'Women_Shoes_Fashion_Sneakers': {
            'converse': {
                'url': 'https://www.zumiez.com/brands/converse/shoes.html?d=1000107&gender=womens',
            },
            'vans': {
                'url': 'https://www.zumiez.com/brands/vans/vans-shoes.html?d=1000107&gender=womens',
            },
        }
    }
    proxy_pool = [
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    def __init__(self, category=None,brand=None, *args, **kwargs):
        super(ZumiezSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category :
            self.logger.error("category should not be None")
            raise CloseSpider('category not found')
        if not brand:
            self.logger.error("brand should not be None")
            raise CloseSpider("brand not fount")
        self.category = category
        self.brand=brand

        self.launch_url=self.url_dict.get(category, {}).get(brand, {}).get('url', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{}'.format(category))
            raise(CloseSpider('Spider is not supported for  category:{}'.format(category)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        if self.launch_url:
            yield Request(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        """
        @url https://www.zumiez.com/brands/converse/shoes.html?d=1000107&gender=womens
        @returns requests 1
        """
        all_links = response.css('a.product-name::attr(href)').getall()
        breadcrumbs = response.xpath('//div[@class="breadcrumbs"]//text()').getall()
        self.breadcrumb = ''
        for x in breadcrumbs:
            if re.findall(r"[a-zA-Z]+",x):
                self.breadcrumb+=re.findall(r"[a-z-A-Z]+",x)[0] + "|"
        self.breadcrumb=self.breadcrumb[:-1]
        print(len(all_links),"\nLINKS\n\n")
        if all_links:
            for link in all_links:
                yield self.create_request(url=link, callback=self.parse_products)
        next_page_url = response.xpath('//a[@title="Next"]/@href').get(default='')
        if next_page_url:
            yield self.create_request(url=next_page_url, callback=self.parse)


    def parse_products(self, response):
        """
        @url https://www.zumiez.com/converse-chuck-taylor-all-star-black-high-top-shoes.html
        @scrape_values id breadcrumb title description image item_id color product_details est_retail
        """

        item = {}
        item["crawlDate"]=str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["statusCode"]=str(200)
        item['id']=response.xpath('//link[contains(@rel,"canonical")]/@href').get()
        item['breadcrumb']='Brands|Vans|Shoes'
        needed_script=response.css('script:contains("evergageProductView")::text').get()
        if needed_script:
            needed_script=re.findall("evergageProductView = {.*?}]}", needed_script, re.S)
            if needed_script:
                needed_script=needed_script[0].split('evergageProductView = ')[1]
        details=json.loads(needed_script)
        item['title']=details.get('name','')

        item['price']=details.get('price','')
        item['description']=details.get('description','')
        item['image']=details.get('imageUrl','').replace('\\/','/')
        item['item_id']=details.get('_id','')
        needed_script=response.css('script:contains("spConfig")::text').get()
        if needed_script:
            needed_script=needed_script.split('\n        var spConfig = new Product.Config(')
            needed_script=needed_script[1].split(');\n')
            item['color']=json.loads(needed_script[0]).get('attributes')['76']['options'][0]['label']
        else:
            item['color']=''
        product_details=response.xpath('//table[@class="data-table"]//text()').getall()
        item['product_details']=''
        item['est_retail']=details.get('listPrice','')
        item['category']="Women_Shoes_Fashion_Sneakers"
        item['brand']="vans"
        for detail in product_details:
            if "\n" not in detail and detail!="Product Details:":
                item['product_details']+=detail+","
        item['product_details']=item['product_details'][:-1]
        yield item


    def create_request(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            for key,value in meta.items():
                request.meta[key]=value

        request.meta.update({"use_cache":True})
        return request
