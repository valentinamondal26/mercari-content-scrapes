import hashlib
import re
import json
from colour import Color
class Mapper(object):
    def map(self, record):
        if record:
            id = record.get('id','')
            crawl_date = record.get('crawlDate','')
            status_code = record.get('statusCode','')
            category = record.get('category','')
            brand = record.get('brand','')
            description=record.get('description','')
            image = record.get('image','')
            product_details=record.get('product_details','')
            title = record.get('title','')
            colour = record.get('color','')
            price=str(record.get('price',''))
            if '$' in price:
                price=price.split('$')[1]
            est_retail=str(record.get('est_retail',''))
            if '$' in est_retail:
                est_retail=est_retail.split('$')[1]


            model=title
            chars=[' ',',','&']
            for char in chars:
                _color = [i.strip(' ,') for i in title.split(char) if self.check_color(i.strip(' ,'))]
                color=''
                for color in _color:
                    if color:
                        model = model.replace(color, '')
                model=model.replace(char,' ')

            if brand:
                model = re.sub(re.compile(r'^{0}'.format(brand), re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            ner_query = description + product_details

            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawl_date": crawl_date,
                "status_code": status_code,
                "category": "Women's Athletic Shoes",
                "brand": brand,
                "image": image,
                "ner_query": ner_query,
                "title": title,
                "color": colour,   
                "model":model,            
                "sku":record.get('item_id',''),
                "price":{
                    "currency_code": "USD",
                    "amount": price.replace("$","")
                },
                "est_retail":{
                     "currency_code": "USD",
                    "amount": est_retail.replace("$","")
                }
            }
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False
