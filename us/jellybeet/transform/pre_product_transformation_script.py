import hashlib
from titlecase import titlecase
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ', '.join(record.get('description', []))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            title = record.get('title', '')
    
            color_meta = [
                'Dark \w+', 'Soft Pink' 'Aqua', 'Black', 'Blue', 'Brown', 'Gold', 'Gray', 'Lavender',
                'Pink', 'Purple', 'Rainbow', 'Tan', 'Teal', 'White', 'Yellow', 'Red', 'Grey'
            ]

            color_data = re.findall(r'|'.join(list(map(lambda x: r'\b' + x + r'\b', color_meta))), title, flags=re.IGNORECASE)
            color = "/".join(color_data)
            if re.findall(r'\bPink Bow\b', model, flags=re.IGNORECASE):
                color = re.sub(r'\bPink\b','', color, flags=re.IGNORECASE)
                color = re.sub(r'\/$', '', color).strip()
    
            model = re.sub(r'w/', 'with', model, flags=re.IGNORECASE)
            model = re.sub(r'\bPlush\b|®', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bAmuse\b', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+\-\s+|\(|\)', ' ', model)

            pattern = r'(.*)(?<!with\s)('+r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color_meta)))+r')(.*)'
            model = re.sub(pattern, r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            model = re.sub(r'\b{col}\b'.format(col=color), '', model, flags=re.IGNORECASE)
            model = re.sub(r'(?!Pink Bow)'+ '('+ r'|'.join(list(map(lambda x: r'\b' + x + r'\b', color_meta)))+')', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\band Friends 12 Days of Christmas Kawaii Advent Calendar\b', '12 Days of Christmas Kawaii Advent Calendar', model, flags=re.IGNORECASE)
            model = re.sub(r'(\bin\b)|(\bwith\b)', lambda x: x.group().lower(), model, flags=re.IGNORECASE)
            if re.findall(r'\bSoft\b|\bDark\b', model, flags=re.IGNORECASE) and color:
                special_color_word = re.findall(r'\bSoft\b|\bDark\b', model, flags=re.IGNORECASE)[0]
                model = re.sub(r'\b{word}\b'.format(word=special_color_word),  '', model, flags=re.IGNORECASE)
                color = f'{special_color_word} {color}'
            model = re.sub(r'\bMofu Mofu Bichon Dog Meringue-Chan Medium\b', 'Mofu Mofu Bichon Dog Meringue-Chan', model, flags=re.IGNORECASE)
            model = re.sub(r'\bShiba Inu Dark\b', 'Shiba Inu', model, flags=re.IGNORECASE)
            model = re.sub(r'\bAqua Unicorn Keychain\b', 'Unicorn Keychain', model, flags=re.IGNORECASE)
            model = re.sub(r'\bUnicorn Aqua\b', 'Unicorn Stuffed Animal', model, flags=re.IGNORECASE)
            model = re.sub(r'\bSakura Shiba Inu\b', 'Shiba Inu with Sakura Bow Stuffed Animal', model, flags=re.IGNORECASE)
            model = re.sub(r'\bGolden Shiba Inu Keychain\b', 'Shiba Inu Keychain', model, flags=re.IGNORECASE)
            
            if not re.findall(r'\bKeychain\b|\bPillow\b|\bStuffed Animal\b', model, flags=re.IGNORECASE):
                model = f'{model} Stuffed Animal'
            model = re.sub(r'\s+', ' ', model).strip()

            if re.findall(r'^Shiba Inu$', model, flags=re.IGNORECASE):
                color = 'Black/Tan'
          
            if re.findall(r'^Unicorn Keychain$', model, flags=re.IGNORECASE) or \
                re.findall(r'^Unicorn Stuffed Animal$', model, flags=re.IGNORECASE):
                color = 'White/Aqua'

            if re.findall(r'^Shiba Inu with Sakura Bow Stuffed Animal$', model, flags=re.IGNORECASE) or \
                re.findall(r'^Shiba Inu Keychain$', model, flags=re.IGNORECASE) and not re.findall(r'\bBlue\b', color, flags=re.IGNORECASE):
                color = 'Golden'

            if re.findall(r'^Shiba Inu Keychain$', model, flags=re.IGNORECASE) and re.findall(r'\bBlue\b', color, flags=re.IGNORECASE):
                color = 'Black/Tan'
            
            features = ', '.join(record.get('features', []))

            plush_size = ''
            match = re.findall(r'(\d+\.*\d*)\s*("|inches|" inches|inch)\s*(Tall|Long|super soft plush| x )', features, flags=re.IGNORECASE)
            if match:
                plush_size = match[0][0]
                plush_size += ' in.'

            if re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Throw', 'Backpack', 'Purse']))), record.get('title', ''), flags=re.IGNORECASE):
                return None


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description + ' ' + features,
                "image": record.get('image', ''),
                "color": color,

                "model": model,
                'plush_size': plush_size,
                'upc': record.get('specs', {}).get('UPC:', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
