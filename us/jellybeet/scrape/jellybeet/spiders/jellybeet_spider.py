'''
https://mercari.atlassian.net/browse/USCC-700 - Amuse Stuffed Animals
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class JellybeetSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from jellybeet.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stuffed Animals
    
    brand : str
        brand to be crawled, Supports
        1) Amuse

    Command e.g:
    scrapy crawl jellybeet -a category="Stuffed Animals" -a brand="Amuse"
    """

    name = 'jellybeet'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'jellybeet.com'
    blob_name = 'jellybeet.txt'

    url_dict = {
        "Stuffed Animals": {
            "Amuse": {
                'url': 'https://jellybeet.com/amuse/',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(JellybeetSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://jellybeet.com/amuse-alpacasso-white-alpaca-plush/
        @meta {"use_proxy":"True"}
        @scrape_values id image breadcrumb title price description features specs
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'breadcrumb': response.xpath('//ul[@class="breadcrumbs"]/li[@class="breadcrumb "]/a[@itemprop="item"]/span[@itemprop="name"]/text()').getall(),
            'title': response.xpath('//h1[@itemprop="name"]/text()').get(),
            'price': response.xpath('//span[@class="price price--withoutTax price--main"]/text()').get(),
            'description': response.xpath('//div[@itemprop="description"]/p[@class="p1"]//text()').getall() \
                or response.xpath('//div[@itemprop="description"]/p/text()').getall(),
            'features': response.xpath('//div[@itemprop="description"]/ul/li//text()').getall(),
            'specs': dict(zip(
                response.xpath('//div[@class="productView-properties-panel-body"]/dl/dt').xpath('normalize-space(./text())').getall(),
                response.xpath('//div[@class="productView-properties-panel-body"]/dl/dd').xpath('normalize-space(./text())').getall()
            )),
        }


    def parse(self, response):
        """
        @url https://jellybeet.com/amuse/
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//section[@class="products_view"]/ul[contains(@class, "productGrid")]/li[@class="product"]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//ul/li[@class="pagination-item pagination-item--next"]/a/@href').extract_first()
        if next_page_link:
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
