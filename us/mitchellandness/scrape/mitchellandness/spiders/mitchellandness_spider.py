'''
https://mercari.atlassian.net/browse/USDE-1711 - Mitchell & Ness Jerseys
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class MitchellandnessSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from mitchellandness.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Jerseys

    brand : str
        brand to be crawled, Supports
        1) Mitchell & Ness

    Command e.g:
    scrapy crawl mitchellandness -a category='Jerseys' -a brand='Mitchell & Ness'
    """

    name = 'mitchellandness'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'id'

    source = 'mitchellandness.com'
    blob_name = 'mitchellandness.txt'

    url_dict = {
        'Jerseys': {
            'Mitchell & Ness': {
                'url': 'https://www.mitchellandness.com/jerseys?item_type=77',
                'filters': ['League', 'Teams', 'Player', ],
            }
        }
    }

    extra_item_infos = {}

    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK', ''):
            self.contracts_mock_function()
            return
        super(MitchellandnessSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.filters = ['League', 'Teams', 'Player']


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse, dont_filter=True)
            yield self.create_request(url=self.launch_url, callback=self.parse_filters, dont_filter=True)


    def crawlDetail(self, response):
        """
        @url https://www.mitchellandness.com/legacy-jersey-chicago-bears-2001-brian-urlacher-lgjylg1
        @meta {"use_proxy": "True"}
        @scrape_values title product_type price sku image specs.Features specs.Care
        """

        specs = {}
        for feature in response.xpath('//ul[@class="pdp-product-details__list"]/li[@class="pdp-product-details__item"]'):
            key = feature.xpath('./h5[@class="pdp-product-details__item-heading"]/text()').extract_first()
            value = feature.xpath('./div[@class="pdp-product-details__item-content"]/ul/li/text()').extract() \
                or feature.xpath('./div[@class="pdp-product-details__item-content"]').xpath(u'normalize-space(.//text())').extract()
            specs[key] = value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//h1[@class="pdp-product-title__heading"]/span[@itemprop="name"]/text()').extract_first(),
            'product_type': response.xpath('//span[@class="pdp-product-title__eyebrow"]/text()').extract_first(),
            'price': response.xpath('//span[@class="price"]/text()').extract_first(),
            'sku': response.xpath('//div[@itemprop="sku"]/text()').extract_first(),
            'specs': specs,
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
        }


    def update_extra_item_infos(self, id, extra_item_info): 
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse_filters(self, response):
        """
        @url https://www.mitchellandness.com/jerseys?item_type=77
        @meta {"use_proxy": "True"}
        @returns requests 1
        """

        for filter_item in response.xpath('//div[@class="filter-options"]/div[contains(@class, "filter-options-item filter")]'): 
            filter_name = filter_item.xpath('./@data-filter-slug').extract_first() 
            if filter_name in self.filters:
                for filter_value_element in filter_item.xpath('./div[@data-role="content"]//li[@class="item"]'): 
                    filter_value = filter_value_element.xpath('./@data-label').extract_first() 
                    url = filter_value_element.xpath('./a/@href').extract_first() 
                    # print({filter_name: [filter_value, url]}) 
                    request = self.create_request(url=url, callback=self.parse)
                    request.meta['extra_item_info'] = {
                        filter_name: filter_value
                    }
                    yield request


    def parse(self, response):
        """
        @url https://www.mitchellandness.com/jerseys?item_type=77
        @meta {"use_proxy": "True"}
        @returns requests 13
        """
        #No. of Requests mentioned in contracts is 13 because 12 items per page and 1 next page url.

        extra_item_info = response.meta.get('extra_item_info', {})
        products_count = response.xpath('//div[@class="toolbar toolbar-products"][1]/p[@id="toolbar-amount"]/span/text()').extract()
        # print(f'products_count: {products_count}')
        for product in response.xpath('//ol[@class="product-tiles"]/li[@class="product-tile product-item"]'):

            id = product.xpath('.//a/@href').extract_first()

            if id:
                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                # self.extra_item_infos.get(id).update(extra_item_info)
                self.update_extra_item_infos(id, extra_item_info)

                yield self.create_request(url=id, callback=self.crawlDetail)

        nextLink = response.xpath('//li[@class="item pages-item-next"]/a[@class="link  next"]/@href').extract_first()
        if nextLink:
            request = self.create_request(url=nextLink, callback=self.parse)
            request.meta['extra_item_info'] = extra_item_info
            yield request


    def create_request(self, url, callback, meta=None, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, meta=meta, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
