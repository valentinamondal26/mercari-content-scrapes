import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bAuthentic\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s*/\s*', '/', model)
            model = re.sub(r'(.*)(\d/\d\s*zip)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            if re.findall(r'\bJersey\b', model):
                model = re.sub(r'\bJersey\b', '', model) + ' Jersey'
            model = re.sub(r'\s+', ' ', model).strip()

            features = ', '.join(record.get('specs', {}).get('Features', []))
            material = ''
            match = re.findall(r'\d+%\s*(\w+)', features)
            if match:
                material = ', '.join(list(dict.fromkeys(match)))

            if re.findall(r"\bWomen's\b", model, flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                'image': record.get('image', ''),

                "model": model,
                "team": ', '.join(record.get('Teams', '')),
                "player": ', '.join(record.get('Player', '')),
                'league': ', '.join(record.get('League', '')),
                'material': material,
                'model_sku': record.get('sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
