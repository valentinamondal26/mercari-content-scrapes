class Mapper:
    def map(self, record):
        key_field = ""

        league = ''
        team = ''
        player = ''
        model = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "league" == entity['name']:
                if entity["attribute"]:
                    league = entity["attribute"][0]["id"]
            elif "team" == entity['name']:
                if entity["attribute"]:
                    team = entity["attribute"][0]["id"]
            elif "player" == entity['name']:
                if entity["attribute"]:
                    player = entity["attribute"][0]["id"]

        key_field = league + team + player + model
        return key_field
