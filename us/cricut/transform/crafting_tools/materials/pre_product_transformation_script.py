import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record.get('id')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            # model transformations
            model = record.get('title')
            pattern = re.compile(
                r"™|Digital|®|\(|\bct\b|\)||\+|\,|\d+\.*\d*\s*\"\s*x\s*\d+\.*\d*\s*\"|\"|Bundle|cricut", re.IGNORECASE)
            model = re.sub(pattern, '', model)
            pattern = re.compile(r"Iron\s+-\s*On", re.IGNORECASE)
            model = re.sub(pattern, "Iron-On", model)
            model = re.sub(re.compile(r"Printable Clear Sticker Paper,Clear",
                                      re.IGNORECASE), "Printable Clear Sticker Paper", model)
            model = re.sub(r"\,", " ", model)
            model = re.sub(re.compile(
                r"\s[\-|\–]\b|\s[\-|\–]\s|\b[\-|\–]\s", re.IGNORECASE), " ", model)
            model = re.sub(re.compile(r"\b(T)\s*(Shirt)\b",
                              re.IGNORECASE), r"\1-\2", model)
            model = ' '.join(model.split())
            if model != "" and (model[-1] == "-" or model[-1] == "–"):
                model = model[:-1]
            model = ' '.join(model.split())
            if record.get('Color Family ', '') != '':
                model += ","+record.get("Color Family ")

            # extract product type
            product_type = record.get('Category', '').strip()
            product_type = ", ".join(product_type.split(" ,"))
            product_type = re.sub(re.compile(
                r", Other|Other, ", re.IGNORECASE), "", product_type)
            product_type = ", ".join(product_type.split(" ,"))
           

            if re.findall(re.compile(r"bundle", re.IGNORECASE), record.get("title")) != [] or product_type == "Other":
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "Crafting Tools",
                "description": description.replace('\r\n', ''),
                "product_line": record.get('Licensors & Artists', ''),
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                # "image": record.get('image',''),
                "ner_query": ner_query.replace('\r\n', ''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "product_type": product_type,
                "model_sku": record.get('model_sku', ''),
                "product_family": record.get('product_family', ''),
                "machine_compatibility": record.get('Machine Compatibility', ''),
                "size": record.get('Material Size', ''),
                "color": record.get('Color Family', '')

            }
            return transformed_record
        else:
            return None
