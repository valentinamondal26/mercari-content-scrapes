import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model=record.get('title')
            pattern=re.compile(r"cricut|Everything|Starter|Iron\s*\-\s*On|Essentials|®|\(|ct|\)||\-|\+|\,|\d+\.*\d*\s*\"\s*x*\s*\d+\.*\d*\s*\"|\"|bundles*|™|Materials",re.IGNORECASE)
            model=re.sub(pattern,'',model)
            # for word in re.findall(r"\w+",record.get('Size','')):
            #     pattern=re.compile(r"{}".format(word),re.IGNORECASE)
            #     model=re.sub(pattern,'',model)
                
            model=' '.join(model.split())

            if re.findall(re.compile(r"bundle|Access Annual Subscription|Access Monthly Subscription|Access Premium Subscription",re.IGNORECASE),record.get("title",""))!=[]:
                return None


            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": "Crafting Tools",
                "description": description.replace('\r\n',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                #"image": record.get('image',''),
                "ner_query": ner_query.replace('\r\n',''),
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
               "product_type":record.get('Category',''),
               "model_sku":record.get('model_sku',''),
               "color":record.get('Machine Color',''),
               "size":record.get('Size',''),
               "product_family":record.get('product_family','')
               
                }


            return transformed_record
        else:
            return None
