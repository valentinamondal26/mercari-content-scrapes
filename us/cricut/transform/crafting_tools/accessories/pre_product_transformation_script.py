import re
import hashlib


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            model = record.get('title')
            product_type = record.get('Category', '')

            # product type transformation
            product_type = ", ".join(product_type.split(" ,"))
            product_type = re.sub(re.compile(
                r"\bMats\b", re.IGNORECASE), "Materials", product_type)
            product_type = product_type.split(", ")[0]

            # model transformation
            pattern = re.compile(
                r"[^\x00-\x7f]|®|\-|\+|\,|cricut|™|\(*\s*\d+\.*\d*\s*ct\.*\s*\)*|\d+\.*\d*\s*mm|\(\s*\d+\.*\d*\s*\)", re.IGNORECASE)
            model = re.sub(pattern, "", model)
            model = re.sub(re.compile(
                r"\d+\.*\d*\s*\"", re.IGNORECASE), "", model)
            model = re.sub(re.compile(r"\bx\b", re.IGNORECASE), "", model)
            # for word in re.findall(r"\w+",record.get('Size','')):
            #     pattern=re.compile(r"{}".format(word),re.IGNORECASE)
            #     model=re.sub(pattern,'',model)

            model = ' '.join(model.split())

            if re.findall(re.compile(r"bundle|shirt", re.IGNORECASE), record.get("title", "")) != []:
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": "Crafting Tools",
                "description": description.replace('\r\n', ''),
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                # "image": record.get('image',''),
                "ner_query": ner_query.replace('\r\n', ''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "product_type": product_type,
                "model_sku": record.get('model_sku', ''),
                "machine_compatibility": record.get('Machine Compatibility', ''),
                "strength": record.get('Mat Strength', ''),
                "size": record.get('Size', ''),
                "product_family": record.get('product_family', '')

            }

            return transformed_record
        else:
            return None
