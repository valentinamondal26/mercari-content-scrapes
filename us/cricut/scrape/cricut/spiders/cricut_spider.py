'''
https://mercari.atlassian.net/browse/USCC-213 - Provo Craft/ Cricut Crafting Tools - Images & Fonts
https://mercari.atlassian.net/browse/USCC-219 - Provo Craft/ Cricut Crafting Tools - Accessories
https://mercari.atlassian.net/browse/USCC-195 - Provo Craft/ Cricut Crafting Tools - Materials
https://mercari.atlassian.net/browse/USCC-199 - Provo Craft/ Cricut Crafting Tools - Machines
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class CricutSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from cricut.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Crafting Tools
    
    brand : str
        brand to be crawled, Supports
        1) Provo Craft/ Cricut

    product_family : str
        product_family to be crawled, Supports
        1) Images & Fonts
        2) Accessories
        3) Materials
        4) Machines

    Command e.g:
    scrapy crawl cricut -a category="Crafting Tools" -a brand="Provo Craft/ Cricut" -a product_family="Images & Fonts"
    scrapy crawl cricut -a category="Crafting Tools" -a brand="Provo Craft/ Cricut" -a product_family="Accessories"
    scrapy crawl cricut -a category="Crafting Tools" -a brand="Provo Craft/ Cricut" -a product_family="Materials"
    scrapy crawl cricut -a category="Crafting Tools" -a brand="Provo Craft/ Cricut" -a product_family="Machines"
    """

    name = 'cricut'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = ''

    source = 'cricut.com'
    blob_name = 'cricut.txt'

    base_url = 'https://cricut.com'

    product_family =''

    url_dict = {
        "Crafting Tools": {
            'Provo Craft/ Cricut': {
                "Images & Fonts": {
                    'url': 'https://cricut.com/en_us/cartridges-images.html#page-title-heading',
                    'filters': ['Category','Licensors & Artists'],
                },
                "Accessories": {
                    'url': 'https://cricut.com/en_us/catalog/category/view/s/accessories/id/9/#page-title-heading',
                    'filters': ['Category','Machine Compatibility','Mat Strength','Size'],            
                },
                "Materials": {
                    'url': 'https://cricut.com/en_us/cutting-materials.html#page-title-heading',
                    'filters': ['Category','Licensors & Artists','Machine Compatibility','Material Size','Color Family'], 
                },
                "Machines": {
                    'url': 'https://cricut.com/en_us/machines.html#page-title-heading',
                    'filters': ['Category','Size','Machine Color']
                }
            }
        }
    }

    filter_list = []
    
    extra_item_infos = {}



    def __init__(self, category=None, brand=None, product_family=None, *args, **kwargs):
        super(CricutSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand
        self.product_family = product_family

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get(product_family,{}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get(product_family,{}).get('filters', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))
    
    def contracts_mock_function(self):
        self.filter_list = ['Category','Machine Compatibility','Mat Strength','Size']

    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        return request


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            if isinstance(self.launch_url, list):
                for url in self.launch_url:
                    yield self.create_request(url=url, callback=self.parse_filter)
            else:
                yield self.create_request(url=self.launch_url, callback=self.parse_filter)


    

    def parse_filter(self, response):
            """
            @url https://cricut.com/en_us/catalog/category/view/s/accessories/id/9/#page-title-heading
            @returns requests 1
            """
            current_filter_list = response.xpath('//div[@class="filter-options-item"]')
            
            for filter in current_filter_list:
                a = filter.xpath('.//div[@class="filter-options-content"]')
                filter_name = filter.xpath('.//div[@class="filter-options-title"]/text()').get() or ''
                
                if filter_name.strip() in self.filter_list:                    
                    for b in a.xpath('.//a'):
                       
                        request = self.create_request(url=b.xpath('./@href').get(), callback=self.parse_listing_page)
                        filter_value = b.xpath('./text()').get()
                        request.meta['extra_item_info'] = {
                            filter_name: filter_value
                        }
                        meta={}
                        meta=meta.fromkeys([filter_name.strip()],filter_value.strip())
                        meta.update({'filter_name':filter_name.strip()})
                        request.meta.update(meta)
                        yield request



    def parse_listing_page(self, response):
        """
        @url https://cricut.com/en_us/catalog/category/view/s/accessories/id/9/?cat=191
        @returns requests 37
        @meta {"use_proxy":"True"}
        """
        # Number of request mentioned in contracts is 37 beacause 36 items per page and 1 Next Page url
        for link in response.xpath('//div[@class="products list items row"]//a[@class="product photo product-item-photo"]/@href').getall():
                request = self.create_request(url=link, callback=self.parse_detail_page)
                request.meta.update(response.meta)
                yield request
                         
        next_page_url = response.xpath('//li[@class="item pages-item-next"]//a/@href').get()
        if next_page_url:
                request = self.create_request(url=next_page_url, callback=self.parse_listing_page)
                request.meta.update(response.meta)
                yield request
            

    def parse_detail_page(self, response):
        """
        @url https://cricut.com/en_us/catalog/product/view/id/4333/s/extra-fine-point-pen-set-bohemian-5-ct/category/9/
        @meta {"use_proxy":"True"}
        @scrape_values title price description model_sku
        """

        extra_item_info = response.meta.get('extra_item_info', {})
        status_code = str(response.status)
        url = response.url
        model_sku = response.xpath('//span[@id="product-sku"]/text()').extract_first(default='')
        if not self.extra_item_infos.get(model_sku, None):
                    self.extra_item_infos[model_sku] = {}

        self.extra_item_infos.get(model_sku).update(extra_item_info)
        
    
        item= {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': status_code,
            'category': self.category,
            'brand': self.brand,
            'id': url,
            'title': response.xpath('//span[@class="base" and @itemprop="name"]/text()').extract_first(default=''),
            'price': response.xpath('//span[@class="price"]/text()').extract_first(default=''),
            'description': ''.join(response.xpath('//div[@class="product attribute description"]//text()').getall()),
            'model_sku': model_sku,
            'product_family': self.product_family,

            }
        meta={}
        filter_name=response.meta.get('filter_name','')
        if filter_name!='':
            meta=meta.fromkeys([filter_name],response.meta[filter_name])
            item.update(meta)
            
        yield item

