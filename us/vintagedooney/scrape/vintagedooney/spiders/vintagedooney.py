import scrapy
from scrapy.exceptions import CloseSpider
import os
import random
import re
import datetime
import unicodedata

class VintagedooneySpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from vintagedooney.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Shoulder Bags

    brand : str
        brand to be crawled, Supports
        1) Dooney & Bourke

    Command e.g:
    scrapy crawl vintagedooney -a category='Shoulder Bags' -a brand='Dooney & Bourke'
    """

    name = 'vintagedooney'
    category = None
    brand = None
    source = 'vintagedooney.com'
    blob_name = 'vintagedooney.txt'

    proxy_pool = [
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    url_dict = {
        'Shoulder Bags': {
            'Dooney & Bourke': {
                'url': 'https://vintagedooney.com/category_82/Shoulder-Bags.htm',
            },
        },
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(VintagedooneySpider,self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            return

        if not category:
            self.logger.error("Category should not be None")
            raise CloseSpider('category not found')

        self.category = category
        self.brand = brand
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category=self.category, brand=self.brand, url=self.launch_url))


    def start_requests(self):
        print('launch_url', self.launch_url)
        if self.launch_url:
            yield scrapy.Request(url=self.launch_url, callback=self.parse)


    def get_url(self,category,brand):
        try:
            return self.url_dict[category][brand]
        except Exception as e:
            print('Exception occured:', e)
            raise(ValueError('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))


    def crawlDetail(self, response):
        """
        @url https://vintagedooney.com/item_744/Caribbean-Blue-Dooney-Satchel-Shoulder-Bag.htm
        @scrape_values breadcrumb title style image description price
        @meta {"item":{}}

        """
        item = response.meta['item']

        item['id'] = response.url
        item['category'] = self.category
        item['brand'] = self.brand
        item['crawlDate'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['statusCode'] = response.status

        item['breadcrumb'] = ''.join(response.xpath('//div[@class="breadcrumbs"]//text()[normalize-space(.)]').extract()).replace('>', '|')
        heading = response.xpath('//h1/span[@class="lgfont"]/text()').extract_first()
        if heading:
            s = heading.split(' ', 1)
            item['title'] = s[-1]
            item['style'] = s[0]
        item['image'] = response.xpath('//div[@class="image-holder"]/img/@src').extract_first()
        item['description'] = unicodedata.normalize('NFKC', ', '.join(response.xpath('//div[@class="p_layout"]//text()[normalize-space(.)]').extract()))
        item['price'] = ''.join(re.findall(r'[$]\d+.\d+', response.xpath('//span[@class="pricedisplay"]').xpath('.//span[@class="salecolor"]/text()').extract_first()))
        # item['brand'] = response.xpath('//div[@class="itemlabels label_price"]/label[@for="brand"]/text()')
        yield item


    def parse(self, response):
        """
        @url https://vintagedooney.com/category_82/Shoulder-Bags.htm
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        viewall = response.xpath('//div[@class="sb_pagination"]/a[contains(text(), "View All")]/@href').extract_first()
        if viewall:
            yield self.createRequest(url=viewall, callback=self.viewAll)


    def viewAll(self, response):
        """
        @url https://vintagedooney.com/category_82/all/Shoulder-Bags.htm
        @returns requests 1
        @meta {"use_proxy":"True"}
        """
        for product in response.xpath('//div[@class="product-multi"]'):
            item = dict()

            id = product.xpath('.//a/@href').extract_first()

            if id:
                request = self.createRequest(url=id, callback=self.crawlDetail)
                yield request


    def createRequest(self, url, callback):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
