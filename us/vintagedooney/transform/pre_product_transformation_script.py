import re
import hashlib


class Mapper(object):

    def get_dimensions(self, measurements):
        dimensions = re.findall(r"[A-Z]\s\S*\s", measurements)
        height, width, length = 0,0,0
        dim_dict = dict()

        for item in dimensions:
            if item.startswith('H'):
                height = item[2:-2]
                val = {'unit': 'INCH', 'value': height}
                dim_dict['height'] = val
            if item.startswith('W'):
                width = item[2:-2]
                val = {'unit': 'INCH', 'value': width}
                dim_dict['width'] = val
            if item.startswith('L'):
                length = item[2:-2]
                val = {'unit': 'INCH', 'value': length}
                dim_dict['length'] = val

        print(height, width, length)
        weigh = re.findall(r"weighs.*", measurements)
        if weigh :
            weight_list = weigh[0].split(' ')

            weight, val = 0 , 0
            for item in weight_list:
                if item == 'weighs':
                    continue
                if item.isnumeric():
                    val = float(item)
                if item == 'lb':
                    weight = val * 16
                if item == 'oz.':
                    weight = weight + val
            if weight > 0:
                val = {'unit': 'OZ', 'value': str(weight)}
                dim_dict['weight'] = val

        return dim_dict


    def get_price(self, price):
        price_list = price.split('-')
        price = 0
        max_price = 0
        for p in price_list:
            p = p.strip()
            p = p[1:]
            if self.isfloat(p):
                pr = float(p)
                if pr > max_price:
                    price = max_price
                    max_price = pr
                else:
                    price = pr
        if price == 0:
            price = max_price
            max_price = 0
        return price, max_price


    def isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def map(self, record):
        if record:

            price, max_price = self.get_price(record['price'])
            dim = 0
            if 'measurements' in record:
                dim = self.get_dimensions(record['measurements'])
            ner_query = record['description'] + " " + record['title']
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()


            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawlDate": record['crawlDate'],
                "statusCode": record['statusCode'],
                "category": record['category'],
                "brand": record['brand'],
                "image": record['image'],
                "description": record['description'],
                "title": record['title'],
                "style": record['style'],
                "ner_query": ner_query,
                "price":{
                    "currencyCode": "USD",
                    "amount": str(price)
                }
            }

            if max_price > 0:
                transformed_record['maxPrice'] = "${}".format(max_price)
            if dim:
                transformed_record['itemDimensions'] = dim

            return transformed_record
        else:
            return None
