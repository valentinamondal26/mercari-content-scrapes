'''
https://mercari.atlassian.net/browse/USCC-698 - Slumberkins Children & Young Adult Books
https://mercari.atlassian.net/browse/USCC-699 - Slumberkins Stuffed Animals
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re
import copy

class SlumberkinsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from slumberkins.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Children & Young Adult Books
        2) Stuffed Animals

    brand : str
        brand to be crawled, Supports
        1) Slumberkins

    Command e.g:
    scrapy crawl slumberkins -a category="Children & Young Adult Books" -a brand="Slumberkins"
    scrapy crawl slumberkins -a category="Stuffed Animals" -a brand="Slumberkins"
    """

    name = 'slumberkins'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'slumberkins.com'
    blob_name = 'slumberkins.txt'

    base_url = 'https://slumberkins.com'

    url_dict = {
        "Children & Young Adult Books": {
            "Slumberkins": {
                'url': 'https://slumberkins.com/collections/books',
            }
        },
        "Stuffed Animals": {
            "Slumberkins": {
                'url': [
                    'https://slumberkins.com/collections/snugglers',
                    'https://slumberkins.com/collections/kins',
                ]
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SlumberkinsSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://slumberkins.com/collections/books/products/confidence-board-book
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title price description
        """

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//nav[@class="breadcrumb"]/*[name()="a" or (name()="span" and not(@aria-hidden))]/text()').getall() \
                or response.xpath('//nav[@id="BreadcrumbsDesktop"]/*[name()="a" or (name()="span" and not(@aria-hidden))]/text()').getall(),
            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'title': response.xpath('normalize-space(//h1[@itemprop="name"]/text())').get(),
            'sub_title': response.xpath('//h2[@class="product-sub-title"]/a/text()').get(),
            'price': response.xpath(u'normalize-space(//span[@itemprop="price"]/text())').get(),
            'description': response.xpath('//section[@id="content1"]//text()[normalize-space(.)]').getall(),
            'specs': response.xpath('//section[@id="content2"]//text()[normalize-space(.)]').getall(),
        }

        options = response.xpath('//select[@id="ProductSelect"]/option')
        if not options:
            yield item

        for option in options:
            option_value = option.xpath('./@value').get()
            # option_selected = option.xpath('./@selected').get()
            option_text = option.xpath(u'normalize-space(./text())').get()
            variant = copy.deepcopy(item)
            variant.update({
                'id': re.sub(r'variant=(\d+)', f'variant={option_value}', response.url, flags=re.IGNORECASE),
                'variant_option': option_text,
            })

            yield variant


    def parse(self, response):
        """
        @url https://slumberkins.com/collections/books
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="grid-uniform"]/div[contains(@class, "grid__item grid-product")]') \
            or response.selector.xpath('//div[contains(@class, "tw-grid")]/div[contains(@class, "grid-product")]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
