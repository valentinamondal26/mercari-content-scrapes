import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ', '.join(record.get('description', []))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\bSingle\b', '', model, flags=re.IGNORECASE)
            if re.findall(r'\bKin\b', model, flags=re.IGNORECASE):
                model += ' Stuffed Animal'
            model = re.sub(r'\s+', ' ', model).strip()

            color = record.get('variant_option', '')
            color = re.sub(r'\bDefault Title\b', '', color, flags=re.IGNORECASE)
            color = re.sub(r'\/ Regular', '', color)
            color = color.split('-')[0].strip()

            if re.findall(r'\bBundle\b|\bSet\b', record.get('title', ''), flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": f'{color} {model}' if color not in model else model,
                "color": color,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
