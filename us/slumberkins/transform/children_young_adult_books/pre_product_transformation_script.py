import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = ', '.join(record.get('description', []))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub('(.*)(Book)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            if not re.findall(r'\bBook\b', model, flags=re.IGNORECASE):
                model += ' Book'
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": record.get('image', ''),

                "model": model,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
