import hashlib
import re
from titlecase import titlecase
from colour import Color

class Mapper(object):

    def map(self, record):
        if record:
            description = '; '.join(record.get('description', []))
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            color = ''
            match = re.findall(r'Color\s*[\:\-\s](.*?);', description)
            if match:
                color = match[0]
            color = re.sub(r'\bMulti\b', 'Multicolor', color, flags=re.IGNORECASE)
            color = re.sub(r'\bDenim Blue\b|\bDenim\b', 'Blue', color, flags=re.IGNORECASE)
            color = re.sub(r'\s*&\s*', '/', color, flags=re.IGNORECASE)
            color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Thread Embroidery', 'and Blush', 'Linen']))), '', color, flags=re.IGNORECASE)
            if re.findall(r'\bMulticolor\b', color, flags=re.IGNORECASE):
                color = '/'.join([c.strip() for c in color.split('Multicolor')]) + 'Multicolor'
            color = re.sub(r'\s+', ' ', color).strip()
            color = titlecase(color)
            if not color:
                colors = []
                for word in re.findall(r"\w+", record.get('title', '')):
                    try:
                        Color(word)
                        colors.append(word)
                    except:
                        pass
                color = ', '.join(colors)


            material = ''
            match = re.findall(r'Material\s*[\:\-\s]\s*(.*?);', description)
            if match:
                material = match[0]
                if re.findall('and Composition', material):
                    material = ''
            if not material:
                match = re.findall(r'Material\s*[\:\-\s]\s*;(.*?);', description)
                if match:
                    material = match[0]
            material = re.sub(r'\xa0|,\s*$', '', material).strip()
            material = re.sub(r'\d+\s*%', '', material)
            material = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['White', 'Multicolored', 'Multicolor']))), '', material, flags=re.IGNORECASE)
            material = re.sub(r'RaffiaPVC', 'Raffia PVC', material, flags=re.IGNORECASE)
            material = re.sub(r'\s*(and|/|&|,)\s*', '/', material, flags=re.IGNORECASE)
            material = re.sub(r'\s+', ' ', material).strip()

            material_meta = ['Suede', 'Linen', 'Leather', 'Nappa']
            def extract_material_from_meta(data):
                material = ''
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', material_meta))), data, flags=re.IGNORECASE)
                if match:
                    material = '/'.join(list(dict.fromkeys(list(map(lambda x: titlecase(x), match)))))
                return material

            if not material:
                material = extract_material_from_meta(record.get('title', ''))

            if not material:
                material = extract_material_from_meta(description)

            if len(material.split('/')) > 2:
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', ['Sequin', 'Leather', 'Canvas', 'Suede', 'Kid Suede']))), material)
                if match:
                    material = '/'.join(match)

            material = titlecase(material)

            heel_height = ''
            match = re.findall(r'Heel Height\s*[\:\-\s](.*?);', description)
            if match:
                heel_height = match[0]
                heel_height = re.sub(r'flat platform shape', '', heel_height, flags=re.IGNORECASE).strip()

            model = record.get('title', '')
            if material or color:
                model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', list(filter(None, [color, material]+re.split(r'[/\s]', color)+re.split(r'[/\s]', material)+material_meta))))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bMulti\b|\bEmbroidered\b|\bKid\b', '', model)
            model = re.sub(r'/\s*$', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                'image': record.get('image', ''),

                "model": model,
                "color": color,
                "material": material,
                'heel_height': heel_height,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
