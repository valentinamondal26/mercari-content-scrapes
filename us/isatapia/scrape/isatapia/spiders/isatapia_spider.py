'''
https://mercari.atlassian.net/browse/USCC-528 - Isa Tapia Women's Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class IsatapiaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from isatapia.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Shoes
    
    brand : str
        brand to be crawled, Supports
        1) Isa Tapia

    Command e.g:
    scrapy crawl isatapia -a category="Women's Shoes" -a brand='Isa Tapia'
    """

    name = 'isatapia'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'isatapia.com'
    blob_name = 'isatapia.txt'

    url_dict = {
        "Women's Shoes": {
            'Isa Tapia': {
                'url': 'https://www.isatapia.com/collections/shop-the-collection-1',
            }
        }
    }

    base_url = 'https://www.isatapia.com'


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(IsatapiaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.isatapia.com/collections/shop-the-collection-1/products/nile-natural
        @meta {"use_proxy":"True"}
        @scrape_values id title price description image
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath(u'normalize-space(//h1[@class="h2 product-single__title"]/text())').extract_first(),
            'msrp': response.xpath(u'normalize-space(//span[contains(@id, "ComparePrice-")]/text())').extract_first(),
            'price': response.xpath(u'normalize-space(//span[contains(@id, "ProductPrice-")]/text())').extract_first(),
            'description': response.xpath('//div[@class="product-single__description rte"]/p//text()').extract(),
            'image': response.xpath('//meta[@property="og:image"]/@content').extract_first(),
        }


    def parse(self, response):
        """
        @url https://www.isatapia.com/collections/shop-the-collection-1
        @meta {"use_proxy":"True"}
        @returns requests 29
        """

        for product in response.xpath('//div[@class="grid grid--uniform grid--collection"]/div[contains(@class, "grid__item grid-product")]'):

            url = product.xpath('.//a/@href').extract_first()

            if url:
                if not url.startswith(self.base_url):
                    url = self.base_url + url

                yield self.create_request(url=url, callback=self.crawlDetail)

        nextLink = response.xpath('//div[@class="pagination"]/span[@class="next"]/a/@href').extract_first()
        if nextLink:
            if not nextLink.startswith(self.base_url):
                nextLink = self.base_url + nextLink
            yield self.create_request(url=nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
