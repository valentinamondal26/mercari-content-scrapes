'''
https://mercari.atlassian.net/browse/USDE-1269 - OnePlus Cell Phones & Smartphones
'''

import os
import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider


class GsmarenaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from gsmarena.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Cell Phones & Smartphones

    brand : str
        brand to be crawled, Supports
        1) OnePlus

    Command e.g:
    scrapy crawl gsmarena -a category='Cell Phones & Smartphones' -a brand='OnePlus'
    """

    name = 'gsmarena'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'gsmarena.com'
    blob_name = 'gsmarena.txt'

    base_url = 'https://www.gsmarena.com/'

    url_dict = {
        'Cell Phones & Smartphones': {
            'OnePlus': {
                'url': 'https://www.gsmarena.com/oneplus-phones-95.php',
            },
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(GsmarenaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.gsmarena.com/oneplus_7t_pro-9872.php
        @meta {"use_proxy":"True"}
        @scrape_values title image  features.released-hl features.body-hl features.os-hl specs.Network.Technology
        specs.Network.4G\sbands specs.Network.Speed specs.Body.Dimensions specs.Body.Weight specs.Display.Size specs.Platform.OS specs.Platform.Chipset specs.Platform.CPU specs.Platform.GPU price_info price_info_1 release_date
        """

        features = {}
        for feature in response.xpath('//ul[@class="specs-spotlight-features"]//span[contains(@data-spec, "-")]'):
            if feature:
                features[feature.xpath('./@data-spec').extract_first()] = feature.xpath('./parent::span//text()').extract() or feature.xpath('./parent::strong//text()').extract()

        specs = {}
        for table in response.xpath('//div[@id="specs-list"]/table'):
            feature = {}
            for tr in table.xpath('./tr'):
                feature[tr.xpath(u'normalize-space(.//td[@class="ttl"]//text())').extract_first().replace('\u00a0', '')] = ', '.join(tr.xpath('.//td[@class="nfo"]//text()[normalize-space(.)]').extract()).replace('\u2009', '')

            specs[table.xpath('.//th/text()').extract_first()] = feature

        price_info = {}
        for table in response.xpath('//table[@class="pricing inline widget"]'):
            feature = {}
            for tr in table.xpath('.//tr'):
                feature[tr.xpath(u'normalize-space(.//td//text())').extract_first().replace('\u00a0', '')] = ', '.join(tr.xpath('.//td//text()[normalize-space(.)]').extract()).replace('\u2009', '')

            price_info[table.xpath(u'normalize-space(.//caption/h3/text())').extract_first()] = feature

        # if not pricing_info:

        price_info_1 = {}
        price_url = '-price-'.join(response.url.rsplit('-', 1))

        import requests
        r = requests.get(price_url)
        from parsel import Selector
        sel = Selector(r.text)

        for pricing in sel.xpath('//div[@class="pricing-container"]/table[contains(@class, "pricing")]'):
            info = {}
            variants = pricing.xpath('./thead/tr/th/text()').extract()
            sellers = pricing.xpath('./tbody/tr/th/img/@alt').extract()
            prices = pricing.xpath('./tbody/tr/td/a/text()').extract()
            print(variants, sellers, prices)
            i = 0
            for seller in sellers:
                for variant in variants:
                    if not info.get(seller, {}):
                        info[seller] = {}
                    info[seller].update({ variant: prices[i].replace('\u2009', '') if i < len(prices) else ''})
                    i+=1


            price_info_1.update({
                pricing.xpath('./caption/text()').extract_first(): info
            })

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath('//h1[@data-spec="modelname"]/text()').extract_first(),
            'image': response.xpath('//div[@class="specs-photo-main"]//img/@src').extract_first(),
            'realase_date': response.xpath('//span[@data-spec="released-hl"]/text()').extract_first(),
            'features': features,
            'specs': specs,
            'price_info': price_info,
            'price_info_1': price_info_1,
        }


    def parse(self, response):
        """
        @url https://www.gsmarena.com/oneplus-phones-95.php
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        for product in response.xpath('//div[@class="section-body"]/div[@class="makers"]/ul/li'):

            id = product.xpath('./a/@href').extract_first()

            if id:
                request = self.create_request(url=self.base_url+id, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('.//a[@class="pages-next"]/@href').extract_first()
        if nextLink:
            yield self.create_request(url=self.base_url+nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

