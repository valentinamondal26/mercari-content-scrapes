class Mapper:
    def map(self, record):

        model = ''
        storage_capacity = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "storage_capacity" == entity['name']:
                for attribute in entity["attribute"]:
                    storage_capacity += attribute["id"]
            elif "color" == entity['name']:
                for attribute in entity["attribute"]:
                    color += attribute["id"]

        key_field = model + storage_capacity + color
        return key_field
