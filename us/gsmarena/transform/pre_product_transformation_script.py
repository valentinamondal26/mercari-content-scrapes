import hashlib
import re
from dateutil import parser

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\(|\)', '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            specs = record.get('specs', {})

            weight = ''
            weight_info = specs.get('Body', {}).get('Weight', '')
            match = re.findall(r'\((.*)\)', weight_info)
            if match:
                weight = match[0]

            dimensions = specs.get('Body', {}).get('Dimensions', '')
            height = ''
            length = ''
            width = ''
            match = re.findall(r'\((.*)x(.*)x(.*)in\)', dimensions)
            if match:
                height = match[0][0].strip()
                height = f'{height} {"inches" if float(height) > 1 else "inch"}'
                length = match[0][1].strip()
                length = f'{length} {"inches" if float(length) > 1 else "inch"}'
                width = match[0][2].strip()
                width = f'{width} {"inches" if float(width) > 1 else "inch"}'

            display_size = ''.join(record.get('features', {}).get('displaysize-hl', '')).replace('"', "inches") \
                or specs.get('Display', {}).get('Size', '').split(',', 1)[0]

            sim_card_size = specs.get('Body', {}).get('SIM', '')
            match = re.findall(r'\((.*)\)', sim_card_size)
            if match:
                sim_card_size = match[0]

            colors = specs.get('Misc', {}).get('Colors', '')

            def S(data):
                "English ordinal suffix for the day of the month, 2 characters; i.e. 'st', 'nd', 'rd' or 'th'"
                if data.day in (11, 12, 13):  # Special case
                    return 'th'
                last = data.day % 10
                if last == 1:
                    return 'st'
                if last == 2:
                    return 'nd'
                if last == 3:
                    return 'rd'
                return 'th'

            release_date = ''
            try:
                release_date = ''.join(record.get('features', {}).get('released-hl', [])).replace('Released', '').strip()
                d = parser.parse(release_date)
                release_date = d.strftime(f"%B %d{S(d)}, %Y")
            except:
                pass

            display_resolution = specs.get('Display', {}).get('Resolution', '').split(',', 1)[0]

            operating_system = specs.get('Platform', {}).get('OS', '').rsplit(',', 1)[-1].strip()

            cpu_brand = specs.get('Platform', {}).get('CPU', '')

            gpu_video_card = specs.get('Platform', {}).get('GPU', '')

            storage_capacities = []
            memory_ram_capacities = []
            storages = specs.get('Memory', {}).get('Internal', '')
            for storage in storages.split(', '):
                storage_capacities.append(storage.split(' ')[0].strip())
                memory_ram_capacities.append(storage.split(' ')[1].strip())
            storage_capacities = list(dict.fromkeys(storage_capacities))
            memory_ram_capacities = list(dict.fromkeys(memory_ram_capacities))

            video_resolution = specs.get('Main Camera', {}).get('Video', '')

            carriers_meta = ['AT&T', 'Sprint', 'T-Mobile', 'Verizon', 'Unlocked']
            match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', carriers_meta))), model)
            if match:
                carrier = match[0]
            else:
                carrier = ', '.join(carriers_meta)

            battery_life = ''.join(record.get('features', {}).get('batsize-hl', ''))

            price_info = record.get('price_info_1', {}).get('United States', {}).get('Amazon US', {})
            # price = ', '.join([x.replace('\u2009', '') for x in price_info.values()])
            price = ', '.join(price_info.values()).replace('$', '')

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),
                "description": record.get('description', ''),
                "price_info": price_info,

                "model": model,
                "color": colors,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'release_date': release_date,
                'height': height,
                'length': length,
                'width': width,
                'weight': weight,
                'sim_card_size': sim_card_size,
                'display_size': display_size,
                'display_resolution': display_resolution,
                'operating_system': operating_system,
                'cpu_brand': cpu_brand,
                'gpu_video_card': gpu_video_card,
                'storage_capacity': ', '.join(storage_capacities),
                'memory_ram_capacity': ', '.join(memory_ram_capacities),

                'video_resolution': video_resolution,
                'carrier': carrier,
                'battery_life': battery_life,
            }

            return transformed_record
        else:
            return None
