'''
https://mercari.atlassian.net/browse/USCC-381 - Color Street  Nail Art Designs
'''

import scrapy
import datetime
import json
from parsel import Selector
import random
import os
from scrapy.exceptions import CloseSpider


class ColorStreetSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from colorstreet.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Nail Art Designs

    brand : str
        category to be crawled, Supports
        1) Color Street

    Command e.g:
    scrapy crawl colorstreet -a category="Nail Art Designs" -a brand='Color Street'

    """

    name = 'colorstreet'

    proxy_pool = [
        # 'http://shp-mercari-us-d00001.tp-ns.com:80',
        # 'http://shp-mercari-us-d00002.tp-ns.com:80',
        # 'http://shp-mercari-us-d00003.tp-ns.com:80',
        # 'http://shp-mercari-us-d00004.tp-ns.com:80',
        # 'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    source = 'colorstreet.com'
    blob_name = 'colorstreet.txt'

    launch_url = None
    base_url = 'https://colorstreet.com'
    schema_keys = ['category','brand','title']

    url_dict = {
        "Nail Art Designs": {
            'Color Street': {
                'url': 'https://colorstreet.com/home/getitemlist',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ColorStreetSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand
        if not self.schema_keys:
            self.schema_keys=self.url_dict.get(category,{}).get('schema_keys',[])
            if self.schema_keys==[]:
                self.schema_keys=self.url_dict.get(category,{}).get(brand,{}).get('schema_keys',[])
        if category and brand:
            self.launch_url = self.url_dict.get(category, {}).get(brand,'')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse)


    def parse(self, response):
        #response=requests.post(url="https://colorstreet.com/home/getitemlist",)
        """
        @url https://colorstreet.com/home/getitemlist
        @returns requests 1
        @headers {"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36"}
        """

        html = json.loads(response.text).get("html",'')
        select = Selector(html)
        all_links = select.xpath('//div[@class="show-tiny-image slider-swipe"]/a/@href').getall()
        all_links = [self.base_url+link if "https" not in link else link for link in all_links]
        for link in all_links:
            yield self.createRequest(url=link, callback=self.parse_product)


    def parse_product(self,response):
        """
        @url https://www.colorstreet.com/home/product/FPG019
        @scrape_values image breadcrumb model_sku title price description
        @meta {"use_proxy":"True"}
        """

        yield {
            'crawlDate': str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            'statusCode': str(response.status),
            'id': response.url,
            'image': response.xpath('//div[@class="item active"]/img/@src').get(default=''),
            'brand': self.brand,
            'category': self.category,
            'breadcrumb': '|'.join(response.xpath('//ol[@class="breadcrumb"]/li//text()').getall()),
            'model_sku': response.xpath('//div[@class="itemcode"]//text()').get(default=''),
            'title': response.xpath('//div[@class="itemdescription"]//text()').get(default=''),
            'price': response.xpath('//div[@class="price"]//text()').get(default=''),
            'description': " ".join(' '.join(response.xpath('//div[@class="summary"]//text()').getall()).split()),
        }


    def createRequest(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
