import hashlib
import re
import html
from colour import Color

class Mapper(object):

    def map(self, record):
        if record:
            transformed_record = dict()

            id = record.get('id','')
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price=record.get("price",'')
            if re.findall(r"\d+\.*\d*",price)!=[]:
                price=re.findall(r"\d+\.*\d*",price)[0]
            else:
                price=''

            color=[]
            description_words=re.findall(r"\w+",record.get("description",''))
            _color = [i for i in description_words if self.check_color(i)]
            if _color!=[]:
                for cl in _color:
                    if cl!='':
                        color.append(cl)

            color=list(set(color))
            color=', '.join(color)
            description=record.get('description','')
            description=description.replace("Hope Blooms during Breast Cancer Awareness month! $2 from the purchase of each set sold through October 2019 will benefit The Breast Cancer Research Foundation , Living Beyond Breast Cancer , and The National Breast Cancer Foundation up to our donation goal of $200,000. Wear your support for Breast Cancer Awareness and 3 organizations that do outstanding work for breast cancer education, research, awareness and direct patient support.",'')
            description=description.replace("While Supplies Last",'')
            description=description.replace("This November, help us to find a cure and raise awareness with 1 Warrior! $2 from the purchase of each set sold through November 2019 will benefit JDRF and The Barton Center for Diabetes Education , up to our donation goal of $100,000. Wear your support for Type 1 Diabetes Awareness and two organizations that do outstanding work for T1D education, research, awareness, and patient support.",'')
            
            if color=='':
                pattern=re.compile(r"\s+\w+\s*shade",re.IGNORECASE)
                if re.findall(pattern,description)!=[]:
                    color=re.findall(pattern,description)[0].replace(" shade",'')

            
            

        

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category":"Nail Art Designs",
                "brand": 'Color Street',
                "ner_query":description.strip(),
                "product_line":record.get('product_type'),
                "description": description.strip(),
                "msrp":{
                    "currency_code": "USD",
                    "amount": price
                },
                "model":record.get("title",''),
                "model_sku":record.get("model_sku",''),
                "color":color

            }

            
            return transformed_record
        else:
            return None

    def check_color(self,color):
        try:
            Color(color)
            return True
        except ValueError:
            return False


