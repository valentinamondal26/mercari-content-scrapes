import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()

            model_sku = record.get('sku', '')
            model_sku = re.sub(r'\(|\)', '', model_sku)
            model_sku = model_sku.strip()

            color = record.get('color', '')
            color = re.sub(r'\bmulti\b', 'Multicolor', color, flags=re.IGNORECASE)
            color = re.sub(r'\bFull Grain \/ Suede Combo\b|\bFull Grain/Suede Combo\b|\bSuede\b', '', color, flags=re.IGNORECASE)
            color = re.sub(r'\s+', ' ', color).strip()

            features = ', '.join(record.get('features', []))

            def fraction_to_decimal(fraction, decimal_count):
                from fractions import Fraction
                import unicodedata
                nums = fraction.split('/')
                if len(nums) == 2:
                    return str(round(float(Fraction(int(nums[0]), int(nums[1]))), decimal_count))[1:]
                else:
                    try:
                        return str(round(unicodedata.numeric(u'{}'.format(fraction)), decimal_count))[1:]
                    except:
                        pass
                    return fraction

            heel_height = ''
            match = re.findall(r'Heel Height:(.*?)(inches|inch|")', features, flags=re.IGNORECASE)
            if match:
                heel_height, _ = match[0]
                heel_height = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group(), 2), heel_height)
                heel_height += ' in.'

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                'model_sku': model_sku,
                'heel_height': heel_height,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
