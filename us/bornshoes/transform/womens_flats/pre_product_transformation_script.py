import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()

            model_sku = record.get('sku', '')
            model_sku = re.sub(r'\(|\)', '', model_sku)
            model_sku = model_sku.strip()

            color_exclude_string = ['suede', 'and', 'leather']
            color = record.get('color', '')
            color = re.sub(r'|'.join(list(map(lambda x:r'\b'+x+r'\b', color_exclude_string))), '', color, flags=re.IGNORECASE)
            color = re.sub(r'\bLuggage\b', 'Luggage Brown', color, flags=re.IGNORECASE)
            color = re.sub(r'\bLt\b', 'Light', color, flags=re.IGNORECASE)
            color = re.sub(r'\bDk\b', 'Dark', color, flags=re.IGNORECASE)
            color = re.sub(r'(?!\(peacoat\))\(.*\)', '', color, flags=re.IGNORECASE)
            color = re.sub(r'\(|\)', '', color, flags=re.IGNORECASE)
            if re.findall(r'\bCombo\b', color, flags=re.IGNORECASE):
                colors = color.split()
                if re.findall(r'^Light \w+', color, flags=re.IGNORECASE):
                    color = f'{colors[0]} {colors[1]}/Multicolor'
                else:
                    color = f'{colors[0]}/Multicolor'
            color = re.sub(r'\bNavy Peacoat\b', 'Navy/Peacoat', color, flags=re.IGNORECASE)
            color = re.sub(r'\s+', ' ', color).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                'model_sku': model_sku,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
