import hashlib
from colour import Color
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = model.title()
            model = re.sub(r'\bSorja Ii\b', 'Sorja II', model, flags=re.IGNORECASE)

            model_sku = record.get('sku', '')
            model_sku = re.sub(r'\(|\)', '', model_sku)
            model_sku = model_sku.strip()

            features = ', '.join(record.get('features', []))

            def fraction_to_decimal(fraction, decimal_count):
                from fractions import Fraction
                import unicodedata
                nums = fraction.split('/')
                if len(nums) == 2:
                    return str(round(float(Fraction(int(nums[0]), int(nums[1]))), decimal_count))[1:]
                else:
                    try:
                        return str(round(unicodedata.numeric(u'{}'.format(fraction)), decimal_count))[1:]
                    except:
                        pass
                    return fraction

            heel_height = ''
            match = re.findall(r'Heel Height:(.*?)(inches|inch|")', features, flags=re.IGNORECASE)
            if match:
                heel_height, _ = match[0]
                heel_height = re.sub(r'\s*\d+\/\d+|[\u00BC-\u00BE\u2150-\u215E]*', lambda x: fraction_to_decimal(x.group(), 2), heel_height)
                heel_height += ' in.'

            color = record.get('color', '')
            color_exclude_string = ['Suede', 'and', 'Distressed', 'Patent', 'Fabric', 'with', 'sole', 'Lagoon', 'leather', 'Nubuck']
            color = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color_exclude_string))), '', color, flags=re.IGNORECASE)
            color = color.title()
            color = ' '.join(list(dict.fromkeys((color.split()))))
            color = re.sub(r'(?!\(\bNude\b\))\(.*\)', '', color)
            color = re.sub(r'\(|\)', '', color)
            color = re.sub(r'\bLt\b', 'Light', color, flags=re.IGNORECASE)
            color = re.sub(r'\bDk\b', 'Dark', color, flags=re.IGNORECASE)

            if re.findall(r'\bmulti\b|\bCombo\b', color, flags=re.IGNORECASE):
                colors = color.split()
                color = f'{colors[0]}/Multicolor'
            if not re.findall(r'\bMulticolor\b', color, flags=re.IGNORECASE):
                new_color = []
                for col in color.split():
                    try:
                        Color(col)
                        new_color.append(col)
                    except ValueError:
                        pass
                if len(new_color) == len(color.split()):
                    color = '/'.join(new_color)
            color = re.sub(r'\s+', ' ', color).strip()
            

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('image', ''),

                "model": model,
                "color": color,
                'model_sku': model_sku,
                'heel_height': heel_height,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
