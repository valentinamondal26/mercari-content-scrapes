'''
https://mercari.atlassian.net/browse/USCC-677 - Born Women's Flats
https://mercari.atlassian.net/browse/USCC-678 - Born Women's Sandals
https://mercari.atlassian.net/browse/USCC-679 - Born Women's Shoes
https://mercari.atlassian.net/browse/USCC-680 - Born Women's Heels
https://mercari.atlassian.net/browse/USCC-681 - Born Men's Sandals
https://mercari.atlassian.net/browse/USCC-682 - Born Men's Boots
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re


class BornshoesSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bornshoes.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women's Flats
        2) Women's Sandals
        3) Women's Shoes
        4) Women's Heels
        5) Men's Sandals
        6) Men's Boots

    brand : str
        brand to be crawled, Supports
        1) Born

    Command e.g:
    scrapy crawl bornshoes -a category="Women's Flats" -a brand="Born"
    scrapy crawl bornshoes -a category="Women's Sandals" -a brand="Born"
    scrapy crawl bornshoes -a category="Women's Shoes" -a brand="Born"
    scrapy crawl bornshoes -a category="Women's Heels" -a brand="Born"
    scrapy crawl bornshoes -a category="Men's Sandals" -a brand="Born"
    scrapy crawl bornshoes -a category="Men's Boots" -a brand="Born"
    """

    name = 'bornshoes'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'bornshoes.com'
    blob_name = 'bornshoes.txt'

    base_url = 'https://www.bornshoes.com'

    url_dict = {
        "Women's Flats": {
            "Born": {
                'url': 'https://www.bornshoes.com/ViewAll.aspx?GenderID=8&CategoryID=130&PageNo=1&SortType=P',
            }
        },
        "Women's Sandals": {
            "Born": {
                'url': 'https://www.bornshoes.com/ViewAll.aspx?GenderID=8&CategoryID=110&PageNo=1&SortType=P',
            }
        },
        "Women's Shoes": {
            "Born": {
                'url': 'https://www.bornshoes.com/ViewAll.aspx?GenderID=8&CategoryID=140&PageNo=1&SortType=P',
            }
        },
        "Women's Heels": {
            "Born": {
                'url': 'https://www.bornshoes.com/ViewAll.aspx?GenderID=8&CategoryID=120&PageNo=1&SortType=P',
            }
        },
        "Men's Sandals": {
            "Born": {
                'url': 'https://www.bornshoes.com/ViewAll.aspx?GenderID=4&CategoryID=110&PageNo=1&SortType=P',
            }
        },
        "Men's Boots": {
            "Born": {
                'url': 'https://www.bornshoes.com/ViewAll.aspx?GenderID=4&CategoryID=102&PageNo=1&SortType=P',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BornshoesSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                url = re.sub(r'PageNo=\d+', 'PageNo=1000', url, flags=re.IGNORECASE)
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.bornshoes.com/Product.aspx?ProductID=18711&cat=130
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title price color sku selected_color description features
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//nav[@class="breadCrumb breadCrumb__product"]/ul/li/span/a/text()[normalize-space(.)]').getall(),
            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'title': response.xpath(u'normalize-space(//h1[@id="main-title"]/text())').get(),
            'price': response.xpath(u'normalize-space(//p[@class="productCoreInfo__price"]/span[@class="productCoreInfo__retail"]/text())').get(),
            'color': response.xpath(u'normalize-space(//span[@class="productCoreInfo__color"]/text())').get(),
            'sku': response.xpath(u'normalize-space(//span[@class="productCoreInfo__stock"]/text())').get(),
            'selected_color': response.xpath(u'normalize-space(//h2[@id="born-swatches-title"]/span[@id="colorname"]/text())').get(),
            'sizes': response.xpath('//ul[@id="size-group"]/li/button/text()').getall(),
            'description': response.xpath(u'normalize-space(//section[@class="description"]/div[@class="description__copy"]/text())').get(),
            'features': response.xpath('//section[@class="features"]/div[@class="features__copy"]/ul/li/text()').getall(),
        }


    def parse(self, response):
        """
        @url https://www.bornshoes.com/ViewAll.aspx?GenderID=8&CategoryID=130&PageNo=1000&SortType=P
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//ul[@id="grid"]/li[contains(@class, "productCard")]'):
            products_url = product.xpath('.//ul[@class="product-swatches"]/li/button/@data-href').extract() \
                or [product.xpath('.//a/@href').extract_first()]
            for product_url in products_url:
                product_url = self.base_url + '/' + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
