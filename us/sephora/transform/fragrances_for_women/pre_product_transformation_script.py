import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            product_type = ''
            match = re.findall(re.compile(r'\bEau de Parfum\b|\bEau de Toilette\b|\bEAU VIVE\b'
                r'|\bL’EAU\b|\bIntense\b|\bEau de Parfum Twist and Spray\b|\bLotion\b|\bPARFUM\b',
                re.IGNORECASE), record.get('title', ''))
            if match:
                product_type = match[0]

            product_type_meta=['L’Eau', 'Eau Vive', 'Parfum', 'Eau De Parfum', 'Lotion', 'Eau De Toilette','EAU TENDRE']

            formulation = record.get('Formulation', [])
            model = record.get('title', '')
            model = re.sub(re.compile(r'\bChanel\b', re.IGNORECASE), '', model)
            # if product_type:
            #     model = re.sub(re.compile(r'\b'+product_type+r'\b', re.IGNORECASE), '', model)
            model=re.sub("L'eau","L'EAU",model)
            words=model.split()
            model=' '.join(list(sorted(set(words),key=words.index)))
            if formulation:
                model = re.sub(re.compile(pattern_words(formulation), re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            for meta in product_type_meta:
                if re.findall(re.compile(r"{}".format(meta),re.IGNORECASE),model)!=[]:
                    model=re.sub(re.compile(r"{}".format(meta),re.IGNORECASE),meta.title(),model)
            if re.findall(re.compile(r"\bset\b",re.IGNORECASE),model)!=[]:
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'product_type': product_type,
                'bristle_material': ','.join(record.get('Bristle Type', [])),
                #'formulation': ','.join(formulation),
                'volume': record.get('size', ''),
                'fragrance_type': ','.join(record.get('Fragrance Type', [])),
                'description': ','.join(record.get('quick_description', '')),

                'facial_coverage': ','.join(record.get('Coverage', [])),
                'product_line': ','.join(record.get('Fragrance Family', [])),
            }

            return transformed_record
        else:
            return None
