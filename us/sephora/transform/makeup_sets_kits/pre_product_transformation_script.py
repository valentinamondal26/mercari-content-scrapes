import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            brand = record.get('brand', '')

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            product_type = record.get('Skincare', []) or []
            product_type.sort()

            formulation = record.get('Formulation', [])
            model = record.get('title', '')
            model = re.sub(re.compile(r'\bSet\b|®|™|-|\s{1}Collection\b|\bpalette\b', re.IGNORECASE), '', model)
            # if brand:
            #     model = re.sub(re.compile(r'\b'+brand+r'\b', re.IGNORECASE), '', model)
            if product_type:
                model = re.sub(re.compile(pattern_words(product_type), re.IGNORECASE), '', model)
            if formulation:
                model = re.sub(re.compile(pattern_words(formulation), re.IGNORECASE), '', model)
            model = model.strip().strip('+')
            model = re.sub(r'\s+', ' ', model).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'benefit': ','.join(record.get('Benefits', [])),
                'bristle_material': ','.join(record.get('Bristle Type', [])),
                'color': ','.join(record.get('Color Family', [])),
                'formulation': ','.join(formulation),
                'size': record.get('size', ''),
                'volume': ','.join(record.get('Volume', [])),
                'coverage': ','.join(record.get('Coverage', [])),
                'description': ','.join(record.get('quick_description', '')),

                'finish': ','.join(record.get('Finish', [])),
                'skin_type': ','.join(record.get('Skin Type', [])),

                'model_sku': record.get('model_sku', ''),
            }

            return transformed_record
        else:
            return None
