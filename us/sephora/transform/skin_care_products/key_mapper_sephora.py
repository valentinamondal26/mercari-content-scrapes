class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        product_type = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity["attribute"]:
                    model = entity["attribute"][0]["id"]
            elif "product_type" == entity['name']:
                if entity["attribute"]:
                    product_type = entity["attribute"][0]["id"]

        key_field = model + product_type
        return key_field
