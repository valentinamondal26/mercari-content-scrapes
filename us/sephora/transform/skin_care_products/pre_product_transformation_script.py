import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            product_type = record.get('Skincare', []) or []
            product_type.sort()
            concerns = record.get('Concerns', [])
            # for x in ['Value & Gift Sets', 'Shop by Concern', 'Mini Size']:
            #     if x in product_type:
            #         product_type.remove(x)

            # product_type.remove('Value & Gift Sets')
            # product_type.remove('Shop by Concern')
            # product_type.remove('Mini Size')
            formulation = record.get('Formulation', [])
            pattern=re.compile(r"balm|bar\s*soap|cream|gel|mask|oil|serum",re.IGNORECASE)
            if formulation==[]:
                if re.findall(pattern,record.get('title',''))!=[]:
                    formulation.append(re.findall(pattern,record.get('title',''))[0])
                

            model = record.get('title', '')
            model = re.sub(re.compile(r'\bExclusive\b|™|®|\bDrunk Elephant\b|\bSephora Collection\b|\bSephora\b|\bSet\b|\d{1}\s*\%\s*[A-Z]{3}\s*\+*', re.IGNORECASE), '', model)

            
    
            if product_type!=[]:
                model = re.sub(re.compile(pattern_words(product_type), re.IGNORECASE), '', model)
            if concerns!=[]:
                model = re.sub(re.compile(pattern_words(concerns), re.IGNORECASE), '', model)
            
            if model[len(model)-1]=='+':
                model=model[0:len(model)-1].strip()

            model = re.sub(r'\s+', ' ', model).strip()

            if concerns!=[]:
                concerns=', '.join(concerns)
            else:
                concerns=''

            pattern=re.compile(r"lip|balm",re.IGNORECASE)
            matches=re.findall(pattern,record.get('title', ''))
            if matches!=[]:
                product_type.append('Lip Treatment')
            
            pattern=re.compile(r"hair|collection|hydrate|set",re.IGNORECASE)
            matches=re.findall(pattern,record.get('title', ''))
            if  matches!=[]:
                product_type.append('Wellness')
                
            pattern=re.compile(r"eye",re.IGNORECASE)
            matches=re.findall(pattern,record.get('title', ''))
            if  matches!=[]:
                product_type.append('Eye Care')  
                
            pattern=re.compile(r"wipe{1}s*|film{1}s*|cleansing",re.IGNORECASE)
            matches=re.findall(pattern,record.get('title', ''))
            if  matches!=[]:
                product_type.append('Cleanser')
                
            pattern=re.compile(r"mask{1}s*",re.IGNORECASE)
            matches=re.findall(pattern,record.get('title', ''))
            if  matches!=[]:
                product_type.append('Mask')
                
            pattern=re.compile(r"pad{1}s*|brush{1}s*",re.IGNORECASE)
            matches=re.findall(pattern,record.get('title', ''))
            if matches!=[]:
                product_type.append('High Tech Tool')

            product_type=[product[0:len(product)-1] if product[len(product)-1]=="s" and product!='Wellness' else product for product in product_type]

            description=record.get('quick_description', '')
            description=[' '.join(des.replace('™','').split()) for des in description]

            words=['Uneven skin tone','Firmness','Texture']
            concerns_words=concerns.split(", ")
            for concern in concerns_words:
                position=concerns_words.index(concern)
                concerns_words[position]=re.sub(re.compile(r"Uneven skin tone|Firmness|Texture",re.IGNORECASE),concern.title(),concern)
            concerns=", ".join(concerns_words)

            product_type_meta=["Value & Gift Sets*","Mini Size","Clean Skincare","Shop by Concern","Moisturizers*","Cleansers*","Treatments*","Eye Care","Masks*","Sun Care","Lip Treatments*","Vegan Skincare","Serums*"]
            

            for type in product_type_meta:
                pattern=re.compile(r"{}".format(type),re.IGNORECASE)
                if re.findall(pattern,model)!=[]:
                    if "*" in type:
                        type=' '.join(type.replace("*",'').split())
                    if type=="Serums":
                        type="Serum"
                    product_type.append(type)
                    model=re.sub(pattern,'',model)

            model=re.sub(r"\s\-\s|\-\s|\s\-","-",model)
            model=' '.join(model.split())
                        

            
            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'product_type': ', '.join(list(set(product_type))),
                'age_range': ', '.join(record.get('Age Range', [])),
                'concerns': concerns,
                'finish': ','.join(record.get('Finish', [])),
                'formulation': ', '.join(formulation),
                'size': record.get('size', ''),
                'skin_type': ', '.join(record.get('Skin Type', [])),
                'description': ','.join(description),

                'spf': ','.join(record.get('Sun Protection', [])),
                'base_ingredients': ', '.join(record.get('Ingredient Preferences', [])),

                'benefit': ', '.join(record.get('Benefits', '')),
                'color': ', '.join(record.get('Color Family', '')),
                'coverage': ', '.join(record.get('Coverage', '')),
            }

            return transformed_record
        else:
            return None
