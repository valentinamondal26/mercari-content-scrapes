import hashlib
import re


class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            formulation = record.get('Formulation', [])
            color = record.get('Color Family', [])

            model = record.get('title', '')
            keywords = ['palettes', 'palette']
            keywords.extend(formulation)
            keywords.extend(color)

            model = re.sub(re.compile(r'|'.join(
                list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), '', model)
            model = re.sub(r'-\s*$|\+\s*$', '', model)
            model = re.sub(re.compile(
                r"[\-|\–]\s*Peaches and Cream Collection|[\-|\–]\s*Cheers to 20 Years Collection", re.IGNORECASE), "", model)
            model = re.sub(r'\s+', ' ', model).strip()

            description = ' '.join(record.get('long_description', ''))
            description = description.replace('\r', ' ').replace('\n', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "image": record.get('image', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'color': ', '.join(color),
                'concerns': ', '.join(record.get('Concerns', [])),
                'finish': ', '.join(record.get('Finish', [])),
                # 'ingredient': ', '.join(record.get('Ingredient Preferences', [])),
                # 'size': ", ".join(sizes),
                'formulation': ', '.join(formulation),
                'description': description,
            }

            return transformed_record
        else:
            return None
