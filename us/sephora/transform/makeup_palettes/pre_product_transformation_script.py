import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            formulation = record.get('Formulation', [])
            model = record.get('title', '')
            model = re.sub(re.compile(r'\bpalette\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            description=' '.join(record.get('long_description',''))
            pattern=re.compile(r"\d+\.*\d*\s*x{1}\s*\d+\.*\d*\s*oz{1}",re.IGNORECASE)
            sizes=re.findall(pattern,description)
            sizes=[s.strip() for s in sizes]




            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate', ''),
                "status_code": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'shade': ','.join(record.get('Color Family', [])),
                'finish': ','.join(record.get('Finish', [])),
                'ingredient': ','.join(record.get('Ingredient Preferences', [])),
                'size': ", ".join(sizes),
                'formulation': ','.join(formulation),
                'description': ','.join(record.get('quick_description', '')),
            }

            return transformed_record
        else:
            return None
