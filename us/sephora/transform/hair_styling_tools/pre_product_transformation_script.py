import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            brand = record.get('brand', '')

            model = record.get('title', '')
            model = re.sub(re.compile(r'™', re.IGNORECASE), '', model)
            if brand:
                model = re.sub(re.compile(r'\b'+brand+r'\b', re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()

            short_description = record.get('short_description', '')
            d=list(map(lambda x: x.strip().strip(':'), short_description))
            details = dict(zip(d[::2], d[1::2]))
            hair_type = details.get('Hair Type', '')
            hair_style = details.get('Hair Texture', '')

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": brand,
                "ner_query": ner_query.replace('™',''),
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'color': record.get('color', ''),
                'concern': ', '.join(record.get('Concerns', [])),
                'description': ','.join(record.get('quick_description', '')).replace('™',''),

                'hair_type': hair_type,
                'hair_style': hair_style,
            }

            return transformed_record
        else:
            return None
