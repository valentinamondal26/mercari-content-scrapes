import hashlib
import re

class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            def pattern_words(words):
                return r'\b' + r'\b|\b'.join(words) + r'\b'

            product_type = record.get('Face', []) or []
            concerns = record.get('Concerns', [])
            # color = ','.join(record.get('Color Family', ''))
            color = record.get('color', '')

            formulation = record.get('Formulation', [])
            model = record.get('title', '')
            model = re.sub(re.compile(r'(with){0,1}\s*SPF\s*\d+\+*|™|®|[\–|\-]\s*Peaches and Cream Collection|[\-|\–]\s*Peaches and Collection', re.IGNORECASE), '', model)

            product_type_meta=["Foundation","BB \& CC Cream","Tinted Moisturizer","Concealer","Face Primer","Setting Spray \& Powder"]
            if product_type==[]:
                for meta in product_type_meta:
                    pattern=re.compile(r"{}".format(meta),re.IGNORECASE)
                    if re.findall(pattern,model)!=[]:
                        product_type.append(meta)
            # if product_type:
            #     model = re.sub(re.compile(pattern_words(product_type), re.IGNORECASE), '', model)
            product_type.sort()
            if color!='':
                model = re.sub(re.compile(color, re.IGNORECASE), '', model)
            if concerns:
                model = re.sub(re.compile(pattern_words(concerns), re.IGNORECASE), '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            if formulation==[]:
                formulations=["Cream","Liquid","Loose powder","Pressed powder"]
                for fr in formulations:
                    if fr in model:
                        formulation.append(fr)
            
            # if formulation!=[]:
            #     model = re.sub(re.compile(pattern_words(formulation), re.IGNORECASE), '', model)
            model=' '.join(model.split())

            pattern=re.compile(r"\d+\.*\d*|for neutral undertones|for warm undertones|\(|\)",re.IGNORECASE)
            color=re.sub(pattern,'',color).strip()
            if re.findall(re.compile(r"\bPalette\b", re.IGNORECASE), record.get("title")):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),
                "ner_query": ner_query,
                "title": record.get('title', ''),

                "model": model,
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },

                'product_type': ', '.join(product_type),
                'age_range': ', '.join(record.get('Age Range', [])),
                'concerns': ', '.join(concerns),
                'finish': ', '.join(record.get('Finish', [])),
                'formulation': ', '.join(formulation),
                'size': record.get('size', ''),
                'skin_type': ', '.join(record.get('Skin Type', [])),
                'description': ', '.join(record.get('quick_description', '')),

                'spf': ', '.join(record.get('Sun Protection', [])),
                # 'base_ingredients': ','.join(record.get('Ingredient Preferences', [])),

                'benefit': ', '.join(record.get('Benefits', '')),
                'color': color,
                'coverage': ', '.join(record.get('Coverage', '')),
            }

            return transformed_record
        else:
            return None
