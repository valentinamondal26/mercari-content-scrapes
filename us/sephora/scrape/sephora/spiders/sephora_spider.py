'''
https://mercari.atlassian.net/browse/USDE-755 - Tarte Face Makeup Products
https://mercari.atlassian.net/browse/USDE-756 - Too Faced Face Makeup Products
https://mercari.atlassian.net/browse/USCC-351 - IT Cosmetics Face Makeup Products
https://mercari.atlassian.net/browse/USDE-757 - Too Faced Makeup Palettes
https://mercari.atlassian.net/browse/USCC-143 - Urban Decay Makeup Palettes
https://mercari.atlassian.net/browse/USCC-207 - Chanel Fragrances for Women
https://mercari.atlassian.net/browse/USCC-139 - Dyson Hair Styling Tools
https://mercari.atlassian.net/browse/USCC-360 - Sephora Makeup Sets & Kits
https://mercari.atlassian.net/browse/USCC-355 - Sephora Skin Care Products
https://mercari.atlassian.net/browse/USCC-264 - Drunk Elephant Skin Care Products
'''

import scrapy
import random
from datetime import datetime
import os
from scrapy.exceptions import CloseSpider

import json
from parsel import Selector

class SephoraSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from sephora.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Fragrances for Women
        2) Hair Styling Tools
        3) Makeup Sets & Kits
        4) Skin Care Products
        5) Makeup Palettes
        6) Face Makeup Products

    brand : str
        brand to be crawled, Supports
        1) Chanel
        2) Dyson
        3) Sephora
        4) Urban Decay
        5) Drunk Elephant
        6) IT Cosmetics

    Command e.g:
    scrapy crawl sephora -a category='Skin Care Products' -a brand='Drunk Elephant'
    scrapy crawl sephora -a category='Fragrances for Women' -a brand='Chanel'
    scrapy crawl sephora -a category='Hair Styling Tools' -a brand='Dyson'
    scrapy crawl sephora -a category='Makeup Sets & Kits' -a brand='Sephora'
    scrapy crawl sephora -a category='Skin Care Products' -a brand='Sephora'
    scrapy crawl sephora -a category='Makeup Palettes' -a brand='Urban Decay'
    scrapy crawl sephora -a category='Face Makeup Products' -a brand='IT Cosmetics'

    scrapy crawl sephora -a category='Makeup Palettes' -a brand='Too Faced'
    scrapy crawl sephora -a category='Face Makeup Products' -a brand='Too Faced'
    scrapy crawl sephora -a category='Face Makeup Products' -a brand='Tarte'

    """

    name = 'sephora'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''

    merge_key = 'target_url'

    source = 'sephora.com'
    blob_name = 'sephora.txt'

    url_dict = {
        'Fragrances for Women': {
            'Chanel': {
                'url': 'https://www.sephora.com/brand/chanel/fragrances-for-women',
                'filters': ['Bristle Type', 'Coverage', 'Formulation', 'Fragrance Family',
                    'Fragrance Impression', 'Fragrance Type', ],
            }
        },
        'Hair Styling Tools': {
            'Dyson': {
                'url': 'https://www.sephora.com/brand/dyson/all',
                'filters': ['Concerns'],
            }
        },
        'Makeup Sets & Kits': {
            'Sephora': {
                'url': 'https://www.sephora.com/brand/sephora-collection/makeup-kits-makeup-sets',
                'filters': ['Benefits', 'Bristle Type', 'Color Family', 'Coverage', 'Finish',
                    'Formulation', 'Skin Type', 'Volume'],
            }
        },
        'Skin Care Products': {
            'Sephora': {
                'url': 'https://www.sephora.com/brand/sephora-collection/skincare',
                'filters': ['Age Range', 'Benefits', 'Color Family', 'Concerns', 'Coverage', 'Finish',
                    'Formulation', 'Ingredient Preferences', 'Skin Type', 'Size', 'Sun Protection'],
                'sub_categories': ['Clean Skincare', 'Moisturizers', 'Cleansers', 'Treatments',
                    'Eye Care', 'Masks', 'Sun Care', 'Lip Treatments',],
            },
            'Drunk Elephant': {
                'url': 'https://www.sephora.com/brand/drunk-elephant/skincare',
                'filters': ['Age Range', 'Benefits', 'Color Family', 'Concerns', 'Coverage', 'Finish',
                    'Formulation', 'Ingredient Preferences', 'Skin Type', 'Size', 'Sun Protection'],
                'sub_categories': ['Clean Skincare', 'Moisturizers', 'Cleansers', 'Treatments',
                    'Eye Care', 'Masks', 'Sun Care', 'Lip Treatments',],
            },
        },
        'Face Makeup Products': {
            'IT Cosmetics': {
                'url': 'https://www.sephora.com/brand/it-cosmetics/face-makeup',
                'filters': ['Age Range', 'Benefits', 'Color Family', 'Concerns', 'Coverage', 'Finish',
                    'Formulation', 'Ingredient Preferences', 'Skin Type', 'Size', 'Sun Protection'],
                'sub_categories': ['Foundation', 'BB & CC Cream', 'Tinted Moisturizer', 'Concealer', 'Face Primer', 'Setting Spray & Powder', ],
            },
            'Too Faced': {
                'url': 'https://www.sephora.com/brand/too-faced/face-makeup',
                'filters': ['Age Range', 'Benefits', 'Color Family', 'Concerns', 'Coverage', 'Finish',
                    'Formulation', 'Ingredient Preferences', 'Skin Type', 'Size', 'Sun Protection'],
                'sub_categories': ['Foundation', 'Concealer', 'Face Primer', 'Setting Spray & Powder', 'Highlighter', 'Contour', 'Face Sets', ],
            },
            'Tarte': {
                'url': 'https://www.sephora.com/brand/tarte/face-makeup',
                'filters': ['Age Range', 'Benefits', 'Color Family', 'Concerns', 'Coverage', 'Finish',
                    'Formulation', 'Ingredient Preferences', 'Skin Type', 'Size', 'Sun Protection'],
                'sub_categories': ['Foundation', 'BB & CC Cream', 'Tinted Moisturizer', 'Concealer', 'Face Primer', 'Setting Spray & Powder', 'Highlighter', 'Contour', 'Face Sets', ],
            },
        },
        'Makeup Palettes': {
            'Urban Decay': {
                'url': 'https://www.sephora.com/brand/urban-decay/makeup-palettes',
                'filters': ['Color Family', 'Finish', 'Formulation', 'Ingredient Preferences',
                    'Size'],
            },
            'Too Faced': {
                'url': 'https://www.sephora.com/brand/too-faced/makeup-palettes',
                'filters': ['Color Family', 'Concerns', 'Finish', 'Formulation', 'Ingredient Preferences',
                    'Size'],
            }
        },
    }

    filter_list = []

    sub_categories = []

    extra_item_infos = {}

    base_url = 'https://www.sephora.com'

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SephoraSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filter_list = self.url_dict.get(category, {}).get(brand, {}).get('filters', [])
        self.sub_categories = self.url_dict.get(category, {}).get(brand, {}).get('sub_categories', [])
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def contracts_mock_function(self):
        # self.filter_list = ['Color Family', 'Finish', 'Formulation', 'Ingredient Preferences','Size']
        self.filter_list =  ['Age Range', 'Benefits', 'Color Family', 'Concerns', 'Coverage', 'Finish',
                    'Formulation', 'Ingredient Preferences', 'Skin Type', 'Size', 'Sun Protection'],
        self.sub_categories = ['Foundation', 'Concealer', 'Face Primer', 'Setting Spray & Powder', 'Highlighter', 'Contour', 'Face Sets', ]


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_filters)


    def crawlDetail(self, response):
        # extra_item_info = response.meta.get('extra_item_info', {})
        """
        @url https://www.sephora.com/product/on-run-mini-eyeshadow-palette-P439095
        @scrape_values title image price model_sku quick_description long_description
        @meta {"use_proxy":"True"}
        """

        d = response.xpath('//script[@id="linkJSON"]/text()').extract_first(default='')
        if d:
            j = json.loads(d)
            for data in j:
                if data.get('class', '') == 'RegularProductTop':
                    item = data.get('props', {}).get('currentProduct', {})
                    title = item.get('displayName', '')
                    quick_look_description = Selector(item.get('quickLookDescription', '')).xpath('//text()').extract()
                    long_description = Selector(item.get('longDescription', '')).xpath('//text()').extract()
                    short_description = Selector(item.get('shortDescription', '')).xpath('//text()').extract()
                    # d=list(map(lambda x: x.strip().strip(':'), short_description))
                    # details = dict(zip(d[::2], d[1::2]))

                    # yield {
                    #     'title': title,
                    #     'target_url': response.url,
                    # }
                    # return

                    for variant in item.get('regularChildSkus', []) or [item.get('currentSku', {})]:
                        size = variant.get('size', '')
                        sku = variant.get('skuId', '')
                        image = variant.get('skuImages', {}).get('image450', '')
                        targeturl = variant.get('targetUrl', '')
                        if targeturl:
                            id = self.base_url + targeturl
                            # if not self.extra_item_infos.get(id, None):
                            #     self.extra_item_infos[id] = {}

                            # self.extra_item_infos.get(id).update(extra_item_info)

                            yield {
                                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                'statusCode': str(response.status),
                                'category': self.category,
                                'brand': self.brand,
                                'id': id,

                                'title': title,
                                'price': variant.get('listPrice', ''),
                                'image': self.base_url + image if image else '',
                                'model_sku': sku,
                                # 'details': details,
                                'quick_description': quick_look_description,
                                'long_description': long_description,
                                'short_description': short_description,
                                'color': variant.get('variationValue', '') if variant.get('variationType', '') == 'Color' else '',
                                'size': size,
                                'target_url': response.url,
                                # 'target_url': response.meta.get('target_url', '') or response.url,
                            }


    def create_subcategories_request(self, category, brand_id):
        for sub_category in category.get('subCategories', []):
            if sub_category.get('displayName', '') in self.sub_categories:
                url = 'https://www.sephora.com/api/catalog/brands/{brand_id}?currentPage=1&pageSize={count}&node={catalog_id}&content=true&includeRegionsMap=true'.format(
                    brand_id=brand_id, catalog_id=sub_category.get('node', ''), count=sub_category.get('recordCount', '')
                )
                request = self.create_request(url=url, callback=self.parse)
                request.meta['extra_item_info'] = {
                    category.get('displayName', ''): sub_category.get('displayName', '')
                }
                print(request.meta['extra_item_info'])
                yield request


    def get_matched_sub_categories(self, category, catalog_id):
        if category.get('node', '') == catalog_id:
            return category
        sub_categories = category.get('subCategories', [])
        if sub_categories:
            for sub_category in sub_categories:
                return self.get_matched_sub_categories(sub_category, catalog_id)
        else:
            return []


    def parse_filters(self, response):
        """
        @url https://www.sephora.com/brand/too-faced/face-makeup
        @returns requests 19
        @meta {"use_proxy":"True"}
        """
        #No of request mentioned in contract is 19 because of total number of filers, subcategories and 1 one api request

        d = response.xpath('//script[@id="linkJSON"]/text()').extract_first(default='')
        if d:
            j = json.loads(d)
            for data in j:
                if data.get('class', '') == 'CatalogPage':
                    props = data.get('props', {})

                    brand_id = props.get('brandId', '')
                    catalog_id = props.get('catalogId', '')
                    if catalog_id == 'all':
                        catalog_id = ''
                    # page_size = props.get('pageSize', '')
                    totalProducts = props.get('totalProducts', '')

                    #categories
                    if self.sub_categories:
                        for category in props.get('categories', []):
                            for request in self.create_subcategories_request(self.get_matched_sub_categories(category, catalog_id), brand_id):
                                yield request


                    #filters
                    for refinement in props.get('allRefinements', []):
                        # print(refinement['displayName'], ':', [(v['refinementValueDisplayName'], v['refinementValueId']) for v in refinement['values']])
                        if refinement.get('displayName', '') in self.filter_list:
                            for v in refinement.get('values', []):
                                url = 'https://www.sephora.com/api/catalog/brands/{brand_id}?ref={refinement_id}&currentPage=1&pageSize={count}&node={catalog_id}&content=true&includeRegionsMap=true'.format(
                                    brand_id=brand_id, refinement_id=v.get('refinementValueId', ''), catalog_id=catalog_id, count=totalProducts
                                )
                                request = self.create_request(url=url, callback=self.parse)
                                request.meta['extra_item_info'] = {
                                    refinement.get('displayName', ''): v.get('refinementValueDisplayName', '')
                                }
                                yield request

                    url = 'https://www.sephora.com/api/catalog/brands/{brand_id}?currentPage=1&pageSize={count}&node={catalog_id}&content=true&includeRegionsMap=true'.format(
                        brand_id=brand_id, catalog_id=catalog_id, count=totalProducts
                    )
                    yield self.create_request(url=url, callback=self.parse)



    # def parse(self, response):
    #     d = response.xpath('//script[@id="linkJSON"]/text()').extract_first(default='')
    #     if not d:
    #         print('d is null')
    #     if d:
    #         j = json.loads(d)
    #         for data in j:
    #             if data.get('class', '') == 'CatalogPage':
    #                 for item in data.get('props', {}).get('products', []):
    #                     print(f"targetUrl:{item['targetUrl']}")
    #                     url = item.get('targetUrl', '')
    #                     if url:
    #                         id = self.base_url + url

    #                         if not self.extra_item_infos.get(id, None):
    #                             self.extra_item_infos[id] = {}

    #                         self.extra_item_infos.get(id).update(response.meta.get('extra_item_info', {}))

    #                         request = self.create_request(url=id, callback=self.crawlDetail)
    #                         # request.meta['extra_item_info'] = response.meta.get('extra_item_info', {})
    #                         # yield request
    #                         yield {
    #                             'target_url': id,
    #                         }


    def update_extra_item_infos(self, id, extra_item_info):
        info = self.extra_item_infos.get(id, {})
        for key, value in extra_item_info.items():
            l = info.get(key, [])
            if value not in l:
                l.append(value)
                info[key] = l
        #print(info)
        self.extra_item_infos[id] = info


    def parse(self, response):
        """
        @url https://www.sephora.com/api/catalog/brands/3806?ref=100080&currentPage=1&pageSize=10&node=1050049&content=true&includeRegionsMap=true
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        data = json.loads(response.text)
        for item in data.get('products', []):
            # print(f"targetUrl:{item['targetUrl']}")
            url = item.get('targetUrl', '')
            if url:
                id = self.base_url + url

                if not self.extra_item_infos.get(id, None):
                    self.extra_item_infos[id] = {}

                self.update_extra_item_infos(id, response.meta.get('extra_item_info', {}))
                # self.extra_item_infos.get(id).update(response.meta.get('extra_item_info', {}))

                request = self.create_request(url=id, callback=self.crawlDetail)
                # request.meta['extra_item_info'] = response.get('extra_item_info', {})
                # request.meta['target_url'] = id
                yield request


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request