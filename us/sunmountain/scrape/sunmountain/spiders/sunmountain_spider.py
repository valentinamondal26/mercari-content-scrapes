'''
https://mercari.atlassian.net/browse/USCC-755 - Sun Mountain Golf Bags
https://mercari.atlassian.net/browse/USCC-756 - Sun Mountain Push-Pull Golf Carts
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import copy
import re


class SunmountainSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from sunmountain.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Golf Bags
        2) Push-Pull Golf Carts

    brand : str
        brand to be crawled, Supports
        1) Sun Mountain

    Command e.g:
    scrapy crawl sunmountain -a category="Golf Bags" -a brand="Sun Mountain"
    scrapy crawl sunmountain -a category="Push-Pull Golf Carts" -a brand="Sun Mountain"
    """

    name = 'sunmountain'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'sunmountain.com'
    blob_name = 'sunmountain.txt'

    base_url = 'https://shop.sunmountain.com'

    url_dict = {
        "Golf Bags": {
            "Sun Mountain": {
                'url': 'https://shop.sunmountain.com/golf-bags',
            }
        },
        "Push-Pull Golf Carts": {
            "Sun Mountain": {
                'url': 'https://shop.sunmountain.com/golf-carts',
            }
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(SunmountainSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://shop.sunmountain.com/vx-stand-bag-detail
        @meta {"use_proxy":"True"}
        @scrape_values id category_title image title price description color features specs
        """

        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'category_title': response.xpath('//span[@class="category-title"]/text()').get(),
            'image': response.xpath('//div[@class="MagicToolboxSelectorsContainer"]/div[contains(@id, "MagicToolboxSelectors")]/a/@href').get(),
            'title': response.xpath('//h1[@class="h1-no-bottomline"]/text()').get(),
            'price': response.xpath('//div[@class="product-price"]//span[@class="PricesalesPrice"]/text()').get(),
            'description': ' '.join(response.xpath('//div[@class="product-short-description"]/p/text()').getall()),
            'color': response.xpath('//div[@class="color-variations"]/div[@class="color-label"]/span[@id="color-variation-name"]/text()').get(),
            'features': response.xpath('//div[@class="product-description"]//text()').getall(),
            'specs': dict(zip(
                response.xpath('//div[contains(@class, "product-spec")]/div[@class="ax-attributes"]/div[@class="ax-attribute"]/span[@class="ax-attribute-label"]/text()').getall(),
                response.xpath('//div[contains(@class, "product-spec")]/div[@class="ax-attributes"]/div[@class="ax-attribute"]/span[@class="ax-attribute-value"]/text()').getall(),
            )),
        }

        variants = response.xpath('//div[@class="color-variations"]//a[contains(@id, "color-front-")]')
        if variants:
            for variant_info in variants:
                variant_id = re.sub('color-front-', '', variant_info.xpath('./@id').get())
                variant = copy.deepcopy(item)
                variant.update({
                    'id': response.url + f'?related_id={variant_id}',
                    'color': variant_info.xpath('./@title').get(),
                    'image': variant_info.xpath('./@href').get(),
                })
                yield variant
        else:
            yield item



    def parse(self, response):
        """
        @url https://shop.sunmountain.com/golf-bags
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@class="row"]/div[contains(@id, "product")]'):
            product_url = product.xpath('.//a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
