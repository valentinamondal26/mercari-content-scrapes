import hashlib
import re
from titlecase import titlecase
from itertools import chain

class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            features = ''.join(record.get('features', [])).replace('\r', '').replace('\n', '')
            ner_query =  (description + ' ' + features) or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            image = record.get('image', '') or ''
            if not image.startswith('https://shop.sunmountain.com'):
                image = 'https://shop.sunmountain.com' + image

            model = record.get('title', '')
            model = titlecase(model)

            color = record.get('color', '') or ''
            color = re.sub(r'\s*-\s*', '/', color)
            if len(color.split('/')) > 3:
                color = '/'.join(list(chain(color.split('/')[:2], ['Multi'])))
            color = color.title()

            if re.findall(r'\bGift Certificate\b', record.get('title', ''), flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": image,

                "model": model,
                "color": color,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
