import hashlib
import re
from titlecase import titlecase
from itertools import chain

class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '')
            features = ''.join(record.get('features', [])).replace('\r', '').replace('\n', '')
            ner_query =  (description + ' ' + features) or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            image = record.get('image', '') or ''
            if not image.startswith('https://shop.sunmountain.com'):
                image = 'https://shop.sunmountain.com' + image

            model = record.get('title', '')
            model = re.sub(r'\bBag\b', '', model)
            model = re.sub(r'\s+', ' ', model).strip()
            model = titlecase(model)

            color = record.get('color', '') or ''
            color = re.sub(r'\s*-\s*', '/', color)
            if len(color.split('/')) > 3:
                color = '/'.join(list(chain(color.split('/')[:2], ['Multi'])))
            color = color.title()

            system_category = ''
            product_type = ''
            if re.findall(r'\bBag\b', record.get('title', ''), flags=re.IGNORECASE):
                product_type = 'Bag'
                system_category = 'Golf Bags'
            else:
                product_type = 'Accessory'
                system_category = 'Other Golf Accessories'

            number_of_pockets = record.get('specs', {}).get('Pockets:', '')

            weight_lbs = record.get('specs', {}).get('Weight:', '')

            if re.findall(r'\bGift Certificate\b', record.get('title', ''), flags=re.IGNORECASE):
                return None

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": system_category,
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description,
                "image": image,

                "model": model,
                "color": color,
                "pockets": number_of_pockets,
                'product_type': product_type,
                'weight_lbs': weight_lbs,
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
