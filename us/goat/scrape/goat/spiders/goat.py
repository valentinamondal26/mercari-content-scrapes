'''
https://mercari.atlassian.net/browse/USDE-276 - All Women's Athletic Shoes
https://mercari.atlassian.net/browse/USDE-285 - All Men's Athletic Shoes
'''
import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import requests
import re
import os

from urllib.parse import urlparse, parse_qs


class GoatSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from goat.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women Sneakers
        2) Men's Athletic Shoes
        3) Women's Athletic Shoes

    brand : str
        category to be crawled, Supports
        1) Air Jordan
        2) Nike
        3) Adidas
        4) All

    Command e.g:
    scrapy crawl goat -a category='Men Athletic Shoes' -a brand='Air Jordan'
    scrapy crawl goat -a category='Womens Sneakers' -a brand='Yeezy'
    """

    name = "goat"
    category = None
    brand = None
    source = 'goat.com'
    blob_name = 'goat.txt'

    url = 'https://2fwotdvm2o-dsn.algolia.net/1/indexes/product_variants_v2/query?x-algolia-agent=Algolia for vanilla JavaScript 3.25.1&x-algolia-application-id={application_id}&x-algolia-api-key={search_api_key}'
    page = 0
    single_gender = ''
    query = ''
    brand_name = ''

    application_id = ''
    search_api_key = ''

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
        'http://shp-mercari-us-d00006.tp-ns.com:80',
        'http://shp-mercari-us-d00007.tp-ns.com:80',
        'http://shp-mercari-us-d00008.tp-ns.com:80',
        'http://shp-mercari-us-d00009.tp-ns.com:80',
        'http://shp-mercari-us-d00010.tp-ns.com:80',
    ]

    url_dict = {
        'Womens Sneakers': {
            'Nike': 'https://www.goat.com/search?category=women&query=nike%20women',
            'Adidas': 'https://www.goat.com/search?category=women&query=adidas',
            'Yeezy': 'https://www.goat.com/search?category=women&query=yeezy',
        },
        "Men's Athletic Shoes" : {
            'All': 'https://www.goat.com/sneakers?category=men',
            'Air Jordan': 'https://www.goat.com/sneakers/brand/air%20jordan',
            'Nike': 'https://www.goat.com/sneakers/brand/nike',
        },
        "Women's Athletic Shoes": {
            'All': 'https://www.goat.com/sneakers?category=women'
        }
    }


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(GoatSpider,self).__init__(*args, **kwargs)

        if  os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.get_url(category=self.category, brand=self.brand)
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        o = urlparse(self.launch_url)
        d = parse_qs(o.query)
        if d:
            self.single_gender = ",".join(d.get('category', []))
            self.query = ",".join(d.get('query', []))
        path = o.path
        sp = path.split('/')
        print(sp)
        if len(sp) > 1 and sp[-2] == 'brand':
            self.brand_name = brand=sp[-1]
        print('single_gender:', self.single_gender, ', query:', self.query, ', brand:', self.brand)
        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def contracts_mock_function(self):
        pass


    def get_url(self,category,brand):
        try:
            return self.url_dict[category][brand]
        except Exception as e:
            print('Exception occured:', e)
            raise(ValueError('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))


    def create_post_request(self):
        data = {"params": "distinct=true&facetFilters=(brand_name: {brand_name}), (single_gender: {single_gender})&facets=[\"size\"]&hitsPerPage=20&numericFilters=[]&page={page_no}&query={query}".format(single_gender=self.single_gender, page_no=str(self.page), query=self.query, brand_name=self.brand_name)}
        url = self.url.format(application_id=self.application_id, search_api_key=self.search_api_key)
        request = scrapy.Request(
            url=url,
            method='POST',
            body=json.dumps(data),
            headers={'Content-Type':'application/json'},
            callback=self.parse_listing_page
        )
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request


    def parse(self, response):
        """
        @url https://www.goat.com/search?category=women&query=yeezy
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        script = response.xpath('//script[contains(text(), "window.__ALGOLIA_SEARCH_API_KEY__ = ")]/text()').get(default='')
        match = re.findall(r'window.__ALGOLIA_SEARCH_API_KEY__ = \'(.*?)\';', script)
        if match:
            self.search_api_key = match[0]
        match = re.findall(r'window.__ALGOLIA_APPLICATION_ID__ = \'(.*?)\';', script)
        if match:
            self.application_id = match[0]
        if self.application_id and self.search_api_key:
            yield self.create_post_request()
        else:
            CloseSpider(f'Failed to get the application id or search api key from the {response.url}')


    def parse_listing_page(self, response):
        res = json.loads(response.text)
        items = res['hits']

        for item in items:

            colorway = item.get('details', '')
            # main_color = item.get('color', '')
            product = {
                'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'statusCode': str(response.status),
                'category': self.category,
                'brand': item.get('brand_name', '') or '' if self.brand == 'All' else self.brand,
                'title': item.get('name', '') or '',
                'id': 'https://www.goat.com/sneakers/' + item.get('slug', ''),
                'price': str(item['lowest_price_cents'] / 100) if item['lowest_price_cents'] is not None else "",
                'image': item.get('original_picture_url', '') or '',
                'description': item.get('story_html', '') or '',
                "sku": item.get('sku', '') or '',
                "product_line": item.get('silhouette', '') or '',
                "color": colorway,
                "breadcrumb": '{} | {} | {}'.format(item['brand_name'], item['silhouette'], item['name']),
                "condition": item.get('shoe_condition', '') or '',
                "designer": item.get('designer', '') or '',
                "release_date": item.get('release_date', '') or '',
                "nickname": item.get('nickname', '') or '',
                "material": item.get('upper_material', '') or '',
                "est_retail": str(item['retail_price_cents'] / 100) if item.get('retail_price_cents', 0) is not None else '',
            }

            product_template_id = item['product_template_id']
            product['product_template_id'] = product_template_id
            price_history = []

            # prices of new shoes
            new_shoes_url = 'https://www.goat.com/web-api/v1/instant_ship_availability/?product_template_id={product_id}'.format(product_id=product_template_id)
            headers = {
                'content-type': "application/json",
                'user-agent': 'HTTPie/0.9.2',
            }
            proxy = random.choice(self.proxy_pool)
            proxies = { "http"  : proxy, "https" : proxy}
            r = requests.get(new_shoes_url, headers=headers, proxies=proxies)
            print(r.text)
            new_shoes = r.json()
            for new_shoe in new_shoes:
                new_shoe_price = {
                    'amount': str(new_shoe['priceCents'] / 100) if new_shoe.get('priceCents', 0) is not None else '',
                    'condition': new_shoe['shoeCondition'],
                    'size': str(new_shoe.get('sizeUs', '')),
                    'date': datetime.now().strftime("%Y-%m-%d"),
                    'currencyCode': 'USD',
                }
                price_history.append(new_shoe_price)

            # prices of used shoes
            url = "https://www.goat.com/web-api/graphql"
            payload = "{\"query\":\"{\\n    viewer {\\n      productTemplate(slug: \\\"{slug}\\\") {\\n        name\\n        gender\\n        used_products_ex_goat_clean {\\n          id\\n          slug\\n          shoe_condition\\n          price_cents\\n          size\\n        }\\n      }\\n    }\\n  }\"}".replace('{slug}', item['slug'])
            proxy = random.choice(self.proxy_pool)
            proxies = { "http"  : proxy, "https" : proxy}
            r = requests.request("POST", url, data=payload, headers=headers, proxies=proxies)
            print(r.text)
            used_shoes_data = r.json()
            used_shoes = used_shoes_data.get('data', {}).get('viewer', {}).get('productTemplate', {}).get('used_products_ex_goat_clean', [])
            for used_shoe in used_shoes:
                used_shoe_price = {
                    'amount': str(used_shoe['price_cents'] / 100) if used_shoe.get('price_cents', 0) is not None else '',
                    'condition': used_shoe['shoe_condition'],
                    'size': str(used_shoe.get('size', '')),
                    'date': datetime.now().strftime("%Y-%m-%d"),
                    'currencyCode': 'USD',
                }
                price_history.append(used_shoe_price)

            product['priceHistory'] = price_history
            yield product

        if res['page'] < res['nbPages']:
            self.page = self.page + 1
            yield self.create_post_request()


    def create_request(self, url, callback, dont_filter=False):
        request = scrapy.Request(url=url, callback=callback, dont_filter=dont_filter)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
