import hashlib
import datetime


class Mapper(object):

    def cleanup_keys(self, key):
        return key.strip().lower().replace(" ","_")

    def isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record

    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(str(v))>0}
        return record

    def map(self, record):
        if record:
            record = self.pre_process(record)

            ner_query = record['description'] + record['condition']
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "")

            title = record['title']
            nickname = record['nickname']
            brand = record['brand']

            import re
            # for color in nickname.split(','):
            #     nickname_insensitive_pattern = re.compile(re.escape(color.strip()), re.IGNORECASE)
            #     color_stripped_title = nickname_insensitive_pattern.sub('', title)
            year_pattern = re.compile(r'(19|20)\d{2}')
            model = re.sub(year_pattern, '', title).replace('\'\'', '').strip()

            color_pattern = r"'(.*?)'"
            if nickname:
                for color in nickname.split(','):
                    model = re.sub(re.compile("'{0}'".format(color.strip()), re.IGNORECASE), '', model)
            model = re.sub(color_pattern, '', model)

            if brand:
                model = re.sub(re.compile(r'{0}|Wmns'.format(brand), re.IGNORECASE), '', model)

            model = re.sub(' +', ' ', model).strip()

            release_date = ''
            release_date_time = record['release_date']
            if release_date_time:
                try:
                    import pandas as pd
                    release_date = pd.Timestamp(datetime.datetime.strptime(release_date_time, "%Y-%m-%dT%H:%M:%S.%f%z")).round('1d').to_pydatetime().strftime("%Y-%m-%d")
                    # print(release_date)
                except Exception as e:
                    print('Exception occured: {}'.format(e))

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record['crawlDate'],
                "status_code": record['statusCode'],
                "title": title,
                "category": record['category'],
                "brand": brand,
                "image": record['image'],
                "description": record['description'],
                "product_line": record['product_line'],
                "designer": record['designer'],
                "breadcrumb": record['breadcrumb'],
                "sku": record['sku'],
                "condition": record['condition'],
                "color": record['color'],
                "release_date": release_date,
                "nickname": nickname,
                "material": record['material'],
                "model": model,
                "ner_query": ner_query,
                "estimated_original_price": {
                    "currencyCode": "USD",
                    "amount": record['est_retail']
                },
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                "listed_price_history ": record['priceHistory'] if record['priceHistory'] else [],
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None
