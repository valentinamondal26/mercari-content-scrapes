import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            description = record.get('description', '') or ''
            ner_query = description or record.get('title', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(r'\s-\s', ' ', model)
            model = re.sub(r'\bPlush\b', 'Stuffed Animal', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Stuffed Animal)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            if not re.findall(r'\bStuffed Animal\b', model, flags=re.IGNORECASE):
                model += ' Stuffed Animal'
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub(r'\bMonster Bowling Stuffed Animal\b', 'Monster Bowling Stuffed Animals', model, flags=re.IGNORECASE)

            details = ', '.join(record.get('details', []))

            plush_size = ''
            match = re.findall(r'(\d+\.*\d*)"H x (\d+\.*\d*)"L x (\d+\.*\d*)"W', details, flags=re.IGNORECASE)
            if match:
                h, _, _ = match[0]
                plush_size = f'{h} in.'

            if not plush_size:
                match = re.findall(r'(\d+\.*\d*)" x (\d+\.*\d*)" x (\d+\.*\d*)"', details, flags=re.IGNORECASE)
                if match:
                    plush_size = f'{max(match[0])} in.'

            if not plush_size:
                match = re.findall(r'(\d+\.*\d*)\s*("|inches|inch|in.)', description, flags=re.IGNORECASE)
                if match:
                    plush_size = f'{max(list(map(lambda x: x[0], match)))} in.'

            if re.findall(r'\bPlay Set\b|\bCritter-Wear\b', record.get('title', ''), flags=re.IGNORECASE):
                return None


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": description + ' ' + details,
                "image": record.get('image', ''),

                "model": model,
                "plush_size": plush_size,
                'model_sku': record.get('sku', ''),
                "msrp": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
