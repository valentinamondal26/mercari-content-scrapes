class Mapper:
    def map(self, record):

        model = ''
        plush_size = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "plush_size" == entity['name']:
                if entity["attribute"]:
                    plush_size = entity["attribute"][0]["id"]

        key_field = model + plush_size
        return key_field
