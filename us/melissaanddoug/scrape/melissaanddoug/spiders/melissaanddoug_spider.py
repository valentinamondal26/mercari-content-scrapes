'''
https://mercari.atlassian.net/browse/USCC-701 - Melissa & Doug Stuffed Animals
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class MelissaanddougSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from melissaanddoug.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Stuffed Animals
    
    brand : str
        brand to be crawled, Supports
        1) Melissa & Doug

    Command e.g:
    scrapy crawl melissaanddoug -a category="Stuffed Animals" -a brand="Melissa & Doug"
    """

    name = 'melissaanddoug'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'melissaanddoug.com'
    blob_name = 'melissaanddoug.txt'

    base_url = 'https://www.melissaanddoug.com'

    url_dict = {
        "Stuffed Animals": {
            "Melissa & Doug": {
                'url': 'https://www.melissaanddoug.com/our-toys/stuffed-animals-and-plush-toys/shop-all-stuffed-animals-and-plush/',
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(MelissaanddougSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info(f"Crawling for category: {self.category} and brand: {self.brand}, url: {self.launch_url}")


    def contracts_mock_function(self):
        pass


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, str):
                urls.append(self.launch_url)
            elif isinstance(self.launch_url, list):
                urls.extend(self.launch_url)
            for url in urls:
                yield self.create_request(url=url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.melissaanddoug.com/examine-and-treat-pet-vet-play-set/8520.html?cgid=our-toys-stuffed-animals-and-plush-shop-all-stuffed-animals-and-plush
        @meta {"use_proxy":"True"}
        @scrape_values id breadcrumb image title sku price details description
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'breadcrumb': response.xpath('//div[@class="breadcrumb breadcrumb-pdp"]/a/text()').getall(),
            'image': response.xpath('//meta[@property="og:image"]/@content').get(),
            'age_badge': ' '.join([x.strip() for  x in response.xpath('//span[@class="pdp-main--product-age age-badge"]').xpath(u'.//text()[normalize-space(.)]').getall()]),
            'title': response.xpath(u'normalize-space(//h1[@class="pdp-main--product-name regular-heading"]/text())').get(),
            'sku': response.xpath('//p[@class="pdp-main--product-id"]/span/text()').get(),
            'price': response.xpath('//div[@class="product-price"]/span[@class="product-standard-price bfx-price"]/text()').get(),
            'details': response.xpath('//div[@class="pdp-extras--short-description"]/ul/li/text()').getall(),
            'description': response.xpath('//div[@class="pdp-extras--section-content"]//p/text()').get(),
        }


    def parse(self, response):
        """
        @url https://www.melissaanddoug.com/our-toys/stuffed-animals-and-plush-toys/shop-all-stuffed-animals-and-plush/
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        for product in response.selector.xpath('//div[@id="search-result-items"]/div[contains(@class, "grid-item grid-item-product product-tile")]'):
            product_url = product.xpath('./a/@href').extract_first()
            if product_url:
                product_url = self.base_url + product_url
                yield self.create_request(url=product_url, callback=self.crawlDetail)

        next_page_link = response.xpath('//div[@class="pagination"]/a[contains(@class, "pagination--paging-btn page-next button")]/@href').extract_first()
        if next_page_link:
            next_page_link = self.base_url + product_url
            yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
