'''
https://mercari.atlassian.net/browse/USDE-1714 - Invicta Men's Wristwatches
https://mercari.atlassian.net/browse/USDE-1716 - Invicta Women's Wristwatches

'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class InvictastoresSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from invictastores.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Men's Wristwatches
        2) Women's Wristwatches

    brand: str
        1) invictastores

    Command e.g:
    scrapy crawl invictastores -a category="Women's Wristwatches" -a brand='Invicta'
    scrapy crawl invictastores -a category="Men's Wristwatches" -a brand='Invicta'
    """

    name = "invictastores"

    category = None
    brand = None

    merge_key = 'id'

    source = 'invictastores.com'
    blob_name = 'invictastores.txt'
    base_url = 'https://www.invictastores.com/'

    url_dict = {
        "Men's Wristwatches":{
            'Invicta':{
                'url':'https://invictastores.com/men?gender=1649&product_category=4420',
                'filters': ['Product Collection']
            }
        },
        "Women's Wristwatches":{
            'Invicta':{
                'url': 'https://invictastores.com/women?product_category=4420',
                'filters': ['Product Collection']
            }
        }
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            return

        super(InvictastoresSpider,self).__init__(*args, **kwargs)

        if not category and not brand:
            self.logger.error("Category should not be None")
            raise CloseSpider('category  not found')

        self.launch_url = self.url_dict.get(category,{}).get(brand,{}).get('url','')
        self.filters = self.url_dict.get(category,{}).get(brand,{}).get('filters','')
        self.category = category
        self.brand = brand
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{}'.format(category))
            raise CloseSpider('launch url  not found')

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def start_requests(self):
        if self.launch_url:
            meta = {"use_filter":True}
            yield self.create_request(url=self.launch_url, callback=self.parse_listing_page, meta=meta)


    def parse_listing_page(self, response):
        meta = response.meta

        item_urls = response.xpath("//li[@class='item last col-md-4 col-sm-4 col-xs-6 element']/div/div/a/@href").getall()
        for url in item_urls:
            yield self.create_request(url=url, callback=self.crawldetails, meta=meta)

        next_page_url = response.xpath("//a[@class='next i-next']/@href").get(default='')
        if next_page_url:
            if meta.get('fiter_name',''):
                yield self.create_request(url=next_page_url, callback=self.parse_listing_page, meta=meta)
            else:
                yield self.create_request(url=next_page_url, callback=self.parse_listing_page)

        if meta.get('use_filter',''):
            if self.filters:
                for apply_filter in self.filters:
                    apply_filter = apply_filter.replace(' ','_').lower()
                    for item in response.xpath("//li/a[contains(@href,'"+apply_filter+"')]"):
                        filter_url = item.xpath(".//@href").get(default='')
                        filter_name = item.xpath(".//text()").get(default='')
                        meta = {}
                        meta.update({'filter_name':apply_filter})
                        meta.update({meta['filter_name']:filter_name})
                        yield self.create_request(url=filter_url, callback=self.parse_listing_page, meta=meta)


    def crawldetails(self, response):
        meta = response.meta
        item = {
            'id': response.url,
            'category': self.category,
            'brand': self.brand,
            'statuscode': str(response.status),
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),

            'title': response.xpath("//div[@class='product-name']/h1/text()").get(default=''),
            'breadcrumb': " | ".join(response.xpath("//div[@class='breadcrumbs']/ul/li/a/text()").getall()),
            'selling_price': response.xpath("//span[@class='currentprice_mb']/text()").get(default=''),
            'base_price': response.xpath("//span[@class='baseprice_mb']/text()").get(default='') ,
            'image': response.xpath("//div[@data-effect='zoom']/@data-src").get(default=''),
            'description': response.xpath("//meta[@property='og:description']/@content").get(),
        }
        specs_dict = {}
        for spec in response.xpath("//div[@id='product-attribute-specs-table']/div[@class='col-md-3']"):
            spec_name =  spec.xpath(".//h4/b/text()").get(default='')
            specs_dict.update({spec_name:{}})
            for items in spec.xpath(".//p"):
                key = items.xpath(".//text()").get(default='').replace(":","").strip()
                value = items.xpath(".//span/text()").get(default='')
                specs_dict[spec_name].update({key: value})
        item.update(specs_dict)

        filter_name = meta.get('filter_name','')
        if filter_name:
            filter_data = dict.fromkeys([filter_name],meta[filter_name])
            item.update(filter_data)

        yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request