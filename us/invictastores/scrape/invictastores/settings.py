# -*- coding: utf-8 -*-


BOT_NAME = 'invictastores'

SPIDER_MODULES = ['invictastores.spiders']
NEWSPIDER_MODULE = 'invictastores.spiders'

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

CONCURRENT_REQUESTS = 16

DOWNLOAD_DELAY = 3

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
    'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'

# GCS settings - actual value be 'raw/invictastores.com/travel_systems/all/2019-02-06_00_00_00/json/walmart.txt'
GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'
