import hashlib
import re
import itertools
from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            case_size = record.get('Case & Dial',{}).get('Case Size','')

            color = []
            color_data = []
            case_tone = record.get('Case & Dial',{}).get('Case Tone','').replace('N/A','')
            dial_color = record.get('Case & Dial',{}).get('Dial Color','').replace('N/A','')
            color_data.append(case_tone.split(', '))
            color_data.append(dial_color.split(', '))
            color_data = list(itertools.chain(*color_data))
            [color.append(x) for x in color_data if x not in color and x] ###this is because when i use set function to remove duplicate the order of color gets changed.they mentioned to form color like case tone+ dial_color
            
            color = '/'.join(color)
            color=re.sub(re.compile(r'Aqua-Plating|Aqua Plating',re.IGNORECASE),'',color).replace('//','/')

            model = record.get('title','')
            model = re.sub(re.compile(r'(-) Model |Model (-)',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'Model',re.IGNORECASE),'',model)
            case_size_value = re.findall(r'\d+\.*\d*',case_size)[0]
            model = re.sub(re.compile(r'\b{case_size}mm\b|\b{case_size}\b|\bMM\b'.format(case_size=case_size_value),re.IGNORECASE),'',model) 
            model = re.sub(re.compile(r'\d+\.*\d*\s*mm',re.IGNORECASE),'',model)
            model = re.sub(r'\(|\)|\-','',model)
            model = re.sub(r'\bMens\b|\bWatch\b|\bWomens\b|\bMen\'s\b|\bWomen\'s|\bShot\-*blast\b', '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\b{}\b'.format(record.get('brand','')),re.IGNORECASE), '',model)
            model = re.sub(re.compile(r'\d{4,6}|\d\w{5}'),'',model)
            # model = re.sub(r'\(|\)','',model)
            model = re.sub(re.compile(r'\bBand\b|\bcase\b|\(|\)',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'\btone\b',re.IGNORECASE),'',model)
            # model = re.sub(re._compile(r'\bsteel\b|\bwith\b',re.IGNORECASE),'',model)
            model = re.sub(re._compile(r'\bwith\b',re.IGNORECASE),'',model)
            if 'Stainless Steel' not in color and 'Stainless Steel' in record['title']:
                color = color+'/Stainless Steel' 
                        
            color = re.sub(re.compile(r'StainlessSteel',re.IGNORECASE),'',color)
            color = re.sub(r'//','/',color)
           
            if color:
                if color[0]=='/':
                    color = color.replace('/','',1)

            if color and not color == 'N/A':        
                color_values = color.split('/')
                for col in color_values:
                    col = ' '.join(re.findall('.[^A-Z]*', col))
                    col = re.sub('\s+',' ',col)
                    model = re.sub(re.compile(r'\b{}\b'.format(col),re.IGNORECASE),'',model)
                model = re.sub(re.compile(r'ROSE|GOLD',re.IGNORECASE),'',model)
                remove_strings = ['Stainless', 'Steel', 'Stee']
                model = re.sub(r'|'.join(list(map(lambda x: r'\b'+x+r'\b',remove_strings))),'',model,flags=re.IGNORECASE).strip()
                if len(color_values) >= 3:
                    color = color_values[0]+'/'+'Multicolor'
            
            colors = color.split('/')
            if len(colors) == 2:
                if colors[0] == 'Stainless Steel' and colors[1] !='Multicolor':
                    color = colors[1]+'/'+colors[0]
            color = re.sub(re.compile(r'RoseGold',re.IGNORECASE),'Rose Gold',color)
            color = titlecase(color)
            model_words = re.findall('\w+',model)
            for words in model_words:
                try:
                   Color(words)
                   model = re.sub(re.compile(r'{}'.format(words),re.IGNORECASE),'',model)
                except ValueError:
                    pass
            model = re.sub(r',|\+|\/','',model)
            model = model+ ' Watch'
            model = re.sub(r'LIMITED EDITION SUPERMAN','Limited Edition Superman',model)
            model = ' '.join([word[0].upper()+word[1:] for word in model.split()])
            model = re.sub(r'(.*Model\s+)([TM]*-*\w+)(.*)',r'\1 \3',model,flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\bTM\b',re.IGNORECASE),'',model)
            if re.findall(r'Angel Quartz Tortoise Watch', model, flags=re.IGNORECASE):
                model = re.sub(r'\bTortoise\b', '', model, flags=re.IGNORECASE)
                color = f'{color}/Tortoise'
            model = re.sub(r'(?!\d+\.*\d* (Carat|Hand)|\w+\d+ (Carat|Hand))(\w+\d+|\d+\.*\d*)', '', model)
            model = re.sub(r'\s+',' ',model).strip()

            transformed_record = {

                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statuscode',''),

                'brand': record.get('brand',''),
                'category': record.get('category',''),

                'title': record.get('title',''),
                'image': record.get('image',''),
                'description': re.sub(r'\r|\n', '', record.get('description', '')),

                'model': model,
                'product_line': record.get('product_collection',''),
                'band_type': record.get('Band',{}).get('Band Material',''),
                'band_size': record.get('Band',{}).get('Band Size (mm)',''),
                'case_size': case_size,
                'type_of_movement': record.get('Movement',{}).get('Movement Type',''),
                'color': color,

                'case_material': record.get('Case & Dial',{}).get('Case Material',''),
                'water_resistance': record.get('The Basics',{}).get('Water Resistance (meters)',''),
                'chronograph_type': record.get('The Basics',{}).get('Chronograph Function',''),

                'price':{
                    'currency_code':'USD',
                    'amount':record.get('selling_price','')
                },
            }
            return transformed_record
        else:
            return None
