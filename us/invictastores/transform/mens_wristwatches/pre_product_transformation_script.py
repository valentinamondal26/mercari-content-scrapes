import hashlib
import re
import itertools
from colour import Color
from titlecase import titlecase

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            case_size = record.get('Case & Dial',{}).get('Case Size','')

            color = []
            color_data = []
            case_tone = record.get('Case & Dial',{}).get('Case Tone','').replace('N/A','')
            dial_color = record.get('Case & Dial',{}).get('Dial Color','').replace('N/A','')
            color_data.append(case_tone.split(', '))
            color_data.append(dial_color.split(', '))
            color_data = list(itertools.chain(*color_data))
            [color.append(x) for x in color_data if x not in color and x] ###this is because when i use set function to remove duplicate the order of color gets changed.they mentioned to form color like case tone+ dial_color
            
            color = '/'.join(color)
            color = re.sub(r'\bN\/A\b', '', color, flags=re.IGNORECASE)
            color=re.sub(re.compile(r'Aqua-Plating|Aqua Plating',re.IGNORECASE),'',color).replace('//','/')

            if 'Stainless Steel' not in color and 'Stainless Steel' in record['title']:
                color = color+'/Stainless Steel' 
                        
            color = re.sub(re.compile(r'StainlessSteel',re.IGNORECASE),'',color)
            color = re.sub(r'//','/',color)
           
            if color:
                if color[0]=='/':
                    color = color.replace('/','',1)

            color = re.sub(re.compile(r'RoseGold',re.IGNORECASE),'Rose Gold',color)
            colors = color.split('/')
            if len(colors)==2:
                if colors[0]== 'Stainless Steel' and colors[1]!='Multicolor':
                    color = colors[1]+'/'+colors[0]     
            color = re.sub(r'\bAntiqueSilver\b', 'Antique Silver', color, flags=re.IGNORECASE)
            color_values = []
            if color:        
                color_values = color.split('/')
                if len(color_values) >= 3:
                    color = color_values[0]+'/'+'Multicolor'

            model = record.get('title','')
            model = re.sub(r'Model (\w*\d+\-*\w+)$|Model (\w*\d+)$|\bModel\b', '', model, flags=re.IGNORECASE)
            case_size_value = re.findall(r'\d+\.*\d*',case_size)[0]
            model = re.sub(r'\b{case_size}\b\s*mm|\s{case_size}\s|\bmm\b'.format(case_size=case_size_value), '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\d+\.*\d*mm',re.IGNORECASE),'',model)
            model = re.sub(r'\(|\)|\s+\-\s+|\-$|^\-',' ',model)
            model = re.sub(r'\bMens\b|\bWatch\b|\bWomens\b|\bMen\'s\b|\bWomen\'s','',model)
            model = re.sub(re.compile(r'\b{}\b'.format(record.get('brand','')),re.IGNORECASE), '',model)
            model = re.sub(r'\b\d{4,6}\b|\b\d\w{5,6}\b','',model)
            model = re.sub(re.compile(r'\bBand\b|\bcase\b|\(|\)',re.IGNORECASE),'',model)
            # model = re.sub(re._compile(r'\bsteel\b|\bwith\b',re.IGNORECASE),'',model)
            model = re.sub(re._compile(r'\bwith\b',re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'ROSE|GOLD',re.IGNORECASE),'',model)
            model = re.sub(r'\bStee\b', 'Steel', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\bStainless\b|\Steel\b',re.IGNORECASE),'',model)
            model = titlecase(model)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x +r'\b', color_values))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x +r'\b', case_tone.split(', ')))), '', model, flags=re.IGNORECASE)
            model = re.sub(r'|'.join(list(map(lambda x: r'\b'+ x +r'\b', dial_color.split(', ')))), '', model, flags=re.IGNORECASE)
            for word in re.findall('\w+', model):
                try:
                    Color(word)
                    model = re.sub(re.compile(r'(?!\b{word}\-\w+\b|\b{word}\w+\b)\b{word}\b'.format(word = word),re.IGNORECASE),'',model)
                except ValueError:
                    pass
            model = re.sub(r',|\+|\bw/\b','',model)
            model = re.sub(r'(\w+\/\w+/*){1,5}',lambda x:x.group().replace('/',' '),model) ## Silicone/Aluminum/Silicone/Aluminum --> Silicone Aluminum Silicone Aluminum
            model = re.sub(r"\/|\'",'',model)
            model = re.sub(r'\bTM(\-)*','',model,flags=re.IGNORECASE)
            model = f'{model} Watch'
            model = re.sub(re.compile(r'\bSS Silicone\b',re.IGNORECASE),'Silicone',model)
            model = re.sub(re.compile(r'\btone\b',re.IGNORECASE),'',model)
            model = re.sub(r'\s+',' ',model).strip()
            model = re.sub(re.compile(r'& Watch',re.IGNORECASE),'Watch',model)
            model = re.sub(re.compile(r'\bAluminum\b',re.IGNORECASE),'Aluminium',model)
            model = re.sub(r'\s+',' ',model).strip()
            model = re.sub('Aluminium Silicone Aluminium Watch','Aluminium Silicone Watch',model,flags=re.IGNORECASE)
            title_case_strings = ['LIMITED EDITION PANTHER', 'LIMITED EDITION PUNISHER', 'LIMITED EDITION SUPERMAN', 'LIMITED EDITION', 'STAR WARS']
            model = re.sub(r'|'.join(list(map(lambda x:r'\b'+x+r'\b',title_case_strings))),lambda x:x.group().title(),model,re.IGNORECASE)
            model=re.sub(r'S\s*\.\s*Coifman',lambda x:x.group().replace('.',''),model,flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\sw\s',re.IGNORECASE),' ',model)
            model = re.sub(r'\bI by\b|\‘|\’', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\bSCUBA\b|\bCOIFMAN\b', lambda x: x.group().title(), model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Dial )(\w+\d+\w+\.*\d*)(.*)', r'\1 \2 \4', model, flags=re.IGNORECASE)
            # model = re.sub(r'(.*)(Dial )(\w+\d+\w+)(.*)', r'\1 \2 \4', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+\-\s+|\-$|^\-', ' ', model)
            model = re.sub(r'\s+',' ',model).strip()
            model = re.sub(r'(\bNoma V\b|\bNoma\b)', lambda x: x.group().upper(), model, flags=re.IGNORECASE)
            if re.findall(r'\bReserve \d+\.\d+ Carat Diamond Automatic Watch\b', model, flags=re.IGNORECASE):
                model = re.sub(r'(\d+\.\d+)0\b', lambda x: re.sub(r'0$', '', x.group()), model)#https://mercari.atlassian.net/browse/USCC-345?focusedCommentId=473392
                                                                                           #We need to drop 0 in decimal part in model if only different between items is “0” then 
                                                                                           # update the model by removing 0 so items match. If there is no match between models we should not 
                                                                                           # drop 0. We don't have privilege in pipeline for comparing models with same raw_output_file. That's why we are doing it specifically for \
                                                                                           # specific model. In future if we are transforming with new raw output \
                                                                                           # make sure to check the mentioned comment for the  new raw output file models.
            model = re.sub(r"\bMens\b|\bMen's\b|\bMen\b", '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            if 'pre-owned' in model.lower():
                return None

            band_type = self.common_transformation(record.get('Band',{}).get('Band Material',''))
            band_size = self.common_transformation(record.get('Band',{}).get('Band Size (mm)',''))
            type_of_movement = self.common_transformation(record.get('Movement',{}).get('Movement Type',''))
            case_material = self.common_transformation(record.get('Case & Dial',{}).get('Case Material',''))
            water_resistance = self.common_transformation(record.get('The Basics',{}).get('Water Resistance (meters)',''))
            chronograph_type = self.common_transformation(record.get('The Basics',{}).get('Chronograph Function',''))
            case_size = self.common_transformation(case_size)

            transformed_record = {

                'id': record.get('id',''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate','').split(',')[0],
                'status_code': record.get('statuscode',''),

                'brand': record.get('brand',''),
                'category': record.get('category',''),

                'title': record.get('title',''),
                'image': record.get('image',''),
                'description': re.sub(r'\r|\n', '', record.get('description', '')),

                'model': model,
                'product_line': record.get('product_collection',''),
                'band_type': band_type,
                'band_size': band_size,
                'case_size': case_size,
                'type_of_movement': type_of_movement,
                'color': titlecase(color),

                'case_material': case_material,
                'water_resistance': water_resistance,
                'chronograph_type': chronograph_type,

                'price':{
                    'currency_code':'USD',
                    'amount':record.get('selling_price','')
                },
            }
            return transformed_record
        else:
            return None
            
    def common_transformation(self, word):
        word = re.sub(r'\bN/A\b|\bNone\b|^\bDate\b$|^\bNA\b$', '', word, flags=re.IGNORECASE)
        word = re.sub(r'^\#$', '', word)
        return word
        