class Mapper:
    def map(self, record):
        key_field = ""

        model = ''
        color = ''
        length = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    color = entity["attribute"][0]["id"]
            elif "length" == entity['name']:
                if entity["attribute"]:
                    length = entity["attribute"][0]["id"]

        key_field = model + color + length
        return key_field
