import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '').replace('$', '')

            material = ''
            fabric_details = ', '.join(record.get('fabric_details', ''))
            if re.findall(r'\d+%', fabric_details):
                fabric_details = re.sub(r'\d+%', '', fabric_details)
                fabric_details = re.sub(r'\s+', ' ', fabric_details).strip()
                fabric_details = list(map(lambda x: x.strip() if 'awesome' != x.lower().strip() else None, fabric_details.split(',')))
                fabric_details = list(filter(None, fabric_details))
                material = ', '.join(fabric_details)

            model = record.get('title', '')

            product_type = record.get('Product Type', '')
            if not product_type:
                meta_product_types = ['Scrub Top', 'Scrub Pants', 'Scrub Jacket']
                match = re.findall(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', meta_product_types))), model, flags=re.IGNORECASE)
                if match:
                    product_type = match[0]


            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawl_date', '').split(', ')[0],
                'status_code': record.get('status_code', ''),

                'category': record.get('category'),
                'brand': record.get('brand', ''),

                'title': record.get('title', ''),
                'image':record.get('image', ''),

                'model': model,
                'color': record.get('Color', ''),
                'product_type': product_type,
                'length': record.get("fit", "").title(),
                'material': material,
                'description': ', '.join(record.get('description', '')),
                'price': {
                    'currencyCode': 'USD',
                    'amount': price
                },
            }

            return transformed_record
        else:
            return None
