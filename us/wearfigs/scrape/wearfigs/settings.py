# -*- coding: utf-8 -*-

# Scrapy settings for wearfigs project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'wearfigs'


SPIDER_MODULES = ['wearfigs.spiders']
NEWSPIDER_MODULE = 'wearfigs.spiders'

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"

DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

CONCURRENT_REQUESTS = 3

DOWNLOAD_DELAY = 5

HTTPCACHE_ENABLED = True
HTTPCACHE_DIR = 'httpcache/{category}/{brand}'
HTTPCACHE_STORAGE = 'common.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {
   'common.pipelines.GCSPipeline': 300,
}

FEED_FORMAT = 'jsonlines'


GCS_BLOB_PREFIX = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'

DOWNLOADER_MIDDLEWARES = {
    'common.selenium_middleware.middlewares.SeleniumMiddleware': 1000,
    'common.httpproxy.HttpProxyMiddleware': 751,
}