'''
https://mercari.atlassian.net/browse/USDE-1613 - Other Health Care Supplies Fig clothing
'''

import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
from common.selenium_middleware.http import SeleniumRequest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


class WearfigsSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from wearfigs.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Other Health Care Supplies

    sub_category : str
        brand to be crawled, Supports
        1) Fig Clothing

    Command e.g:
    scrapy crawl wearfigs -a category='Other Health Care Supplies' -a brand='Fig Clothing'
    """

    name = 'wearfigs'

    category = None
    brand = None

    merge_key = 'id'

    source = 'wearfigs.com'
    blob_name = 'wearfigs.txt'
    base_url = 'https://www.wearfigs.com'

    wait_time = 10
    listin_page_url_wait_until = EC.presence_of_element_located((By.CLASS_NAME, "sc-pZCuu htgBjT FIGS__FilterInputSegment__FilterInputLabel"))

    item_page_url_wait_until = EC.presence_of_element_located((By.CLASS_NAME, "sc-oUMOe cYjonr FIGS__FigsScrubsAreSection__StyledText"))

    proxy_pool = [
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
    ]

    # TODO: NEXT PAGE NAVIGATION IS NOT PRESENT WHILE WRITING THE CRAWLER.
    # IN CASE THEY ADD WE NEED TO ADD NEXT PAGE NAVIGATION

    url_dict ={ ''
        'Other Health Care Supplies':{
            'FIG Clothing':{
                'url': [
                    'https://www.wearfigs.com/pages/shop-collections/all-scrubs-mens',
                    'https://www.wearfigs.com/pages/shop-collections/all-scrubs-womens'
                ],
                'filter': ['Color','Product Type','Fit']
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            return

        super(WearfigsSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category,{}).get(brand,{}).get('url','')
        self.filters = self.url_dict.get(category,{}).get(brand,{}).get('filter','')

        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def start_requests(self):
        if self.launch_url:
            urls = []
            if isinstance(self.launch_url, list):
                urls = self.launch_url
            elif isinstance(self.launch_url, str):
                urls = [self.launch_url]

            for url in urls:
                yield self.createDynamicRequest(
                    url=url,
                    callback=self.parse_listing_page,
                    wait_time=self.wait_time,
                    wait_until=self.listin_page_url_wait_until
                )


    def parse_listing_page(self, response):
        meta = response.meta

        item_urls = response.xpath("//div[@class='sc-ptfmh kWhIYz FIGS__ProductGroupTile__DetailsContents']/a/@href").getall()
        for url in item_urls:
            if self.base_url not in url:
                url = self.base_url+url
            yield self.createDynamicRequest(
                url=url,
                callback=self.crawl_details,
                wait_time=self.wait_time,
                wait_until=self.item_page_url_wait_until,
                meta=meta)

        if not response.meta.get('filter_name',''):
            for filter_applied in self.filters:
                if filter_applied == 'Color':
                    filter_names = response.xpath("//div[@class='sc-oTaid dhnpNo FIGS__FilterInputSegment__FilterInputDetails']/div/section/label/input[contains(@data-heap-label,'color')]/@value").getall()
                else:
                    filters = filter_applied.lower().split()
                    for fil in filters:
                        filter_names = response.xpath("//div[@class='sc-oTaid dhnpNo FIGS__FilterInputSegment__FilterInputDetails']/div/label/input[contains(@data-heap-label,'{}')]/@value".format(fil)).getall()
                        if filter_names:
                            break

                for filter_name in filter_names:
                    filter_url = response.url \
                        + "?{filter_applied}={filter_name}".format(
                            filter_applied='category' if filter_applied == 'Product Type' else filter_applied.lower(),
                            filter_name=filter_name
                        )
                    meta = {}
                    meta.update({"filter_name":filter_applied})
                    meta.update({meta['filter_name']:filter_name})
                    yield self.createDynamicRequest(
                        url=filter_url,
                        callback=self.parse_listing_page,
                        wait_time=self.wait_time,
                        wait_until=self.listin_page_url_wait_until,
                        meta=meta
                    )


    def crawl_details(self, response):
        meta = response.meta
        item = {
            'id': response.url,
            'category': self.category,
            'brand': self.brand,
            'status_code': str(response.status),
            'crawl_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'title':  response.xpath("//h2[@data-heap-label='title']/text()").get(default='') \
                + " " + response.xpath("//p[@data-heap-label='description']/text()").get(default=''),
            'price': response.xpath("//span[@data-heap-label='price']/text()").get(default=''),
            'fabric_details':response.xpath("//div[@data-test-id='fabric-and-care']/ul/li/text()").getall(),
            'image': response.xpath("//div[@class='sc-pdKru cDmkle FIGS__ProgressiveAsset__Container']/img/@src").get(default=''),
            'description': response.xpath("//div[@data-test-id='product-details']/ul/li/text()").getall(),
            'why_we_love_text': response.xpath("//h6[@data-test-id='why-we-love-text']/text()").get(),
            'additional_info': response.xpath("//p[@class='sc-fzoOEf klFMkA sc-qQlyE cvPUOg FIGS__WhyWeLove__IconTitle']/text()").getall(),
            'color_classification': response.xpath("//p[@data-heap-label='color classification']/text()").get(),
            'listing_page_url': meta.get('listing_page_url',''),
            'Fit': response.xpath("//button[contains(@class, 'FIGS__FitSelector__StyledFit')]/text()").getall()
        }

        filter_name = meta.get('filter_name', '')
        if  filter_name and filter_name != 'Fit': ###we extract fit filter data in the item landing page
            meta_data =  dict.fromkeys([filter_name], meta[filter_name])
            item.update(meta_data)
            yield item


    def createDynamicRequest(self, url, callback,wait_time=None, wait_until=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, wait_time=wait_time, wait_until=wait_until, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request




