import hashlib
import re

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get("title","")
            try:
                color = record.get("color","")[0].strip()
                color = color.title()
                color = re.sub(r'\s*(,|&)\s*', '/', color)
                color = re.sub(r'\s+', ' ', color).strip()
            except IndexError:
                color = ""

            model_sku = record.get("model_sku","")
            model_sku = model_sku.replace(u'\xa0', u' ').replace("Ref.","").strip()

            price =  record.get("price","").replace("$","").strip()

            heel_height = record.get("heel_inch", "").strip()#.replace("mm","").strip()

            material = record.get("material","")
            material = re.sub(r'\s*(,|&)\s*', '/', material)
            materials = [m if len(re.findall(r'\b'+m+r'\b', material, re.IGNORECASE)) == 1 else None for m in material.split('/')]
            materials = list(filter(None, materials))
            material = '/'.join(materials)
            material = re.sub(r'\s+', ' ', material).strip()

            keywords = [
                'Sandals', 'Slingbacks', 'Mules', 'Mary Janes',
            ]
            if not re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', keywords))), re.IGNORECASE), title):
                return None

            transformed_record = {
                "id": record.get("id", ""),
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', "").split(",")[0],
                "status_code":record.get("status_code", ""),

                "category": record.get('category', ""),
                "brand": record.get("brand", ""),

                "title": title,
                "image": record.get("image", ""),
                "description": record.get("description", ""),

                "model": title,
                "color": color,
                "heel_height": heel_height,
                "material": material,
                "model_sku": model_sku,
                "price": {
                    "currency_code": "USD",
                    "amount": price
                },
            }
            return transformed_record
