class Mapper:
    def map(self, record):

        material = ''
        color = ''

        entities = record['entities']
        for entity in entities:
            if "material" == entity['name']:
                material = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                color = entity["attribute"][0]["id"]

        key_field = material + color
        return key_field
