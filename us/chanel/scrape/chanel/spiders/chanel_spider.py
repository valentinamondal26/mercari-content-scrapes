'''
https://mercari.atlassian.net/browse/USDE-1215
'''

import scrapy
import json
from datetime import datetime
import re
from scrapy.exceptions import CloseSpider
import os
import itertools



class ChanelSpider(scrapy.Spider):
    
    """
    spider to crawl items in a provided category and sub-category from chanel.com

    Attributes

    category : str
        category to be crawled, Supports
        1) Sandals 
        

    brand : str
    brand to be crawled, Supports
        1) Chanel

    Command e.g:
    scrapy crawl chanel  -a category="Sandals" -a brand='Chanel' 

    """
    name = "chanel"

    category = None
    brand = None

    merge_key = 'id'

    source = 'chanel.com'
    blob_name = 'chanel.txt'
    spider_project_name = "chanel"
    gcs_cache_file_name = 'chanel.tar.gz'

    filters = None
    launch_url = None

    base_url = 'https://www.chanel.com'

    url_dict = {
        "Sandals": {
            "Chanel": {
                "url": "https://www.chanel.com/us/fashion/shoes/c/1x1x5/?gclid=EAIaIQobChMIhaOBhtCT6QIV1BatBh1fPgc9EAAYASAAEgLKDfD_BwE&q=tridiv_subcategory_united_states:1x1x5x4",
                "filter": [],
            }
        }
    }

    proxy_pool = [
      'http://shp-mercari-us-d00001.tp-ns.com:80',
      'http://shp-mercari-us-d00002.tp-ns.com:80',
      'http://shp-mercari-us-d00003.tp-ns.com:80',
      'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(ChanelSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.round_robin = itertools.cycle(self.proxy_pool)
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filter', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))

    def contracts_mock_function(self):
        self.round_robin = itertools.cycle(self.proxy_pool) 
        self.launch_url = "https://www.chanel.com/us/fashion/shoes/c/1x1x5/?gclid=EAIaIQobChMIhaOBhtCT6QIV1BatBh1fPgc9EAAYASAAEgLKDfD_BwE&q=tridiv_subcategory_united_states:1x1x5x4"

    def start_requests(self):
        if self.launch_url:
            page = 1
            self.launch_url = re.sub(r'&q=(.*)',"",self.launch_url)  
            url = self.launch_url + "&requestType=ajax&page="+str(page)+"&totalElementsCount=24"
            meta ={"page":1}
            request = self.createRequest(url = url,callback = self.parse,meta=meta)
            yield request


    def parse(self, response):
        """
        @url https://www.chanel.com/us/fashion/shoes/c/1x1x5/?gclid=EAIaIQobChMIhaOBhtCT6QIV1BatBh1fPgc9EAAYASAAEgLKDfD_BwE&q=tridiv_subcategory_united_states:1x1x5x4&requestType=ajax&page=1&totalElementsCount=24
        @meta {"page":1}
        @returns requests 1
        """

        meta = response.meta
        response_json = json.loads(response.body)
        product_ids = list(response_json.get('dataLayer',{}).get('productList',{}).keys())
        products_data = response_json.get('dataLayer',{}).get('productList',{})
        for product in product_ids:
            product_page = products_data.get(product,{}).get('quickviewPopin',{}).get('page',"")
            if self.base_url not in product_page:
                product_page = self.base_url + product_page
                product_page = product_page.replace("quickview","").strip()
                product_page = product_page.replace(product.lower(),product.upper())
            meta.update({"use_cache":True})
            request = self.createRequest(product_page,self.crawl_details,meta=meta)
            yield request
        if response_json.get("next",""):
            meta['page']+=1
            next_page_url = self.launch_url + "&requestType=ajax&page="+str(meta["page"])+"&totalElementsCount=24"
            request = self.createRequest(url=next_page_url, callback=self.parse, meta=meta)
            yield request


    def crawl_details(self, response):
        """
        @url https://www.chanel.com/us/fashion/p/G36361Y54258K2304/mary-janes-laminated-lambskin-grosgrain/
        @scrape_values title material color heel_inch heel_mm price model_sku image
        """

        title = response.xpath("//span[@class='heading product-details__title ']/text()").get(default='')
        material = response.xpath("//span[@class='product-details__description']/text()").get(default='').strip()
        color = response.xpath("//span[@class='product-details__description product-details__color']/text()").getall() 
        heel_inch = response.xpath("//span[@data-test='lblHealHeight_MM']/text()").get(default='').strip()
        heel_mm = response.xpath("//span[@data-test='lblHealHeight_IN']/text()").get(default='').strip()
        price = response.xpath("//p[@class='product-details__price']/text()").get(default='').strip()
        model_sku = response.xpath("//p[@class='product-details__reference']/text()").get(default='').strip()
        image = response.xpath("//div[@class='carousel__slide-container']//img/@src").get(default='').strip()  

        data = {
            "title": title,
            "material": material,
            "color": color,
            "heel_inch": heel_inch,
            "heel_mm": heel_mm,
            "id": response.url,
            "status_code": response.status,
            "price": price,
            "model_sku": model_sku,
            "image": image,
            "category": self.category,
            "brand": self.brand,
            "crawl_date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        yield data


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = next(self.round_robin)

        request.meta.update({"use_cache":True})
        return request