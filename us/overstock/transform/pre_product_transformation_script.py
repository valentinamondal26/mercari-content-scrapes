import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price=record.get("price","")
            match=re.findall(r"[\d\.]+",price)
            if len(match)>0:
                price=match[0]
            price=price.replace(',','')
            metal=record.get("Metal","")
            karat=record.get("Gold Karat","")
            if karat!='':
                metal=karat+" "+metal


            transformed_record = {
                "id": record.get('id',''),
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": "Rings",
                "description": description,
                "title": record.get('title',''),
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query,
                "price":{
                    "currency_code": "USD",
                    "amount": price
                },
                "metal":metal,
                "stone":record.get("Stone",""),
                "metal_color":record.get("Metal Color",""),
                "carat_total_weight":record.get("Total Diamond Weight",""),
                "stone_shape":record.get("Stone Shape",""),
                "clarity_grade":record.get("Clarity",""),
                "stone_color":record.get("Stone Color","")


                }

            return transformed_record
        else:
            return None
