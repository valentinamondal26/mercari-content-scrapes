import json
import jsonschema
from jsonschema import validate


class SchemaValidator:
    record = None

    def __init__(self, record):
        self.record = record
        return

    def process(self, schema):
        schema_filter = Filter(schema)
        schema_valid = schema_filter.filter_passed(self.record)
        return schema_valid


class Filter:
    schema = None

    def __init__(self, schema):
        self.schema = schema
        return

    def filter_passed(self, record):
        return self.filter(record)

    def filter(self, record):
        try:
            validate(record, self.schema)
        except jsonschema.ValidationError as e:
            return False
        except json.decoder.JSONDecodeError as e:
            return False
        return True
