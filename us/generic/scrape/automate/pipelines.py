from scrapy.exporters import JsonLinesItemExporter
from scrapy.crawler import CrawlerProcess
from scrapy.crawler import CrawlerProcess
from scrapy.exceptions import CloseSpider


from io import BytesIO
from io import StringIO

import os
import json
from .schema_validator import SchemaValidator
from .report_writer import ReportWriter
from datetime import datetime
import datetime
from google.cloud import storage
import pandas as pd


class GCSPipeline(object):

    def __init__(self, gcs_blob_prefix, report_gcs_blob_prefix, gcs_blob_timestamp, stats):

        self.gcs_blob_prefix = gcs_blob_prefix
        self.gcs_blob_timestamp = gcs_blob_timestamp
        self.report_gcs_blob_prefix = report_gcs_blob_prefix
        self.bucket_name = 'content_us'
        self.client = None
        self.schema = None
        self.bucket = None
        self.items = []
        self.stats = stats
        self.validate = []
        self.report = None

    @classmethod
    def from_crawler(cls, crawler):

        return cls(
            gcs_blob_prefix=crawler.settings.get('GCS_BLOB_PREFIX'),
            gcs_blob_timestamp=crawler.settings.get('GCS_BLOB_TIMESTAMP'),
            report_gcs_blob_prefix=crawler.settings.get(
                'REPORT_GCS_BLOB_PREFIX'),
            stats=crawler.stats
        )

    def genereate_report_dict(self, spider, passed_count, failed_count):

        report = ReportWriter()
        dict = {}
        # header
        dict['source'] = spider.source
        dict['category'] = spider.category.lower()
        dict['brand'] = spider.brand.lower()
        dict['start_time'] = self.gcs_blob_timestamp
        dict['spider'] = spider.name
        report.add_data('header', dict=dict)
        dict = {}
        # schema
        dict['schema_passed'] = passed_count
        dict['schema_failed'] = failed_count
        dict['item_scraped_count'] = self.stats.get_value('item_scraped_count')
        dict['failed_percentage'] = failed_count/dict['item_scraped_count']*100
        if dict['failed_percentage'] >= 50:
            dict['threshold_status'] = False
        else:
            dict['threshold_status'] = True
        report.add_data('schema', dict=dict)
        # footer
        dict = {}
        dict['end_time'] = str(
            datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))
        report.add_data('footer', dict=dict)
        return report.return_report()

    def open_spider(self, spider):

        self.client = storage.Client()
        self.bucket = self.client.get_bucket(self.bucket_name)

    def get_report_file(self, spider):

        f_passed = self._make_fileobj(True)
        f_failed = self._make_fileobj(False)
        f_passed_lines = len(f_passed.readlines())
        f_failed_lines = len(f_failed.readlines())
        # if f_passed_lines / (f_passed_lines+f_failed_lines) < 0.5:
        spider.crawl_status = False
        spider.logger.error('\n\n\nThresheold doesnt fit\n\n\n')
        report = self.genereate_report_dict(
            spider, f_passed_lines, f_failed_lines)
        return report

    def close_spider(self, spider):

        category = spider.category
        brand = spider.brand
        if spider.name == "automate_dynamic":
            spider.driver.quit()

        if not category or not brand:
            spider.logger.error(
                'category or brand is empty, so skip uploading result to gcs')
            return

        source = spider.source
        timestamp = self.gcs_blob_timestamp
        filename = spider.blob_name
        csv_filename = spider.blob_name.split(".")[0]+".csv"

        self.report = self.get_report_file(spider)

        self.upload(schema_valid="schema_passed", schema_needed=True, category=category, brand=brand, source=source,
                    timestamp=timestamp, filename=filename, csv_filename=csv_filename, spider=spider)
        self.upload(schema_valid="schema_failed", schema_needed=False, category=category, brand=brand,
                    source=source, timestamp=timestamp, csv_filename=csv_filename, spider=spider)

        report_destination_blob_name = self.report_gcs_blob_prefix.format(
            category=category, brand=brand, source=source, timestamp=timestamp, filename="report.txt", format="report")

        report_destination_blob_name = report_destination_blob_name.replace(
            " ", "_").lower()
        jsonData = json.dumps(self.report)
        binaryData = jsonData.encode()
        f = BytesIO(binaryData)
        report_blob = self.bucket.blob(report_destination_blob_name)
        f.seek(0)
        report_blob.upload_from_file(f)

        spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
            bucket=self.bucket_name, blob=report_destination_blob_name))

    def upload(self, schema_valid=None, schema_needed=None, category=None, brand=None, source=None, timestamp=None, filename=None, csv_filename=None, spider=None):

        destination_blob_name = self.gcs_blob_prefix.format(category=category,
                                                            brand=brand, source=source, timestamp=timestamp, schema_valid=schema_valid, filename=filename, format="json")
        destination_blob_name = destination_blob_name.replace(" ", "_").lower()

        csv_destination_blob_name = self.gcs_blob_prefix.format(category=category,
                                                                brand=brand, source=source, timestamp=timestamp, schema_valid=schema_valid, filename=csv_filename, format="csv")
        csv_destination_blob_name = csv_destination_blob_name.replace(
            " ", "_").lower()

        blob = self.bucket.blob(destination_blob_name)
        csv_blob = self.bucket.blob(csv_destination_blob_name)
        f = self._make_fileobj(schema_needed)

        blob.upload_from_file(f)
        spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
            bucket=self.bucket_name, blob=destination_blob_name))
        f.seek(0)
        df = pd.read_json(f, lines=True)
        csv_str = df.to_csv(index=False)
        output = BytesIO()
        output.write(csv_str.encode("ascii", "ignore"))
        output.seek(0)

        csv_blob.upload_from_file(output)

        spider.logger.info('File uploaded to gs://{bucket}/{blob}'.format(
            bucket=self.bucket_name, blob=csv_destination_blob_name))

    def process_item(self, item, spider):
        """
         validate each item with the scheam and stores the vaildator result in valid list 
        """

        schema_validator = SchemaValidator(item)

        client = storage.Client()
        self.schema = spider.schema
        valid = schema_validator.process(self.schema)
        self.validate.append(valid)
        self.items.append(item)

        return item

    def _make_fileobj(self, schema_needed):
        """
        Build file object from items with respect to the schema_needed [ True or False i.e Passed or Failed]
        """

        bio = BytesIO()
        f = bio

        # Build file object using ItemExporter
        exporter = JsonLinesItemExporter(f)
        exporter.start_exporting()
        for item in self.items:
            position = self.items.index(item)
            if self.validate[position] == schema_needed:
                exporter.export_item(item)
        exporter.finish_exporting()

        # Seek to the top of file to be read later
        bio.seek(0)

        return bio
