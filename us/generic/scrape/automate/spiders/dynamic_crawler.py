# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import CloseSpider
from scrapy.crawler import CrawlerProcess

import json
from collections import OrderedDict
import time
import re
import hashlib
from google.cloud import storage
import difflib
import datetime
import os
from genson import SchemaBuilder
import random

from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class CrawlAutomateSelenium(scrapy.Spider):
    """
    generic spider to crawl items in a provided category and brand from the given source

    Attributes
    ----------

    brand : str
        brand to be crawled

    url : str
        url to be crawled

    listing : str[path]
        json file with the listing page xpaths

    details : str[path]
        json file with the details page xpaths


    Command e.g:
    scrapy crawl dynamic  -a brand="All"  -a xpath_json="<path to xpath.json"
    """

    name = 'dynamic'
    brand = None
    xpath_json = None
    blob_name = None
    source = None
    base_url = None
    category = None
    driver = None
    builder = None
    schema = None
    xpaths = None

    # proxy_pool = [
    #     'http://shp-mercari-us-d00001.tp-ns.com:80',
    #     'http://shp-mercari-us-d00002.tp-ns.com:80',
    #     'http://shp-mercari-us-d00003.tp-ns.com:80',
    #     'http://shp-mercari-us-d00005.tp-ns.com:80',
    # ]

    def __init__(self, brand=None, xpath_json=None, *args, **kwargs):

        super(CrawlAutomateSelenium, self).__init__(*args, **kwargs)
        if not brand:
            self.logger.error("Brand should not be None")
            raise CloseSpider("Brand not found")
        if not xpath_json:
            self.logger.error("Enter xpath json")
            raise CloseSpider("Enter xpath json")

        self.brand = brand
        self.xpath_json = xpath_json
        self.extract_xpath_json()
        if not self.launch_url:
            self.logger.error(
                "Spider is not supported for brand:{}".format(brand))
            raise(CloseSpider("Spider is not supported for brand:{}".format(brand)))
        self.logger.info("Crawling for brand "+brand +
                         ",url:" + self.launch_url)

    def extract_xpath_json(self):
        client = storage.Client()
        bucket = client.get_bucket('content_us')
        blob_path = self.xpath_json.split("content_us/")[1]
        blob = bucket.get_blob(blob_path)
        downloaded_blob = blob.download_as_string().decode("utf-8")
        js = json.loads(downloaded_blob)

        self.launch_url = js.get('crawlSource', '')

        # get base url from launch url eg: baseurl="https://shop.rebag.com" from "https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true"
        pattern = r"https://[a-zA-Z\.\-]+"
        matches = re.findall(pattern, self.launch_url)
        if len(matches) > 0:
            self.base_url = matches[0]

        # get source from baseurl eg: source="rebag.com" from "https://shop.rebag.com"
        pattern = r"[a-zA-Z]+\.{1}[a-zA-Z]{2,3}"
        occurences = [i for i, letter in enumerate(
            self.base_url) if letter == '.']
        occurences = occurences[-2:]
        self.source = self.base_url[occurences[0]+1:occurences[1]]+".com"

        self.blob_name = self.source.split(".com")[0]+".txt"
        self.category = js.get('category', '')
        self.xpaths = js.get('xpaths', [])

    def createRequest(self, url, callback):

        request = scrapy.Request(url=url, callback=callback)
        # if self.proxy_pool:
        #     request.meta['proxy'] = random.choice(self.proxy_pool)
        return request

    def start_requests(self):

        if self.launch_url:
            # start selenium driver in headless mode
            options = Options()
            options.headless = True
            options.add_argument(
                "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")
            self.driver = webdriver.Chrome(chrome_options=options)

            yield self.createRequest(url=self.launch_url, callback=self.parse)

    def extract_element(self, product_url_xpath):

        product_url_xpath = ''.join(product_url_xpath.split())
        wait = WebDriverWait(self.driver, 20)
        element = wait.until(EC.visibility_of_element_located(
            (By.XPATH, product_url_xpath)))
        element = self.driver.find_elements_by_xpath(product_url_xpath)
        all_links = [elem.get_attribute('href') for elem in element]
        return all_links

    def parse(self, response):

        self.driver.get(response.url)

        for path in self.xpaths:
            if path.get('entity', '') == "id":
                product_url_xpath = path.get('xpath', '')
                break

        if product_url_xpath.find("body") == 0:
            product_url_xpath = "//"+product_url_xpath
        # pattern to search for anchor tag in the parent div
        pattern = r"\/a[\[\d+}\]]*\/"
        match = re.findall(pattern, product_url_xpath)
        # extract the path for anchor tag from the whole xpath string
        if len(match) > 0:
            pos = product_url_xpath.find(match[0])+len(match[0])
            product_url_xpath = product_url_xpath[0:pos]
            product_url_xpath = product_url_xpath[:-1]
        product_url_xpath_copy = product_url_xpath

        # pattern to match similar patterns of anchor tags [ i.e to remove specific paths - div[1]->div ]
        pattern = r"\[\d{1}\]"
        matches = re.findall(pattern, product_url_xpath)
        matches_minus = matches[:-1]
        for match in matches_minus:
            product_url_xpath = product_url_xpath.replace(match, '', 1)

        try:
            all_links = self.extract_element(product_url_xpath)
        except:
            all_links = []

        if len(all_links) <= 1:
            for match in matches:
                product_url_xpath = product_url_xpath.replace(match, '', 1)
            product_url_xpath = product_url_xpath[:-1] + \
                "/"+product_url_xpath[-1:]
        try:
            all_links = self.extract_element(product_url_xpath)
        except:
            all_links = []

        if all_links == []:
            if "@id" in product_url_xpath_copy:
                pattern = r"[a-zA-Z]+\[\d{1}\]"
                matches = re.findall(pattern, product_url_xpath_copy)
                for match in matches:
                    if "a[" not in match:
                        product_url_xpath_copy = product_url_xpath_copy.replace(
                            match, '')
                try:
                    all_links = self.extract_element(product_url_xpath_copy)
                except:
                    all_links = []

        all_link = []
        all_link = [self.base_url +
                    link for link in all_links if "https" not in link]

        if all_link:
            for link in all_link:
                yield self.createRequest(url=link, callback=self.parse_product)
        else:
            for link in all_links:
                yield self.createRequest(url=link, callback=self.parse_product)

    def get_position(self, value_xpath, ids):

        pos = value_xpath.find('/', 2)
        xpath_string = value_xpath[2:pos+1]
        if xpath_string != '':
            xpath_string = xpath_string.split("*[@id='")[1].split("']/")[0]
        match = []
        position = []
        match = difflib.get_close_matches(xpath_string, ids)
        for mat in match:
            position.append(ids.index(mat))
        return position

    def update_xpath(self, position, ids, value_xpath, details, key, response):

        for pos in position:
            id = ids[pos]
            quote1 = value_xpath.find("'")
            quote2 = value_xpath.find("']")
            replace_text = value_xpath[quote1+1:quote2]
            value_xpath = value_xpath.replace(replace_text, id)
            check_value = ''.join(response.xpath(value_xpath).getall()).strip()
            if check_value == '':
                continue
            else:
                break
        return value_xpath

    def check_id(self, val, ids, details, key, response):

        if "@id" in val:
            position = self.get_position(val, ids)
            value_xpath = self.update_xpath(
                position, ids, val, details, key, response)
        else:
            value_xpath = "//"+val

        return value_xpath

    def parse_product(self, response):
        self.driver.get(response.url)
        try:
            os.mkdir("/html/{category}/{brand}".format(category=self.category,brand=self.brand))
        except:
            pass

        b = response.url.encode('utf-8')
        hash_object = hashlib.sha1(b)
        file = hash_object.hexdigest()+".html"
        open("/html/{category}/{brand}/{filename}".format(category=self.category,brand=self.brand,filename=file),"w").write(self.driver.page_source)
        
        item = OrderedDict()
        default_keys = ['crawl_date', 'status_code', 'id', 'brand', 'category']
        item['crawl_date'] = str(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item['status_code'] = str(response.status)
        item['id'] = response.url
        item['brand'] = self.brand
        item['category'] = self.category
        keys = []
        values = []
        required = []

        for path in self.xpaths:
            if path.get('entity', '') != "id":
                keys.append(path.get('entity', ''))
                # appending added entities with default entities
                default_keys.append(path.get('entity', ''))
                values.append(path.get('xpath', ''))
                required.append(path.get('required'))

        if self.builder is None:
            self.builder = SchemaBuilder()
            schema_json = OrderedDict()
            schema_json = schema_json.fromkeys(default_keys, '')
            self.builder.add_object(schema_json)
            self.schema = self.builder.to_schema()
            # removing required because required filed checks for the order of the entites along with empty check.
            del self.schema['required']

        # generate schema with minlength property for empty checking
        prop = self.schema.get('properties', {})
        for key, value in prop.items():
            update = {"minLength": 1}
            try:
                position = keys.index(key)
                reqiure = required[position]
                if reqiure == True:
                    value.update(update)
            except:
                pass
        details = OrderedDict()
        details = details.fromkeys(keys, '')
        ids = []
        ele = self.driver.find_elements_by_xpath('//*[@id]')
        ids = [elem.get_attribute('id') for elem in ele]

        wait = WebDriverWait(self.driver, 20)

        for i in range(len(keys)):
            if details[keys[i]] == '':
                value_xpath = self.check_id(
                    values[i], ids, details, keys[i], response)
                element = wait.until(
                    EC.visibility_of_element_located((By.XPATH, value_xpath)))
                if keys[i] == "image":
                    details[keys[i]] = ' '.join(element.get_attribute('src').split()).strip(
                    ).replace("\n", ' ').replace("\t", ' ').replace("\r", " ")
                elif keys[i] == "breadcrumb":
                    details[keys[i]] = ' '.join(element.text.split()).strip().replace(
                        "\n", ' ').replace("\t", ' ').replace("\r", " ")
                    pattern = r"[a-zA-Z]+"
                    details[keys[i]] = '|'.join(re.findall(pattern, details[keys[i]])).replace(
                        "\n", ' ').replace("\t", ' ').replace("\r", " ")
                else:
                    details[keys[i]] = ''.join(element.text.split()).strip().replace(
                        "\n", ' ').replace("\t", ' ').replace("\r", " ")

        item.update(details)
        yield item
