# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import CloseSpider

import json
from collections import OrderedDict
import re
from google.cloud import storage
import difflib
import datetime
from genson import SchemaBuilder


class CrawlAutomate(scrapy.Spider):
    """
    generic spider to crawl items in a provided category and brand from the given source

    Attributes
    ----------

    brand : str
        brand to be crawled

    url : str
        url to be crawled

    listing : str[path]
        json file with the listing page xpaths

    details : str[path]
        json file with the details page xpaths


    Command e.g:
    scrapy crawl static  -a brand="All" -a url="https://shop.rebag.com/collections/all-bags?_=pf&pf_st_availability_hidden=true&pf_t_silhouette=bc-filter-Shoulder%20Bags" -a listing="<path to listing.json" -a details="path to details.json"

    """

    name = 'static'
    category = None
    brand = None
    xpath_json = None
    blob_name = None
    source = None
    base_url = None
    category = None
    xpaths=None
    builder = None
    schema = None

    # proxy_pool = [
    #     'http://shp-mercari-us-d00001.tp-ns.com:80',
    #     'http://shp-mercari-us-d00002.tp-ns.com:80',
    #     'http://shp-mercari-us-d00003.tp-ns.com:80',
    #     'http://shp-mercari-us-d00005.tp-ns.com:80',
    # ]

    def __init__(self, brand=None, xpath_json=None, *args, **kwargs):

        super(CrawlAutomate, self).__init__(*args, **kwargs)

        if not brand:
            self.logger.error("Brand should not be None")
            raise CloseSpider("Brand not found")
        if not xpath_json :
            self.logger.error("Enter xpath json")
            raise CloseSpider("Enter xpath json")

        self.brand = brand
        self.xpath_json = xpath_json
        self.extract_xpath_json()
        if not self.launch_url:
            self.logger.error(
                "Spider is not supported for brand:{}".format(brand))
            raise(CloseSpider("Spider is not supported for brand:{}".format(brand)))
        self.logger.info("Crawling for brand "+brand +
                         ",url:" + self.launch_url)


    def extract_xpath_json(self):
        client = storage.Client()
        bucket = client.get_bucket('content_us')
        blob_path = self.xpath_json.split("content_us/")[1]
        blob = bucket.get_blob(blob_path)
        downloaded_blob = blob.download_as_string().decode("utf-8")
        js = json.loads(downloaded_blob)

        self.launch_url=js.get('crawlSource','')

        pattern = r"https://[a-zA-Z\.\-]+"
        matches = re.findall(pattern, self.launch_url)
        if len(matches) > 0:
            self.base_url = matches[0]

        pattern = r"[a-zA-Z]+\.{1}[a-zA-Z]{2,3}"
        occurences = self.findOccurrences(self.base_url, '.')
        occurences = occurences[-2:]
        self.source = self.base_url[occurences[0]+1:occurences[1]]+".com"

        self.blob_name = self.source.split(".com")[0]+".txt"
        self.category=js.get('category','')
        self.xpaths = js.get('xpaths', [])


    def findOccurrences(self, s, ch):

        return [i for i, letter in enumerate(s) if letter == ch]


    def start_requests(self):

        if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse)


    def parse(self, response):

        for path in self.xpaths:
            if path.get('entity', '') == "id":
                product_url_xpath = path.get('xpath', '')
                break

        if product_url_xpath.find("body") == 0:
            product_url_xpath = "//"+product_url_xpath
        # pattern to search for anchor tag in the parent div
        pattern = r"\/a[\[\d+}\]]*\/"
        match = re.findall(pattern, product_url_xpath)
        # extract the path for anchor tag from the whole xpath string
        if len(match) > 0:
            pos = product_url_xpath.find(match[0])+len(match[0])
            product_url_xpath = product_url_xpath[0:pos]
            product_url_xpath = product_url_xpath[:-1]

        product_url_xpath_copy = product_url_xpath

        # pattern to match similar patterns of anchor tags [ i.e to remove specific paths - div[1]->div ]
        pattern = r"\[\d{1}\]"
        matches = re.findall(pattern, product_url_xpath)
        matches_minus = matches[:-1]
        for match in matches_minus:
            product_url_xpath = product_url_xpath.replace(match, '', 1)
        product_url_xpath += "/@href"

        all_links = response.xpath(product_url_xpath).getall()
        if len(all_links) <= 1:
            for match in matches:
                product_url_xpath = product_url_xpath.replace(match, '', 1)
            product_url_xpath = product_url_xpath[:-1] + \
                "/"+product_url_xpath[-1:]+"/@href"

        all_links = response.xpath(product_url_xpath).getall()
        if all_links == [] or len(all_links) <= 1:
            matches = re.findall(pattern, product_url_xpath)
            for match in matches:
                product_url_xpath = product_url_xpath.replace(match, '', 1)
            product_url_xpath += "/@href"
            all_links = response.xpath(product_url_xpath).getall()

        # if no similar other patterns available rephrase the pattern with id attribute
        if len(all_links) <= 1:
            if "@id" in product_url_xpath_copy:
                pattern = r"[a-zA-Z]+\[\d{1}\]"
                matches = re.findall(pattern, product_url_xpath_copy)
                for match in matches:
                    if "a[" not in match:
                        product_url_xpath_copy = product_url_xpath_copy.replace(
                            match, '')
                product_url_xpath_copy += "/@href"
                all_links = response.xpath(product_url_xpath_copy).getall()

        all_link = []
        all_link = [self.base_url +
                    link for link in all_links if "https" not in link]
        if all_link:
            for link in all_link:
                yield self.createRequest(url=link, callback=self.parse_product)
        else:
            for link in all_links:
                yield self.createRequest(url=link, callback=self.parse_product)

    def get_value(self, value_xpath, key, details):

        xpath = ''
        if key == "image":
            if "/@src" not in xpath:
                xpath = value_xpath+"/@src"
            return xpath
        if key == "breadcrumb":
            if "//li//text()" not in xpath:
                xpath = value_xpath+"//li//text()"
            return xpath
        else:
            if "//text()" not in xpath:
                xpath = value_xpath+"//text()"
            return xpath

    def get_position(self, value_xpath, ids):

        pos = value_xpath.find('/', 2)
        xpath_string = value_xpath[2:pos+1]
        if xpath_string != '':
            xpath_string = xpath_string.split("*[@id='")[1].split("']/")[0]
        match = []
        position = []
        match = difflib.get_close_matches(xpath_string, ids)
        for mat in match:
            position.append(ids.index(mat))
        return position

    def update_xpath(self, position, ids, value_xpath, details, key, response):

        for pos in position:
            id = ids[pos]
            quote1 = value_xpath.find("'")
            quote2 = value_xpath.find("']")
            replace_text = value_xpath[quote1+1:quote2]
            value_xpath = value_xpath.replace(replace_text, id)
            check_path = self.get_value(value_xpath, key, details)
            check_value = ''.join(response.xpath(check_path).getall()).strip()
            if check_value == '':
                continue
            else:
                break
        return value_xpath

    def check_id(self, val, ids, details, key, response):

        if "@id" in val:
            position = self.get_position(val, ids)
            value_xpath = self.update_xpath(
                position, ids, val, details, key, response)
            value_xpath = self.get_value(value_xpath, key, details)
        else:
            value_xpath = "//"+val
            value_xpath = self.get_value(value_xpath, key, details)

        return value_xpath

    def parse_product(self, response):

        item = {}
        default_keys = ['crawl_date', 'id', 'status_code', 'brand', 'category']
        item['crawl_date'] = str(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item['status_code'] = str(response.status)
        item['id'] = response.url
        item['brand'] = self.brand
        item['category'] = self.category

        keys = []
        values = []
        required = []

        for path in self.xpaths:
            if path.get('entity','')!='id':
                keys.append(path.get('entity', ''))
                #appending added entities with default entities
                default_keys.append(path.get('entity', ''))
                values.append(path.get('xpath', ''))
                required.append(path.get('required'))

        if self.builder is None:
            self.builder = SchemaBuilder()
            schema_json = {}
            schema_json = schema_json.fromkeys(default_keys, '')
            self.builder.add_object(schema_json)
            self.schema = self.builder.to_schema()
            # removing required because required filed checks for the order of the entites along with empty check.
            del self.schema['required']

        #generate schema with minlength property for empty
        prop = self.schema.get('properties', {})
        for key, value in prop.items():
            update = {"minLength": 1}
            try:
                position = keys.index(key)
                reqiure = required[position]
                if reqiure == True:
                    value.update(update)
            except:
                pass

        details = OrderedDict()
        details = details.fromkeys(keys, '')
        ids = response.xpath('//*/@id').getall()

        for i in range(len(keys)):
            if details[keys[i]] == '':
                value_xpath = self.check_id(values[i], ids, details, keys[i], response)
                details[keys[i]] = ''.join(response.xpath(value_xpath).getall()).strip().replace("\n", ' ').replace("\t", ' ').replace("\r", " ")
                if keys[i] == "breadcrumb":
                    details[keys[i]] = ' '.join(response.xpath(value_xpath).getall()).strip().replace("\n", ' ').replace("\t", ' ').replace("\r", " ")
                    pattern = r"[a-zA-Z]+"
                    details[keys[i]] = '|'.join(re.findall(pattern, details[keys[i]])).replace("\n", ' ').replace("\t", ' ').replace("\r", " ")

        item.update(details)
        yield item


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback)
        # if self.proxy_pool:
        #     request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)
        return request
