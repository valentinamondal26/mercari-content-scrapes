import sys
import argparse
from automate.spiders.static_crawler import CrawlAutomate
from automate.spiders.dynamic_crawler import CrawlAutomateSelenium
import json
from datetime import datetime
import os
import subprocess
import re

import logging
from logging.handlers import RotatingFileHandler

from google.cloud import storage


import scrapy
from scrapy.crawler import CrawlerProcess
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner

logger = logging.getLogger("app")
logger.setLevel(logging.DEBUG)
file_handler = RotatingFileHandler(
    'debug.log', maxBytes=1024 * 1024 * 100, backupCount=20)
formatter = logging.Formatter(
    "%(asctime)s - [%(filename)s:%(lineno)s - %(funcName)20s() ] - %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)


class Main(object):

    gcs_report_path = 'raw/{source}/{category}/{brand}/{timestamp}/{format}/{filename}'
    brand = None
    xpath_json = None
    time_stamp = None

    def execute_command(self, preexec_fn=False, spider_name=None):

        response = {}
        cmd = 'python -m scrapy crawl {spider_name} -a brand={brand}  -a xpath_json={xpath_json}  -s GCS_BLOB_TIMESTAMP={timestamp}'.format(
            brand=self.brand, xpath_json=self.xpath_json, timestamp=self.time_stamp, spider_name=spider_name)

        logger.info('Executing {} crawl : {}\n'.format(spider_name, cmd))
        print('Executing {} crawl : {}\n'.format(spider_name, cmd))
        cmd = cmd.split()
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             stdin=subprocess.PIPE,
                             preexec_fn=os.setsid if preexec_fn else None)
        out, err = p.communicate()
        response["out"] = out.decode("utf-8") if out.decode("utf-8") else ""
        if err.decode("utf-8"):
            response["error"] = err.decode("utf-8")
        logger.debug("execute command response {0}".format(response))
        print("execute command response {0}".format(response))
        if spider_name == 'dynamic':
            print(response)

    def findOccurrences(self, s, ch):

        return [i for i, letter in enumerate(s) if letter == ch]

    def extract_source(self):

        client = storage.Client()
        bucket = client.get_bucket('content_us')
        blob_path = self.xpath_json.split("content_us/")[1]
        blob = bucket.get_blob(blob_path)
        downloaded_blob = blob.download_as_string().decode("utf-8")
        js = json.loads(downloaded_blob)

        launch_url = js.get('crawlSource', '')
        source = ''
        category = ''
        pattern = r"https://[a-zA-Z\.\-]+"
        matches = re.findall(pattern, launch_url)
        if len(matches) > 0:
            pattern = r"[a-zA-Z]+\.{1}[a-zA-Z]{2,3}"
            occurences = self.findOccurrences(matches[0], '.')
            occurences = occurences[-2:]
            source = matches[0][occurences[0]+1:occurences[1]]+".com"
        category = js.get('category', '')

        return source, category

    def get_threshold(self):

        # extract the gcs path of reports.txt of static crawler
        source, category = self.extract_source()
        self.gcs_report_path = self.gcs_report_path.format(source=source, category=category.lower(),
                                                           brand=self.brand.lower(), timestamp=self.time_stamp, format='report', filename='report.txt')

        client = storage.Client()
        bucket = client.get_bucket('content_us')
        blob = bucket.get_blob(self.gcs_report_path)
        downloaded_blob = blob.download_as_string().decode("utf-8")
        report = json.loads(downloaded_blob)
        logger.info('Threshold status : {}\n'.format(
            report.get('schema', {}).get('threshold_status', '')))

        return report.get('schema', {}).get('threshold_status', '')

    def main(self, brand, xpath_json, timestamp):

        self.brand = brand
        self.xpath_json = xpath_json
        self.time_stamp = timestamp

        self.execute_command(spider_name='static')
        # logger.info('Checking threshold for switching\n')
        # print(self.get_threshold(),"\n\nThreshold\n\n")
        # if not self.get_threshold():
        #     # generating new timestamp for dynamic crawl to avoid overwritting
        #     self.time_stamp = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        #     self.execute_command(spider_name='dynamic')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Crawler Process')
    parser.add_argument('--brand', required=True, help='Enter Brand')
    parser.add_argument('--xpath_json', required=True,
                        help='Enter xpath json path')
    parser.add_argument('--timestamp', required=True,
                        help='Enter timestamp for gcs uplaod')

    args = parser.parse_args()
    Main().main(brand=args.brand, xpath_json=args.xpath_json, timestamp=args.timestamp)
