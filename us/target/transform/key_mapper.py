class Mapper:
    def map(self, record):

        model = ''
        color = ''

        for entity in record['entities']:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                color = entity["attribute"][0]["id"]

        key_field = model + color
        return key_field
