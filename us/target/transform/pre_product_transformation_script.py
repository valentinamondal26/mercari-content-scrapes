import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            record = self.pre_process(record)
            transformed_record = dict()

            ner_query = record.get('description', record.get('title', '')) + ' ' + record.get('features', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record['price']
            price = price.replace("$", "")

            title = record.get('title', '')
            brand = record.get('brand', '')
            model = re.sub(re.compile(brand, re.IGNORECASE), '', title)
            for color in record.get('color', '').split(','):
                model = model.replace(color, '')
            model = re.sub(re.compile(r' - (.*)', re.IGNORECASE), '', model)
            model = re.sub(re.compile('Travel System|®|&#174;', re.IGNORECASE), '', model)
            model = re.sub(re.compile(', Choose Your Color', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()


            max_weight = ''
            max_weight_capacity = record.get('Max. Stroller Weight Capacity', '') or ''
            max_weight = re.sub(re.compile(r"pounds|lbs", re.IGNORECASE),
                '', max_weight_capacity).strip()

            weight = ''
            weight_capacity = record.get('Weight', '') or ''
            weight = re.sub(re.compile(r"pounds|lbs", re.IGNORECASE),
                '', weight_capacity).strip()

            height = ''
            width = ''
            length = ''
            dimensions = record.get('Dimensions (Overall)', '')
            dimenstions_pattern = r'(\d+(?:.\d+)?)\s*inches\s*\(H\)\s*x\s*(\d+(?:.\d+)?)\s*inches\s*\(W\)\s*x\s*(\d+(?:.\d+)?)\s*inches\s*\(D\)'
            match = re.findall(re.compile(dimenstions_pattern, re.IGNORECASE), dimensions)
            if match and len(match[0]) > 2:
                height = match[0][0].strip()
                width = match[0][1].strip()
                length = match[0][2].strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),

                "category": record.get('category'),
                "brand": brand,

                "title": title,
                # "breadcrumb": record.get('breadcrumb', ''),
                "description": record.get('description', ''),
                "ner_query": ner_query,
                "image": record.get('image', ''),

                "model": model,
                "color": record.get('color', ''),
                "material": '',
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
                'upc': record.get('upc', ''),
            }

            transformed_record = self.post_process(transformed_record)
            return transformed_record
        else:
            return None
    

    def pre_process(self, record):
        for key in record:
            if not record[key]:
                record[key]=''
        return record


    def post_process(self, record):
        record = {k: v for k, v in record.items() if len(v) > 0}
        return record
