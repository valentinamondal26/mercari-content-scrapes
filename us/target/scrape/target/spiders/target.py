'''
https://mercari.atlassian.net/browse/USDE-281 - Strollers Travel System
https://mercari.atlassian.net/browse/USDE-355 - Strollers Double Strollers
https://mercari.atlassian.net/browse/USDE-356 - Strollers Jogging Strollers
https://mercari.atlassian.net/browse/USDE-357 - Strollers Lightweight Strollers

https://mercari.atlassian.net/browse/USCC-631 - Epic - Target ONLY brands
'''

import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import copy
from urllib.parse import urlparse, parse_qs, urlencode
import re
import uuid

class TargetSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from target.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Strollers

    sub_category : str
        brand to be crawled, Supports
        1) Travel System
        2) Double Strollers
        3) Jogging Strollers
        4) Lightweight Strollers

    Command e.g:
    scrapy crawl target -a category='Strollers' -a brand='Travel System'
    scrapy crawl target -a category='Strollers' -a brand='Double Strollers'
    scrapy crawl target -a category='Strollers' -a brand='Jogging Strollers'
    scrapy crawl target -a category='Strollers' -a brand='Lightweight Strollers'

    scrapy crawl target -a category='' -a brand=''

    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='Cat & Jack'
    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='A New Day'
    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='All In Motion'
    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='More Than Magic'
    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='Pillowfort'
    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='Art Class'
    scrapy crawl target -a category='Multiple Clothing Categories' -a brand='Prologue'

    scrapy crawl target -a category='Pet Accessories & Toys' -a brand='Boots & Barkley'

    scrapy crawl target -a category='Luggage' -a brand='Open Story'
    scrapy crawl target -a category='Luggage' -a brand='Ava & Viv'

    scrapy crawl target -a category="Bra's and Underwear" -a brand='Auden'

    scrapy crawl target -a category="Women's Swimwear" -a brand='Shade & Shore'

    scrapy crawl target -a category="Women's clothes" -a brand='Colsie'

    scrapy crawl target -a category="Women's Sleepwear" -a brand='Stars Above'

    scrapy crawl target -a category="Men's Wear" -a brand='Goodfellow & Co'

    scrapy crawl target -a category='Clothing and Shoes' -a brand='Universal Thread'
    scrapy crawl target -a category='Clothing and Shoes' -a brand='JoyLab'
    scrapy crawl target -a category='Clothing and Shoes' -a brand='Wild Fable'
    scrapy crawl target -a category='Clothing and Shoes' -a brand='Knox Rose'
    scrapy crawl target -a category='Clothing and Shoes' -a brand='Kona Sol'
    scrapy crawl target -a category='Clothing and Shoes' -a brand='Xhilaration'
    """

    name = "target"
    category = None
    brand = None
    source = 'target.com'
    blob_name = 'target.txt'


    product_listing_url = 'https://redsky.target.com/v2/plp/search'
    count = 24
    offset = 0
    category_id = None
    detail_url_template = 'https://redsky.target.com/v3/pdp/tcin/{prod_id}?excludes=taxonomy&key={key}'

    base_url = 'https://www.target.com'

    proxy_pool = [
        'http://shp-mercari-us-d00011.tp-ns.com:80',
        'http://shp-mercari-us-d00012.tp-ns.com:80',
        'http://shp-mercari-us-d00013.tp-ns.com:80',
        'http://shp-mercari-us-d00014.tp-ns.com:80',
        'http://shp-mercari-us-d00015.tp-ns.com:80',
    ]

    url_dict = {
        'Strollers': {
            'Travel System': {
                'url': 'https://www.target.com/c/travel-system-strollers-baby/-/N-5xtjw?lnk=TravelSystemStr',
            },
            'Double Strollers': {
                'url': 'https://www.target.com/c/double-triple-quad-strollers-baby/-/N-5xtk5',
            },
            'Jogging Strollers': {
                'url': 'https://www.target.com/c/joggers-strollers-baby/-/N-5xtk3?lnk=JoggingStroller',
            },
            'Lightweight Strollers': {
                'url': 'https://www.target.com/c/lightweight-strollers-baby/-/N-5xtk2?lnk=Umbrellastrolle',
            },
        },
        'Multiple Clothing Categories': {
            'Cat & Jack': {
                'url': 'https://www.target.com/b/cat-jack/-/N-qqqgm?Nao=0',
            },
            'A New Day': {
                'url': 'https://www.target.com/b/a-new-day/-/N-xrye7',
            },
            'All In Motion': {
                'url': 'https://www.target.com/b/all-in-motion/-/N-4apdi',
            },
            'More Than Magic': {
                'url': 'https://www.target.com/b/more-than-magic/-/N-f7ns3',
            },
            'Pillowfort': {
                'url': 'https://www.target.com/b/pillowfort/-/N-o212a',
            },
            'Art Class': {
                'url': 'https://www.target.com/b/art-class/-/N-holui',
            },
            'Prologue': {
                'url': 'https://www.target.com/b/prologue/-/N-cb3bq',
            }
        },
        'Pet Accessories & Toys': {
            'Boots & Barkley': {
                'url': 'http://target.com/b/boots-barkley/-/N-vyzcr',
            },
        },
        'Luggage': {
            'Open Story': {
                'url': 'https://www.target.com/b/open-story/-/N-vi5wb?lnk=Premiumluggagea_OpenStory',
            },
            'Ava & Viv': {
                'url': 'https://www.target.com/b/ava-viv/-/N-o267d',
            }
        },
        "Bra's and Underwear": {
            'Auden': {
                'url': 'https://www.target.com/b/auden/-/N-mg0o7',
            }
        },
        "Women's Swimwear": {
            'Shade & Shore': {
                'url': 'https://www.target.com/b/shade-shore/-/N-4uja2',
            }
        },
        "Women's clothes": {
            'Colsie': {
                'url': 'https://www.target.com/b/colsie/-/N-gk29m',
            }
        },
        "Women's Sleepwear": {
            'Stars Above': {
                'url': 'https://www.target.com/b/stars-above/-/N-g0mql',
            }
        },
        "Men's Wear": {
            'Goodfellow & Co': {
                'url': 'https://www.target.com/b/goodfellow-co/-/N-rcv2g',
            }
        },
        'Clothing and Shoes': {
            'Universal Thread': {
                'url': 'https://www.target.com/b/universal-thread/-/N-1vs54',
            },
            'JoyLab': {
                'url': 'https://www.target.com/b/joylab/-/N-mabkw',
            },
            'Wild Fable': {
                'url': 'https://www.target.com/b/wild-fable/-/N-e6p1m',
            },
            'Knox Rose': {
                'url': 'https://www.target.com/s?searchTerm=Knox+Rose',
            },
            'Kona Sol': {
                'url': 'https://www.target.com/b/kona-sol/-/N-xnj7h',
            },
            'Xhilaration': {
                'url': 'https://www.target.com/b/xhilaration/-/N-5y5ws',
            }
        }
    }


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TargetSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        # self.sub_category = sub_category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        o = urlparse(self.launch_url)
        path = o.path
        sp = path.split('/')
        print(sp)
        if len(sp) > 1 and sp[-2] == '-':
            self.category_id = sp[-1].split('-')[-1]

        self.logger.info(f'Crawling for category: {category} and brand[or]sub_category: {brand}')


    def contracts_mock_function(self):
        self.category_id = 'N-qqqgm'
        self.key = str(uuid.uuid4())


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def req(self):
        query_params = {
            "brand_id": "",
            "category": self.category_id,
            "channel": "web",
            "count": str(self.count),
            "default_purchasability_filter": "true",
            "offset": str(self.offset),
            "pricing_store_id": "1407",
            "useragent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
            "key": self.key,
        }

        self.offset = self.offset + self.count
        url = self.product_listing_url + '/?' + urlencode(query_params)
        return self.create_request(url=url, callback=self.parse_listing_page)


    def parse(self, response):
        """
        @url https://www.target.com/b/cat-jack/-/N-qqqgm?Nao=0
        @returns requests 1
        @meta {"use_proxy":"True"}
        """

        script = response.xpath('//script[contains(text(), "window.__PRELOADED_STATE__=")]/text()').get(default='')
        match = re.findall(r'"apiKey":"(.*?)"', script)
        if match:
            self.key = match[0]
        else:
            self.key = str(uuid.uuid4())

        if self.key and self.category_id:
            yield self.req()


    def parse_listing_page(self, response):
        # TODO: removed existing scrapy contract. Will needs to be restored by finding a way to
        # dynamically inject the header params from the scrapy contracts

        res = json.loads(response.text)

        products = res.get('search_response', {}).get('items', {}).get('Item', [])
        if products:
            for product in products:
                prod_id = product.get('tcin', '')

                rating = product.get('average_rating', 0)
                url = product.get('url', '') or ''
                if not url:
                    continue
                url = self.base_url + url

                item = {
                    'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'statusCode': str(response.status),
                    'category': self.category,
                    'id': url,
                    'image': (product.get('images', [{}])[0].get('base_url', '') or '') + (product.get('images', [{}])[0].get('primary', '') or ''),
                    'rating': str(rating) if rating else '',
                    'price': product.get('price', {}).get('formatted_current_price', '') or '',
                    'title': product.get('title', '') or '',
                    'description': product.get('description', '') or '' + ' ' + ' '.join(product.get('soft_bullets', {}).get('bullets', []) or []),
                    'highlights': '\n'.join(product.get('soft_bullets', {}).get('bullets', [])),
                    'brand': product.get('brand', '') or '',
                    # 'breadcrumb': ' | '.join([breadcrumb.get('label', '') or '' for breadcrumb in product.get('search_response', {}).get('breadCrumb_list', [{}]).get('breadCrumbValues', [])]) or '',
                    'color': ','.join(product.get('variation_attributes', {}).get('color', [])),
                    # 'stroller_type': self.brand,
                    'sub_category': self.brand,
                    'upc':product.get('upc', ''),
                }
                # if self.category == 'Strollers':
                #     item['stroller_type'] = self.brand


                features = {}
                for desc in product.get('bullet_description', []):
                    feature = desc.replace('<B>', '').replace('</B>', '').split(':', 1)
                    if feature and len(feature) > 1:
                        features[feature[0]] = feature[1]
                # item.update(features)
                item['spec'] = features


                if product.get('variation_attributes', {}):
                    for variant_key, variant_values in product.get('variation_attributes', {}).items():
                        if isinstance(variant_values, list):
                            index = 0
                            for variant_value in variant_values:
                                variant = product.get('child_items')[index]
                                yield copy.deepcopy(item).update({
                                    'image': (variant.get('images', {}).get('base_url', '') or '') + (variant.get('images', {}).get('primary', '') or ''),
                                    'id': url.replace(prod_id, variant.get('tcin', '')) or '',
                                    variant_key: variant_value,
                                })
                                index = index + 1
                else:
                    yield item

                # if prod_id:
                #     detail_url = self.detail_url_template.format(prod_id=prod_id, key=self.key)
                #     request = self.create_request(detail_url, self.parse_detail)
                #     # request.meta['item'] = item
                #     yield request



        total = ''.join([metadata.get('value', '') for metadata in res.get('search_response', {}).get('metaData', [{}]) if metadata.get('name', '')=='total_results'])
        offset = ''.join([metadata.get('value', '') for metadata in res.get('search_response', {}).get('metaData', [{}]) if metadata.get('name', '')=='offset'])
        # count = ''.join([metadata.get('value', '') for metadata in res.get('search_response', {}).get('metaData', [{}]) if metadata['name']=='count'])

        if int(offset) + self.count < int(total):
            yield self.req()


    # def parse_detail(self, response):
    #     # print('response:', response.text)
    #     res = json.loads(response.text)

    #     item = {} #res.meta.get('item', {})

    #     product_item = res.get('product', {}).get('item', {}) or {}
    #     product_id = product_item.get('tcin', '') or ''
    #     rating = res.get('product', {}).get('rating_and_review_statistics', {}).get('result', {}).get(product_id, {}).get('coreStats', {}).get('AverageOverallRating', 0)
    #     color = product_item.get('variation', {}).get('color', '') or ''
    #     image = product_item.get('enrichment', {}).get('images', [{}])[0]
    #     item = {
    #         'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
    #         'statusCode': str(response.status),
    #         'category': self.category,
    #         'id': product_item.get('buy_url', '') or '',
    #         'image': image.get('base_url', '') or '' + image.get('primary', '') or '',
    #         'rating': str(rating) if rating else '',
    #         'description': product_item.get('product_description', {}).get('downstream_description', '') or '',
    #         'collection': product_item.get('parent_items', [{}])[0].get('product_description_title', ''),

    #         # 'price': res.get('priceSet', [{}])[0].get('formattedPrice', '') or '',
    #         'title': product_item.get('product_description', {}).get('title', '') or '',
    #         'brand': product_item.get('product_brand', {}).get('brand', '') or '',
    #         # 'height': '',
    #         # 'width': '',
    #         # 'length': '',
    #         # 'weight': '',
    #         # 'max_weight_capacity': '',
    #         # 'upc': '',
    #         'color': color,
    #         'price': '',
    #         'stroller_type': '',
    #         # 'model': '',
    #     }

    #     features = {}
    #     for desc in product_item.get('product_description', {}).get('bullet_description', []):
    #         feature = desc.replace('<B>', '').replace('</B>', '').split(':', 1)
    #         if feature and len(feature) > 1:
    #             features[feature[0]] = feature[1]
    #     item.update(features)
    #     yield item


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
