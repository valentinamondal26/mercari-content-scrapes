'''
https://mercari.atlassian.net/browse/USCC-705 - Tony Lama Unisex Kids' Shoes
https://mercari.atlassian.net/browse/USCC-706 - Tony Lama Women's Shoes
https://mercari.atlassian.net/browse/USCC-707 - Tony Lama Men's Shoes
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os
import re


class TonylamaSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from tonylama.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Unisex Kids' Shoes
        2) Women's Shoes
        3) Men's Shoes

    brand : str
        brand to be crawled, Supports
        1) Tony Lama

    Command e.g:
    scrapy crawl tonylama -a category="Unisex Kids' Shoes" -a brand='Tony Lama'
    scrapy crawl tonylama -a category="Women's Shoes" -a brand='Tony Lama'
    scrapy crawl tonylama -a category="Men's Shoes" -a brand='Tony Lama'
    """

    name = 'tonylama'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'tonylama.com'
    blob_name = 'tonylama.txt'

    base_url = 'https://www.tonylama.com'
    page = 2

    url_dict = {
        "Unisex Kids' Shoes": {
            'Tony Lama': {
                'url': 'https://www.tonylama.com/en-US/Products?gender=9&productline=264',
            }
        },
        "Women's Shoes": {
            'Tony Lama': {
                'url': 'https://www.tonylama.com/en-US/Products?gender=8&productline=264',
            }
        },
        "Men's Shoes": {
            'Tony Lama': {
                'url': 'https://www.tonylama.com/en-US/Products?gender=4&productline=264',
            }
        },
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(TonylamaSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category "+category+" and brand "+self.brand + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.launch_url = 'https://www.tonylama.com/en-US/Products?gender=4&productline=264'


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.tonylama.com/en-US/Product/Details?stylenumber=RR3041
        @meta {"use_proxy":"True"}
        @scrape_values id title images price color specs model_sku description
        """

        specs = {}
        details = response.xpath("//ul[@class='descDetails__list']/li/text()").getall()
        for detail in details:
            if ":" in detail:
                key, value = detail.split(":")
                specs[key] = value

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath("//h1[@id='main-title']/span/text()").get(),
            'images': response.xpath("//span[@class='productImage productImages__image productImage--quaternary']/span/img/@data-src").getall(),
            'price': response.xpath("//span[@class='sizeGrid__retailPrice']/s/text()").get() or response.xpath("//span[@class='sizeGrid__price']/text()").get(),
            'color': response.xpath("//span[@class='swatches__color']/text()").get(),
            'specs': specs,
            'model_sku': response.xpath("normalize-space(//span[@class='sizeGrid__stockNumber'])").get(),
            'description': response.xpath("normalize-space(//div[@class='descDetails__column'])").get()
        }


    def parse(self, response):
        """
        @url https://www.tonylama.com/en-US/Products?gender=4&productline=264
        @meta {"use_proxy":"True"}
        @returns requests 1
        """

        links = response.selector.xpath('//div[@class="productGroup__inner"]//a[@class="productCaption__anchor"]')
        for product in links:
            product_url = product.xpath('./@href').extract_first()
            if product_url:
                yield self.create_request(url=self.base_url+product_url, callback=self.crawlDetail)

        if links:
            current_page = ''.join(re.findall(r'page=(\d+)', response.url)) or '0'
            next_page_link = self.launch_url + f"&page={int(current_page) + 1}"
            if next_page_link:
                yield self.create_request(url=next_page_link, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
