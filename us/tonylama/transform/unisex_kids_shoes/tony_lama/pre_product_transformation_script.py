import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            specs = record.get('specs', {})

            material = specs.get('Material', '')
            material = re.sub(r'(.*)(Leather)(.*)', r'\1 \3 \2', material, flags=re.IGNORECASE)
            if not re.findall(r'\bLeather\b', material, flags=re.IGNORECASE):
                material += ' Leather'
            material = re.sub(r'\s+', ' ', material).strip()

            model = record.get('title', '')
            model = model.title()

            color = ''
            colors = record.get('color', '').split('/')
            if len(colors) > 2:
                colors = [colors[0], 'Multi']
            color = '/'.join(colors)


            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,
                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "image": record.get('images', [''])[0],

                "model": model,
                "color": color,
                "material": material,
                'toe_type': specs.get('Toe Shape', '').strip(),
                'boot_shaft_height': specs.get('Height','').strip(),
                'model_sku': record.get('model_sku', '').replace('Style:','').strip(),
                'msrp': {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
