'''
https://mercari.atlassian.net/browse/USDE-1058 - Kate Spade New York Crossbody Bags
https://mercari.atlassian.net/browse/USDE-1059 - Kate Spade New York Shoulder Bags
https://mercari.atlassian.net/browse/USDE-1060 - Kate Spade New York Tote Bags
https://mercari.atlassian.net/browse/USDE-1061 - Kate Spade New York Satchel
https://mercari.atlassian.net/browse/USDE-1062 - Kate Spade New York Women's Wallet
'''

# -*- coding: utf-8 -*-
from parsel import Selector
import scrapy
import os
from datetime import datetime
import re
from scrapy.exceptions import CloseSpider
import random
from common.selenium_middleware.http import SeleniumRequest



class KatespadeSpider(scrapy.Spider):

    """
     spider to crawl items in a provided category and sub-category from katespade.com

     Attributes

     category : str
        category to be crawled, Supports
        1) Shoulder Bags
        2) Crossbody Bags
        3) Tote Bags
        4) Satchel
        5) Women's Wallet
        
    
     brand : str
      brand to be crawled, Supports
       1) Kate Spade New York
    
     Command e.g:
     scrapy crawl katespade -a category='Shoulder Bags' -a brand='Kate Spade New York'
     scrapy crawl katespade -a category='Crossbody Bags' -a brand='Kate Spade New York'
     scrapy crawl katespade -a category='Tote Bags' -a brand='Kate Spade New York'
     scrapy crawl katespade -a category='Satchel' -a brand='Kate Spade New York'
     scrapy crawl katespade -a category="Women's Wallet" -a brand='Kate Spade New York'
    
    """
    name = "katespade"

    category = None
    brand = None
    source = 'katespade.com'
    blob_name = 'katespade.txt'

    merge_key = 'id'

    filters = None
    launch_url = None

    base_url='https://katespade.com'

    url_dict = {
        'Shoulder Bags': {
            'Kate Spade New York': {
                'url': 'https://www.katespade.com/handbags/shoulder-bags/',
                'filter': ["Color","Material"]
            }
        },
        'Crossbody Bags': {
            'Kate Spade New York': {
                'url': 'https://www.katespade.com/handbags/crossbodies/',
                'filter': ["Color","Material"]
            }
        },
        "Women's Wallet": {
            'Kate Spade New York': {
                'url': 'https://www.katespade.com/wallets/',
                'filter': ["Color","Material","Item-type"]
            }
        },
        'Tote Bags': {
            'Kate Spade New York': {
                'url': 'https://www.katespade.com/handbags/totes/',
                'filter': ["Color","Material"]
            }
        },
        'Satchel': {
            'Kate Spade New York': {
                'url': 'https://www.katespade.com/handbags/satchels/',
                'filter': ["Color","Material"]
            }
        },
    }

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80'
    ]


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(KatespadeSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            self.contracts_mock_function()
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filter', '')

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for  category " +category + ",url:" + self.launch_url)


    def contracts_mock_function(self):
        self.filters = ["Color","Material"]
        self.launch_url = 'https://www.katespade.com/handbags/shoulder-bags/'


    def start_requests(self):
        if self.launch_url:
            yield self.createDynamicRequest(url=self.launch_url, callback=self.total_filters)


    def total_filters(self, response):
        """
        @url https://www.katespade.com/handbags/shoulder-bags/
        @meta {"use_selenium":"True"}
        @returns requests 7
        """

        searchResultsCount = response.css('div.sort-box-count').xpath('.//span/text()').get(default='')
        # print(searchResultsCount)
        
        counts = re.findall(r'\d+', searchResultsCount)
        self.logger.info('Total products count: {}'.format(counts))
        if counts:
            totalCount = int(counts[0])
            startIndex = 0
            itemsPerPage = 30
            while startIndex < totalCount:
                url = self.launch_url + '?start='+str(startIndex)+'&sz='+str(itemsPerPage)+'&format=page-element&instart_disable_injection=true'
                startIndex = startIndex + 30
                yield self.createDynamicRequest(url, callback=self.parse_data)

        for filter_type in self.filters:
            filter_class = 'refinement-list '+filter_type.lower()
            if filter_type == "Color":
                total_style_filter_urls = response.xpath("//ul[@class='"+filter_class+"']/li/div/a/@href").getall()
                total_style_name =  response.xpath("//ul[@class='"+filter_class+"']/li/div/a/@title").getall() 
            elif filter_type == "Material" or filter_type == "Item-type":
                total_style_filter_urls = response.xpath("//ul[@class='"+filter_class+"']/li/a/@href").getall()
                total_style_name =  response.xpath("//ul[@class='"+filter_class+"']/li/a/@title").getall()

            for filter_name, filter_url in zip(total_style_name, total_style_filter_urls):
                if "format=ajax" not in filter_url:
                    filter_url = filter_url+"&format=ajax"
                meta = {"filter_name": filter_type}
                meta.update({meta["filter_name"]: filter_name.replace("Refine by:","").strip()})

                # since we dont have the number of items available for this filter,
                # we iterate for the total count ot items without filter
                if counts:
                    totalCount = int(counts[0])
                    startIndex = 0
                    itemsPerPage = 30
                    while startIndex < totalCount:
                        url = filter_url + '&start='+str(startIndex)+'&sz='+str(itemsPerPage)#+'&format=page-element&instart_disable_injection=true'
                        startIndex = startIndex + 30
                        yield self.createDynamicRequest(url, callback=self.parse_data, meta=meta)


    def parse_data(self,response):
        """
        @url https://www.katespade.com/handbags/shoulder-bags/?start=0&sz=30&format=page-element&instart_disable_injection=true
        @returns requests 13
        @meta {"use_selenium":"True"}
        """

        meta = response.meta 
        for product in response.xpath('//div[contains(@class, "grid-tile")]'):
            url = product.xpath(".//div[@class='product-name ']/h2/a/@data-full-url").extract_first()
            category_id = product.xpath('.//div[@class="product-tile"]/@data-cgid').extract_first()
            if category_id:
                url = url.replace('&cgid={}'.format(category_id), '')
            urls = []
            urls.append(url)
            if not meta.get('filter_name', ''):
                # color variants will only be fetched from the source page,
                # need to be be again fetched from the filter page,
                # as the filter informations will be wrongly updated to
                # other color variants
                color_variants = product.xpath('.//li[contains(@class, "swatchlie")]//a/@href').getall()
                urls.extend(color_variants)
            for url in urls:
                yield self.createRequest(url, callback=self.parse_items, meta=meta)


    def parse_items(self,response):
        """
        @url https://www.katespade.com/products/polly-medium-convertible-flap-shoulder-bag/PXRUA885.html?cgid=ks-handbags-shoulder-bags
        @scrape_values id image measurements description model price style color details_info.materials details_info.features details_info.details details_info.all
        """

        meta = response.meta
        id = response.url
        crawl_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        status_code = str(response.status)
        image = response.xpath('//a[@class="product-image main-image"]/img/@src').get()
        color = response.xpath('//ul[contains(@class, "swatches Color")]//li[@class="selected"]/span[@class="title"]/text()').extract_first()

        measurements = response.xpath('//div[@id="small-description"]/div[1]/ul[1]/li/text()').get(default='')
        x = re.findall(r'\d+"h x', measurements)  
        if not x:
            measurements = response.xpath("//div[@class='short-right']/ul/li/text()").get(default='')

        description = response.xpath(u'normalize-space(//div[@id="small-details"]/text())').get()
        model = response.xpath(u'normalize-space(//h1[@class="product-name"]/text())').get()
        price = response.xpath(u'normalize-space(//span[@class="price-sales"]/text())').get()
        style = response.xpath('//div[@class="product-variations"]/@data-master').get()
        data = {
            'id': id,
            'crawl_date': crawl_date,
            'category': self.category,
            'status_code': status_code,
            'brand': self.brand,
            'image': image,
            'measurements': measurements,
            'description': description,
            'model': model,
            'price': price,
            'style': style,
            'color': color,
            'details_info': {
                'materials': response.xpath('//div[@class="short-right" or @class="short-left"]/p[contains(text(), "MATERIAL")]/following-sibling::ul[1]//text()').extract(),
                'features': response.xpath('//div[@class="short-right" or @class="short-left"]/p[contains(text(), "FEATURES")]/following-sibling::ul[1]//text()').extract(),
                'details': response.xpath('//div[@class="short-right" or @class="short-left"]/p[contains(text(), "DETAILS")]/following-sibling::ul//text()').extract(),
                'all': response.xpath('//div[@id="small-description"]//li/text()').extract(),
            }
        }
        meta_data={}
        filter_name = meta.get('filter_name', '')
        if filter_name:
            meta_data =  meta_data.fromkeys([filter_name], meta[filter_name])
            data.update(meta_data)
        
        yield data


    def createRequest(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        request.meta.update({"use_cache":True})
        return request


    def createDynamicRequest(self, url, callback, errback=None, meta=None):
        request = SeleniumRequest(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = self.proxy_pool[0]

        request.meta.update({"use_cache":True})
        return request
