import hashlib
import re


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            title = record.get("model", "")

            material = record.get('Material', '')
            if not material:
                material_info = record.get(
                    'details_info', {}).get('materials', [])
                if material_info:
                    material_meta = ['italian leather', 'saffiano leather', 'pvc', 'refined grain leather', 'crossgrain leather',
                                     'nylon', 'italian pebbled leather', 'pebbled leather', 'leather', 'canvas']
                    match = re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b{}\b'.format(
                        x), material_meta))), re.IGNORECASE), material_info[0])
                    if match:
                        material = ', '.join(match)
            material = material.title().strip()
            material = re.sub('Pvc', 'PVC', material)
            match = re.findall(re.compile(r'Leather', re.IGNORECASE), material)
            if match and len(match) > 1:
                material = ','.join(
                    list(filter(lambda x: x != 'Leather', material.split(','))))
            material = re.sub(r'\s+', ' ', material).strip()

            color = record.get('Color', '') or record.get('color', '')
            color = re.sub(re.compile(r'Blackandwhite',
                                      re.IGNORECASE), 'Black/White', color)
            color = color.title().strip()

            keywords = ['tote', 'saddle', r'bag[s]*',
                        r'\bx*\s*kate spade new york']
            keywords.extend(color.split(', '))
            keywords.extend(material.split(', '))
            model = re.sub(re.compile(r'|'.join(
                list(map(lambda x: r'\b{}\b'.format(x), keywords))), re.IGNORECASE), '', title)
            model = re.sub(r'\s+', ' ', model).strip()
            model = model.title()
            model = re.sub(re.compile(r"X Minnie Mouse Francis",
                                      re.IGNORECASE), "Minnie Mouse Francis", model)
            model = " ".join(model.split())

            measurements = record.get("measurements", "")
            try:
                pattern = re.compile(r'(\d+\.*\d*\")h')
                height = re.findall(pattern, measurements)[0]
            except IndexError:
                height = ''

            try:
                pattern = re.compile(r'(\d+\.*\d*\")w')
                width = re.findall(pattern, measurements)[0]
            except IndexError:
                width = ''

            try:
                pattern = re.compile(r'(\d+\.*\d*\")d')
                depth = re.findall(pattern, measurements)[0]
            except IndexError:
                depth = ''

            # removing the items missing the color info, since the crawl produces
            # noisy ids(urls) which doesn't have selected colors present in it
            if not color or re.findall(re.compile(r"^Large Tote$", re.IGNORECASE), title) != []:
                return None

            transformed_record = {
                "id": record.get("id", ""),
                "item_id": hex_dig,
                "statusCode": record.get("status_code", ""),
                "crawlDate": record.get("crawl_date", []).split(', ')[0],

                "title": record.get("model", ""),

                "brand": record.get('brand', ''),
                "category": record.get('category', ''),

                "model": model.strip(),
                "price": {
                    "currencyCode": "USD",
                    "amount": record.get("price", "").replace("$", "")
                },
                "width": width,
                "height": height,
                "depth": depth,
                "image": record.get("image", ""),
                "description": record.get("description", ""),
                "model_sku": record.get("style", ""),
                "color": color,
                "material": material,
            }
            return transformed_record
        else:
            return None
