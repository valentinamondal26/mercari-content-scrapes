class Mapper:
    def map(self, record):
        key_field = ""

        color = ''
        material = ''
        model = ''

        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                if entity["attribute"]:
                    for attribute in entity["attribute"]:
                        color = color + attribute["id"]
            elif "material" == entity['name']:
                if entity["attribute"]:
                    for attribute in entity["attribute"]:
                        material = material + attribute["id"]

        key_field = color + material + model
        return key_field
