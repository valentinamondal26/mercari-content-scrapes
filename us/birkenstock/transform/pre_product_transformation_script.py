import hashlib
import re
class Mapper(object):

    def map(self, record):
        if record:
            ner_query = record.get('description', record.get('title', ''))
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            price = record.get('price', '')
            price = price.replace("$", "").strip()

            model = record.get('title', '')
            model = re.sub(re.compile('Patent', re.IGNORECASE), '', model)
            model = re.sub(' +', ' ', model).strip()

            material = record.get('material', '')
            material = re.sub(re.compile('Patent', re.IGNORECASE), '', material)
            material = re.sub(' +', ' ', material).strip()

            transformed_record = {
                "id": record.get('id', ''),
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', ''),
                "category": record.get('category'),
                "brand": record.get('brand', ''),

                "ner_query": ner_query,

                "title": record.get('title', ''),
                "description": record.get('description', ''),
                "model": model,
                "color": record.get('color', ''),
                "material": material,
                'shoe_width': record.get('shoe_width', ''),
                'model_sku': record.get('model_sku', ''),
                "price": {
                    "currencyCode": "USD",
                    "amount": price
                },
            }

            return transformed_record
        else:
            return None
