'''
https://mercari.atlassian.net/browse/USCC-194 - Birkenstock Sandals
'''

import scrapy
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os

class BirkenstockSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from birkenstock.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Sandals
    
    brand : str
        brand to be crawled, Supports
        1) Birkenstock

    Command e.g:
    scrapy crawl birkenstock -a category='Sandals' -a brand='Birkenstock'
    """

    name = 'birkenstock'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = ''
    brand = ''
    source = 'birkenstock.com'
    blob_name = 'birkenstock.txt'

    url_dict = {
        'Womens Shoes': {
            'Birkenstock': {
                'url': 'https://www.birkenstock.com/us/shoes/women/',
            }
        },
        'Sandals': {
            'Birkenstock': {
                'url': 'https://www.birkenstock.com/us/women/sandals/'
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BirkenstockSpider,self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category or not brand:
            self.logger.error("Category or brand should not be None")
            raise CloseSpider('category or brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, self.brand)))

        self.logger.info("Crawling for category {category} and brand {brand}, url: {url}".format(category = self.category,brand=self.brand, url=self.launch_url))


    def start_requests(self):
        self.logger.info('launch_url: {}'.format(self.launch_url))
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse)


    def crawlDetail(self, response):
        """
        @url https://www.birkenstock.com/us/santa-cruz-textile/santacruz-jute-textile-0-thermorubber-u.html?dwvar_santacruz-jute-textile-0-thermorubber-u_color=2
        @meta {"use_proxy":"True"}
        @scrape_values title material price image model_sku description color shoe_width
        """

        yield {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath(u'normalize-space(//div[@class="product-modelname"]/h1/text())').extract_first(default='')
                or response.xpath(u'normalize-space(//div[@class="product-modelname"]/h2/text())').extract_first(default='') or '',
            'material': response.xpath(u'normalize-space(//div[@class="product-name-and-price"]/h2[@class="product-shortname"]/text())').extract_first(default='')
                or response.xpath(u'normalize-space(//div[@class="product-name-and-price"]/div[@class="product-shortname"]/text())').extract_first(default='') or '',
            'price': response.xpath('//span[@class="component price"]/@data-price').extract_first(default='') or '',
            'image': response.xpath('//div[@class="product-primary-image"]/a/@href').extract_first(default='') or '',
            'model_sku': response.xpath('//div[@class="product-number"]/span[@class="top-productnumber"]/@data-productnumber').extract_first(default='') or '',
            'description': response.xpath(u'normalize-space(//div[@class="content-inner"]/span/text())').extract_first(default='') or '' or
                response.xpath(u'normalize-space(//div[@class="content-inner"]/p/text())').extract_first(default='') or '',
            'color': response.xpath('//li[@data-attribute="color"]//span[@class="selection-text"]/text()').extract_first(default='') or '',
            'shoe_width': response.xpath('//li[@data-attribute="width"]//li[@class="selectable selected"]//span/text()').extract_first(default='') or '',
        }


    def parse(self, response):
        """
        @url https://www.birkenstock.com/us/shoes/women/
        @meta {"use_proxy":"True"}
        @returns requests 49
        """
        ### number of request in contract is 49 because 48 items per page and 1 next link url.

        for product in response.selector.xpath('//li[contains(@class, "xlt-producttile grid-tile")]'):

            id = product.xpath('.//a[@class="product-tile has-hover-effect"]/@href').extract_first()

            if id:
                request = self.create_request(url=id, callback=self.crawlDetail)
                yield request

        nextLink = response.xpath('//div[@class="show-more-products-placeholder"]/@data-grid-url').extract_first()
        if nextLink:
            yield self.create_request(url=nextLink, callback=self.parse)


    def create_request(self, url, callback, meta=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request

