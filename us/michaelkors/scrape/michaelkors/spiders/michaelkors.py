'''
https://mercari.atlassian.net/browse/USDE-737 - Michael Kors Women’s Watches
https://mercari.atlassian.net/browse/USDE-736 - Michael Kors Backpacks, Shoulder Bags, Tote Bags, Crossbody Bags, Satchel, Wallets
'''

import scrapy
import datetime
import time
import requests
import json
from parsel import Selector
import hashlib
from datetime import date
from scrapy import Request
import random
from collections import OrderedDict
import os
import re
from scrapy.exceptions import CloseSpider


class MichaelkorsSpider(scrapy.Spider):
    """
        spider to crawl items in a provided category from michaelkors.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Women’s Watches
        2) Backpacks
        3) Shoulder Bags
        4) Tote Bags
        5) Crossbody Bags
        6) Satchel
        7) Wallets

    brand : str
        category to be crawled, Supports
        1) Michael Kors

    Command e.g:
    scrapy crawl michaelkors -a category="Women’s Watches" -a brand='Michael Kors'

    scrapy crawl michaelkors -a category="Backpacks" -a brand='Michael Kors'
    scrapy crawl michaelkors -a category="Shoulder Bags" -a brand='Michael Kors'
    scrapy crawl michaelkors -a category="Tote Bags" -a brand='Michael Kors'
    scrapy crawl michaelkors -a category="Crossbody Bags" -a brand='Michael Kors'
    scrapy crawl michaelkors -a category="Satchel" -a brand='Michael Kors'
    scrapy crawl michaelkors -a category="Wallets" -a brand='Michael Kors'

    """

    name = 'michaelkors'

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    source = 'michaelkors'
    blob_name = 'michaelkors.txt'

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None
    filter_visit = False

    product_count = 0
    base_url = 'https://www.michaelkors.com'

    spider_project_name = "michaelkors"
    gcs_cache_file_name = 'michaelkors.tar.gz'

    url_dict = {
        "Women’s Watches": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/watches/women/_/N-28c5',
                'filters': ['Material','Group']
            }
        },
        "Backpacks": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/handbags/backpacks-belt-bags/_/N-17nzni1',
                'filters': ['Group', 'Color']
            }
        },
        "Shoulder Bags": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/handbags/shoulder-bags/_/N-283l',
                'filters': ['Group', 'Color']
            }
        },
        "Tote Bags": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/handbags/totes/_/N-283j',
                'filters': ['Group', 'Color']
            }
        },
        "Crossbody Bags": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/handbags/crossbody-bags/_/N-283m',
                'filters': ['Group', 'Color']
            }
        },
        "Satchel": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/handbags/satchels/_/N-283k',
                'filters': ['Group', 'Color']
            }
        },
        "Wallets": {
            'Michael Kors': {
                'url': 'https://www.michaelkors.com/wallets/view-all-wallets/_/N-j6rpdd',
                'filters': ['Wallet Size', 'Wallet Shape', 'Color']
            }
        },

    }
    source_url = "https://www.michaelkors.com/server/data/guidedSearch?stateIdentifier=_/{identifier}&No={current_product}&Nrpp={per_page_count}"


    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(MichaelkorsSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')
        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category, {}).get(brand, '').get('url', '')
        self.filters = self.url_dict.get(category, {}).get(brand, '').get('filters', [])

        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for  category " +category + ",url:" + self.launch_url)


    def start_requests(self):
         if self.launch_url:
            yield self.createRequest(url=self.launch_url, callback=self.parse, meta={'identifier':self.launch_url.rsplit('/', 1)[-1]})


    def parse(self, response):
        """
        @url https://www.michaelkors.com/handbags/shoulder-bags/_/N-283l
        @meta {"use_proxy": "True", "identifier": "N-283l"}
        @returns requests 1
        """

        product_count = response.xpath('//span[@class="product-count"]/text()').get(default='')  

        while( self.product_count <= int(product_count)):
            url = self.source_url.format(current_product=self.product_count, per_page_count=42, identifier=response.meta.get('identifier',''))
            self.product_count+=42
            yield self.createRequest(url=url, callback=self.parse_links, meta=response.meta)

        self.product_count = 0


    def get_filter_urls_and_values(self, refinements, filter):
        filter_urls=[]
        filter_values=[]
        for fac in refinements:
            if fac.get('name','') == filter:
                types=fac.get('refinements', [])
                for type in types:
                    filter_urls.append(type.get('stateId', ''))
                    filter_values.append(type.get('displayName', ''))

        return filter_urls, filter_values


    def parse_links(self,response):
        """
        @url https://www.michaelkors.com/server/data/guidedSearch?stateIdentifier=_/N-283l&No=0&Nrpp=42
        @meta {"use_proxy": "True"}
        @returns requests 1
        @scrape_values meta.sku meta.price meta.color meta.description
        """
        js = json.loads(response.text)
        products = js.get('result',{}).get('productList',[])
        all_links = []
        skus = []
        prices = []
        colors = []
        descriptions = []
        for product in products:
            all_links.append(product.get('seoURL',''))
            skus.append(product.get('identifier',''))
            prices.append(product.get('prices',{}).get('highSalePrice',''))
            color=product.get('variant_options',{}).get('colors',[])
            colour=[]
            for cl in color:
                if cl.get('colorName','')!='':
                    colour.append(cl.get('colorName',''))
            colour=', '.join(colour)
            colors.append(colour)
            descriptions.append(product.get('description',''))


        if self.filters and not self.filter_visit:
            refinement=js.get('facets',[])
            self.filter_visit=True
            for filter in self.filters:
                filter_urls,filter_values=self.get_filter_urls_and_values(refinement,filter)
                filter_urls=[self.base_url+"/"+link if "https" not in link else link for link in filter_urls ]
                filter_values=[val.strip() for val in filter_values]

                for url in filter_urls:
                    meta={}
                    identifier=url.split("_/")[1].split("?")[0]
                    index=filter_urls.index(url)
                    meta=meta.fromkeys([filter],filter_values[index])
                    meta.update({'filter_name':filter})
                    meta.update({'identifier':identifier})
                    response.meta.update(meta)
                    yield self.createRequest(url=url,callback=self.parse,meta=response.meta)

        all_links=[self.base_url+link if "https" not in link else link for link in all_links ]
        for link in all_links:
            meta={}
            index=all_links.index(link)
            meta['sku']=skus[index]
            meta['price']=prices[index]
            meta['color']=colors[index]
            meta['description']=descriptions[index]
            response.meta.update(meta)
            yield self.createRequest(url=link,callback=self.parse_product,meta=response.meta)


    def parse_product(self,response):
        """
        @url https://www.michaelkors.com/bradshaw-small-studded-convertible-shoulder-bag/_/R-US_30T1L2BL1U
        @meta {"use_proxy": "True"}
        @scrape_values title image details
        """
        item={}
        item["crawlDate"] = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["statusCode"] = str(response.status)
        item['id'] = response.url
        item['image'] = response.xpath('//meta[@property="og:image"]/@content').get(default='')
        item['brand'] = self.brand
        item['category'] = self.category
        item['model_sku'] = response.meta.get('sku','')
        item['title'] = response.xpath('//meta[@name="og:title"]/@content').get(default='')
        item['price'] = response.meta.get('price','')
        item['color'] = response.meta.get('color','')
        item['description'] = response.meta.get('description','')
        script_data = response.xpath('//*[@id="en_US"]/script[contains(text(), "richTextDescription")]/text()').get()
        script_data = script_data.split('window.__INITIAL_STATE__ = ')[1].split(';\n                  /* </sl:translate_json> */\n                ')[0]
        js = json.loads(script_data)

        item['details'] = js.get('pdp',{}).get('rawJson',{}).get('richTextDescription','')
        meta={}
        filter_name = response.meta.get('filter_name', '')
        if filter_name:
            meta = meta.fromkeys([filter_name], response.meta[filter_name])
            item.update(meta)

        yield item


    def createRequest(self, url, callback,meta=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if meta:
            request.meta.update(meta)

        request.meta.update({"use_cache":True})
        return request
