import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            title = record.get('title', '').replace('\r', '')
            brand = record.get('brand', '')
            material = record.get('Material', '')
            details_info = record.get('details', '')
            category = record.get('category', '')

            material_meta = [
                'Pebbled Leather', 'Calf Leather', 'Patent Leather', 'Leather',
                'Coated Canvas', 'Canvas',
                'Polyester', 'Polyurethane', 'Nylon', 'Linen', 'Cotton',
                'Coated Twill', 'suede', 'Snakeskin', 'River Snake',
            ]
            if not material:
                match = re.findall(re.compile(r'|'.join([r'\b'+x+r'\b' for x in material_meta]), re.IGNORECASE), ' '.join(list(filter(lambda x: 'Lining' not in x, re.split(r'<br>|<br\s*/>', details_info)))))
                if match:
                    material = ', '.join(match)
            if not material:
                match = re.findall(re.compile(r'|'.join([r'\b'+x+r'\b' for x in material_meta]), re.IGNORECASE), title.split('|')[0])
                if match:
                    material = ', '.join(match)
            material = re.sub(re.compile(r'River Snake', re.IGNORECASE), 'Snakeskin', material).strip()
            material = re.sub(r'\s+', ' ', material).strip()
            material = material.title()
            material = ', '.join(list(dict.fromkeys(material.split(', '))))

            color_meta_map = {
                'BRN': 'Brown',
                'Multi': 'Multicolor',
                'MLTI': 'Multicolor',
                'COMBO': 'Multicolor',
                'MLT': 'Multicolor',
                'BLK': 'Black',
                'DK': 'Dark',
                'LVNDR': 'Lavender',
                'SFTPINK': 'Soft Pink',
                'SFT': 'Soft',
                'PNKGRPFT MLT': 'Pink Grapefruit Multicolor',
                'BRT': 'Burnt',
                'LT': 'Light',
                'CRM': 'Cream',
                'PLBL': 'Pale Blue',
                'PLBLUE': 'Pale Blue',
                'PBLUE': 'Pale Blue',
                'WHT': 'White',
                'BLU': 'Blue',
                'GRN': 'Green',
                'BUTRNT': 'Butternut',
                'BUTTRNT': 'Butternut',
                'ADMRL': 'Admiral',
                'NVY': 'Navy',
                'PGRY': 'Pale Grey',
                'GRY': 'Grey',
                'SFP': 'Soft Pink',
                'LTCR': 'Light Cream',
                'FWN': 'Fawn',
                'CHBRY': 'Chambray',
                'TERRACTTA': 'Terracotta',
                'OPWHT': 'Optic White',
                'LUGG': 'Luggage',
                'ADMIRL': 'Admiral',
                'ACRN': 'Acorn',
            }
            colors = []
            for color in record.get('color', '').split(', '):
                color = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b'+x+r'\b', color_meta_map.keys()))), re.IGNORECASE), lambda x: color_meta_map.get(x.group(), x.group()), color)
                color = '/'.join(list(dict.fromkeys(color.split('/'))))
                if len(color.split('/')) > 2:
                    color = color.split('/', 1)[0] + '/Multicolor'
                color = re.sub(re.compile(r'\bMulti\b', re.IGNORECASE), 'Multicolor', color)
                color = re.sub(r'\s+', ' ', color).strip()
                color = color.title()
                colors.append(color)
            color = ', '.join(colors)

            if color == 'Hemp':
                color = ''

            lining_material = ''
            match = re.findall(re.compile(r'lining:(.*?)(\n|<br.*?>|$)', re.IGNORECASE), details_info)
            if match:
                lining_material = re.sub(r'\d+%', '', match[0][0])
                lining_material = re.sub(r'\s+', ' ', lining_material).strip()
                lining_material = lining_material.title()

            length = ''
            match = re.findall(re.compile(r'(\d+\.*\d*"\s*)[LW]', re.IGNORECASE), details_info)
            if match:
                length = match[0].strip()

            height = ''
            match = re.findall(re.compile(r'(\d+\.*\d*"\s*)H', re.IGNORECASE), details_info)
            if match:
                height = match[0].strip()

            depth = ''
            match = re.findall(re.compile(r'(\d+\.*\d*"\s*)D', re.IGNORECASE), details_info)
            if match:
                depth = match[0].strip()

            model = title.split('|')[0]
            remove_words_from_model = [
                brand,
                'backpack', 'shoulder bag', 'tote', 'crossbody', 'satchel', 'wallet', 'wristlet', 'clutch', 'belt bag', 'messenger',
                'bag', 'size', 'mini', 'small', 'medium', 'large', 'extra-small', 'extra small', 'extra-large', 'extra large',
                'Two-Tone', 'Tri-Color', 'Tri-Tone', 'Quilted'
            ]
            remove_words_from_model.extend(material_meta)
            remove_words_from_model.extend(material.split(', '))
            model = re.sub(re.compile(r'|'.join(list(map(lambda x: r'\b{}\b'.format(x), remove_words_from_model))), re.IGNORECASE), '', model)
            model = re.sub(re.compile(r'\sand\s*$|-\s*$', re.IGNORECASE), '', model)
            if re.findall(re.compile(r'^\s*Color-Block\b', re.IGNORECASE), model):
                model = re.sub(re.compile(r'^\s*Color-Block Crossgrain\b', re.IGNORECASE), 'Crossgrain Color-Block', model)
            model = re.sub(r'\s+', ' ', model).strip()

            d = {
                'Backpacks': ['Backpack'],
                'Shoulder Bags': ['Shoulder Bag'],
                'Tote Bags': ['Tote Bag', 'Tote'],
                'Crossbody Bags': ['Crossbody Bag'],
                'Satchel': ['Satchel'],
                'Wallets': [],
            }
            match = d.get(category, '')
            if not model or \
                (match and not re.findall(re.compile(r'|'.join(list(map(lambda x: r'\b{}\b'.format(x), match))), re.IGNORECASE), title)):
                # print(title)
                return None

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate','').split(', ')[0],
                "status_code": record.get('statusCode',''),
                "category": category,
                "brand": brand,

                "title": title,
                "image": record.get('image',''),
                "ner_query": ner_query.strip().replace('\n', ''),

                "model": model,
                "description": description.strip().replace('\n', ''),
                "msrp": {
                    "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "model_sku": record.get('model_sku',''),
                "color": color,
                "material": material,
                "product_line": record.get('Group', ''),

                'lining_material': lining_material,
                'length': length,
                'height': height,
                'depth': depth,

                'size': record.get('Wallet Size', ''),
                'style': record.get('Wallet Shape', ''),

            }

            return transformed_record
        else:
            return None
