class Mapper:
    def map(self, record):
        model = ''
        color = ''
        material = ''
        
        entities = record['entities']
        for entity in entities:
            if "model" == entity['name']:
                if entity['attribute']:
                    model = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                for attribute in entity['attribute']:
                    color += attribute["id"]
            elif "material" == entity['name']:
                for attribute in entity['attribute']:
                    material += attribute["id"]

        key_field = model + color + material
        return key_field