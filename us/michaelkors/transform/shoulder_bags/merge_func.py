
class Merger:
    def merge(self, record1, record2):

        for key, value in record2.items():
            if value and (key not in record1 or not record1[key]):
                record1[key] = value

        record1['id'] = record2.get('id', '')
        record1['item_id'] = record2.get('itemId', '')

        return record1
