import re
import hashlib

class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            ner_query = description 
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()
            material=record.get('Material','')
            model=record.get('title','')
            material_meta=['Stainless Steel','Aluminum','Acetate','Ceramic','Leather','Metal','Pavé','Silicone','Tortoise & Horn']


            metal_color=''
            pattern=re.compile(r"\w+\s*\-\s*tone",re.IGNORECASE)
            if re.findall(pattern,model)!=[]:
                metal_color=re.findall(pattern,model)[0]

            pattern=re.compile(r"watche*s*|women\'*s*|\-\s*tone|\| Michael Kors",re.IGNORECASE)
            model=re.sub(pattern,'',model)

            for word in re.findall(r"\w+",record.get('color','')):
                pattern=re.compile(r"\b{}\b".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            model=model.replace("-"," ")
            model=' '.join(model.split())
            

        
            
            for word in metal_color.split("-"):
                pattern=re.compile(r"{}".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            model=' '.join(model.split())                    
                
            
            
            details=record.get('details','')
            pattern=re.compile(r'<[^>]+>')
            details=re.sub(pattern,'',details)
            details=details.replace('\n','').replace("•","")

            
            if material=='':
                material=[]
                for mat in material_meta:
                    pattern=re.compile(r"{}".format(mat),re.IGNORECASE)
                    if re.findall(pattern,details)!=[]:
                        material.append(mat)
                material=', '.join(material)
            
            
            for word in material.split(", "):
                pattern=re.compile(r"\b{}\b".format(word),re.IGNORECASE)
                model=re.sub(pattern,'',model)
            
            

            pattern=re.compile(r"\d+\.*\d*\w+\s*case",re.IGNORECASE)
            case=''
            if re.findall(pattern,details)!=[]:
                case=re.findall(pattern,details)[0]
                case=re.sub(re.compile(r'\s*Case',re.IGNORECASE),'',case)

           
            model_words=re.findall(r"\w+",model)
            if re.findall(re.compile(r"\band\b",re.IGNORECASE),model_words[len(model_words)-1])!=[]:
                model_words=model_words[:-1]
            model=' '.join(model_words)

            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawlDate',''),
                "status_code": record.get('statusCode',''),
                "category": "Women's Watches",
                "description": description.strip().replace('\n',''),
                "title": record.get('title',''),
                "model":model,
                "brand": record.get('brand',''),
                "image": record.get('image',''),
                "ner_query": ner_query.strip().replace('\n',''),
                "msrp":{
                     "currency_code": "USD",
                    "amount": record.get("price","").strip('$').replace(',','')
                },
                "model_sku" : record.get('model_sku',''),
                "color":metal_color,
                "material":material,
                "product_line":record.get('Group',''),
                "case":case
                
                
                }

            return transformed_record
        else:
            return None
