class Mapper:
    def map(self, record):

        product_type = ''
        color = ''
        headphone_jack = ''

        entities = record['entities']
        for entity in entities:
            if "product_type" == entity['name']:
                product_type = entity["attribute"][0]["id"]
            elif "color" == entity['name']:
                color = entity["attribute"][0]["id"]
            elif "headphone_jack" == entity['name']:
                headphone_jack = entity["attribute"][0]["id"]

        key_field = product_type + color + headphone_jack
        return key_field
