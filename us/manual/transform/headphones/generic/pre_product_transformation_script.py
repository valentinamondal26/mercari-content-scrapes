'''
https://mercari.atlassian.net/browse/USCC-529 - Custom Tree - Generic Headphones
'''

import hashlib

class Mapper(object):

    def map(self, record):
        if record:
            category = record.get('system_category', '')
            brand = record.get('Brand', '')

            product_type = record.get('product_type', '')
            color = record.get('color', '')
            headphone_jack = record.get('headphone_jack', '')

            id = record.get('id', '')
            if not id:
                id = f'https://www.manual.com/{category}/{brand}/{product_type}/{color}/{headphone_jack}'
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            transformed_record = {
                "id": id,
                "item_id": hex_dig,
                "crawlDate": record.get('crawlDate', ''),
                "statusCode": record.get('statusCode', '200'),

                "category": category,
                "brand": brand,

                "product_type": product_type,
                "color": color,
                'headphone_jack': headphone_jack,
            }

            return transformed_record
        else:
            return None
