#Key Mapper for bigbadtoystore data - manufacturer + model  combination is used to de-dup items
class Mapper:
    def map(self, record):
        model = ''
        manufacturer = ''

        entities = record['entities']
        for entity in entities:
            if "manufacturer" == entity['name'] and entity["attribute"]:
                manufacturer = entity["attribute"][0]["id"]
            elif "model" == entity["name"] and entity["attribute"]:
                model = entity["attribute"][0]["id"]

        key_field = manufacturer + model
        return key_field
