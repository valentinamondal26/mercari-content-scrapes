import re
import hashlib
from titlecase import titlecase


class Mapper(object):

    def map(self, record):
        if record:

            description = record.get('description', '')
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            model = re.sub(r'\||Two-Packs|Two-Pack|Exclusive|BigBadToyStore|BBTS|With Bonus', '', model, flags=re.IGNORECASE)
            model = re.sub(r'\s-\s', ' ', model)
            words_to_transform = ['The Last Jedi', 'A New Hope', 'Attack of the Clones', 'Forces of Destiny',
                     'The Mandalorian', 'The Force Awakens', 'Return of the Jedi', 'Clone Wars',
                     'Phantom Menace', 'Empire Strikes Back']
            def remove_unnecessary_brackets(x):
                if not re.findall(r'|'.join(words_to_transform), x.group(), flags=re.IGNORECASE):
                    return re.sub(r'\(|\)', '', x.group())
                return x.group()
            model = re.sub(r'\(.*?\)', lambda x: remove_unnecessary_brackets(x), model)
            def model_trans(x):
                if re.findall(r'|'.join(words_to_transform), x.group(4), flags=re.IGNORECASE):
                    match = re.findall(r'|'.join(
                        ['40th Anniversary The Black Series', 'The Black Series', 'The Vintage Collection', 'S.H.Figuarts', 'Force Link 2.0']
                        ), x.group(3).strip(), flags=re.IGNORECASE)
                    if match:
                        modified = re.sub(match[0], f'{match[0]} {x.group(4)}', x.group(3), flags=re.IGNORECASE)
                        return f'{x.group(1)} {x.group(2)} {modified} {x.group(5)}'
                    return f'{x.group(1)} {x.group(2)} {x.group(4)} {x.group(3)} {x.group(5)}'
                return x.group()
            model = re.sub(r'(.*)(Star\s*\-*wars*)(.*)\((.*)\)(.*)', lambda x: model_trans(x), model, flags=re.IGNORECASE)
            model = re.sub(r'(Star\s*\-*wars*)\s*\:', r'\1', model, flags=re.IGNORECASE)
            model = re.sub(r'(.*)(Figures|Figure)(.*)', r'\1 \3 \2', model, flags=re.IGNORECASE)
            model = re.sub(r'Deluxe Ver\.', 'Deluxe', model, flags=re.IGNORECASE)
            model = re.sub(r'\s+', ' ', model).strip()
            model = re.sub('Jaina Solo Jaina Solo', 'Jaina Solo', model, flags=re.IGNORECASE)
            # model = titlecase(model)

            if record.get("Company", "") == "Funko" \
                or re.findall(re.compile(r"Plush", re.IGNORECASE),
                              record.get("title", "")):
                return None
            transformed_record = {
                "id": record['id'],
                "item_id": hex_dig,
                "crawl_date": record.get('crawl_date', ''),
                "status_code": record.get('status_code', ''),
                "category": record.get("category"),
                "breadcrumb": record.get('breadcrumb', ''),
                "description": ", ".join(description).replace('\n', ''),
                "title": record.get('title', ''),
                "model": model,
                "brand": record.get('brand', ''),
                "image": record.get('image', ''),
                "product_features": ", ".join(
                    record.get("product_features", "")).replace("\n", ""),
                "ner_query": ", ".join(description).replace('\n', ''),
                "price": {
                    "currency_code": "USD",
                    "amount": record.get("price", "").strip('$').replace(',', '')
                },
                "manufacturer": record.get("Company", ""),
                "product_type": "Figures",
                "series": record.get("Series", ""),
                "scale": record.get("Scale", ""),
                "size": record.get("Size", "")

            }

            return transformed_record
        else:
            return None
