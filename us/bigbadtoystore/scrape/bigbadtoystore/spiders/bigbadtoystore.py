'''
https://mercari.atlassian.net/browse/USCC-289 - Star Wars Action Figures
'''

import scrapy
import datetime
import random
import os
import re
from scrapy.exceptions import CloseSpider


class BigbadtoystoreSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from bigbadtoystore.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) Action Figures

    brand : str
        category to be crawled, Supports
        1) Star Wars

    Command e.g:
    scrapy crawl bigbadtoystore -a category="Action Figures" -a brand='Star Wars'

    """

    name = 'bigbadtoystore'
    start_urls = [
        'https://www.bigbadtoystore.com'
    ]

    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    category = None
    brand = None

    merge_key = 'id'

    launch_url = None
    filters = None
    number_of_products = None
    source = 'bigbadtoystore.com'
    blob_name = 'bigbadtoystore.txt'
    base_url = 'https://www.bigbadtoystore.com'

    url_dict = {
        "Action Figures": {
            'Star Wars': {
                "url": 'https://www.bigbadtoystore.com/Search?HideInStock=false&HidePreorder=false&HideSoldOut=false&InventoryStatus=i%2Cp%2Cso&PageIndex=1&PageSize=20&SortOrder=BestSelling&ProductType=321&Department=7399&Brand=2308',
                'filters': ['Product Type', 'Series', 'Company', 'Scale', 'Size']
            }
        }
    }

    def __init__(self, category=None, brand=None, *args, **kwargs):
        super(BigbadtoystoreSpider, self).__init__(*args, **kwargs)

        if os.environ.get('SCRAPY_CHECK',''):
            return

        if not category and not brand:
            self.logger.error("category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand
        self.filters = self.url_dict.get(category, {}).get(brand, {}).get('filters', '')
        self.launch_url = self.url_dict.get(category, {}).get(brand, {}).get('url', '')
        if not self.launch_url:
            self.logger.error('Spider is not supported for  category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for  category:{} and brand:{}'.format(category, brand)))
        self.logger.info("Crawling for  category " + category + ",url:" + self.launch_url)


    def start_requests(self):
        if self.launch_url:
            if self.filters:
                yield self.createRequest(url=self.launch_url, callback=self.parse_filters)
            else:
                yield self.createRequest(url=self.launch_url, callback=self.parse_filters)


    def parse_filters(self, response):
        """
        @url https://www.bigbadtoystore.com/Search?HideInStock=false&HidePreorder=false&HideSoldOut=false&InventoryStatus=i%2Cp%2Cso&PageIndex=1&PageSize=20&SortOrder=BestSelling&ProductType=321&Department=7399&Brand=2308
        @returns requests 2
        @meta {"use_proxy": "True"}
        """

        if self.filters:
            for filter in self.filters:
                filter_urls = response.xpath(
                    '//div[span[contains(text(),"{}")]]/ul//li//a/@href'.format(
                        filter)).getall()
                filter_urls = [
                    self.base_url+link if "https" not in link else link
                    for link in filter_urls]

                filter_values = response.xpath(
                    '//div[span[contains(text(),"{}")]]/ul//li/@title'.format(
                        filter)).getall()
                filter_values = [
                    re.sub(r"\(\s*\d+\s*\)", "", v).strip()
                    for v in filter_values]
                for url in filter_urls:
                    meta = {}
                    index = filter_urls.index(url)
                    meta = meta.fromkeys([filter], filter_values[index])
                    meta.update({'filter_name': filter})
                    yield self.createRequest(url=url, callback=self.parse,
                                             meta=meta)

        yield self.createRequest(url=response.url, callback=self.parse,
                                 meta=response.meta)

    def parse(self, response):
        all_links = response.xpath(
            '//a[span[@class="product-name"]]/@href').getall()
        all_links = [self.base_url +
                     link if "https" not in link else link for link in all_links]
        for link in all_links:
            yield self.createRequest(url=link, callback=self.parse_product,
                                     meta=response.meta)
        next_page_url = response.xpath(
            '//li[@class="PagedList-skipToNext"]/a/@href').get(default='')
        if next_page_url:
            if "https" not in next_page_url:
                next_page_url = self.base_url + next_page_url
                yield self.createRequest(url=next_page_url, callback=self.parse,
                                         meta=response.meta)

    def parse_product(self, response):
        item = {}
        item["crawl_date"] = str(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        item["status_code"] = str(response.status)
        item['id'] = response.url
        item['image'] = response.xpath(
            '//meta[@property="og:image"]/@content').get(default='')
        item['brand'] = self.brand
        item['category'] = self.category
        item['title'] = response.xpath(
            '//meta[@property="og:title"]/@content').get(default='')
        item['price'] = response.xpath(
            '//div[@class="price"]/text()').get(default='')
        description = response.xpath(
            '//div[@class="description-block"]//div[h4[strong[contains(text(),"Description")]]]//text()').getall()
        item['description'] = [d for d in description if not re.compile(
            r"^[\r|\n|\t|\s]+$|^Product Description$").match(d)]
        product_features = response.xpath('//div[@class="description-block"]//div[h4[strong[contains(text(),"Contents")]]]//text()').getall(
        ) + response.xpath('//div[@class="description-block"]//div[h4[strong[contains(text(),"Features")]]]//text()').getall()
        item['product_features'] = [d for d in description if not re.compile(
            r"^[\r|\n|\t|\s]+$|^Product Description$").match(d)]

        meta = {}
        filter_name = response.meta.get('filter_name', '')
        if filter_name:
            meta = meta.fromkeys([filter_name], response.meta[filter_name])
            item.update(meta)
        yield item

    def createRequest(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)
        if headers:
            for key, value in headers.items():
                request.headers[key] = value
        if meta:
            for key, value in meta.items():
                request.meta[key] = value

        request.meta.update({"use_cache":True})
        return request
