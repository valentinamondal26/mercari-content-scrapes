'''
https://mercari.atlassian.net/browse/USDE-1717 - G-Shock Men's Wristwatches
https://mercari.atlassian.net/browse/USDE-1718 - G-Shock Women's Wristwatches
'''

import scrapy
import json
import random
from datetime import datetime
from scrapy.exceptions import CloseSpider
import os


class GshockSpider(scrapy.Spider):
    """
    spider to crawl items in a provided category from gshock.com

    Attributes
    ----------
    category : str
        category to be crawled, Supports
        1) All watches

    sub_category : str
        brand to be crawled, Supports
        1) G-shock


    'Men"s Wristwatches': 'https://www.gshock.com/watches/men',
    'Women"s Wristwatches': 'https://www.gshock.com/watches/women',


    scrapy crawl gshock -a category='All Watches' -a brand='G-Shock'
    """

    name = "gshock"

    category = None
    brand = None

    merge_key = 'id'

    source = 'gshock.com'
    blob_name = 'gshock.txt'
    base_url = 'https://www.gshock.com/watches'


    proxy_pool = [
        'http://shp-mercari-us-d00001.tp-ns.com:80',
        'http://shp-mercari-us-d00002.tp-ns.com:80',
        'http://shp-mercari-us-d00003.tp-ns.com:80',
        'http://shp-mercari-us-d00004.tp-ns.com:80',
        'http://shp-mercari-us-d00005.tp-ns.com:80',
    ]

    url_dict = {
        'All Watches': {
            'G-Shock': {
                'url': 'https://www.gshock.com/assets/products.json?v=1598515058295',
            }
        }
    }

    '''
    NOTE: url in the url_dict is an api and will have the list of all products available in the gshock.
    Men's Wristwatches and Women's Wristwatches don't have an seperate apis
    and html crawling is also not possible since the pages is loading the
    data dynamically by scripts, as its difficult to handle infinite scroll page.
    This url(https://www.gshock.com/assets/products.json?v=1598515058295) will have
    all the products for both men's and women's watches.
    We can seggregate the men's watches and women's watches in the transformation phase.
    This api has the tags for individual products we can match the tags with
    appropriate filter name in the transformation phase..
    '''


    def __init__(self, category=None, brand=None, *args, **kwargs):
        if os.environ.get('SCRAPY_CHECK',''):
            return
        super(GshockSpider,self).__init__(*args, **kwargs)

        if not category or not brand:
            self.logger.error("Category and brand should not be None")
            raise CloseSpider('category and brand not found')

        self.category = category
        self.brand = brand

        self.launch_url = self.url_dict.get(category,{}).get(brand,{}).get('url','')
        if not self.launch_url:
            self.logger.error('Spider is not supported for the category:{} and brand:{}'.format(category, brand))
            raise(CloseSpider('Spider is not supported for the category:{} and brand:{}'.format(category, brand)))

        self.logger.info("Crawling for category "+category+" and brand "+brand)


    def start_requests(self):
        if self.launch_url:
            yield self.create_request(url=self.launch_url, callback=self.parse_listing_page_url)


    def parse_listing_page_url(self, response):
        """
        @url https://www.gshock.com/assets/products.json?v=1598515058295
        @meta {"use_proxy": "True"}
        @returns requests 1
        """

        response_text = json.loads(response.text)
        total_products = response_text.get('products',[])
        for product in total_products:
            meta = {
                'tags': product['tags']
            }
            url = self.base_url+ product.get('url_media','')
            if url!=self.base_url:
                yield self.create_request(url=url, callback=self.crawldetails, meta=meta)


    def crawldetails(self, response):
        print(response.url)
        product_info = response.xpath("//script[@type='application/ld+json']/text()").getall()[0].replace('\n','')
        product_info = json.loads(product_info)
        item = {
            'crawlDate': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'statusCode': str(response.status),
            'category': self.category,
            'brand': self.brand,
            'id': response.url,

            'title': response.xpath("//meta[@property='og:title']/@content").get(default=''),
            'tags': response.meta.get('tags',[]),
            'product_info': product_info,
            'sku': response.xpath("//h1[@class='name heading--2 line-left']/text()").get(default=''),
            'price': response.xpath("//meta[@property='product:price:amount']/@content").get(default='').strip(),
            'breadcrumb': " | ".join(response.xpath("//div[@id='breadCrumbGroup']/a/text()").getall()),
            'image': response.xpath("//div[@class='main-img main-img-dropshadow']/div/img/@src").get(default=''),
            'description':   response.xpath("//div[@class='watch-info']/p/text()").getall(),
            'features_list': response.xpath("//ul[@class='features__list row']/li/div[@class='features__info']/div/text()").getall(),
        }
        meta = {'item': item}
        headers = {}
        headers.update({'x-requested-with': 'XMLHttpRequest'})
        specs_url = (response.url+"?fancybox=true")
        specs_url = specs_url.replace('/watches/','/watches/specs/')##this is because full specs are displayed as an image in item_detail_page so we are unable to scrape specs in the detail page.
        yield self.create_request(url=specs_url, callback=self.get_specs_data,meta=meta,headers=headers)


    def get_specs_data(self, response):
        meta = response.meta
        item = meta.get('item',{})
        specs_data =  response.xpath("//div/ul/li/text()").getall()
        item.update({'specs_data':specs_data})
        yield item


    def create_request(self, url, callback, meta=None, headers=None):
        request = scrapy.Request(url=url, callback=callback, meta=meta, headers=headers)
        if self.proxy_pool:
            request.meta['proxy'] = random.choice(self.proxy_pool)

        request.meta.update({"use_cache":True})
        return request
