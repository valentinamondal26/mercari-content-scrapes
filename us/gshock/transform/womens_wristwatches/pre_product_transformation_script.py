import hashlib
import re
from colour import Color


class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            title = record.get('title', '')

            description = record.get('description', '')
            description = ' '.join([des.strip()
                                    for des in description]).replace('\n', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            product_tags = record.get('tags', [])
            filters_dict = {
                'Color': {
                    'cwhi': 'White', 'cgra': 'Gray', 'cbla': 'Black', 'cblu': 'Blue', 'cpur': 'Purple',
                    'cyel': 'Yellow', 'cora': 'Orange', 'cred': 'Red', 'cgre': 'Green',
                },
                'Type': {
                    'extr': 'Extreme Conditions', 'spor': 'Sports', 'form': 'Formal', 'inte': 'Interchangeable Bands', 'limi': 'Limited Edition', 'mud-': 'Mud Resistant', 'digi': 'Digital', 'anal':  'Analog', 'wome': 'Women', 'men': 'Men', 'wate': 'Water Resistant',
                },
                'Collections': {
                    'baby': 'Baby-G', 'g-ms': 'G-MS', 'g-sh': 'G-SHOCK Women', 'g-st': 'G-Steel', 'mast': 'Master Of G', 'mr-g': 'MR-G', 'mt-g': 'MT-G'
                },
                'Material': {
                    'mine': 'Mineral Glass', 'sapp': 'Sapphire Crystal', 'ion-': 'Ion Plated', 'tita': 'titanium', 'carb': 'Carbon',
                    'stai': 'Stainless Steel', 'clot': 'Cloth', 'resi': 'Resin'
                },
                'Function': {
                    'alti': 'Altimeter', 'baro': 'Barometer', 'blue': 'Bluetooth-Connected', 'gps': 'GPS', 'sola': 'Solar-Powered',
                    'step': 'Step-Tracker', 'ther': 'Thermometer', 'tide': 'Tide Graph', 'mult': 'Multiband / Atomic Timekeeping'
                }
            }

            filter_dict_keys = list(filters_dict.keys())
            filter_data = {}
            for tag in product_tags:
                for filter_name in filter_dict_keys:
                    if tag in list(filters_dict[filter_name].keys()):
                        value = filters_dict[filter_name][tag]
                        if filter_name in filter_data:
                            filter_data[filter_name].append(value)
                        else:
                            filter_data.update({filter_name: [value]})

            color_value = []
            for title_split in title.split():
                if '/' in title_split:
                    split_data = title_split.split('/')
                    for col_data in split_data:
                        try:
                            Color(col_data)
                            color_value.append(col_data)
                        except ValueError:
                            if 'Clear' in col_data.title() or 'Multi' in col_data.title():
                                color_value.append(col_data)
                else:
                    try:
                        Color(title_split)
                        color_value.append(title_split)
                    except ValueError:
                        pass
            color = '/'.join(color_value)
            color_in_title = '/'.join(color_value)

            color_in_item_page = record.get(
                'product_info', {}).get('color', '')

            filter_color = ''
            if not color:
                color = '/'.join(filter_data.get('Color', []))
                filter_color = color

            color_in_description = ''
            if not color:
                for desc in description.split():
                    desc = re.findall(r'\w+', desc)
                    if desc:
                        try:
                            Color(desc[0])
                            color_value.append(desc[0])
                        except ValueError:
                            pass
                color = '/'.join(color_value)
                color_in_description = '/'.join(color_value)
            
            manual_color = ''
            if re.findall(r'^Multi$', color_in_item_page, flags=re.IGNORECASE):
                additional_color = filter_color or color_in_title or color_in_description
                manual_color = f'{additional_color}/Multicolor'
            elif re.findall(r'^Clear$', color_in_item_page, flags=re.IGNORECASE):
                additional_color = filter_color or color_in_title or color_in_description
                manual_color = f'{additional_color}/Clear'
            elif re.findall(r'\bClear\b', color_in_item_page, flags=re.IGNORECASE):
                manual_color = re.sub(r'(Clear)\s*\-*\s*(\w+\s*\w*\s*\w*)', r'\2/\1', color_in_item_page, flags=re.IGNORECASE)

            model = re.sub(r'\b{color}\b'.format(color=color), '', model)
            model = re.sub(re.compile(
                r"\bWomen's\b|\bWomen\b", re.IGNORECASE), '', model)
            sku = record.get('sku', '')
            model = re.sub(sku, '', model)

            replace_string = f'G-SHOCK {sku}'
            model = model.replace('G-SHOCK', replace_string, 1)
            model = re.sub(r'\s+', ' ', model)
            model = re.sub(re.compile(r'\b{color}\b|\&'.format(
                color=color), re.IGNORECASE), '', model)
            for col in color.split('/'):
                model = re.sub(
                    r'\b{color}\b'.format(color=col), '', model)

            model = re.sub(re.compile(r'\b{}\b'.format(
                record.get('brand', '')), re.IGNORECASE), '', model)
            model = re.sub(re.compile(
                r'\bWatches\b', re.IGNORECASE), 'Watch', model)
            if not re.findall(r'^{}'.format(sku), model):
                model_split = model.split('|')
                if len(model_split) == 3:
                    if model_split[1].strip():
                        model = sku + ' '+model_split[1]+' '+model_split[0]
                    else:
                        if sku in model_split[0]:
                            model = sku+' '+model_split[0].replace(sku, '')
                        else:
                            model = sku+' '+model_split[0]

            model = re.sub(r'\||/', '', model)
            model = re.sub(r'\s-|-\s|-$', ' ', model)
            model = re.sub(r'\s,|,\s|,$', ' ', model)
            model = re.sub(re.compile(r'\bRose\b', re.IGNORECASE), ' ', model)
            if re.findall(r'\bRose\b', title, flags=re.IGNORECASE):
                color = re.sub(re.compile(
                        r'\bgold\b', re.IGNORECASE), 'Rose Gold', color)

            color = re.sub(re.compile(
                    r'Navy/Blue', re.IGNORECASE), 'Navy Blue', color)

            model = re.sub(r'\s+', ' ', model).strip()

            try:
                water_resistant = re.findall(re.compile(
                    r'(\d+\s*-*[Meter|M]*) Water Resistan', re.IGNORECASE), ', '.join(record['specs_data']))[0]
            except IndexError:
                water_resistant = re.findall(re.compile(
                    r'(\d+\s*-*[Meter|M]*) Water Resistan', re.IGNORECASE), ', '.join(record['features_list']))
                if water_resistant:
                    water_resistant = water_resistant[0]
                else:
                    water_resistant = ''
            water_resistant = re.sub(
                r'-meter|\s*Meter', 'm', water_resistant, flags=re.IGNORECASE).lower().strip()

            try:
                battery_life = re.findall(
                    r'Approx\. battery life:\s*(\d+\s+years)', ', '.join(record['specs_data']))[0]
            except IndexError:
                battery_life = ''

            category = ''
            if 'wome' in product_tags:
                category = "Women's Wristwatches"

            if 'men' in product_tags or record['id'] == 'https://www.gshock.com/watches':
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'brand': record.get('brand', ''),
                'category': category,

                'title': record.get('title', ''),
                'image': record.get('image', ''),
                'description': description,

                'model': model,
                'color': manual_color.title() or color_in_item_page.title() or filter_color.title() or color_in_title.title() or color_in_description.title(),

                'material': ', '.join(filter_data.get('Material', [])),
                'function': ', '.join(filter_data.get('Function', [])),
                'product_line': ', '.join(filter_data.get('Collections', [])),
                'water_resistance': water_resistant,
                'model_sku': record.get('sku', ''),
                'battery_life': battery_life,
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', '')
                },
            }
            return transformed_record

        else:
            return None
