import hashlib
import re
from colour import Color

class Mapper(object):
    def map(self, record):
        if record:
            id = record['id']
            b = id.encode('utf-8')
            hash_object = hashlib.sha1(b)
            hex_dig = hash_object.hexdigest()

            model = record.get('title', '')
            title = record.get('title', '')
            color_value = []
            for title_split in title.split():
                if '/' in title_split:
                    split_data = title_split.split('/')
                    for col_data in split_data:
                        try:
                            Color(col_data)
                            color_value.append(col_data)
                        except ValueError:
                            if 'Clear' in col_data.title() or 'Multi' in col_data.title():
                                color_value.append(col_data)
                else:
                    try:
                        Color(title_split)
                        color_value.append(title_split)
                    except ValueError:
                        pass

            color = ', '.join(color_value)
            product_tags = record.get('tags', [])
            filters_dict = {
                'Color': {
                    'cwhi': 'White', 'cgra': 'Gray', 'cbla': 'Black', 'cblu': 'Blue', 'cpur': 'Purple',
                    'cyel': 'Yellow', 'cora': 'Orange', 'cred': 'Red', 'cgre': 'Green',
                },
                'Type': {
                    'extr': 'Extreme Conditions', 'spor': 'Sports', 'form': 'Formal', 'inte': 'Interchangeable Bands', 'limi': 'Limited Edition', 'mud-': 'Mud Resistant', 'digi': 'Digital', 'anal':  'Analog', 'wome': 'Women', 'men': 'Men', 'wate': 'Water Resistant',
                },
                'Collections': {
                    'baby': 'Baby-G', 'g-ms': 'G-MS', 'g-sh': 'G-SHOCK Women', 'g-st': 'G-Steel', 'mast': 'Master Of G', 'mr-g': 'MR-G', 'mt-g': 'MT-G'
                },
                'Material': {
                    'mine': 'Mineral Glass', 'sapp': 'Sapphire Crystal', 'ion-': 'Ion Plated', 'tita': 'titanium', 'carb': 'Carbon',
                    'stai': 'Stainless Steel', 'clot': 'Cloth', 'resi': 'Resin'
                },
                'Function': {
                    'alti': 'Altimeter', 'baro': 'Barometer', 'blue': 'Bluetooth-Connected', 'gps': 'GPS', 'sola': 'Solar-Powered',
                    'step': 'Step-Tracker', 'ther': 'Thermometer', 'tide': 'Tide Graph', 'mult': 'Multiband / Atomic Timekeeping'
                }
            }

            filter_dict_keys = list(filters_dict.keys())
            filter_data = {}
            for tag in product_tags:
                for filter_name in filter_dict_keys:
                    if tag in list(filters_dict[filter_name].keys()):
                        value = filters_dict[filter_name][tag]
                        if filter_name in filter_data:
                            filter_data[filter_name].append(value)
                        else:
                            filter_data.update({filter_name: [value]})

            if not color:
                color = ', '.join(filter_data.get('Color', []))

            if not color:
                color = record.get('product_info',{}).get('color','')

            # model = re.sub(color.replace(', ','/'), '', model)
            model = re.sub(re.compile(
                r"\bMen's\b|\bMen\b", re.IGNORECASE), '', model)
            sku = record.get('sku', '')
            model = re.sub(sku, '', model)

            model = f'{sku} {model}'
            model = re.sub(r'\s+', ' ', model) ##some multiple spaces are present in title by default
            occurence = 2
            count = 0
            new_title=[]
            for ti in model.split():
                if ti=='G-SHOCK':
                    count+=1
                    if count!=occurence:
                            new_title.append(ti)
                else:
                    new_title.append(ti)
            model = ' '.join(new_title)
            model = re.sub(re.compile(r'& {color}|{color} &'.format(color=color.replace(', ','/')),re.IGNORECASE),'',model)
            model = re.sub(re.compile(r'{color}'.format(color=color.replace(', ','/')),re.IGNORECASE),'',model)
            for col in color.split(', '):
                model = re.sub(r'{color}\s*\&*|&*\s*{color}'.format(color=col),'',model)

            model = re.sub(r'X G-Shock|Heart Rate & GPS|{brand}'.format(brand=record.get('brand','')), '', model, flags=re.IGNORECASE)
            model = re.sub(re.compile(r'\bWatches\b',re.IGNORECASE),'Watch',model)

            model = re.sub(r'\||/','',model)
            model=re.sub(r'\s-|-\s|-$',' ',model)
            model=re.sub(r'\s,|,\s|,$',' ',model)
            model = re.sub(r'\s+',' ',model).strip() ##after doing the transformation removing unnecessary white spaces
            model = re.sub('GBA800-9A Digital Bluetooth Watch GBD800-9A', 'GBA800-9A Digital Bluetooth Watch', model, flags=re.IGNORECASE)
            # model = re.sub('GBDH1000-1 Digital Watch', 'BDDG1001-1 Digital Watch', model, flags=re.IGNORECASE)
            model = re.sub('GA2100-1A1 Minimalist Watch Carbon Fiber', 'GA2100-1A1 Carbon Fiber Minimalist Watch', model, flags=re.IGNORECASE)

            try:
                water_resistant = re.findall(re.compile(
                    r'(\d+\s*-*[Meter|M]*) Water Resistan', re.IGNORECASE), ', '.join(record['specs_data']))[0]
            except IndexError:
                water_resistant = re.findall(re.compile(
                    r'(\d+\s*-*[Meter|M]*) Water Resistan', re.IGNORECASE), ', '.join(record['features_list']))
                if water_resistant:
                    water_resistant = water_resistant[0]
                else:
                    water_resistant = ''
            water_resistant = re.sub(r'-meter|\s*Meter', 'm', water_resistant, flags=re.IGNORECASE).lower().strip()

            try:
                battery_life = re.findall(
                    r'Approx\. battery life:\s*(\d+\s+years)', ', '.join(record['specs_data']))[0]
            except IndexError:
                battery_life = ''

            category = ''
            if 'men' in product_tags:
                category = "Men's Wristwatches"

            description = record.get('description', '')
            description = ' '.join([des.strip() for des in description]).replace('\n', ' ')
            description = re.sub(r'\s+', ' ', description).strip()

            if 'wome' in product_tags or record['id'] == 'https://www.gshock.com/watches':
                return None

            transformed_record = {
                'id': record.get('id', ''),
                'item_id': hex_dig,
                'crawl_date': record.get('crawlDate', '').split(',')[0],
                'status_code': record.get('statusCode', ''),

                'brand': record.get('brand', ''),
                'category': category,

                'title': record.get('title', ''),
                'image': record.get('image', ''),

                'description': description,

                'model': model,

                'color': color,
                'material': ', '.join(filter_data.get('Material', [])),
                'function': ', '.join(filter_data.get('Function', [])),
                'product_line': ', '.join(filter_data.get('Collections', [])),
                'water_resistance': water_resistant,
                'model_sku': record.get('sku', ''),
                'battery_life': battery_life,
                'price': {
                    'currency_code': 'USD',
                    'amount': record.get('price', '')
                },
            }
            return transformed_record

        else:
            return None
