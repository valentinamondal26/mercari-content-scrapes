# ./run_pipeline_script.sh APP_NAME="isatapia" ENV="stage" JOB_CONFIG="isatapia-womens_shoes-isatapia-chain-pipeline.json" BRANCH="master" KG_INSTANCE="knowledge_review"

for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

    case "$KEY" in
            APP_NAME)              APP_NAME=${VALUE} ;;
            ENV)    ENV=${VALUE} ;;
            JOB_CONFIG)    JOB_CONFIG=${VALUE} ;;
            BRANCH)    BRANCH=${VALUE} ;;
            KG_INSTANCE)    KG_INSTANCE=${VALUE} ;;
            *)
    esac
done

if [ -z "$BRANCH" ]
then
    BRANCH="master"
fi

if [ -z "$KG_INSTANCE" ]
then
    KG_INSTANCE="knowledge_review"
fi

echo "APP_NAME=$APP_NAME"
echo "ENV=$ENV"
echo "JOB_CONFIG=$JOB_CONFIG"
echo "BRANCH=$BRANCH"
echo "KG_INSTANCE"=$KG_INSTANCE


git fetch
git checkout $BRANCH
git pull
rm us.zip; zip -r us.zip us  > /dev/null && gsutil cp us.zip gs://content_us/framework/$ENV/dependencies/us.zip
gsutil -m cp us/$APP_NAME/configs/*.json gs://content_us/framework/$ENV/dependencies/configs/apps/$APP_NAME/
python utils/load_jobs_config_to_pipeline_table_settings.py -a us/$APP_NAME/jobs/$JOB_CONFIG -i $KG_INSTANCE -e $ENV
PIPELINE_ID=`cat us/$APP_NAME/jobs/$JOB_CONFIG | python -c 'import json,sys;obj=json.load(sys.stdin);print(obj.get("name", ""));'`
echo PIPELINE_ID=$PIPELINE_ID
python utils/trigger_pipeline.py -p $PIPELINE_ID -e $ENV