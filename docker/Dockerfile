FROM python:3.7-slim-buster

ENV HOME /opt

RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install unzip && \
    apt-get install --assume-yes --no-install-recommends \
    gcc \
    jq \
    libffi-dev \
    libssl-dev \
    libxml2-dev \
    libxslt1-dev \
    python-pip \
    python-dev \
    curl \
    gnupg2 \
    vim \
    zlib1g-dev && \
    apt-get clean && \
    rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/* /tmp/* /var/tmp/*

#RUN wget https://chromedriver.storage.googleapis.com/78.0.3904.105/chromedriver_linux64.zip
#RUN unzip chromedriver_linux64.zip
#RUN mv chromedriver /usr/local/bin

#RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
#    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list


#RUN apt-get update && apt-get -y install google-chrome-stable

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN apt-get update && apt-get install google-cloud-sdk

# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# upgrade pip
RUN pip install --upgrade pip

# install selenium
RUN pip install selenium

RUN apt-get install unzip

COPY ./requirements.txt $HOME/requirements.txt

RUN pip install --upgrade pip && \
    pip install --upgrade \
    setuptools \
    wheel && \
    pip install -r $HOME/requirements.txt

COPY ./data_bags $HOME/data_bags

ENV GOOGLE_APPLICATION_CREDENTIALS $HOME/data_bags/sa_key.json

COPY ./entrypoint.sh $HOME/entrypoint.sh
RUN chmod +x $HOME/entrypoint.sh
RUN unset PYTHONPATH

ENTRYPOINT [ "/opt/entrypoint.sh" ]
