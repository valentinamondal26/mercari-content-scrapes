#!/bin/bash

if [ -z $env ]
then
    echo "enviroment variable is not set"
    exit 1
fi

echo "environment: $env"

PAYLOAD="$1"

gcloud auth activate-service-account --key-file /opt/data_bags/sa_key.json

if echo "$PAYLOAD" | jq -e 'has("code_path")'  > /dev/null; then
    GCS_CODE_PATH=$( jq -r  '.code_path' <<< "${PAYLOAD}" )
else 
   GCS_CODE_PATH="gs://content_us/framework/$env/dependencies/us.zip"
fi
echo $GCS_CODE_PATH

gsutil cp $GCS_CODE_PATH ./
if [ $? != 0 ]
then
    echo "Failed to fetch the code, $GCS_CODE_PATH"
    exit 1
fi

unzip us.zip > /dev/null
cd us

echo "pwd: $(pwd)"
export PYTHONPATH="${PYTHONPATH}:$(pwd)"

echo "payload="$PAYLOAD""

python common/crawl_trigger.py --payload="$PAYLOAD"

