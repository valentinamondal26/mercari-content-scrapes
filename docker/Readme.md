Comma

To add new dependencies, add them to requirements.txt file

To build image:
``
docker build -t gcr.io/mercari-us-de/crawler-<env>:v1.0.0 -f Dockerfile .
``

To run docker instance with mounted volume
``
docker run -v <local_path_to_mount_inside_container>:/local gcr.io/mercari-us-de/crawler-<env>:v1.0.0 &
``

To run a spider directly:
``
docker run -v <local_path_to_mount_inside_container>:/local gcr.io/mercari-us-de/crawler-<env>:v1.0.0 <spider_name>
``

To stop running docker container:
``
docker ps
docker stop <container_id>
``

To push image to the gcr:
``
gcloud docker -- push gcr.io/mercari-us-de/crawler-<env>:v1.0.0
[or]
For Docker versions > 18:
gcloud auth configure-docker
docker push gcr.io/mercari-us-de/crawler-<env>:v1.0.0
``

To add proxy-secret to the cluster:
``
kubectl create secret generic proxy-secret --from-literal=username=<base64_encoded_username> --from-literal=password=<base64_encoded_password>
``
