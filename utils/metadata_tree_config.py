import requests
import json
import uuid
import copy
from pprint import pprint
import configs as cfg
import normalizer
import argparse


def read_all_tree_config(env='stage'):
    url = f"http://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn"
    queryparams = {
        'db': 'metadatadb',
        'fn': 'sp_a_metadata_tree_configuration_read_all',
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    tree_config_response = requests.get(url, headers=headers, params=queryparams)
    if tree_config_response.status_code == 200:
        return tree_config_response.json()
    else:
        return None


def fetch_tree_configuration_by_tree_name(tree_name, env):
    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    queryparams = {
        'db': 'metadatadb',
        'fn': 'sp_a_metadata_tree_configuration_read_by_fieldname',
        'fieldname': 'name',
        'value': tree_name
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    tree_config_response = requests.get(url, headers=headers, params=queryparams)
    if tree_config_response.status_code == 200:
        response_json = tree_config_response.json()
        return response_json[0] if response_json else None
    else:
        return None


def delete_tree_configuration(configuration_id, env):
    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    queryparams = {
        'db': 'metadatadb',
        'fn': 'sp_a_metadata_tree_configuration_delete',
        'metadataTreeConfigurationId': configuration_id
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }

    tree_config_response = requests.get(url, headers=headers, params=queryparams)
    if tree_config_response.status_code == 200:
        return True
    else:
        print(f'Error: {tree_config_response.status_code} \n {tree_config_response.json()}')
        return False


def normalize_tree_config(tree_config):
    for criteria in tree_config["criteria"]:
        entityResponse = normalizer.normalize(criteria.get('entity'), criteria.get('attribute'), env=env)
        if entityResponse \
                and not entityResponse.get('entity_id').startswith('000_') \
                and not entityResponse.get('attribute_id').startswith('000_'):
            criteria.update({'entity_id': entityResponse.get('entity_id')})
            criteria.update({'attribute_id': entityResponse.get('attribute_id')})
        else:
            raise Exception(f'Failed to normalize: {criteria}')

    for heirarchy in tree_config['hierarchy']:
        entityResponse = normalizer.normalize(heirarchy.get('entity_name'), '', env=env)
        if entityResponse \
                and not entityResponse.get('entity_id').startswith('000_'):
            heirarchy.update({'entity_id': entityResponse.get('entity_id')})
        else:
            raise Exception(f'Failed to normalize: {heirarchy}')

    return tree_config


def create_item_name_suggest_config(tree_name, tree_config):
    hierarchy_config = copy.deepcopy(tree_config['hierarchy'])
    for i, entity in enumerate(hierarchy_config):
        prefix = ''
        if i == 0 \
                and len(tree_config['criteria']) > 1 \
                and tree_config['criteria'][1]['entity'] == 'brand':
            prefix = tree_config['criteria'][1]['attribute'] + ' '

        entity.pop('required', None)
        entity.update({
            'entity_name': entity['entity_name'].capitalize(),
            'suffix': '',
            'order': i+1,
            'prefix': prefix,
        })

    item_name_suggest = {
        'hierarchy': hierarchy_config,
        'tree_reference': tree_name,
        'gcs_upload': str(True),
        'filename': 'listing-name-suggestions/' + tree_name + '.csv',
        'score': 10000,
        'tree_id': '',
    }
    return item_name_suggest


def create_tree_configuration(tree_name, tree_config, env):
    print(f'creating tree configuration for {tree_name}')
    tree = {
        'active': True,
        'published': False,
        'config': {
            'metadata_tree': normalize_tree_config(tree_config),
            'item_name_suggest': create_item_name_suggest_config(tree_name, tree_config),
        },
        'name': tree_name,
        'metadataTreeConfigurationId': uuid.uuid4().hex,
        'status': 'new'
    }
    is_created = request_metadata_tree_create_or_update('sp_a_metadata_tree_configuration_create', tree, env)
    print(f'{"Success" if is_created else "Failed"}: Created tree configuration: {tree_name}')


def update_tree_configuration(old_tree_configuration, tree_config, env):
    tree_name = old_tree_configuration['name']
    print(f'Updating tree configuration for {tree_name}')
    old_tree_configuration.update({
        'config': {
            'metadata_tree': normalize_tree_config(tree_config),
            'item_name_suggest': create_item_name_suggest_config(tree_name, tree_config),
        },
    })
    is_updated = request_metadata_tree_create_or_update('sp_a_metadata_tree_configuration_update', old_tree_configuration, env)
    print(f'{"Success" if is_updated else "Failed"}: Updated tree configuration: {tree_name}')



def request_metadata_tree_create_or_update(fn, tree, env):
    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    params = {
        'db': 'metadatadb',
        'fn': fn,
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    response = requests.post(url, data=json.dumps(tree).replace("'", "''"), headers=headers, params=params)
    print(f'{fn} response:: {response.json()}')
    if response.status_code == 200 and  len(response.json()) > 0:
        return True
    else:
        print(f'Error: {response.status_code} \n {response.json()}')
        return False


def delete_tree_configuration_by_tree_name(tree_name, env):
    tree_config = fetch_tree_configuration_by_tree_name(tree_name, env=env)
    if not tree_config:
        print(f'Not Found/Missing Tree config: {tree_name}')
        return False
    tree_config_id = tree_config['metadataTreeConfigurationId']
    return delete_tree_configuration(tree_config_id, env=env)


def create_or_update_tree_configuration(tree_name, tree_config, env):
    tree_configuration = fetch_tree_configuration_by_tree_name(tree_name, env)
    if not tree_configuration:
        create_tree_configuration(tree_name, tree_config, env)
    else:
        update_tree_configuration(tree_configuration, tree_config, env)


if __name__ == "__main__":
    """
    Create/Update/Delete Tree configurations in metadatadb
    usage: metadata_tree_config.py [-h]

    Args:
        tree_name: tree name
        tree_config: tree configuration
        create: create or update the tree configuration
        list: fetch the tree configuration with the provided tree_name
        delete: delete the tree configuration with provided tree_name
        env: environment [dev | stage]
    """

    parser = argparse.ArgumentParser(description='MetadataTree Configuration')
    parser.add_argument('-n', '--tree_name', required=False, help='Tree Configuration Name')
    parser.add_argument('-C', '--create', required=False, action=argparse.BooleanOptionalAction, help='Create/Update Tree by Tree name and configuration')
    parser.add_argument('-L', '--list', required=False, action=argparse.BooleanOptionalAction, help='List Tree by Tree Name')
    parser.add_argument('-D', '--delete', required=False, action=argparse.BooleanOptionalAction, help='Delete Tree by Tree name')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')

    opts, rem_args = parser.parse_known_args()

    if not opts.tree_name:
        raise Exception('Tree Name is empty')

    tree_name = opts.tree_name
    env = opts.env

    if opts.create:
        parser.add_argument('-t', '--tree_config_json_string', required=True, help='Tree configuration')
        args = parser.parse_args(rem_args, namespace=opts)

        tree_config_json_string = args.tree_config_json_string
        tree_config_json_string = tree_config_json_string.replace(u'\u2019', "'")
        tree_config = json.loads(tree_config_json_string)

        create_or_update_tree_configuration(tree_name=tree_name, tree_config=tree_config, env=env)
    elif opts.list:
        tree = fetch_tree_configuration_by_tree_name(tree_name=tree_name, env=env)
        pprint(f'Missing/Not Found: {tree_name} in {env}' if not tree else tree)
    elif opts.delete:
        is_deleted = delete_tree_configuration_by_tree_name(tree_name=tree_name, env=env)
        print(f'{"Success" if is_deleted else "Failed"}: Delete the tree_config: {tree_name} in {env}')
    else:
        raise Exception('please choose the action on the tree name [--create | --list | --delete]')

