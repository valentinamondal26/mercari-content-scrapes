import requests
import json
import configs as cfg
import re
import sys, os
import subprocess
import argparse


def create_crawl_config(crawl_id, crawl_config, crawl_type='null', env='dev'):

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    payload = {
        "crawlDataId": crawl_id,
        "name": crawl_id,
        "crawl_data_id": crawl_id,
        "configurationId": '',
        "crawlType": crawl_type,
        "targetResourceType": "null",
        "settings": crawl_config,
        "next_run_time": None,
        "active": True,
        "crawl_status" : '',
        "size": 2
    }
    query_params = {'db': 'contentdb', 'fn': 'sp_a_crawl_data_create'}
    response = requests.request('POST', url, data=json.dumps(payload).replace("'", "''"), params=query_params, headers=headers)
    rs_crawlDataId = response.json()[0]['rs_crawlDataId']
    print(f"env: {env}, Created: {rs_crawlDataId}")
    return rs_crawlDataId


def get_crawl_config(crawl_id, env='dev'):

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    query_params = {
        'db': 'contentdb',
        'fn': 'sp_a_crawl_data_read_by_fieldname',
        'fieldname': 'crawlDataId',
        'value': crawl_id}
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    response = requests.request('GET', url, headers=headers, params=query_params)
    return response.json()


def get_all_crawl_config(env='dev'):
    url = f"http://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn?db=contentdb&fn=sp_a_crawl_data_read_all"
    payload={}
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    return response.json()


def delete_crawl_configuration(crawl_id, env='dev'):

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    query_params = {
        'db': 'contentdb',
        'fn': 'sp_a_crawl_data_delete',
        'crawlDataId': crawl_id,
    }
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    payload={}
    files={}

    response = requests.request("POST", url, params=query_params, headers=headers, data=payload, files=files)
    if response.status_code == 200 \
        and response.json() \
        and response.json()[0]['rs_crawlDataId'] == crawl_id:
        return True
    else:
        print(response.text)
        return False


def generate_crawl_id(source, category, brand):
    crawl_id = f'{source}-{category}-{brand}'
    crawl_id = re.sub(re.compile(r"['&,\.]"), '', crawl_id)
    crawl_id = re.sub(r'\s+', '_', crawl_id).strip().lower()
    return crawl_id


def update_crawl_config(existing_crawl_config, new_crawl_config, crawl_type='null', env='dev'):

    payload = {
        "crawlDataId": existing_crawl_config['crawlDataId'],
        "name": existing_crawl_config['name'],
        "crawl_data_id": existing_crawl_config['crawl_data_id'],
        "configurationId": '',
        "crawlType": crawl_type,
        "targetResourceType": "null",
        "settings": new_crawl_config,
        "active": True,
        "crawl_status" : '',
        "size": 2
    }
    if 'next_run_time' in existing_crawl_config:
        payload["next_run_time"] =  existing_crawl_config.get('next_run_time', '')

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    query_params = {'db': 'contentdb', 'fn': 'sp_a_crawl_data_update'}
    response = requests.request('POST', url, data=json.dumps(payload).replace("'", "''"), params=query_params, headers=headers)
    rs_crawlDataId = response.json()[0]['rs_crawlDataId']
    print(f"env: {env}, Updated: {rs_crawlDataId}")
    return rs_crawlDataId


def create_or_update_crawl_config(crawl_id, crawl_config, crawl_type, env='dev'):
    response = get_crawl_config(crawl_id, env=env)
    if not response:
        return create_crawl_config(crawl_id=crawl_id, crawl_config=crawl_config, crawl_type=crawl_type, env=env)
    else:
        return update_crawl_config(existing_crawl_config=response[0], new_crawl_config=crawl_config, crawl_type=crawl_type, env=env)


def construct_and_create_crawl_config(app_name, spider_name, category, brand, crawl_sources, jira_id, pipelineId, tree_name, crawl_type, gcs_cache_location, env='dev', branch="master"):

    meta_crawl_types = [
        'crawl',
        'crawl+pipeline'
        'crawl_monitor',
        'generic',
    ]
    crawl_config = {
        "crawl": {
            "app_name": app_name,
            "brand": brand,
            "category": category,
            "crawlSource": ', '.join(crawl_sources),
            "spider_name":spider_name,
            'gcs_cache_location': gcs_cache_location,
        },
        "crawl_sources": crawl_sources,
        "jira_ids": [jira_id] if isinstance(jira_id, str) else jira_id,
        "pipelineId": [pipelineId] if isinstance(pipelineId, str) else pipelineId,
        "schedule": {
            "auto_schedule": False,
            "schdeule_time": "* * * * *"
        },
        "tree_name": [tree_name] if isinstance(tree_name, str) else tree_name,
        "spider_github_url": f'https://github.com/kouzoh/mercari-content-scrapes/blob/master/us/{app_name}/scrape/{app_name}/spiders/{spider_name}_spider.py',
    }

    # update the us.zip based on branch
    crawl_config["crawl"] = update_us_zip(crawl_config["crawl"], branch, env)

    crawl_id = generate_crawl_id(app_name, category, brand)
    return create_or_update_crawl_config(crawl_id=crawl_id, crawl_config=crawl_config, crawl_type=crawl_type, env=env)


def update_us_zip(payload, branch, env, preexec_fn=False):
    
    print("Updating us.zip")
    sys.path.append(os.getcwd())
    app_name = payload.get("app_name")
    category = payload.get("category")
    brand = payload.get("brand")
    cmd = './update_us_zip.sh APP_NAME={app_name} ENV={env} BRANCH={branch} CATEGORY={category} BRAND={brand} PIPELINE={pipeline}'
    cmd = cmd.split()
    cmd = '%'.join(cmd).format(app_name=app_name, env=env, branch=branch, category=category,brand=brand,pipeline="false")
    cmd = cmd.split('%')
    print(f"Executing command: {cmd}")
    p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,stdin=subprocess.PIPE,preexec_fn=os.setsid if preexec_fn else None)
    out, err = p.communicate()
    print(f"Command output: {str(out.decode('utf-8'))}")
    new_path = re.findall(r"\bNEW PATH=(.*)\b", out.decode("utf-8"))
    if not new_path or err:
        raise ValueError("Error While updating zip")
    payload.update({"code_path": new_path[0]})
    return payload

if __name__ == "__main__":
    """
    Create/Update/Delete Crawl configurations in contentdb
    usage: load_crawl_config_to_crawl_data_table_settings.py [-h] [-C | --create]
                                                            [-L | --list]
                                                            [-D | --delete] -e ENV
                                                            [-c CATEGORY] [-b BRAND] [-s SPIDER_NAME]
                                                            [-a APP_NAME] [-g GCS_CACHE_LOCATION]
                                                            [-ct CRAWL_TYPE] [-ci CRAWL_ID]
                                                            [-t TREE_NAMES [TREE_NAMES ...]]
                                                            [-p PIPELINE_IDS [PIPELINE_IDS ...]]
                                                            [-j JIRA_IDS [JIRA_IDS ...]]
                                                            [-u CRAWL_SOURCE_URLS [CRAWL_SOURCE_URLS ...]]
    Args:
        create: create or update the tree configuration
        list: fetch the tree configuration with the provided tree_name
        delete: delete the tree configuration with provided tree_name
        env: environment [dev | stage]

        category: crawl_config->category
        brand: crawl_config->brand
        app_name: project name or app_name
        spider_name: name of the spider
        gcs_cache_location: http cache location availbe in gcs, which would be used by the crawler

        crawl_type: Type of the crawler. ['crawl', 'crawl+pipeline', 'crawl_monitor', 'generic']
        tree_names: trees associated with this crawler
        pipeline_ids: pipelines associated with this crawler
        jira_ids: crawl requirement jira Id
        crawl_source_urls: crawl source urls which are crawled

        crawl_id: crawler_id to be fetched or deleted
    """

    parser = argparse.ArgumentParser(description='Crawler Configuration')
    parser.add_argument('-C', '--create', required=False, action=argparse.BooleanOptionalAction, help='Create/Update Crawl configuration')
    parser.add_argument('-L', '--list', required=False, action=argparse.BooleanOptionalAction, help='Fetch Crawl Configuration by config_id')
    parser.add_argument('-D', '--delete', required=False, action=argparse.BooleanOptionalAction, help='Delete Crawl Configuration by config_id')

    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')
    parser.add_argument('-r', '--branch', default='master', required=False, help='Git repo branch')

    opts, rem_args = parser.parse_known_args()

    env = opts.env

    if opts.create:
        parser.add_argument('-c', '--category', required=True, help='Category')
        parser.add_argument('-b', '--brand', required=True, help='Brand')
        parser.add_argument('-s', '--spider_name', required=True, help='Spider name')
        parser.add_argument('-a', '--app_name', required=True, help='App Name or Project Name')
        parser.add_argument('-g', '--gcs_cache_location', default='', required=False, help='HTTP cache Location in gcs')

        parser.add_argument('-ct', '--crawl_type', default='crawl',
            choices=['crawl', 'crawl+pipeline', 'crawl_monitor', 'generic'], required=False, help='Crawler Type')

        parser.add_argument('-t', '--tree_names', nargs='+', default=[], required=False, help='List of Associated Trees')
        parser.add_argument('-p', '--pipeline_ids', nargs='+', default=[], required=False, help='List of Associated Pipelines')
        parser.add_argument('-j', '--jira_ids', nargs='+', default=[], required=False, help='List of Associated Jira Ticket Ids')
        parser.add_argument('-u', '--crawl_source_urls', nargs='+', default=[], required=False, help='List of Crawl Source Urls')

        args = parser.parse_args(rem_args, namespace=opts)

        construct_and_create_crawl_config(
            app_name=args.app_name,
            spider_name=args.spider_name,
            category=args.category,
            brand=args.brand,
            crawl_sources=args.crawl_source_urls,
            jira_id=args.jira_ids,
            pipelineId=args.pipeline_ids,
            tree_name=args.tree_names,
            crawl_type=args.crawl_type,
            gcs_cache_location=args.gcs_cache_location,
            env=env,
            branch=args.branch
        )
    elif opts.list:
        parser.add_argument('-ci', '--crawl_id', required=True, help='Crawler Id')
        args = parser.parse_args(rem_args, namespace=opts)

        crawl_config = get_crawl_config(crawl_id=args.crawl_id, env=env)
        print(f'{json.dumps(crawl_config, indent=2)}')
    elif opts.delete:
        parser.add_argument('-ci', '--crawl_id', required=True, help='Crawler Id')
        args = parser.parse_args(rem_args, namespace=opts)

        is_deleted = delete_crawl_configuration(crawl_id=args.crawl_id, env=env)
        print(f'{"Success" if is_deleted else "Failed"}: Delete the crawl_config: {args.crawl_id} in {env}')
    else:
        raise Exception('please choose the action on the tree name [--create | --list | --delete]')
