import requests
import json
import configs as cfg
import argparse


def trigger_crawl_job_by_crawl_id(crawl_id, env='dev'):

    url = f'http://mario-api-{env}.endpoints.mercari-us-de.cloud.goog/api/trigger-crawler/{crawl_id}'
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='mario').decode('utf-8')),
    }
    response = requests.request('GET', url, headers=headers)
    print('crawl_job_id: ' + response.json()['crawl_id'])


if __name__ == "__main__":
    """
    Trigger Crawler Job by crawl_id
    usage: trigger_crawler.py [-h] -c CRAWL_ID -e ENV

    Args:
        crawl_id: crawler_config_name/crawl_id from contentb
        env: environment to trigger the crawler job

    Returns:
        prints crawler_job_id for the triggered crawl_id
    """

    parser = argparse.ArgumentParser(description='Trigger Crawler Job')
    parser.add_argument('-c', '--crawl_id', required=False, help='Crawler confiuration id to trigger')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Deployment Environment to trigger [dev | stage]')
    args = parser.parse_args()

    crawl_ids = [
    ]

    env = args.env

    if args.crawl_id:
        crawl_ids.append(args.crawl_id)

    if args.env:
        env = args.env

    for crawl_id in crawl_ids:
        trigger_crawl_job_by_crawl_id(crawl_id, env=env)
