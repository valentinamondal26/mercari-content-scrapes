import re
import os
import sys
import glob
import string
import argparse
from os.path import join, exists, abspath
from shutil import ignore_patterns, move, copy2, copystat
from stat import S_IWUSR as OWNER_WRITE_PERMISSION

import scrapy
from scrapy.commands import ScrapyCommand
from scrapy.utils.template import string_camelcase
# from scrapy.utils.template import render_templatefile, string_camelcase
from scrapy.exceptions import UsageError


def render_templatefile(path, **kwargs):
    """Helper functions for working with templates"""
    with open(path, 'rb') as fp:
        raw = fp.read().decode('utf8')

    content = string.Template(raw).safe_substitute(**kwargs)

    render_path = path[:-len('.tmpl')] if path.endswith('.tmpl') else path

    if path.endswith('.tmpl'):
        os.rename(path, render_path)

    with open(render_path, 'wb') as fp:
        fp.write(content.encode('utf8'))


TEMPLATES_TO_RENDER = (
    ('jobs', '${app_name}-${category}-${brand}-chain-pipeline.json',),
    ('scrape', 'scrapy.cfg',),
    ('scrape', '${app_name}', 'settings.py.tmpl'),
    ('scrape', '${app_name}', 'spiders', '${app_name}_spider.py.tmpl'),
)

IGNORE = ignore_patterns('*.pyc', '__pycache__', '.svn')


def _make_writable(path):
    current_permissions = os.stat(path).st_mode
    os.chmod(path, current_permissions | OWNER_WRITE_PERMISSION)

def _copytree(src, dst):
    """
    Since the original function always creates the directory, to resolve
    the issue a new function had to be created. It's a simple copy and
    was reduced for this case.

    More info at:
    https://github.com/scrapy/scrapy/pull/2005
    """
    ignore = IGNORE
    names = os.listdir(src)
    ignored_names = ignore(src, names)

    if not os.path.exists(dst):
        os.makedirs(dst)

    for name in names:
        if name in ignored_names:
            continue

        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        if os.path.isdir(srcname):
            _copytree(srcname, dstname)
        else:
            copy2(srcname, dstname)
            _make_writable(dstname)

    copystat(src, dst)
    _make_writable(dst)


def main(template_dir, project_dir, app_name, category, brand, Category, Brand):
    if exists(join(project_dir, 'scrape')):
        print(f'Error: project already exists in {abspath(project_dir)}')
        return

    _copytree(template_dir, abspath(project_dir))

    def move_or_rename_files():
        for filepath in glob.glob(project_dir+'/**' ,recursive=True):
            if 'app_name' in filepath or 'category' in filepath or 'brand' in filepath:
                filepath_updated = filepath.replace('app_name', app_name)
                if category:
                    filepath_updated = filepath_updated.replace('category', category)
                if brand:
                    filepath_updated = filepath_updated.replace('brand', brand)
                try:
                    move(filepath, filepath_updated)
                except:
                    print(f'Error: filepath: {filepath}, filepath_updated: {filepath_updated}')

    move(join(project_dir, 'scrape', 'app_name'), join(project_dir, 'scrape', app_name))
    move_or_rename_files()

    for paths in TEMPLATES_TO_RENDER:
        path = join(*paths)
        tplfile = join(project_dir, string.Template(path).substitute(app_name=app_name, category=category, brand=brand))
        render_templatefile(tplfile, app_name=app_name, AppName=string_camelcase(app_name), category=category, brand=brand, Category=Category, Brand=Brand)


def string_to_snake_case(name):
    CAMELCASE_INVALID_CHARS = re.compile(r'[^a-zA-Z\d]')
    name = CAMELCASE_INVALID_CHARS.sub('', name.title())
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Create Prject from template')
    parser.add_argument('-a', '--app_name', required=True, help='App Name or Project Name')
    parser.add_argument('-c', '--category', required=True, help='Category')
    parser.add_argument('-b', '--brand', required=True, help='Brand')
    args = parser.parse_args()

    if not args.app_name:
        print('Error: app_name cannot be empty')
        sys.exit()

    app_name = args.app_name
    category = args.category or ''
    brand = args.brand or ''
    category_normalized = string_to_snake_case(category) or 'category'
    brand_normalized = string_to_snake_case(brand) or 'brand'
    project_home = os.path.abspath(join(os.path.dirname(__file__), '../'))
    template_dir = join(project_home, 'us', 'template')
    project_dir = join(project_home, 'us', f'{app_name}')

    print(
        f'template_dir: {template_dir}',
        f'template_dir_exists: {exists(template_dir)}',
        f'project_dir: {project_dir}',
        f'project_dir_exists: {exists(project_dir)}',
        f'app_name: {app_name}',
        f'category: {category}',
        f'brand: {brand}',
        sep='\n'
    )
    main(template_dir=template_dir, project_dir=project_dir, app_name=app_name, category=category_normalized, brand=brand_normalized, Category=category, Brand=brand)
    print(f'created: {project_dir}')
