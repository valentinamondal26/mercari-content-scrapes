import json
import requests
import configs as cfg
import argparse
import sys, os
import subprocess
import re

def create_config(new_job_config, env):
    new_job_config['desc'] = new_job_config.get('desc', '').replace("'","-")
    settings_payload = {'payload': new_job_config}

    payload = {
        'pipelineId': new_job_config['name'],
        'pipelineType': 'Content Pipeline',
        'targetResourceType': '',
        'name': new_job_config['name'],
        'settings': settings_payload,
        'create_date': 'null',
        'modified_date': 'null',
        'details': {},
        'crawlDataId': 'null',
        'knowledgeDataId': 'null',
        'active': 'true',
        'pipelineConfigId': 'null',
    }
    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn?db=contentdb&fn=sp_a_pipeline_create'
    jwt_token = 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode("utf-8"))
    cfg.headers['Authorization'] = jwt_token
    response = requests.request('POST', url, data=json.dumps(payload), headers=cfg.headers)
    print(f"Created: {response.json()[0]['rs_pipelineId']}")


def get_pipeline_details(pipeline_id, env):

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    querystring = {"db": "contentdb", "fn": "sp_a_pipeline_read_by_fieldname", "fieldname": "pipelineId",
                   "value": pipeline_id}
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode("utf-8"))
    }

    response = requests.request('GET', url, params=querystring,headers=headers)
    job_config = None
    try:
        job_config = response.json()[0]
    except:
        pass

    return job_config


def update_config(existing_job_config, new_job_config, env):

    new_job_config['desc'] = new_job_config.get('desc', '').replace("'", '-')
    settings_payload = {'payload': new_job_config}

    payload = {
        'pipelineId': new_job_config['name'],
        'pipelineType': 'Content Pipeline',
        'targetResourceType': '',
        'name': new_job_config['name'],
        'settings': settings_payload,
        'create_date': existing_job_config['create_date'],
        'modified_date': 'null',
        'details': {},
        'crawlDataId': 'null',
        'knowledgeDataId': 'null',
        'active': 'true',
        'pipelineConfigId': 'null',
    }
    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn?db=contentdb&fn=sp_a_pipeline_update'
    jwt_token = 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8'))
    cfg.headers['Authorization'] = jwt_token
    response = requests.request('POST', url, data=json.dumps(payload), headers=cfg.headers)
    print(f"Updated: {response.json()[0]['rs_pipelineId']}")

def delete_config(pipeline_id, env):
    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'

    querystring = {'db': 'contentdb', 'fn': 'sp_a_pipeline_delete', 'pipelineId': pipeline_id}
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode("utf-8"))
    }

    response = requests.request('POST', url, headers=headers, params=querystring)
    print(f'Deleted: {response.text}')

def update_us_zip(payload, branch, env, preexec_fn=False):
    
    print("Updating us.zip")
    sys.path.append(os.getcwd())
    vars = [var for var in payload["arguments"] if "var" in var][0]["var"]
    vars = [v.split("=") for v in vars.split(",")]
    vars = dict(zip(list(zip(*vars))[0],list(zip(*vars))[1]))
    app_name = vars.get("app")
    category = vars.get("category")
    brand = vars.get("brand")
    cmd = './update_us_zip.sh APP_NAME={app_name} ENV={env} BRANCH={branch} CATEGORY={category} BRAND={brand} PIPELINE={pipeline}'
    cmd = cmd.split()
    cmd = '%'.join(cmd).format(app_name=app_name, env=env, branch=branch, category=category,brand=brand,pipeline="true")
    cmd = cmd.split('%')
    print(f"Executing command: {cmd}")
    p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,stdin=subprocess.PIPE,preexec_fn=os.setsid if preexec_fn else None)
    out, err = p.communicate()
    print(f"Command output: {str(out.decode('utf-8'))}")
    new_path = re.findall(r"\bNEW PATH=(.*)\b", out.decode("utf-8"))
    if not new_path or err:
        raise ValueError("Error While updating zip")
    
    # update pyfile
    payload["pyFiles"][0].update({"pyFile": new_path[0]})

    # update vars
    vars.update({'pyfile_path': new_path[0]})
    vars = ",".join([k+"="+v for k,v in vars.items()])
    for ctr,pair in enumerate(payload["arguments"]):
        if "var" in pair:
            payload["arguments"][ctr]["var"] = vars
    return payload

if __name__ == "__main__":
    """
    Status the job and the chaining pipeline status information
    usage: job_status.py [-h] -a JOB_ID -i KG_INSTANCE

    Args:
        job_id: job_id
        KG_INSTANCE: Knowledge DB instance (knowledge_v0 | knowledge_review)

    Returns:
        prints status of the job_id along with their chained job pipeline information
    """

    parser = argparse.ArgumentParser(description='Create/Update job config in the content_db')
    parser.add_argument('-a', '--job_config_filepath', required=False, help='Filepath to the Job configuration')
    parser.add_argument('-d', '--delete_pipeline_id', required=False, help='Pipeline_id to be deleted')
    parser.add_argument('-i', '--kg_instance', required=False, help='Knowledge DB instance')
    parser.add_argument('-r', '--branch', required=False, help='Git repo branch', default="master")
    parser.add_argument('-e', '--env', default='stage', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')
    args = parser.parse_args()

    env = args.env

    # delete the pipeline configuration by pipeline id
    delete_pipeline_ids = [
        # 'gshock-mens-wristwatches-gshock-trans-pl',
    ]
    if args.delete_pipeline_id:
        delete_pipeline_ids.append(args.delete_pipeline_id)
    for delete_pipeline_id in delete_pipeline_ids:
        print(get_pipeline_details(delete_pipeline_id, env=env))
        delete_config(delete_pipeline_id, env=env)

    # add/update pipeline job config
    job_config_filepaths = [
        # 'us/gshock/jobs/gshock-mens_wristwatches-gshock-chain-pipeline.json',
    ]

    if args.job_config_filepath:
        job_config_filepaths.append(args.job_config_filepath)

    for job_config_filepath in job_config_filepaths:
        with open(job_config_filepath,'r') as f:
            data = f.read()
            from string import Template
            data = Template(data).safe_substitute({'env': env})
            if args.kg_instance == 'knowledge_review':
                data = data.replace('pipelines/product/product_transformation_pipeline.json',
                    'pipelines/knowledge_review/product/product_transformation_pipeline.json')
            new_job_config = json.loads(data)

            # update the us.zip based on branch
            new_job_config = update_us_zip(new_job_config, args.branch, env)

            existing_job_config = get_pipeline_details(new_job_config.get('name', ''), env=env)
            if not existing_job_config:
                create_config(new_job_config, env=env)
            else:
                update_config(existing_job_config, new_job_config, env=env)

