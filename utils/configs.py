import time
import argparse
import google.auth.crypt
import google.auth.jwt
import os

#Update the service account json path
prod_key = 'data_bags/sa_key.json'

prod_account = 'content-pipeline@mercari-us-de.iam.gserviceaccount.com'

contentdb_endpoint = 'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/'

mario_endpoint = 'http://mario-api-{env}.endpoints.mercari-us-de.cloud.goog/'

trigger_pipeline_with_pipeline_id = mario_endpoint + 'api/trigger-pipeline/{pipeline_id}'

headers = {'Content-Type': "application/json", 'cache-control': "no-cache"}


def generate_jwt(sa_keyfile,
                 sa_email='account@project-id.iam.gserviceaccount.com',
                 audience='your-service-name',
                 expiry_length=3600):
    """
    Generate the jwt token for the given service account
    :param sa_keyfile: service account key file path
    :param sa_email: service account mail id
    :param audience: endpoint
    :param expiry_length: in seconds
    :return:
    """
    now = int(time.time())
    payload = {
        'iat': now,
        "exp": now + expiry_length,
        'iss': sa_email,
        'aud':  audience,
        'sub': sa_email,
        'email': sa_email
    }
    signer = google.auth.crypt.RSASigner.from_service_account_file(sa_keyfile)
    jwt = google.auth.jwt.encode(signer, payload)

    return jwt

def get_jwt_token(env='stage', endpoint='mario'):
    key = prod_key
    service_account = prod_account
    # mario_endpoint = mario_prod_endpoint if env == 'prod' else  mario_dev_endpoint
    audience = mario_endpoint.format(env=env) if endpoint == 'mario' else contentdb_endpoint.format(env=env)
    jwt = generate_jwt(sa_keyfile=key,
                       sa_email=service_account,
                       audience=audience,
                       expiry_length=3600)

    return jwt

if __name__ == "__main__":
    """
    Print the JWT Token
    usage: configs.py [-h] -e ENDPOINT -env ENVIRONMENT

    Args:
        endpoint: Endpoint
        env: Environment

    Returns:
        prints result JWT Token for the env and endpoint
    """

    parser = argparse.ArgumentParser(description='JWT token')
    parser.add_argument('-ep', '--endpoint', required=True, choices=['contentdb', 'mario'], help='Endpoint')
    parser.add_argument('-e', '--env', default='dev', required=True, choices=['dev', 'stage'], help='Environment')
    args = parser.parse_args()

    print(get_jwt_token(env=args.env, endpoint=args.endpoint))

