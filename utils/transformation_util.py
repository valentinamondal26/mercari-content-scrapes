import json
import pandas as pd
import importlib
import argparse
import jsonschema
import tempfile
import datetime
from string import Template
from os import path
import numpy as np
import os


from helper import import_csv_to_spreadsheet, get_kg_raw_items_to_gcs, \
    snake_case_to_camelCase, camelCase_to_snake_case, \
    make_sure_path_exists, jsonlines_to_csv, download_gcs_file


def pre_product_transformation(input_file, output_file, Mapper):
    if input_file.startswith('gs://'):
        temp = tempfile.NamedTemporaryFile()
        download_gcs_file(input_file, temp.name)
        input_file=temp.name
    make_sure_path_exists(output_file.rsplit('/', 1)[0])
    with open(input_file, 'r') as f, open(output_file, 'w+') as dest:
        df = pd.read_json(f, lines=True, dtype=False)
        df = df.fillna('')
        for index, item in df.iterrows():
            d=item.to_dict()
            out= Mapper().map(d)
            if out:
                dest.write(json.dumps(out))
                dest.write('\n')


def sample_pre_product_script(crawl_output, pre_product_output, m, schema=None):
    pre_product_transformation(crawl_output, pre_product_output, m)
    csv_filename = pre_product_output.rsplit('.', 1)[0] + '.csv'
    # print(csv_filename)
    if schema:
        pre_product_output=validate_schema(schema, pre_product_output)
    jsonlines_to_csv(pre_product_output, csv_filename)


def validate_schema(schema_filepath, input_filepath):
    path = input_filepath.rsplit('/', 1)[0]
    filename = input_filepath.rsplit('/', 1)[-1].split('.')[0]
    # A sample schema, like what we'd get from json.load()
    with open(input_filepath, 'r') as f1:

        df = pd.read_json(f1, lines=True)
        def lambda_validate(x):
            with open(schema_filepath, 'r') as f:
                schema = json.load(f)
                try:
                    y=x.to_dict()

                    jsonschema.validate(instance=y, schema=schema)
                    return True
                except Exception as e:
                    pass
            return False
        df.loc[:,'valid'] = df.loc[:,].apply(lambda x: lambda_validate(x), axis=1)
        df_passed = df[df['valid']==True]
        success_path = path+'/'+filename+'_passed.jl'
        df_passed.drop('valid', axis=1).to_json(success_path, orient='records', lines=True)
        print('passed', len(df_passed), success_path)
        df_failed=df[df['valid']==False]
        failed_path=path+'/'+filename+'_failed.jl'
        df_failed.drop('valid', axis=1).to_json(failed_path, orient='records', lines=True)
        print('Failed', len(df_failed), failed_path)
        return success_path


def transform(pipeline_config_filepath, spreadsheet_url=None, env='dev'):
    var = ''
    pre_product_map_class = ''
    with open(pipeline_config_filepath, 'r+') as f:
        data = f.read()
        pipeline_config = Template(data).safe_substitute({'env': env})
        pipeline_config_json = json.loads(pipeline_config)
        vars = list(map(lambda x: x.get('var'), filter(lambda x: x if x.get('var', '') else None, pipeline_config_json.get('arguments'))))
        if len(vars) == 1:
            var = vars[0]
        pre_product_map_class = pipeline_config_json.get('pipeline_properties', {}).get('modules', [{}])[0].get('properties', {}).get('map_class', '')

    def get_args(var):
        var_dict = {}
        for a in var.split(','):
            x = a.split('=')
            if len(x) == 2:
                var_dict[x[0]] = x[1]
        return var_dict
    var_dict = get_args(var)
    if var_dict.get('env', '') == '$env':
        var_dict['env'] = env
    # pprint(var_dict)

    timestamp = str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
    # timestamp = raw_items_gcs_path.split('/')[-1].replace('raw_items_', '').strip()
    home = os.environ['HOME']
    base_filepath = '{home}/transform/{env}/{app}/{category}/{brand}/'\
        .format(
            home = home,
            env=var_dict.get('env'),
            app=var_dict.get('app'),
            category=var_dict.get('category'),
            brand=var_dict.get('brand'),
            # timestamp=timestamp
        )
    make_sure_path_exists(base_filepath)

    pre_product_map_class = Template(pre_product_map_class).substitute(var_dict)

    # preproduct_transformation and save the result
    def pre_product_transformation(pre_product_map_class):
        # Get the current working directory
        cwd = os.getcwd()

        # Print the current working directory
        print("Current working directory: {0}".format(cwd))
        mapper_module, mapper_class = pre_product_map_class.rsplit(".", 1)
        print(mapper_module, mapper_class)
        mapper_type = getattr(importlib.import_module(mapper_module), mapper_class)
        # mapper = mapper_type()
        pre_product_output_path = base_filepath + 'pre_product_output.jl'
        sample_pre_product_script(f"gs://{var_dict.get('inputPath', '')}",
            pre_product_output_path, mapper_type, schema=None)
        pre_product_output_csv_path = pre_product_output_path.replace('.jl', '.csv')
        import_csv_to_spreadsheet(pre_product_output_csv_path, spreadsheet_url)
        return pre_product_output_csv_path

    pre_product_output_path = pre_product_transformation(pre_product_map_class)

    is_dedupe_pipeline = True if var_dict.get('dedupe_criteria_system_category', '') \
        or var_dict.get('dedupe_criteria_brand', '') else False
    if not is_dedupe_pipeline:
        print(f'Pre-Product Transformed output: {pre_product_output_path}')
        return

    raw_items_gcs_path = get_kg_raw_items_to_gcs(
        system_category=var_dict['dedupe_criteria_system_category'].replace('%20', ' ').replace("%27", "'").replace('%26', '&').replace('%2C', ','),
        brand=var_dict['dedupe_criteria_brand'].replace('%20', ' ').replace("%27", "'").replace('%26', '&').replace('%2C', ','),
        env=var_dict['env'])
    print(raw_items_gcs_path)

    raw_items_filepath = base_filepath + 'raw_items.jl'
    if not path.exists(raw_items_filepath) and raw_items_gcs_path.startswith('gs://'):
        download_gcs_file(raw_items_gcs_path, raw_items_filepath)

    raw_items_csv_filepath = base_filepath + 'raw_items.csv'

    jsonlines_to_csv(raw_items_filepath, raw_items_csv_filepath)
    print('raw_items_csv_filepath:', raw_items_csv_filepath)
    import_csv_to_spreadsheet(raw_items_csv_filepath, spreadsheet_url)

    # join the two dfs and save the intermediate result
    pre_product_output_path = pre_product_output_path.replace('.csv', '.jl')
    with open(raw_items_filepath) as f1, open(pre_product_output_path) as f2:
        df1 = pd.read_json(f1, lines=True)
        df2 = pd.read_json(f2, lines=True)
        df1_columns = list(df1.columns)
        df2_columns = list(df2.columns)
        df1 = df1.replace(np.nan, '', regex=True)
        df2 = df2.replace(np.nan, '', regex=True)
        pre_product_dedupe_columns = var_dict.get('pre_product_dedupe_column', '').split('+')
        kg_raw_items_dedupe_columns = snake_case_to_camelCase(var_dict.get('pre_product_dedupe_column', '')).split('+')
        def generate_key(x, dedupe_columns):
            keys = []
            for dedupe_column in dedupe_columns:
                keys.append(x[dedupe_column])
            return '/'.join(list(filter(None, keys)))
        df1['key'] = df1.apply(lambda x: generate_key(x, kg_raw_items_dedupe_columns), axis=1)
        df2['key'] = df2.apply(lambda x: generate_key(x, pre_product_dedupe_columns), axis=1)
        # df2['key'] = df2.apply(lambda x: f'{x.model} {x.series} {x.color} {x.case_size}', axis=1)

        raw_items_with_key_csv_filepath = base_filepath+'raw_items_with_key.csv'
        df1.to_csv(raw_items_with_key_csv_filepath, index=False)
        import_csv_to_spreadsheet(raw_items_with_key_csv_filepath, spreadsheet_url)

        pre_product_output_with_key_csv_filepath = base_filepath+'pre_product_output_with_key.csv'
        df2.to_csv(pre_product_output_with_key_csv_filepath, index=False)
        import_csv_to_spreadsheet(pre_product_output_with_key_csv_filepath, spreadsheet_url)

        # df1.drop_duplicates(subset="key", keep='first', inplace=True)
        # df1.to_csv(base_filepath+'df1_drop_duplicates.csv', index=False)
        df1.rename(columns={col: col+'_y' if not col=='key' else col for col in df1_columns}, inplace=True)

        df3 = pd.merge(df2, df1, how='left', on='key')
        # df3 = pd.merge(df2, df1, how='left', on='key', suffixes=('', '_y'))
        # df3 = df3.rename(columns={"id": "id_a"})
        df3['id_copy'] = df3['id']
        df3 = df3.replace(np.nan, '', regex=True)
        merged_intermedidate_csv_filepath = base_filepath + 'merged_intermediate.csv'
        df3.to_csv(merged_intermedidate_csv_filepath, index=False)
        print('merged_intermediate_csv_filepath:', merged_intermedidate_csv_filepath)
        import_csv_to_spreadsheet(merged_intermedidate_csv_filepath, spreadsheet_url)

        merge_function_class = var_dict.get('merge_func', '')
        merger_module, merger_class = merge_function_class.rsplit(".", 1)
        merger_type = getattr(importlib.import_module(merger_module), merger_class)
        merger = merger_type()
        def _merge_func(x, merger):
            record_1 = {}
            record_2 = {}
            for key, value in x.items():
                if key.endswith('_y'):
                    record_2[camelCase_to_snake_case(key.strip('_y'))] = value
                else:
                    record_1[key] = value

            return merger.merge(record_1, record_2)
        df4 = df3.apply(func=lambda x: _merge_func(x, merger), axis=1, result_type='expand')
        print(df4.columns)
        merged_csv_filepath = base_filepath + 'merged.csv'
        df4.to_csv(merged_csv_filepath, index=False)
        print('merged_csv_filepath:', merged_csv_filepath)
        import_csv_to_spreadsheet(merged_csv_filepath, spreadsheet_url)

        def f(x):
            id = x.id_y if x.id_y else x.id_x
    #         print(id)
            return id
        # df3['id'] = df3.apply(func=lambda x: x.id_y if x.id_y else x.id, axis=1)
        # print(df3.info())
        # merged_csv_filepath = base_filepath + 'merged.csv'
        # df3.to_csv(merged_csv_filepath, index=False, columns=df2_columns)
        # print('merged_csv_filepath:', merged_csv_filepath)
        # import_csv_to_spreadsheet(merged_csv_filepath, spreadsheet_url)
    # save the merged result


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Pre-Product Transformation Util Script')
    parser.add_argument('-p', '--pipeline_config_path', required=True, help='Pipeline configuration path')
    parser.add_argument('-s', '--spreadsheet_url', required=False, help='Spreadsheet url to upload the result')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')
    args = parser.parse_args()

    transform(
        pipeline_config_filepath=args.pipeline_config_path,
        spreadsheet_url=args.spreadsheet_url,
        env=args.env,
    )
