import requests
import json
import time
import configs as cfg
import argparse

def submit_job(pipeline_id):
    url = "http://35.202.167.254:5050/fn"

    querystring = {"db":"contentdb","fn":"sp_a_pipeline_read_by_fieldname","fieldname":"pipelineId","value":pipeline_id}

    response = requests.request("GET", url, params=querystring)
    print(response.text)

    for pipeline in response.json():
        url = 'http://104.197.133.103/api/jobs/'
        headers = {
            'Content-Type': 'application/json',
        }
        print(json.dumps(pipeline['settings']['payload']))
        payload = json.dumps(pipeline['settings']['payload'])
        response = requests.request('POST', url, data=payload, headers=headers)

        print(f'job_id: {response.text}')


def submit_pipeline_v2(pipeline_id, env):
    """
    triggers the pipeline for the given pipeline id
    pipeline_id : job id , job_id from contentb
    """

    cfg.headers['Authorization'] = 'Bearer {}'.format(cfg.get_jwt_token(env, endpoint='mario').decode("utf-8"))
    url = cfg.trigger_pipeline_with_pipeline_id.format(env=env, pipeline_id=pipeline_id)
    response = requests.request("GET", url, headers=cfg.headers)
    print(f'job_id: {response.text}, pipeline_id: {pipeline_id}')


if __name__ == "__main__":
    """
    Trigger pipeline by pipeline_id
    usage: trigger_pipeline.py [-h] -p PIPELINE_ID -e ENV

    Args:
        pipeline_id: job_config_name/pipeline_id from contentb
        env: environment to trigger the pipeline job

    Returns:
        prints job_id for the triggered pipeline_id
    """

    parser = argparse.ArgumentParser(description='Trigger Pipeline')
    parser.add_argument('-p', '--pipeline_id', required=False, help='Pipeline id to trigger')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Deployment Environment to trigger [dev | stage]')
    args = parser.parse_args()

    trigger_pipeline_ids = [
    ]

    env = args.env

    if args.pipeline_id:
        trigger_pipeline_ids.append(args.pipeline_id)

    if args.env:
        env = args.env

    for pipeline_id in trigger_pipeline_ids:
        print(pipeline_id)
        submit_pipeline_v2(pipeline_id=pipeline_id, env=env)