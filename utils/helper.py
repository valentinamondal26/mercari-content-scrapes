import json
import requests
import configs as cfg
import pandas as pd
import os
import re
import urllib
from google.cloud import storage


def make_sure_path_exists(path):
    import os
    import errno
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def jsonlines_to_csv(input_filename, output_filename):
    with open(input_filename, 'r') as f:
        df = pd.read_json(f, lines=True)
        df.to_csv(output_filename, index=False, sep=',')


def download_gcs_file(gcs_path, local_filepath=None, local_filename=None):
    o=urllib.parse.urlsplit(gcs_path)
    path = o.path.split('/', 1)[-1]
    bucket_name = o.netloc
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(path)
    destination_filename = local_filepath + (('/' + local_filename) if local_filename else '')
    blob.download_to_filename(destination_filename)
    return True


def snake_case_to_camelCase(snake_case):
    return re.sub('_.', lambda x: x.group()[1].upper(), snake_case)


def camelCase_to_snake_case(camelCase):
    from functools import reduce
    return reduce(lambda x, y: x + ('_' if y.isupper() else '') + y, camelCase).lower()


def get_kg_raw_items_to_gcs(system_category, brand, env):
    system_category = system_category.replace('%20', ' ')
    url = f"http://kg-{env}.endpoints.mercari-us-de.cloud.goog/exportRawItemsDataToGcs"
    payload = {
        "criteria": [
            {
                "attribute": system_category,
                "entity": "system_category"
            },
            {
                "attribute": brand,
                "entity": "brand"
            }
        ]
    }
    jwt = cfg.get_jwt_token(env=env, endpoint='content_db').decode("utf-8")
    headers = {
        'content-type': "application/json",
        'authorization': f'Bearer {jwt}',
    }
    response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
    print(response.text)
    return response.json().get('gcs_path', '')


def import_csv_to_spreadsheet(csv_filepath, spreadsheet_url):
    if not spreadsheet_url:
        return

    import gspread
    from oauth2client.service_account import ServiceAccountCredentials

    scope = ["https://spreadsheets.google.com/feeds", 'https://www.googleapis.com/auth/spreadsheets',
            "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]

    service_account_json = os.getenv('GOOGLE_APPLICATION_CREDENTIALS', '')
    credentials = ServiceAccountCredentials.from_json_keyfile_name(service_account_json, scope)
    client = gspread.authorize(credentials)

    spreadsheet = client.open_by_url(spreadsheet_url)
    with open(csv_filepath, 'r') as file_obj:
        worksheet_title = csv_filepath.rsplit('/', 1)[-1].replace('.csv', '')
        worksheet = None
        try:
            worksheet = spreadsheet.worksheet(worksheet_title)
        except:
            pass
        if not worksheet:
            worksheet = spreadsheet.add_worksheet(title=worksheet_title, rows="500", cols="50")
        # print(worksheet.id, worksheet.title)
        import csv
        spreadsheet.values_update(
            worksheet.title,
            params={'valueInputOption': 'USER_ENTERED'},
            body={'values': list(csv.reader(file_obj))}
        )
        worksheet.set_basic_filter()



if __name__ == "__main__":
    pass