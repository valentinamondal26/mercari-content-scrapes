import requests
import json
import configs as cfg
from pprint import pprint
import argparse


def get_crawl_job_details(crawl_job_id, env='dev'):

    url = f'https://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'
    query_params = {
        'db': 'contentdb',
        'fn': 'sp_a_crawl_schedule_history_read_by_fieldname',
        'fieldname': 'crawlScheduleHistoryId',
        'value': crawl_job_id}
    headers = {
        'Authorization': 'Bearer {}'.format(cfg.get_jwt_token(env=env, endpoint='contentdb').decode('utf-8')),
    }
    response = requests.request('GET', url, headers=headers, params=query_params)
    return response.json()[0]


if __name__ == "__main__":

    """
    Status the crawler job
    usage: crawler_job_status.py [-h] -a CRAWL_JOB_ID

    Args:
        crawl_job_id: crawl_job_id

    Returns:
        prints status of the job_id along with their chained job pipeline information
    """

    parser = argparse.ArgumentParser(description='Job Status')
    parser.add_argument('-a', '--crawl_job_id', required=False, nargs='+', default=[], help='Crawler Job Id')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')
    args = parser.parse_args()

    crawl_job_ids = [
    ]

    env = args.env

    if args.crawl_job_id:
        crawl_job_ids.extend(args.crawl_job_id)

    if args.env:
        env = args.env

    for crawl_job_id in crawl_job_ids:
        response = get_crawl_job_details(crawl_job_id, env=env)
        print(response['name'], ':', 'Completed' if response['task_completed'] else '')
        pprint(response['details']['crawl_details']['footer']['gcs_upload_path'])
