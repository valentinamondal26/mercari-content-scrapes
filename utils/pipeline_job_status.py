import configs as cfg

import requests
import json
import time
import argparse

def get_job_status(job_id, env):
    url = f'http://kg-{env}.endpoints.mercari-us-de.cloud.goog/fn'

    querystring = {
        'db': 'contentdb',
        'fn': 'sp_a_pipeline_schedule_history_read_by_fieldname',
        'fieldname':'pipelineScheduleHistoryId',
        'value': job_id,
    }

    jwt = cfg.get_jwt_token(env=env, endpoint='content_db').decode('utf-8')
    headers = {'Authorization': f'Bearer {jwt}'}

    response = requests.request('GET', url, params=querystring, headers=headers)
    group_id = response.json()[0]['group_id']
    querystring = {
        'db': 'contentdb',
        'fn': 'sp_a_pipeline_schedule_history_read_by_fieldname',
        'fieldname': 'group_id',
        'value': group_id,
    }

    jwt = cfg.get_jwt_token(env=env, endpoint='content_db').decode('utf-8')
    headers = {'Authorization': f'Bearer {jwt}'}

    response = requests.request('GET', url, params=querystring, headers=headers)
    print(group_id)
    data = response.json()
    data = sorted(data, key=lambda k: k.get('task_starting_date', 0))
    d = []
    for i in data:
        d.append(f"{i['pipelineScheduleHistoryId']} ({'Completed' if i['task_completed'] else ''})")
    print(' -> '.join(d))
    print(len(response.json()))


if __name__ == "__main__":
    """
    Status the job and the chaining pipeline status information
    usage: pipeline_job_status.py [-h] -a JOB_ID -e ENV

    Args:
        job_id: job_id
        env: environment where the pipeline job submitted

    Returns:
        prints status of the job_id along with their chained job pipeline information
    """

    parser = argparse.ArgumentParser(description='Job Status')
    parser.add_argument('-a', '--job_id', required=True, nargs='+', default=[], help='Job Id')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')
    args = parser.parse_args()

    job_ids = [
    ]

    env = args.env

    if args.env:
        env = args.env

    if args.job_id:
        job_ids.extend(args.job_id)

    for i in job_ids:
        get_job_status(i, env=env)
