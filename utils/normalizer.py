import configs as cfg
import json
import requests
import argparse

def normalize(entity, attribute, env):
    url = f"http://kg-{env}.endpoints.mercari-us-de.cloud.goog/entity/normalizer"

    querystring = {"entity": entity, "query": attribute}

    jwt = cfg.get_jwt_token(env=env, endpoint='content_db').decode("utf-8")
    headers = {'authorization': f"Bearer {jwt}"}

    response = requests.request("GET", url, headers=headers, params=querystring)

    result = None
    if response.status_code == 200:
        result = json.loads(response.text)
    else:
        print(f'Error: {response.status_code} \n {response.json()}')

    return result


if __name__ == "__main__":
    """
    Normalize entity and attribute
    usage: normalizer.py [-h] -e ENTITY -a ATTRIBUTE

    Args:
        entity: entity to be noramlized
        attribute: attribute to be noramlized
        env: environment where normalizer module deployed

    Returns:
        prints result Json with normalized entity_id and attribute_id
    """

    parser = argparse.ArgumentParser(description='Normalizer')
    parser.add_argument('-en', '--entity', required=False, help='Entity')
    parser.add_argument('-a', '--attribute', required=False, help='Attribute')
    parser.add_argument('-e', '--env', default='dev', choices=['dev', 'stage'], required=False, help='Environment [dev | stage]')
    args = parser.parse_args()

    d = [
        # ('system_category', "Women's Athletic Shoes"),
    ]

    env = args.env

    if args.entity and args.attribute:
        d.append((args.entity, args.attribute))

    if args.env:
        env = args.env

    for (entity, attribute) in d:
        data = normalize(entity=entity, attribute=attribute, env=env)
        print(json.dumps(data, indent=2))
