#!/bin/sh

# ./update_us_zip.sh APP_NAME="isatapia" ENV="stage" BRANCH="master" CATEGORY="Women's Shoes" BRAND="Isa Tapia" PIPELINE=true"

for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

    case "$KEY" in
            APP_NAME)              APP_NAME=${VALUE} ;;
            ENV)    ENV=${VALUE} ;;
            BRANCH)    BRANCH=${VALUE} ;;
            CATEGORY)    CATEGORY=${VALUE} ;;
            BRAND)    BRAND=${VALUE} ;;
            PIPELINE)    PIPELINE=${VALUE} ;;
            *)
    esac
done

if [ -z "$BRANCH" ]
then
    BRANCH="master"
fi

if [ -z "$KG_INSTANCE" ]
then
    KG_INSTANCE="knowledge_review"
fi

echo "APP_NAME=$APP_NAME"
echo "ENV=$ENV"
echo "BRANCH=$BRANCH"
echo "CATEGORY=$CATEGORY"
echo "BRAND=$BRAND"
echo "PIPELINE=$PIPELINE"


git fetch || exit 0
git checkout $BRANCH || exit 0
git pull || exit 0
rm us.zip;


CATEGORY=`echo $CATEGORY | python -c "import re,sys;obj=str(sys.stdin.read());print(re.sub(r'\s+', '_', re.sub(re.compile(r'[\'\&,\.]'), '', obj)).strip().lower()[:-1]);"`
BRAND=`echo $BRAND | python -c "import re,sys;obj=str(sys.stdin.read());print(re.sub(r'\s+', '_', re.sub(re.compile(r'[\'\&,\.]'), '', obj)).strip().lower()[:-1]);"`
NEW_PATH="gs://content_us/framework/${ENV}/dependencies/${APP_NAME}/${CATEGORY}/${BRAND}/us.zip"

zip -r us.zip us

if $PIPELINE; then
    echo "Upadting pipeline configs"
    gsutil -m cp us/$APP_NAME/configs/*.json gs://content_us/framework/$ENV/dependencies/configs/apps/$APP_NAME/
fi
gsutil cp us.zip $NEW_PATH


echo "NEW PATH=$NEW_PATH"